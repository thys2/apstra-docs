# Minimal makefile for Sphinx documentation
#

# You can set these variables from the command line, and also
# from the environment for the first two.
SPHINXOPTS    ?=
SPHINXBUILD   ?= sphinx-build
SOURCEDIR     = source
BUILDDIR      = build

# Put it first so that "make" without argument is like "make help".
help:
	@$(SPHINXBUILD) -M help "$(SOURCEDIR)" "$(BUILDDIR)" $(SPHINXOPTS) $(O)

.PHONY: help Makefile

# Catch-all target: route all unknown targets to Sphinx using the new
# "make mode" option.  $(O) is meant as a shortcut for $(SPHINXOPTS).
%: Makefile
	@$(SPHINXBUILD) -M $@ "$(SOURCEDIR)" "$(BUILDDIR)" $(SPHINXOPTS) $(O)

a:
	# source env/bin/activate

install:
	pip install -r requirements.txt

build-sphinx:
	docker build -t apstra-sphinx .

build-sphinx-pdf:
	docker build -t apstra-sphinx-pdf -f Dockerfile.pdf .

gen:
	docker run --rm -v $(CURDIR):/docs apstra-sphinx make html

b:
	docker run --rm -v $(CURDIR):/docs apstra-sphinx sphinx-build -b html source build/html

pdf:
	docker run --rm -v $(CURDIR):/docs apstra-sphinx-pdf make latexpdf
