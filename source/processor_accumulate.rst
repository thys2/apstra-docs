=====================
Processor: Accumulate
=====================
The Accumulate processor used in IBA probes creates one (N/DS) time-series output
for each input with the same properties; each time the input changes, it takes its
timestamp and value and appends it to the corresponding output series. If total
duration (total_duration) is set and the length of the output time series in time
is greater than total_duration, it removes old samples from the time series until
this is no longer the case. If max samples (max_samples) is set and the length of
the output time series in terms of number of samples is greater than max_samples,
it removes old samples from the time series until this is no longer the case.

**Input Types** - Number-Set (NS), Discrete-State-Set (DSS)

**Output Types** - NSTS, DSSTS

**Properties**

   Max Samples (max_samples)
     Limits the maximum number of samples or an expression that evaluates to
     number of samples (default:1024)
   Total Duration (total_duration)
     Limits the number of samples by their total duration. (in seconds) or an
     expression that evaluates to number of seconds (default:0)

   .. include:: includes/processors/graph_query.rst

   .. include:: includes/processors/non_collector_graph_query.rst

   .. include:: includes/processors/enable_streaming.rst

Accumulate Example
------------------
Assume a configuration of

.. code-block:: none

  max_samples: 3
  total_duration: 0

Assume the following input at time t=1

.. code-block:: none

  [if_name=eth0] : "up"
  [if_name=eth1] : "down"
  [if_name=eth3] : "up"

We have the following output at time t=1

.. code-block:: none

  [if_name=eth0] : [{"up", 1 second"}]
  [if_name=eth1] : [{"down", 1 second"}]
  [if_name=eth3] : [{"up", 1 second"}]

Assume the following input at time t=2

.. code-block:: none

  [if_name=eth0] : "down"
  [if_name=eth1] : "down"
  [if_name=eth3] : "up"

We have the following output at time t=2

.. code-block:: none

  [if_name=eth0] : [{"up", 1 second"}, {"down", 2 seconds"}]
  [if_name=eth1] : [{"down", 1 second"}]
  [if_name=eth3] : [{"up", 1 second"}]

Assume the following input at time t=3

.. code-block:: none

  [if_name=eth0] : "up"
  [if_name=eth1] : "down"
  [if_name=eth3] : "up"

We have the following output at time t=3

.. code-block:: none

  [if_name=eth0] : [{"up", 1 second"}, {"down", 2 seconds"}, {"up", 3 seconds"}]
  [if_name=eth1] : [{"down", 1 second"}]
  [if_name=eth3] : [{"up", 1 second"}]

Assume the following input at time t=4

.. code-block:: none

  [if_name=eth0] : "down"
  [if_name=eth1] : "down"
  [if_name=eth3] : "up"

We have the following output at time t=4

.. code-block:: none

  [if_name=eth0] : [{"down", 2 seconds"}, {"up", 3 seconds"}, {"down", 4 seconds"}]
  [if_name=eth1] : [{"down", 1 second"}]
  [if_name=eth3] : [{"up", 1 second"}]

If the expressions are used for max_samples or total_duration, then they are
evaluated for each input item and the corresponding key is added for each
output item.

.. code-block:: none

  max_samples: context.ref_max_samples * 2
  total_duration: context.ref_duration * 2

Sample input:

 .. code-block:: none

   [if_name=eth0, ref_max_samples=10, ref_duration=60] : "up"
   [if_name=eth1, ref_max_samples=20, ref_duration=120] : "down"

Output

 .. code-block:: none

   [if_name=eth0, max_samples=20, duration=120] : "up"
   [if_name=eth1, max_samples=40, duration=240] : "down"
