========
Topology
========

Topology Overview
=================
Topologies can be viewed at three levels of detail: grouped, compact, and full.
(Grouped and compact are new in AOS version 3.3.0.)

When a node is selected from the topology view (by clicking its element in the
topology, or by selecting it from the **Selected Nodes** drop-down list), the node
topology is shown. The selected node can be viewed in two different ways:
neighbors view and links view.

From the blueprint, navigate to **Staged / Physical / Topology**.

Grouped
-------
* To show servers, click the **Show Servers** check box.
* To show node name (and hostname as applicable) hover over an element (square).
* To show node details, select the node by either clicking its element or by
  selecting it from the **Selected Node** drop-down list.

.. image:: static/images/blueprints/staged/physical/topology/staged_topology_grouped_330.png

Compact
-------
* To show servers, click the **Show Servers** check box.
* To show node name (and hostname as applicable) hover over an element (square).
* To show node details, select the node by either clicking its element or by
  selecting it from the **Selected Node** drop-down list.

.. image:: static/images/blueprints/staged/physical/topology/staged_topology_compact_330.png

Full
----
* To make topology elements larger, click the **Expand Nodes** check box.
* To show the links between elements, click the **Show Links** check box.
* To show node name (and hostname as applicable) hover over an element.
* To change the labels (name, hostname, S/N) that are shown in the topology,
  select a different label from the **Topology Label** drop-down list.
* To show node details, select the node by either clicking its element or by
  selecting it from the **Selected Node** drop-down list.

.. image:: static/images/blueprints/staged/physical/topology/staged_topology_full_330.png

Neighbors View
--------------
* Aggregated server-leaf links are encircled (new in AOS version 3.3.0).
* To show unused ports, click the **Show Unused Ports** check box (new in AOS version
  3.3.0).
* To change the labels (name, hostname, S/N) that are shown in the topology,
  select a different label from the **Topology Label** drop-down list.
* Choose to show all neighbors or only specific types (leaf, spine, L2 server,
  access, etc.).

.. image:: static/images/blueprints/staged/physical/topology/staged_topology_neighbor_complex_330.png

Links View
----------

.. image:: static/images/blueprints/staged/physical/topology/staged_topology_links_330.png

Adding Links
============

.. image:: static/images/blueprints/staged/physical/topology/add_links_330.png

.. tip::

    When adding a link, you can review the config changes in the
    :ref:`incremental config <incremental_config>` to evaluate the changes.

Add Server Links
----------------
You can add L2 servers or L3 servers to a running blueprint by adding a server
link (as of AOS version 3.2). You can link to a new server, or to an existing
one to make it dual-homed. In an MLAG configuration, server links and external
links must be symmetrical to both switches in the MLAG. When changing a server
from single- to dual-homed, both links must be of the same speed.

#. From the blueprint, navigate to **Staged / Physical / Topology** and make a
   selection.
#. From the neighbors view or the links view of a leaf selection, click
   **Add links**, then select **Add server links**.
#. Click in the **Server** field.

   * To make an existing server dual-homed, select it from the drop-down list.
   * For a new server, click **Add new server**.

#. In the port layout, select an unused server port on the leaf logical device.

   .. image:: static/images/blueprints/staged/physical/topology/add_server_link1_330.png

#. Select the interface.
#. For **Select Source**, select **Logical Device**.
#. In the drop-down list, select the appropriate server logical device.
#. Click **Add Link**.

   .. image:: static/images/blueprints/staged/physical/topology/add_server_link2_330.png

#. If you're adding a new server, enter a **System Group Name**.
#. Select/enter a **Logical Link Name** and **LAG Mode**.
#. Click **Add** to stage the addition.

.. comments

    -----------------------------------------------
    - Need to add SIGNIFICANCE OF LINK NAME & GROUP
    -----------------------------------------------

Add Access Switch Links
-----------------------
Access switches have limited support. Apstra does not recommend using this feature
in a production network. See :doc:`Adding Access Layer Switches <access_switch>`
for more information.

Add Leaf Peer Links
-------------------
You can add MLAG peer links as of AOS version 3.2.

.. important::

  Do not use MLAG peer Links if the platform used does not support it. Currently,
  SONiC devices do not support MLAG L3 peers.

#. From the blueprint, navigate to **Staged / Physical / Topology**.
#. Select a leaf that is an MLAG member.
#. From the neighbors view or the links view of the selection, click **Add links**,
   then click **Add leaf peer links**.
#. Select the link type.

   * Peer Link - adds a link between the two leafs and adds a BGP session
     (between the two leafs) in the default vrf only.
   * L3 Peer Link - adds a link between the two leafs and adds a BGP session
     (between the two leafs) in the default vrf and in non-default vrfs.

#. Select an unused port for each MLAG member. (Only unused ports are selectable).
   When a port has been selected for each MLAG member, the **Add Link** button
   becomes clickable.
#. Click **Add Link**.
#. Click **Add** to stage the addition.

Add External Router Links
-------------------------
You can import external routers and assign external
router links from the topology selection view (as of AOS version 3.2).

#. From the blueprint, navigate to **Staged / Physical / Topology** and make a
   selection.
#. From the neighbors view or the links view of a selection, click **Add links**,
   then click **Add external router links**.

   .. image:: static/images/blueprints/staged/physical/topology/add_ext_rtr_link_330.png

#. Select an appropriate port on the leaf switch with the **External Router** role
   (defined in the respective interface map), then click **Add Link**.
#. If using an MLAG, select a port on the other MLAG member.
#. Add additional links, as needed.
#. Select the external router from the drop-down list. If the external
   router that you need has not been imported yet, check the
   **Show routers from Global Catalog** check box, then select the external router.
   It will be imported into the blueprint.
#. Choose Layer 2 or Layer 3 connectivity.
#. Click **Add** to add stage the addition.


Manually Overriding IP Addresses on External Links
--------------------------------------------------
When the remote end has already been configured and cannot be changed, you may
want to manually override the external link IP addresses.

#. From the blueprint, navigate to **Staged / Physical / Topology**.
#. Select the link to modify.

   .. image:: static/images/external_routers/external_router_assign3_320.png

#. Click the **Edit** button and make your changes.
#. Click **Save** to stage the changes.

If you are using external routers in an EVPN blueprint, you must add external
connectivity points (ECPs) in the security zone.

Editing Links
=============

.. image:: static/images/blueprints/staged/physical/topology/edit_links_330.png

Update Logical Links
--------------------
To edit the server logical link for existing leaf-server links follow the steps
below.

#. From the blueprint, navigate to **Staged / Physical / Topology** and make a
   selection.
#. From the neighbors view or the links view of a selection, click **Edit links**,
   then click **Update logical links and LAG Mode**.
#. Change logical link, as needed.

   .. image:: static/images/blueprints/staged/physical/topology/update_lag_330.png

#. Click **Update** to stage the change.

Update LAG Mode
---------------
The LAG mode between a server and a leaf can be changed. A common example is if an
individual dual-connected server was configured with LACP, and you want it to have
separate, individually configured layer-2 links. If the server links are in a
"LAG" mode (e.g. **LACP (Active**), and you would like to change it to *no LAG*,
follow the next steps:

#. From the blueprint, navigate to **Staged / Physical / Topology** and make a
   selection.
#. From the neighbors view or the links view of a selection, click **Edit links**,
   then click **Update logical links and LAG Mode**.
#. Select the involved links to be changed and choose the new LAG Mode per link
   based or from the **Apply to selection** option (see picture below). LAG Mode
   must be **No LAG**.
#. Change the **Logical Link** to different values across the links as it is
   highlighted in the example from the picture below. If any of the **Logical Link**
   values are the same when changing the LAG Mode to **No LAG**, you will receive
   an error.
#. When the **Logical Link** values are changed, all virtual network assignments
   for the port(s) are lost and will need to be reassigned. If changing a
   dual-connected server to LAG Mode **No LAG**, it is recommended to only change
   the second **Logical Link** value. Virtual network assignments will be retained
   on the first port.
#. Click **Update** and verify the new settings before committing the change.

   .. image:: static/images/blueprints/staged/physical/topology/update_logical_link1_330.png

If you would like to change from *no LAG* links to *LAG* Mode, follow the same
steps, set the same **Logical Link** values in all the links belonging to the
same group and select the desired *LAG* mode. Again, changing the **Logical Link**
value will result in virtual network assignment to be lost. However, using an
existing value will retain the virtual network assignment.

Change Link Speeds
------------------

#. From the blueprint, navigate to **Staged / Physical / Topology** and make a
   selection.
#. From the neighbors view or the links view of a selection, click **Edit links**,
   then click **Change link speeds**.

   .. image:: static/images/blueprints/staged/physical/topology/chg_link_speed_topology_330.png

#. Change link speeds, as needed.
#. Click **Update** to stage the change.

Deleting Links
==============
External router links, server links, and MLAG peer links can be removed as of
AOS version 3.2.

#. From the blueprint, navigate to **Staged / Physical / Topology** and make a
   selection.
#. From the links view of a selection, click the **Delete** button for the link to
   remove.

   .. image:: static/images/blueprints/staged/physical/topology/delete_link_330.png

#. Click **Delete** to stage the deletion.
