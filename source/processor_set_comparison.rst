=========================
Processor: Set Comparison
=========================
The Set Comparison processor does a set-comparison of input stages.

Accept two DS or NS inputs, called "A" and "B". There are three outputs:
A stage "A - B" that contains the items that are only in stage "A," a stage
"B - A" that contains the items that are only in stage "B," and a stage "A & B"
that contains the items that are in both stage "A" and stage "B."

When conducting the above operations, we first normalize all items in each stage
by dropping all the keys that are not in "significant_keys." It is an error if a
key in "significant_keys" is not present in either stage "A" or "B."

Furthermore, only the keys of each normalized item are considered; values are
preserved (and kept from stage "A" in the intersection output), but not considered
in the comparison operations.

Results are undefined if, when normalizing items in either stage_A or stage_B,
there is more-than-one item with a given set of key-value pairs.

**Input Types** - Discrete-Set (DSS), Number-Set (NS)

**Output Types**

**Properties**

   .. include:: includes/processors/significant_keys.rst

   .. include:: includes/processors/enable_streaming.rst

Set Comparison Example
----------------------
Consider we have inputs with device temperature information.

Input A:

.. code-block:: none

    [system_id=leaf1]: 45
    [system_id=leaf2]: 52
    [system_id=leaf3]: 61

Input B:

.. code-block:: none

    [system_id=leaf2]: 52
    [system_id=leaf4]: 64

Outputs will be the following.

A - B:

.. code-block:: none

    [system_id=leaf1]: 45
    [system_id=leaf3]: 61

B - A:

.. code-block:: none

    [system_id=leaf4]: 64

A & B:

.. code-block:: none

    [system_id=leaf2]: 52
