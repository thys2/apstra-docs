=============
Adding Device
=============
Please have an understanding of the
:doc:`AOS device configuration lifecycle <configuration_lifecycle>` before
working with devices.

.. note::
    If you are adding a device to replace a decommissioned faulty one (for RMA)
    and you plan to use the same management IP address, please make sure that
    the original device has been
    :ref:`decommissioned <device_decomm:removing device from aos management>`
    before the new device is assigned, as AOS expects each device to have a
    unique management IP address.

#. Install :doc:`device agent(s) <device_agents>` onto the device(s); then they
   will appear as managed devices in the out-of-service quarantined state.
#. :ref:`Acknowledge device(s) <ack_device>` to bring them under AOS management
   in the out-of-service ready state.
#. After :doc:`creating the blueprint <bp_create>`, assign
   :ref:`interface map(s) <staging_device_profiles>` (device profiles) to leafs
   and spines.
#. Assign :ref:`device system ID(s) <staging_devices>` to the devices and
   confirm that deploy mode(s) are set to **Deploy** to bring them in-service in
   the ready state.
#. :doc:`Commit <uncommitted>` the device assignment(s) to the blueprint to bring
   them in-service in the active state.

   .. image:: static/images/devices/managed_devices_is_active_330.png
