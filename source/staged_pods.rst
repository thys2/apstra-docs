====
Pods
====
From the blueprint, navigate to **Staged / Physical / Pods** to see details about
the staged pods in a **5-stage Clos network**. (The pod view only applies to
pod-based blueprints.) You can search for specific nodes or links and select a
layer to see build details, deploy modes, or uncommitted changes.

.. image:: static/images/blueprints/staged/physical/staged_pods_330.png

Changing Pod Name
=================
#. From the blueprint, navigate to **Staged / Physical / Pods**.
#. Click the name of the pod to change.
#. In **Pod Properties** (right panel) click the **Edit** button for the pod name.
#. Change the name and click the **Save** button to stage the change.

.. image:: static/images/blueprints/staged/physical/staged_pods_name_change_330.png
