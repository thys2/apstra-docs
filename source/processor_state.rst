================
Processor: State
================
The State processor checks that a value is one of the specified anomalous states.
It outputs DSS with anomaly values, such as 'true' if the value is in the
specified states, and otherwise, it returns 'false'. (previously called
'state_check' and 'in_state'). As of AOS version 3.1 state processor can support
multiple reference states and output is 'true' when input is in any of the
specified states.

**Input Types** - Discrete-State-Set (DSS), DSTS

**Output Types** - Discrete-State-Set (DSS)

**Properties**

   .. include:: includes/processors/graph_query.rst

   .. include:: includes/processors/non_collector_graph_query.rst

   Anomalous States
      Expression that evaluates to DS value or list of DS values which is used for
      the check. For example, it can be: "'true'" (expression evaluating to a
      string) or "['missing', 'unknown', 'down']" (expression evaluating to a list
      of strings).

   Anomaly MetricLog Retention Duration
      Retain anomaly metric data in MetricDb for specified duration in seconds
   Anomaly MetricLog Retention Size
      Maximum allowed size, in bytes of anomaly metric data to store in MetricDB
   Anomaly Metric Logging
      Enable metric logging for anomalies

   .. include:: includes/processors/enable_streaming.rst

   .. include:: includes/processors/raise_anomaly.rst

State Example
-------------

.. code-block:: none

    state: '"up"'

Sample Input (DS)

.. code-block:: none

  [if_name=eth0] : "up"
  [if_name=eth1] : "down"
  [if_name=eth3] : "up"

Sample Output (DSS)

.. code-block:: none

  [if_name=eth0] : "false"
  [if_name=eth1] : "true"
  [if_name=eth3] : "false"

If expression is used for the `state` field, then it's evaluated for each
input item, and it results into item-specific state value. Properties of the
respective output item are extended by the `state` property with value of
the evaluated expression.

.. code-block:: none

    state: expected_if_state

Sample Input (DS):

.. code-block:: none

   [if_name=eth0,expected_if_state=up] : "up"
   [if_name=eth1,expected_if_state=down] : "down"
   [if_name=eth3,expected_if_state=up] : "down"

Sample Output (DSS)

.. code-block:: none

   [if_name=eth0,state=up] : "false"
   [if_name=eth1,state=down] : "false"
   [if_name=eth3,state=up] : "true"
