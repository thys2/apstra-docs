============================
Resource Pools API Reference
============================
.. <API doc needs attention>

This reference demonstrates the resource group API usage with parity to the UI.
For full API documentation, view the REST Platform API reference under the AOS
Web UI.

To list resource group slots in a blueprint, perform an authenticated HTTP GET
to https://aos-server/api/blueprints/<blueprint_id>/resource_groups

Both **ASN pools** and **IP pools** must be assigned in order for a blueprint
to complete the build phase.

API - ASN Pools
===============

Creating an ASN Pool
--------------------

An example payload for creating an ASN Pool:

If an ID is not specified, one will be created and returned in the HTTP response.

.. code-block:: json

    {
        "id": "RFC6996-Private",
        "display_name": "RFC6996-Private",
        "tags": [ "default" ],
        "ranges": [
            {
                "last": 65534,
                "first": 64512
            }
        ]
    }

To create an ASN pool perform an HTTP POST to
https://aos-server/api/resources/asn-pools with a JSON payload.

.. code-block:: text

    curl 'https://192.168.25.250/api/resources/asn-pools?comment=create'
    -H 'AuthToken: eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6
    ImFkbWluIiwiY3JlYXRlZF9hdCI6IjIwMTctMDUtMzFUMDA6MjI6MDcuNTIwMTgzWiIsIn
    Nlc3Npb24iOiJjOTliOGVlOS05Y2NjLTRjZTAtYTY5NS0wODI3N2ZkYjA0ZDYifQ.FnJMR3
    crPoD0-lQRXnpPOJ8TCsRG9Wr-DaddnAIj6ko' - --data-binary '{"display_name"
    :"Example","ranges":[{"first":100,"last":200}],"tags":[]}' --compressed
    --insecure

Listing ASN Pools
-----------------

.. code-block:: text

    curl 'https://192.168.25.250/api/resources/asn-pools' -H 'AuthToken:
    eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6ImFkbWluIiwiY3JlY
    XRlZF9hdCI6IjIwMTctMDUtMzFUMDA6MjI6MDcuNTIwMTgzWiIsInNlc3Npb24iOiJjOTliO
    GVlOS05Y2NjLTRjZTAtYTY5NS0wODI3N2ZkYjA0ZDYifQ.FnJMR3crPoD0-lQRXnpPOJ8TCsR
    G9Wr-DaddnAIj6ko' --compressed --insecure

.. code-block:: json

    {
        "items": [
            {
                "created_at": "2017-05-30T12:56:07.293082Z",
                "display_name": "Private ASN",
                "id": "c23ea447-8f37-419a-9b1c-c48cc55d5b9c",
                "last_modified_at": "2017-05-30T12:56:07.293082Z",
                "ranges": [
                    {
                        "first": 65412,
                        "last": 65534,
                        "status": "pool_element_in_use"
                    }
                ],
                "status": "in_use",
                "tags": []
            }
        ]
    }

Deleting an ASN Pool
--------------------
To delete an ASN Pool perform an HTTP DELETE to
https://aos-server/resources/asn-pools/{pool_id}

A successful DELETE returns HTTP 200 OK.

.. code-block:: text

    curl
    'https://192.168.25.250/api/resources/asn-pools/d0312b4a-017e-4478-8b8d-df0417ce8d3b'
    -X DELETE -H 'AuthToken: eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2Vybm
    FtZSI6ImFkbWluIiwiY3JlYXRlZF9hdCI6IjIwMTctMDUtMzFUMDA6MjI6MDcuNTIwMTgzW
    iIsInNlc3Npb24iOiJjOTliOGVlOS05Y2NjLTRjZTAtYTY5NS0wODI3N2ZkYjA0ZDYifQ.FnJ
    MR3crPoD0-lQRXnpPOJ8TCsRG9Wr-DaddnAIj6ko' --compressed --insecure

Assigning an ASN Pool to a Blueprint
------------------------------------
To assign an IP pool to the blueprint perform an HTTP PUT to
https://aos-server/blueprints/<blueprint_id>/resource_groups/ip/<pool_name>

For instance, to post a resource pool to **spine_loopback_ips**, first obtain the ID
of the resource pool, and append it to a list for slot assignation.  When updating
the IP Pool resource group, specify all pools in the payload at the
same time.  We cannot add single pools, so PUT them all at once.

Payload:

.. code-block:: json

    {"pool_ids": ["pool_id1", "pool_id2", "pool_id3"] }

.. code-block:: text

    curl
    'https://192.168.25.250/api/blueprints/4c1e69c6-97bd-4c99-9504-7818f138b17f/resource_groups/asn/spine_asns'
    -X PUT -H 'AuthToken: eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2Vyb
    mFtZSI6ImFkbWluIiwiY3JlYXRlZF9hdCI6IjIwMTctMDUtMzFUMDA6MjI6MDcuNTI
    wMTgzWiIsInNlc3Npb24iOiJjOTliOGVlOS05Y2NjLTRjZTAtYTY5NS0wODI3N2ZkYj
    A0ZDYifQ.FnJMR3crPoD0-lQRXnpPOJ8TCsRG9Wr-DaddnAIj6ko'  --data-binary
    '{"pool_ids":["c23ea447-8f37-419a-9b1c-c48cc55d5b9c"]}' --compressed --insecure

A successful ASSIGNMENT returns HTTP 200 OK.


Unassigning an ASN Pool from a blueprint
----------------------------------------
Removing IP pools from the blueprint requires the user to PUT an empty pool_id
list to the blueprint with the payload ``[]``:

PUT to the HTTP endpoint https://aos-server/api/blueprints/<blueprint_id>
/resource_groups/asn/<pool_name>

With the payload:

.. code-block:: text

    { "pool_ids": [] }

.. code-block:: text

    curl
    'https://192.168.25.250/api/blueprints/4c1e69c6-97bd-4c99-9504-7818f138b17f/resource_groups/asn/spine_asns'
    -X PUT -H 'AuthToken: eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFt
    ZSI6ImFkbWluIiwiY3JlYXRlZF9hdCI6IjIwMTctMDUtMzFUMDA6MjI6MDcuNTIwMTgzWi
    IsInNlc3Npb24iOiJjOTliOGVlOS05Y2NjLTRjZTAtYTY5NS0wODI3N2ZkYjA0ZDYifQ.FnJ
    MR3crPoD0-lQRXnpPOJ8TCsRG9Wr-DaddnAIj6ko'  --data-binary '{"pool_ids":[]}'
    --compressed --insecure

If the request is successful there will be no response.

Listing the ASN Pools assigned to a blueprint
---------------------------------------------
Available ASN Pool resource groups for assignment can be shown with an HTTP GET
to https://aos-server/api/blueprints/<blueprint_id>/resource_groups

.. code-block:: text

    curl
    'https://192.168.25.250/api/blueprints/4c1e69c6-97bd-4c99-9504-7818f138b17f/resource_groups'
    -H 'AuthToken: eyJhbGciOiJIUzI1NwMTctMDUtMzFUMDA6MjI6MDcuNTIwMTgz
    WiIsInNlc3Npb24iOiJjOTliOGVlOS05Y2NjLTRjZTAtYTY5NS0wODI3N2ZkYjA0ZD
    YifQ.FnJMR3crPoD0-lQRXnpPOJ8TCsRG9Wr-DaddnAIj6ko' --compressed --insecure
    | python -m json.tool

.. code-block:: json

    {
        "items": [
            {
                "name": "leaf_asns",
                "pool_ids": [
                    "c23ea447-8f37-419a-9b1c-c48cc55d5b9c"
                ],
                "type": "asn"
            },
            {
                "name": "spine_asns",
                "pool_ids": [
                    "c23ea447-8f37-419a-9b1c-c48cc55d5b9c"
                ],
                "type": "asn"
            },
            {
                "name": "leaf_loopback_ips",
                "pool_ids": [
                    "56e8e0dc-babd-4652-92a5-fc37294a7b26"
                ],
                "type": "ip"
            },
            {
                "name": "mlag_domain_svi_subnets",
                "pool_ids": [
                    "ed7d8830-c703-4ac0-8252-77e0f272a677"
                ],
                "type": "ip"
            },
            {
                "name": "spine_leaf_link_ips",
                "pool_ids": [
                    "ed7d8830-c703-4ac0-8252-77e0f272a677"
                ],
                "type": "ip"
            },
            {
                "name": "spine_loopback_ips",
                "pool_ids": [
                    "56e8e0dc-babd-4652-92a5-fc37294a7b26"
                ],
                "type": "ip"
            },
            {
                "name": "to_external_router_link_ips",
                "pool_ids": [
                    "aae25699-9ad6-4382-8a7b-3f4a8ef582ac"
                ],
                "type": "ip"
            }
        ]
    }

API - IP Pools
==============

Creating an IP Pool
-------------------

JSON Payload for creating an IP Pool:

.. code-block:: json
    :caption: IP Pool creation

    {
        "id": "example_ip_pool",
        "display_name": "example_ip_pool",
        "tags": ["default"],
        "subnets": [
            {"network": "10.0.0.0/8"}
        ]
    }

The **subnets** section requires a list of dictionaries with keyword **network** and
value matching a CIDR mask.  The subnets cannot overlap with each other in
the same pool.  That is to say, 192.168.10.0/24 and 192.168.0.0/16 cannot
be configured in the same pool.

Tags are optional and are not currently used in AOS.
If ID is specified, it will be saved, otherwise an ID will be returned in the HTTP
Response after creating the pool.

An HTTP POST to https://aos-server/api/resources/ip-pools with JSON payload will
reply with the ID of the new IP pool.

.. code-block:: text

    curl 'https://192.168.25.250/api/resources/ip-pools' -X
    POST -H 'AuthToken: eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmF
    tZSI6ImFkbWluIiwiY3JlYXRlZF9hdCI6IjIwMTctMDUtMzFUMDA6MjI6MDcuNTIwMTgzWi
    IsInNlc3Npb24iOiJjOTliOGVlOS05Y2NjLTRjZTAtYTY5NS0wODI3N2ZkYjA0ZDYifQ.Fn
    JMR3crPoD0-lQRXnpPOJ8TCsRG9Wr-DaddnAIj6ko' --data-binary '{"display_name":
    "example_ip_pool","subnets":[{"network":"10.0.0.0/8"},{"network":
    "192.168.0.0/16"}],"tags":[]}' --compressed --insecure

.. code-block:: json

    {"id": "d0312b4a-017e-4478-8b8d-df0417ce8d3b"}

Listing IP Pools
----------------

Perform an HTTP GET to https://aos-server/api/resources/ip-pools -

.. code-block:: text

    jp@ApstraVM ~ $ curl 'https://192.168.25.250/api/resources/ip-pools' -H
    'AuthToken: eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6ImFkbW
    luIiwiY3JlYXRlZF9hdCI6IjIwMTctMDUtMzFUMDA6MjI6MDcuNTIwMTgzWiIsInNlc3Npb24
    iOiJjOTliOGVlOS05Y2NjLTRjZTAtYTY5NS0wODI3N2ZkYjA0ZDYifQ.FnJMR3crPoD0-lQRXnpP
    OJ8TCsRG9Wr-DaddnAIj6ko' --compressed --insecure | python -m json.tool

.. code-block:: json
    :caption: Listing IP Pools

    {
        "items": [
            {
                "created_at": "2017-05-30T12:56:37.190419Z",
                "display_name": "External Routers",
                "id": "aae25699-9ad6-4382-8a7b-3f4a8ef582ac",
                "last_modified_at": "2017-05-30T12:56:37.190419Z",
                "status": "in_use",
                "subnets": [
                    {
                        "network": "192.168.66.0/24",
                        "status": "pool_element_in_use"
                    }
                ],
                "tags": []
            },
            {
                "created_at": "2017-05-31T03:48:38.562331Z",
                "display_name": "example_ip_pool",
                "id": "d5046aa6-eab2-4990-9816-0a519ce1a8db",
                "last_modified_at": "2017-05-31T03:48:38.562331Z",
                "status": "not_in_use",
                "subnets": [
                    {
                        "network": "10.0.0.0/8",
                        "status": "pool_element_available"
                    },
                    {
                        "network": "192.168.0.0/16",
                        "status": "pool_element_available"
                    }
                ],
                "tags": []
            },
            {
                "created_at": "2017-05-30T12:56:50.576598Z",
                "display_name": "L3-CLOS",
                "id": "ed7d8830-c703-4ac0-8252-77e0f272a677",
                "last_modified_at": "2017-05-30T12:56:50.576598Z",
                "status": "in_use",
                "subnets": [
                    {
                        "network": "10.16.0.0/16",
                        "status": "pool_element_in_use"
                    }
                ],
                "tags": []
            },
            {
                "created_at": "2017-05-30T12:56:24.222906Z",
                "display_name": "Loopbacks",
                "id": "56e8e0dc-babd-4652-92a5-fc37294a7b26",
                "last_modified_at": "2017-05-30T12:56:24.222906Z",
                "status": "in_use",
                "subnets": [
                    {
                        "network": "10.254.0.0/16",
                        "status": "pool_element_in_use"
                    }
                ],
                "tags": []
            },
            {
                "created_at": "2017-05-31T03:49:15.485164Z",
                "display_name": "example_ip_pool",
                "id": "d0312b4a-017e-4478-8b8d-df0417ce8d3b",
                "last_modified_at": "2017-05-31T03:49:15.485164Z",
                "status": "not_in_use",
                "subnets": [
                    {
                        "network": "10.0.0.0/8",
                        "status": "pool_element_available"
                    },
                    {
                        "network": "192.168.0.0/16",
                        "status": "pool_element_available"
                    }
                ],
                "tags": []
            }
        ]
    }

Deleting an IP pool
-------------------

To delete an IP Pool perform an HTTP DELETE to
https://aos-server/resources/ip-pools/{pool_id}

A successful DELETE returns HTTP 200 OK and an empty JSON response ``{}``

.. code-block:: text

    curl
    'https://192.168.25.250/api/resources/ip-pools/d0312b4a-017e-4478-8b8d-df0417ce8d3b'
    -X DELETE -H 'AuthToken: eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZS
    I6ImFkbWluIiwiY3JlYXRlZF9hdCI6IjIwMTctMDUtMzFUMDA6MjI6MDcuNTIwMTgzWiIsInNl
    c3Npb24iOiJjOTliOGVlOS05Y2NjLTRjZTAtYTY5NS0wODI3N2ZkYjA0ZDYifQ.FnJMR3crPoD0
    -lQRXnpPOJ8TCsRG9Wr-DaddnAIj6ko' --compressed --insecure

Assigning an IP pool to a blueprint
-----------------------------------

To assign an IP pool to the blueprint perform an HTTP PUT to https://aos-server/blueprints/
<blueprint_id>/resource_groups/ip/<group_name>

For instance, to associate a resource pool **spine_loopback_ips** with a blueprint
first obtain the ID of the resource pool, and append it to a list for slot
assignation.  When updating the IP Pool resource group, specify all
pools in the payload at the same time.  We cannot add single pools, so PUT them
all at once. Instruct AOS to associate IP pool with ID
‘ed7d8830-c703-4ac0-8252-77e0f272a677’to the blueprint. You may have to GET
existing pool IDs prior to adding a new one to avoid deleting existing pools.

Payload:

.. code-block:: json

    {"pool_ids": ["pool_id1", "pool_id2", "pool_id3"] }

.. code-block:: text

    curl
    'https://192.168.25.250/api/blueprints/4c1e69c6-97bd-4c99-9504-7818f138b17f/resource_groups/ip/spine_loopback_ips'
    -X PUT -H 'AuthToken: eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6ImF
    kbWluIiwiY3JlYXRlZF9hdCI6IjIwMTctMDUtMzFUMDA6MjI6MDcuNTIwMTgzWiIsInNlc3Npb2
    4iOiJjOTliOGVlOS05Y2NjLTRjZTAtYTY5NS0wODI3N2ZkYjA0ZDYifQ.FnJMR3crPoD0-lQRXnp
    POJ8TCsRG9Wr-DaddnAIj6ko'  --data-binary '{"pool_ids":["ed7d8830-c703-4ac0-825
    2-77e0f272a677"]}' --compressed --insecure

A successful ASSIGNMENT returns an HTTP 200 OK.

Removing an IP pool from a Blueprint
------------------------------------

To remove IP pools from the blueprint PUT an empty ``pool_id``
list to the blueprint with the payload ``[]``:

PUT to the HTTP endpoint https://aos-server/api/blueprints/<blueprint_id>/
resource_groups/ip/<allocation_group_name>

With the payload:

.. code-block:: json

    { "pool_ids": [] }

CURL Example

.. code-block:: text

    curl
    'https://192.168.25.250/api/blueprints/4c1e69c6-97bd-4c99-9504-7818f138b17f/resource_groups/ip/spine_loopback_ips'
    -X PUT -H 'AuthToken: eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZ
    SI6ImFkbWluIiwiY3JlYXRlZF9hdCI6IjIwMTctMDUtMzFUMDA6MjI6MDcuNTIwMTgzWiIsI
    nNlc3Npb24iOiJjOTliOGVlOS05Y2NjLTRjZTAtYTY5NS0wODI3N2ZkYjA0ZDYifQ.FnJMR3cr
    PoD0-lQRXnpPOJ8TCsRG9Wr-DaddnAIj6ko'  --data-binary '{"pool_ids":[]}'
    --compressed --insecure

A successful REMOVAL returns an empty response: ``{}``

Listing the IP Pools Assigned to a Blueprint
--------------------------------------------

.. code-block:: text

    curl
    'https://192.168.25.250/api/blueprints/4c1e69c6-97bd-4c99-9504-7818f138b17f/resource_groups'
    -H 'AuthToken: eyJhbGciOiJIUzI1NwMTctMDUtMzFUMDA6MjI6MDcuNTIwMTgzWiIsInNlc3
    Npb24iOiJjOTliOGVlOS05Y2NjLTRjZTAtYTY5NS0wODI3N2ZkYjA0ZDYifQ.FnJMR3crPoD
    0-lQRXnpPOJ8TCsRG9Wr-DaddnAIj6ko' --compressed --insecure | python -m json.tool

.. code-block:: json

    {
        "items": [
            {
                "name": "leaf_asns",
                "pool_ids": [
                    "c23ea447-8f37-419a-9b1c-c48cc55d5b9c"
                ],
                "type": "asn"
            },
            {
                "name": "spine_asns",
                "pool_ids": [
                    "c23ea447-8f37-419a-9b1c-c48cc55d5b9c"
                ],
                "type": "asn"
            },
            {
                "name": "leaf_loopback_ips",
                "pool_ids": [
                    "56e8e0dc-babd-4652-92a5-fc37294a7b26"
                ],
                "type": "ip"
            },
            {
                "name": "mlag_domain_svi_subnets",
                "pool_ids": [
                    "ed7d8830-c703-4ac0-8252-77e0f272a677"
                ],
                "type": "ip"
            },
            {
                "name": "spine_leaf_link_ips",
                "pool_ids": [
                    "ed7d8830-c703-4ac0-8252-77e0f272a677"
                ],
                "type": "ip"
            },
            {
                "name": "spine_loopback_ips",
                "pool_ids": [
                    "56e8e0dc-babd-4652-92a5-fc37294a7b26"
                ],
                "type": "ip"
            },
            {
                "name": "to_external_router_link_ips",
                "pool_ids": [
                    "aae25699-9ad6-4382-8a7b-3f4a8ef582ac"
                ],
                "type": "ip"
            }
        ]
    }
