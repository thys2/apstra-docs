==========
Rack Types
==========

.. notes for 4.0 updates
   * You can now specify port channel ID for anything, not just for servers.
   * You don't have to specify any servers (generic device) in the rack, so you
     can build a rack w/leafs only or access switches only. Previously you needed
     at least one server.
   * Now, instead of specifying the server or ext rtr in the logical device, you
     specify it with tags in the rack type.
   * (not specific to generic devices) - Add capability of specifying MLAG peer
     link VLAN IDs during rack type creation "Peer Link VLAN ID". Before 4.0 you
     could do this with AOS CLI but not with UI.

Rack Type Overview
==================

.. sidebar:: Rack Types in Blueprints

   Racks in running blueprints can be added, deleted, also can be edited (as of AOS
   version 3.2). Editing Racks can be done either by changing the Rack Types
   manually, or by modifying the Blueprints directly, e.g. Changing Link Speeds,
   Adding Servers, Adding & Removing Links, etc. which is so-called "Flexible Fabric
   Expansion (FFE)".
   Rack Types in Blueprints have timestamps. FFE operations will modify the embedded
   Rack Types then update the timestamps. Embedded Rack Types can be exported into
   the global catalog.
   FFE provides complete flexibility in fabric design and the day-2 operations.

Rack types in AOS define the type and number of leafs and servers to be used in a
rack build. They use :doc:`logical devices <logical_devices>` (abstractions of
physical devices that specify port roles and speeds) so you can design your racks
before selecting a specific vendor's device. Rack types are then used when
creating :doc:`templates <templates>`.

The AOS global catalog (in the **Design** menu) includes numerous predefined rack
types based on common rack designs. You can create your own or clone, edit, and
delete existing ones as described in the sections below.

Rack types include the following details:

Summary
   Name
      17 characters or fewer, and (optional) description

   Connectivity Type
      L2 - Connects to servers via VLAN or VXLAN.

      L3 - IP addresses assigned to switch ports, IP address and BGP routing
      enabled via AOS server agent for servers

   IP Version (for L3 connectivity type)
      IPv4, IPv6, or IPv4-IPv6

Leafs
   Leaf Name
      64 characters or fewer

   Leaf Logical Device
      Used as ToR leaf switch network device(s)

   Links per spine (and speed)
      Number of leaf-spine links and their speed

   Redundancy Protocol

      None
         For single-homed server connections

      MLAG
         For dual-homed server connections. Both switches use the same logical
         device

         Peer Links (and speed)
            Number of links between the MLAG devices, and their speed

         Peer Link Port Channel ID

         L3 peer links (and speed)
            Applies when connectivity type is L3.
            Used mainly for BGP peering between border MLAG leafs in non-default
            security zones. Mainly used for routed L3 traffic to solve EVPN
            blackhole issues or if upstream routers go down. L3 peer-links act as
            backup paths for the north-south traffic. Other than border leaf it
            can be used on any other ToR leafs as well for avoiding blackholing
            traffic for a VRF.

         L3 Peer Link Port Channel ID

      ESI - Junos ONLY (New in AOS version 3.3.0)
         Ethernet Segment ID assigned to the bundled links. Specifying device
         platforms other then Juniper Junos (such as Cisco, Cumulus, Arista) will
         result in blueprint build errors. See
         :doc:`Juniper EVPN Support <junos_evpn>` for information about Juniper
         ESI support and :ref:`ESI MAC MSB blueprint settings <esi_mac>` for more
         information about ESI.

      .. important::

        When designing racks, make sure the intended platform supports the chosen
        redundancy protocol. For example, SONiC does not support L3 MLAG peers,
        and ESI is only supported on JunOS.

   External Facing
      When enabled, connectivity is configured from the leaf to the external
      router. In an MLAG pair, external connectivity is applied to both leafs. For
      example, 2 x external links with MLAG enabled create four external links.

Access Switches - Junos ONLY (New in AOS version 3.3.0)
   Limited support. See :doc:`Adding Access Layer Switches <access_switch>` for
   more information.

Servers
   Name
      64 characters or fewer

   Server Count
      Number of servers in the set

   Port Channel ID Min and Max
      AOS uses port channel IDs when rendering leaf device port-channel
      configuration towards servers. default: 1-4096. This field can be customized
      (as of AOS version 3.3.0).

   Logical Device
      The server network device

   Link
      Name
         64 characters or fewer

      Switch
         Leaf

      LAG Mode (for L2 connectivity type)
         LACP (Active)

         LACP (Passive)

         Static LAG (no LACP)

         No LAG

     Physical link count per leaf (and link speed)
        Number of links from each server to each leaf and their speed. If using
        dual leaf switches, this number should be half of the total links attached
        to the server.

From the AOS web interface, navigate to **Design / Rack Types**.

.. image:: static/images/racks/rack_type_330.png

.. sidebar:: Cloning Logical Device

   Instead of entering all details for a new logical device, you can clone an
   existing one, give it a new name and customize it.

.. _creating_a_rack_type:

Creating Rack Type
==================

#. From the AOS web interface, navigate to **Design / Rack Type**, then click
   **Create Rack Type**.
#. Enter a name, (optional) description, then select a connectivity type
   (and IP version if L3 connectivity).
#. Configure the panel as required for your design.

   * See rack type overview above for details and the example below for a specific
     use case.
   * To add a **Logical link**, click **Add New Link**.
   * To add an additional Server Group, click **Add New Server Group**
     and enter/select details.
   * To clone or delete a Logical Link or Server Group within a Rack Type,
     click the **Clone** icon or **Delete** icon (top-right of section).


Creating Rack Type - Example
----------------------------
This example shows how to create a rack type for a dual-connected L2 rack with
two AOS-48x10+6x100-1 logical device leaf switches, each with 4-100 GbE spine
links and forty-eight dual-connected 10 GbE L2 servers.

#. From the list view (Design / Rack Types) click **Create Rack Type**, enter a
   name (**MyFirstRackType** in this example), then select **L2** connectivity
   type.
#. In the **Leafs** section, enter a name (**MyLeaf1** in this example),
   select **AOS-48x10+6x100-1** from the **Leaf Logical Device** drop-down list,
   then change the **Links per spine** to **2**. Notice the **Topology** preview
   on the right side shows the first leaf.

   .. image:: static/images/racks/rack_type_create_ex1_330.png

#. Click **Add New Leaf** and enter a name for the second leaf (**MyLeaf2** in
   this example), select **AOS-48x10+6x100-1** from the **Leaf Logical Device**
   drop-down list, then change the **Links per spine** to **2**. Notice the
   **Topology** preview on the right side now shows both leafs.
#. Click **Servers**, click **Add new server group** and enter a name
   (**MyServerGroup1** for this example), change the **Server Count** to **20**,
   then select **AOS-2x10-1** from the **Logical Device** drop-down list. Notice
   that the **Topology** preview changes as you configure the rack type.

   .. image:: static/images/racks/rack_type_create_ex2_330.png

#. Click **Add link**, enter a name (**MyLogicalLink1** in this example), select
   **MyLeaf1** from the **Switch** drop-down list, select **LACP (Active)** for
   **LAG Mode**, then change **Physical link count per leaf** to **2**.
#. Click **Add new server group**, and enter a name (**MyServerGroup2** for this
   example), change the **Server Count** to **20**, then select **AOS-2x10-1**
   from the **Logical Device** drop-down list.
#. Click **Add link**, enter a name (**MyLogicalLink2** in this example), select
   **MyLeaf2** from the **Switch** drop-down list, select **LACP (Active)** for
   **LAG Mode** then change **Physical link count per leaf** to **2**.
#. If you'd like to see a preview of the logical devices that you've configured in
   the rack type, click **Logical Devices** in the **Preview** section.

   .. image:: static/images/racks/rack_type_create_ex3_330.png

#. Click **Create** to create the rack type in the global catalog and return to
   the list view. The details view is shown below:

   .. image:: static/images/racks/rack_type_ex_330.png

Editing Rack Type
=================
#. To edit the rack type in the global catalog, either from the list view
   (Design / Rack Type) or the details view, click the **Edit** button for the
   rack type to edit.
#. Make your changes.
#. Click **Update** (bottom-right) to update the rack type in the global catalog
   and return to the list view.

Editing Rack types in Templates
-------------------------------
Changes to rack types in the global catalog do not affect rack types that have
already been embedded into templates. If the intent is for a template to use a
modified rack type, then after editing the rack type it must be
:ref:`updated in the template <update_rack_type_template>`.

Editing Rack types in Blueprints
--------------------------------
Rack types that are being used in a running blueprint, can be edited
(as of AOS version 3.2.0). See :ref:`Editing Rack <edit_rack>` in the staged
blueprint section for more information.

Deleting Rack Type
==================
#. Either from the list view (Design / Rack Type) or the details view,
   click the **Delete** button for the rack type to delete.
#. Click **Delete** to delete the rack type and return to the list view.
