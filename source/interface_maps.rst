==============
Interface Maps
==============

Interface Map Overview
======================
Interface maps consist of interfaces used for achieving the intended network
configuration rendering. They map interfaces between logical devices and physical
hardware devices (represented with device profiles) while adhering to vendor
specifications.

Some characteristics and capabilities of interface maps include:

* Precisely select device ports, transformations and interfaces.

* You are not restricted to selecting interfaces in a contiguous manner.

* Provision QSFP+ breakout ports to transform ports, such 40GbE ports to 10GbE,
  100GbE ports to 25GbE, and so on.

* Port breakouts and available speeds affect possible values of the mapping fields.

* The logical device enables you to plan port and panel mappings accordingly. For
  example, you can assign a network policy that ensures that spine uplink ports on
  a leaf switch are always the furthest right ports on a panel.

* If a smaller logical device is mapped to a larger physical device, the unmapped
  ports in the device profile are marked as **Unused** in the interface map.

.. image:: static/images/interface_maps/interface_map_330.png

**To access interface maps** - from the AOS web interface, navigate to
**Design / Interface Maps**. AOS ships with many predefined interface maps.

.. _use_case_final:

.. image:: static/images/interface_maps/interface_map_detail_330.png

**To see details** - click an interface map name. Click a port to see its
interface details. Interface maps include the following details:

.. _interface_map_parameters:

Logical Device
    Abstraction of the physical device.

Device Profile
    Physical device characteristics.

Interfaces
    Mapping between logical devices and physical devices (device profile)

.. sidebar:: Cloning Interface Map

   Instead of entering all details for a new interface map, you can clone an
   existing one, give it a unique name and customize it.

Creating Interface Map
======================
#. From the list view (Design / Interface Maps), click **Create Interface Map**,
   then enter a name (64 characters or fewer). This field can be left blank for
   the name to be created for you that consists of the concatenation of the names
   of the selected logical device and device profile.
#. Select a logical device from the drop-down list. If you don't see a logical
   device that fits your requirements, you can :doc:`create <logical_devices>`
   one.
#. Select a device profile from the drop-down list. If you don't see a device
   profile that fits your requirements, you can :doc:`create <device_profile>` one.
#. Map the logical device to the device profile. See example below for details.
#. Click **Create** to create the interface map and return to the list view.

Creating Interface Map - Example: Breakout Ports
------------------------------------------------
To create dense server connectivity, let's create an interface map that breaks out
the twenty-four 40 GbE transformable ports of an **Arista DCS-7050QX-32** physical
device to ninety-six 10 GbE ports of an **AOS-96x10-8x40-2** logical device.

1. From the list view (Design / Interface Maps), click **Create Interface Map**,
   and leave the name blank for AOS to name it for you.

.. sidebar:: AOS 96x10 8x40

   **AOS-96x10-8x40-2** is not one of the predefined logical devices
   that ships with AOS, so if you have not created it you will not find
   it in the drop-down list. If you'd like to follow along with this example,
   you can create the :ref:`logical device <logical_device_use_case>` before
   continuing.

2. From the **Logical Device** drop-down list, select **AOS-96x10-8x40-2**. This
   logical device has 96-10 GbE ports for servers and 8-40 GbE ports for uplinks
   to spine switches or external routers.

3. From the **Device Profile** drop-down list, select **Arista DCS-7050QX-32**.
   This device has 24-40 GbE QSFP+ ports that are transformable (4x10 GbE or 1x40
   GbE) and 8-40 GbE QSFP+ ports that are not transformable. As soon as both the
   logical device and device profile are selected, the interface map name is
   automatically populated.

4. Under **Device profile interfaces** (middle-right), click **Select Interfaces**
   for the 10 GbE logical ports to see the port layout.

   .. image:: static/images/interface_maps/interface_map_create1_330.png

5. Drag to select the first 24 ports. As the ports are selected the white numbers
   turn gray. When all interfaces are selected the red circle turns green.
6. Under **Device profile interfaces** (middle-right), click **Select Interfaces**
   for the 40 GbE ports to see the port layout.

   .. image:: static/images/interface_maps/interface_map_create2_330.png

7. Drag to select the remaining 8 ports. As the ports are selected the white
   numbers turn gray. When all interfaces are selected the red circle turns green.

   .. image:: static/images/interface_maps/interface_map_create3_330.png

8. Click **Create** to create the interface map. The finalized interface map can
   be seen in the interface map overview :ref:`above <use_case_final>`.

.. _im_4_disabled-dp-ports:

Use Case: Inter Port Constraints - Disabled Ports
-------------------------------------------------

Inter Port Constraint Overview
______________________________
Inter port constraints for Cumulus devices are handled in both the
:ref:`device profile <cumulus_device_profile:Inter port constraints>`
and the interface map. For AOS to generate the correct ports.conf file with these
constraints, the unused interfaces must be disabled in the interface map.

For example, if each of the top (odd-numbered) QSFP28 ports in a **Mellanox 2700**
device are split into four SFP28 ports, the bottom (even-numbered) QSFP28 ports
are blocked.
(Source: https://docs.mellanox.com/display/sn2000pub/Cable+Installation) The
blocked interfaces must be disabled.

.. image:: static/images/device_profiles/mellanox_msn2700_port_constraints.png

Using the predefined interface map **Mellanox_MSN2700_Cumulus__AOS-48x10_8x100-1**
as an example, ports 1,3,5,7,9,11,13,15,17,19,21, and 23 were used to
generate the 4x10G interfaces, and the 5th transformation for ports
2,4,6,8,10,12,14,16,18,20,22, and 24 have been disabled.

   .. image:: static/images/interface_maps/interface_map_constraint1_330.png

Disabling Unused Ports
______________________
When creating an interface map that requires disabling ports for
inter port constraints, AOS will flag you with the prompt
**Do you want to select the disabled interfaces for unused device profile ports?**
Click **OK** and AOS will automatically set the corresponding ports to disabled.

   .. image:: static/images/interface_maps/interface_map_constraint2_330.png

Editing Interface Map
=====================
Changes to interface maps in the global catalog do not affect interface maps that
have already been imported into blueprint catalogs, thereby preventing potentially
unintended changes to blueprints.

.. important::
  Any changes made to predefined interface maps (the ones that ship with AOS)
  are discarded when AOS is upgraded. To retain a customized interface map
  through AOS upgrades, clone the predefined interface map, give it a unique
  name, and customize it instead of changing the predefined one directly.

#. Either from the list view (Design / Interface Maps) or the details view,
   click the **Edit** button for the interface map to edit.
#. Make your changes.
#. Click **Update** (bottom-right) to update the interface map and return to the
   list view.

Deleting Interface Map
=======================
#. Either from the list view (Design / Interface Maps) or the details view,
   click the **Delete** button for the interface map to delete.
#. Click **Delete Interface Map** to delete it from the global catalog and return
   to the list view.
