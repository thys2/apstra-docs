==================================
Processor: Generic Graph Collector
==================================
The Generic Graph Collector processor imports data from the graph into the output
stage, depending on the configuration (a graph query).

'graph_query' and 'additional properties' behave as in other source processors.
Importantly, the expression in the 'value' field yields a value per each item.
Thus, unique to this source processor, values come from the graph rather than
from device telemetry.

**Input Types** - No inputs. This is a source processor.

**Output Types** - Discrete-State-Set (DSS), Number-Set (NS),
TS (based on data_type)

**Properties**

   .. include:: includes/processors/data_type.rst

   .. include:: includes/processors/graph_query.rst

   .. include:: includes/processors/value_map.rst

   Value (value)
     Expression evaluated per query result to collect value. (integer for NS and
     string for TS/DSS)

   .. include:: includes/processors/additional_keys.rst

   .. include:: includes/processors/enable_streaming.rst

Generic Graph Collector Example
-------------------------------

.. code-block:: none

      graph_query: "node("system", role="leaf", name="system").
                    out("hosted_interfaces").
                    node("interface", name="iface").out("link").
                    node("link", role="spine_leaf")"
      system_id: "system.system_id"
      interface: "iface.if_name"
      value: "iface.if_type"
      data_type: "dss"
      value_map: {0: "ip", 1: "loopback", ...}

Sample output (DSS):

.. code-block:: none

      [system_id=leaf1,interface=eth0]: "ip"
      [system_id=leaf1,interface=eth1]: "ip"
