=============
Device Guides
=============

.. toctree::
   :maxdepth: 1

   configuration_lifecycle
   device_add
   device_deploy
   device_drain
   device_decomm
   device_aaa
