====
Pods
====

.. image:: static/images/blueprints/active/physical/active_pods_330.png

From the blueprint, navigate to **Active / Physical / Pods** to see details about
the active pods in a 5-stage Clos network. (The pod view only applies to
pod-based blueprints.) You can search for specific nodes or links and select a
layer to see anomalies, deploy modes, deployment status and more. Click a rack
name in a pod to see its rack type preview.
