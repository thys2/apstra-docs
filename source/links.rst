=====
Links
=====

.. note::
  This page describes tasks that can be performed from the **Links** view of the
  staged blueprint. You can perform additional links-related tasks, such as
  adding, editing and deleting links, from the
  :doc:`Topology <staged_topology>` view of the staged blueprint.

When you've built the blueprint, review the AOS-calculated cabling map and cable
up your devices according to the map. If you don't want to use the prescribed
cabling from AOS you can have AOS discover existing cabling in the network.
You can also change link speeds of existing links.

.. image:: static/images/blueprints/staged/physical/links/staged_links_overview_330.png

**Links Example**

In this example, each link is assigned
a unique /31 subnet from the IP Pool, the smaller /31 IP is assigned to the
spine interface, the larger /31 IP is assigned to the leaf interface. Subnets
are assigned in increasing order in a spine-major order, that is, the links
between spine1 and all leafs (in ascending order) are assigned subnets first,
followed by links between spine2 and all leafs, and so on.

   .. image:: static/images/ip/bp_ip_link_assign6_310.png

Importing Cabling Map
=====================

#. From the links view (Staged / Physical / Links) click the
   **Import cabling map** button to see the dialog for importing a cabling map.
#. Either click **Choose File** and navigate to the file on your computer, or drag
   and drop the file onto the dialog window.
#. Click **Import**.

Exporting Cabling Map
=====================
Datacenter technicians may find a printed cabling map useful when wiring in
switches, or remote network operators may find it useful for viewing IP
assignments. It's available in CSV or JSON format, and you can copy the contents
or download the file to your local computer.

#. From the links view (Staged / Physical / Links) click the
   **Export cabling map** button and select **JSON** or **CSV**.
#. Click **Copy** to copy the contents or click **Save As File** to download the
   file.
#. When you've copied or downloaded the cabling map, close the dialog to return
   to the **Links** view.

Cabling maps can also be exported from the **Active / Physical / Links** view.

Editing Cabling Map
===================
Some of the applications where you might want to override existing cabling include:

* having AOS use existing network cabling to avoid recabling
* changing interface names or IP addresses in the existing network cabling map
* specifying a different port from the one that the AOS cabling algorithm selected
* avoiding the use of a defective interface

.. warning::
  Overriding AOS-generated cabling can be disruptive to the network.
  Use with extreme caution. Please contact :doc:`Apstra Support <support>`
  for assistance with production networks.

Overriding Cabling Using Web Interface
--------------------------------------
Device Profiles must already be assigned to Blueprint nodes.

.. sidebar:: Drop the Override

   To drop the override for either an interface name or IPv4/IPv6 address,
   submit an empty value in the corresponding field.

#. From the links view (Staged / Physical / Links) click the
   **Edit cabling map** button to see the dialog for editing a cabling map.
#. Change interface names and/or IP addresses, as applicable.
#. **Batch clear override** can be used to clear all Interface and IPv4/IPv6
   values for a specific device type.

   .. image:: static/images/blueprints/321_cabling-override1.png

#. Click **Update** to stage the changes.
#. Click **Uncommitted** to see the diffs between **Staged** and **Active**.
#. Click the **Commit** button to save the changes to the **Active** Blueprint.

Overriding Cabling Using JSON
-----------------------------
To change the cabling map with a JSON file, you'll export the JSON file, edit the
file, then import it back into AOS.

#. From the links view (Staged / Physical / Links) click the
   **Export cabling map** button to see the dialog for exporting a cabling map.
#. Select **JSON** and click **Save As File** to download the file.
#. Change interface names (if_name) and/or IP addresses (ipv4_addr or ipv6_addr)
   in the file, as applicable. Do not change any other fields. If you
   do, the changes will be ignored or they will result in an error message.
#. From the cabling map (**Staged / Physical / Links**) click the
   **Import cabling map** button to see the dialog for importing a cabling map.
#. Either click **Choose File** and navigate to the revised file on your computer,
   or drag and drop the file onto the dialog window.
#. Click **Import**.
#. Click **Uncommitted** to see the diffs between **Staged** and **Active**.
#. Click the **Commit** button to save the changes to the **Active** Blueprint,
   or click the **Revert** button to discard changes and return to the previous
   cabling map.

Changing Link Speeds
====================

.. sidebar:: Alternate Method

   You can also change link speeds from the **Topology** view.

From the **Links** view of the **Staged** blueprint, you can change link speeds
on leaf-server, external routers and MLAG peer links (as of AOS 3.2).
To change link speeds on spine-leaf and superspine-spine you must
:doc:`change the Rack <racks>`.

#. From the links view (Staged / Physical / Links) click the
   **Change link speeds** button to see the dialog for changing link speeds.
#. To search for specific links, click the query box, enter search criteria and
   click **Apply** to see results.
#. From the **Speed** drop-down list corresponding to the link to be changed,
   select the new speed.
#. Click **Update** to update the link and return to the cabling map.

Fetching Discovered LLDP Data
=============================
Before AOS can discover existing cabling, all system nodes in the Blueprint must
have system IDs assigned to them.

.. warning::

   This is a disruptive operation. All links can potentially be renumbered.

#. From the links view (Staged / Physical / Links) click the
   **Fetch discovered LLDP data**.
#. If staged data is *identical* to LLDP discovery results, you will see a message
   with that statement. Your actual cabling matches the AOS cabling map.
   No further action is needed.
#. If staged data is *different* from LLDP discovery results, the message includes
   the number of links that are different.
#. Scroll to see details of the diffs (in red), or check the
   **Show only links with LLDP diff?** checkbox to see only the differences.
#. To accept the changes and update the map to match LLDP data, click
   **Update Stated Cabling Map from LLDP**. You might also need to
   :ref:`reset_resource_group_overrides`.

You can also discover cabling using :ref:`AOS CLI <cli_cabling_discovery>`.

.. _change_link_name:

Changing Link Name
==================
If you have :ref:`changes server names and/or hostnames <chg_name>` for switches,
any associated link names do not automatically update to match. This may cause
confusion when reviewing an updated cabling map in the **Uncommitted** tab.
New in AOS version 3.3.0, you can change link names to match your other name
changes.

#. From the blueprint, navigate to **Staged / Physical / Links**, then click the
   name of the link to change.
#. In **Properties** (right panel) click the **Edit** button for the link name.
#. Change the name and click the **Save** button to stage the change.
