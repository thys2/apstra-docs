====================
|active_icon| Active
====================

.. |active_icon| image:: static/images/icons/active_icon.png

The details of a deployed network are shown in the active view. From here,
you can view telemetry status in a topology-centric view. Alerts and anomalies
can be filtered by different layers to conduct root cause analysis of any
problems.

.. toctree::
   :maxdepth: 2

   active_physical
   blueprint_active_query
   active_anomalies
   rci
