==========================
VMware NSX-T Integration
==========================

Overview
========
.. versionadded:: 3.1

AOS NSX-T integration feature helps in deploying the necessary fabric VLANs
needed for deploying NSX-T in the Data Center or for providing connectivity
between NSX-T overlay networks and fabric underlay networks. It helps with
accelerating NSX-T deployments by making sure the fabric is ready in terms of
LAG, MTU and VLAN configuration as per NSX-T Transport Nodes requirements.
This feature also helps with fabric visibility for network operators
in terms of ability to see all the NSX-T VMs, VM ports, physical
gateway ports.


AOS NSX-T integration helps identify if an issue exists on the fabric or on the
virtual infrastructure. It eliminates manual config validation tasks
between the NSX-T Nodes side and the ToR switches.

.. important::

  NSX-T Edge integration is currently only supported on Bare Metal deployments.
  NSX-T Edge on a VM is not supported.

Supported Versions
--------------------

As of AOS version 3.3.0 VMware NSX-T integration is currently available
for the VMware NSX-T Data Center 2.5 version.

Enabling NSX Integration
============================

We recommend creating a dedicated NSX-T Manager user for AOS integration, with
the role “Auditor”.

1. From the AOS web interface, navigate to **External Systems / Virtual Infra
Managers**.

2. Click Create **Virtual Infra Managers** to see the dialog for creating a
**VMware NSX-T Manager**.

.. image:: static/images/nsx/NSX-T_Manager.png

3. Enter the **NSX-T Manager** IP address or DNS name, Username, and Password.


4. Click **Create** so that AOS connects to **NSX-T Manager**. While trying
   to connect, it is in a **DISCONNECTED** state.

5. When the **NSX-T Manager** successfully connects, the state changes to
   **CONNECTED**.

.. note::

    Once connected after providing correct credentials for **NSX-T Manager**
    disabling of the service is not allowed by default. Only option to
    disconnect from **NSX-T Manager** under **Virtual infra Managers** will
    be to use the **delete** option under **Actions**.

6. From the **Virtual tab** of the **Staged** view of your Blueprint, click
   **Virtual Infra**.

7. Select the **NSX-T Manager** to add one click Add **Virtual Infra**.A dialog
   appears for adding **Virtual Infra Manager** and specifying  **VLAN
   Remediation policy**. The configured remediation policy will be used when
   **Remediate anomalies** is clicked in the probes. The user will need to
   specify the **VN type** and **Security Zone**. Default VN type is
   **rack-local(i.e. VLAN)**  but it should be selected as
   **inter-rack(i.e. VXLAN)** if VXLAN Virtual Network extends to different ToRs
   in a fabric. Only if the remediation policy is set to
   **rack-local(i.e. VLAN) Security Zone** can be set as default one.
   Otherwise, a specific security zone under which VXLAN Virtual Networks exist
   needs to be  given under  Security Zone option.


.. image:: static/images/nsx/Remediation_Policy.png

.. image:: static/images/nsx/VN_Type.png

It shows successfully connected using VLAN Remediation policy as below.

.. image:: static/images/nsx/Virtual_Infra.png

8. Click **Uncommitted** (top) to see a comparison of staged values and active
   values.

9. Click **Commit** (top-right) to commit your changes and add the NSX-T
   Manager to your active Blueprint.


.. note::
  Adding an NSX-T infra to a Blueprint is not supported in case any existing
  NSX-T registered vCenter servers infra are already registered, and vice-versa.
  It is recommended to have only one Virtual Infra per BP.

Virtual Infrastructure Visibility
==================================

NSX-T and AOS integration helps with visualization of Virtual Infrastructure in
terms of visibility of NSX-T VMs and Transport nodes.

Once you’ve successfully added NSX-T you can query about the status of the
fabric health for VMware. List of the VMs connected to the hypervisor
underneath can be viewed by navigating to dashboard and scrolling to fabric
health for VMware option.

.. image:: static/images/nsx/Fabric_Health.png

When you have successfully added NSX-T to your active Blueprint, you can query
VMs hosted on Hypervisors connected to TOR leafs.

From the **Active** view of the Blueprint, click **Query** and then click
**VMs**. You will see the list of VMs that will be queried.

.. image:: static/images/nsx/VMs_Query.png

VM Name
     The Virtual Machine name which is hosted on NSX managed hypervisor.

Hosted On
    The ESXi host on which Virtual Machine is hosted.

Hypervisor Hostname
    The hypervisor hostname on which Virtual Machine is hosted and
    is connected to the leaf TORs in a fabric.

Hypervisor Version
    The software version of OS running on the hypervisor.

VM IP
    The IP address as reported by NSX-T after the installation of VM tools.
    If the IP address is not available this field is empty. AOS displays VM IP
    if the IP address is available on installation VM tools on the VM.

Leaf:Interface
    System ID for the interface on the leaf to which ESXi host is connected
    and on which VM resides.

Port Group Name:VLAN ID
    The VLAN ID which NSX-T port groups are using. Overlay VM
    to VM traffic in a NSX-T enabled Data Center tunnels between
    transport nodes over this Virtual network.

MAC Addresses
    MAC address of the VM connected to the AOS Fabric.

Virtual Infra address
   IP address of the NSX-T infra added to a Blueprint


Which nodes in a physical topology have VMs connected can also be searched.
It can be queried in the **Active** view of the Blueprint by using the
**“Has VMs“?** option under **Nodes** in **Physical>Topology** tab.

    .. image:: static/images/nsx/Has_VMs.png

Validating Virtual Infra Integration
====================================

.. include:: includes/virtual_infra_validation.rst

Disabling Virtual Infra Integration
===================================

.. include:: includes/integration_disable.rst
