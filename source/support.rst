=================
Technical Support
=================

Apstra Global Support is available for all Apstra Customers with a valid and current
license for any Apstra Software such as AOS. This includes customers who have
purchased a license directly from Apstra or via an Apstra Partner or Reseller.
This also includes customers who have obtained an evaluation license. If a
customer's purchased or evaluation license is expired, Apstra Global Support may not
be able to offer support and will refer the customer to their appropriate Apstra
Sales Team to purchase a current license.

The Apstra Global Support Group is available 24x7x365 and can be contacted via
Portal, E-Mail or Phone:

**Portal:**
  https://portal.apstra.com
  (registered, individual users only)

  Any Severity 1 or 2 issue submitted via the Portal must be escalated to Apstra
  Support via phone.

**E-Mail:**
  support@apstra.com

  E-Mails are only accepted from EMail domains (e.g. company.com) for registered
  Companies. The sender should receive an auto-response EMail within 5 minutes. If
  an auto-response is not received, please contact Apstra Global Support via phone.
  Any Severity 1 or 2 issue submitted via EMail must be escalated to Apstra Global
  Support via phone.

**Phone:**
  **US/Toll-Free:**
    +1-844-9APSTRA (+1-844-927-7872) option 1

  **US/Worldwide:**
    +1-650-285-2390 option 1

  **United Kingdom:**
    +44-20-8089-2594 option 1

  **Japan:**
    +81-3-4540-6144 option 1

Apstra Global Support Service Level Agreements
==============================================

To properly manage Apstra Customer issues, the Customer will assign a Severity
Level to their issue so Apstra Global Support can effectively prioritize and address
this issue. The Severity Level will correspond to a certain Service Level Agreement
(SLA) detailed below. As an issue is addressed, the Customer may change (up or down)
the Severity Level of an issue.

.. list-table:: Apstra Global Support Service Level Agreements
    :header-rows: 1
    :class: nowrap

    * - Severity Level
      - Description
      - Initial Response
      - Updates
    * - 1 - Critical
      - An issue causing critical impact to Customer Production Systems with no workaround available.
      - 30 minutes (24x7x365)
      - Every 1 hour (24x7x365)
    * - 2 - Major
      - An issue causing a major impact on Customer Production Systems.
      - 2 hours (24x7x365)
      - Every 4 hours (24x7x365)
    * - 3 - Minor
      - An issue causing partial or non-impacting loss to Customer Production Systems or a Critical or Major issue with a provided workaround.
      - 24 hours (24x5)
      - Every 24 hours (24x5)
    * - 4 - General
      - General issues not requiring a higher severity level.
      - 48 hours (24x5)
      - Every 120 hours (24x5)

24x5 Support is currently between 0100 GMT Monday to 0100 GMT Saturday, excluding
the following holidays: New Years Day, Christmas.

An Apstra Global Support Agent will respond to the customer within the 'Initial
Response' time corresponding to the Severity Level assigned by the Customer. Apstra
Global Support will work with the customer to triage the issue and confirm the
severity and risk level as applicable. While the issue is open and Apstra Global
Support is not waiting for a response from the customer, Apstra Global Support will
provide periodic status updates within the "Updates" interval.

Apstra Global Support will escalate the issue internally per our internal SLA. If
the case has not been resolved or mitigated, Apstra Global Support will escalate
the issue to our Engineering Group, then to our Executive Team who will ensure that
appropriate resources are being allocated to bring the Customer's issue to
resolution as quickly as possible.

.. list-table:: Apstra Global Support Escalation Agreements
    :header-rows: 1
    :class: nowrap

    * - Severity Level
      - Tier 3 (Engineering) Escalation
      - Tier 4 (Executive) Escalation
    * - 1 - Critical
      - 1 hour (24x7x365)
      - 4 hours (24x7x365)
    * - 2 - Major
      - 4 hours (24x7x365)
      - 8 hours (24x7x365)
    * - 3 - Minor
      - 24 hours (24x5)
      - 48 hours (24x5)
    * - 4 - General
      - 72 hours (24x5)
      - 120 hours (24x5)

Providing Technical Support Data
================================
To aid the support process, please provide Apstra Global Support with diagnostic
information from the AOS environment. Separate *show tech* files are needed from
the AOS server and from each of the affected AOS device agents. The show tech
files for the AOS server and on-box agents that are connected to AOS can be
obtained from the AOS web interface (as of version 3.1). The show tech files for
off-box agents (and the AOS Server and on-box agents running versions earlier than
3.1) must be obtained from the AOS controller Linux CLI. See procedures below.

.. _upload_show_tech:

The files will most likely be too large to attach to a support ticket or email.
Registered, licensed customers can upload the files to the Apstra Support portal.
From the link below, click **Upload**, then select and upload the show tech files.

https://portal.apstra.com/downloads

Show Tech for on-box Agents and AOS Server (Web Interface)
----------------------------------------------------------
As of version 3.1 you can use the AOS web interface to collect show tech files for
the AOS server (controller) and on-box agents for Cumulus Linux, Arista EOS, and
Cisco NX-OS that are connected to AOS.

#. If an AOS controller Linux CLI username and password have not been configured,
   they must be configured now, before collecting show tech files.

   #. From the AOS web interface, navigate to **Platform / AOS Cluster** to see
      the AOS VMs list view.
   #. Click the IP address of the AOS controller.
   #. Click the **Edit** button (top-right) to see the dialog for editing AOS VMs.
   #. Enter the AOS controller Linux CLI username and password.
   #. Click **Update** to complete the update.

#. From the AOS web interface, navigate to **Platform / Technical Support** to see
   the show tech files list view.
#. Click **Collect Show Tech** to see the dialog for selecting and collecting
   show tech files.
#. If the user wants to collect a Show Tech from the AOS controller, leave
   **AOS Controller** selected.

   .. note::

     For AOS server controllers with large databases, the operation may timeout.
     If this happens, the user will need to collect the AOS server controllers
     Show Tech from the AOS server Linux CLI using the ``aos_show_tech``
     command.

#. Check the box for **Managed Devices** to see the list of connected managed
   devices (devices with installed agents that have been acknowledged).
#. Select up to twenty devices for show tech collection.

   .. note::

     AOS will use the configured Device System Agent username and password
     authentication to collect the device show tech. If the user has configured
     the device to use another authentication (AAA) method like RADIUS and TACACS
     do not have the same username and password, AOS will not be able to collect
     the show tech from the UI. The user will need to collect the AOS Device
     System Agent direct from the device AOS CLI ``aos_show_tech`` command.

#. Click **Collect** to start the collection process.
#. After the jobs are complete and marked **SUCCESS**, click the download button
   for each of the files (under **Logs**).

   .. image:: static/images/support/support6_310.png

#. When the show tech files have been downloaded, you may click the **Delete**
   button for each job to free up disk space.
#. :ref:`Upload show tech files <upload_show_tech>` via the Apstra Support portal
   from a computer with the ability to upload.


Show Tech for JunOS off-box Agents (CLI)
--------------------------------------------
Starting with AOS 3.3.0, the user can collect off-box agent and JunOS device
"Show Tech" information from the CLI using the
``aos_offbox_show_tech_collector`` command which can be run on the AOS server that
the JunOS off-box agent is running on.

This command requires the device management IP address(es) and a valid device SSH
username and password.

 .. code-block:: prompt
    :emphasize-lines: 1

    admin@aos-server:~$ aos_offbox_show_tech_collector --help
    usage: aos_offbox_show_tech_collector [--ips IPS [IPS ...]] [--user USER]
                                          [--password PASSWORD]
                                          [--output-dir OUTPUT_DIR]
    aos_offbox_show_tech_collector: error: unrecognized arguments: --help
    admin@aos-server:~$

#. Log into the AOS server that the JunOS off-box agent is running on via SSH.
#. Run the ``aos_offbox_show_tech_collector`` command with the IP(s) of the JunOS
   device(s), and a valid admin username and password.

   .. code-block:: prompt
      :caption: Generating aos_show_tech for JunOS off-box AOS Agent
      :emphasize-lines: 1

      admin@aos-server:~$ sudo aos_offbox_show_tech_collector --ips 172.20.202.6 --user admin --password admin-password
      2020-08-10 12:03:46,116 aos-offbox-172_20_202_6-f
      2020-08-10 12:03:46,384 collecting offbox container aos-offbox-172_20_202_6-f showtech
      2020-08-10 12:03:50,873 invoking DI container to collect device 172.20.202.6 show_tech
      2020-08-10 12:05:42,155 Done collecting device show_tech
      2020-08-10 12:05:42,204 AOS offbox show tech generated at /home/admin/aos_show_tech_20200810_120542_172_20_202_6.tar.gz
      admin@aos-server:~$

#. The AOS "Show Tech" file (e.g.
   ``aos_show_tech_20200810_120542_172_20_202_6.tar.gz``) will be in the user's
   directory. Note, the file will be owned by ``root`` and may need a permissions
   change in order to copy the file off of the AOS server.

   .. code-block:: prompt
      :emphasize-lines: 1,3,4

      admin@aos-server:~$ ls -l aos_show_tech_20200810_120542_172_20_202_6.tar.gz
      -rw------- 1 root root 238156 Aug 10 12:05 aos_show_tech_20200810_120542_172_20_202_6.tar.gz
      admin@aos-server:~$ sudo chmod a+r aos_show_tech_20200810_120542_172_20_202_6.tar.gz
      admin@aos-server:~$ ls -l aos_show_tech_20200810_120542_172_20_202_6.tar.gz
      -rw-r--r-- 1 root root 238156 Aug 10 12:05 aos_show_tech_20200810_120542_172_20_202_6.tar.gz
      admin@aos-server:~$

Show Tech for off-box Agents (CLI)
--------------------------------------
Show tech logs for installed off-box agents (all AOS versions) must be obtained
from the CLI. The AOS web interface does not support obtaining show tech files for
off-box agents.

#. Log into the AOS server via SSH.
#. From the AOS server, run the ``docker exec`` command as described below
   to generate the AOS show tech file.

   ``docker exec -ti aos-off-box-<ip_address>-t aos show tech``

   The Docker container name is *aos-off-box-* plus the IP address of the off-box
   device agent with the dots replaced with underscores followed by *-t*.

   .. code-block:: prompt
       :caption: Example: generate show tech file for off-box agent with IP address 172.20.47.6

       admin@aos-server:~$ docker exec -ti aos-off-box-172_20_47_6-t aos_show_tech
       AOS show tech generated at /tmp/aos_show_tech_20200401_181128.tar.gz
       admin@aos-server:~$

#. Using SCP, run the ``docker cp`` command as shown below to copy the AOS show
   tech file from the off-box agent Docker container to the ``/tmp`` directory of
   the AOS server.

   .. code-block:: prompt
       :caption: Example: copy show tech file from Docker container to the AOS Server

       admin@aos-server:~$ docker cp aos-off-box-172_20_47_6-t:/tmp/aos_show_tech_20200401_181128.tar.gz .
       admin@aos-server:~$ ls
       aos_show_tech_20200401_181128.tar.gz  docker.service.log
       admin@aos-server:~$

#. Locate the file archive in the ``/tmp`` directory and copy the file to a local
   computer with the ability to upload.
#. :ref:`Upload the files <upload_show_tech>` via the Apstra Support portal.

Show Tech for AOS Server (CLI)
----------------------------------
We recommend using the AOS web interface for obtaining AOS server show tech files,
but if you prefer, you have the option of using the AOS server Linux CLI.

#. Log into use the AOS server via SSH.
#. From the AOS server, run the ``sudo aos_show_tech`` command to generate and
   copy the AOS show tech file to the current working directory of the AOS server.

   .. code-block:: text
       :caption: Generating aos_show_tech for AOS server

       admin@aos-server:~$ sudo aos_show_tech
       [sudo] password for admin:
       Generating technical support data under directory /tmp/tmp.YmjuJDhatJ
       --- collecting sysinfo/cpuinfo from /proc/cpuinfo ---
       --- collecting network/etc_hosts from /etc/hosts ---
       --- collecting aos/aos.conf from /etc/aos/aos.conf ---
       --- collecting sysinfo/meminfo from /proc/meminfo ---
       --- collecting sysinfo/vmstat from /proc/vmstat ---
       --- collecting network/etc_hostname from /etc/hostname ---
       --- collecting network/interfaces_config from /etc/network/interfaces ---
       --- collecting network/resolv.conf from /etc/resolv.conf ---
       --- collecting logs/kern_log from /var/log/kern.log* ---
       --- collecting logs/syslog from /var/log/syslog* ---
       --- collecting filesystem/aos_cachaca_db_usage with command: du -a /var/lib/aos/cachaca ---
       --- collecting sysinfo/uptime with command: uptime ---
       --- collecting filesystem/aos_db_usage with command: du -a /var/lib/aos/db ---
       --- collecting filesystem/disk_free with command: df -h ---
       [snip]
       Remaining dump took 8.477 ms
       2020-04-01 03:35:39,010 131:INFO:aos.infra.core.entity_util:Create partition mount factory for partition Anomaly
       Dumping entity (anomaly_sysdb_dump/Tac) took 0.389 ms
       Dumping entity (anomaly_sysdb_dump/alert_aggregation) took 3.986 ms
       Dumping entity (anomaly_sysdb_dump/streaming) took 0.173 ms
       Dumping entity (anomaly_sysdb_dump/alerts) took 4.174 ms
       Dumping entity (anomaly_sysdb_dump/counters) took 0.160 ms
       Dumping entity (anomaly_sysdb_dump/telemetry_adaptor) took 0.156 ms
       Dumping entity (anomaly_sysdb_dump/deployment) took 0.214 ms
       Dumping entity (anomaly_sysdb_dump/device) took 0.675 ms
       Dumping entity (anomaly_sysdb_dump/cachaca) took 0.144 ms
       Dumping entity (anomaly_sysdb_dump/var) took 0.201 ms
       Skipping SysDB dump
       Archiving show tech data into aos_show_tech_20200401_033431.tar.gz
       Removing working directory /tmp/tmp.YmjuJDhatJ
       All done.
       admin@aos-server:~$

#. Locate the file archive in the ``/tmp`` directory
   (aos_show_tech_20200401_033431.tar.gz for example), and via SCP, copy the file
   to a local computer with the ability to upload.
#. :ref:`Upload the file <upload_show_tech>` via the Apstra Support portal.

Show Tech for Arista on-box Agents (CLI)
--------------------------------------------
We recommend using the AOS web interface for obtaining on-box agent show tech files,
but if you prefer, you have the option of using the AOS server Linux CLI.

#. SSH to the Arista device.
#. From the device, run the ``sudo aos_show_tech --platform eos`` command to
   generate and copy the AOS show tech file to the ``/tmp`` directory.

   .. code-block:: text
       :caption: Generating aos_show_tech for Arista AOS Agent

       l2-virtual-ext-003-leaf1#bash

       Arista Networks EOS shell

       [admin@l2-virtual-ext-003-leaf1 ~]$ sudo aos_show_tech --platform eos
       AOS show tech generated at /tmp/aos_show_tech_20200401_034102.tar.gz
       [admin@l2-virtual-ext-003-leaf1 ~]$

#. Locate the file archive in the ``/tmp`` directory
   (aos_show_tech_20200401_034102.tar.gz for example), and copy the file to a
   local computer with the ability to upload via SCP.
#. :ref:`Upload the file <upload_show_tech>` via the Apstra Support portal.

Show Tech for Cisco on-box Agents (CLI)
-------------------------------------------
We recommend using the AOS web interface for obtaining on-box agent show tech files,
but if you prefer, you have the option of using the AOS server Linux CLI.

#. SSH to the Cisco device.
#. From the device, run the ``sudo aos_show_tech --platform nxos`` command to
   generate and copy the AOS show tech file to the ``/tmp`` directory.

   .. code-block:: text
       :caption: Generating aos_show_tech for Cisco AOS Agent

       l2-virtual-ext-004-leaf1# guestshell
       [admin@guestshell ~]$ sudo aos_show_tech --platform nxos
       AOS show tech generated at /tmp/aos_show_tech_20200401_034529.tar.gz
       [admin@guestshell ~]$

#. Locate the file archive in the ``/tmp`` directory on the Cisco device
   (aos_show_tech_20200401_034529.tar.gz for example), and via SCP, copy the file
   to a local computer with the ability to upload.
#. :ref:`Upload the file <upload_show_tech>` via the Apstra Support portal.

Show Tech for Cumulus on-box Agents (CLI)
---------------------------------------------
We recommend using the AOS web interface for obtaining on-box agent show tech files,
but if you prefer, you have the option of using the AOS server Linux CLI.

#. SSH to the Cumulus device.
#. From the device, run the ``aos_show_tech --platform cumulus`` command to
   generate and copy the AOS show tech file to the ``/tmp`` directory.

   .. code-block:: text
       :caption: Generating aos_show_tech for Cumulus AOS Agent

       admin@l2-virtual-ext-001-leaf1:mgmt-vrf:~$ sudo aos_show_tech --platform cumulus
       AOS show tech generated at /tmp/aos_show_tech_20200401_034527.tar.gz
       admin@l2-virtual-ext-001-leaf1:mgmt-vrf:~$

#. Locate the file archive in the ``/tmp`` directory on the Cumulus device
   (aos_show_tech_20200401_034527.tar.gz for example), and via SCP, copy the file
   to a local computer with the ability to upload.
#. :ref:`Upload the files <upload_show_tech>` via the Apstra Support portal.

Show Tech for SONiC on-box Agents (CLI)
-------------------------------------------
We recommend using the AOS web interface for obtaining on-box agent show tech files,
but if you prefer, you have the option of using the AOS server Linux CLI.

#. SSH to the SONiC device.
#. From the device, run the ``aos_show_tech --platform sonic`` command to
   generate and copy the AOS show tech file to the ``/tmp`` directory.

   .. code-block:: text
       :caption: Generating aos_show_tech for SONiC AOS Agent

       admin@l2-virtual-ext-001-leaf1:mgmt-vrf:~$ sudo aos_show_tech --platform sonic
       AOS show tech generated at /tmp/aos_show_tech_20200401_034527.tar.gz
       admin@l2-virtual-ext-001-leaf1:mgmt-vrf:~$

#. Locate the file archive in the ``/tmp`` directory on the device
   (aos_show_tech_20200401_034527.tar.gz for example), and via SCP, copy the file
   to a local computer with the ability to upload.
#. :ref:`Upload the files <upload_show_tech>` via the Apstra Support portal.
