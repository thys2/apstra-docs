============
5-stage Clos
============

As of version 3.1.0 AOS supports 5-stage Clos topologies. 5-Stage Clos
architectures allow for very large scale topologies, bringing multiple PODs together
into a single fabric through the use of an additional aggregation layer.

* **Superspines** provide an additional aggregation layer that interconnect multiple
  PODs.
* **Planes** are groups of Superspines. Each 5-stage topology can consist of one or
  more Planes, each consisting of one or more Superspines.

Creating a 5-stage topology in AOS is similar to the typical 3-stage topology,
however there are several additional factors to take into account, which are
described further in this document.

Note careful planning and consideration is required to build large 5-stage Clos
networks using AOS. Apstra can offer services to help with validation and design.

Creating 5-stage Clos Network
=============================

To create a 5-stage network a POD-based Template is needed. In essence, this is a
"Template of templates", where one or more rack-based templates are combined into
a larger topology. Use the following steps to create a 5-stage network:

#. Logical Devices

    * As per standard AOS practice, make sure appropriate
      Logical Devices (LD) exist. For 5-stage topologies you need an LD with role
      *Superspine*. Also make sure you have sufficient ports for your Superspine to
      Spine connections (The exact number depends on your design):

      .. image:: static/images/5stage/ss_ld.png

    * For the Spine devices you need an LD with roles **Superspine** and
      **Leaf**. Again, make sure you have sufficient ports for the roles:

      .. image:: static/images/5stage/spine_ld.png

    * Refer to :doc:`Logical Devices <logical_devices>` for more details on LD
      creation.

#. Create Interface maps

    * Next we need to make sure we have the correct **Logical Device** to **Device
      Profile** mapping in place. We need to create two at minimum (the exact
      number depends on your design), one to map the new **Superspine** Logical
      Device to your Device Profile, and one to map the new **Spine** Logical Device
      to your Device Profile.
    * Under Design -> Interface Maps, click **Create interface
      Map**.
    * Select your **Superspine** Logical Device and required Device Profile. The new
      IM will be named accordingly, or you can rename it as desired:

      .. image:: static/images/5stage/create_im.png

    * Click **Create** to complete the Interface Map creation.
    * Repeat this step for your new **Spine** Logical Device. If you are using
      different models or vendors you need to repeat this step for each device,
      as per standard AOS processes.
    * The :doc:`Interface Maps <interface_maps>`, :doc:`Logical Devices
      <logical_devices>`
      and :doc:`Device Profiles <device_profile>` chapters cover these topics in
      detail.

#. :ref:`Create a rack-based template <create_rack-based_template>` that includes
   at least one link for **Superspine Connectivity**.
#. :ref:`Create a pod-based template <create_pod-based_template>` that uses as the
   pod the rack-based template created in the previous step.
#. :doc:`Create a blueprint <bp_create>` using the pod-based template that you
   created in the previous step.
#. Proceed with :doc:`building the network <staged_physical>` (assigning
   resources, devices, etc.) in the same manner as for 3-stage designs.

Modifying 5-stage Clos Network
==============================

5-stage blueprints are modified in the same way that a 3-stage network is
modified, provided the :ref:`limitations <limitations>` are taken into account.
Typically, topology changes are achieved with :doc:`rack changes <racks>`.

EVPN in 5-stage Topologies
==========================

.. versionadded:: 3.2

EVPN networks can be extended across multiple PODs within the same AOS Blueprint.
This provides the following added value:

* Scaling: provide any-to-any connectivity for applications distributed across
  multiple PODs.
* Workloads Redistribution: Load-balance applications by migrating a group of
  applications from one pod to another pod while preserving application IP and MAC
  addresses.
* Maintenance: Perform POD maintenance by migrating all the applications from one
  POD to another, while preserving the application IP and MAC addresses.
* Active / Standby Applications across sites/pods: Deploy A/S applications across
  multiple PODs to provide high availability at POD level, or as part of application
  migration tasks.
* Facilitate External Connectivity for a Virtual Network from a remote POD without
  external connectivity.

Just like with any other AOS managed network, AOS renders all required
configuration to bring up the multi-POD network, and use its proprietary *Intent
Based Networking* technology to validate the network operates as designed.

Creation of cross-POD Virtual Networks is no different from 3-stage networks. Refer
to :doc:`Virtual Networks <virtual_networks>` for a comprehensive
guide.

.. versionadded:: 3.3

AOS now supports a 5-stage Clos network for Junos QFX series of switches.
For end host redundancy we can use **ESI based** Rack Types. Please find the
detail steps to create **ESI based** Rack Type:

* Under **Design / Rack Type** click **Create Rack Type** to see the
  dialog for creating a Rack Type.

* Provide a Rack Type **Name** (17 characters or fewer) and configure
  as required for your design.

* Enter **Name** of the Juniper Leaf and provide **Leaf Logical Device**.

* Enter the number of the **Links per Spine** and **Link speed** of each link.

* Choose the **Redundancy Protocol** as **“ESI”** as per below example:

.. image:: static/images/5stage/esi.png

* To add a **External Link**, click **Add new link**.

* Click **Add New Server Group** and enter/select details.
  Please note **Port Channel ID Min/Max** values can be provided or
  left to default value. These IDs will be used to define aggregated
  Ethernet interfaces which connect each leaf device to the server.

* To add links from server to Leaf use **Add link** option.

* ESI is supported only on LAGs that span two leafs on the fabric. Hence,
  under **Link** **Attachment type** should be selected as **Dual-Homed**. Also
  enable **LACP** under **LAG Mode** option as per below screenshot:

.. image:: static/images/5stage/dualhoamed.png


* After this click **Create** to create Rack Type.

Now **ESI based** Rack Type can be used to crate a **RACK-BASED** template
as per the below example:

.. image:: static/images/5stage/5stageclos_junos.png

These templates can be further added as PODs to build Junos 5-stage
Clos network.

.. _limitations:

Limitations
===========

The following limitations apply to AOS 5-stage Clos topologies in AOS v3.1.0:

* It is not possible to upgrade a 3-stage topology to 5-Stage
* Only Cumulus Linux, Cisco NXOS, Arista EOS and Juniper Junos devices are supported
* Using Segmentation / :doc:`Group Based Policies <security_policies>`
* Predefined IBA Probes have Best Effort support
* Root Cause Analysis (ARCA) is not supported
* The same overlay protocol needs to be used for all Rack Types in al PODs
* Mixing PODs with L2 and L3 server connections is not supported
* IPv6 / IPv4 support:

  * No IPv6 support for Applications (IPv6 underlay is supported)
  * The entire fabric across all PODs needs to be either all IPv4, all IPv6 or all
    dual-stack

* Unsupported External Connectivity implementations:

  * One :doc:`External Router <external_routers>` connecting to different PODs
  * Mixing L2 and L3 external connectivity
  * EVPN with external Routers on Superspines
  * External Routers on Spines and Leafs in the same POD

* Unsupported Blueprint modifications:

  * Add or remove Superspine devices
  * Add or remove Superspine Planes
  * Add or remove PODs (Note it is possible to replace all Racks within a POD)

.. comments

 FAQ
 ===

 | **Q:** Why is the Pods dropdown empty when I try to create a Superspine template?
 | **A:** You do not have a **RACK-BASED** template that includes Spine to Superspine
  Links

 | **Q:**
 | **A:**
