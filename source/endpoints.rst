=========
Endpoints
=========

.. versionadded:: 3.0
    AOS supports the creation of Endpoints to be used with Segmentation and Group
    Based Policies (GBP).

.. note::
    As of AOS 3.0, Segmentation and Group Based Policies (GBP) is only supported on
    Cisco NX-OS and Arista EOS physical network devices.

.. warning::
    As of AOS 3.0, Segmentation and Group Based Policies (GBP) is not supported on
    Blueprints with IPv6 Applications enabled. It is only supported on Layer2,
    IPv4-only Blueprints.

This describes the necessary tasks to create Endpoints to be used with AOS
Segmentation and Group Based Policies (GBP).

For more information, see :doc:`Security Policies <security_policies>`.

Internal Endpoints
------------------

#. From the **Virtual** view of your **Staged** Blueprint, click
   **Internal Endpoints** to see the list of existing Internal Endpoints.
   If you have not yet created any Internal Endpoints, the list will be
   empty.

   .. image:: static/images/blueprint_virtual_tasks/blueprint_virtual_tasks_9_310.png

#. Click **Create Internal Endpoint** to see the dialog for creating an
   Internal Endpoint.

   .. image:: static/images/blueprint_virtual_tasks/300_endpoints-2.png

#. Fill in the required information.

   Name
      32 characters or fewer. Alphanumeric characters, underscores
      and dashes only.

   Virtual Network
      Select the Virtual Network where the Endpoint is located.

   IPv4 Subnet
      Enter the IPv4 Subnet/CIDR.

   Tags (optional)
      Set of tags that can be used for filtering, grouping beyond membership Custom
      Group or Virtual Network (e.g. “web server”, “db”, etc.).

    .. image:: static/images/blueprint_virtual_tasks/300_endpoints-3.png

   Validation is performed to ensure that the IP is within the L2 subnet of the
   Virtual Network and there is no endpoint with the same IP within the same
   Security Zone.

#. Click **Create** to create the Internal Endpoint. The new Endpoint appears in
   the list view.

   .. image:: static/images/blueprint_virtual_tasks/blueprint_virtual_tasks_10_310.png

External Endpoints
------------------

#. From the **Virtual** view of your **Staged** Blueprint, click
   **EXternal Endpoints** to see the list of existing External Endpoints.
   If you have not yet created any External Endpoints, the list will be
   empty.

   .. image:: static/images/blueprint_virtual_tasks/blueprint_virtual_tasks_11_310.png

#. Click **Create External Endpoint** to see the dialog for creating an
   External Endpoint.

   .. image:: static/images/blueprint_virtual_tasks/300_endpoints-6.png

#. Fill in the required information.

   Name
      32 characters or fewer. Alphanumeric characters, underscores
      and dashes only.

   Virtual Network
      Select the Virtual Network where the Endpoint is located.

   IPv4 Subnet
      Enter the IPv4 Subnet/CIDR.

   Tags (optional)
      Set of tags that can be used for filtering, grouping beyond membership Custom
      Group or Virtual Network (e.g. “web server”, “db”, etc.).

      .. image:: static/images/blueprint_virtual_tasks/300_endpoints-7.png

   Enforcement Points (optional)
      Enforcement Points are external facing points where access lists which involve
      External Endpoints are applied. Any External Router or External
      Connectivity Points can be added. Also any Enforcement Groups can be added.

      .. image:: static/images/blueprint_virtual_tasks/300_endpoints-8.png

      .. note::
          Enforcment Points are only supported on external facing interface on border
          leaf devices.

#. Click **Create** to create the External Endpoint. The new Endpoint appears in
   the list view.

   .. image:: static/images/blueprint_virtual_tasks/blueprint_virtual_tasks_12_310.png

Enforcement Points
------------------

Enforcement Points are automatically created when the user adds External Router or
External Connectivity Points to a Blueprint.

To view Enforcement Points, from the Blueprint **Staged** tab, click the **Virtual**
tab, click the **Endpoints** tab, then click the **Enforcement Points** tab.

.. image:: static/images/blueprint_virtual_tasks/blueprint_virtual_tasks_13_310.png

Creating Endpoint Groups
------------------------

#. From the **Virtual** view of your **Staged** Blueprint, click **Endpoint Groups**.

   .. image:: static/images/blueprint_virtual_tasks/blueprint_virtual_tasks_14_310.png

#. Click **Create Endpoint Group** to see the dialog for creating a group.

   .. image:: static/images/blueprint_virtual_tasks/300_endpoints-12.png

#. Fill in the required information.

   Name
      32 characters or fewer. Alphanumeric characters, underscores and
      dashes only

   Type
      Select the type of Endpoint Group to create: **Internal Endpoint Group**,
      **External Endpoint Group**, or **Enforcement Point Group**.

   Members
      Depending on the type of Endpoint Group you are creating, options for
      selecting members are presented.

      Internal Endpoint Group
         Select Multiple Internal Endpoints or other Internal Endpoint Groups.

         .. image:: static/images/blueprint_virtual_tasks/300_endpoints-13.png

      External Endpoint Group
         Select Multiple External Endpoints or other External Endpoint Groups then
         select which Enforcement Points or Enforcement Point Groups to associate
         the External Endpoint Group with.

         .. image:: static/images/blueprint_virtual_tasks/300_endpoints-14.png

      Enforcement Points Group
         Select Multiple Enforcement Points or other Enforcement Point Groups.

         .. image:: static/images/blueprint_virtual_tasks/300_endpoints-15.png

#. Click **Create** to create the group. The new Endpoint Groups appears in
   the list view.

   .. image:: static/images/blueprint_virtual_tasks/blueprint_virtual_tasks_15_310.png
