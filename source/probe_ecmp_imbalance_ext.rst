==========================================
ECMP Imbalance (External Interfaces) Probe
==========================================

.. image:: static/images/iba/external_ecmp_imbalance.svg

Found at /predefined_probes/external_ecmp_imbalance

It first identifies all the interfaces on all deployed and operational devices
that are attached to external routers. It then collects samples for each,
generates time series and calculates average traffic across configurable time
interval. It then calculates imbalance between traffic averages for these
interfaces on each device by calculating standard deviation. It then checks if
this imbalance is within acceptable range (which is also configurable) and if
not, how long has it been outside acceptable range and if this time exceeds
configurable interval it raises an anomaly. It also creates time series for this
anomaly so that one can observe recent history. Probe also calculates how many
systems are imbalanced in total and if this number is out of configurable range,
it raises the system level anomaly and keeps track about the history for this
system level anomaly.



"external interface traffic" processor
--------------------------------------

Purpose: wires in interface traffic samples (measured in transmitted bytes per
second) from each interface connected to external router.

Outputs of "external interface traffic" processor
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

'external_int_bytes': set of traffic samples (for each external router facing
interface). Each set member has the following keys to identify it: label (human
readable name of the system), system_id (id of the system, usually serial
number), interface (name of the interface).



"external interface traffic avg" processor
------------------------------------------

Purpose: Calculate average traffic during period specified by average_period
facade parameter. Unit is bytes per second.

Outputs of "external interface traffic avg" processor
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

'external_int_bytes_avg': set of traffic average values (for each external router
facing interface). Each set member has the following keys to identify
it: label (human readable name of the system), system_id (id of the system,
usually serial number), interface (name of the interface).



"ext int traffic history" processor
-----------------------------------

Purpose: create recent history time series out of traffic samples from the
external_int_bytes output. In terms of the number of samples, the time series
will hold the smaller of: 1024 samples or samples collected during the last
'total_duration' seconds (facade parameter). Samples unit is bytes per second.

Outputs of "ext int traffic history" processor
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

ext_int_traffic_accumulate: set of traffic samples time series (for each external
router facing interface).  Each set member has the following keys to identify
it: label (human readable name of the leaf), system_id (id of the system,
usually serial number), interface (name of the interface). Samples unit is bytes
per second.


"external interface std-dev" processor
--------------------------------------

Purpose: calculate standard deviation for a set consisting of traffic averages
for each external router facing interface on a given system. Grouping per system
is achieved using 'group_by' property set to 'system_id' and 'label'.

Outputs of "external interface std-dev" processor
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

ext_int_std_dev: set of values, each indicating standard deviation (as a
measure of ECMP imbalance) for traffic averages for each external router facing
interface on a given system. Each set member has 'system_id' and 'label' key to
identify system whose ECMP imbalance the value represents.


"live ecmp imbalance" processor (external router)
-------------------------------------------------

Purpose: Evaluate if standard deviation between external router facing interfaces
on each system is within acceptable range. In this case acceptable range is between
0 and std_max facade parameter (in bytes per second unit).


Outputs of "live ecmp imbalance" processor (external router)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

'live_ecmp_imbalance': set of true/false values, each indicating if standard
deviation (as a measure of ECMP imbalance) for traffic averages for each external
router facing interface on a given leaf is within acceptable range. Each set
member has system_id key to identify system whose ECMP imbalance the value
represents.


"sustained ecmp imbalance" processor (exernal router)
-----------------------------------------------------

Purpose: Evaluate if standard deviation between external router facing interfaces
on each leaf has been outside acceptable range, (as defined by 'live ecmp
imbalance' processor) for more than 'threshold_duration' seconds during last
'total_duration' seconds. These two parameters are part of facade specification.


Outputs of "sustained ecmp imbalance" processor (exernal router)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

'sustained_ecmp_imbalance': set of true/false values, each indicating if standard
deviation (as a measure of ECMP imbalance) for traffic averages for each external
router facing interface on a given system has been outside acceptable range for
more than specified period of time. Each set member has system_id key to identify
system whose ECMP imbalance the value represents.


"anomaly ecmp imbalance" processor
----------------------------------

Purpose: Export sustained ecmp imbalance when true as an anomaly for each system.


Outputs of "anomaly ecmp imbalance" processor
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

'system_tx_imbalance_state_anomaly': set of true/false values, each indicating
if standard deviation (as a measure of ECMP imbalance) for traffic averages for
each external router facing interface on a given system has been outside
acceptable range for more than specified period of time. Each set member has
system_id key to identify system whose ECMP imbalance the value represents.




"anomaly accumulate" processor
------------------------------

Purpose: Create time series showing ecmp anomaly being raised and cleared for
each system under consideration. This time series may contain up to
'anomaly_history_count' anomaly state changes.

Outputs of "anomaly accumulate" processor
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

'anomaly_accumulate': Time series showing ecmp anomaly being raised and cleared
for each system under consideration. This time series may contain up to
'anomaly_history_count' anomaly state changes.



"systems imbalanced count" processor
------------------------------------

Purpose: Count how many systems have external ecmp imbalance anomaly true at any
instant in time.

Outputs of "systems imbalanced count" processor
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

'systems_tx_imbalance_count': Number of systems with external ecmp imbalance.




"live systems imbalanced" processor
-----------------------------------

Purpose: Evaluate if the number of imbalanced systems is within acceptable
range, which in this instance means less than 'max_systems_imbalanced' value
which is a facade parameter

Outputs of "live systems imbalanced" processor
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

'live_system_imbalance_count': Boolean indicating if the number
of imbalanced systems is within accepted range, i.e. less than
'max_systems_imbalanced" which is a facade parameter




"anomaly sustained ecmp imbalance" processor
--------------------------------------------------------

Purpose: Export as anomaly when the number of imbalanced systems is
not within acceptable range, where acceptable range is defined as less
than 'max_systems_imbalanced' value.

Outputs of "anomaly sustained ecmp imbalance" processor
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

'system_tx_imbalance_count_anomaly': Boolean indicating if the number
of imbalanced systems is within accepted range, i.e. less than
'max_systems_imbalanced" which is a facade parameter



"historical anomaly accumulate" processor
-----------------------------------------

Purpose: Create time series showing imbalanced system count out of range
anomaly being raised and cleared. This time series may contain up to
'system_imbalance_history_count' anomaly state changes.

Outputs of "historical anomaly accumulate" processor
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

'historical_anomaly_accumulate': time series showing imbalanced system
count out of range anomaly being raised and cleared. This time series may contain
up to 'system_imbalance_history_count' anomaly state changes.
