=========
Event Log
=========
Information about user login, logout, blueprint changes, and other events
are captured in the event log.

From the AOS web interface, navigate to **Platform / Audit / Event Log**.

.. image:: static/images/platform/event_log_330.png

You can view changes to device configurations by
clicking **View Config** next to a **DeviceConfigChange** type of event.

.. figure:: static/images/user_management/device_config_310.png
   :align: center

   Device Configuration Change Details

Exporting Event Log to CSV File
===============================

#. Click **Export to CSV**.
#. Enter details for information that you want to extract.
#. Click **Save as CSV File**.

Sending Event Log with Syslog
=============================
See :doc:`Syslog Configuration <syslog_configuration>` for details about sending
the event log to an external system with the Syslog protocol.
