=============================
Processor: System Utilization
=============================
Interface Counters Utilization Per System processor groups detailed interface
counter data by system ID and then calculates aggregate TX and RX bits, their
aggregate utilization and identifies the highest TX and RX utilizations among
the interfaces.

**Input Types**

**Output Types**

**Properties**

   RX Property Name
      Property name from the parent stage having RX bits value
   RX Utilization Property Name
      Property name from the parent stage having RX utilization value
   TX Property Name
      Property name from the parent stage having TX bits value
   TX Utilization Property Name
      Property name from the parent stage having TX utilization value

   .. include:: includes/processors/enable_streaming.rst
