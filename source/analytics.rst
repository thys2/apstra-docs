==========================
|analytics_icon| Analytics
==========================

.. |analytics_icon| image:: static/images/icons/analytics_icon.png

.. toctree::
   :maxdepth: 2

   iba
   dashboard_analytics
   anomalies_probes
   widgets
   probes
