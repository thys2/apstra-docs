==========
IPv6 Pools
==========

IPv6 Pool Overview
==================
You can create resource pools during the design phase or just before you need them
during the build phase. When you assign resources to managed devices in your
network (blueprint), AOS pulls them automatically from the pool you specify. In
cases where you need to assign a specific network identifier you have the option
of assigning a resource individually. To prevent shortages as a network grows,
network design best practices recommend defining more resources than are required
for the initial design.

To use IPv6 addressing, IPv6 must be :doc:`enabled <staged_settings>`. IPv6 is
supported on EVPN L2 deployments and L3 deployments. Full feature parity for IPv6
across vendors is not available. Refer to :doc:`feature_matrix` for details.
IPv6 pools include the following details:

Pool Name
  To identify the resource pool.

Total Usage
  Percentage of IPv6 addresses in use for all subnets in the resource pool. (Hover
  over status bar to see number of IPv6 addresses in use and total number of
  IPv6 addresses in the pool.) (New in AOS version 3.3.0)

Per Subnet Usage
  The IPv6 addresses included in the subnet and the percentage that are in use.
  (Hover over status bar to see number of IPv6 addresses in use and total
  number of IPv6 addresses in that subnet.) (New in AOS version 3.3.0)

Status
  Indicates if the pool is in use or not.

Tags (optional)
  To enable filtering by user-specified categories.

From the AOS web interface, navigate to **Resources / IPv6 Pools**.
AOS ships with a predefined pool (fc01:a05:fab::/48).

.. image:: static/images/resources/ipv6_pool_330.png

.. sidebar:: Cloning IP Pool

   Instead of entering all details for a new IP pool, you can clone an existing
   one, give it a new name and customize it.

Creating IPv6 Pool
==================
#. From the AOS web interface, navigate to **Resources / IPv6 Pools**, then click
   **Create IPv6 Pool**.
#. Enter a name and valid subnet.

   * To specify an additional subnet, click **Add a Subnet**.

#. Click **Create** to create the pool and return to the list view.

When the blueprint is ready, you can assign resources by
:doc:`updating assignments <build_virtual>` in the **Staged / Virtual** view.

Editing IPv6 Pool
=================
If any IP addresses in a pool are in use, they cannot be removed from the pool.

#. Either from the list view (Resources / IPv6 Pools) or the details view,
   click the **Edit** button for the pool to edit.
#. Make your changes.

   * To add a subnet, click **Add a subnet**.
   * To change a subnet, change the subnet value.
   * To remove a subnet, click the **X** to the right of the subnet to delete.

#. Click **Update** to update the pool and return to the list view.

Deleting IPv6 Pool
==================
If any IP addresses in an IP pool are in use, the pool cannot be deleted.

#. Either from the list view (Resources / IPv6 Pools) or the details view,
   click the **Delete** button for the pool to delete.
#. Click **Delete** to delete the pool and return to the list view.
