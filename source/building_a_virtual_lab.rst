======================
Building a Virtual Lab
======================

Apstra Customers can build their own virtual lab with an instance of the Apstra
AOS Server and a small group of virtual network devices.

Apstra Support only provides limited support for Customer virtual labs.

AOS Virtual Lab on EVE-NG
=========================

This guide describes the process to set up a virtual lab environment with Apstra
AOS and virtual network devices using the Emulated Virtual Environment Next
Generation (EVE-NG).

Installing EVE-NG
-----------------

EVE-NG is a virtual machine which can be obtained from the EVE-NG website
(http://www.eve-ng.net/).

The following documentation describes installing EVE-NG on VMware Workstation.

:Step 1: Download the EVE-NG OVA and import it as a new Virtual Machine.

  .. image:: static/images/building_a_virtual_lab/eve-1.png

  .. image:: static/images/building_a_virtual_lab/eve-2.png

  .. image:: static/images/building_a_virtual_lab/eve-3.png

  .. image:: static/images/building_a_virtual_lab/eve-4.png

:Step 2: Use as much memory as possible for the virtual machine. This memory will be
         shared for the AOS VM and virtual network devices.

  .. image:: static/images/building_a_virtual_lab/eve-5.png

  Use as many processors as available.

  IMPORTANT … Make sure to enable “Virtualize Intel VT-x/EPT or AMD-V/RVI.

  .. image:: static/images/building_a_virtual_lab/eve-6.png

  .. image:: static/images/building_a_virtual_lab/eve-7.png

  .. image:: static/images/building_a_virtual_lab/eve-8.png

:Step 3: Start the EVE-NG VM.

  .. image:: static/images/building_a_virtual_lab/eve-9.png

:Step 4: After starting the VM, the console will display the EVE-NG Linux login
  prompt. Log in as username ``root``, password ``eve``.

  .. image:: static/images/building_a_virtual_lab/eve-10.png

:Step 5: EVE-NG will prompt to set a new root password.

  .. image:: static/images/building_a_virtual_lab/eve-11.png

:Step 6: After booting the EVE-NG VM, using a web browser, navigate to the IP
  address of EVE-NG and log in a username ``admin``, password ``eve``.

  .. image:: static/images/building_a_virtual_lab/eve-12.png

:Step 7: Click the “create a new lab” icon.

  .. image:: static/images/building_a_virtual_lab/eve-13.png

:Step 8: You will see a blank lab screen.

  .. image:: static/images/building_a_virtual_lab/eve-14.png

:Step 9: First, right-click and add a new “Network”.

  .. image:: static/images/building_a_virtual_lab/eve-15.png

:Step 10: Adding a network of Type “Management (Cloud0)” will connect this network
  to the interface of the EVE-NG VM.

  .. image:: static/images/building_a_virtual_lab/eve-16.png

Installing AOS VM
-----------------

:Step 1: To add an AOS Server, upload the “.qcow2.gz” AOS KVM image (e.g.
  aos_server_2.2.1-166.qcow2.gz) to the EVE-NG server. Then decompress the image with
  the ``gzip -d`` command. Make a directory under the ``/opt/unetlab/addons/qemu/``
  directory beginning with ``linux-``.

.. code-block:: bash

  root@eve-ng:~# ls -l aos_server_2.2.1-166.qcow2.gz
  -rw-r--r-- 1 root root 1195424229 Aug 31 20:34 aos_server_2.2.1-166.qcow2.gz
  root@eve-ng:~# mkdir -p /opt/unetlab/addons/qemu/linux-aos_server_2.2.1-166
  root@eve-ng:~# gzip -d aos_server_2.2.1-166.qcow2.gz
  root@eve-ng:~# cp aos_server_2.2.1-166.qcow2 /opt/unetlab/addons/qemu/linux-aos_server_2.2.1-166/hda.qcow2
  root@eve-ng:~# /opt/unetlab/wrappers/unl_wrapper -a fixpermissions

:Step 2: From the EVE-NG lab, right-click and add a new Node.

.. image:: static/images/building_a_virtual_lab/eve-17.png

.. image:: static/images/building_a_virtual_lab/eve-18.png

.. image:: static/images/building_a_virtual_lab/eve-19.png

:Step 3: Set the following VM CPU/RAM:

* CPU: 8
* RAM: 8192 MB (16384 MB for AOS 3.0 or later)

:Step 4: Change the Console to “telnet”.

.. image:: static/images/building_a_virtual_lab/eve-20.png

:Step 5: Connect the “e0” interface to the Management Network.

.. image:: static/images/building_a_virtual_lab/eve-21.png

.. image:: static/images/building_a_virtual_lab/eve-22.png

:Step 6: Start the VM and connect to its console.

.. image:: static/images/building_a_virtual_lab/eve-23.png

.. image:: static/images/building_a_virtual_lab/eve-24.png

:Step 7: Follow :ref:`server_installation:AOS Server Installation` Documentation
  for setting up the AOS Server VM.

.. image:: static/images/building_a_virtual_lab/eve-26.png

:Step 8: You should now be able to connect to the IP address of the AOS Controller
  with a web browser.

.. image:: static/images/building_a_virtual_lab/eve-27.png

Installing Virtual Network Devices
----------------------------------

.. note::

  It is up to the user to obtain Virtual Network Device images. Apstra Support cannot
  provide these images.


Add Arista EOS Devices
``````````````````````

:Step 1: Obtain the Arista vEOS 4.20.1F VMDK (vEOS-lab-4.20.1F.vmdk) and "Aboot"
  (Aboot-veos-serial-8.0.0.iso) images from Arista.

.. note::

  Please check the :doc:`AOS Feature Matrix <feature_matrix>` for the latest
  recommended version of Arista EOS. Arista EOS is licensed software and cannot
  be provided by Apstra Support.

.. code-block:: bash

  root@eve-ng:~# ls -l Aboot-veos-serial-8.0.0.iso vEOS-lab-4.20.1F.vmdk
  -rw-r--r-- 1 root root   5242880 Aug 31 20:35 Aboot-veos-serial-8.0.0.iso
  -rw-r--r-- 1 root root 662044672 Aug 31 20:34 vEOS-lab-4.20.1F.vmdk
  root@eve-ng:~# mkdir -p /opt/unetlab/addons/qemu/veos-4.20.1F
  root@eve-ng:~# /opt/qemu/bin/qemu-img convert -f vmdk -O qcow2 vEOS-lab-4.20.1F.vmdk /opt/unetlab/addons/qemu/veos-4.20.1F/hda.qcow2
  root@eve-ng:~# cp Aboot-veos-serial-8.0.0.iso /opt/unetlab/addons/qemu/veos-4.20.1F/cdrom.iso
  root@eve-ng:~# /opt/unetlab/wrappers/unl_wrapper -a fixpermissions

:Step 2: From the EVE-NG lab, right-click and add a new Node.

.. image:: static/images/building_a_virtual_lab/eve-eos-1.png

.. image:: static/images/building_a_virtual_lab/eve-eos-2.png

.. image:: static/images/building_a_virtual_lab/eve-eos-3.png

.. image:: static/images/building_a_virtual_lab/eve-eos-4.png

.. image:: static/images/building_a_virtual_lab/eve-eos-5.png

:Step 3: Repeat the process for additional devices.

.. image:: static/images/building_a_virtual_lab/eve-eos-6.png

:Step 4: Connect the devices together as desired.

.. image:: static/images/building_a_virtual_lab/eve-eos-7.png

:Step 5: Specify interfaces.

.. image:: static/images/building_a_virtual_lab/eve-eos-8.png

.. image:: static/images/building_a_virtual_lab/eve-eos-9.png

:Step 6: Connect to the EOS device console.

.. image:: static/images/building_a_virtual_lab/eve-eos-10.png

:Step 7: Configure the necessary management configuration.

.. image:: static/images/building_a_virtual_lab/eve-eos-12.png


Add Cisco NX-OS Devices
```````````````````````

:Step 1: Obtain the Cisco NX-OSv 7.0(3)I7(4) QCOW2 (nxosv-final.7.0.3.I7.4.qcow2)
  image from Cisco and copy to the EVE-NG Server.

.. note::

  Please check the :doc:`AOS Feature Matrix <feature_matrix>` for the latest
  recommended version of Cisco NX-OS. Cisco NX-OS is licensed software and cannot
  be provided by Apstra Support.

.. code-block:: bash

  root@eve-ng:~# ls -l nxosv-final.7.0.3.I7.4.qcow2
  -rw-r--r-- 1 root root 1745027072 Aug 31 20:33 nxosv-final.7.0.3.I7.4.qcow2
  root@eve-ng:~# mkdir -p /opt/unetlab/addons/qemu/nxosv9k-7.0.3.I7.4
  root@eve-ng:~# cp nxosv-final.7.0.3.I7.4.qcow2 /opt/unetlab/addons/qemu/nxosv9k-7.0.3.I7.4/sataa.qcow2
  root@eve-ng:~# /opt/unetlab/wrappers/unl_wrapper -a fixpermissions

:Step 2: From the EVE-NG lab, right-click and add a new Node.

.. image:: static/images/building_a_virtual_lab/eve-nxos-1.png

.. image:: static/images/building_a_virtual_lab/eve-nxos-2.png

.. image:: static/images/building_a_virtual_lab/eve-nxos-3.png

.. image:: static/images/building_a_virtual_lab/eve-nxos-4.png

:Step 3: Connect to the NX-OS device console.

:Step 4: Configure the necessary management configuration.

.. image:: static/images/building_a_virtual_lab/eve-nxos-5.png

.. image:: static/images/building_a_virtual_lab/eve-nxos-6.png


Add Cumulus Linux Devices
`````````````````````````

:Step 1: Obtain the Cumulus Linux CVX QCOW2
  (e.g. cumulus-linux-3.7.3-vx-amd64.qcow2) image from Cumulus and copy to the EVE-NG
  Server.

.. note::

  Please check the :doc:`AOS Feature Matrix <feature_matrix>` for the latest
  recommended version of Cumulus Linux. Cumulus Linux is licensed software and
  cannot be provided by Apstra Support.

.. code-block:: bash

  root@eve-ng:~# ls -l cumulus-linux-3.7.3-vx-amd64.qcow2
  -rw-r--r-- 1 root root 1068761088 Aug 31 20:33 cumulus-linux-3.7.3-vx-amd64.qcow2
  root@eve-ng:~# mkdir -p /opt/unetlab/addons/qemu/cumulus-linux-3.7.3
  root@eve-ng:~# cp cumulus-linux-3.7.3-vx-amd64.qcow2 /opt/unetlab/addons/qemu/cumulus-linux-3.7.3/hda.qcow2
  root@eve-ng:~# /opt/unetlab/wrappers/unl_wrapper -a fixpermissions

.. image:: static/images/building_a_virtual_lab/eve-cl-1.png

.. image:: static/images/building_a_virtual_lab/eve-cl-2.png

.. image:: static/images/building_a_virtual_lab/eve-cl-3.png

.. image:: static/images/building_a_virtual_lab/eve-cl-4.png

After devices have been added, proceed with installing the agents
(**Devices / System Agents / Agents**).

Build an ESXi vEOS Lab
======================

This document describes how to build a dual MLAG leaf-pair running Arista vEOS
inside of VMware ESXi.

.. image:: static/images/building_a_virtual_lab/esxi-1.png


Obtain and Deploy AOS Server OVA in VMware ESXi
-----------------------------------------------

Install the AOS server from the "Software Downloads" section and deploy it under
VMware ESXi.  After the server is built, log in with username ``admin`` password
``admin``, and assign an IP address by editing /etc/network/interfaces.

The AOS server is distributed as a virtual-machine image in OVA format.  You will
need to import this OVA into your hypervisor environment as described in the
installation section.

The AOS server defaults to use DHCP to acquire its network address.  You can
statically configure the IP address after the server is initially installed.
Please ensure that AOS server IP-address or AOS server hostname value is properly
configured in the device-agent ``aos.conf`` file.  Refer to the Vendor-specific
device-agent installation guides for further details.

Start the VMware vSphere Client and connect to the ESXi host or vCenter Server
managing that host on which we want to deploy the AOS Server VM.

:Step 1: File->Deploy OVF template

:Step 2: Provide the HTTP URL or the path in the local filesystem of the AOS Server
  OVA file and click “Next” until a name for the Virtual Machine is asked.

:Step 3: Select the Disk format “Thin Provision”

:Step 4: A summary screen with the VM configuration will appear. Check “Power on
  after deployment” and click Finish.

:Step 5: A progress bar will indicate the deployment status, at the end of which
  the VM will boot automatically.

  .. image:: static/images/building_a_virtual_lab/esxi-2.png

:Step 6: To open a console window on AOS Server, right click on the AOS Server VM
  -> Open Console

Follow :ref:`server_installation:AOS Server Installation` Documentation for setting
up the AOS Server VM.

Build Arista vEOS Template in ESXi
----------------------------------

Building vEOS images
````````````````````

This document contains instructions on how to build an Arista vEOS image for use
with AOS.

In order to build a vEOS lab, we need some images

* Arista vEOS VMDK, appropriate for your hypervisor
* ABoot image (Optional)
* Arista EOS SDK (Required)

Download Arista vEOS
````````````````````

Obtain the Arista vEOS image from https://www.arista.com/en/support/software-download

Download Arista Aboot
`````````````````````

The Arista ABoot ISO is used to bootstrap the VM to begin loading from the IDE:0
hard drive.

.. note::
    The ABoot ISO is only used if the customer deploys the vEOS-lab images.
    vEOS-combined images already include the ABoot as part of the vmdk image.  We
    recommend -combined- images.

The Aboot image is available from the vEOS section on
https://www.arista.com/en/support/software-download.

.. image:: static/images/arista_veos/aboot.png

Arista vEOS configuration specifics
```````````````````````````````````

Minimum resources

+-------------+--------------+
| Minimum RAM | Minumum vCPU |
+=============+==============+
| 2.5 GB      | 1 vCPU       |
+-------------+--------------+

Arista vEOS requires 2.5GB and at least 1 vCPU for a stable installation.

While vEOS itself can run on 512mb, the AOS Agent takes about another 1.5GB of ram.

Performance note
````````````````

Arista vEOS is used for **Demo Purposes Only**.  Arista vEOS suffers from a
lack of performance due to the way it forwards traffic.  All of the 'front panel'
ports - eg, Eth1, Eth2, Eth3, etc, operate in the CPU plane.  There is a software
emulation layer, called the `Ethernet Bridging Agent` or `Etba` for short.

`Etba` is responsible for all L2 and L3 traffic forwarding, spanning-tree decisions,
BFD, etc.  Etba cannot provide line-speed performance.  At Apstra, we have tested
that vEOS may max out at only **100Kbps** of traffic.  On complicated L2 networks,
this performance is even slower.  These performance issues do not appear in a network
based on hardware switches.  We cannot improve the performance of vEOS.

When networks are deployed on top of vEOS, Apstra AOS may take a few minutes to
converge.  If an network administrator runs ``top`` on the switch, we observe that
Etba is maxing out the CPU as it is performing calculations.

The Arista vEOS platform also exhibits the behavior of consuming 100% of a vCPU.

.. warning::
    When a vEOS instance co-habitates another hypervisor, other applications
    will suffer.

To reiterate, these performance problems do not appear on physical hardware switches.

vEOS VXLAN Limitation with MLAG
```````````````````````````````

The Arista implementation of VXLAN and MLAG on vEOS does not have functionality to
share MAC addresses across a peer-link.  This means that when applications are
forwarding L2 traffic on an arista VXLAN network deployed with vEOS, traffic that
is routed to leaf2 will not be forwarded across peer-link to leaf1.  This will give
the behavior of very long ping times and extremely high drop rates to remote vxlan
endpoints.  These are vEOS limitations.   Packets may suddenly start dropping as
the fabric changes an ECMP hash somewhere.

When configuring vxlan on vEOS, **do not deploy VXLAN on MLAG Pairs with vEOS**
otherwise traffic will be dropped.  Non-MLAG pairs operate normally with VXLAN and
AOS 2.0+.

This is only an issue for VXLAN deployed on vEOS.

Promiscuous mode
````````````````

On some hypervisors, Arista vEOS also requires the *management1* interface to be
marked as *promiscuous*.
This allows the ma1 interface to respond to packets on the automatically generated
MAC address used
for the ma1 interface when it's configured in a VRF.


.. warning::

    If the management interface is not marked as promiscuous on a virtual platform,
    vEOS will not be able to send or receive any traffic.  Ensure the hypervisor
    has the Management interface configured as promiscuous.

We can make use of a script that runs with the startup of the interface to force
the nic to be promiscuous.

.. code-block:: text
    :caption: Automatically set Management1 interface as promiscuous when booting

    event-handler ma-promisc
     action bash /sbin/ifconfig ma1 promisc
     delay 10
     trigger on-intf Management1 operstatus

Change vEOS VM NIC type
```````````````````````

For VMware ESXi, you may need to modify the .vmx file to ensure that the NICs are
set to ``e1000`` as the NIC type to make sure all nics are Intel.

.. important::

    Failure to modify NIC types may cause the agent to fail in unexpected ways -
    LLDP/LACP may fail, L2 may not flow traffic, or interfaces may never come up, or
    a management NIC may never get a DHCP lease during ZTP.

.. code-block:: ini
    :caption: ESXi NIC types

    ethernet0.present = "TRUE"
    ethernet0.virtualDev = "e1000"
    ethernet1.present = "TRUE"
    ethernet1.virtualDev = "e1000"
    ethernet2.present = "TRUE"
    ethernet2.virtualDev = "e1000"

For Virtualbox, the NIC type can be set in the GUI.

Creating the VM template
````````````````````````

:Step 1: In vSphere, create a new virtual machine.

.. image:: static/images/arista_veos/esxi_new_vm.png

:Step 2: Choose a storage location and a name for the VM.

The details of your storage location will be specific to your environment.

:Step 3: Set VM HW Version 8

Setting VM HW version 8 allows you to perform all of the operations on a VM
using the legacy ESX console application without any errors.

.. image:: static/images/arista_veos/esxi_vm_hw8.png

:Step 4: For Guest OS, choose Other 2.6 Linux (32-Bit)

This setting is recommended by Arista.

.. image:: static/images/arista_veos/esxi_guest_os.png

:Step 5: Set a management NIC.  We will add the other 7 front-panel nics in a
  future step.

.. image:: static/images/arista_veos/esxi_mgmt_nic.png

:Step 6: Create a LSI Logic Parallel controller (default).

This is required to ensure the first disk attaches in IDE Mode.

.. image:: static/images/arista_veos/esxi_lsi_logic.png

:Step 7: When prompted, Do NOT create a disk.

.. image:: static/images/arista_veos/esxi_no_disk.png

:Step 8: Create the VM and edit VM settings.

:Step 9: Mount the ABoot ISO you downloaded and make sure *Connect at power-on* is
  checked.

.. note::
    This step is optional if using the ``vEOS-combined`` vmdk image.

.. warning::
    Mounting the ABoot ISO from a shared location can cause deadlocking issues on
    your storage infrastructure.  Copy a unique ABoot ISO to each VM guest to
    avoid issues when booting up your VM.

.. image:: static/images/arista_veos/esxi_aboot_iso.png

:Step 10: Add 7 more NICs to an "Unused" Vmware Port-group.

.. note::
    Make sure Adapter type remains E1000, connect at power-on is UNSELECTED, and
    network label is unused.  If the 'connect at power-on' setting is set to yes,
    this may bridge all of the VMs together when they boot up with L2 interfaces.

    A further step will set the NIC bindings properly.

.. image:: static/images/arista_veos/esxi_nics_unused.png

:Step 11: Copy the Arista VMDK to the VM folder.  You may have to browse for this.

.. warning::
    Do not share the VMDK with multiple vEOS images - this will cause the vEOS
    installation to become corrupted

.. code-block:: prompt

    [root@host2:/vmfs/volumes/a25f43f6-f2c3bc72/arista_lab_2_leaf_2] ls -lah
    total 507788
    drwxr-xr-x    2 root     root        4.0K Mar  9 00:29 .
    drwxrwxrwx   59 root     root        4.0K Mar  9 00:10 ..
    -rw-r--r--    1 root     root           0 Mar  9 00:11 arista_lab_2_leaf_2.vmsd
    -rwxr-xr-x    1 root     root        2.4K Mar  9 00:12 arista_lab_2_leaf_2.vmx
    -rwxr-xr-x    1 root     root      495.9M Mar  9 00:29 vEOS-lab-4.16.6M.vmdk
    [root@host2:/vmfs/volumes/a25f43f6-f2c3bc72/arista_lab_2_leaf_2]

:Step 12: On ESXi 6.0, we have to convert the disk to thick format.

.. note::
    The disk format that Arista ships the VMDK with may not boot properly when
    mounting.  See https://eos.arista.com/tip-for-arista-veos-on-vmware-esx-6/

Run ``vmkfstools -i vEOS-lab-4.16.6M.vmdk -d eagerzeroedthick
vEOS-lab-4.16.6M-thick.vmdk``

.. code-block:: prompt

    [root@host2:/vmfs/volumes/a25f43f6-f2c3bc72/arista_spine1] vmkfstools -i vEOS-lab-4.16.6M.vmdk -d eagerzeroedthick vEOS-lab-4.16.6M-thick.vmdk
    Destination disk format: VMFS eagerzeroedthick
    Cloning disk 'vEOS-lab-4.16.6M.vmdk'...
    Clone: 99% done.
    ...
    Clone: 100% done.


:Step 13: Add the VMDK as a previously mounted hard drive in ESX

.. image:: static/images/arista_veos/esxi_add_hd.png

.. image:: static/images/arista_veos/esxi_add_hdd2.png

.. image:: static/images/arista_veos/esxi_add_hdd3.png

:Step 14: Ensure the VMDK and HDD are attached to ``IDE: (0:0)``.

.. warning::
    Connecting the HDD to the wrong slot will cause vEOS to fail to boot

.. image:: static/images/arista_veos/esxi_hdd_ide0.png

:Step 15: Boot the switch.

.. image:: static/images/arista_veos/esxi_boot.png

:Step 16: Log in to the switch.

.. note::
    Default credentials on vEOS are ``admin`` and ``admin`` for username and
    password.

:Step 17: Confirm the right number of NICs are present.  We expect one Management
  nic and 7 front-panel nics, the maximum Arista vEOS can support.

.. image:: static/images/arista_veos/esxi_nic_confirm.png

Install Arista Device System Agents via the Web interface
(**Devices / System Agents / Agent**).

Create VMware Port-groups for AOS Virtual Labs
----------------------------------------------

In order for virtual switches to be able to properly communicate with each other,
each interface pair needs to share a dedicated port-group in VMware that carries
all VLANs and allows MAC changes, Forged Transmits, and Promiscuous mode.

Download the AOS Cable map from the AOS Blueprint.  An example CSV file is below.

.. image:: static/images/building_a_virtual_lab/esxi-ports-1.png

The left-hand column 'A' describes the network name that needs to be set up under
ESXi attached to a promiscuous port-group.

We need to connect the device on column 'C' to the device on column 'F' interface.
For example:
Vmware port-group:
Spine1<->virtual_leaf_1_leaf2:
Virtual port-group name: 'spine1-virtual-leaf-1-leaf2'

This

spine1 Ethernet3 (VMware Network Adapter #4)
virtual_leaf_1_leaf2 Ethernet1 (VMWare network adapter #2)

The process to create a port-group is as follows.

In the ESXi Console, go to the Host configuration tab, and click "Add networking".

.. image:: static/images/building_a_virtual_lab/esxi-ports-2.png

Choose "Virtual Machine" as the connection type.

.. image:: static/images/building_a_virtual_lab/esxi-ports-3.png

Select "Create a vSphere standard switch".

.. image:: static/images/building_a_virtual_lab/esxi-ports-4.png

Create a network label that helps represent the connection name. In this case in
example:

‘Spine1_eth3-lab1-leaf1_eth1’

Make sure the VLAN ID is set to ‘4095’.  This means “All vlans” and allows the
switch interface to act promiscuously.

.. image:: static/images/building_a_virtual_lab/esxi-ports-5.png

After the switch is created we have to assign the traffic policy.  View the vSwitch
under the UI and click 'properties'.

.. image:: static/images/building_a_virtual_lab/esxi-ports-6.png

Followed by 'Edit'.

.. image:: static/images/building_a_virtual_lab/esxi-ports-7.png

On the General tab, set the MTU to 9000 from the default 1500.  We also recommend
changing the number of ports from the default 120 to 8 to prevent resource
exhaustion with many networks.

.. image:: static/images/building_a_virtual_lab/esxi-ports-8.png

Under the Security tab, change Promiscuous Mode from Reject to Accept.

.. image:: static/images/building_a_virtual_lab/esxi-ports-9.png

All of the values should say Accept.

If you receive this error, please ignore it - we are not connecting these virtual
switches anywhere to the real world.

.. image:: static/images/building_a_virtual_lab/esxi-ports-10.png

After the switch is created the properties should appear like this:

.. image:: static/images/building_a_virtual_lab/esxi-ports-11.png

After the Port-group is created, we need to assign it to the VM.

Find your VM (spine1), and edit settings.

.. image:: static/images/building_a_virtual_lab/esxi-ports-12.png

Each network adapter is mapped out starting from Management1 - Note this allocation
format.

.. image:: static/images/building_a_virtual_lab/esxi-ports-13.png

In this example, we want to connect Eth3 to lab-1-leaf1 Eth1.  Set the NIC to
connected, and change the port group.

.. image:: static/images/building_a_virtual_lab/esxi-ports-14.png

That's a lot of steps.

A short example script can be used to create the port-groups under the ESX Console.
This will create the port-groups quickly, but you will have to still assign each
VM's network adapter in the GUI.

.. code-block:: text

    esxcli network vswitch standard add --vswitch-name=$NIC_NAME$ --ports=8
    esxcli network vswitch standard set --vswitch-name=$NIC_NAME$ --cdp-status=down --mtu=9000
    esxcli network vswitch standard policy security set --vswitch-name=$NIC_NAME$ --allow-forged-transmits true --allow-mac-change true --allow-promiscuous true
    esxcli network vswitch standard portgroup add --vswitch-name=$NIC_NAME$ --portgroup-name=$NIC_NAME$
    esxcli network vswitch standard portgroup set --portgroup-name=$NIC_NAME$ –vlan-id=4095
