=====================
Processor: Comparison
=====================
The Comparison processor takes two NS inputs: 'A' and 'B'. It then matches
corresponding items from the inputs by their keys, and performs a comparison
operation defined by the 'operation' configuration property. If the inputs have
different sets of keys, the 'significant_keys' configuration property should be
set, which is a list of keys used to map items from the inputs. Otherwise,
if the inputs set of keys are different, no items will be matched and an empty
result is returned. Also, inputs and significant_keys (if specified) must allow
only 1:1 item mapping from 'A' to 'B'. If it allows to match one item from 'A'
to more than one item from 'B' and vice versa, the probe goes into error state.

**Input Types** - Number-Set (NS)

**Output Types** - Discrete-State (DS): true or false

**Properties**

   Comparison Operation (operation)
       Operation for comparing operands. le (less than or equal), ne (not equal),
       ge (greater than or equal), gt (greater than), lt (less than), eq (equal)

   .. include:: includes/processors/significant_keys.rst

   .. include:: includes/processors/enable_streaming.rst

Comparison Example
------------------

.. code-block:: none

    significant_keys: ["system_id", "interface"]
    operation: "ge"

Input A:

.. code-block:: none

  [system_id=leaf1,interface=eth0,counter_type=tx_bytes]: 34
  [system_id=leaf1,interface=eth1,counter_type=tx_bytes]: 58

Input B:

.. code-block:: none

  [system_id=leaf1,interface=eth0,counter_type=rx_bytes]: 15
  [system_id=leaf1,interface=eth1,counter_type=rx_bytes]: 73

Output (Discrete-State-Set):

.. code-block:: none

  [system_id=leaf1,interface=eth0]: "true"
  [system_id=leaf1,interface=eth1]: "false"
