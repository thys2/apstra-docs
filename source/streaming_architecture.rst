======================
Streaming Architecture
======================
AOS can be configured to generate Google Protocol Buffer (protobuf) streams for
counter data (perfmon), alerts, and events. You can choose from the many
open-source projects, or develop your own solutions to capture, store and inspect
the protobuf data. Apstra has developed a project available on GitHub called
`AOSOM-Streaming <https://github.com/Apstra/aosom-streaming>`_ to demonstrate how
this can be achieved using several open-source components. The AOSOM-Streaming
project is meant to help customers understand how they can consume the AOS protobuf
stream. It is for demonstration purposes only, except for the AOS Telegraf input
plugin. This plugin is fully supported by Apstra for customers to use as part of
their streaming telemetry solution.

.. toctree::
   :maxdepth: 2

   aosom_streaming
