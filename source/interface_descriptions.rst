Interface Descriptions
======================

Besides main parameters of network interfaces like name, speed and port mode,
AOS also configures a description for physical interfaces and aggregated logical
interfaces (so called port channels). Interface description is automatically
generated if the following conditions are met:

    1. The interface is connected to a peer.
    2. The interface belongs to leaf, spine or L3 server.
    3. The peer interface belongs to leaf, spine, L3 server, external router
       or L2 server with virtual network endpoint on this server.

The generated description has the form
``<facing_|to.><peer-device-label>[:peer-interface-name]``. Examples:

* facing_spine2:Ethernet1/2
* to.server1:eth0
* facing_external-router
* to.server2

The prefix of the name is ``facing_`` if the peer is leaf, spine or external router.
The prefix is ``to.`` in case peer device is an L2 or L3 server. The peer interface
name part is present only when the peer device is controlled by AOS.

AOS REST API - Interface descriptions
-------------------------------------

The AOS API is able to change the auto-generated interface description. However,
there is no such functionality in the AOS UI.

The interface description may contain ASCII characters with codes 33-126 and spaces,
except "?", which is interpreted as a command-completion. The description length
is limited to 240 characters, which is the longest possible length
across supported switch models.

Interfaces are stored internally as graph nodes with certain set of properties.
Description is one of these properties. To modify the description,
use the generic API to interact with graph nodes.

API - Obtaining interface configuration
_______________________________________
To obtain interface configuration, send GET request to
`https://aos-server/api/blueprints/{blueprint-id}/nodes/{interface-node-id}
<https://aos-server/api/blueprints/{blueprint-id}/nodes/{interface-node-id}>`_.

Request:

.. code-block:: text

    curl -H "AuthToken: EXAMPLE" \
         http://aos-server:8888/api/blueprints/id-1/nodes/interface-id-1

Response:

.. code-block:: json

    {
        "description": "facing_dkl-2-leaf:Ethernet1/2",
        "mlag_id": null,
        "tags": null,
        "if_name": "swp2",
        "label": null,
        "port_channel_id": null,
        "ipv4_addr": "203.0.113.10/31",
        "mode": null,
        "if_type": "ip",
        "type": "interface",
        "id": "interface-id-1",
        "protocols": "ebgp"
    }

API - Creating or modifying interface description
_________________________________________________
To create or modify interface description, send PATCH request to
`https://aos-server/api/blueprints/{blueprint-id}/nodes/{interface-node-id}
<https://aos-server/api/blueprints/{blueprint-id}/nodes/{interface-node-id}>`_
with a valid JSON. The JSON should contain the "description" field
with a valid data.

Request:

.. code-block:: text

    curl -X PATCH -H "AuthToken: EXAMPLE" \
         -d '{"description": "New description I want!"}'
         http://aos-server:8888/api/blueprints/id-1/nodes/interface-id-1

Response:

.. code-block:: json

    {
        "description": "New description I want!",
        "mlag_id": null,
        "tags": null,
        "if_name": null,
        "label": null,
        "port_channel_id": null,
        "ipv4_addr": null,
        "mode": null,
        "if_type": "ip",
        "type": "interface",
        "id": "interface-id-1",
        "protocols": "ebgp"
    }

API - Deleting interface description
____________________________________
To delete custom interface description and get back to automatic description
generation, set the description to empty value.

Request:

.. code-block:: text

    curl -X PATCH -H "AuthToken: EXAMPLE" \
         -d '{"description": ""}'
         http://aos-server:8888/api/blueprints/id-1/nodes/interface-id-1

Response:

.. code-block:: json

    {
        "description": "",
        "mlag_id": null,
        "tags": null,
        "if_name": null,
        "label": null,
        "port_channel_id": null,
        "ipv4_addr": null,
        "mode": null,
        "if_type": "ip",
        "type": "interface",
        "id": "interface-id-1",
        "protocols": "ebgp"
    }

Subsequent GET request will show that the description was automatically generated.

Request:

.. code-block:: text

    curl -H "AuthToken: EXAMPLE" \
         http://aos-server:8888/api/blueprints/id-1/nodes/interface-id-1

Response:

.. code-block:: json

    {
        "description": "facing_dkl-2-leaf:Ethernet1/2",
        "mlag_id": null,
        "tags": null,
        "if_name": "swp2",
        "label": null,
        "port_channel_id": null,
        "ipv4_addr": "203.0.113.10/31",
        "mode": null,
        "if_type": "ip",
        "type": "interface",
        "id": "interface-id-1",
        "protocols": "ebgp"
    }
