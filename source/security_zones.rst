==============
Security Zones
==============
Virtual Networks can now be associated with "Security Zones". A Security Zone is an
L3 domain, and is the unit of tenancy in multi-tenant networks. You may create
Security Zones for tenants so that their IP traffic is isolated from one another,
thus enabling tenants to re-use IP subnets. Within a Security Zone, you may create
one or more virtual networks. Therefore, a tenant can stretch their L2 applications
across multiple racks within its Security Zone.

For VN with Layer3 SVI, the SVI is associated with a Virtual Routing and
Forwarding (VRF) instance for each Security Zone isolating the VN SVI from other VN
SVI in other Security Zones.

In addition to being in its own VRF, each Security Zone can optionally have its own
DHCP Relay Server(s) and External Router connections.

If using multiple Security Zones, External Router connections must be from leaf
switches in the AOS Fabric. If routing between Security Zones is desired, this must
be done by the External Routers.

The number of Security Zones is limited by the Network Devices being used.

Default Security Zone
---------------------
All Blueprints will have a **Default security zone** associated with them. All SVIs
configured for Virtual Networks in this zone are in the "default" VRF. This is
the same VRF used for the "underlay" or fabric network routing between network
devices.

Configuring Security Zone DHCP Relay Servers
--------------------------------------------
#. From the **Virtual** view of your **Staged** Blueprint, click
   **Security Zones**.
#. Click the name corresponding to the Security Zone to edit.
#. From the editing screen of your Security Zone, click the **Assign DHCP
   Servers** button (top-right) to see the dialog for adding addresses.

   .. image:: static/images/blueprint_virtual_tasks/securityzones_default_dhcp_310.png

#. Enter the IPv4 or Ipv6 for one or more DHCP Servers used for DHCP Relay.

   .. image:: static/images/blueprint_virtual_tasks/securityzones_default_dhcp_2_310.png

#. Click **Update** to add the DHCP Relay Server(s) to the Security Zone.

The same process is used to assign DHCP Relay Servers to additional security.

Creating Security Zones
-----------------------
To create multiple Security Zones beyond the Default Security Zone, you
must create a Blueprint from a Template that has **MP-EBGP EVPN** Overlay Control
Protocol enabled. Configuring a Template with **MP-EBGP EVPN**
Overlay Control Protocol is described in :doc:`Templates <templates>`.

Make sure that External Routers, if any, are connected to racks. AOS does not
support **MP-EBGP EVPN** Overlay Control Protocol where External Routers are
connected to Spines.

#. From the **Virtual** view of your **Staged** Blueprint, click
   **Security Zones**.
#. Click **Create Security Zone** to see the dialog for creating a
   Security Zone.

   .. image:: static/images/blueprint_virtual_tasks/300_security-zone-1.png

#. Fill in the required information.

   VRF Name
      64 characters or fewer. Underscore, dash and alphanumeric characters only

   VLAN ID
      VLAN ID associated with the Security Zone. This can either be explicitly
      assigned or left blank to have AOS auto-assign from a static pool with the
      range of 2-4094. This VLAN ID is used for VLAN tagged Layer3 links on the
      External Router connections.

   VNI
      VxLAN VNI associated with the Security Zone. This can be explicitly assigned
      or you may leave this blank and let AOS auto-assign a VNID from a VNID pool
      created in the **Resources** tab.

   Routing Policies
      The default routing policies for the Security Zone. These may be overridden
      when configuring External Router Links.

      Import Policies
         Import policies are specified to import only the default, "All Routes" or
         "Extra Only" routes, defined under "Extra Import Routes" and "Extra Export
         Routes". These options are additive; if "Default" is defined, extra routes
         are permitted in addition to the default route:

         Default
            Only the default BGP route (0.0.0.0/0, ::/0) is accepted.

         All
            Any BGP route is accepted.

         Extra Only
            Accept additional defined routes.

      Extra Import Routes**
         User defined import routes are used in addition to any routes generated
         by the import policies. Prefixes specified here are additive to the import
         policy, unless the "Import Policy" is set to "Extra Only", in which case only
         these routes are imported.

         .. image:: static/images/blueprint_virtual_tasks/300_security-zone-2.png


         Prefix
            Specify the IPv4 or IPv6 network address **Prefix** specified in the
            form of network/prefixlen. IPv4 or IPv6 networks without a prefixlen
            are assumed to be host routes (/32, /128).

         GE Mask and LE Mask
             **GE mask** matches less-specific prefixes from a parent prefix, up
             from the GE mask to the prefix length of the route. The range is
             0-32 for IPv4 and 0-128 for IPv6. If not specified, implies the
             prefix-list entry should be an exact match. This option can be
             optionally used in combination with the **LE mask**. GE mask must
             be longer than the subnet prefix length. If the LE mask and GE mask
             are both specified, then the LE mask must be greater than
             the GE mask.

         .. image:: static/images/blueprint_virtual_tasks/300_security-zone-10.png

      Export Policies
         Defines the default Export Policies for the Security Zone.

         Spine Leaf Links
            Exports all spine-leaf (fabric) links within a VRF. EVPN Security Zones
            do not have spine-leaf addressing, so this generated list may be empty.
            For Security Zones at the type Virtual L3 Fabric, subinterfaces between
            spine-leaf will be included.

         L3 Edge Server Links
            Exports all leaf to L3 server links within a VRF. This is an empty
            list on a L2 based Blueprint.

         L2 Edge Subnets
            Exports all Virtual Networks (VLANs) that have L3 addresses within a VRF.

         Loopbacks
            Exports all loopbacks within a VRF across spine, leaf and L3 servers.

         Extra Export Routes
            User defined export routes will be used in addition to any other routes
            specified in export policies. These policies are additive. To advertise
            only extra routes, unselect all export policies, and only the extra
            policies specified here will be advertised.

            Routes are specified in the same manner as **Prefix**, **GE mask**,
            and **LE mask**.

         ggregate Prefixes
            BGP aggregate routes to be imported into a VRF on all border switches.
            This option can only be set on routing policies associated with Security
            Zones, and cannot be set on a per Connectivity Point policies. The
            aggregated routes are set to all external router peers in a VRF.

           Aggregate Prefixes must be specified with an exact network/prefixlen.

After creating the Security Zone, you will need to assign necessary network
resources.

Security Zone Loopback IPs
--------------------------

Each Leaf Network Device needs an additional Loopback IP for each Security
Zone. Assign an IP Pool for these.

.. image:: static/images/blueprint_virtual_tasks/blueprint_virtual_tasks_1_310.png

Starting with AOS 3.0, if IPv6 is enabled for the Blueprint, you must assign
IPv6 Resource Pools to the Security Zone.

.. image:: static/images/blueprint_virtual_tasks/300_security-zone-7.png

Security Zone EVPN L3 VNIs
--------------------------

If you did not assign an explicit VNI when creating the Security Zone, you will
need to assign a VNI Pool to allocate one from.

.. image:: static/images/virtual_networks/securityzones_create_vni_22.png

Security Zone External Connectivity Points
------------------------------------------
As of AOS 3.0, If you are using External Routers in an EVPN Blueprint, you
must add External Connectivity Points (ECP) in the Security Zone.

#. From the **Virtual** view of your **Staged** Blueprint, click
   **Security Zones**.
#. Click the VRF name corresponding to the Security Zone to edit.

.. image:: static/images/blueprint_virtual_tasks/blueprint_virtual_tasks_1_310.png

#. ECP appears under the **External Connectivity Points** section. Click on
   the **Add Connectivity Point** to add a new ECP.

#. Fill in the required information.

   Connectivity Type
      Define whether the ECP is L2 or L3. This must match the type defined when
      you :ref:`Assign External Routers to a Blueprint <staging_external_routers>`.

   Peering Type
      Define whether the ECP uses EBGP multihop Loopback peering or Interface
      peering.

   Enable IPv6
      Starting with AOS 3.0, if IPv6 is enabled for the Blueprint, you will
      be asked if IPv6 needs to be enabled for the ECP.

      .. image:: static/images/blueprint_virtual_tasks/300_security-zone-8.png

   VLAN ID
      The tagged VLAN ID when ECP is a L2 SVI interface.

   IPv4 Subnet
      The IPv4 subnet used for the ECP. This must be assigned and cannot be
      assigned via a Resource Pool.

   IPv6 Subnet
      If IPv6 is enabled for the ECP, this defines the IPv6 subnet used for the
      ECP. This must be assigned and cannot be assigned via a Resource Pool.

   Links
      Select the links to use for the ECP.

   .. image:: static/images/blueprint_virtual_tasks/300_security-zone-5.png

   Resources
      For L3 ECP, AOS automatically assigns IP for each ECP device from IPv4
      Subnet. For L2 ECP, you must manually assign IP addresses for each node.
      If IPv6 is enabled for the ECP, enter the IPv6 addresses for each node.

   .. image:: static/images/blueprint_virtual_tasks/300_security-zone-9.png

   Routing Policies / Enable Routing Policies Overrides
      If checked, you can override the default Routing Policies defined when
      creating the Security Zone. Refer to the `Add Security Zones` section
      above for more information.

   .. image:: static/images/blueprint_virtual_tasks/300_security-zone-6.png

Security Zone External Connectivity Points Scenarios
````````````````````````````````````````````````````

**Shared ECP**

In this scenario, a shared upstream gateway (e.g. Active/Passive Firewalls) is
connected to a leaf or an MLAG leaf-pair. The default or any new security zone is
connected to two external firewalls using a single IP connectivity point (the
shared Virtual IP address). Each security zone uses a different IP connectivity
point (with a different SVI interface/VLAN ID and different VIP). On each border
leaf, 1 EBGP session is established between its SVI and the shared virtual IP of
the firewall pair.

Custom export/import policies per Security Zone can be defined:

- Enable/Disable export of l2edge_subnets
- Import specific prefix-list (new)
- Export aggregated subnets (new)

.. image:: static/images/blueprint_virtual_tasks/300_security-zone-ecp-1.png

To configure this in AOS when adding the ECP, you must add both "links"
to the External Connectivity Point.

.. image:: static/images/blueprint_virtual_tasks/300_security-zone-ecp-2.png

Under "Resources" there will be an "IPv4 Address" field for each link node. Place
the same "VIP" IP address in the link node "IPv4 Address" field.

.. image:: static/images/blueprint_virtual_tasks/300_security-zone-ecp-3.png

When you view the "Details" of the ECP, you will see the (2) link nodes as
"shared_endpoint" along with the External EBGP sessions.

.. image:: static/images/blueprint_virtual_tasks/300_security-zone-ecp-4.png

Deleting Security Zones
-----------------------
If any Virtual Networks are associated with the Security Zone, they will be
deleted as well.

#. From the list view (**Blueprint /** *your Blueprint*
   **/Staged / Virtual / Security Zones**), click the name corresponding
   to the Security Zone to delete.

   .. image:: static/images/blueprint_virtual_tasks/blueprint_virtual_tasks_3_310.png

#. Click the **Delete** button (top-right) to see the dialog for deleting
   a Security Zone.

   .. image:: static/images/blueprint_virtual_tasks/blueprint_virtual_tasks_4_310.png

#. Click **Confirm** to delete the Security Zone.
