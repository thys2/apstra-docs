===============
Role Management
===============

Role Management Overview
========================
Users with the **administrator** role can create, edit and delete user roles
(which are assigned to :doc:`user profiles <managing_users>`). These roles can
also be :ref:`mapped <map_user_roles>` to external groups used by
authentication providers such as LDAP, Active Directory, TACACS+, and RADIUS.

With Enhanced Role Based Access Control introduced in AOS version 3.3,
AOS administrators can now create blueprint-specific roles with very specific
privileges allowing limited control to associated users. This allows AOS
administrators to create more hierarchical roles & protect against accidental
changes to the network.

The blueprint locking feature, introduced in AOS version 3.3, prevents
restricted users from making changes that effectively are not permitted.
In particular, a restricted user should not be able to commit changes made
by another user.

A blueprint with no uncommitted changes is considered “unlocked”.

.. image:: static/images/blueprints/bp_locking_00.png

Let’s say you have a permission to create/update/delete virtual networks, and
another user has made uncommitted changes to the blueprint. The blueprint is
considered "locked", and you will not be able to create/update/delete virtual
networks until the changes are committed or reverted by the "locking
user" who made the uncommitted changes, unless you are the locking user.

.. image:: static/images/blueprints/bp_locking_2.png

An admin user who has "Write/Commit Blueprints" permissions can make any
changes to, apply changes for, revert changes for any blueprint.

.. image:: static/images/blueprints/bp_locking_3.png

AOS ships with four predefined user roles (administrator, device_ztp, user,
viewer). These user roles cannot be modified. User roles include the following
details and options:

* Name
* Type - global permission or per-blueprint permissions
* Global Permissions (read, write, commit, as applicable)

  * Blueprints - blueprints
  * Devices - device profiles, agents, devices
  * Design - configlets, templates, rack types, logical devices, property sets,
    interface maps
  * Resources - IP pools, external routers, IPv6 pools, ASN pools, VNI pools
  * AAA - sysdb data, AAA providers, roles, audit config, audit events, users
  * Other - streaming, AOS metric logs, ztp, port setting schema,
    AOS cluster management, virtual infra manager, telemetry service registry,
    port aliases

* Per-Blueprint Permissions (new in AOS version 3.3.0)

  * Which Blueprints? All or by ID
  * Permissions

    * Read blueprint
    * Make any changes to staging blueprint (includes managing VNs and their
      endpoints)
    * Commit changes
    * Manage virtual networks (includes managing VN endpoints)
    * Manage virtual network endpoints

From the AOS web interface, navigate to **Platform / User Management / Roles**.

.. image:: static/images/user_management/user_roles_330.png

.. sidebar:: Cloning User Roles

    Instead of entering all details for a new role, you can clone an existing one,
    give it a new name and customize it.

Creating User Role
==================
#. From the AOS web interface, navigate to **Platform / User Management / Roles**,
   then click **Create Role**.
#. Enter a name and description, then select permission type and one or more
   permissions.
#. Click **Create** to create the role and return to the list view.

Use Case 1: Read, Write and Commit Specified Blueprints
-------------------------------------------------------
To create a role that gives a user permission to read, write, and commit to
specified blueprints, select **Per-Blueprint Permissions**, select one or more
blueprint IDs (or **All** for all blueprints), then toggle on **Read blueprint**,
**Make any change to staging blueprint**, and **Commit changes**. The changes
that can be made include **Manage virtual networks** and
**Manage virtual network endpoints** even though those permissions may or may not
be toggled on.

.. image:: static/images/user_management/role_read_write_commit_330.png

Use Case 2: Manage VN Endpoints on Specified Blueprints
-------------------------------------------------------
To create a role that gives a user permission to only manage virtual network
endpoints on specified blueprints, select **Per-Blueprint Permissions**, select
one or more blueprint IDs (or **All** for all blueprints), then toggle on
**Manage virtual network endpoints**.

.. image:: static/images/user_management/role_manage_vn_endpoint_330.png

.. _vn_role:

Use Case 3: Create Virtual Networks (not Including Allocating Resources)
------------------------------------------------------------------------
To create a role that gives a user permission to only create virtual networks,
select **Per-Blueprint Permissions**, select one or more blueprint IDs (or toggle
on **All** for all blueprints), then toggle on **Read Blueprint**,
**Commit changes**, **Manage virtual networks**, and
**Manage virtual network endpoints**. By not selecting **Make any change to
staging blueprint** you are limiting the changes that can be made to virtual
networks only.

.. image:: static/images/user_management/role_create_vn_330.png

.. _vn_role3a:

Use Case 3A: Create Virtual Networks and Allocate Resources
-----------------------------------------------------------
For the user with the role in use case 3 above to be able to allocate resources to
the virtual networks that they create, they must also be assigned two additional
roles: one with global permissions to read and write resources (see use case 4
below) and another one with per-blueprint permissions to
**Make any change to staging blueprint**, effectively giving them access to make
other changes in addition to making changes to virtual networks. Of course, this
second one would not be needed if the role for creating virtual networks also
enabled **Make any change to staging blueprints**.

Use Case 4: Read and Write Resources for All Blueprints
-------------------------------------------------------
To create a role that gives a user permission to read and write resources on any
blueprint, select **Global Permissions**, then toggle on **Resources** for
**Read** and **Write**, which toggles on all resource types.

.. image:: static/images/user_management/role_read_write_resources_330.png

Editing User Role
=================
The four predefined user roles (administrator, device_ztp, user, viewer)
cannot be modified.

#. Either from the list view (Platform / User Management / Roles) or the details
   view, click the **Edit** button for the user role.
#. Change permissions, as applicable.
#. Click **Update** to update the role and return to the list view.

.. sidebar:: AOS REST API and User Roles

    In addition to using the AOS web interface to manage user roles (as
    described below) AOS REST API can also be used.
    Navigate to **Platform / Developers** for documentation and tools.
    See the **aaa** section for role-related APIs.

Deleting User Role
==================
The four predefined user roles (administrator, device_ztp, user, viewer)
cannot be deleted.

#. Either from the list view (Platform / User Management / Roles) or the details
   view, click the **Delete** button for the user role.
#. Click **Delete** to delete the role and return to the list view.
