########################
L3 Server Configurations
########################

Introduction
========================
Users will need to update `/etc/aos/aos.conf` file with the desired device_profile_id
based on the server device facts which includes server version and supported
interface configuration. The management interface on the server should also be
mentioned in the config file.

Servers running ubuntu 16.04 or 18.04 will need special handling depending on
predictable interface naming scheme feature. With this feature, the interface
names on the server would be lot different than the traditional names we are
used to seeing(like eth0, eth1, eth2 ...).

AOS provides a base device profile matching the output from ifconfig from an
Ubuntu 16.04 (Xenial) and Ubuntu 18.04 (Bionic) servers.

.. image:: static/images/l3_servers/xenial_interfaces.png

Below is a snippet of the configuration file with these values updated for a
Ubuntu 16.04 (Xenial) server based on the above ifconfig dump. (note the fields
'interface' and 'device_profile_id').

.. code-block:: text
    :caption: ``/etc/aos/aos.conf``
    :emphasize-lines: 5-8,52-55

    [controller]
    # <metadb> provides directory service for AOS. It must be configured properly
    # for a device to connect to AOS controller.
    metadb = tbt://172.20.63.3:29731
    # <interface> is used to specify the management interface.This is currently
    # being used only on server devices and the AOS agent on the server device will
    # not come up unless this is specified.
    interface = eno1
    # Use <web> to specify AOS web server IP address or name. This is used by
    # device to make REST API calls to AOS controller. It is assumed that AOS web
    # server is running on the same host as metadb if this option is not specified
    web =


    [service]
    # AOS device agent by default starts in "telemetry-only" mode.Set following
    # variable to 1 if you want AOS agent to manage the configuration of your
    # device.
    enable_configuration_service = 1
    # When managing device configuration AOS agent will restore backup config if it
    # fails to connect to AOS controller in <backup_config_restoration_timeout>,
    # specified as <hh:mm:ss>. Set it to 00:00:00 to disable backup restoration
    backup_config_restoration_timeout = 00:00:00


    [logrotate]

    # AOS has builtin log rotate functionality. You can disable it by setting
    # <enable_log_rotate> to 0 if you want to use linux logrotate utility to manage
    # your log files. AOS agent reopens log file on SIGHUP
    enable_log_rotate = 1
    # Log file will be rotated when its size exceeds <max_file_size>
    max_file_size = 1M
    # The most recent <max_kept_backups> rotated log files will be saved. Older
    # ones will be removed. Specify 0 to not save rotated log files, i.e. the log
    # file will be removed as soon as its size exceeds limit.
    max_kept_backups = 5
    # Interval, specified as <hh:mm:ss>, at which log files are checked for
    # rotation.
    check_interval = 1:00:00


    [device_info]

    # <model> is used to specify the device's hardware model to be reported to AOS
    # device manager. This is only used by servers, so can be ignored for non-
    # server devices such as switches. By default a server reports "Generic Model"
    # which matches a particular HCL entry's selector::model value in AOS. Specify
    # another model for the server to be classified as a different HCL entry.
    model = Generic Model

    # <device_profile_id> is used to specify the device profile to be associated to
    # the device. Selector in the specified device profile should match the
    # reported device facts.
    device_profile_id = Generic_Server_1RU_4x10G_Xenial

AOS provides pre-built device profiles for servers with different interface
configurations. The complete list of device profiles pre-built in AOS can be accessed
from the GUI at `https://<AOS-vm-ip>/#/devices/device-profiles`. The same applies to
interface maps as well, from GUI at `https://<AOS-vm-ip>/#/design/interface-maps`.
The below picture illustrates a subset of device profiles available to choose from
the GUI.

.. image:: static/images/l3_servers/device-profile-list.png

Following example lists 4 different types of device_profile_ids based on server
type for a 4x1G interface configuration.

.. list-table:: sample device_profile_id and interface_map for 4x1G
   :widths: 30 25 50 50
   :header-rows: 1

   * - OS_Type
     - Version
     - Device_profile_id
     - Interface_map
   * - Centos
     - 7.5
     - Generic_Server_1RU_4x1G_Centos
     - Generic_Server_1RU_4x1G_Centos__AOS-4x1-1
   * - \*Ubuntu
     - 14.04
     - Generic_Server_1RU_4x1G
     - Generic_Server_1RU_4x1G__AOS-4x1-1
   * - \**Ubuntu
     - 16.04
     - Generic_Server_1RU_4x1G_Xenial
     - Generic_Server_1RU_4x1G_Xenial__AOS-4x1-1
   * - \***Ubuntu
     - 18.04
     - Generic_Server_1RU_4x1G_Bionic
     - Generic_Server_1RU_4x1G_Bionic__AOS-4x1-1

.. code-block:: text

    * Choose this device profile if running ubuntu 14.04 Trust which is the default
    * Choose this device profile if running ubuntu 16.04 Xenial or
      18.04 Bionic with predictable interface naming scheme disabled
    ** Choose this device profile if running ubuntu 16.04 Xenial with predictable
     interface naming schema enabled and interface naming scheme matches with the
     above image's ifconfig dump
    *** Choose this device profile if running ubuntu 18.04 Bionic with predictable
     interface naming schema enabled and interface naming scheme matches with the
     above image's ifconfig dump

Users are encouraged to create new device profiles and interface_maps from the GUI
based on the interface names available on your servers running ubuntu 16.04 or 18.04.
AOS supports different server interface map configuration which includes - 1x10G,
1x1G,1x25G, 1x40G, 2x10G, 2x1G, 2x40G, 4x10G, 4x1G.

.. attention::
    Once the aos.conf file has been modified with the desired fields and values, the
    AOS device agents need to be restarted for the new values to be reflected in the
    AOS controller as well as in the GUI.

Managed Devices
===============
Users can choose the servers from the managed devices option in the GUI and choose a
specific server of interest and view the device details as shown below. In this
example lets pick the second device in the list which is Ubuntu Xenial 16.04.


.. image:: static/images/l3_servers/managed-device.png

The user config section shows a default device profile that AOS picks which need not
match exactly with the actual server capabilities. The user can edit this and choose
a device profile that suitably matches their server configurations based on the
guidelines in the aforementioned table "sample device_profile_id" and "interface_map
for 4x1G".

.. image:: static/images/l3_servers/user-config.png

.. image:: static/images/l3_servers/facts.png

The section following the user config is the device facts which AOS device agents
have reported. If the `/etc/aos/aos.conf` file on the device has been modified with
the appropriate management interface name and device_profile_id (as highlighted in
the Introdction section), then the updated values will show up here.

Users can click on the edit button next to the User config section to edit the fields
as shown below.

.. image:: static/images/l3_servers/edit-user-config.png

.. image:: static/images/l3_servers/edit-user-config-2.png

.. attention::
    It is at a minimum required to make sure the correct management interface name is
    mentioned in the ```/etc/aos/aos.conf`` file on the server and the User config
    section has the correct device_profile_id (default value showing in AOS GUI may
    not match).

Cloning device-profiles
=======================

For users who want to create new device profiles from the GUI (especially for Ubuntu
Xenial and Bionic servers where the target platform has completely different
interface names), can follow the workflow as illustrated below.

For this discussion lets pick 4x10G Xenial server which the user wishes to change to
cater to their needs of interface naming. Once the interface names are identified (as
mentioned in the Introduction section), the user can choose a
``Generic_Server_1RU_4x10G_Xenial`` device profile from the pre-built list and choose
to clone it as shown below.

.. image:: static/images/l3_servers/clone.png

The user can rename the new device profile to a desired naming convention, lets say
`Generic_Server_1RU_4x10G_Xenial_2`.

.. image:: static/images/l3_servers/rename.png

Now the user can edit the values of the Ports by clicking on the ports panel and then
clicking on the individual port numbers (colored violet). The user can now click on
the edit button to rename the interface name to a desired value, lets say ``enp0s3``,
and click on the save button. This step can be repeated for all 4 ports available for
this 4x10G device profile. The same procedure applies to other configurations as
well.

.. image:: static/images/l3_servers/rename-port.png

The newly created device_profile will show up in the list of device profiles in the
GUI at `https://<AOS-vm-ip>/#/devices/device-profiles`.
