=================
REST API Explorer
=================
With Apstra's REST API explorer, you can browse and search for specific
AOS REST API endpoints relevant to both the platform and reference designs.

From the AOS web interface, navigate to
**Platform / Developers / REST API Explorer** to see the screen as shown below.
The left column contains a list of API categories from which you can browse.
You can also search for a specific endpoint by entering a query in the **Quick
Search** field. The details view of an endpoint includes information about the
URL, method, summary, parameters and responses. The example below shows the model
for checking provider settings by login with username and password.

.. image:: static/images/api/rest_api_explorer1_321.png
