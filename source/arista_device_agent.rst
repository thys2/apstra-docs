===================
Arista Device Agent
===================

Initial Arista EOS Configuration
================================

.. note::
    Please see :doc:`Apstra ZTP Documentation <apstra_ztp>` for an
    option for automatically booting and installing the AOS Device agent
    and prerequisite configuration on switches.  This option is simpler
    and easier to support at scale than manually installing agents.

Disabling ZTP
-------------

If the switch is to be provisioned without ZTP (ZTP Disabled), we need to ensure that
the ZTP process is disabled before proceeding. After logging into the switch for
the first time, run the command `zerotouch disable`.

This will require a device reload.

.. code-block:: prompt
    :caption: Disabling ZTP

    localhost login: admin
    localhost> zerotouch disable

Configuring AAA and network-admin User
--------------------------------------

In order to install or manage the AOS System Agent from AOS, there must be a
network-admin user configured on the device with a known password.

.. code-block:: prompt

    aaa authorization exec default local
    username admin privilege 15 role network-admin secret <admin-password>


Configuring IP address and Management VRF
-----------------------------------------

Apstra AOS Device Agent makes use of the 'management' VRF.  Accordingly, any
management interfaces must be moved from the default (none) VRF into the
Management VRF.

.. note::

    If the user is installing a on-box AOS Device Agent from AOS, the creation
    of the Management VRF can be skipped. The AOS Device Agent installer will
    automatically create the Management VRF if needed.

The device agent makes use of the `Management1` interface by default. On modular
chassis such as the Arista 7504 or 7508, the management interface is Management0
- please check your platform if management interfaces appear as "Management1" or
"Management1/1, Management1/2, and Management0".  Management0 is a shared mgmt
interface between both supervisors

If you are logging into this switch remotely, make sure you have an out-of-band
connection prior to issuing the ``vrf forwarding management`` command under an
interface.  This will immediately remove the IP address from the NIC and potentially
lock you out of your system.

.. code-block:: prompt

    vrf definition management
     rd 100:100
    interface management1
     vrf forwarding management
     ip address <address>/<cidr>
    ip route vrf management 0.0.0.0/0 <management-default-gateway>

Configuring DNS for EOS
-----------------------

AOS server discovery supports DNS-based discovery if the user is manually
configuring the the AOS Device System Agent. By default, the ``aos-config``
file will look for `tbt://aos-server:29731` - accordingly, we can use a DNS
nameserver to resolve `aos-server`.

.. code-block:: prompt
    :caption: DNS for EOS

    ip name-server vrf management <dns-server-ip>
    ip name-server vrf management <dns-server-ip>

Configure HTTP API for EOS
--------------------------

.. note::

    If the user is installing a on-box AOS Device Agent from AOS, the creation
    of the configuration of HTTP API can be skipped. The AOS Device Agent
    installer will automatically configure the HTTP API if needed.

AOS makes use of the HTTP API and Unix sockets to connect to the EOS API for
configuration rendering and telemetry commands. The API must be made available
for both the default route and the management VRF.

Additionally, the AOS agent connects using the unix-socket locally on the
filesystem.

.. code-block:: prompt
    :caption: Configure HTTP API for AOS connectivity

    management api http-commands
     protocol unix-socket
     no shutdown
     vrf management
      no shutdown

Configure multi-agent for EVPN
------------------------------

For Arista devices running EOS 4.22, the ``service routing protocols model
multi-agent`` configuration command is required to run EVPN. The configuration
also requires a device reboot to apply the configuration.

.. code-block:: prompt

    localhost(config)#service routing protocols model multi-agent
    ! Change will take effect only after switch reboot
    localhost(config)#

Apstra recommends that this configuration is added to Arista device prior to the
installation of the AOS Device System Agent. This will ensure the multi-agent
configuration is added to the Device Pristine Configuration.

After configuring this, save the device configuration and reload the device.

    .. code-block:: prompt

        localhost(config)#wr mem
        Copy completed successfully.
        localhost(config)#reload now

        Broadcast message from root@localhost (Mon Sep 21 20:25:03 2020):

        The system is going down for reboot NOW!

Decommission device
===================

Find the device under Devices, select it, and press the Decommission button in the
UI. This will allow us to delete the device.

Delete the device from the Devices list
Once the device is decommissioned and ‘available’ we can delete it.

Remove the AOS package from EOS
===============================

Uninstall the AOS Device agent package using the EOS CLI
--------------------------------------------------------

Erasing the startup-configuration does not delete the installed EOS Extension files.
We must explicitly remove the AOS device agent.

These steps must be done in order.

.. code-block:: prompt

    localhost#no extension aos-device-agent-2.0.0-0.1.210.i386.rpm
    localhost#delete extensions:no extension aos-device-agent-2.0.0-0.1.210.i386.rpm
    localhost#copy boot-extensions installed-extensions


Uninstall the AOS Device agent using bash
-----------------------------------------
Using the Bash CLI requires the administrator to edit /mnt/flash/boot-extensions to
remove the reference to the extension, and deleting the extension from
``/mnt/flash/.extensions/aos-device-agent.i386.rpm`` - This filename will
be unique depending on the version that AOS was installed

.. code-block:: prompt
    :caption: View the AOS Agent extension installation details

    localhost#dir /all flash:.extensions/
    Directory of flash:/.extensions

           -rwx     1798948           May 31 02:11  EosSdk-1.8.1-4.16.6M.i686.rpm
           -rwx       36199           May 31 02:25  aos-device-agent-1.2.0-0.1.137.i386.rpm
    localhost#more flash:boot-extensions
    EosSdk-1.8.1-4.16.6M.i686.rpm
    aos-device-agent-1.2.0-0.1.137.i386.rpm

.. code-block:: prompt
    :caption: Remove the AOS device agents

    [admin@localhost ~]$ vi /mnt/flash/boot-extensions

Remove remaining AOS data from system
-------------------------------------
AOS modifies and retains information on the filesystem from a few locations, these
can be cleaned up by hand by running the below:

.. warning::
    If AOS files are not removed (especially /mnt/flash/.aos/ which includes
    checkpoint files), the next time AOS is installed, the configuration will be
    replaced by whatever configuration AOS rendered last -- including quarantine
    configuration, which could shut down all interfaces.

    Be very sure to remove /mnt/flash/.aos/ when removing AOS data.

.. code-block:: prompt
    :caption: Remove AOS data from the filesystem

    root@Arista:~# rm -rf /mnt/flash/aos*
    root@Arista:~# rm -rf /mnt/flash/.aos*
    root@Arista:~# rm -rf /var/log/aos
    root@Arista:~# rm -rf /.aos

Save the config file
--------------------
In order for the extension to be removed from bootup, we need to ``wr mem`` to
ensure the extension no longer appears in boot-extensions, or else AOS may start
the agent up again if the RPM is still installed in available extensions.

Restart the system
==================
After AOS is uninstalled we need to reboot.

.. note::
    Say 'yes' to configuration saving, this will ensure the extension is
    removed from boot extensions.

.. code-block:: prompt

    localhost#reload
    System configuration has been modified. Save? [yes/no/cancel/diff]:yes
    Proceed with reload? [confirm]

    Broadcast message from root@localhost (Thu Oct 19 02:03:28 2017):

    The system is going down for reboot NOW!

The configuration running on the switch at the time of removing the AOS agent will
not be modified or changed in any way, it is completely safe to remove AOS without
disrupting the network.

Arista Device Agent Manual Installation
=======================================

This describes the process of **manual** Agent Installation on Arista EOS
devices. For the recommended method using the UI, refer to
:doc:`Device Agents <device_agents>`

.. include:: includes/device_agents_warning.rst

Download Agent Installer
------------------------

The AOS Device agent is available over HTTPs from the AOS Server from the base URL
https://aos-server/device_agent_images/aos_device_agent.run

.. code-block:: prompt
   :caption: Downloading the AOS Agent

    spine1#routing-context vrf management
    spine1(vrf:management)#copy https://192.168.25.250/device_agent_images/aos_device_agent.run flash:
    Copy completed successfully.

Install Arista Device Agent
---------------------------

.. code-block:: prompt
   :caption: Run the AOS Device agent installer

    localhost#bash sudo /mnt/flash/aos_device_agent.run
    Verifying archive integrity... All good.
    Uncompressing AOS Device Agent installer  100%
    + set -o pipefail
    +++ dirname ./agent_installer.sh
    ++ cd .
    ++ pwd
    + script_dir=/tmp/selfgz726322812
    ++ date
    + echo 'Device Agent Installation : Wed' Oct 18 20:34:11 UTC 2017
    Device Agent Installation : Wed Oct 18 20:34:11 UTC 2017
    + echo

    + UNKNOWN_PLATFORM=1
    + WRONG_PLATFORM=1
    + CANNOT_EXECUTE=126
    + '[' 0 -ne 0 ']'
    + arg_parse
    + start_aos=True
    + [[ 0 > 0 ]]
    + supported_platforms=(["centos"]="install_sysvinit_rpm" ["eos"]="install_on_arista" ["nxos"]="install_on_nxos" ["cumulus"]="install_sysvinit_deb" ["trusty"]="install_sysvinit_deb" ["icos"]="install_sysvinit_rpm" ["snaproute"]="install_sysvinit_deb" ["simulation"]="install_sysvinit_deb")
    + declare -A supported_platforms
    ++ /tmp/selfgz726322812/aos_get_platform
    + current_platform=eos
    + installer=install_on_arista
    + [[ -z install_on_arista ]]
    + [[ -x /etc/init.d/aos ]]
    + echo 'Stopping AOS'
    Stopping AOS
    +++ readlink /sbin/init
    ++ basename upstart
    + [[ systemd == upstart ]]
    + /etc/init.d/aos stop
    + install_on_arista
    ++ pwd
    + local pkg_dir=/tmp/selfgz726322812/arista
    + local to_be_installed=
    + local flash_dir_from_bash=/mnt/flash/aos-installer
    + local flash_dir_from_cli=flash:/aos-installer
    + cp aos_device_agent.img /mnt/flash/
    + mkdir -p /mnt/flash/aos-installer
    ++ ls /mnt/flash/.extensions/aos-device-agent-2.0.0-0.1.138.i386.rpm
    + existing_aos=/mnt/flash/.extensions/aos-device-agent-2.0.0-0.1.138.i386.rpm
    + for aos_rpm in '${existing_aos}'
    ++ basename /mnt/flash/.extensions/aos-device-agent-2.0.0-0.1.138.i386.rpm
    + ip netns exec default FastCli -p15 -c 'no extension aos-device-agent-2.0.0-0.1.138.i386.rpm'
    ++ basename /mnt/flash/.extensions/aos-device-agent-2.0.0-0.1.138.i386.rpm
    + ip netns exec default FastCli -p15 -c 'delete extension:aos-device-agent-2.0.0-0.1.138.i386.rpm'
    + pushd /tmp/selfgz726322812/arista
    /tmp/selfgz726322812/arista /tmp/selfgz726322812
    ++ ls aos-device-agent-2.0.0-0.1.138.i386.rpm
    + aos_rpm=aos-device-agent-2.0.0-0.1.138.i386.rpm
    + cp aos-device-agent-2.0.0-0.1.138.i386.rpm /mnt/flash/aos-installer
    + ip netns exec default FastCli -p15 -c 'copy flash:/aos-installer/aos-device-agent-2.0.0-0.1.138.i386.rpm extension:'
    Copy completed successfully.
    + ip netns exec default FastCli -p15 -c 'extension aos-device-agent-2.0.0-0.1.138.i386.rpm force'
    + popd
    /tmp/selfgz726322812
    + ip netns exec default FastCli -p15 -c 'copy installed-extensions boot-extensions'
    Copy completed successfully.
    + rm -rf /mnt/flash/aos-installer
    + /etc/init.d/aos config_gen
    + [[ True == \T\r\u\e ]]
    + aos_starter -f


AOS Device Agent Configuration File
===================================
The Arista device agent manages the running-configuration file.  No other
configuration files are modified throughout the device agent lifecycle.
Configuration can be managed by editing the device agent
configuration file directly. The Arista EOS device agent config file is located at
``/mnt/flash/aos-conf``. See
:doc:`AOS Device Agent Configuration file <aos_agent_configuration_file>`
for parameters. After updating the file, restart the AOS device agent.

.. code-block:: text
   :caption: Restart AOS Device Agent

    localhost# bash sudo systemctl stop aos
    localhost# bash sudo systemctl start aos


Arista Agent Troubleshooting
============================

AOS Log files
-------------

AOS logs to a number of files in /var/log/aos.

Confirming installation of AOS Agent
Check if the AOS device agent package is installed.

.. code-block:: prompt
    :caption: Show RPM Data

    -bash-4.1# rpm -q --info aos-device-agent
    Name        : aos-device-agent          Relocations: /
    Version     : 1.0.1                             Vendor: (none)
    Release     : 0.1.15                        Build Date: Thu Oct  6 21:21:08 2016
    Install Date: Fri Oct 21 04:14:07 2016      Build Host: 6539ff88c5b0
    Group       : Unspecified                   Source RPM: aos-device-agent-1.0.1-0.1.15.src.rpm
    Size        : 87227369                      License: Copyright 2014-present, Apstra, Inc. All rights reserved.
    Signature   : (none)
    Summary     : AOS device agent package for Arista switches
    Description :
    AOS device agent for Arista switches

    localhost#show extension detail
           Name: EosSdk-1.8.1-4.16.6M.i686.rpm
        Version: 1.8.1
        Release: 3206305.idboiseeossdk
       Presence: available
         Status: installed
         Vendor:
        Summary: EOS Software Development Kit
           RPMS: EosSdk-1.8.1-4.16.6M.i686.rpm 1.8.1/3206305.idboiseeossdk
     Total size: 8073886 bytes
    Description:
    The EOS Software Development Kit provides a set of stable C++ interfaces for
    high-performance access to EOS primitives, for onbox programming beyond what
    can be done with Python.

           Name: aos-device-agent-1.2.0-0.1.137.i386.rpm
        Version: 1.2.0
        Release: 0.1.137
       Presence: available
         Status: installed
         Vendor:
        Summary: AOS device agent package for Arista switches
           RPMS: aos-device-agent-1.2.0-0.1.137.i386.rpm 1.2.0/0.1.137
     Total size: 88651 bytes
    Description:
    AOS device agent for Arista switches

Check if the AOS Agent is running
---------------------------------

.. code-block:: prompt
    :caption: The AOS services must be running

    localhost#bash sudo service aos status
    AOS is running

.. code-block:: prompt
    :caption: Check for presence of files in flash

    localhost#dir flash:aos*
    Directory of flash:/aos*

           -rwx        2228           May 31 02:26  aos-config
           -rwx    55668736           May 31 02:25  aos_device_agent.img
           -rwx    54889549           May 31 02:10  aos_device_agent_1.2.0-137_eos.run

    Directory of flash:/aos

           drwx        4096           May 31 02:25  plugins

    4025892864 bytes total (3392516096 bytes free)

.. code-block:: prompt
    :caption: Check for AOS data in /var/log/aos

    localhost#dir file:/var/log/aos
    Directory of file:/var/log/aos

           -rw-           0           May 31 02:37  000C29E808A1-0.4602.1496198223.log
           -rw-           0           May 31 02:37  000C29E808A1-0.err
           -rw-           0           May 31 02:37  000C29E808A1-0.out
           -rw-       63643           May 31 02:40  000C29E808A1-LocalTasks-000C29E808A1-0_2017-05-31--02-37-03_4602-2017-05-31--02-37-03.tel
           -rw-           0           May 31 02:37  CounterProxyAgent.4604.1496198231.log
           -rw-           0           May 31 02:37  CounterProxyAgent.4684.1496198239.log
           -rw-        1490           May 31 02:37  CounterProxyAgent.err
           -rw-           0           May 31 02:37  CounterProxyAgent.out
           -rw-       33589           May 31 02:37  CounterProxyAgent000C29E808A1_2017-05-31--02-37-12_4604-2017-05-31--02-37-12.tel
           -rw-       42562           May 31 02:37  CounterProxyAgent000C29E808A1_2017-05-31--02-37-20_4684-2017-05-31--02-37-20.tel
           -rw-           0           May 31 02:37  DeploymentProxyAgent.4603.1496198226.log
           -rw-           0           May 31 02:37  DeploymentProxyAgent.4629.1496198235.log
           -rw-        1569           May 31 02:37  DeploymentProxyAgent.err
           -rw-           0           May 31 02:37  DeploymentProxyAgent.out
           -rw-       33618           May 31 02:37  DeploymentProxyAgent000C29E808A1_2017-05-31--02-37-07_4603-2017-05-31--02-37-07.tel
           -rw-       39585           May 31 02:37  DeploymentProxyAgent000C29E808A1_2017-05-31--02-37-16_4629-2017-05-31--02-37-16.tel
           -rw-           0           May 31 02:37  DeviceKeeperAgent.4606.1496198231.log
           -rw-         510           May 31 02:37  DeviceKeeperAgent.err
           -rw-           0           May 31 02:37  DeviceKeeperAgent.out
           -rw-       38221           May 31 02:37  DeviceKeeperAgent000C29E808A1_2017-05-31--02-37-12_4606-2017-05-31--02-37-12.tel
           -rw-           0           May 31 02:37  DeviceTelemetryAgent.4605.1496198230.log
           -rw-         158           May 31 02:37  DeviceTelemetryAgent.4670.1496198242.log
           -rw-        2580           May 31 02:37  DeviceTelemetryAgent.err
           -rw-           0           May 31 02:37  DeviceTelemetryAgent.out
           -rw-       33597           May 31 02:37  DeviceTelemetryAgent000C29E808A1_2017-05-31--02-37-12_4605-2017-05-31--02-37-12.tel
           -rw-       56620           May 31 02:37  DeviceTelemetryAgent000C29E808A1_2017-05-31--02-37-23_4670-2017-05-31--02-37-23.tel
           -rw-       50737           May 31 02:37  Spawner-000C29E808A1_2017-05-31--02-37-02_4597-2017-05-31--02-37-02.tel
           -rw-         640           May 31 02:37  _000C29E808A1-00000000592e2c4f-00054c50-checkpoint
           -rw-           0           May 31 02:37  _000C29E808A1-00000000592e2c4f-00054c50-checkpoint-valid
           -rw-           0           May 31 02:37  _000C29E808A1-00000000592e2c4f-00054c50-log
           -rw-           0           May 31 02:37  _000C29E808A1-00000000592e2c4f-00054c50-log-valid
           -rw-           0           May 31 02:37  aos.log

    291463168 bytes total (260136960 bytes free)

DNS resolution failure
----------------------

AOS agent is sensitive to the DNS resolution of the metadb connection.
Ensure that the IP and/or DNS from the config file is reachable from the device
management port.

.. code-block:: prompt
    :caption: Check for DNS failures

    localhost# bash sudo service aos show_tech | grep -i dns
    [2016/10/20 23:04:20.534538UTC@event-'warning']:(textMsg=Failing outgoing mount to <'tbt://aos-server:29731/Data/ReplicaStatus?flags=i','/Metadb/ReplicaStatus'>' due to code 'resynchronizing' and reason 'Dns lookup issue "Temporary failure in name resolution" Unknown error 18446744073709551613)
    [2016/10/20 23:04:21.540444UTC@OutgoingMountConnectionError-'warning']:(connectionName=--NONE--,localPath=/Metadb/ReplicaStatus,remotePath=tbt://aos-server:29731/Data/ReplicaStatus?flags=i,msg=Tac::ErrnoException: Dns lookup issue "Temporary failure in name resolution" Unknown error 18446744073709551613)
    [2016/10/20 23:04:21.541174UTC@event-'warning']:(textMsg=Failing outgoing mount to <'tbt://aos-server:29731/Data/ReplicaStatus?flags=i','/Metadb/ReplicaStatus'>' due to code 'resynchronizing' and reason 'Dns lookup issue "Temporary failure in name resolution" Unknown error 18446744073709551613)

Listing running processes
-------------------------

AOS processes on an Arista switch run alongside other management components on the
switch.  List the services with the ‘ps wax’ command.

.. code-block:: prompt
    :caption: List processes

    localhost#bash sudo service aos attach
    aos@localhost:/# ps wax
      PID TTY      STAT   TIME COMMAND
        1 ?        Ss     0:03 /sbin/init
        2 ?        S      0:00 [kthreadd]
        3 ?        S      0:00 [ksoftirqd/0]
        4 ?        S      0:00 [kworker/0:0]
        6 ?        S      0:00 [migration/0]
        8 ?        S<     0:00 [khelper]
        9 ?        S<     0:00 [netns]
       10 ?        S      0:00 [kworker/u:1]
      168 ?        S      0:00 [sync_supers]
      170 ?        S      0:00 [bdi-default]
      172 ?        S<     0:00 [kblockd]
      179 ?        S<     0:00 [ata_sff]
      189 ?        S      0:00 [khubd]
      290 ?        S      0:00 [dst_gc_task]
      375 ?        S      0:00 [arp_cache-prd]
      376 ?        S      0:00 [icmp_unreachabl]
      377 ?        S<     0:00 [rpciod]
      380 ?        S<     0:00 [ecc_log_wq]
      388 ?        S      0:00 [khungtaskd]
      389 ?        S      0:00 [khungtaskd2]
      394 ?        S      0:00 [kswapd0]
      395 ?        S      0:00 [fsnotify_mark]
      396 ?        S<     0:00 [nfsiod]
      397 ?        S<     0:00 [crypto]
      467 ?        S<     0:00 [pcielwd]
      506 ?        S      0:00 [scsi_eh_0]
      509 ?        S      0:00 [scsi_eh_1]
      512 ?        S      0:00 [kworker/u:2]
      599 ?        S<     0:00 [edac-poller]
      631 ?        S      0:00 [ndisc_cache-prd]
      635 ?        S<     0:00 [deferwq]
      951 ?        S<     0:00 [loop0]
     1244 ?        S<s    0:00 /sbin/udevd -d
     1374 ?        S      0:01 [kworker/0:2]
     1471 ?        S<     0:00 /sbin/udevd -d
     1730 ?        S      0:00 python /usr/bin/immortalize --daemonize --log=/var/log/agents/ConnMgr --logpidsuffix --maxcredits=5 --cos
     1732 ?        S      0:00 /usr/bin/ConnMgr -p /var/run/ConnMgr.pid
     1750 ?        S      0:00 python /usr/bin/immortalize --daemonize --log=/var/log/agents/TimeAgent --logpidsuffix --maxcredits=5 --c
     1751 ?        S<     0:00 /usr/bin/TimeAgent -c /etc/TimeAgent.conf -p /var/run/TimeAgent.pid
     1762 ?        S      0:00 watchdog
     1763 ?        S<     0:00 wdog-cld
     1786 ?        S      0:00 python /usr/bin/inotifyrun -c pax -x sv4cpio -O -w -f /mnt/flash/persist/local.new . && mv /mnt/flash/per
     1788 ?        Ss+    0:00 inotifywait -m -r -e modify -e create -e delete -e attrib -e move .
     1798 ?        S      0:00 python /usr/bin/inotifyrun -c pax -x sv4cpio -O -w -f /mnt/flash/persist/sys.new . && mv /mnt/flash/persi
     1799 ?        Ss+    0:00 inotifywait -m -r -e modify -e create -e delete -e attrib -e move .
     1811 ?        S      0:00 python /usr/bin/inotifyrun -c shred --exact --iterations=1 /mnt/flash/persist/secure; pax -x sv4cpio -O -
     1813 ?        Ss+    0:00 inotifywait -m -r -e modify -e create -e delete -e attrib -e move .
     1820 ?        S      0:00 [watchdog/0]
     1964 ?        S      0:00 /usr/bin/EosOomAdjust
     1968 ?        Ss     0:00 /usr/sbin/mcelog --daemon --no-syslog --logfile /var/log/mcelog
     1979 ?        S      0:00 [kbfd_v4v6_rx]
     1980 ?        S      0:00 [kbfd_v4v6_echo]
     1981 ?        S<     0:00 [kbfd_tx]
     1982 ?        S<     0:00 [kbfd_rx_expire]
     1983 ?        S<     0:00 [kbfd_tx_reset]
     1984 ?        S<     0:00 [kbfd_echo_tx]
     1985 ?        S<     0:00 [kbfd_echo_rx_ex]
     1986 ?        S<     0:00 [kbfd_echo_tx_re]
     1987 ?        S<     0:00 [kbfd_echo_exp_r]
     2030 ?        Ss     0:00 crond
     2079 ?        S      0:00 netnsd-watcher  -d -i --dlopen -p -f  -l libLoadDynamicLibs.so procmgr libProcMgrSetup.so --daemonize
     2081 ?        S      0:00 netnsd-server   -d -i --dlopen -p -f  -l libLoadDynamicLibs.so procmgr libProcMgrSetup.so --daemonize
     2091 ?        S      0:00 ProcMgr-mast    -d -i --dlopen -p -f  -l libLoadDynamicLibs.so procmgr libProcMgrSetup.so --daemonize
     2092 ?        S      0:02 ProcMgr-work    -d -i --dlopen -p -f  -l libLoadDynamicLibs.so procmgr libProcMgrSetup.so --daemonize
     2093 ?        S      0:14 Sysdb           -d -i --dlopen -p -f  -l libLoadDynamicLibs.so procmgr libProcMgrSetup.so --daemonize
     2094 ?        S      0:02 /usr/bin/SlabMonitor
     2095 ?        S      0:03 FastClid-ser    -d -i --dlopen -p -f  -l libLoadDynamicLibs.so procmgr libProcMgrSetup.so --daemonize
     2131 ?        S      0:01 Fru             -d -i --dlopen -p -f  -l libLoadDynamicLibs.so procmgr libProcMgrSetup.so --daemonize
     2136 ?        S      0:02 Launcher        -d -i --dlopen -p -f  -l libLoadDynamicLibs.so procmgr libProcMgrSetup.so --daemonize
     2222 ?        S      0:01 /usr/bin/EosProxySdkAgent --agenttitle=EosSdk-EosProxySdkAgent --demuxerOpts=172749640510,172743984283,tb
     2244 ?        S      0:00 netns --agenttitle=LacpTxAgent --demuxerOpts=176938128982,176937081924,tbl://sysdb/+n,Sysdb (pid:2093) --
     2249 ?        Ss     0:00 netnsd-session  -d -i --dlopen -p -f  -l libLoadDynamicLibs.so procmgr libProcMgrSetup.so --daemonize
     2250 ?        S      0:00 LacpTxAgent     -d -i --dlopen -p -f  -l libLoadDynamicLibs.so procmgr libProcMgrSetup.so --daemonize
     2264 ?        S      0:00 netns --agenttitle=Ipv6RouterAdvt --demuxerOpts=177054066724,176993113047,tbl://sysdb/+n,Sysdb (pid:2093)
     2266 ?        Ss     0:00 netnsd-session  -d -i --dlopen -p -f  -l libLoadDynamicLibs.so procmgr libProcMgrSetup.so --daemonize
     2267 ?        S      0:00 Ipv6RouterAd          --dlopen -p -f  -l libLoadDynamicLibs.so procmgr libProcMgrSetup.so --daemonize
     2286 ?        S      0:00 netns --agenttitle=AgentMonitor --demuxerOpts=180713744050,180503816091,tbl://sysdb/+n,Sysdb (pid:2093) -
     2289 ?        Ss     0:00 netnsd-session  -d -i --dlopen -p -f  -l libLoadDynamicLibs.so procmgr libProcMgrSetup.so --daemonize
     2290 ?        S      0:02 AgentMonitor    -d -i --dlopen -p -f  -l libLoadDynamicLibs.so procmgr libProcMgrSetup.so --daemonize
     2294 ?        S      0:00 netns --agenttitle=Mirroring --demuxerOpts=181173742385,181026608825,tbl://sysdb/+n,Sysdb (pid:2093) --sy
     2295 ?        Ss     0:00 netnsd-session  -d -i --dlopen -p -f  -l libLoadDynamicLibs.so procmgr libProcMgrSetup.so --daemonize
     2296 ?        S      0:00 Mirroring       -d -i --dlopen -p -f  -l libLoadDynamicLibs.so procmgr libProcMgrSetup.so --daemonize
     2315 ?        S      0:00 netns --agenttitle=Acl --demuxerOpts=184720501541,181293026506,tbl://sysdb/+n,Sysdb (pid:2093) --sysdbfd=
     2316 ?        Ss     0:00 netnsd-session  -d -i --dlopen -p -f  -l libLoadDynamicLibs.so procmgr libProcMgrSetup.so --daemonize
     2317 ?        S      0:00 Acl             -d -i --dlopen -p -f  -l libLoadDynamicLibs.so procmgr libProcMgrSetup.so --daemonize
     2328 ?        S      0:00 IgmpSnooping    -d -i --dlopen -p -f  -l libLoadDynamicLibs.so procmgr libProcMgrSetup.so --daemonize
     2359 ?        S      0:01 SuperServer     -d -i --dlopen -p -f  -l libLoadDynamicLibs.so procmgr libProcMgrSetup.so --daemonize
     2446 ?        S      0:00 netns --agenttitle=Dot1x --demuxerOpts=193890685273,189430843618,tbl://sysdb/+n,Sysdb (pid:2093) --sysdbf
     2447 ?        Ss     0:00 netnsd-session  -d -i --dlopen -p -f  -l libLoadDynamicLibs.so procmgr libProcMgrSetup.so --daemonize
     2448 ?        S      0:00 Dot1x           -d -i --dlopen -p -f  -l libLoadDynamicLibs.so procmgr libProcMgrSetup.so --daemonize
     2467 ?        S      0:00 FastClidCapi    -d -i --dlopen -p -f  -l libLoadDynamicLibs.so procmgr libProcMgrSetup.so --daemonize
     2503 ?        S      0:00 FastClid-ses    -d -i --dlopen -p -f  -l libLoadDynamicLibs.so procmgr libProcMgrSetup.so --daemonize
     2504 ?        Ssl    0:13 FastCapi        -d -i --dlopen -p -f  -l libLoadDynamicLibs.so procmgr libProcMgrSetup.so --daemonize
     2540 ?        S      0:00 netns --agenttitle=EventMgr --demuxerOpts=198435198068,198381904787,tbl://sysdb/+n,Sysdb (pid:2093) --sys
     2541 ?        Ss     0:00 netnsd-session  -d -i --dlopen -p -f  -l libLoadDynamicLibs.so procmgr libProcMgrSetup.so --daemonize
     2542 ?        S      0:00 EventMgr        -d -i --dlopen -p -f  -l libLoadDynamicLibs.so procmgr libProcMgrSetup.so --daemonize
     2544 ?        S      0:00 netns --agenttitle=TopoAgent --demuxerOpts=207004990826,206854969014,tbl://sysdb/+n,Sysdb (pid:2093) --sy
     2546 ?        Ss     0:00 netnsd-session  -d -i --dlopen -p -f  -l libLoadDynamicLibs.so procmgr libProcMgrSetup.so --daemonize
     2547 ?        S      0:00 TopoAgent       -d -i --dlopen -p -f  -l libLoadDynamicLibs.so procmgr libProcMgrSetup.so --daemonize
     2568 ?        S      0:00 netns --agenttitle=PortSec --demuxerOpts=211114755521,211113859019,tbl://sysdb/+n,Sysdb (pid:2093) --sysd
     2570 ?        Ss     0:00 netnsd-session  -d -i --dlopen -p -f  -l libLoadDynamicLibs.so procmgr libProcMgrSetup.so --daemonize
     2571 ?        S      0:00 PortSec         -d -i --dlopen -p -f  -l libLoadDynamicLibs.so procmgr libProcMgrSetup.so --daemonize
     2573 ?        S      0:00 netns --agenttitle=Bfd --demuxerOpts=211236786399,211177838833,tbl://sysdb/+n,Sysdb (pid:2093) --sysdbfd=
     2576 ?        Ss     0:00 netnsd-session  -d -i --dlopen -p -f  -l libLoadDynamicLibs.so procmgr libProcMgrSetup.so --daemonize
     2580 ?        S      0:00 Bfd             -d -i --dlopen -p -f  -l libLoadDynamicLibs.so procmgr libProcMgrSetup.so --daemonize
     2595 ?        S      0:00 netns --agenttitle=Ira --demuxerOpts=214768824794,211370899495,tbl://sysdb/+n,Sysdb (pid:2093) --sysdbfd=
     2596 ?        Ss     0:00 netnsd-session  -d -i --dlopen -p -f  -l libLoadDynamicLibs.so procmgr libProcMgrSetup.so --daemonize
     2598 ?        S      0:00 Ira             -d -i --dlopen -p -f  -l libLoadDynamicLibs.so procmgr libProcMgrSetup.so --daemonize
     2618 ?        S      0:00 netns --agenttitle=LedPolicy --demuxerOpts=215245146330,215100253912,tbl://sysdb/+n,Sysdb (pid:2093) --sy
     2619 ?        Ss     0:00 netnsd-session  -d -i --dlopen -p -f  -l libLoadDynamicLibs.so procmgr libProcMgrSetup.so --daemonize
     2621 ?        S      0:00 LedPolicy       -d -i --dlopen -p -f  -l libLoadDynamicLibs.so procmgr libProcMgrSetup.so --daemonize
     2628 ?        Sl     0:00 Aaa             -d -i --dlopen -p -f  -l libLoadDynamicLibs.so procmgr libProcMgrSetup.so --daemonize
     2648 ?        S      0:00 netns --agenttitle=CapiApp-CapiApp --demuxerOpts=219306529482,219133267319,tbl://sysdb/+n,Sysdb (pid:2093
     2651 ?        Ss     0:00 netnsd-session  -d -i --dlopen -p -f  -l libLoadDynamicLibs.so procmgr libProcMgrSetup.so --daemonize
     2657 ?        Sl     0:01 uwsgi           -d -i --dlopen -p -f  -l libLoadDynamicLibs.so procmgr libProcMgrSetup.so --daemonize
     2661 ?        S      0:00 netns --agenttitle=StpTxRx --demuxerOpts=219560663096,219463089954,tbl://sysdb/+n,Sysdb (pid:2093) --sysd
     2668 ?        Ss     0:00 netnsd-session  -d -i --dlopen -p -f  -l libLoadDynamicLibs.so procmgr libProcMgrSetup.so --daemonize
     2669 ?        S      0:00 StpTxRx         -d -i --dlopen -p -f  -l libLoadDynamicLibs.so procmgr libProcMgrSetup.so --daemonize
     2681 ?        S      0:00 netns --agenttitle=Macsec --demuxerOpts=219852379174,219704155526,tbl://sysdb/+n,Sysdb (pid:2093) --sysdb
     2682 ?        Ss     0:00 netnsd-session  -d -i --dlopen -p -f  -l libLoadDynamicLibs.so procmgr libProcMgrSetup.so --daemonize
     2683 ?        S      0:00 Macsec          -d -i --dlopen -p -f  -l libLoadDynamicLibs.so procmgr libProcMgrSetup.so --daemonize
     2718 ?        S      0:00 MplsUtilLsp     -d -i --dlopen -p -f  -l libLoadDynamicLibs.so procmgr libProcMgrSetup.so --daemonize
     2744 ?        Ss     0:00 nginx: master process /usr/sbin/nginx -c /etc/nginx/nginx.conf -g pid /var/run/nginx.pid;
     2748 ?        S      0:00 nginx: worker process
     2910 ?        S      0:00 netns --agenttitle=MaintenanceMode --demuxerOpts=236329384403,223871866307,tbl://sysdb/+n,Sysdb (pid:2093
     2916 ?        Ss     0:00 netnsd-session  -d -i --dlopen -p -f  -l libLoadDynamicLibs.so procmgr libProcMgrSetup.so --daemonize
     2920 ?        S      0:00 MaintenanceM                   -p -f  -l libLoadDynamicLibs.so procmgr libProcMgrSetup.so --daemonize
     2963 ?        S      0:00 netns --agenttitle=Arp --demuxerOpts=236663705062,236485011967,tbl://sysdb/+n,Sysdb (pid:2093) --sysdbfd=
     2971 ?        Ss     0:00 netnsd-session  -d -i --dlopen -p -f  -l libLoadDynamicLibs.so procmgr libProcMgrSetup.so --daemonize
     2974 ?        Sl     0:00 Arp             -d -i --dlopen -p -f  -l libLoadDynamicLibs.so procmgr libProcMgrSetup.so --daemonize
     2980 ?        Ss     0:00 /usr/sbin/sshd
     2997 ?        S      0:00 netns --agenttitle=PowerManager --demuxerOpts=240546963425,236860990252,tbl://sysdb/+n,Sysdb (pid:2093) -
     3002 ?        Ss     0:00 netnsd-session  -d -i --dlopen -p -f  -l libLoadDynamicLibs.so procmgr libProcMgrSetup.so --daemonize
     3004 ?        S      0:00 PowerManager    -d -i --dlopen -p -f  -l libLoadDynamicLibs.so procmgr libProcMgrSetup.so --daemonize
     3007 ?        S      0:00 netns --agenttitle=Mpls --demuxerOpts=241249655231,241228647018,tbl://sysdb/+n,Sysdb (pid:2093) --sysdbfd
     3014 ?        Ss     0:00 netnsd-session  -d -i --dlopen -p -f  -l libLoadDynamicLibs.so procmgr libProcMgrSetup.so --daemonize
     3015 ?        S      0:00 Mpls            -d -i --dlopen -p -f  -l libLoadDynamicLibs.so procmgr libProcMgrSetup.so --daemonize
     3031 ?        S      0:01 CliSessionMg    -d -i --dlopen -p -f  -l libLoadDynamicLibs.so procmgr libProcMgrSetup.so --daemonize
     3040 ?        S<     0:00 /sbin/udevd -d
     3070 ?        S      0:00 netns --agenttitle=Fhrp --demuxerOpts=245198240050,244921462712,tbl://sysdb/+n,Sysdb (pid:2093) --sysdbfd
     3075 ?        Ss     0:00 netnsd-session  -d -i --dlopen -p -f  -l libLoadDynamicLibs.so procmgr libProcMgrSetup.so --daemonize
     3077 ?        S      0:00 Fhrp            -d -i --dlopen -p -f  -l libLoadDynamicLibs.so procmgr libProcMgrSetup.so --daemonize
     3118 ?        Sl     0:00 /sbin/rsyslogd -i /var/run/syslogd.pid -c 5
     3122 ?        S      0:00 netns --agenttitle=Qos --demuxerOpts=249452799773,245803103371,tbl://sysdb/+n,Sysdb (pid:2093) --sysdbfd=
     3131 ?        Ss     0:00 netnsd-session  -d -i --dlopen -p -f  -l libLoadDynamicLibs.so procmgr libProcMgrSetup.so --daemonize
     3136 ?        S      0:00 Qos             -d -i --dlopen -p -f  -l libLoadDynamicLibs.so procmgr libProcMgrSetup.so --daemonize
     3184 ?        S      0:00 netns --agenttitle=Thermostat --demuxerOpts=253407320281,249878057576,tbl://sysdb/+n,Sysdb (pid:2093) --s
     3185 ?        Ss     0:00 netnsd-session  -d -i --dlopen -p -f  -l libLoadDynamicLibs.so procmgr libProcMgrSetup.so --daemonize
     3187 ?        S      0:00 Thermostat      -d -i --dlopen -p -f  -l libLoadDynamicLibs.so procmgr libProcMgrSetup.so --daemonize
     3189 ?        S      0:00 netns --agenttitle=Lldp --demuxerOpts=254384000160,254383598162,tbl://sysdb/+n,Sysdb (pid:2093) --sysdbfd
     3190 ?        Ss     0:00 netnsd-session  -d -i --dlopen -p -f  -l libLoadDynamicLibs.so procmgr libProcMgrSetup.so --daemonize
     3192 ?        S      0:00 Lldp            -d -i --dlopen -p -f  -l libLoadDynamicLibs.so procmgr libProcMgrSetup.so --daemonize
     3198 ?        S      0:00 Lag             -d -i --dlopen -p -f  -l libLoadDynamicLibs.so procmgr libProcMgrSetup.so --daemonize
     3217 ?        S      0:00 EventMon        -d -i --dlopen -p -f  -l libLoadDynamicLibs.so procmgr libProcMgrSetup.so --daemonize
     3220 ?        S      0:00 /usr/bin/conlogd
     3222 ?        S      0:00 sh -c /usr/bin/tail -n 0 --retry --follow=name --pid=3220 /var/log/eos-console | sed 's/\(.*\)/\1\r/'
     3223 ?        S      0:00 /usr/bin/tail -n 0 --retry --follow=name --pid=3220 /var/log/eos-console
     3224 ?        S      0:00 sed s/\(.*\)/\1\r/
     3233 ?        S      0:01 PhyEthtool      -d -i --dlopen -p -f  -l libLoadDynamicLibs.so procmgr libProcMgrSetup.so --daemonize
     3264 ?        S      0:00 netns --agenttitle=StpTopology --demuxerOpts=262614958826,262505739622,tbl://sysdb/+n,Sysdb (pid:2093) --
     3269 ?        Ss     0:00 netnsd-session  -d -i --dlopen -p -f  -l libLoadDynamicLibs.so procmgr libProcMgrSetup.so --daemonize
     3277 ?        S      0:00 StpTopology     -d -i --dlopen -p -f  -l libLoadDynamicLibs.so procmgr libProcMgrSetup.so --daemonize
     3278 ?        S      0:00 netns --agenttitle=Stp --demuxerOpts=262947885263,262802812166,tbl://sysdb/+n,Sysdb (pid:2093) --sysdbfd=
     3279 ?        Ss     0:00 netnsd-session  -d -i --dlopen -p -f  -l libLoadDynamicLibs.so procmgr libProcMgrSetup.so --daemonize
     3280 ?        S      0:00 Stp             -d -i --dlopen -p -f  -l libLoadDynamicLibs.so procmgr libProcMgrSetup.so --daemonize
     3281 ?        S      0:07 Etba            -d -i --dlopen -p -f  -l libLoadDynamicLibs.so procmgr libProcMgrSetup.so --daemonize
     3289 ?        S      0:00 netns --agenttitle=Ebra --demuxerOpts=267068997224,266942848299,tbl://sysdb/+n,Sysdb (pid:2093) --sysdbfd
     3290 ?        Ss     0:00 netnsd-session  -d -i --dlopen -p -f  -l libLoadDynamicLibs.so procmgr libProcMgrSetup.so --daemonize
     3291 ?        S      0:00 Ebra            -d -i --dlopen -p -f  -l libLoadDynamicLibs.so procmgr libProcMgrSetup.so --daemonize
     3295 ?        S      0:00 netns --agenttitle=KernelFib --demuxerOpts=270859722189,270754589714,tbl://sysdb/+n,Sysdb (pid:2093) --sy
     3296 ?        Ss     0:00 netnsd-session  -d -i --dlopen -p -f  -l libLoadDynamicLibs.so procmgr libProcMgrSetup.so --daemonize
     3297 ?        S      0:00 KernelFib       -d -i --dlopen -p -f  -l libLoadDynamicLibs.so procmgr libProcMgrSetup.so --daemonize
     3298 ?        S      0:02 /usr/sbin/ribd -N
     3496 ?        Ss     0:00 /usr/sbin/sshd-management -f /etc/ssh/sshd_config-management
     3554 ttyS0    Ss+    0:00 /sbin/mingetty --noclear /dev/ttyS0
     3564 tty1     Ss+    0:00 /sbin/mingetty /dev/tty1
     3566 tty2     Ss+    0:00 /sbin/mingetty /dev/tty2
     3569 tty3     Ss+    0:00 /sbin/mingetty /dev/tty3
     3571 tty4     Ss+    0:00 /sbin/mingetty /dev/tty4
     3573 tty5     Ss+    0:00 /sbin/mingetty /dev/tty5
     3575 tty6     Ss+    0:00 /sbin/mingetty /dev/tty6
     3618 ?        S      0:02 /usr/sbin/ribd -N -z client name management ns-name ns-management vrfname management servername vre_serve
     4566 ?        S<     0:00 [loop1]
     4600 ?        Sl     0:00 tacspawner --daemonize=/var/log/aos/aos.log --pidfile=/var/run/aos.pid --name=000C29E808A1 --hostname=000
     4602 ?        S      0:00 tacleafsysdb --agentName=000C29E808A1-LocalTasks-000C29E808A1-0 --partition= --storage-mode=persistent --
     4606 ?        Sl     0:00 /usr/bin/python /usr/bin/aos_agent --class=aos.device.common.DeviceKeeperAgent.DeviceKeeperAgent --name=D
     4629 ?        Sl     0:00 /usr/bin/python /usr/bin/aos_agent --class=aos.device.common.ProxyDeploymentAgent.ProxyDeploymentAgent --
     4670 ?        Sl     0:00 /usr/bin/python /usr/bin/aos_agent --class=aos.device.arista.AristaTelemetryAgent.AristaTelemetryAgent --
     4684 ?        Sl     0:00 /usr/bin/python /usr/bin/aos_agent --class=aos.device.common.ProxyCountersAgent.ProxyCountersAgent --name
     5366 ?        S      0:00 FastClidHelp    -d -i --dlopen -p -f  -l libLoadDynamicLibs.so procmgr libProcMgrSetup.so --daemonize
     5371 ?        S      0:00 FastClid-ses    -d -i --dlopen -p -f  -l libLoadDynamicLibs.so procmgr libProcMgrSetup.so --daemonize
     5372 ?        Ssl    0:00 Cli [interac    -d -i --dlopen -p -f  -l libLoadDynamicLibs.so procmgr libProcMgrSetup.so --daemonize
     5483 ?        S      0:00 FastClidHelp    -d -i --dlopen -p -f  -l libLoadDynamicLibs.so procmgr libProcMgrSetup.so --daemonize
     5488 ?        S      0:00 FastClid-ses    -d -i --dlopen -p -f  -l libLoadDynamicLibs.so procmgr libProcMgrSetup.so --daemonize
     5489 ?        Ssl    0:00 Cli [interac    -d -i --dlopen -p -f  -l libLoadDynamicLibs.so procmgr libProcMgrSetup.so --daemonize
     5506 ?        Ss     0:00 sshd-management: admin [priv]
     5531 ?        S      0:00 sshd-management: admin@pts/3
     5534 ?        Ssl+   0:00 FastCli
     5579 ?        S      0:00 sudo service aos attach
     5581 ?        S      0:00 /bin/sh /sbin/service aos attach
     5589 ?        S      0:00 /bin/bash /etc/init.d/aos attach
     5616 ?        S      0:00 /bin/bash
     5622 ?        R+     0:00 ps wax

‘Unable to Connect’ error during EOS Installation
-------------------------------------------------

When attempting to install the Arista EOS Device agent, you may receive a
‘connection refused’ error.  This means that A) The SDK is not running,
and B) the unix-socket is not listening, or C) you are attempting to run the
AOS Installer in the management VRF.

.. code-block:: text
    :caption: Connection refused when running FastCli commands

    Unable to connect: Connection refused
    + status=
    + [[ '' =~ .*Status: installed.* ]]
    + return 1
    + cp aos-device-agent-1.2.1-0.1.72.i386.rpm /mnt/flash/aos-installer
    + FastCli -p15 -c 'copy flash:/aos-installer/aos-device-agent-1.2.1-0.1.72.i386.rpm extension:'
    Unable to connect: Connection refused
    'sudo /mnt/flash/aos_device_agent_1.2.1-72_eos.run' returned error code:255

To resolve this, Switch routing-contexts to ‘default’.
