=====
Links
=====

To access active links, from the blueprint, navigate to
**Active / Physical / Links**. To search for specific nodes or links,
click its query box, enter your criteria and click **Apply** to see results.
To see properties of a particular link (in the right panel), click its name.

.. image:: static/images/blueprints/active/physical/active_links_330.png

Exporting Cabling Map
=====================
#. From the links view (Active / Physical / Links) click the
   **Export cabling map** button and select **JSON** or **CSV**.
#. Click **Copy** to copy the contents or click **Save As File** to download the
   file.
#. When you've copied or downloaded the cabling map, close the dialog to return
   to the **Links** view.

Cabling maps can also be exported from the **Staged / Physical / Links** view.
