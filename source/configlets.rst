==========
Configlets
==========

Configlet Overview
==================

.. sidebar:: What about multi-line banners?

   Using configlets to configure multi-line banners (such as banner motd) on
   Cisco NX-OS and Arista EOS is problematic because of an extra non-ASCII
   character that cannot be entered.

   We recommend that, before you install the AOS agent on the device, you
   configure multi-line banners with ZTP (Arista Zero Touch Provisioning)
   or Cisco POAP (Power-on Auto Provisioning). The banner configuration will
   become part of the device's pristine configuration, and
   it will persist throughout the AOS configuration.

   Another option is to *manually* configure multi-line banners on the device.
   This method causes a *configuration deviation* anomaly that you can clear
   by accepting the new configuration as the
   :ref:`Golden Configuration <golden_config>`. For more information, see
   `Configuration Deviation <active_anomalies.html#configuration-deviation>`_.

Configlets are configuration templates that are applied to network devices. They
augment the AOS reference design with non-native device configuration.
Circumstances when configlets might be employed include syslog, SNMP access
policy, TACACS / RADIUS, management ACLs, control plane policing, NTP and
username / passwords. Configlets are powerful, but, if used improperly, they do
pose risks to deployment stability and feature interactions with the AOS reference
design.

Configlets must *augment* existing configuration, and not attempt to replace it.
While it *is* technically possible to modify AOS-rendered configuration, we
strongly recommend that you refrain from modifying any configuration that affects
routing or connectivity. If configlets are used to change interface configuration,
the *intended* AOS interface configuration can be inadvertently overwritten.
For example, if you add a configlet to create a network span port, you must make
sure that the configlet is properly applied to an **Unused** port, or it might
override one that is already in use.

Configlets are created and added to the global catalog, then they are imported
into the catalogs of existing blueprints and assigned to one or more devices. They
can be assigned to spines and/or leafs, and to specific devices. Devices must be
deployed for configlets to be rendered. If you want to change a configlet that has
been imported into a blueprint catalog, you must import it again into the blueprint
after it's been updated in the global catalog.

Additional characteristics of configlets include:

* Configlets can contain syntax for different vendor NOS types.
* Configlets can use key-values stored in a :doc:`property set <property_sets>` to
  parameterize configuration.
* Passwords and other secret keys are not encrypted in configlets.
* You can use the same configlets across the entire enterprise, but creating and
  applying regionally-specific :doc:`property sets <property_sets>` is recommended
  instead.
* Avoid using shortened versions of commands. AOS may validate the exact commands,
  then return them as-is in the rendered configuration after pushing changes.

.. warning::

    Using configlets to add non-native configuration is not always appropriate,
    or possible, depending on the application. Use care with configlets to
    prevent deployment issues.

Rendering Order
---------------

.. image:: static/images/configlets/config_render_order.png

To control the order of operations within a section, create configlets with
numeric names. For example, ``01_syslog`` renders before ``02_ntp``. Configlets
will then be ordered based on the condition of the configlet (for example the spine
or leaf role), and then by the Node ID of the configlet.

Configlet Details
-----------------

Configlets include the following details:

.. _config_style:

Config Style (NOS)
    Cumulus Linux (applicable sections: SYSTEM, INTERFACE, FILE, OSPF, FRR)

    Cisco NX-OS (applicable sections: SYSTEM, INTERFACE, SYSTEM TOP, OSPF)

    Arista EOS (applicable sections: SYSTEM, INTERFACE, SYSTEM TOP, OSPF)

    Juniper Junos (applicable section: SYSTEM)

    SONiC (applicable sections: SYSTEM, FILE, OSPF, FRR)

.. _configlet_levels:

Section
    System Top (NXOS, EOS)
        Configuration is rendered *before* the AOS reference design is applied.
        System top is ideal for ensuring that AOS can overwrite a setting to
        implement the programmed intent. If you "turn off" a needed feature,
        AOS will reenable it when the reference design is applied.

    System (All NOSs)

        Configuration is rendered *after* the AOS reference design is applied
        and *after* any file-type configlets are applied. System-type configlets
        allow you to run commands on a Linux device as the root user. This
        location offers absolute insertion of the configlet. These changes could
        **potentially break the functionality of the reference design**.

        It's also used in conjunction with file-type configlets to restart
        processes or perform administrative tasks after file-type operations have
        completed. System-type configlets can nest other configuration.

        On Cumulus, the ``net commit`` command is required.

        System type configlets require negation equivalents, so when a configlet
        is unassigned from the node, the configuration is removed from the device.
        For example, if the template text is
        ``username example privilege 15 secret 0 MyPassword``, the negation
        template text might be ``no username example``.

        .. note::
          For NX-OS and EOS, AOS ensures that the appropriate **configure terminal**
          context is applied. This does not need to be part of the configlet.

        .. warning::

            Template text and negation template text are not validated by
            AOS. They are issued directly to the devices. If you improperly
            configure a configlet, AOS does not raise warnings or restrictions. To
            ensure that a configlet performs exactly as intended, test configlet
            templates and negation templates on a separate dedicated device.

        .. warning::

            Use system-type configlets with great caution. Contents are written to
            the file as user root. Improper use can take down a network.

    Interface (Cumulus, NXOS, EOS)
        Configuration is rendered *after* any system-type configlets are applied.
        Interface-type configlets apply config within the context of an interface.
        Only physical interfaces are supported. The actual interfaces that the
        configlet is to be applied to is not specified within the configlet itself.
        The configlet scope is specified when :ref:`importing the configlet into a
        blueprint <import_configlet>`.

        For Cumulus on AOS version 3.2, ``net clu`` (NCLU) syntax are no longer
        used. The config line must match ``/etc/network/interfaces`` syntax exactly.

        For Cumulus on AOS version 3.1 and earlier, do not use ``net commit``.
        (In interface context this is handled by AOS.) Also, omit ``net add``
        and specific interface designation (e.g. ``net add interface swp1``) from
        both the template text and negation template text, as AOS adds it
        automatically.

    File (Cumulus, SONiC)
        File-type configlets add configuration to Cumulus devices that
        cannot be applied with Cumulus NCLU. The text file is added to
        the system and the contents of the targeted file are replaced
        with the template text *after* the AOS reference design is applied
        and *before* any system-type or interface-type configlets are
        applied. See :ref:`example <cumulus_configlet>` below.

        File-type configlets enable you to replace text in an
        existing file located under ``/etc`` *only* (because of the Docker
        container host mount of AOS). File-type configlets do not support
        negation.

        .. important::

            * The entire contents of the file must be present within the configlet.
            * **All** contents of the file are overwritten.
            * There is no versioning or storing of the original file contents.
            * A file that has been overwritten cannot be restored to its original
              contents.

        .. warning::

            Use file-type configlets with great caution. Contents are written to
            the file as user root. Improper use can take down a network.

            File-type configlets should *never* be used on
            configuration files of critical processes (e.g. ``/etc/frr/frr.conf``
            or ``/etc/network/interfaces/``).

        .. tip::

            If you need to write to a file outside of ``/etc`` (``/usr`` for example)
            build the file-type configlet, then use a system-type configlet
            to move the file afterward.


    OSPF (Cumulus, NX-OS, EOS, SONiC)
        OSPF configlets are designed for controlling **SPF Timers** and
        **LSA Pacing and throttling timers**. They apply config within the context
        of the OSPF process. Configuration is rendered *after* any system-type
        configlets are applied. Configuration is specified in **section_condition**
        to define the scope (i.e. which VRF it is applied to).
        See :ref:`Staging Configlets <staging_configlets>`. Under
        **section_condition**, VRF name must be checked. If no VRFs are checked,
        the OSPF configlet applies to all VRFs.

        On Cumulus, OSPF-type configlets are added directly into the OSPF section.
        Negation is not supported.

        On NX-OS and EOS, the configuration is rendered under **router ospf**.

    FRR (Cumulus as of AOS version 3.2.0, SONiC as of AOS version 3.3.0a)
        FRR configlets gives the ability to append FRR configuration to the
        configuration file (``/etc/frr/frr.conf``) generated by AOS. The configlet
        content is added at the *bottom* of the file, so the user-defined
        configlet becomes part of FRR "intent" and the configlet configuration is
        *incrementally included* in frr-reload.

        .. important::
           AOS cannot validate template text. It is the user's
           responsibility to ensure that the FRR configuration in the configlet
           is valid. Errors in the FRR configlet are likely to cause
           deployment errors, unintended configuration, and device impact.

   .. warning::

      Use configlet types **File** and **System** with great caution. Contents are
      written to the file as user root. Improper use can take down a network.

Template Text
    CLI commands to add custom configuration to a device.
    Contents are not validated by AOS.

Negation Template Text (as applicable)
    CLI commands to disable functionality of the configlet.
    Contents are not validated by AOS.

Filename (Cumulus, SONiC as of AOS version 3.3.0a)
    For file-type configlets


From the AOS web interface, navigate to **Design / Configlets**.

.. image:: static/images/configlets/configlets_330.png

.. sidebar:: Cloning Configlets

   Instead of entering all details for a new configlet, you can clone an
   existing one, give it a new name and customize it.

.. _create_configlet:

Creating Configlet
==================

1. From the AOS web interface, navigate to **Design / Configlets**, then
   click **Create Configlet**.
2. Enter a configlet name (64 characters or fewer).
3. Select the :ref:`config style <config_style>` (Cumulus, NXOS,  EOS, Junos,
   SONiC). If you are creating a configlet on Cumulus, please see additional
   information in the **Configlets on Cumulus** section below.
4. Select the :ref:`section <configlet_levels>` where the configlet is to
   be rendered (SYSTEM TOP, SYSTEM, INTERFACE, FILE, OSPF, FRR). The available
   choices depend on the selected config style.
5. In the **Template Text** field, enter the CLI commands for the custom
   configuration.

   .. important::

      Using a raw text editor (OSX TextEdit, Windows Notepad++) is critical. The
      inclusion/addition of hidden characters cause unforeseen issues
      when deploying configlets.

6. If **Negation Template Text** is required, enter the CLI commands to
   remove the configuration.
7. For file-type configlets, in the **Filename** field, enter the filename.
8. If you want to add another style, click **Add a style**, then add applicable
   details.
9. Click **Create** to create the configlet in the global catalog. It is now
   available for
   :ref:`importing into the catalog of a blueprint <staging_configlets>`.

.. tip::

   Multiple styles are useful in a mixed vendor environment. Create
   one configlet with a single-purpose that contains a style for each vendor.

Configlets on Cumulus
---------------------

Configlets on Cumulus Linux have specifics to be aware of.

**For AOS Version 3.2**

.. sidebar:: What about Time Voyager?

   When using Time Voyager you roll back to a previous AOS commit. As such, it is
   not to be confused with an UNDO function, as things deleted on the last commit
   are re-applied when rolling back.

* On system-type configlets,  ``net commit`` is required in both **Template
  Text** and **Negation Template Text**.
* On interface-type configlets, ``net clu`` (NCLU) syntax is no longer used.
  The config line must match ``/etc/network/interfaces`` syntax exactly.
* On file-type configlets, all file contents are overwritten.
  Removing the configlet will not restore its original content.
* On OSPF-type configlets, configuration will be directly added into the OSPF
  section.
* On FRR-type configlets, all content of the configlet is appended
  to the file ``/etc/frr/frr.conf``.

**For AOS Versions 3.1 and Earlier**

* On system-type configlets,  **net commit** is required in both **Template
  Text** and **Negation Template Text**.
* On interface-type configlets, **net commit** should *not* be used. AOS will
  ensure this is correctly applied.
* On interface-type configlets, the **net add** and specific interface
  designation (e.g. ``net add interface swp1``) should be omitted from both the
  **Template Text** and **Negation Template Text**. This is added by AOS
  automatically.
* On file-type configlets, *all* contents of the file are overwritten.
  Removing the configlet will not restore its original contents.

See :ref:`Cumulus examples <cumulus_configlet>` below.

Configlets and Property Sets
----------------------------
Instead of hard-coding data into a configlet, you can refer to a
:doc:`property set <property_sets>` (key-value pairs).
The example below refers to a property set named NTP SERVER.

.. image:: static/images/configlets/configlet_prop_set_320.png

-----

.. _cumulus_configlet:

Cumulus Linux Configlet Examples
--------------------------------

Cumulus Configlet Example: NTP in Management VRF
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
NTP in Cumulus by default runs within the default VRF. For certain scenarios,
NTP is required to run in the mgmt VRF. It requires stopping the services in
the default VRF and starting services in the mgmt VRF. The following system-level
configlet shows how NTP configuration can be applied on the mgmt VRF on
Cumulus devices :

* Config Style - **Cumulus**
* Section - **SYSTEM**
* Template Text

  .. code-block:: text

     systemctl stop ntp.service
     systemctl disable ntp.service
     systemctl daemon-reload
     systemctl start ntp@mgmt.service
     systemctl enable ntp@mgmt.service

* Negation Template Text

  .. code-block:: text

     systemctl stop ntp.service
     systemctl disable ntp.service
     systemctl daemon-reload
     systemctl start ntp.service
     systemctl enable ntp.service

.. image:: static/images/configlets/cumulus/cumulus_ntp_330a.png

-----

Cumulus Configlet Example: SNMP
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
The following system-level configlet shows how to apply SNMP server
configuration on Cumulus Linux using NCLU. Note the ``net commit`` command:

* Config Style - **Cumulus**
* Section - **SYSTEM**
* Template Text

  .. code-block:: text

     net add snmp-server listening-address 172.20.40.10
     net add snmp-server readonly-community MyCommunity access
     10.0.0.1
     net commit

* Negation Template Text

  .. code-block:: text

     net del snmp-server listening-address 172.20.40.10
     net del snmp-server readonly-community MyCommunity access
     10.0.0.1
     net commit

.. image:: static/images/configlets/cumulus/cumulus_snmp_330a.png

-----

Cumulus Configlet Example: OSPF
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
The following OSPF configlet shows how to modify SPF and LSA timers on
Cumulus Linux. Note configuration will be directly added into the OSPF section.

* Config Style - **Cumulus**
* Section - **OSPF**
* Template Text

  .. code-block:: text

     timers throttle spf {{ospf_spf_delay_msec}}
     {{ospf_spf_hold_msec}} {{ospf_spf_max_msec}}

.. image:: static/images/configlets/cumulus/cumulus_ospf_330a.png

-----

Cumulus Configlet Example: FRR
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
The following FRR configlet shows how to add a static route to VRF SZ_ART_DR.

* Config Style - **Cumulus**
* Section - **FRR**
* Template Text

  .. code-block:: text

     vrf SZ_ART_DR
      ip route 10.90.0.0/16 10.136.118.24

.. image:: static/images/configlets/cumulus/cumulus_frr_330a.png

The FRR configlet will be shown at the bottom of the routing section when
viewing Rendered Config for the device.

.. image:: static/images/configlets/cumulus/cumulus_frr_configlet_render.png
     :width: 500px

-----

Cumulus Configlet Example: Syslog
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
A file-based configlet adds a text file to the system and replaces the contents
of the targeted file with template text from the configlet. As an example it can
be of great use when configuring a syslog server with management VRF enabled.
To add this configuration to Cumulus devices the file written can require double
quotation marks as shown below:

.. code-block:: prompt

   cumulus@switch:~$ cat /etc/rsyslog.d/11-remotesyslog.conf
    ## Copy all messages to the remote syslog server at 192.168.0.254 port 514
    action(type="omfwd" Target="192.168.0.254" Device="mgmt" Port="514"
    Protocol="udp")

To setup such a file-based configlet, precede each double quotation mark in the
configuration with three backslashes. Three backslashes are needed because double
quotes must be escaped and backslashes need escaping too.

The following example shows how a file-type configlet with double quotation marks
should be applied with three backslashes. These are required to escape double-quote.

* Config Style - **Cumulus**
* Section - **FILE**
* Template Text

  .. code-block:: text

     *.* @192.168.0.253:514 #UDP
     *.* @192.168.0.254:514 #UDP
     action(type=\\\"omfwd\\\" Target=\\\"192.168.0.254\\\"
     Device=\\\"mgmt\\\" Port=\\\"514\\\" Protocol=\\\"udp\\\")

* Filename

  .. code-block:: text

     /etc/rsyslog.d/11-remotesyslog.conf

.. image:: static/images/configlets/cumulus/cumulus_syslog_330a.png

-----

Cisco NX-OS Configlet Examples
------------------------------

Cisco NX-OS Configlet Example: Syslog
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
The following system-level configlet shows how syslog configuration can
be applied on NX-OS devices:

* Config Style - **NXOS**
* Section - **SYSTEM**
* Template Text

  .. code-block:: text

     logging server 192.168.0.30
     logging facility local3
     logging trap warning

* Negation Template Text

  .. code-block:: text

     no logging server 192.168.0.30
     no logging facility local3
     no logging trap warning

.. image:: static/images/configlets/nxos/nxos_syslog_330a.png

-----

Arista EOS Configlet Examples
-----------------------------

EOS Configlet Example: NTP
^^^^^^^^^^^^^^^^^^^^^^^^^^
The following configlet configures NTP servers on Arista EOS devices. The
configlet uses property sets for the NTP server IP addresses.

* Config Style - **EOS**
* Section - **SYSTEM**
* Template Text

  .. code-block:: text

     ntp server {{NTP_SERVER_1}}
     ntp server {{NTP_SERVER_2}}

* Negation Template Text

  .. code-block:: text

     no ntp server {{NTP_SERVER_1}}
     no ntp server {{NTP_SERVER_2}}

.. image:: static/images/configlets/eos/eos_ntp_330a.png

-----

EOS Configlet Example: Interface Speed
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
The following configlet applies ‘speed auto’ to an interface. Scope
(devices and interfaces) is set when importing the configlet:

* Config Style - **EOS**
* Section - **INTERFACE**
* Template Text

  .. code-block:: text

     speed auto

* Negation Template Text

  .. code-block:: text

     no speed auto

.. image:: static/images/configlets/eos/eos_interface_330a.png

-----

EOS Configlet Example: OSPF
^^^^^^^^^^^^^^^^^^^^^^^^^^^
The following OSPF configlet shows how to modify **SPF** and **LSA** timers on EOS.
Note the configuration will be added underneath router ospf :

* Config Style - **EOS**
* Section - **OSPF**
* Template Text

  .. code-block:: text

     timers throttle spf {{ospf_spf_delay_msec}}
     {{ospf_spf_hold_msec}} {{ospf_spf_max_msec}}

* Negation Template Text

  .. code-block:: text

     no timers throttle spf

.. image:: static/images/configlets/eos/eos_ospf_330a.png

-----

Juniper Junos Configlet Examples
--------------------------------

Junos Configlet Example: NTP
^^^^^^^^^^^^^^^^^^^^^^^^^^^^
The following system-level configlet shows how NTP configuration can be
applied on Junos devices:

* Config Style - **Junos**
* Section - **SYSTEM**
* Template Text

.. code-block:: text

   system {
     ntp {
       boot-server 10.1.4.1;
       server 10.1.4.2;
     }
   }


.. image:: static/images/configlets/junos/junos_ntp_330a.png


Enterprise SONiC Configlet Examples
-----------------------------------

SONiC Configlet Example: NTP
^^^^^^^^^^^^^^^^^^^^^^^^^^^^
The following SONiC system-level configlet uses the SONiC ``config`` command
to set the NTP server for SONiC using the mgmt VRF.

* Config Style - **SONiC**
* Section - **SYSTEM**
* Template Text

  .. code-block:: text

     sonic-db-cli CONFIG_DB hset NTP\\\\|global vrf mgmt
     config ntp add {{ntp_server}}

* Negation Template Text

  .. code-block:: text

     config ntp del {{ntp_server}}

.. image:: static/images/configlets/sonic/sonic_ntp_330a.png

-----

SONiC Configlet Example: SNMP
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
The following SONiC system-level configlet uses the SONiC ``config`` command to
set the SNMP snmptrap for SONiC using the mgmt VRF.

* Config Style - **SONiC**
* Section - **SYSTEM**
* Template Text

  .. code-block:: text

     config snmptrap modify 2 {{SNMP_SERVER}} -v mgmt -c mypass

* Negation Template Text

  .. code-block:: text

     config snmptrap del 2

.. image:: static/images/configlets/sonic/sonic_snmp.png

-----

SONiC Configlet Example: Syslog
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
The following SONiC system-level configlet uses the SONiC config command to set
the Syslog server for SONiC.

* Config Style - **SONiC**
* Section - **SYSTEM**
* Template Text

  .. code-block:: text

     config syslog add {{syslog_host}}

* Negation Template Text

  .. code-block:: text

     config syslog del {{syslog_host}}

.. image:: static/images/configlets/sonic/sonic_syslog.png

-----

SONiC Configlet Example: FRR
^^^^^^^^^^^^^^^^^^^^^^^^^^^^
The following SONiC FRR configlet shows how to add a static route.

* Config Style - **SONiC**
* Section - **FRR**
* Template Text

  .. code-block:: text

     ip route 4.2.2.2/32 {{static_route_next_hop}}
     ip route 4.2.2.3/32 {{static_route_next_hop}}

.. image:: static/images/configlets/sonic/sonic_frr.png

-----

SONiC Configlet Example: Using ``sonic-cli`` Commands
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
The following SONiC system-level configlet uses the ``sonic-cli`` command to set
the ``delay-restore`` option for SONiC ``mclag``. It is required to use
``sudo -u admin`` at the beginning, ``\\\\`` for spaces in each sonic-cli
command, and to add ``< /dev/console`` to the end.

* Config Style - **SONiC**
* Section - **SYSTEM**
* Template Text

  .. code-block:: text

     sudo -u admin sonic-cli -c config -c mclag\\\\ domain\\\\ 1 -c delay-restore\\\\ 600 < /dev/console

* Negation Template Text

  .. code-block:: text

     sudo -u admin sonic-cli -c config -c mclag\\\\ domain\\\\ 1 -c no\\\\ delay-restore < /dev/console

.. image:: static/images/configlets/sonic/sonic_cli.png

.. _edit_configlet:

Editing Configlet in Global Catalog
===================================
Changes to configlets in the global catalog do not affect configlets that have
already been imported into blueprint catalogs, thereby preventing unintended
changes to blueprints. If the intent is for an existing blueprint to use a
modified configlet, follow the steps in :ref:`Editing Configlet Generators in
Blueprint Catalog <edit_configlet_in_blueprint_catalog>`.

#. From the list view (Design / Configlets) or the details view,
   click the name of the configlet to edit.
#. Make your changes (name, config style, section, template text, negation
   template text, filename, as applicable).
#. Click **Update** (bottom-right) to update the configlet in the global catalog
   and return to the list view.

Deleting Configlet from Global Catalog
======================================
Deleting a configlet removes it from the global catalog. Configlets that were
previously imported into a blueprint are not affected.

#. Either from the list view (Design / Configlets) or the details view,
   click the **Delete** button for the configlet to delete.
#. Click **Delete** to delete the configlet from the global catalog and return to
   the list view.

Troubleshooting
===============

Configlets and Config Deviation
-------------------------------
If an improperly-configured configlet causes AOS deployment errors (when the
command is rejected by the device), a **service config deployment** failure occurs.
In this case, follow the steps below to resolve the anomaly.

#. From the blueprint, navigate to **Staged / Catalog / Configlets** and
   delete the configlet.
#. Click **Uncommitted** and commit the change. The configuration deviation
   persists showing an empty expected config. This happens because when a
   deployment fails, there is no golden config. The golden config is the running
   config of the device after *successful* deployment of AOS-rendered config.
   The expected config has no relationship to the AOS-rendered config.
#. Click **Dashboard**, then click **Config Dev.** (in the **Deployment Status**
   section).
#. Click the node name, then select **Accept Changes** to notify AOS that the
   failure can be ignored.
