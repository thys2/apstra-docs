=================
Security Policies
=================

Segmentation and Group Based Policies
=====================================
AOS Segmentation and Group Based Policies (GBP) enable you to create more granular
security policies. Reachability and security jointly define connectivity. In order
for two endpoints to be “connected” they have to be reachable (correct forwarding
state in the network) and the security policy has to allow it. In particular,
there is a requirement to specify security policies between L2 and L3 domains,
which are already modeled by AOS, but also between more granular L2/L3 IP
endpoints.

Segmentation and Group Based Policies have many available use cases.

* Customers have multiple vendor types and want simplified security management
* Customers wish to isolate VMs and control the allowed application flows between
  VNs.

AOS provides policy constructs to express security/segmentation intent with
different levels of granularity. Granularity level is a function of reference
design, but policy constructs are able to express a wide range of granularity
levels.

Endpoints/groups that are a possible subject of security policies are as follows:

* VN (virtual networks, contain subnet)
* IPE (internal IP endpoints, associated with VN, contain IP /32 address)
* IPECG (IP endpoints custom groups)
* EIPE (External IP endpoints, contain /32 or subnet)
* EIPCG (External IP endpoint custom groups)
* SZ (security zone, logical collection of all VNs and IPE)

You can associate one or more security policy with endpoints/groups.
In addition you can indicate if you want to log actions taken by specific rules.

Policy controls traffic in and out of the endpoint/group to which it is applied,
in this case, the subnet or IP endpoint. Policy is expressed via “policy” node
type, which contains the description of the policy and its association with
endpoints/groups using relationships “from” and “to”, indicating the
directionality. For a bi-directional security policy, two instances of the policy
must be created, one for each direction. A policy contains a list of rules. An
actual rule consists of 5-tuple (Source IP, Destination IP, Source Port,
Destination Port, Protocol) and an action (deny, deny+log, allow, allow+log,
log_only). Each subnet/IP endpoint can have one or more security policies applied
to it.

Source IP and Destination IP are derived from properties of endpoints/groups
attached to the policy.

Actions that include “logging” merely indicate that ACL is configured to log
matches using whatever mechanism is supported on the device. Parsing these logs
is outside scope of this document.

Policies consist of a set of rules that are stateless, meaning responses to
allowed inbound traffic are subject to the rules for outbound traffic (and vice
versa).

Given that any endpoint can have more than one policy applied to it, it is important
to deal with composition of these policies as ordering of rules has an impact on the
behavior. In addition, there is implicit hierarchy between IPE, VN and SZ
and policies applied at different levels of hierarchy also need to be considered
from the composition perspective.

Composition (in this case ordering of rules) matters when applicable policies have
“conflicting” rules which are defined as rules that have:

* Overlapping match sets (there is a packet whose 5-tuple matches both rules)
* Different action (for example “allow” vs “deny”)

Match sets are non-overlapping when the condition in at least one field in the
5-tuple is disjoint (non-overlapping) between the rules. Or to put it differently,
match sets are overlapping when the conditions set in all the fields in the 5-tuple
are overlapping.

When conflicting rules are present, there are two situations. First one is when one
rule’s match set contains the other’s match set (full containment case). In this
situation the ordering can be driven by a policy knob such as:

* More specific rule is executed first (“exception” focus/mode)
* Less specific executes first (“override” focus/mode)

There is a situation when there is a full containment situation between the rules
but the action is the same. In this case, there is potential for compression by
using less specific rule and more specific rule becomes what is called a
“shadow” rule. AOS alerts you at the time conflicting rules are detected and
what is the resolution.

AOS built in hierarchy and the nature/semantic of endpoints/groups also helps with
identifying the presence of conflicting rules. The following is the analysis on a
case by case basis:

* Rules in policies between different pairs of IPEs (even if one IPE is common to
  both pairs) are non-overlapping given that the pairs of IP addresses are
  different. This will cause disjoint match set from the sIP/dIP perspective
  (different “IP signature”).
* Rules in policies between the same IPEs can be overlapping on other (e.g.
  Destination Port) fields and AOS will check for this.
* Rules in policies between different pairs of VNs (even if one VN is common to
  both pairs) are non-overlapping given that the pairs of subnets are different.
  This will cause disjoint match set from the Source IP / Destination IP perspective
  (different “IP signature”).
* Rules in policies between the same VNs can be overlapping on other (e.g.
  Destination Port) fields and AOS will check for this.
* When IPECG are used they are essentially resulting in a set of IPE pairs so the
  above discussion related to IPE pairs applies.
* Rules in policies between a pair of IPEs and pair of parent VNs have containment
  from IP signature perspective. AOS will analyze Destination Port / Protocol
  overlap and classify it as full-containment or non-full-containment conflict.
* Rules in policies between pair of IPEs and pair of VNs where at least one VN is
  not parent are non conflicting (different IP signature).
* Rules in policies between pair of IPEs and IPE-VN pair where VN is a parent have
  full containment from IP signature perspective so the remaining fields will be
  analyzed by AOS.
* Rules in policies that contain EIPE or EIPECG have to be analyzed from IP
  signature perspective as external points are not bound by any hierarchical
  assumptions.
* SZ is simply a set of VNs and IPE endpoints so the above discussions apply.

In order to make composition tractable, both from an analysis point of view as well
as from comprehending the resulting composition it may be useful to limit the
number of security policies that may apply to any given endpoint/group.

AOS implements this feature by doing the following:

* Automated creation and deployment of ACLs to Leaf switches (enforcement points).
* Inter VLAN/VXLAN filtering.
* Automated rule ordering and conflict resolution.
* Config validation - useful for compliance.
* Adding a new VXLAN Endpoint (ex. Add rack or add leaf to VN) automatically places
  the ACL on the VN interface.
* Adding a new External Router External Connectivity Point (ECP) (enforcement
  point) automatically places ACL for external endpoint groups.

Endpoint creation and management is done in the **Virtual** tab of an AOS Blueprint.
Task information can be found in :doc:`Endpoints <endpoints>`.

Security Policies creation and management is done in the **Policies** tab of an AOS
Blueprint.

.. versionadded:: 3.0
    AOS supports the creation of Endpoints to be used with Segmentation and Group
    Based Policies (GBP).

.. versionadded:: 3.1
    AOS adds support for shadow relationship, user-defined
    conflict resolution, selective policy enablement, searching for networks
    impacted by policy, security policy search, per Blueprint settings and views of
    policy changes between staged and active Blueprints.

.. note::
    As of AOS 3.1, Segmentation and Group Based Policies (GBP) is only supported on
    Cisco NX-OS and Arista EOS physical network devices.

.. warning::
    As of AOS 3.1, Segmentation and Group Based Policies (GBP) is not supported on
    Blueprints with IPv6 Applications enabled. It is only supported on Layer2,
    IPv4-only Blueprints.

.. note::
    Controls inter Virtual Network traffic (ACLs on SVIs) or external to internal
    traffic (ACLs in border leafs).

.. note::
    For External Endpoints, ACL enforcement only on border leafs.

Recommended Workflow
====================
The necessary tasks for creating Security Policies for AOS Segmentation and Group
Based Policies (GBP) are listed below. AOS renders Access Control Lists (ACL) in
the appropriate device syntax. ACLs will be applied on Enforcement Points.

#. Create Security Zones
#. Create Virtual Networks
#. Create Endpoints (Internal vs External)
#. Create Endpoint Groups (Internal vs External)
#. Create Policies
#. Commit Changes

Creating a Security Policy
==========================

.. sidebar:: Screen Changes in Version 3.2

   The Policies screen in the web interface has changed for AOS version
   3.2. Interface Policies has been added as a new policy type, and Security Policy
   Search, Conflicts and Settings has moved to a sidebar.

#. From the **Policies** view of the **Staged** Blueprint, click
   **Security Policies** (if not already active) to see the list of Security
   Policies.
#. Click **Create Security Policy** to see the dialog for creating a Security
   Policy.

   .. image:: static/images/blueprint_policies_tasks/security_policy_create1_320.png

Common Parameters
-----------------

#. Enter a name (32 characters or fewer, underscore, dash and alphanumeric
   characters only), description (optional), and tags (optional).
#. The Security Policy is enabled by default. If you want to disable it, click
   the toggle to turn it off.

Application Points
------------------

#. Select a source point type, then select the source point (that was previously
   created) from the drop-down list.
#. Select a destination point type, then select the destination point (that was
   previously created) from the drop-down list.

Rules
-----

#. If you would like to create a whitelist-type policy, click **Permit All** to
   automatically create the rule.
#. If you would like to create a blacklist-type policy, click **Deny All** to
   automatically create the rule.
#. To manually create other rules, click **Add Rule** to open the dialog for
   adding a rule.
#. Enter a name (32 characters or fewer, underscore, dash and alphanumeric
   characters only) and description (optional).
#. Select an action from the drop-down list: **Deny**, **Deny & Log**, **Permit**
   or **Permit & Log**

   .. note::
         Log configuration is local to the network device. This logging will
         be on the device and not on AOS at this time.

#. Select a protocol from the drop-down list: **TCP**, **UDP**, **IP**, or
   **ICMP**.
#. If you select **TCP** or **UDP**, fields appear for **Source port** and
   **Destination port**. You can enter specific numeric ports, and/or port-ranges
   (such as, 8000-8080,9000,9092). If you have previously created
   :doc:`TCP/UDP port aliases<tcp_udp_alias>`, they will appear in the
   drop-down list for selection.
#. To add an additional rule, click **Add Rule** and enter rule details as above.
#. You can adjust the order of the rules by clicking the **Move up** or
   **Move Down** buttons in each rule.
#. When you're finished adding and ordering the rules, click **Create**.

Errors
------
If errors occur during creation, AOS highlights the policy in red.
Click the **Show errors** button to see error details.

.. image:: static/images/blueprint_policies_tasks/blueprint_policies_tasks_3_310.png

.. image:: static/images/blueprint_policies_tasks/blueprint_policies_tasks_4_310.png

After resolving any Security Policy errors, the policy is no longer highlighted
red and the **Errors** field is blank.

.. image:: static/images/blueprint_policies_tasks/blueprint_policies_tasks_5_310.png

A Security Policy can be disabled and enabled with the **Enabled** toggle
(as of AOS version 3.1).

You can also clone or delete a Security Policy.

Viewing and Searching Security Policies
=======================================
.. versionadded:: 3.1
    Beginning in AOS 3.1, the user can view and serch for affected Security Policy
    endpoints and impacted IP subnets.

**Listing Security Policies**

From the **Policies** view of the **Staged** Blueprint, click
**Security Policies** (if not already active) to see the list of Security
Policies. If no policies have been created, the list will be empty.

.. image:: static/images/blueprint_policies_tasks/blueprint_policies_tasks_8_310.png

**Viewing Affected Security Policy Endpoints and Impacted IP Subnets**

From the list view, click on a **Source Application Point** or
**Destination Application Point**. The endpoints and subnets (if applicable)
are displayed.

.. image:: static/images/blueprint_policies_tasks/blueprint_policies_tasks_9_310.png

**Searching for Security Policies by Associated Source or Destination Points**

From the **Policies** view of your **Staged** Blueprint, click
**Security Policy Search**. For IPv4 subnet, you must search for the exact
network subnet configured in the policy.

.. image:: static/images/blueprint_policies_tasks/blueprint_policies_tasks_10_310.png

Security Policy Conflicts
=========================
AOS automatically detects and resolves Security Policy conflicts when possible.
AOS gives priority to the "most specific" policy and applies it before "less
specific" policies. If multiple Security Policies are created that have
conflicting rules, AOS notifies you of the conflicts.

.. image:: static/images/blueprint_policies_tasks/blueprint_policies_tasks_6_310.png

For more information about the conflicting policies, click **Conflicts**.

.. image:: static/images/blueprint_policies_tasks/blueprint_policies_tasks_7_310.png

If AOS can automatically resolve the conflict, it tags the conflict as
**Autoresolving**. The policy with the most specific rule will be given priority.

Configuring Per Blueprint GBP Settings
======================================
.. versionadded:: 3.1
    Two global settings are introduced. You can configure whether a more/less
    specific rule goes first in auto-resolution. You can configure a policy
    default action.

From the **Policies** view of your **Staged** Blueprint, click **Settings**.

.. image:: static/images/blueprint_policies_tasks/blueprint_policies_tasks_11_310.png

Configure **Conflicts resolution** as one of:
   More specific first
      The more IP specific policy is used (default).

   More generic first
      The less IP specific policy is used.

   Disabled
      Conflict resolution is disabled.

Configure **Default action** as one of:
   Permit
      The default action for all policies is to permit all traffic.

   Permit & Log
      The default action for all policies is to permit and log all traffic.

   Deny
      The default action for all policies is to deny all traffic.

   Deny & Log
      The default action for all policies is to deny and log all traffic.

Blueprint GBP Changed Uncommitted Diffs
=======================================
.. versionadded:: 3.1
    After you've made GBP changes, the AOS Blueprint
    "Uncommited" tab shows differences for any pending GBP changes.

After making GBP changes to policies for an AOS Blueprint, the pending
policy changes in the AOS Blueprint are shown in the "Uncommited" tab.

.. image:: static/images/blueprint_policies_tasks/blueprint_policies_tasks_12_310.png

After review, you can commit these changes by clicking on the "Commit" icon.
