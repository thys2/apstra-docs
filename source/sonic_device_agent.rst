==================
SONiC Device Agent
==================
This describes the process of manual AOS agent installation on supported SONiC
devices. For the recommended method using the web interface, refer to
:doc:`Device Agents <device_agents>`.

.. important::
    Only in rare exceptions is it needed to follow the manual process of
    agent installation. In almost all cases agents should be installed by creating
    system agents in the web interface. Installing agents manually is more bespoke,
    more effort, and prone to user error.

    An in-depth understanding of the various device states, configuration stages,
    and agent operations is required when attempting manual agent installation.

    When in doubt, contact :doc:`Apstra Global Support <support>`.

SONiC Support
=============

Starting in AOS version 3.3.0a, the only Software for Open Networking in the
Cloud (SONiC) distribution is SONiC Enterprise.

The SONiC device agent currently manages the following files in the filesystem:

* ``/etc/sonic/config_db.json`` - The main configuration file for SONiC,
  specifying interfaces, IP addresses, port breakouts etc.
* ``/etc/sonic/frr/frr.conf`` - frr.conf contains all of the routing application
  configuration for BGP on the device.

.. warning::

  The user should not edit the ``config_db.json`` or ``frr.conf`` files manually
  at any time, before or after AOS Device System Agent installation. The AOS Agent
  will overwrite any existing configuration in these files.

Manual SONiC Device Management IP Configuration
===============================================

SONiC automatically creates a management VRF for the "eth0" management
interface. By default, "eth0" will get a DHCP address from the management
network. In most cases, no management configuration should be needed.

However, if the user needs to manually configure a SONiC device management IP
address, the user **must** configure this using the ``sonic-cli`` interface.

.. code-block:: bash

  admin@sonic:~$ sonic-cli
  sonic# show interface Management 0
  eth0 is up, line protocol is up
  Hardware is MGMT
  Description: Management0
  Mode of IPV4 address assignment: not-set
  Mode of IPV6 address assignment: not-set
  IP MTU 1500 bytes
  LineSpeed 1GB, Auto-negotiation True
  Input statistics:
          11 packets, 1412 octets
          0 Multicasts, 0 error, 4 discarded
  Output statistics:
          31 packets, 5290 octets
          0 error, 0 discarded
  sonic# configure terminal
  sonic(config)# interface Management 0
  sonic(conf-if-eth0)# ip address 192.168.59.7/24 gwaddr 192.168.59.1
  sonic(conf-if-eth0)# exit
  sonic(config)# exit
  sonic# write memory
  sonic# show interface Management 0
  eth0 is up, line protocol is up
  Hardware is MGMT
  Description: Management0
  IPV4 address is 192.168.59.7/24
  Mode of IPV4 address assignment: MANUAL
  Mode of IPV6 address assignment: not-set
  IP MTU 1500 bytes
  LineSpeed 1GB, Auto-negotiation True
  Input statistics:
          18 packets, 2494 octets
          0 Multicasts, 0 error, 6 discarded
  Output statistics:
          38 packets, 6455 octets
          0 error, 0 discarded
  sonic#

The Managment VRF can be checked from the SONiC Linux command line.

.. code-block:: bash

  admin@leaf1:~$ show mgmt-vrf

  ManagementVRF : Enabled

  Management VRF interfaces in Linux:
  48: mgmt: <NOARP,MASTER,UP,LOWER_UP> mtu 65536 qdisc noqueue state UP mode DEFAULT group default qlen 1000
      link/ether 8e:32:49:6c:ec:71 brd ff:ff:ff:ff:ff:ff promiscuity 0
      vrf table 5000 addrgenmode eui64 numtxqueues 1 numrxqueues 1 gso_max_size 65536 gso_max_segs 65535
  2: eth0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc pfifo_fast master mgmt state UP mode DEFAULT group default qlen 1000
      link/ether 52:54:00:c1:ac:1b brd ff:ff:ff:ff:ff:ff
  49: lo-m: <BROADCAST,NOARP,UP,LOWER_UP> mtu 1500 qdisc noqueue master mgmt state UNKNOWN mode DEFAULT group default qlen 1000
      link/ether c2:39:a7:6c:4b:be brd ff:ff:ff:ff:ff:ff
  admin@leaf1:~$ show mgmt-vrf routes

  Routes in Management VRF Routing Table:
  default via 172.20.9.1 dev eth0 metric 201
  broadcast 127.0.0.0 dev lo-m proto kernel scope link src 127.0.0.1
  127.0.0.0/8 dev lo-m proto kernel scope link src 127.0.0.1
  local 127.0.0.1 dev lo-m proto kernel scope host src 127.0.0.1
  broadcast 127.255.255.255 dev lo-m proto kernel scope link src 127.0.0.1
  broadcast 172.20.9.0 dev eth0 proto kernel scope link src 172.20.9.7
  172.20.9.0/24 dev eth0 proto kernel scope link src 172.20.9.7
  local 172.20.9.7 dev eth0 proto kernel scope host src 172.20.9.7
  broadcast 172.20.9.255 dev eth0 proto kernel scope link src 172.20.9.7
  admin@leaf1:~$

Manual AOS Agent Installation
=============================
The section below is a guide on manually installing the SONiC AOS device agent.

#. Download the AOS agent with the ​"sudo cgexec -g l3mdev:mgmt curl -o
   /tmp/aos.run -k -O https://{{aos-ip-address}}/
   device_agent_images/aos_device_agent_{{aos-version}}-{{aos-build}}.runcurl​​`"
   command.

   .. code-block:: prompt

      admin@sonic:~$ ​sudo cgexec -g l3mdev:mgmt curl -o /tmp/aos.run -k -O
      https://172.20.74.3/device_agent_images/aos_device_agent_3.3.0a-93.run​​
        % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                    Dload  Upload   Total   Spent    Left  Speed
      100  111M  100  111M    0     0   328M      0 --:--:-- --:--:-- --:--:--  328M
      admin@sonic:~$

#. Install the AOS agent with the ``sudo /bin/bash /tmp/aos.run -- --no-start``
   command.

   .. code-block:: prompt

      admin@sonic:~$ ​sudo /bin/bash /tmp/aos.run -- --no-start​​
      Verifying archive integrity... All good.
      Uncompressing AOS Device Agent installer  100%
      + set -o pipefail
      +++ dirname ./agent_installer.sh
      ++ cd .
      ++ pwd
      + script_dir=/tmp/selfgz334323135
      + systemd_available=false
      ++ date
      + echo 'Device Agent Installation : Mon' Oct 19 19:02:01 UTC 2020
      Device Agent Installation : Mon Oct 19 19:02:01 UTC 2020
      + echo

      + UNKNOWN_PLATFORM=1
      + WRONG_PLATFORM=1
      + CANNOT_EXECUTE=126
      + '[' 0 -ne 0 ']'
      + arg_parse --no-start
      + start_aos=True
      + [[ 1 > 0 ]]
      + key=--no-start
      + case $key in
      + start_aos=False
      + shift
      + [[ 0 > 0 ]]
      + supported_platforms=(["centos"]="install_centos" ["eos"]="install_on_arista" ["nxos"]="install_on_nxos" ["cumulus"]="install_sysvinit_deb" ["opx"]="install_systemd_deb opx" ["trusty"]="install_sysvinit_deb" ["xenial"]="install_sysvinit_deb" ["icos"]="install_sysvinit_rpm" ["snaproute"]="install_sysvinit_deb" ["simulation"]="install_sysvinit_deb" ["sonic"]="install_systemd_deb sonic" ["bionic"]="install_sysvinit_deb")
      + declare -A supported_platforms
      ++ /tmp/selfgz334323135/aos_get_platform
      + current_platform=sonic
      + installer='install_systemd_deb sonic'
      + [[ -z install_systemd_deb sonic ]]
      +++ readlink /sbin/init
      ++ basename /lib/systemd/systemd
      + [[ systemd == systemd ]]
      + systemd_available=true
      + [[ -x /etc/init.d/aos ]]
      + echo 'Stopping AOS'
      Stopping AOS
      + true
      + systemctl stop aos
      + install_systemd_deb sonic
      ++ pwd
      + local pkg_dir=/tmp/selfgz334323135/sonic
      + install_deb /tmp/selfgz334323135/sonic
      + local pkg_dir=/tmp/selfgz334323135/sonic
      + dpkg -s aos-device-agent
      + dpkg --purge aos-device-agent
      (Reading database ... 34189 files and directories currently installed.)
      Removing aos-device-agent (3.3.0a-93) ...
      Purging configuration files for aos-device-agent (3.3.0a-93) ...
      Processing triggers for systemd (232-25+deb9u12) ...
      + dpkg -i /tmp/selfgz334323135/sonic/aos-device-agent-3.3.0a-93.amd64.deb
      Selecting previously unselected package aos-device-agent.
      (Reading database ... 34180 files and directories currently installed.)
      Preparing to unpack .../aos-device-agent-3.3.0a-93.amd64.deb ...
      Unpacking aos-device-agent (3.3.0a-93) ...
      Setting up aos-device-agent (3.3.0a-93) ...
      Synchronizing state of aos.service with SysV service script with /lib/systemd/systemd-sysv-install.
      Executing: /lib/systemd/systemd-sysv-install enable aos
      /var/lib/dpkg/info/aos-device-agent.postinst: line 7: /usr/sbin/aosconfig: No such file or directory
      Processing triggers for systemd (232-25+deb9u12) ...
      + mkdir -p /opt/aos
      + cp aos_device_agent.img /opt/aos
      + post_install_common
      + /etc/init.d/aos config_gen
      + [[ False == \T\r\u\e ]]
      + true
      + systemctl enable aos
      Synchronizing state of aos.service with SysV service script with /lib/systemd/systemd-sysv-install.
      Executing: /lib/systemd/systemd-sysv-install enable aos
      admin@sonic:~$


#. Set the IP of the AOS server and enable configuration service by
   editing ``/etc/aos/aos.conf`` with the ``sudo vi /etc/aos/aos.conf`` command.

   * For the following, replace "aos-server" with the IP address or valid FQDN of
     your AOS server.

     .. code-block:: prompt

        [controller]
        # <metadb> provides directory service for AOS. It must be configured properly
        # for a device to connect to AOS controller.
        metadb = tbt://aos-server:29731

   * For example

     .. code-block:: prompt

        [controller]
        # <metadb> provides directory service for AOS. It must be configured properly
        # for a device to connect to AOS controller.
        metadb = tbt://​​172.20.74.3​​:29731

   * For the following, add the management interface (usually eth0).

     .. code-block:: prompt

        # <interface> is used to specify the management interface.This is currently
        # being used only on server devices and the AOS agent on the server device will
        # not come up unless this is specified.
        interface = eth0

   * For the following, set "enable_configuration_service" to **1** to enable
     "full control" mode from AOS.

     .. code-block:: prompt

        [service]
        # AOS device agent by default starts in "telemetry-only" mode.Set following
        # variable to 1 if you want AOS agent to manage the configuration of your
        # device.
        enable_configuration_service = 1

   * Add the following, "credential" configuration with "username = " and the
     local Linux user to be used for the AOS agent (usually "admin").

     .. code-block:: prompt

        [credential]
        username = admin

#. Start the AOS agent with the ``sudo service aos start`` command and check its
   status with the ``sudo service aos status`` command.

   .. code-block:: prompt

      admin@sonic:~$ sudo service aos start
      admin@sonic:~$ sudo service aos status
      ● aos.service - AOS Device Agent
         Loaded: loaded (/etc/systemd/system/aos.service; enabled; vendor preset: enabled)
         Active: active (running) since Mon 2020-10-19 19:22:50 UTC; 19s ago
        Process: 23375 ExecStart=/etc/init.d/aos start (code=exited, status=0/SUCCESS)
       Main PID: 23521 (tacspawner)
          Tasks: 22 (limit: 4915)
         Memory: 367.1M
            CPU: 15.278s
         CGroup: /system.slice/aos.service
                 ├─23521 tacspawner --daemonize=/var/log/aos/aos.log --pidfile=/host_var_run/aos.pid --name=5254001B4A4D --hostname=5254001B4A4D --domainSocket=aos_spawner_sock --hostS
                 ├─23528 tacsysdb --sysdbType=leaf --agentName=5254001B4A4D-LocalTasks-5254001B4A4D-0 --partition= --storage-mode=persistent --eventLogDir=. --eventLogSev=
                 ├─23541 /usr/bin/python /usr/bin/aos_agent --class=aos.device.common.ProxyCountersAgent.ProxyCountersAgent --name=CounterProxyAgent device_type=Sonic serial_number=@(S
                 ├─23544 /usr/bin/python /usr/bin/aos_agent --class=aos.device.sonic.SonicTelemetryAgent.SonicTelemetryAgent --name=DeviceTelemetryAgent serial_number=@(SYSTEM_UNIQUE_I
                 ├─23551 /usr/bin/python /usr/bin/aos_agent --class=aos.device.common.DeviceKeeperAgent.DeviceKeeperAgent --name=DeviceKeeperAgent serial_number=@(SYSTEM_UNIQUE_ID)
                 ├─23617 /usr/bin/python /usr/bin/aos_agent --class=aos.device.common.ProxyDeploymentAgent.ProxyDeploymentAgent --name=DeploymentProxyAgent device_type=Sonic serial_num
                 ├─25007 sh -c aos_host_exec show interface transceiver eeprom Ethernet12 2>&1
                 └─25010 /usr/bin/python /usr/bin/show interface transceiver eeprom Ethernet12
      admin@sonic:~$

#. The SONiC device will be listed in **Device / Managed Devices** in the AOS web
   interface and can be acknowledged and assigned to a blueprint.

   .. image:: static/images/agents/sonic_manual_agent_330a.png

Manual AOS Agent Uninstallation
===============================
The section below is a guide on manually uninstalled the SONiC AOS device agent.

#. Stop the AOS agent with the ``sudo service aos stop`` command.

   .. code-block:: prompt

      admin@sonic:~$ sudo service aos stop
      admin@sonic:~$

#. Uninstall the AOS agent with
   the ``sudo dpkg --purge --force-all aos-device-agent`` command.

   .. code-block:: prompt

      admin@sonic:~$ sudo dpkg --purge --force-all aos-device-agent
      (Reading database ... 34189 files and directories currently installed.)
      Removing aos-device-agent (3.3.0a-93) ...
      Purging configuration files for aos-device-agent (3.3.0a-93) ...
      Processing triggers for systemd (232-25+deb9u12) ...
      admin@sonic:~$

#. Remove remaining AOS files with the "sudo rm -fr /etc/aos /var/log/aos
   /mnt/persist/.aos /opt/aos /run/aos /run/lock/aos /tmp/aos_show_tech
   /usr/sbin/aos*" command.

   .. code-block:: prompt

      admin@sonic:~$ sudo rm -fr /etc/aos /var/log/aos /mnt/persist/.aos /opt/aos /run/aos /run/lock/aos /tmp/aos_show_tech /usr/sbin/aos*
      admin@sonic:~$
