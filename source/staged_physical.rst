========
Physical
========

You can build the network from any of the physical views (topology, nodes, links,
racks, and on 5-stage topologies, pods). The topology view is shown by default.

.. toctree::
   :maxdepth: 2

   build_physical
   staged_topology
   staged_nodes
   links
   racks
   staged_pods
