=========
OS Images
=========
In preparation for :doc:`upgrading device operating systems <dos_upgrade>` (DOS),
OS images (obtained from device vendors) must be registered with AOS.

Registering Device OS Image
===========================

.. image:: static/images/device_management/os_images_330.png

#. From the AOS web interface, navigate to **Devices / System Agents / OS Images**,
   then click **Register OS Image** (top-right).
#. Select the platform from the drop-down list (EOS, CUMULUS, NXOS, JUNOS), then
   enter a description.
#. Proceed by either :ref:`uploading an image <upload_image>` directly to the
   AOS server or by :ref:`providing a URL <provide_url>` download link pointing
   to an image file on another HTTP server.

.. _upload_image:

Method One: Upload Image
------------------------

.. image:: static/images/device_management/os_images_upload_330.png

#. Select **Upload Image**, then either click **Choose File** and navigate to the
   image on your computer, or drag and drop the image from your computer into the
   dialog window and click **Open**.
#. :ref:`Add a checksum <add_checksum>` (optional).
#. Click **Upload** to upload and register the image with AOS.

.. _provide_url:

.. sidebar:: HTTP URLs Only

    Only HTTP URLs are supported. HTTPS, FTP, SFTP, SCP; others are not.

Method Two: Provide Image URL
-----------------------------
If another HTTP server is accessible to the devices being upgraded via their
network management port, you can register the Device OS Image instead of uploading
it.

.. image:: static/images/device_management/os_images_url_330.png

#. Select **Provide Image URL**.
#. Enter the Image URL that is pointing to the image on another server.
#. :ref:`Add a checksum <add_checksum>` (optional).
#. Click **Register** to register the URL in AOS.

.. _add_checksum:

Adding Checksum
---------------
You have the option of adding a checksum before uploading or registering an OS
image. After the image is uploaded to the device being upgraded, AOS verifies
the checksum. If the checksum is incorrect, AOS stops the upgrade before rebooting
the device. The type of checksum is determined by the platform:

* Arista EOS - SHA512 (128 characters)
* Cisco NX-OS - SHA512 (128 characters)
* Cumulus Linux - MD5 (32 characters)
* Enterprise SONiC - MD5 (32 characters)

If the OS Image Vendor provides a checksum file, we recommend that you download it
and copy the checksum to the Checksum field. If a checksum file is not available,
you can generate a checksum with the Linux **md5sum** or **shasum** commands, as
applicable, or  with equivalent programs.

.. code-block:: prompt

    $ shasum -a 512 EOS-4.20.11M.swi
    dbfd28d3597777a6ee5946b52277205fc714e11ab992574b7ef1156ffcd6e379979979f8c009f665fc21212e4d38d1794a412d79bab149f859aa72be417c0975  EOS-4.20.11M.swi
    $

.. code-block:: prompt

    $ md5sum cumulus-linux-3.7.5-bcm-amd64.bin
    MD5 (cumulus-linux-3.7.5-bcm-amd64.bin) = 8ffe069651cf4ffe8cfbe1162491761a
    $

Editing OS Image
================

.. important::

    AAA authentication must be disabled before proceeding with the new Image
    upgrade, otherwise it will lead to failure.

#. From the list view (Devices / System Agents / OS Images) click the **Edit**
   button for the OS Image to edit.
#. Make your changes.
#. Click **Update** to update the OS image.

Deleting OS Image
=================
#. From the list view (Devices / System Agents / OS Images) click the **Delete**
   button for the OS Image to delete.
#. Click **Delete** to delete the OS image.
