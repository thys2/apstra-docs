========================
LLDP (Cabling) Telemetry
========================
Every node in AOS is part of intent, so AOS knows what should be connected on
each link, what its neighbor hostname should be, and what its neighbor interface
should be. Physical cabling and links must match the specified intent. Any
deviations result in anomalies that must be corrected by either recabling to match
the Blueprint or by modifying the Blueprint to match cabling already in place.

Viewing LLDP Telemetry
======================

#. Navigate to **Devices / Telemetry** to see the device telemetry list view.

   .. image:: static/images/telemetry/lldp1_list_view_322.png

#. Click **LLDP** to see the detailed device list.

   .. image:: static/images/telemetry/lldp2_lldp_322.png

#. Click an individual device name to see its telemetry page, then click
   **LLDP** to see LLDP telemetry.

   .. image:: static/images/telemetry/lldp3_telemetry_322.png

#. You can locate the details of any LLDP violations against intent here and
   attend to them.
