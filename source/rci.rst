===========
Root Causes
===========
Root Cause Identification (RCI) is a technology integrated into AOS that
automatically determines root causes of complex network issues. RCI leverages the
AOS datastore for realtime network status, and automatically correlates telemetry
with each active blueprint intent.

Enabling Root Cause Analysis
============================
#. From the blueprint, navigate to  **Active / Root Causes**. (formerly known as
   *ARCA*).

   .. image:: static/images/blueprints/active/root_causes/root_causes_overview_330.png

#. Click **Enable Root Cause Analysis**.
#. Enter a **Trigger Period** or leave the default, and click **Create**
   to enable root cause analysis and return to the list view.

Viewing Root Cause Analysis
===========================
From the blueprint, navigate to **Active / Root Causes**, and click the model name
**connectivity** in the list.

.. image:: static/images/rci/rci_detail.png

Root cause analysis runs periodically. Each time it runs, it produces zero or more
root causes. Each root cause has associated detection timestamp, context, a
human-readable description, and a list of symptoms caused by the root cause. Each
symptom has associated context and a human-readable description.

AOS version 3.0 added RCI Connectivity Fault Model, which identifies any miscabled
leaf/spine links. AOS correlates faults between neighbor down
(with UP/UP interfaces) and intended LLDP.
