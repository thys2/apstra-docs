====================================================
Hypervisor Missing LLDP Config Probe (Virtual Infra)
====================================================

Purpose
   Detect virtual infra hosts that are not configured for LLDP. (Formerly known
   as Virtual Infra missing LLDP config).

Source Processor
   Hypervisor NIC LLDP Config (:doc:`generic graph collector <processor_generic_graph_collector>`)
      output stage: Hypervisor NIC LLDP config (discrete state set)
      (generated from graph)

Additional Processor(s)
   LLDP config by switch (:doc:`match count <processor_match_count>`)
      input stage: Hypervisor NIC LLDP config

      output stage: LLDP config by switch (number set)

   Switches missing LLDP config (:doc:`range <processor_range>`)
      input stage: LLDP config by switch

      output stage: Switches missing LLDP config anomaly (discrete state set)

Example Usage
   **VMware Integration** - If LLDP information is missing on ToR connected to
   physical ports on ESXi, AOS raises an anomaly.
