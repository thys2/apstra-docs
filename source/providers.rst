=========
Providers
=========

Provider Overview
=================
AOS supports Role-based Access Control (RBAC) for logging into the AOS server.
RBAC servers in AOS are remote network servers that authenticate and authorize
network access based on roles assigned to individual users within an enterprise
(The accounting part of AAA is not included). If a user's group in the RBAC server
is not specified, or if the provider group is not mapped to any user roles, that
user cannot log in. This restriction avoids security issues by ignoring users
without mapped groups. AOS users can be authenticated and authorized using the
following protocols: LDAP, Active Directory, TACACS+, and RADIUS. See the
**Creating** sections below for more information about individual
protocols.

From the AOS web interface, navigate to **External Systems / Providers**.

.. image:: static/images/external_systems/providers_330a.png

.. sidebar:: Cloning Provider

   Instead of entering all details for a new provider,
   you can clone an existing one, give it a new name and customize it.

.. _create_provider:

Creating LDAP Provider
======================
Lightweight Directory Access Protocol (LDAP)

#. From the AOS web interface, navigate to **External Systems / Providers**,
   then click **Create Provider**.
#. Enter a **Name** (64 characters or fewer), select **LDAP**, and if you want
   LDAP to be the active provider, toggle on **Active?**.
#. For **Connection Settings**, enter/select the following:

   * **Port** - The TCP port - LDAP: **389**, LDAPS: **636**
   * **Hostname FQDN IP(s)** - The fully qualified domain name (FQDN) or IP
     address of the LDAP server. For high availability (HA) environments, specify
     multiple LDAP servers using the same settings. If the first server cannot be
     reached, connections to succeeding ones are attempted in order.

#. For **Provider-specific Parameters** enter/select the following,
   as appropriate:

   * **Groups Search DN** - The LDAP Distinguished Name (DN) path for the
     RBAC Groups Organizational Unit (OU)
   * **Users Search DN** - The LDAP Distinguished Name (DN) path for the
     RBAC Users Organization Unit (OU)
   * **Bind DN** - The LDAP Distinguished Name (DN) path for the active server
     user that the AOS server will connect as
   * **Password** - The LDAP server user password for the AOS server to connect as
   * **Encryption** - None, SSL/TLS or STARTTLS
   * **Advanced Config**


     * **Timeout** (seconds)
     * **Username Attribute Name** - The LDAP attribute from the user entry
       that AOS Server uses for authentication. (usually cn or uid)
     * **User Search Attribute Name**
     * **User First Name Attribute Name**
     * **User Last Name Attribute Name**
     * **User Email Attribute Name**
     * **User Object Class Attribute Name**
     * **User Member Attribute Name**
     * **Group Name Attribute Name**
     * **Group DN Attribute Name**
     * **Group Search Attribute Name**
     * **Group Member Attribute Name**
     * **Group Member Mapping Attribute Name**
     * **Group Object Class Attribute Name**

#. You can **Check provider parameters** and **Check login** (to verify
   authentication with the remote user credentials) before creating the provider.
#. Click **Create** to create the provider and return to the list view.

After configuring and activating a provider in the AOS server, you'll need to
map user roles to it to give users assigned those roles access.

Creating Active Directory Provider
==================================
Active Directory (AD) is a database-based system that provides authentication,
directory, policy, and other services in a Windows environment.

#. From the AOS web interface, navigate to **External Systems / Providers**,
   then click **Create Provider**.
#. Enter a **Name** (64 characters or fewer), select **AD**, and if you want
   AD to be the active provider, toggle on **Active?**.
#. For **Connection Settings**, enter/select the following:

   * **Port** - The TCP port used by the server
   * **Hostname FQDN IP(s)** - The fully qualified domain name (FQDN) or IP
     address of the AD server. For high availability (HA) environments, specify
     multiple AD servers using the same settings. If the first server cannot be
     reached, connections to succeeding ones are attempted in order.

#. For **Provider-specific Parameters** enter/select the following,
   as appropriate:

   * **Groups Search DN** - The AD Distinguished Name (DN) path for the
     RBAC Groups Organizational Unit (OU)
   * **Users Search DN** - The AD Distinguished Name (DN) path for the
     RBAC Users Organization Unit (OU)
   * **Bind DN** - The AD Distinguished Name (DN) path for the
     active server user that the AOS server will connect as
   * **Password** - The AD server user password for AOS server to connect as
   * **Encryption** - None, SSL/TLS or STARTTLS
   * **Advanced Config**


     * **Timeout** (seconds)
     * **Username Attribute Name** - The AD attribute from the user entry
       that the AOS server uses for authentication. (usually **cn** or **uid**)
     * **User Search Attribute Name**
     * **User First Name Attribute Name**
     * **User Last Name Attribute Name**
     * **User Email Attribute Name**
     * **User Object Class Attribute Name**
     * **User Member Attribute Name**
     * **Group Name Attribute Name**
     * **Group DN Attribute Name**
     * **Group Search Attribute Name**
     * **Group Member Attribute Name**
     * **Group Member Mapping Attribute Name**
     * **Group Object Class Attribute Name**

#. You can **Check provider parameters** and **Check login** (to verify
   authentication with the remote user credentials) before creating the provider.
#. Click **Create** to create the provider and return to the list view.

After configuring and activating a provider in the AOS server, you'll need to
:ref:`map user roles <map_user_roles>` to it to give users assigned those roles
access.

Creating TACACS+ Provider
=========================
Terminal Access Controller Access-Control Systems (TACACS+)

#. From the AOS web interface, navigate to **External Systems / Providers**,
   then click Create Provider.
#. Enter a **Name** (64 characters or fewer), select **TACACS+**, and if you want
   TACACS+ to be the active provider, toggle on **Active?**.
#. For **Connection Settings**, enter/select the following:

   * **Port** - The TCP port used by the server, usually **49**
   * **Hostname FQDN IP(s)** - The fully qualified domain name (FQDN) or IP
     address of the TACACS+ server. For high availability (HA) environments,
     specify multiple TACACS+ servers using the same settings. If the first server
     cannot be reached, connections to succeeding ones are attempted in order.

#. For **Provider-specific Parameters** enter/select the following,
   as appropriate:

   * **Shared Key** - shared key configured on the server

     .. caution::
         Shared key is not displayed when editing a configured TACACS+ provider.
         If you do not change it, the previously configured shared key is
         retained. If you test the provider and you have not re-entered
         the shared key, a null shared key is used for the test and may
         not work.

   * **Auth Mode** - Authentication mode - ASCII (clear-text), PAP
     (Password Authentication Protocol), or CHAP (Challenge-Handshake
     Authentication Protocol)

#. You can **Check provider parameters** and **Check login** (to verify
   authentication with the remote user credentials) before creating the provider.
#. Click **Create** to create the provider and return to the list view.

Configuring TACACS+ Provider
----------------------------
To authorize AOS users via a TACACS+ provider, the TACACS+ server must be
configured to properly return an **aos-group** attribute. This attribute must be
mapped to a defined AOS Role. The example configuration below is for the
open-source tac_plus TACACS+ server.

.. code-block:: text

   user = jdoe {
       default service = permit
       name = "John Doe"
       member = admin
       login = des LQqpIWvpxDXDw
   }

   group = admin {
       service = exec {
           priv-lvl = 15
       }
       cmd=show {
           permit .*
       }
       service = aos-exec {
           default attribute = permit
           priv-lvl = 15
           aos-group = aos-admins
       }
   }

The **aos-admins** group must be mapped to a defined AOS Role.

After configuring and activating a provider in the AOS server, you'll need to
:ref:`map user roles <map_user_roles>` to it to give users assigned those roles
access.

Creating RADIUS Provider
========================
Remote Authentication Dial-In User Service (RADIUS). See below for limitations.

#. From the AOS web interface, navigate to **External Systems / Providers**,
   then click **Create Provider**.
#. Enter a **Name** (64 characters or fewer), select **RADIUS**, and if you want
   RADIUS to be the active provider, toggle on **Active?**.
#. For **Connection Settings**, enter/select the following:

   * **Port** - The TCP port used by the server, default is **1812** as specified
     in RFC 2865.
   * **Hostname FQDN IP(s)** - The fully qualified domain name (FQDN) or IP
     address of the RADIUS server. For high availability (HA) environments,
     specify multiple RADIUS servers using the same settings. If the first server
     cannot be reached, connections to succeeding ones are attempted in order.

#. For **Provider-specific Parameters** enter/select the following,
   as appropriate:

   * **Shared Key** (64 characters or fewer) - shared key configured on the server

     .. caution::
         Shared key is not displayed when editing a configured RADIUS provider.
         If you do not change it, the previously configured shared key is
         retained. If you test the provider and you have not re-entered
         the shared key, a null shared key is used for the test and may
         not work.

     An example of a pre-shared key configuration that tests successfully with
     AOS is from Ubuntu FreeRADIUS (an open source RADIUS server).
     The Shared Key as given in the RADIUS server configuration must be
     provided in AOS.

     .. code-block:: prompt

           home_server localhost {
           ipaddr = 127.0.0.1
           port = 1812
           type = "auth"
           secret = "testing123"
           response_window = 20
           max_outstanding = 65536

    * Advanced Config

      * **Group Name Attribute Name** - To tell AOS which role a user belongs to,
        the RADIUS server must specify the users’ group. The user group
        information must be specified with **Framed-Filter-ID** as the attribute.
        It is used to assign users to different RADIUS groups.

        For example, the FreeRADIUS config below specifies the
        **Framed-Filter-ID** attribute to be **freerad**. In this case, when
        mapping later, you would enter **freerad** for the Provider Group.

       .. code-block:: prompt

           /etc/freeradius/users
                  freerad Cleartext-Password := "testing123"
                  Framed-Filter-Id = "freerad"

       So that AOS can map the user to an existing group in AOS the RADIUS server
       must return the AOS group name as part of the authentication response.

       .. warning::

          If the group is unmapped, users cannot log in.

    * **Timeout** (seconds) - Defaults to 30 seconds

After configuring and activating a provider in the AOS server, you'll need to
:ref:`map user roles <map_user_roles>` to it to give users assigned those roles
access.

RADIUS Limitations
------------------

* No support for changing the RADIUS user's password on a remote RADIUS server.
* Linux user login via SSH is not controlled by RADIUS Auth.
* Group role-mapping changes are not supported.
* Nested groups are not allowed. All groups must be explicitly assigned to an AOS
  role.
* When a user logs into AOS, only username and password are required for
  authenticating against the remote RADIUS server. Log in credentials are not
  cached. Therefore, when a user logs into AOS, a connection between AOS and the
  remote RADIUS server is required.

Editing Provider
================

.. important::
    Any users who are logged into AOS when a setting is changed in an active
    RBAC provider, are immediately logged out without notification. To
    continue, the user must log back into the AOS server. This does
    not affect users who are defined locally on the AOS server
    (for example, **admin**).

#. Either from the list view (External Systems / Providers) or the details view,
   click the **Edit** button for the provider to edit.
#. Make your changes.
#. Click **Update** (bottom-right) to edit the provider and return to
   the list view.

Deleting Provider
=================

#. Either from the list view (External Systems / Providers) or the details view,
   click the Delete button for the provider to delete.
#. Click **Delete** to delete the provider and return to the list view.

.. _map_user_roles:

Provider Role Mapping
---------------------
After configuring a provider in the AOS server, it needs to be mapped to one or
more AOS user roles. Users with those roles will then be able to access AOS with
the permissions specified. Provider role mappings can be created, edited and
deleted, as needed. Other details to be aware of include the following:

* Only one provider can be active at a time.
* When the same username exists both locally and in the RBAC provider, the local
  user is used to authenticate login attempts.
* Changing users with the web-based RBAC feature does not modify accounts on the
  AOS server VM. To change these credentials, use standard Linux CLI commands:
  `useradd`, `usermod`, `userdel`, `passwd`.

 From the AOS web interface, navigate to
 **External Systems / Providers / Provider Role Mapping**.

.. image:: static/images/external_systems/provider_role_mapping_330.png

Creating Role Map
-----------------

#. From the AOS web interface, navigate to
   **External Systems / Providers / Provider Role Mapping**, then click the
   **Edit** button (top-right).
#. Click **Add mapping**, select an **AOS Role** from the drop-down list, then
   enter a **Provider Group** (that was previously
   :ref:`created <create_provider>`).

   .. tip::
       To see details of AOS roles, navigate to
       **Platform / User Management / Roles**. From there, you can also create new
       roles, as needed.

#. Click **Update** to create the role map. If the provider that you mapped is the
   active provider, then users with the mapped roles can log into AOS with their
   usernames and passwords defined in the RBAC server.

Editing Role Map
----------------

.. important::
    Changing role mappings for an active provider causes all remotely logged in
    users to be logged out (because the session tokens are cleared when changes
    are made). Users will need to log back into the system. This includes the user
    **admin**, if **admin** is not logged in locally.

#. From the AOS web interface, navigate to
   **External Systems / Providers / Provider Role Mapping**, then click the
   **Edit** button (top-right).
#. Edit role mapping as needed.
#. Click Update to update the role map.

Deleting Role Map
-----------------

#. From the list view (External Systems / Providers / Provider Role Mapping)
   click the **Edit** button (top-right), then click the **X** next to the mapping
   to delete.
#. Click **Update** to update the role map.
