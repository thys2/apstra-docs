=================================
Hot/Cold Interface Counters Probe
=================================

.. image:: static/images/iba/if_hotcold.svg

Found at /predefined_probes/fabric_hotcold_ifcounter

It first identifies all the fabric interfaces on all deployed and operational
leafs. Fabric interfaces are defined as those facing spines, which also satisfy
the constraint that they are deployed and operational. It then collects samples
for each, generates time series and calculates average traffic across configurable
time interval. It then raises hot/cold anomaly per interface if it has been
cold/hot (defined as traffic being below/above a specified hot/cold threshold)
longer then configurable period of time. It also creates time series showing
these anomalies being raised and cleared. It then checks if the number of hot/cold
interfaces per device is above specified threshold in which case it generates
device hot/cold anomaly and then creates time series allowing user to inspect
the recent history of these device level anomalies being raised/cleared. In
addition, this probe provides the total traffic per device and per device and
interface role.


"leaf interface traffic" processor
-----------------------------------------

Purpose: wires in interface traffic samples (measured in bytes per second) from
each spine facing interface on each leaf.

Outputs of "leaf interface traffic" processor
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

'leaf_int_traffic': set of traffic samples (for each spine facing interface on
each leaf). Each set member has the following keys to identify it: system_id
(id of the leaf system, usually serial number), interface (name of the interface),
role (role of the interface, such as 'fabric').



"leaf interface tx average" processor
-------------------------------------

Purpose: Calculate average traffic during period specified by average_period
facade parameter. Unit is bytes per second.

Outputs of "leaf interface tx average" processor
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

'leaf_int_tx_avg': set of traffic average values (for each spine
facing interface on each leaf). Each set member has the following keys to identify
it: system_id (id of the leaf system, usually serial number), interface (name of
the interface), role (role of the interface, such as 'fabric').



"leaf int traffic accumulate" processor
-----------------------------------------

Purpose: create recent history time series out of traffic samples from the
leaf_int_traffic output. In terms of the number of samples, the time series
will hold the smaller of: 1024 samples or samples collected during the last
'total_duration' seconds (facade parameter). Samples unit is bytes per second.

Outputs of "leaf int traffic accumulate" processor
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

leaf_int_traffic_accumulate: set of traffic samples time series (for each spine
facing interface on each leaf).  Each set member has the following keys to identify
it: system_id (id of the leaf system, usually serial number), interface (name of
the interface) role (role of the interface, such as 'fabric'). Samples unit is
bytes per second.




"live leaf interface hot" processor
-----------------------------------

Purpose: Evaluate if the average traffic on spine facing interfaces on each
leaf is within acceptable range. In this case acceptable range is between 0 and
max facade parameter (in bytes per second unit).

'live_leaf_int_hot': set of true/false values, each indicating if traffic averages
for each spine facing interface on each leaf is within acceptable range. Each set
member has the following keys to identify it: system_id (id of the leaf system,
usually serial number), interface (name of the interface) role (role of the
interface, such as 'fabric'). Samples unit is bytes per second.



"live leaf interface cold" processor
------------------------------------

Purpose: Evaluate if the average traffic on spine facing interfaces on each
leaf is within acceptable range. In this case acceptable range means larger than
min facade parameter (in bytes per second unit).

'live_leaf_int_cold': set of true/false values, each indicating if traffic averages
for each spine facing interface on each leaf is within acceptable range. Each set
member has the following keys to identify it: system_id (id of the leaf system,
usually serial number), interface (name of the interface) role (role of the
interface, such as 'fabric'). Samples unit is bytes per second.



"sustained hot leaf interface" processor
-----------------------------------------

Purpose: Evaluate if the average traffic spine facing interfaces on each
leaf has been outside acceptable range, (as defined by 'live leaf interface hot'
processor) for more than 'threshold_duration' seconds during the last
'total_duration' seconds. These two parameters are part of facade specification.


Outputs of "sustained hot leaf interface" processor
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

'hot_leaf_int': set of true/false values, each indicating if the traffic average
for each spine facing interface on each leaf has been in 'hot' range for more
than specified period of time. Each set member has the following keys to identify
it: system_id (id of the leaf system, usually serial number), interface (name of
the interface) role (role of the interface, such as 'fabric'). Samples unit is
bytes per second.



"sustained cold leaf interface" processor
-----------------------------------------

Purpose: Evaluate if the average traffic spine facing interfaces on each
leaf has been outside acceptable range, (as defined by 'live leaf interface cold'
processor) for more than 'threshold_duration' seconds during the last
'total_duration' seconds. These two parameters are part of facade specification.


Outputs of "sustained cold leaf interface" processor
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

'cold_leaf_int': set of true/false values, each indicating if the traffic average
for each spine facing interface on each leaf has been in 'cold' range for more
than specified period of time. Each set member has the following keys to identify
it: system_id (id of the leaf system, usually serial number), interface (name of
the interface) role (role of the interface, such as 'fabric'). Samples unit is
bytes per second.



"anomaly hot leaf int traffic" processor
----------------------------------------

Purpose: Export hot leaf interface status when true as an anomaly.


Outputs of "anomaly hot leaf int traffic" processor
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

'anomaly_hot_int_traffic': set of true/false values, each indicating if the
traffic average for each spine facing interface on each leaf has been in
'hot' range for more than specified period of time. Each set member has the
following keys to identify it: system_id (id of the leaf system, usually
serial number), interface (name of the interface) role (role of the interface,
such as 'fabric'). Samples unit is bytes per second.



"anomaly cold leaf int traffic" processor
-----------------------------------------

Purpose: Export cold leaf interface status when true as an anomaly.


Outputs of "anomaly cold leaf int traffic" processor
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

'anomaly_cold_int_traffic': set of true/false values, each indicating if the
traffic average for each spine facing interface on each leaf has been in
'hot' range for more than specified period of time. Each set member has the
following keys to identify it: system_id (id of the leaf system, usually
serial number), interface (name of the interface), role (role of the interface,
such as 'fabric'). Samples unit is bytes per second.



"leaf int hot anomaly history" processor
----------------------------------------

Purpose: Create time series showing hot anomaly being raised and cleared for
each interface under consideration. This time series may contain up to
'anomaly_history_count' anomaly state changes.

Outputs of "leaf int hot anomaly history" processor
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

'int_hot_accumulate_anomaly': Time series showing hot anomaly being raised
and cleared for each interface under consideration. This time series may contain
up to 'anomaly_history_count' anomaly state changes.



"leaf int cold anomaly history" processor
-----------------------------------------

Purpose: Create time series showing cold anomaly being raised and cleared for
each interface under consideration. This time series may contain up to
'anomaly_history_count' anomaly state changes.

Outputs of "leaf int cold anomaly history" processor
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

'int_cold_accumulate_anomaly': Time series showing cold anomaly being raised
and cleared for each interface under consideration. This time series may contain
up to 'anomaly_history_count' anomaly state changes.


"interface sum per device" processor
-----------------------------------------

Purpose: Sum average traffic for all interface under consideration per device.


Outputs of "interface sum per device" processor
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

'if_counter_sum_per_device': Set of numbers, each indicating the total average
traffic for all interface under consideration per device, expressed in bytes
per second. Each set member has the following key to identify it: system_id
(id of the leaf system, usually serial number).


"interface sum per device per link role" processor
--------------------------------------------------

Purpose: Sum average traffic for all interface under consideration per device,
per interface role.


Outputs of "interface sum per device per link role" processor
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

'if_counter_sum_per_device_per_role': Set of numbers, each indicating the total
average traffic for all interface under consideration per device, expressed in
bytes per second. Each set member has the following keys to identify it:
system_id (id of the leaf system, usually serial number), role (role of the
interface, such as 'fabric').


"system percent hot" processor
--------------------------------------------------

Purpose: Calculate percentage of interfaces that are hot on any given device
under consideration.


Outputs of "system percent hot" processor
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

'system_perc_hot': Set of numbers, each indicating the the percentage of
hot interfaces on any given device under consideration. Each set member has the
following key to identify it: system_id (id of the leaf system, usually serial
number).


"system percent cold" processor
--------------------------------------------------

Purpose: Calculate percentage of interfaces that are cold on any given device
under consideration.


Outputs of "system percent cold" processor
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

'system_perc_cold': Set of numbers, each indicating the the percentage of
cold interfaces on any given device under consideration. Each set member has the
following key to identify it: system_id (id of the leaf system, usually serial
number).


"device hot" processor
--------------------------------------------------

Purpose: Evaluate if the percentage of hot interfaces on a specific device is
outside the acceptable range, where acceptable range in his case means less than
'max_hot_interface_percentage', which is a facade parameter.



Outputs of "device hot" processor
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

'device_hot_anomalous': Set of boolean values, each indicating if the the
percentage of hot interfaces on any given device was out of acceptable range.
Each set member has the following key to identify it: system_id (id of the
leaf system, usually serial number).

"device cold" processor
--------------------------------------------------

Purpose: Evaluate if the percentage of cold interfaces on a specific device is
outside the acceptable range, where acceptable range in his case means less than
'max_cold_interface_percentage', which is a facade parameter.


Outputs of "device cold" processor
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

'device_cold_anomalous': Set of boolean values, each indicating if the the
percentage of cold interfaces on any given device was out of acceptable range.
Each set member has the following key to identify it: system_id (id of the
leaf system, usually serial number).


"anomaly device hot" processor
--------------------------------------------------

Purpose: Export 'device hot' state as anomaly.

Outputs of "anomaly device hot" processor
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

'device_hot_anomaly': Set of boolean values, each indicating if the the
percentage of hot interfaces on any given device was out of acceptable range.
Each set member has the following key to identify it: system_id (id of the
leaf system, usually serial number).

"anomaly device cold" processor
--------------------------------------------------

Purpose: Export 'device cold' state as anomaly.

Outputs of "anomaly device cold" processor
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

'device_cold_anomaly': Set of boolean values, each indicating if the the
percentage of cold interfaces on any given device was out of acceptable range.
Each set member has the following key to identify it: system_id (id of the
leaf system, usually serial number).

"anomaly device hot history" processor
-----------------------------------------

Purpose: Create time series showing device hot anomaly being raised and cleared for
each device consideration. This time series may contain up to
'anomaly_history_count' anomaly state changes.

Outputs of "anomaly device hot history" processor
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

device_hot_anomaly_timeseries: Time series showing device hot anomaly being raised
and cleared for each device under consideration. This time series may contain
up to 'anomaly_history_count' anomaly state changes. Each set member has the
following key to identify it: system_id (id of the leaf system, usually
serial number).


"anomaly device cold history" processor
-----------------------------------------

Purpose: Create time series showing device cold anomaly being raised and cleared for
each device consideration. This time series may contain up to
'anomaly_history_count' anomaly state changes.

Outputs of "anomaly device cold history" processor
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

device_cold_anomaly_timeseries: Time series showing device cold anomaly being raised
and cleared for each device under consideration. This time series may contain
up to 'anomaly_history_count' anomaly state changes. Each set member has the
following key to identify it: system_id (id of the leaf system, usually
serial number).
