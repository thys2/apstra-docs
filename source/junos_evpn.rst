########################
Juniper EVPN Support
########################

Overview
==========
.. versionadded:: 3.3.0
    AOS now supports Juniper Junos devices in AOS Blueprints
    using the MP-EBGP EVPN Overlay Control Protocol. AOS supports the
    implementation of **EVPN ESI multi-homing** for AOS racks with leaf-pair
    redundancy.

The Junos EVPN ESI multi-homing feature enables users to directly connect end
servers to Leafs and provide redundant connectivity via multi-homing. AOS supports
this feature only on LAGs that span 2 leafs on the fabric. EVPN ESI also removes the
need for "peer-link" and hence facilitates clean leaf-spine design.

AOS EVPN ESI multi-homing helps to maintain EVPN service and traffic forwarding to
and from the multi-homed site in the event of the following types of network
failures and avoid single point of failure as per below scenarios:

* Link failure from one of the Leaf devices to end server device
* One of the Leaf device failure
* Fast convergence on the local VTEP by changing next-hop adjacencies and
  maintaining end host reachability across multiple remote VTEPs

Understanding EVPN multi-homing Terminology and Concepts
========================================================

Please find below EVPN multi-homing terminology and concepts:

**EVI** - This is an EVPN instance that spans between the leaf devices making
up the EVPN. The EVPN instance (EVI) is represented by the
Virtual Network Identifier(VNI). AOS maps EVI to Virtual Networks(VN) of type
VXLAN.

**MAC-VRF** - A virtual routing and forwarding (VRF) table to house MAC
addresses on the VTEP leaf device (often just called a "MAC table"). A unique
route distinguisher and VRF target is configured per MAC-VRF in AOS.

**Ethernet Segment(ES)** - Ethernet links span from an end host to multiple
ToR leafs and form Ethernet Segment. It constitutes a set of bundled links.

**Ethernet Segment Identifier(ESI)** - Ethernet Segment Identifier to represent
each ES uniquely across the network. ESI is supported only on LAGs that span
two leafs on the AOS fabric.

AOS only **supports active-active** redundancy mode for Juniper
EVPN multi-homing where each Juniper ToR leaf attached to an ES is allowed to
forward traffic to and from for a given VLAN.

Topology Specification
----------------------

Here Leaf1 and Leaf2 are part of the same Ethernet Segment(ES), Leaf3 is the
switch sending traffic towards the Ethernet Segment(ES).

.. image:: static/images/junos_evpn/evpn_multihoming.png

There are 5 different route types currently used for Juniper EVPN multi-homing:

* Ethernet Auto-Discovery (EAD) Route(Type-1)
* MAC advertisement Route(Type-2)
* Inclusive Multicast Route(Type-3)
* Ethernet Segment Route(Type-4)
* IP Prefix Route(Type-5)

In AOS BGP EVPN running on Juniper devices uses route type-2 to advertise MAC
and IP (host) information, route type-3 to carry VTEP information and the
EVPN route type-5 allows advertisements of IP prefixes in an Network Layer
Reachability Information (NLRI).

.. note::
  In Junos MAC/IP route-type 2 doesn't contain VNI and RT for the IP part
  of the route, it is derived from the accompanying route-type 5.

Type-1 routes are used for per-ES auto-discovery (A-D) to advertise EVPN
multi-homing mode. Remote ToR leaf devices in the EVPN network use the EVPN
Route type 1 functionality to learn the EVPN Type 2 MAC routes from other
leaf devices. In this route type Ethernet Segment Identifier and the Ethernet
Tag ID are considered to be part of the prefix in the NLRI.
Upon a link failure between ToR leaf and end server VTEP withdraws Ethernet
Auto-Discovery routes(i.e Type-1) per Ethernet Segment. In AOS implementation
for Juniper EVPN multi-homing Ethernet Tag value is set to VLAN ID
for ES auto-discovery/ES route types.

**Mass Withdrawal** - It is used for fast convergence during the
link failure scenarios between the Leaf devices to end server using the
type-1 EAD/ES routes.

**DF Election** - It is used to prevent forwarding of the loops and the
duplicates as only a single switch is allowed to decapsulate and forward the
traffic for a given Ethernet Segment. Ethernet Segment Route is exported and
imported when ESI is locally configured under the LAG. Type-4 NLRI is mainly
used for designated forwarder(DF) elections and to apply Split Horizon
Filtering.

**Split Horizon** - It is used to prevent forwarding of the loops and the
duplicates for the Broadcast, Unknown-unicast and Multicast (BUM) traffic.
Only the BUM traffic that originates from a
remote site is allowed to be forwarded to a local site.


EVPN Services
==============

EVPN VLAN-Aware
---------------

At a high level, Ethernet Services can be (1) VLAN-based, (2) VLAN Bundle or
(3) VLAN-Aware. Only VLAN-Aware is supported on Junos. With the EVPN VLAN-Aware
Service each VLAN is mapped directly to its own EVPN instance (EVI). The mapping
between VLAN, Bridge Domain (BD) and EVPN instance (EVI) is N:1:1. For example, N
VLAN's are mapped into a single BD mapped into a single EVI. In this model all
VLAN IDs share the same EVI as shown below:

.. image:: static/images/junos_evpn/evpn_vlan_aware.png

VLAN-aware Ethernet Services in Junos have a separate Route target for each VLAN
(which is Juniper internal optimization), so each VLAN has a label to mimic AOS
VLAN-based implementations.

From the control plane perspective EVPN MAC/IP routes (type 2) for VLAN-aware
services carry VLAN ID in the Ethernet Tag ID attribute that is used to
disambiguate MAC routes received.

From the data plane perspective - every VLAN is tagged with its own Virtual
Network Identifier (VNI) that is used during packet lookup to place it onto the
right Bridge Domain(BD)/VLAN.

AOS Workflow
------------

#. **Manage Devices**

   Create/Install AOS off-box :doc:`device agents <device_agents>` for all
   switches. Only off-box agents are supported on Junos switches.

#. **Design Network**

   - Logical Devices are abstractions of physical devices that specify port roles
     and speeds of devices used in rack types/templates of blueprint topologies.
     Check existing :doc:`logical devices <logical_devices>` for ones that meet
     the Juniper device requirements and create them if necessary.

   - Interface maps create links between physical devices (device profiles) and
     logical devices. Check existing :doc:`interface maps <interface_maps>` for
     ones that meet the Juniper device requirements and create them if necessary.

   - For single leaf racks, create a :doc:`rack type <rack_types>` with
     redundancy protocol of **None**.

   - For dual-leaf racks, create a rack type with redundancy protocol of **ESI**
     for Ethernet Segment Identifier (new in AOS version 3.3.0). When specifying
     the end server in the rack type, specify the **Attachment Type** as
     **Dual-Homed** towards ESI-based ToR leafs. ESI provides the option of link
     aggregation in EVPNs using Ethernet Segments.

     ESI helps with end host level redundancy in an EVPN VXLAN-based AOS blueprint.
     Ethernet links from each Juniper ToR leaf connected to the server are bundled
     as an aggregated Ethernet interface. LACP is enabled for each aggregated
     Ethernet interface of the Juniper devices. Multi-homed interfaces into the
     Ethernet segment are identified using the Ethernet Segment Identifier (ESI).

     ESI has certain restrictions and requirements as listed below:

     * ESI based ToR leafs cannot have any L2/L3 peer links as EVPN multi-homing
       eliminates peer links used by MLAG/vPC.
     * A bond of two physical interfaces towards a single leaf is not supported
       in the AOS ESI implementation (AOS version 3.3.0), so make sure the server
       having LAG in that rack type spans two leafs.
     * ESI and MLAG/vPC-based rack types cannot be mixed in a single AOS
       blueprint.
     * L2 External Connectivity Points (ECPs) with an ESI-based rack type is not
       supported. Only L3 ECPs are supported.
     * Per-leaf VN assignment i.e having different VLAN sets among
       individual leafs for an ESI-based port channel is not supported in AOS.
     * Connecting a single server to a single leaf using a bond of two physical
       interfaces cannot use an ESI in AOS.
     * ESI is supported only on LAGs (i.e port-channels) and not directly on physical
       interfaces. This has no functional impact, as AOS automatically generates leaf
       local port-channels for multi-home links.
     * Only ESI **active-active redundancy** mode is supported. Active-standby mode
       is not supported.
     * More than two leafs in one ESI segment using ESI based rack types is not
       supported.
     * Switching from an ESI to MLAG rack type or vice versa is currently not
       supported under AOS Flexible Fabric Expansion (FFE) operations.

   - Use the Rack/POD :doc:`template <templates>` for the topology layout of
     Juniper devices based blueprint. EVPN multi-homing blueprints should
     use an ESI-based rack template.

   - Plan the network: Create :doc:`external router <external_routers>` and pools
     for :doc:`ASNs <asn_pools>`, :doc:`IP addresses <ip_pools>`, and
     :doc:`VNIs <vni_pools>`.

#.  **Build the Network**

    Create a :doc:`blueprint <bp_create>` based on the template, then
    :doc:`build <build_physical>` the EVPN-based network topology for the
    Juniper devices by assigning resources, device profiles, device IDs, and
    external routers.

Configuration Rendering
=======================

Reference Design
----------------

#. **Underlay** - The underlay in AOS DC fabric is layer-3 configured
   using standard eBGP over the physical interfaces of Juniper devices.

#. **Overlay** - Overlay will be configured eBGP over ``lo0.0`` address. EVPN
   VXLAN is used as an overlay protocol. All the ToR devices will be enabled
   with L2 VN. Each one of these L2 VNs can have its default gateway
   to be hosted on connected ToR Leafs. For the inter-VN traffic VXLAN routing
   will be done in AOS fabric using L3 VNIs on the Border Leafs as per
   standard design.

#. **VXLAN VTEPs** - On Juniper leafs AOS renders one ip address on ``lo0.0``
   which is used as VTEP address. The VTEP IP address is used to establish
   the VXLAN tunnel.

#. **EVPN multi-homing LAG** - In AOS **Unique ESI value** and
   **LACP system IDs** are used per EVPN LAG. The multi-homed links are
   configured with an ESI and a LACP system identifier is specified for
   each link. The ESI is used to identify LAG groups and loop prevention.
   In AOS to support Active/Active and multi-homing for Juniper Leafs, they
   are configured with the same LACP parameter for a given ESI so that they
   appear as a single system.

   .. image:: static/images/junos_evpn/esi.png

   ESI MAC addresses are auto-generated internally. You can
   :ref:`configure the value of the most significant byte <esi_mac>` used in the
   generated MAC. A new facade API is added to update the MSB value. A new node
   will be added to the rack based template that will contain the MAC MSB value.
   The default value of this byte is 2 and you can change it to any even number
   up to 254. Updating this value results in regeneration of all ESI MACs in the
   blueprint. This is exposed to address DCI use cases where ESIs should be unique
   across multiple blueprints (IP Fabrics).

#. **L3VNIs** - AOS renders L3VNI as a Security zone per VRF. AOS offers
   multi-tenancy functionality ensuring that workloads remain logically separated
   within a virtual network (overlay) construct using Security zone.

#. **Route Target(RT) for L2/L3 VNIs** - AOS auto-generates RT for L2/L3 VNIs
   in the format VNI:1. There is 1 RT per MAC-VRF(i.e L3VNI), this is fabric
   wide RT, value must be the same across all switches participating in one EVI.
   Please find the example as below:

   .. image:: static/images/junos_evpn/RT.png

#. **Route Distinguisher(RD) for L2/L3 VNIs** - In AOS for Junos VLAN-Aware
   based model, the RD is per EVI (switch). There is no RD for each l2 VNI.
   Route Distinguisher(RD) exist only for Security Zone VRF in the format
   ``{primary_loopback}:vlan_id``.

#. **Virtual Switch Configuration** - Under the *switch-options* hierarchy for
   Juniper devices AOS renders *vtep-source-interface* parameter and specifies
   the VTEP IP address used to establish the VXLAN tunnel. Reachability to
   loopback interface (e.g. lo0.0) is provided by the underlay. The
   route-distinguisher(RD) here defines the EVI specific RD carried by
   Type 1,Type 2, Type 3 routes. RD for the global switch options is provided
   in the format ``{loopback_id}:65534``.

   The route-target(RT) here defines the global route target inherited by
   EVPN routes. It is used by Type 1 routes. AOS will render a default RT
   value for it i.e ``100:100`` for global switch options across all switches.

#. **MTU** - These are the MTU values AOS  will render for Juniper Devices:

   * L2 ports: 9100
   * L3 ports: 9216
   * Integrated Routing and Bridging(IRB) Interfaces: 9000

#. **Anycast Gateway** - In AOS the same IP on
   Integrated Routing and Bridging(IRB) interfaces of all the leafs is
   configured and no virtual gateway is set. Every IRB interface that
   participates in the stretched L2 service will have the same IP/MAC
   configured as below:

   .. image:: static/images/junos_evpn/IRB.png

   In this model, all default gateway IRB interfaces in an overlay subnet are
   configured with the same IP and MAC address. A benefit of this model is
   that only a single IP address is required per subnet for default gateway
   IRB interface addressing, which simplifies gateway configuration on end
   systems.

   Here MAC address of the IRB is auto generated by AOS.

Limitations
============

The following limitations apply to AOS EVPN multi-homing topologies for Juniper
devices in AOS 3.3.0:

*  Only two-way multi-homing is currently supported is AOS. This means more than
   two Juniper leafs in multi-homed group is not yes supported in AOS.

*  AOS currently does not support Juniper EVPN with EVPN on other
   network vendors in the same AOS Blueprint.

*  No Static VXLAN support.

*  AOS does not yet support IPv6 based fabrics with Junos.

*  AOS does not yet support Segmentation and Group Based Policies (GBP).

*  In Juniper EVPN multi-homing, AOS only supports L3 External Connectivity
   Points (ECP) towards external routers and there is no support yet for
   L2 ECP.

*  AOS does not yet support BGP routing from Junos leafs to AOS managed Layer 3
   servers.
