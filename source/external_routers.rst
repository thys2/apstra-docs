.. _page-external-routers:

================
External Routers
================

.. _external_router_concept:

|external_router_icon| External Router Overview
===============================================

.. |external_router_icon| image:: static/images/icons/external_router_icon.png

External routers represent AOS-managed switches that the network uses for traffic
exiting and entering the data center fabric (via BGP). AOS ensures that
proper routing is in place by peering leafs or spines with external routers
(via ASNs and loopback IPs).

Characteristics and Requirements
--------------------------------
* External router points are defined per security zone (as opposed to the entire
  blueprint) (as of AOS version 3.0.0).
* Leaf and external router point connectivity can be via an L2 or L3 interface.
* L2 interfaces can be in a bond such as MLAG or vPC.
* Specify custom import prefix-lists per security zone or per
  external connectivity point (ECP) (for different external routers).

BGP Specific Requirements
-------------------------
* IPv6 peering to external routers is supported (as of AOS version 3.0.0).
* AOS does not support Bidirectional Forwarding Detection (BFD) for
  external routers (since not all vendors support BFD).
* AOS BGP timers are 1 second for keepalive interval, 3 seconds for hold time,
  and 5 seconds for connect time.
* The external router role must be assigned to a device (specified in a
  logical device) to enable it to be deployed as an external router.
* Choose between BGP loopback (eBGP multi-hop) or BGP interface peering.

OSPF Specific Requirements
--------------------------
* IPv6 peering to external routers is not supported for OSPF (as of AOS version
  3.2.0).
* AOS supports Bidirectional Forwarding Detection (BFD) for external routers
  with OSPF. It is disabled by default.
* The AOS default values for NX-OS, EOS, and Cumulus Linux OSPF Hello/Dead timers
  can be customized.
* The external router role must be assigned to a device (specified in a logical
  device) to enable it to be deployed as an external router.

Policies
--------
External router connections have one or more external connectivity points (ECPs).
ECPs can be fine-tuned with complex custom routing policies to control traffic
entering and exiting a pod or security zone. ECPs are defined per L3 Group,
rather than the entire blueprint.

You can disable the export of spine-leaf links,
L3 edge server links, L2 edge subnets and loopbacks,
based on the security zone or per ECP (for different external routers).

Routing Policy
   Default only (0.0.0.0/0) - at least one default routing policy is expected
   for each external router in a blueprint (for telemetry)

   All (accept all incoming routes)

Overlay Control Protocol (specified in the template)
   Static VXLAN
      External routers can peer with spine or leaf devices configured with
      external connections.

   MP-EBGP EVPN
      External routers must peer with leaf devices configured with
      external connections.

External Router Config - BGP Example
------------------------------------
Below is an example of BGP configuration for an external router that generates a
default route facing AOS. It has the following characteristics:

* The ASN of the external router is 100.
* The loopback IP of the external router is 9.0.0.1.
* The external router is connected to two spine switches.
* The ASN of Spine1 is 65416.
* The loopback IP of Spine1 is 172.20.0.4.
* The ASN of Spine2 is 65417.
* The loopback IP of Spine1 is 172.20.0.6.
* L3 fabric link facing spine1 is 10.0.0.1/31.
* L3 fabric link facing spine2 is 10.0.0.3/31.

The prefix-list *PREPEND-FABRIC-PREFIX* prepends the external router's ASN
multiple times, which eliminates the fabric preferring the external router
for internal fabric prefixes. This is just one resolution and is not the
required method.

.. literalinclude:: static/api_samples/external_routers/bgp_config.txt
   :caption: Linux Quagga External Router Example

External Router Config - OSPF Example
-------------------------------------
Below is an example of OSPF configuration for an external router that generates a
default route facing AOS. It has the following characteristics:

* The OSPF area of the external router is 51.
* The loopback IP of the external router is 9.0.0.1.
* The external router is connected to two border leaf switches.
* AOS border leaf is an ASBR.
* AOS border leaf is NOT part of a backbone area.
* External router is an ABR attached to the backbone area.
* AOS border leaf to be configured with MTU ignore.
* OSPF network type is broadcast.
* OSPF Hello/Dead interval timers are default.
* L3 fabric link IP on border leaf router is is 10.61.60.1/24.
* L3 fabric link IP on external router is 10.61.60.254/24.

.. literalinclude:: static/api_samples/external_routers/ospf_config.txt
 :caption: Linux Quagga External Router Example

External Router Details
-----------------------
External routers include the following details:

.. _external_router_parameters:

   Name
      64 characters or fewer

   IPv4 Address
      IPv4 Address of the external router. For BGP loopback peering,
      AOS uses this for eBGP multi-hop peering.

   IPv6 Address (available for AOS version 3.0 and later)
      Optional IPv6 Address of the external router. For BGP loopback peering,
      AOS uses this for eBGP multi-hop peering.

   ASN
      AOS automatically generates eBGP peer configuration facing this router
      Autonomous System Number (ASN).

.. image:: static/images/external_routers/external_routers_330.png

.. sidebar:: Cloning External Router

   Instead of entering all details for a new external router, you can clone an
   existing one, give it a new name and customize it.

Creating External Router
========================
#. From the blueprint, navigate to **External Systems / External Routers**, then
   click **Create External Router**.
#. Enter a name, IPv4 address, IPv6 Address (optional), and ASN.
#. Click **Create** (bottom-right) to create the external router and add it to
   the global catalog. It is now available for
   :ref:`importing and assigning in a blueprint <staging_external_routers>`.

Editing External Router
=======================
Changes to external routers in the global catalog do not affect external routers
that have already been used in blueprints, thereby preventing potentially
unintended changes to those blueprints. If your intent is for the blueprint to
use an updated external router, then you must delete it from the blueprint,
re-import it after it has been updated, and re-assign the link to it. See below
for the steps for updating an external router.

#. Either from the list view (External Systems / External Routers) or the details
   view, click the **Edit** button for external router to edit.
#. Make your changes.
#. Click **Update** (bottom-right) to update the external router in the global
   catalog and return to the list view.

Deleting External Router
========================
#. Either from the list view (External Systems / External Routers) or the details
   view, click the **Delete** button for the external router to delete.
#. Click **Delete External Router** to delete the external router from the global
   catalog.
