=========
VNI Pools
=========

VNI Pool Overview
=================
You can create resource pools during the design phase or just before you need them
during the build phase. When you assign resources to managed devices in your
network (blueprint), AOS pulls them automatically from the pool you specify. In
cases where you need to assign a specific network identifier you have the option
of assigning a resource individually. To prevent shortages as a network grows,
network design best practices recommend defining more resources than are required
for the initial design.

Virtual network identifiers (VNIs) are VXLAN network identifiers used
in a VXLAN-Aware datacenter fabrics. For more information about VNI usage, see
:ref:`virtual_networks:Create Inter-rack VXLAN/EVPN based Virtual Networks`.
VNI pools include the following details:

Pool Name
  To identify the resource pool.

Total Usage
  Percentage of VNIs in use for all ranges in the resource pool. (Hover over
  status bar to see number of VNIs in use and total number of VNIs
  in the pool.) (New in AOS version 3.3.0)

Range Usage
  The VNIs included in the range and the percentage that are in use. (Hover over
  status bar to see number of VNIs in use and total number of VNIs
  in that range.) (New in AOS version 3.3.0)

Status
  Indicates if the pool is in use or not.

Tags (optional)
  To enable filtering by user-specified categories.

From the AOS web interface, navigate to **Resources / VNI Pools**.

   .. image:: static/images/resources/vni_pool_330.png

.. sidebar:: Cloning VNI Pool

   Instead of entering all details for a new VNI pool, you can clone an existing
   one, give it a new name and customize it.

Creating VNI Pool
=================
#. From the AOS web interface, navigate to **Resources / VNI Pools**, then click
   **Create VNI Pool**.
#. Enter a name, (optional) tags, and a valid range (4096 through 16777214).

   * To specify an additional range, click **Add a range**.

#. Click **Create** to create the pool and return to the list view.

When the blueprint is ready, you can assign resources by
:doc:`updating assignments <build_physical>` in the **Staged / Physical** view.

Editing VNI Pool
================
If any VNIs in a VNI pool are in use, they cannot be removed from the pool.

#. Either from the list view (Resources / VNI Pools) or the details view,
   click the **Edit** button corresponding to the pool to edit.
#. Make your changes.

   * To add a range, click **Add a range**.
   * To change a range, change the existing range numbers.
   * To remove a range, click the **X** to the right of the range to delete.

#. Click **Update** to update the pool and return to the list view.

Deleting VNI Pool
=================
If any VNIs in a VNI pool are in use, the pool cannot be deleted.

#. Either from the list view (Resources / VNI Pools) or the details view,
   click the **Delete** button for the pool to delete.
#. Click **Delete** to delete the pool and return to the list view.
