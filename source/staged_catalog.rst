=======
Catalog
=======

Logical Devices
===============

.. image:: static/images/blueprints/staged/catalog/staged_catalog_logical_device_330.png

From the :ref:`blueprint <access_blueprint>`, navigate to
**Staged / Catalog / Logical Devices**.

Logical devices in a blueprint catalog come from the template that was used to
create the blueprint.

Exporting Logical Device
------------------------
#. From the blueprint, navigate to **Staged / Catalog / Logical Devices**, then
   click the **Export to global catalog** button for the logical device to export.
#. Click **Export** to export the logical device and return to the list view.

Interface Maps
==============

.. image:: static/images/blueprints/staged/catalog/staged_catalog_interface_map_330.png

Importing Interface Map
-----------------------
#. Make sure that the :doc:`interface map <interface_maps>` to import has been
   created in the global catalog.
#. From the blueprint, navigate to **Staged / Catalog / Interface Maps**, then
   click **Import Interface Map**.
#. Select a logical device and an interface map from the drop-down lists to see
   a preview of your selection.
#. Click **Import Selected Interface Map** to import the interface map and return
   to the list view.

Deleting Interface Map from Blueprint Catalog
---------------------------------------------
#. From the blueprint, navigate to **Staged / Catalog / Interface Maps**, then
   click the **Delete** button for the interface map to delete.
#. Click **Delete** to delete the interface map and return to the list view.

External Routers
================

.. image:: static/images/blueprints/staged/catalog/staged_catalog_external_router_330.png

.. _import_external_router:

Importing External Router
-------------------------
#. Make sure that the :doc:`external router <external_routers>` to import has been
   created in the global catalog.
#. From the blueprint, navigate to **Staged / Catalog / External Routers**, then
   click **Import External Router**.
#. From the drop-down list, select an external router from the global catalog,
   then click **Import External Router** to import the router and return to the
   list view.

Editing External Router in Blueprint Catalog
--------------------------------------------
Editing an external router in the blueprint does not affect the same-named one in
the global catalog, just as editing the one in the global catalog does not affect
the same-named one in the blueprint catalog.

#. From the blueprint, navigate to **Staged / Catalog / External Routers**, then
   click the **Edit** button for the external router to edit.
#. Make your changes, then click **Update** to change the router and return to the
   list view.

Deleting External Router from Blueprint Catalog
-----------------------------------------------
Deleting an external router in the blueprint catalog does not affect the same-named
one in the global catalog, just as deleting the one in the global catalog does not
affect the same-named one in the blueprint catalog.

#. From the blueprint, navigate to **Staged / Catalog / External Routers**, then
   click the **Delete** button for the external router to delete.
#. Click **Delete** to delete the external router and return to the list view.

Property Sets
=============

.. image:: static/images/blueprints/staged/catalog/staged_catalog_property_set_330.png

Importing Property Set
----------------------
#. Make sure that the :doc:`property set <property_sets>` to import has been
   created in the global catalog.
#. From the blueprint, navigate to **Staged / Catalog / Property Sets**, then
   click **Import Property Set**.
#. From the drop-down list, select a property set from the global catalog, then
   click **Import Property Set** to import it and return to the list view.

Re-importing Property Set
-------------------------
If a property set that is used in a blueprint is updated in the global catalog,
a message appears in the blueprint catalog stating that the property set in the
blueprint catalog is **Different from global catalog**. You can re-import the
property set from the global catalog by clicking the **Re-import** button, then
clicking **Re-import Property Set** to stage the update.

Deleting Property Set from Blueprint Catalog
--------------------------------------------
As long as a property set is not used in a configlet, it can be unassigned
from a device at any time. If it is used in a configlet, a build error
occurs and you won't be able to commit the change until you remove the property
set from the configlet, which would resolve that build error.

#. From the blueprint, navigate to **Staged / Catalog / Property Sets**, then
   click the **Delete** button for the property set to delete.
#. Click **Delete** to delete the property set and return to the list view.

Configlets
==========

.. _import_configlet:

.. image:: static/images/blueprints/staged/catalog/staged_catalog_configlet_330.png

Importing Configlet
-------------------
#. Make sure that the :doc:`configlet <configlets>` to import has been
   created in the global catalog.
#. From the blueprint, navigate to **Staged / Catalog / Configlets**, then
   click **Import Configlet**.
#. From the drop-down list, select a configlet from the global catalog.
#. For OSPF configlets only - select security zone VRF names. If no VRFs are
   checked, the OSPF configlet will apply to all VRFs.
#. Enter/select details to specify the configlet scope: roles and/or
   individual nodes. (The individual nodes selection depends on the role
   and predicate settings.) As of AOS version 3.2.1 you can filter for specific
   individual nodes using regex (such as l2_virtual_ext_00[1-2]).
#. Importing an interface configlet includes additional options for defining the
   interface scope.
#. Click **Import Configlet** to stage the configlet and return to the list view.

Editing Configlet Scope in Blueprint Catalog
--------------------------------------------
To change the roles and/or nodes that a configlet applies to directly in the
blueprint catalog, follow the steps below:

#. From the blueprint, navigate to **Staged / Catalog / Configlets**, then
   click the **Edit** button for the configlet to edit.
#. Make your changes to the configlet scope.
#. Click **Update** to stage the update and return to the list view.

.. note::
     To change configlet generators (template text, negation template text,
     filename, as applicable) you must change them in the global catalog,
     then re-import the configlet into the blueprint catalog. See
     :ref:`Editing Configlet Generators in Blueprint Catalog
     <edit_configlet_in_blueprint_catalog>`
     for details.

.. _edit_configlet_in_blueprint_catalog:

Editing Configlet Generators in Blueprint Catalog
-------------------------------------------------
Configlet generators (template text, negation template text, filename, as
applicable) cannot be changed directly in blueprints. If an existing configlet is
no longer relevant, you can delete it and import a new or revised one. If you are
changing a configlet in a blueprint catalog because of a configuration deviation,
see also
:ref:`Configuration Deviation and Configlets <config_deviation_and_configlets>`.

#. :ref:`Edit <edit_configlet>` or :ref:`create <create_configlet>`
   a configlet in the global catalog.
#. :ref:`Delete the configlet <delete_configlet_from_blueprint>` from the
   blueprint catalog.
#. :ref:`Import the configlet <import_configlet>` from the global catalog
   into the blueprint catalog.
#. :ref:`Commit <commit>` the changes.

.. _delete_configlet_from_blueprint:

Deleting Configlet from Blueprint Catalog
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
#. From the blueprint, navigate to **Staged / Catalog / Configlets** and
   click the **Delete** button for the configlet to delete.
#. Click **Delete** to stage the deletion and return to the list view. AOS will
   remove the configlet from all devices that are within its scope.

.. _creating_aaa_server:

AAA Servers
===========

.. image:: static/images/blueprints/staged/catalog/staged_catalog_aaa_330.png

AAA servers are used when configuring
:doc:`interface policies <interface_policies>`.

**To access AAA servers** - from the blueprint, navigate to
**Staged / Catalog / AAA servers**. AAA servers include the following details:

Label
   To identify the AAA server

Server Type
   RADIUS 802.1x
      If an 802.1x policy is bound to at least one interface on a
      switch, all defined AAA RADIUS 802.1x servers will be added to that switch.
      The server is not rendered unless it is needed.

   RADIUS COA (Change of Authorization)
      Used by switches to enable Dynamic
      Authorization Server (DAS) requests from RADIUS servers. This enables the
      switch to 'trust' the given RADIUS server to do dynamic VLAN assignment after
      authentication instead of during auth. All RADIUS COA implementations are
      hard-coded to auth port 3799.

Hostname

Auth Ports

Accounting Port
   Optional

AAA RADIUS Server configuration Tasks
-------------------------------------
AAA RADIUS server configuration tasks are external to AOS. For example,
the following files are to be configured in the case of *FreeRADIUS*.

*/etc/freeradius/clients.conf* -- has credentials for each switch

.. code-block:: text

  client Arista-7280SR-48C6-1 {
      shortname = Arista-7280SR-48C6-1
      ipaddr    = 172.20.191.10
      secret    = testing123
      nastype   = other
  }

*/etc/freeradius/users* -- has users and MAC addresses to authenticate.
Tunnel-Private-Group-Id shows a dynamic VLAN ID, which is optional.

.. code-block:: text

  leaf1-server1 ClearText-Password := "password"

  "52:54:00:37:d5:e1" Cleartext-Password := "52:54:00:37:d5:e1"
      Tunnel-Type = VLAN,
      Tunnel-Medium-Type = IEEE-802,
      Tunnel-Private-Group-Id = "50"

Although this example shows a simple credential, actual implementations may use
any EAP method that both the client and RADIUS server support.

Client Supplicant Configuration Tasks
-------------------------------------
Client supplicant configuration tasks are external to AOS.
The following is an example for *wpa_supplicant*.

*/etc/wpa_supplicant/aos_wpa_supplicant.conf*

.. code-block:: text

  # Ansible managed
  ctrl_interface=/var/run/wpa_supplicant
  # Default version is 0 - ensure we're using modern protocols.
  eapol_version=2
  # Don't scan for wifi.
  ap_scan=0
  # Hosts will be configured to authenticate with usernames that match their
  # Slicer DUT name, configured in radius_server playbook.
  network={
      key_mgmt=IEEE8021X
      eap=TTLS MD5
      identity="leaf1-server1"
      anonymous_identity="leaf1-server1"
      password="password"
      phase1="auth=MD5"
      phase2="auth=PAP password=password"
      eapol_flags=0
  }

Creating AAA Server
-------------------
#. From the blueprint, navigate to **Staged / Catalog / AAA Servers**, then
   click **Create AAA Server**.
#. Enter a label, select the server type (RADIUS 802.1x, RADIUS COA), enter a
   hostname, key, auth port, and (optional) accounting port.
#. Click **Create** to stage the server and return to the list view.

Editing AAA Server
------------------
#. From the blueprint, navigate to **Staged / Catalog / AAA Servers**, then
   click the **Edit** button for the AAA server to edit.
#. Make your changes, then click **Update** to stage the update and return to
   the list view.

Deleting AAA Server
-------------------
#. From the blueprint, navigate to **Staged / Catalog / AAA Servers** and
   click the **Delete** button for the AAA server to delete.
#. Click **Delete** to stage the deletion and return to the list view.
