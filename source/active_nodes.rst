=====
Nodes
=====

.. image:: static/images/blueprints/active/physical/active_node_330.png

From the blueprint, navigate to **Active / Physical / Nodes** to see the nodes in
the active topology, either in table view or card view. You can search for
specific nodes or links and see more details about a specific node by clicking
its name.

.. _apply_full_config:

Applying Full Config
====================
See :ref:`Configuration Deviation <configuration_deviation>` for more information
about when to apply a full config.

.. warning::
    Applying a full config is a disruptive operation and will result in a
    temporary loss of service to the device.

#. From the blueprint, navigate to **Active / Physical / Nodes** and select the
   device.
#. From the selection panel (right-side) click **Device**, then click **Rendered**,
   **Incremental**, or **Pristine** to review the different configurations.
#. Click **Apply Full Config**.
