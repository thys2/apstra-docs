====================
Processor: Set Count
====================
The Set Count processor groups as described in **Group by**, then calculates
the number of items in each group.

**Input Types** - Number-Set (NS), Discrete-State-Set (DSS), TS

**Output Types** - Number-Set (NS)

**Properties**

   .. include:: includes/processors/group_by.rst

   .. include:: includes/processors/enable_streaming.rst

Set Count Example
-----------------
See :doc:`standard deviation example <processor_standard_deviation>` -
it is the same except we calculate the number of stage items.
