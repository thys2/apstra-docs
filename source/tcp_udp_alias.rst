====================
TCP/UDP Port Aliases
====================

TCP/UDP Port Alias Overview
===========================
When you create a security policy and add rules for TCP or UDP protocols, a
source port and destination port are specified. You can enter port numbers or you
can create aliases ahead of time that can be entered instead of the port numbers.
For example, you could create an alias with name *SSH* and a value of *22*.

**To access TCP/UDP ports** - from the AOS web interface, navigate to
**Design / TCP/UDP Ports**.

.. image:: static/images/tcp_alias/port_alias_330.png

.. sidebar:: Cloning TCP/UDP Port Alias

   Instead of entering all details for a new TCP/UDP port alias, you can clone an
   existing one, give it a unique name, and customize it.

Creating TCP/UDP Port Alias
===========================
#. From the list view (Design / TCP/UDP Ports) click **Create Port Alias**, then
   enter a name and one or more values.
#. Click **Create**. When you add a rule for TCP or UDP protocols to a security
   policy, the TCP/UDP port alias will appear in the drop-down list.

Editing TCP/UDP Port Alias
==========================
#. From the list view (Design / TCP/UDP Ports) click the **Edit** button for the
   port alias to edit.
#. Make your changes.
#. Click **Update** to update the TCP/UDP port alias and return to the list view.

Deleting TCP/UDP Port Alias
=============================
#. From the list view (Design / TCP/UDP Ports) click the **Delete** button for the
   port alias to delete.
#. Click **Delete** to delete the TCP/UDP port alias from the system.
