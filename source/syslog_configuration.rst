====================
Syslog Configuration
====================
You can forward audit messages and anomaly messages to one or more external Syslog
servers from AOS, then AOS will generate alarms that can be used in an NMS
alarming environment.

From the AOS web interface, navigate to
**Platform / External Services / Syslog Configuration**.

.. image:: static/images/platform/syslog_config_330.png

Configuring Syslog Servers
==========================
#. From the AOS web interface, navigate to
   **Platform / External Services / Syslog Configuration**, then click
   **Create Syslog Config**.
#. Configure the Syslog server with an **IP Address** or hostname, **Port**,
   **Protocol** (UDP or TCP), and **Facility** (kern, user, mail, daemon, auth,
   syslog, lpr, news, uucp, authpriv, ftp, cron, local0, local1, local2, local3,
   local4, local5, local6, local7).
#. Click **Create** to save the configuration and return to the list view.
   Messages are not automatically sent to the Syslog server.
#. To configure another Syslog server, repeat the steps above.
#. To enable messages to be sent to the configured server(s), toggle on
   **Use for Audit** and/or **Forward Anomalies**, as appropriate.

Syslog messages follow Common Event Format (CEF) conventions as shown below:

.. code-block:: text
   :caption: CEF in Syslog Messages

    AOS Log Format:

    '{timestamp} {host} '
           'CEF:{version}|{device_vendor}|{device_product}|{device_version}|'
           '{device_event_class_id}|{name}|{severity}|{extension}

    Where:

      {version}        : always "0"
      {device_vendor}  : always "Apstra"
      {device_product} : always "AOS"
      {device_version} : current AOS version
      {device_event_class_id} : "100" for audit logs, "101" for anomaly logs
      {name}           : "Audit event" for audit logs, "Alert" for anomaly logs
      {severity}       : "medium" for audit logs, "Very-High" for anomaly logs

    And where {extension} is either :

      For anomaly logs : msg=<json payload>
      For audit logs   : cat=<activity> src=<src_IP> suser=<username> act=<activity result> cs1Label=<field1_type> cs1=<field1_value> cs2Label=<field2_type> cs2=<field2_value> cs3Label=<field3_type> cs3=<field3_value>

    Anomaly Log JSON Format

      u'blueprint_label' : Name of the blueprint the anomaly was raised in.
      u'timestamp'       : Unix timestamp when the Anomaly was raised.
      u'origin_name'     : Serial Number of the device the anomaly affects.
      u'alert'           : The value is a JSON Payload with the actual anomaly (see next table)
      u'origin_hostname' : Hostname of the device the anomaly affects.
      u'device_hostname' : Hostname of the device the anomaly affects.
      u'origin_role      : Role of the device the anomaly affects.
      u'first_seen'      : Unix timestamp when the Anomaly was raised for the first time.
      u'raised'          : Always True.
      u'severity         : The severity level of the anomaly. Always 3.

    Audit Log Extension Format:

      cat       : Activity performed. Valid values: “Login”, “Logout”, “BlueprintCommit”, “DeviceConfigChange”, “BlueprintDelete”.
      src       : Source IP of the client making HTTP requests.
      suser     : Who performed the activity.
      act       : Outcome of the activity - free-form string. In case of error, include error string. Ex: Unauthorized
      cs1Label  : The string “Blueprint Name”
      cs1       : Name of the blueprint on which action was taken.
      cs2Label  : The string “Blueprint ID”
      cs2       : Id of the blueprint on which action was taken.
      cs3Label  : The string “Commit Message”. Only exists if user has added a commit message (optional)
      cs3       : Commit Message. Only exists if user has added a commit message (optional)
      deviceExternalId : Id (typically serial number) of the managed device on which action was taken.
      deviceConfig  : Config that is pushed and applied on the device where “#012” is used to indicate a line break to log collectors and parsers.

An example of Syslog messages is shown below:

.. code-block:: text
   :caption: Syslog Message Example

    Jul 31 03:10:36 aos-server - 2019-07-31T03:10:36.797839+0000 aos-server
    CEF:0|Apstra|AOS|3.0.0-151|101|Alert|Very-High|msg={'device_hostname':
    '<device hostname unknown>', u'timestamp': 1564542636797839, u'alert':
    {u'probe_alert': {u'stage_name': u'Anomaly', u'actual_int': 9711, u'probe_id':
    u'46b827c2-be4d-47a5-95f9-1ed0a57224f6', u'key_value_pairs': [{u'value':
    u'"Ethernet5"', u'key': u'interface'}, {u'value': u'"2CC260994101"', u'key':
    u'system_id'}], u'item_id': u'anomaly,probe\=46b827c2-be4d-47a5-95f9-1ed0a57224f6
    ,proc\=Anomaly,stage\=out,interface\=Ethernet5,system_id\=2CC260994101',
    u'expected_int': 8000}, u'first_seen': 1564542636797795, u'raised': True,
    u'severity': 3, u'id': u'4981e646-e665-481b-bada-391689e7ebf3'}, u'origin_name':
    u'anomaly,probe\=46b827c2-be4d-47a5-95f9-1ed0a57224f6,proc\=Anomaly,stage\=out,
    interface\=Ethernet5,system_id\=2CC260994101'}

    Jul 31 03:11:01 aos-server - 2019-07-31T03:11:01.699190+0000 aos-server
    CEF:0|Apstra|AOS|3.0.0-151|101|Alert|Very-High|msg={'device_hostname':
    '<device hostname unknown>', u'timestamp': 1564542661699190, u'alert':
    {u'probe_alert': {u'stage_name': u'Anomaly', u'actual_int': 12890, u'probe_id':
    u'46b827c2-be4d-47a5-95f9-1ed0a57224f6', u'key_value_pairs': [{u'value':
    u'"Ethernet5"', u'key': u'interface'}, {u'value': u'"2CC260994101"', u'key':
    u'system_id'}], u'item_id': u'anomaly,probe\=46b827c2-be4d-47a5-95f9-1ed0a57224f6,
    proc\=Anomaly,stage\=out,interface\=Ethernet5,system_id\=2CC260994101',
    u'expected_int': 8000}, u'first_seen': 1564542636797795, u'raised': False,
    u'severity': 3, u'id': u'4981e646-e665-481b-bada-391689e7ebf3'}, u'origin_name':
    u'anomaly,probe\=46b827c2-be4d-47a5-95f9-1ed0a57224f6,proc\=Anomaly,stage\=
    out,interface\=Ethernet5,system_id\=2CC260994101'}
