=======
Widgets
=======

Widget Overview
===============
Widgets generate data based on IBA :doc:`probes <probes>`. Depending on the type
of widget, they can either return a total count of a particular type of anomaly,
or they can display outputs generated from stages and processors in an IBA probe.
Some widgets are created automatically (but they are not deleted automatically).
Widgets can be viewed by themselves or they can be added to
:doc:`analytics dashboards <dashboard_analytics>`. You can create custom widgets
before or during dashboard creation.

From the blueprint, navigate to **Analytics / Widgets**.

.. image:: static/images/analytics/widgets/analytics_widgets_330.png

.. sidebar:: Cloning Widgets

   Instead of entering all details for a new widget, you can clone an
   existing one, give it a new name and customize it.

Creating Anomaly Heat Map Widget
================================
Anomaly heatmap widgets count the anomalies from tagged IBA probes and stages.

#. From the blueprint, navigate to **Analytics / Widgets**, then click
   **Create Widget**.
#. Select **Anomaly Heat Map** from the **Type** drop-down list and enter a name.
#. Enter row tags, column tags, and (optional) description.
#. Click **Create** to create the widget and return to the list view. Creating a
   large widget may take some time. You can check the status under the
   **Active Tasks** section at the bottom of the screen.

Creating Stage Widget
=====================
Stage widgets include outputs from a stage of an IBA probe.

#. From the blueprint, navigate to **Analytics / Widgets**, then click
   **Create Widget**.
#. Select **Stage** from the **Type** drop-down list and enter a name.
#. Select a probe and a stage, then customize the output as needed.
#. Click **Create** to create the widget and return to the list view. Creating a
   large widget may take some time. You can check the status under the
   **Active Tasks** section at the bottom of the screen.

Editing Widget
==============
Auto-created widgets can be modified, but defaults should work in most cases.
When you edit widgets that are used in a dashboard you'll get the message
‘This widget is currently in use from dashboard(s). Editing this widget will
affect those dashboard(s)’.

#. From the list view (Analytics / Widgets) or the details view, click the
   **Edit** button for the widget to edit.
#. Make your changes.
#. Click **Update** to stage the changes and return to the list view.

Deleting Widget
===============
A widget that is used in a dashboard cannot be deleted.

#. From the list view (Analytics / Widgets) or the details view, click the
   **Delete** button for the widget to delete.
#. Click **Delete Widget** to stage the deletion and return to the list view.
