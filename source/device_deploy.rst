================
Deploying Device
================

As explained in :doc:`AOS device configuration lifecycle <configuration_lifecycle>`,
**Deploy** is one of several modes any device can have. This topic explains in more
detail what happens when a device mode is set to Deploy.

Generally, by "Deploy" we mean changing the device mode to **Deploy** AND
committing that change to the blueprint. Also note it is entirely possible (and in
fact common practice) to have a committed blueprint with no deployed devices.
Users can deploy devices as and when required, be it in batches, one by one, or all
in one go.

The implications of deploying a device can be summarized into two main categories:

#. The **full** AOS rendered configuration is applied to the device.
#. Anomalies associated to this device are now raised on the dashboard.

Immediately upon successful deployment, AOS collects the full running
configuration. AOS continuously collects and matches the running config against
this "Golden Config".

Protocol related anomalies like BGP or LLDP are only raised if devices at both ends
are deployed.

A device's deployment status can be seen on the dashboard and should become visible
as *SUCCEEDED* quickly after the deploy mode is committed to the blueprint:

.. image:: static/images/device_management/deployment_status.png
      :width: 300px

AOS may raise several anomalies immediately after deploying. This is
because Intent now dictates certain states which AOS has not been able to verify yet.
One could say AOS 'assumes' an anomaly until it is verified to be ok. At this point,
telemetry data is being checked against Intent, and these anomalies will clear again
if there is a match. This can take some time in some cases, eg. BGP sessions that
come up and routes that are being advertised.

Vendor specifics
----------------

Implications of deploying a device can vary across different vendors. Below are
known NOS characteristics that result in differences in the way anomalies are raised
by AOS.

Juniper Junos
,,,,,,,,,,,,,

#. "show interface" commands do not list interfaces on ports that do not have
   a transceiver plugged in. This means AOS can not raise any *Interface Down*
   anomalies for these interfaces. Such interfaces can be recognized using the
   'show virtual-chasses vc-port', and will have a status of 'Absent'.

#. If a Virtual Network endpoint is configured on a leaf interface, AOS expects an
   EVPN type 3 route for that interface. If this interface is down, Junos does not
   advertise the RT-3, resulting in a "Missing Route" anomaly in AOS. If this
   anomaly is undesirable it is recommended to remove the interface from the VN
   until the interface is up.
