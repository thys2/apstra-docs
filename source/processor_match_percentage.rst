===========================
Processor: Match Percentage
===========================
For each input group, the Match Percentage processor creates a single output that
is the percentage of items in the input group that are equal to the reference.

**Input Types** - Discrete-State-Set (DSS), TS

**Output Types** - Number-Set (NS)

**Properties**

   .. include:: includes/processors/group_by.rst

   Reference State (reference_state)
     DS or TS value which is used as a reference state to match input samples.

   .. include:: includes/processors/enable_streaming.rst

Match Percentage Example
------------------------
Assume a configuration of:

.. code-block:: none

  reference_state: "false"
  group_by: []

Sample Input:

.. code-block:: none

  [if_name=eth0] : "true"
  [if_name=eth1] : "true"
  [if_name=eth3] : "false"

Sample Output:

.. code-block:: none

  [] : 33

In the above example, we have 33% as the output because 33% of the
input group match the reference value of "false".
