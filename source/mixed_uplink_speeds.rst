============================================
Mixed Uplink Speeds between Leafs and Spines
============================================
.. versionadded:: 3.2.1

As of AOS 3.2.1, heterogeneous speeds between Spine and Leaf planes are allowed.
This applies to the following:

 * AOS Rack Based Templates
 * Rack Change Operations in a Blueprint

.. important::

   * It is not possible to have mixed speed on parallel links between the same
     devices
   * As a prerequisite, the Spine Logical Device should have mixed port speeds
     defined, specifying the port role as Leaf for the required number of ports.
     This should be as per the **day 0** design requirements while keeping in mind
     how many rack need to be added, so that sufficient ports for Spine to Leaf
     connections with mixed link speed are available.

.. warning::
  AOS does not support updating of Logical Device of a Spine already used in a
  Blueprint. For this manual patching using AOS CLI will be required. AOSCLI is an
  experimental tool and may not be able to provide a solution. Please contact
  :doc:`Apstra Global Support <support>` for further assistance.

This allows for the following operations:

#. Create a Rack Based template with Rack Types having different link
   speeds to Spines.

   * For example, create a L2 Rack Type with Logical Device
     AOS-7x10-Leaf and AOS-40x10+6x40 for two Leaf switches, having respectively
     10 GbE and 40 GbE as uplinks towards the Spines.

     .. image:: static/images/operational/rack_type_mixed_speed.png

   * Then design a Rack Based template based on the mixed line speed Rack
     Type as above.

     .. image:: static/images/operational/rack_template_mixed_speed.png

#. The user can create a "POD Based" Template with such Rack Based template as per
   screenshot below:

   .. image:: static/images/operational/pod_template_mixed_speed.png

#. AOS allows adding a rack with different uplink speed to Spines in an existing
   Blueprint.

   * In the below example, the user can verify Logical Device AOS-48x10-4x40 for
     Spine switch having port role Leaf and mixed link speed
     (i.e 10 GbE and 40 GbE) ports being used in the Rack Type.

     .. image:: static/images/operational/spine_mixed_ld.png

   * The user is able to add more racks with different type link speeds between
     Leaf-spine as below:

     .. image:: static/images/operational/add_racks.png
