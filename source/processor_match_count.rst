======================
Processor: Match Count
======================
For each input group, the Match Count processor creates a single output that is
the number of items in the input group that are equal to the reference. The
'total_count' key is added into output item keys where the value is a number of
items in an input group.

**Input Types** - Discrete-State-Set (DSS), TS

**Output Types** - NS

**Properties**

   .. include:: includes/processors/group_by.rst

   Reference State (reference_state)
     DS or TS value which is used as a reference state to match input samples.
     discrete-state value

   .. include:: includes/processors/enable_streaming.rst

Match Count Example
-------------------
Assume a configuration of:

.. code-block:: none

  reference_state: "false"
  group_by: []

Sample Input:

.. code-block:: none

  [if_name=eth0] : "true"
  [if_name=eth1] : "true"
  [if_name=eth3] : "false"

Sample Output:

.. code-block:: none

  [] : 1

In the above example, we have 1 as the output because 1 element of the
input group matches the reference value of "false".
