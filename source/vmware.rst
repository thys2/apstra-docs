==========================
VMware vSphere Integration
==========================

VMware vSphere Integration Overview
===================================

With AOS vCenter integration, VM visibility is provided in virtualized
environments. This feature helps to troubleshoot any VM connectivity issues.
Any inconsistencies between virtual network settings (VMware Port
Groups) and physical networks (AOS Virtual Networks) that might affect VM
connectivity are flagged.

To do this, AOS identifies the ESX/ESXi hosts and thereby the VMs connected to
AOS-managed leaf switches. LLDP information transmitted by the ESX/ESXi
hosts is used to associate host interfaces with leaf interfaces. For this feature
to work, LLDP transmit must be enabled on the VMware distributed virtual switch.

AOS also connects to vCenter to collect information about VMs, ESX/ESXi hosts,
port groups and VDS. This collection is done as an AOS extensible telemetry
collector. The collector runs in an off-box agent and uses pyVmomi to connect
to vCenter. On first connect, it downloads all of the necessary information and
thereafter polls vCenter every 60 seconds for new updates. The collector updates
the discovered data into the AOS Graph Datastore allowing VM queries and alerts
to be raised on physical/virtual network mismatch.


Supported Versions
-------------------

As of AOS version 3.3.0 VMware vSphere/vCenter integration is currently
available for the following versions of VMware:

- vCenter Server/vSphere 6.7
- vCenter Server/vSphere 6.5

Enabling vSphere Integration
============================
To enable vSphere Integration, only **Read** permission is required.

#. From the AOS web interface, navigate to
   **External Systems / Virtual Infra Managers**, then click
   **Create Virtual Infra Manager**.

   .. image:: static/images/vsphere/virtual_infra_mgr_create_330.png

#. Enter the vCenter IP address (or DNS name), select
   **VMware vCenter Server**, then enter a username and password.
#. Click **Create** to launch an offbox container running vCenter.
   While the container is connecting, it is in a DISCONNECTED
   state. When the container successfully connects, the state changes to
   CONNECTED.
#. When vCenter is connected, from the blueprint, navigate to
   **Staged / Virtual / Virtual Infra**, then click **Add Virtual Infra**.

   .. image:: static/images/vsphere/virtual_infra_add_330.png

#. Select the vCenter Server from the **Virtual Infra Manager** drop-down list,
   then click **Create** to stage the change.
#. When you are ready to deploy, commit the changes from the **Uncommitted** tab.

VM Visibility
=============
When virtual infra is being managed by AOS, you can query their VMs by name.
From the blueprint, navigate to **Active / Query / VMs** and enter search
criteria. VMs include the following details:

Hosted On
    The ESX host that the VM is on

VM IP
    The IP address as reported by vCenter after installation of VM tools. If the
    IP address is not available this field is empty. AOS displays the VM IP
    address if the IP address is available on installation VM tools on the VM.

Leaf:Interface
    The leaf and the interface ESX host is connected to

Port Group Name:VLAN ID
    The VNIC’s portgroup and the VLAN ID associated with the portgroup

MAC Addresses
    MAC address of the VNIC

Virtual Infra Address
    IP address of the vCenter the VM is part of

Validating Virtual Infra Integration
====================================

.. include:: includes/virtual_infra_validation.rst

vCenter Policy-Based Auto-Remediation
=====================================

vCenter Policy-Based Auto-Remediation Overview
----------------------------------------------
Beginning in AOS 3.1, AOS provides automatic remediation of virtual network
anomalies without user intervention (as of AOS version 3.1).
This helps by reducing operational cost where a network operator
does not need to investigate each anomaly and check for details and intervene to
mitigate anomalies. VxLAN auto-remediation is a policy configured while adding
vCenter/NSX-T to an AOS Blueprint. Remediation of anomalies is done in
accordance with this policy.

AOS provides a policy-based auto-remediation approach to automatically
notify you if there is a mismatch between vSphere DPG (VMware Port Groups)
and AOS VN in a particular Blueprint, or if there is a VLAN mismatch between Virtual
Infra and AOS Fabric, or if there is a mismatch in LAG configuration on hypervisors
and the corresponding leaf ports. AOS provides automatic guided remediation of
such anomalies.

Enabling VXLAN vCenter Policy-Based Auto-Remediation
-----------------------------------------------------

#. From the blueprint, navigate to
   **Staged / Virtual / Virtual Infra**, then click Add **Virtual Infra**.
#. Select the **Virtual Infra Manager** from the drop-down list.
#. Click **VLAN Remediation Policy** to see the attributes to configure.
#. Select the **VN Type** from the drop-down list.

   * **VXLAN** (inter-rack) (default)
     Assumes VXLAN virtual network and looks for VN mismatch in all of the related
     ToRs in the AOS fabric.

   * **VLAN** (rack-local)
     Choosed VLAN if the VLAN footprint on local vSphere does not extend to other
     ToR leafs in a fabric.

#. Select the **Security zone**: If VN type is **rack-local** only the default
   security zone is allowed.
#. Click **Create**.

After enabling the VLAN remediation policy as inter-rack, AOS searches for
matching local VLANs in all ToRs connecting any member host (i.e hypervisor)
participating in the virtual infra virtual network. If such a VN is found, it
simply extends that VN to also be bound to the ToR in question with the same
local VLAN. If not found, a new inter-rack VN is created in the specified
security zone.

Constraints and Validations
---------------------------

Please find some of the constraints and validations that take place
before the remediation happens:

* When remediation policy is set to VLAN, that is rack-local, security zone
  can only be the default one.

* If VLAN ID for virtual network spanning multiple hypervisors is the same,
  then it is assumed to be a single layer 2 broadcast domain. For such scenarios,
  the VLAN remediation policy must be set to VXLAN as for any missing
  VLAN anomalies it is checked on all the ToR leafs connected to different
  hypervisors having virtual network with the same VLAN ID. If this is mistakenly
  chosen as VLAN type, validation errors are thrown.

* AOS throw errors if different types of remediation policies (For example,
  if one is VXLAN type and other is VLAN type) are found attached to different
  Virtual Infras (such as two different vCenter servers) having the same
  VLAN ID in anomalies.

* If two different Virtual Infra servers are mapped in a blueprint and they
  have the same VLAN IDs then it is checked as two separate virtual networks
  by VXLAN auto-remediation policy.

vCenter Policy-Based Auto-Remediation Features
----------------------------------------------

AOS policy-based remediation has the following features:

* VLAN mismatch anomalies create one virtual network for one vCenter
  Distributed Virtual Switch (vDS) port group that is attached to hypervisors
  connected to leaf ports of ToRs in AOS fabric.

* AOS does not allow the deletion of the security zone that is being
  referenced in remediation policy.

.. note::

    For an EVPN enabled fabric, it is recommended to have VN type as inter-rack
    or VXLAN in a specific security zone.

Remediation Steps - Resolve Probe Anomalies
-------------------------------------------

#. From the blueprint, navigate to **Analytics / Probes** and click one of the
   instantiated predefined probe names.
#. Click **Remediate Anomalies** on a given stage. AOS automatically updates the
   staged blueprint by **adding/removing/updating VN endpoints** and **VNs** to
   resolve the anomalies.
#. Review the staged configuration in terms of virtual network parameters,
   then commit the configuration. AOS indicates if there are no detected changes.
   This could happen if you invoke remediation more than once.
#. Review and commit the changes on the **Uncommitted** tab.
#. Return to the predefined probe to view any remaining anomalies.

Disabling Virtual Infra Integration
===================================

.. include:: includes/integration_disable.rst
