===========
AOS Cluster
===========
As features are added to AOS, the demand for compute resources grows beyond the
capacity of a single virtual machine (VM). AOS allows clustering multiple VMs,
specifically for scaling AOS off-box agents and IBA probe processing units
(as of AOS 3.0).

When a worker VM is added to the main AOS controller VM, it registers with the
controller VM through sysdb, collects facts about the VM (such as
core/memory/disk configuration and usage), and launches a container on the
local VM. The controller VM reacts to REST API requests, configures the worker
VM for joining or leaving the cluster, and keeps track of cluster-wide runtime
information. It also reacts to container configuration entities and schedules
them to the worker VM.

.. sidebar:: AOS REST API and AOS Clusters

   In addition to managing AOS VMs using the AOS web interface (as described
   below) you can also use AOS REST API. Navigate to **Platform / Developers**
   for **REST API Documentation** and tools. See the **cluster** section.

AOS VMs include the following details:

Static Configuration
   Address
      IP address or Fully-qualified Domain Name (FQDN) of the VM

   Name
      AOS VM name, such as **controller** (the main AOS controller) or **node1**
      (a worker node)

   State
      ACTIVE, MISSING, or FAILED

   Roles
      Controller or worker

   Tags
      The function of the node: iba and/or offbox

   Capacity Score
      Calculated by AOS

   CPU
      Number of CPUs

Errors
   If an error exists, a message appears here. For example, the following image
   shows the error message when an agent process has been restarted because an
   agent has crashed.

   .. image:: static/images/aos_cluster/aos_cluster_error_1.png

Usage
   Container Service Usage
      Current VM container server usage (Percentage)

   Containers Count
      Number of containers

   Memory Usage
      Current VM memory usage (percentage)

   CPU Usage
      Current VM CPU usage (percentage)

   Disk Usage
      Current VM disk usage per logical volume (GB and percentage)

Containers
   The containers that are running on the node and the resources used by each
   container

Username
   AOS Web interface username login credential, such as *admin*

Password
   AOS web interface password login credential


**To go to AOS Clusters** - from the AOS web interface, navigate to
**Platform / AOS Cluster**. You can health check the VMs from here. To see more
information, click an AOS VM name.

.. image:: static/images/aos_cluster/aos_cluster_330.png

.. sidebar:: Cloning AOS VM

   Instead of entering all details for a new VM, you can clone an existing one,
   give it a new name and customize it.

Creating AOS VM
===============
#. :doc:`Install AOS <server_installation>` on the VMs to be clustered, making
   sure they are all the same AOS version as the main AOS controller (which acts
   as the cluster manager). If they are not the same AOS version, the AOS
   controller will not accept them as part of the cluster.
#. From the list view (Platform / AOS Cluster) click **Add Node**, then
   enter a name, tags (optional), address (IP or FQDN), username and password.
#. Click **Create**. As the main AOS controller connects to the new AOS VM worker
   node, the state of the new AOS VM changes from **INIT** to **ACTIVE**.

Editing AOS VM
==============
#. Either from the list view (Platform / AOS Cluster) or the details view,
   click the **Edit** button for the AOS VM to edit.
#. Make your changes.
#. Click **Update** to update the AOS VM worker Node.

Deleting AOS VM
===============
#. Either from the list view (Platform / AOS Cluster) or the details view,
   click the **Delete** button for the AOS VM to delete.
#. Click **Delete** to delete the AOS VM.
