=====
Nodes
=====
From the staged nodes view you can edit various node properties and device details,
and access device configuration (rendered, incremental, pristine).

.. image:: static/images/blueprints/staged/physical/nodes/staged_node_overview_330.png

.. _chg_name:

Changing Server Names and Hostnames
===================================
You can edit multiple server names and hostnames at the same time, fetch discovered
LLDP data (hostnames), and update the names based on hostnames, all from the same
dialog.

#. From the blueprint, navigate to **Staged / Physical / Nodes**.
#. Click the **Edit server names and hostnames** button above the nodes (table
   view / card view) and make your changes.

   * To change names, select a name and enter a different one.
   * To fetch discovered LLDP data (hostnames), click its button.
   * To update the names based on hostnames, click its button.

   .. image:: static/images/blueprints/staged/physical/nodes/staged_node_edit_names_330.png

#. Click **Update** to stage the changes and return to the staged physical view.

Any associated link names do not automatically update to match the new server
names and/or hostnames. As of AOS version 3.3.0 you can manually
:ref:`change the link names <change_link_name>` to match so when you are reviewing
an updated cabling map the names will align.

.. _change_node_deploy_mode:

Setting Deploy Mode for Multiple Nodes
======================================
You can change the deploy mode for one or more nodes at the same time from the
nodes view.

#. From the blueprint, navigate to **Staged / Physical / Nodes**.
#. Check the boxes for one or more nodes that are to be changed to the same deploy
   mode, then click the **Set Deploy Modes** button.
#. Select the new deploy mode.
#. To filter the node selections before changing deploy mode, use the query.
#. Click **Set Deploy Mode** to stage the change and return to the list view.

Editing Node Properties
=======================
Selecting a node gives you access to change various attributes such as name,
interface map, ASN, and loopback IP, depending on the node chosen. The attributes
that can be edited have an **Edit** button associated with them.

.. versionadded:: 3.3.0

   Leaf pair names can be changed. If the names of leafs that are used in a leaf
   pair have been changed, the leaf pair name does not automatically change to
   match the new leaf names. As of AOS version 3.3.0, you can manually change the
   leaf pair name to correspond with the new leaf names. This is especially useful
   when assigning them during virtual network creation.

#. From the blueprint, navigate to **Staged / Physical / Nodes**.
#. Select a node from the table view or card view. In the right panel, click
   **Properties**, if not already selected.

   .. image:: static/images/blueprints/staged/physical/nodes/staged_node_properties_330.png

#. Change properties, as applicable.
#. Click the **Save** button to stage the changes.

Editing Device Details
======================
In addition to editing node properties, you can change device details such as
deploy mode, system ID (serial number), and hostname. You can also access device
configuration files from the device tab.

Example: You can suppress L2 server cabling anomalies by setting the
**Deploy Mode** to **Undeploy**. This is useful when you want to pre-provision
virtual networks on all L2 servers in a blueprint, even if some L2 servers are
not actually part of the network yet. (You can also get to the editing panel by
navigating to Staged / Physical / Topology and selecting the server from there.)

#. From the blueprint, navigate to **Staged / Physical / Nodes**.
#. Select a node from the table view or card view. In the right panel, click
   **Device**, if not already selected.

   .. image:: static/images/blueprints/staged/physical/nodes/staged_node_device_330.png

#. Change device details, as applicable.
#. Click the **Save** button to stage the changes.
