================
Processor: Ratio
================
The Ratio processor calculates the ratio of inputs. It takes two inputs:
numerator and denominator. Denominator is optional and could be specified as
'denominator' configuration property instead. It could be either an integer or
an expression that evaluates to an integer. It should not be '0'.

When 'denominator' is specified as an input, 'numerator' and 'denominator' input
items must allow only 1:1 mapping. If that is not the case, 'significant_keys'
configuration property should be specified to list keys that will allow such
mapping.

It also supports 'multiplier' configuration property, which is an integer
value greater than one to multiply numerator by before calculating ratio. This
allows it to overcome limitations of dealing with integers. Default value is 100.

**Input Types** - Number-Set (NS)

**Output Types** - Number-Set (NS)

**Properties**

   Denominator
      Integer or an expression that evaluates to integer that is used as
      denominator. Optional denominator value if it's not specified as input;
      should be non-zero integer or an expression that evaluates to non-zero
      integer.

   .. include:: includes/processors/significant_keys.rst

   Multiplier
      Multiply numerator by a given value before calculating ratio.
      Optional. Default is 100.

   .. include:: includes/processors/enable_streaming.rst

Ratio Output Example
--------------------
Simple scenario with a static denominator.

.. code-block:: none

    denominator: 100
    multiplier: 1

Input 'numerator':

.. code-block:: none

    [system_id=spine1,role=spine,interface=eth0]: 300
    [system_id=spine2,role=spine,interface=eth1]: 500

Output:

.. code-block:: none

    [system_id=spine1,role=spine,interface=eth0]: 3
    [system_id=spine1,role=spine,interface=eth1]: 5

Configuration where numerator and denominator are coming from inputs,
and 'multiplier' value is the default 100:

.. code-block:: none

     significant_keys: ['system_id', 'interface']

Input 'numerator':

.. code-block:: none

    [system_id=spine1,role=spine,interface=eth0]: 300
    [system_id=spine2,role=spine,interface=eth1]: 750

Input 'denominator':

.. code-block:: none

    [system_id=spine1,role=spine,interface=eth0]: 150
    [system_id=spine2,role=spine,interface=eth1]: 250

Output:

.. code-block:: none

    [system_id=spine1,interface=eth0]: 200
    [system_id=spine1,interface=eth1]: 300
