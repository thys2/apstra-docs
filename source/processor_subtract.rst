===================
Processor: Subtract
===================
One N is created on output per each N with the same properties in both inputs.
For each input item the processor leaves only significant keys, drops the others
and puts the result. If there is no common set of properties between both inputs,
the output is the empty set.

**Input Types** - Number-Set (NS)

**Output Types** - Number-Set (NS)

**Properties**

   .. include:: includes/processors/significant_keys.rst

   .. include:: includes/processors/enable_streaming.rst
