=======================
Draining Device Traffic
=======================

.. versionadded:: 3.3.0 AOS supports draining traffic from ESI leaf pairs.

Devices can be gracefully taken out-of-service for maintenance
(or decommissioning) by draining them of traffic.

#. :ref:`Change the deploy mode <change_deploy_modes>` on the device to **Drain**.
#. :doc:`Commit <uncommitted>` the change. While TCP sessions drain (which could
   take some time, especially for EVPN blueprints) BGP anomalies are expected.
   When drain configuration is complete, the temporary anomalies will be resolved.

   .. image:: static/images/blueprints/dashboard/dashboard_drain_anomalies_330.png

Monitoring Traffic Draining
---------------------------
While a device is draining you can monitor its progress from the AOS web interface
from various locations:

* From the **Deployment Status** section of the blueprint dashboard (Drain Config).

  .. image:: static/images/blueprints/dashboard/dashboard_drain_config_330.png

* From **Active / Physical** in the **Status** panel (Deployment Status: Drain).

  .. image:: static/images/blueprints/active/physical/active_physical_status_drain_330.png

* From **Analytics / Dashboards** when the predefined **Drain Validation**
  dashboard is :ref:`instantiated <instantiate_dashboard>`. (If you set the
  dashboard as default, you can see it on the blueprint dashboard as well as on
  the analytics dashboard). In the image below, traffic is still in the process of
  draining.

  .. image:: static/images/blueprints/analytics/dashboard/dashboard_drain_validation_with_anomaly_330.png

After performing device maintenance, simply change the deploy mode back to
**Deploy** and :doc:`commit <uncommitted>` the change to bring the device back
into active service.
