===========
EVPN Probes
===========

To troubleshoot EVPN routes, AOS provides 3 pre-defined EVPN probes:
**VXLAN Flood List Validation** , **EVPN VXLAN Type-3 Route Validation**
and **EVPN VXLAN Type-5 Route Validation**

AOS users can make use of these probes for troubleshooting EVPN routing.

From the Probes view of your Analytics Blueprint, click Instantiate Predefined
Probe button, then find any of these probes in the list that pops up, then click
Create.

.. figure:: static/images/iba_tutorial/probe_evpn_create.png

EVPN dashboard is NOT auto-enabled. Customers must manually enable it as needed.
The dashboard, if enabled, auto enables two probes:

  * **VXLAN Flood Probe**: To monitor all VNs on all devices

  * **EVPN VXLAN Type-3 Probe**: This probe is created with empty “monitored_vn”
    list, i.e. the probe monitors no VN at all. Customers need to manually update
    this configuration option as needed.


.. note::

    **EVPN Type 5 Routes Probe** is not part of EVPN dashboard.

These probes give the following parameters that user can configure at
creation time or update subsequently:

  * **Probe Label**: Custom name for the probe.

  * **Anomaly Time Window** : The average period duration for interface
    counters in seconds. Default of 6 minutes is for **VXLAN Flood List** probe and
    11 minutes for **EVPN VXLAN Type-3** and **EVPN VXLAN Type-5** probes.

  * **Anomaly Threshold**: If routes are missing for more than or equal to percentage
    of Anomaly Time Window, an anomaly will be raised.
    If Anomaly Time Window ATW, and Anomaly Threshold is AT. It calculates
    Z = (ATW * AT)/100 in seconds.
    E.g. If ATW = 20 seconds, AT = 5%, then Z = (20 * 5)/100 = 1 second.
    When the route is in Missing state for Z seconds from total ATW duration, anomaly
    will be raised.

  * **Collection period**: All these probes are polling based so has polling period.
    Default polling period is **10 minutes** for VXLAN Type-3 and VXLAN Type-5
    probes, but **5 minutes** for VXLAN Flood List probe. These can be modifed at
    creation time or can be updated later.

  * **Monitored VNs**: Only for EVPN VXLAN Type-3 Probe.
    Specify what Virtual Networks are to be monitored. Either list of
    desired VN's e.g. "1-3,6,8,10-13" or  " * " to monitor all VN's.



Each of these Probe has 3 route labels:

  * **Expected**: This route is expected on the device as per service defined.

  * **Missing** : This route is missing on the device when compared to the
    expected route set.

  * **Unintended**: There are no expectations rendered by AOS for this route.

.. note::

    AOS raise anomaly only for **Missing** routes. No anomalies are raised for
    **Unintended** routes.

Processors
----------

Once the probe has been created, you can click it to see a breakdown of the
probe. Click the desired option on the left side, you can select the processors:

  * **Routes**: Purpose: Collects specific routes of route table from all leafs.
    Each route has a “state” which is either Missing, Expected, or Unintended.
    Shows route table VXLAN Flood List, or EVPN Type 3 or EVPN Type 5.

    .. figure:: static/images/iba_tutorial/probe_evpn_processor_route.png
       :align: center

  * **Missing Routes** : This processor consumes in "Number of missing routes".
    Calculates number of missing routes. For all probes, the number of missing routes
    are calculated per leaf per leaf. For VXLAN Floodlist and EVPN Type 3 probes, the
    probe checks for routes per Virtual Network. For EVPN Type 5 probe, the probe checks
    for routes per route target.

  * **Sustained Missing Routes**: Raises an anomaly per System and VNI when missing routes
    are present longer that Anomaly Time Window.

  * **Total number of sustained anomalies**: Show total number of sustained anomalies.



VXLAN Flood List Validation Probe
---------------------------------

This probe validates the VXLAN flood list entries on every leaf in the network.
It collects appropriate telemetry data compares it to the set of flood list
forwarding entries expected to be present & alerts if expected entries are
missing on any device.
This probe will monitor all Virtual Networks from all devices.

.. figure:: static/images/iba_tutorial/probe_evpn_vxlan_flood.png
  :align: center

Default collection period for this probe is **5 minutes**. It is not recommended
to change the default collection interval.

EVPN VXLAN Type-3 Route Validation Probe
----------------------------------------

This probe validates EVPN Type-3 routes on every leaf in the network.
It collects appropriate telemetry data; compares it to the set of Type-3 routes
expected to be present & alerts if expected routes are missing on any device.

.. figure:: static/images/iba_tutorial/probe_evpn_type3.png
  :align: center

This probe allows customers at creation time to specify a list of Virtual Networks
for which routes are collected.

.. note::

    The maximum number of Virtual Networks in the list is limited to 10.
    However customers are allowed to specify " * " in which case all VNs are monitored.

.. warning::

    Customers must exercise caution if they choose " * " (i.e. all VNs) as the
    monitored VN list due to high cpu/memory/network I/O overhead associated
    with BGP routing table iteration on the device side.

EVPN VXLAN Type-5 Route Validation Probe
----------------------------------------

This probe validates EVPN Type-5 routes on every leaf in the network.
It collects appropriate telemetry data; compares it to the set of Type-5 routes
expected to be present & alerts if expected routes are missing on any device.

.. figure:: static/images/iba_tutorial/probe_evpn_type5.png
  :align: center

.. note::

    If enabled it will monitor all VNs from all devices. It does not provide the
    “monitored VN list” configuration option like VXLAN Type-3 probe.

.. warning::

    Customers must exercise caution if they choose * (i.e. all VNs) as the
    monitored VN list due to high cpu/memory/network I/O overhead associated
    with BGP routing table iteration on the device side.
