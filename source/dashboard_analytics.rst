==========
Dashboards
==========

Analytics Dashboard Overview
============================
Based on the state of the active (operational) blueprint, AOS automatically
creates analytics dashboards. These dashboards contain :doc:`widgets <widgets>`
that monitor different aspects of the network and raise alerts to any anomalies.
You cannot configure the trigger logic that determines when dashboards are
auto-created, but you can create/instantiate your own dashboards. AOS-generated
dashboards are labeled with **System** and user-generated dashboards are labeled
with the user's name. Dashboards can be shown in various levels of detail by
choosing a **Display mode**: summary, preview, expanded.

Additional characteristics of analytics dashboards include:

* Analytics dashboards can be added to blueprint dashboards for viewing additional
  network information on one screen. To add them, turn **ON** the analytics
  dashboards' default toggles. The dashboards will then display on the blueprint
  dashboard.
* Auto-generated dashboards are not deleted automatically.
* Probes that already exist (already created by users) and that are not modified
  after initial creation, are reused instead of creating duplicates of those probes.
* Required probes and widgets are ensured only as part of the creation of
  dashboards. Post auto-creation, any user action to update/delete referenced
  probes and widgets may cause the dashboard to enter an invalid state. AOS
  does not automatically repair such dashboards.
* For AOS upgrade, the behavior of auto-creating dashboards happens on preexisting
  active blueprints, in the same way as for the newly-created blueprints.

From the blueprint, navigate to **Analytics / Dashboards**.

.. image:: static/images/analytics/dashboards/analytics_dashboards_330.png

Configuring Auto-Enabled Dashboards
===================================
Certain auto-created dashboards generate anomalies that you may already expect for
your network. To avoid seeing these anomalies, you can delete the dashboard either
by proactively disabling auto-creation of the predefined dashboard before it is
created, or by deleting it after it has been created. Dashboards, once disabled
are no longer automatically created, except if they are re-enabled and the
respective trigger is satisfied.

#. From the blueprint, navigate to **Analytics / Dashboards**, then click
   **Configure Auto-Enabled Dashboards**. Dashboard are listed with descriptions
   giving more details including the trigger for when dashboards are auto-created.
   Widgets that are used in each dashboard are also listed.
#. Toggle the dashboards on to auto-enable them or off to disable them.

.. _instantiate_dashboard:

Instantiating Predefined Dashboard
==================================
AOS ships with several predefined dashboards.
A predefined dashboard can also be explicitly created and managed. This
is useful for scenarios where greater control over dashboard lifecycle is desirable.
There can be more than one instance of a predefined dashboard.

#. From the blueprint, navigate to **Analytics / Dashboards**, then click
   **Create Dashboard** and select **Instantiate Predefined Dashboard** from the
   drop-down list.
#. Select a predefined dashboard from the drop-down list. See below for
   descriptions of some of the predefined dashboards.
#. Click **Create** to instantiate the dashboard and return to the list view.

Device Health Summary Predefined Dashboard
------------------------------------------

.. list-table::
    :header-rows: 1
    :class: nowrap

    * - Goal
      - Trigger
      - Probes and Widgets

    * - Find issues in the system health of managed devices present in the BluePrint
      - Always
      - 1. Check that memory usage is below 80%
        2. Check that average CPU usage is below 80%
        3. Check that filesystem usage on every writable mount point is below a certain threshold

.. note::

   Ensure same metric is not collected twice from the same device.

Drain Validation Predefined Dashboard
-------------------------------------

.. list-table::
    :header-rows: 1
    :class: nowrap

    * - Goal
      - Trigger
      - Probes and Widgets

    * - Ensure drained switches are indeed drained of traffic by ensuring total bandwidth is minimal
      - Presence of at-least one drained switch
      - 1. Drain Traffic

Throughput Health Predefined Dashboard
--------------------------------------

.. list-table::
    :header-rows: 1
    :class: nowrap

    * - Goal
      - Trigger
      - Probes and Widgets

    * - Find issues in physical infrastructure that affect the available throughput caused by
        issues such as imbalanced traffic over a group of L3 (ECMP) or L2 (LAG) links
      - Manual
      - All imbalance thresholds are expressed as % of the link speed and not absolute values.
        1. Fabric ECMP imbalance
        2. External ECMP imbalance
        3. LAG imbalance - should include both MLAG and LAG

Traffic Trends Predefined Dashboard
-----------------------------------

.. list-table::
    :header-rows: 1
    :class: nowrap

    * - Goal
      - Trigger
      - Probes and Widgets

    * - Visualize traffic trends for general insights into fabric usage
      - Manual
      - 1. Total East/West Traffic
        2. Bandwidth utilization history

Virtual Infra Fabric Health Check Predefined Dashboard
------------------------------------------------------

.. list-table::
    :header-rows: 1
    :class: nowrap

    * - Goal
      - Trigger
      - Probes and Widgets

    * - Find problems in physical or virtual infrastructure that affect workload connectivity
      - Presence of at-least one virtual Infra Manager in the BluePrint
      - 1. Hypervisor VLANs missing Fabric
        2. Hypervisor Low MTU anomalies
        3. Critical Services affected by VLAN misconfiguration
        4. Hypervisor with inconsistent MTU
        5. Hypervisor PNIC LAG Status

Virtual Infra Redundancy Checks Predefined Dashboard
----------------------------------------------------

.. list-table::
    :header-rows: 1
    :class: nowrap

    * - Goal
      - Trigger
      - Probes and Widgets

    * - Find single points of failure in physical or virtual infrastructure that
        affect
        high availability and available bandwidth for workloads
      - Presence of at-least one virtual Infra Manager in the BluePrint
      - Hypervisor VLANs missing Fabric, Hypervisor Low MTU anomalies, Critical
        Services affected by VLAN misconfiguration, Hypervisor with inconsistent
        MTU, Hypervisor PNIC LAG Status

.. sidebar:: Cloning Analytics Dashboard

   Instead of entering all details for a new dashboard, you can clone an
   existing one, give it a new name and customize it.

Creating Dashboard
==================
AOS automatically creates probes and dashboards giving you instant value without
any learning curve. The probes auto-adjust based on the state of the blueprint
(examples: undeployed / unassigned device, addition / removal of virtual infra
managers).

You can also create your own dashboards to display custom information from AOS
IBA probes and stages (as of AOS version 3.0).

#. From the blueprint, navigate to **Analytics / Dashboards**, then click
   **Create Dashboard** and select **New Dashboard** from the drop-down list.
#. Enter a name and (optional) description.
#. Select a layout (one-column, two-column, three-column) and if you want the
   dashboard to appear on the **Dashboards** tab by default, toggle on **Default**.
#. Add and/or :doc:`create widgets <widgets>` to include in the dashboard.
#. Click **Create Dashboard** to create the dashboard and return to the list view.
   A large dashboard may take some time to create. You can check the status at the
   bottom of the screen under **Active Tasks**.

Editing Dashboard
=================
Auto-created dashboards can be modified, but defaults should work in most cases.

#. From the list view (Analytics / Dashboards) or the details view, click the
   **Edit** button for the dashboard to edit.
#. Make your changes.
#. Click **Update** to stage the changes and return to the list view.

Deleting Dashboard
==================
If you delete an auto-created dashboard (because it does not apply to your
network for example), AOS disables its auto-creation feature so it does not
reappear automatically.

#. From the list view (Analytics / Dashboards) or the details view, click the
   **Delete** button for the dashboard to delete.
#. If you would like AOS to delete all referenced widgets and probes that are
   exclusively referenced from this dashboard so you don't have to delete them
   manually, check the checkbox. Deleting unnecessary widgets and probes also
   frees up resources.
#. Click **Delete Dashboard** to stage the deletion and return to the list view.
