##################
Device AAA support
##################

AOS supports Device AAA (authentication, authorization and accounting) framework
including RADIUS and TACACS+ on certain platforms. This chapter details
the minimum requirements for correct AOS AAA implementations.

There are two methods to apply AAA configurations:

  #. Configlets
       This is the recommended and preferred method. This method means the
       configuration is added to a configlet which is then imported into a blueprint.
       Drawback is that local credentials need to be available from AOS so that the
       device can be added and the configlet can be applied. See
       :doc:`Configlets and Property Sets <configlets>` for
       details.
  #. User Required configuration
       It is possible to ensure pre-existing configuration is retained when a device
       is brought under AOS management. This is done by ensuring the config is in
       place before the device is brought under AOS management.
       See :doc:`Configuration Lifecycle <configuration_lifecycle>`.

.. note::
  The use of AAA framework such as TACACS+ and RADIUS is optional, and correct
  implementation is the responsibility of the end user.

.. warning::
  When using AAA framework we recommend adding a local AOS user to the devices: in
  the event AAA authentication or authorization fails when AOS performs a full
  configuration push, manual recovery (config push) is required.

.. important::
  When upgrading AOS, Device Agent, or NOS, user **must** delete Device
  AAA/TACACS configlets from the Blueprint before the upgrades. User can re-apply
  them after the upgrades.

Supported Platforms
===================

AOS supports AAA implementations on the following platform:

Cisco NXOS
``````````
The below example NXOS configuration has been tested to work correctly with AOS.
This uses both authentication and authorization:

.. code-block:: text

    tacacs-server key 7 “<key>“
    tacacs-server timeout <timeout>
    tacacs-server host <host>
    aaa group server tacacs+ <group>
      server <host>
      use-vrf management
      source-interface mgmt0

    aaa authentication login default group <group>
    aaa accounting default group <group> local
    aaa authentication login error-enable
    aaa authentication login ascii-authentication

.. important::
  For NXOS, Apstra has observed cases in which a remote user was erratically
  removed from the device, causing authentication and authorization failures. AOS
  requires the user (role 'network-admin') to exist on the device in order to
  manage the device. If not, AOS functions like Agent installation, Telemetry
  collection and device configuration may fail. The only currently known
  workaround is to use local authentication.

Arista EOS
``````````
AOS supports AAA configuration on EOS devices.

.. important::
  For EOS, Apstra has observed cases in which AOS Device System Agent upgrade failed
  while copying files from the AOS server to the device with TACACS+ AAA configured.
  This commonly happens if TACACS+ is used to use a custom password prompt. To
  prevent this type of failure, the user should temporarily disable all TACACS+ AAA
  to where device authentication uses an admin-level username and password for any
  AOS Device System Agent operations including upgrade.
