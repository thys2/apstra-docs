=======
Devices
=======

Please see our :doc:`device guides <device_guide>` for more information on
working with devices in AOS.

.. toctree::
   :maxdepth: 2

   managed_devices
   telemetry
   device_agents
   agent_profiles
   packages
   os_images
   apstra_ztp
   device_profile
