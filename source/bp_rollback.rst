===========================
|history_icon| Time Voyager
===========================

.. |history_icon| image:: static/images/icons/history_icon.png

When you commit a Staged Blueprint, thereby deploying updates to the network,
you may find that the result is not what you expected. Or maybe you've committed
changes to a Blueprint by mistake and you want to undo those changes. Another
scenario may be that you've decided to return the network to the state it was in
several revisions ago. Depending on the level of complexity, manually staging
and committing changes to undo what you've done can be difficult and error-prone.
In these cases you'll want to use AOS Time Voyager (new in version 3.2)
to automatically restore previous revisions of a Blueprint.

.. sidebar:: Why not use AOS backup/restore to jump to a previous revision?

   Time Voyager maintains synchronized configuration between the controller and
   devices (as much as possible); AOS backup/restore does not. Effectively,
   the AOS backup/restore is an out-of-band change from a device configuration
   standpoint. If a backup is restored, you would need to perform a full config
   apply to make sure the device configuration reflects what you restored from the
   database backup. This would most likely be disruptive.

A Blueprint can be jumped back to any retained revision. The five most recent
Blueprint commits are retained. When you commit a sixth time, the first revision
is discarded, and the sixth revision becomes the fifth, the second revision
becomes the first, and so on as additional Blueprint changes are committed.
You can retain a particular revision indefinitely by *keeping* it. When you keep a
revision it is not included in the five revisions that cycle out. You can keep up
to twenty-five revisions, effectively having thirty Blueprint revisions to choose
from. Keep in mind that each revision requires storage space. If you decide that
you no longer want to keep a revision you can simply delete it.

When committing a Blueprint you can add a revision description to help identify
the changes made in that revision. These descriptions are displayed in the
revision history section of the Blueprint as long as that revision is retained.
If you don't add a description when you commit you can always add one later.
When jumping to a revision, this description helps you choose the correct one.
Currently, specific diffs between revisions are not displayed, so the
description is the only change information available for that revision.

When jumping to a revision, any previously staged changes that have not been
committed are discarded. If this is an issue, do not jump until you've
addressed the uncommitted changes.

.. sidebar:: Isn't Time Voyager just an UNDO function?

   When using Time Voyager you roll back to a previous AOS commit. This
   means that anything deleted on the last commit will be re-applied when rolling
   back. There can be many changes in-between revisions, both additions and removals,
   all of which would be included in the rollback. It is important to do a detailed
   review of changes before Commiting a Rollback.
   Therefore Time Voyager is better compared with a Revision Control System (for
   the whole network!) than an UNDO function.

Unsupported Time Voyager Scenarios

* After you've upgraded AOS, you cannot jump to a Blueprint with an older AOS
  version because the Blueprint revision history is discarded on upgrade.
  If you need to return to a previous AOS version, refer to
  :doc:`Restoring AOS Database <server_management>` that was taken prior
  to upgrading AOS, although this may cause issues from a device config standpoint.
* It's not supported when the Pristine config has changed between revisions.
* It's not supported when the device operating system (DOS) versions are
  different between revisions. You could downgrade the DOS version to the same
  version using the device manager, then jump to a previous revision.
* Devices that were allocated in a previous revision that are no longer
  available result in the build error *system ID does not exist*. (Conversely,
  *adding* a device and jumping to a previous revision without that
  device *will* be successful. The added device will be removed.)
* Resources that were assigned in a previous revision that have been
  reassigned cause the build error *resource already in use*. To resolve the
  build error, you must manually assign resources to each member in that group or
  reset the resource group overrides. (Jumping to a previous revision after a
  previously assigned global resource pool is modified *may* be successful, but it
  could cause an intent violation.)
* It's not supported if manual device config changes have been accepted.
* It's not supported in any other cases where the resulting device config
  state is different.

Listing Blueprint Revisions
===========================
From the AOS web interface, navigate to the **Time Voyager** view of the Blueprint.
From the **Revisions** tab, you'll see the list of retained Blueprint revisions.
The first revision in the list is the currently active one. Successive revisions
are ordered by date from most recent to oldest.

Keeping a Saved Blueprint Revision
==================================
#. From the **Time Voyager** view of the Blueprint, click the
   **Keep this revision** button corresponding to the revision to keep. (It looks
   like a floppy disk.)
#. Click **Save** to confirm and proceed. The button turns gray indicating that
   the revision has been saved indefinitely. It will not be deleted until you
   manually delete it.

Deleting a Kept Blueprint Revision
===================================
#. From the **Time Voyager** view of the Blueprint, click the **Delete** button
   corresponding to the revision to delete. You cannot delete a revision if there
   are five or fewer of them in the list.
#. Click **Delete** to confirm.

Jumping to a Previous Revision in a Blueprint
=============================================

.. note::
    When you rollback to a previous revision, any previously staged changes that
    have not been committed are discarded. If this is an issue, do not jump to a
    different revision until you've committed the uncommitted changes.

#. From the **Time Voyager** view of the Blueprint, click the **Jump to this
   revision** button corresponding to the revision to jump to. You'll see its
   dialog.
#. Any uncommitted changes in the staged area will be discarded.
   If this is an issue, close the dialog and address the uncommitted changes
   before proceeding. To proceed, click **Rollback**.
#. You can make additional changes to the Blueprint before committing. For
   example, if you've replaced a device, the device ID (serial number) will change,
   but the IP will not. You can create the Device Agent and update the serial
   number in your Blueprint before committing the revision change.
#. Click **Uncommitted**, then click the diff tabs to review the changes.
#. If you decide that you don't want to jump to this revision, click the
   **Revert** button to discard the changes.
#. To proceed, click the **Commit** button (top-right) to see the dialog for
   committing changes and creating a revision.
#. We recommend that you enter the optional revision description to identify the
   changes. Currently, specific diffs between revisions are not displayed, so
   the description is the only change information available for the revision.
#. Click **Commit** to commit your changes to the active Blueprint and create a
   revision. In some cased, you might also need to
   :ref:`reset_resource_group_overrides`.
#. If you click **Time Voyager** you'll see the revision as the current one.
