============================
Adding Access Layer Switches
============================

As of AOS version 3.3.0, Apstra includes the possibility to create and manage
access switches in data center networks.

You can follow our existing guidelines for most of the stages, there are some
more options and design considerations to take into account as described in
the following points.

.. important::

   Access switches have **limited support**. Apstra does not recommend using
   this feature in a production network.
   Please contact :doc:`Apstra Support <support>` to learn more about this
   feature availability.

Design
======

Logical Devices
---------------

A new role has been introduced for ports facing access switches, **access**
Role.

.. image:: static/images/access_switch/access_switch_ld.png

Configure this role on leaf switches facing an access switch, and configure
leaf and server ports in the access switch logical device.

To learn about how to create logical devices please refer to our logical
device documentation :doc:`logical devices <logical_devices>`.

Rack Types
----------

Once the logical devices with the required access ports have been created and
the interface map is defined, you can create a new rack and add the access
switches.

.. image:: static/images/access_switch/access_rack_type1.png

To configure the access switch links, click **Add link** as shown
in the above picture. Access switches can be single-homed only.

Name the link connection and select LAG Mode or No LAG and specify
the number of links.

.. image:: static/images/access_switch/access_rack_type2.png

To learn more about Rack Types please refer to our rack types
documentation :doc:`rack types <rack_types>`.

Templates
---------

To create a template with access switches please follow our templates
documentation :doc:`templates <templates>`.

Configlets
----------

Access switches configlet is not supported as of AOS version 3.3.0. Please
contact our support or sales team if you require configlets in access switches.

Blueprints
==========

Blueprints are mostly built in the same way as in a regular data center network
with no access switches. You can follow our documentation
:doc:`blueprint creation <bp_create>`.

As of AOS version 3.3.0 the following operations can be performed in the access
switches.

To learn more about AOS operations please visit our documentation
:doc:`Staged <staged>`.

Changing Link Speed
-------------------

As in the leaf switches towards the servers, AOS allows you to change the link
speed between an access switch and a leaf or a server.

#. From the blueprint, navigate to **Staged / Physical / Links**, then click
   the **Change link speeds** button.
#. Select the desired link speed.
#. Commit the change in the blueprint.

.. image:: static/images/access_switch/link_speed_change1.png

.. image:: static/images/access_switch/link_speed_change2.png

Adding New Link
---------------

You can add an access-server or leaf-access link similar to adding a link
between a leaf and a server.

#. From the blueprint, navigate to **Staged / Physical / Topology**.
#. Select the desired device.
#. Click **Add links** and select the configuration.
#. Commit the change in the blueprint.

You will be able to change the link design in the access switch
towards the leaf or the server.

.. image:: static/images/access_switch/add_link1.png

Select the desired ports as well as the LAG mode.

.. image:: static/images/access_switch/add_link2.png


Deleting Link
-------------

From the same view as before where you have selected the device:

#. Change the **Neighbors view** to **Links view**.
#. Click the **Delete** button (trash can).
#. Commit in the blueprint.

.. image:: static/images/access_switch/delete_link.png
