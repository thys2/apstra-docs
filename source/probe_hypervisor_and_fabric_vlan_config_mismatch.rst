================================================================
Hypervisor and Fabric VLAN Config Mismatch Probe (Virtual Infra)
================================================================

Purpose
   Calculate VLAN mismatch between AOS-configured virtual networks on leafs and
   VLANs needed by VMs running on the hypervisors attached to leafs. (Formerly
   known as Virtual Infra VLAN Match).

Source Processors
   Fabric configured VLAN configs (:doc:`generic graph collector <processor_generic_graph_collector>`)
      output stage: Fabric VLAN configs (number set) (generated from graph)

   Hypervisor expected VLAN configs (:doc:`generic graph collector <processor_generic_graph_collector>`)
      output stage: Hypervisor VLAN configs (number set)

Additional Processor(s)
   Hypervisor unique VLAN configs (:doc:`set count <processor_set_count>`)
      input stage: Hypervisor VLAN configs

      output stage: Hypervisor unique VLAN configs (number set)

   Differences between Hypervisor and Fabric (:doc:`set comparison <processor_set_comparison>`)
      input stages:
         Hypervisor unique VLAN configs

         Fabric VLAN configs

      output stages:
         Common in Fabric and Hypervisor (number set)

         Fabric Only (number set)

         Hypervisor Only (number set)

   Fabric missing VLAN configs accumulator (:doc:`accumulate <processor_accumulate>`)
      input stage: Hypervisor Only

      output stage: Hypervisor Only TimeSeries (number set time series)

   Hypervisor missing VLAN configs accumulator (:doc:`accumulate <processor_accumulate>`)
      input stage: Fabric Only

      output stage: Fabric Only TimeSeries (number set time series)

   Check for Fabric missing VLAN configs (:doc:`range <processor_range>`)
      input stage: Hypervisor Only TimeSeries

      output stage: Fabric missing VLAN configs anomaly (discrete state set)

   Check for Hypervisor missing VLAN configs (:doc:`range <processor_range>`)
      input stage: Fabric Only TimeSeries

      output stage: Hypervisor missing VLAN configs anomaly (discrete state set)

Example Usage
   **NSX-T Integration**

   If the VLAN on the AOS data center fabric and NSX-T transport nodes match, then
   the probe looks similar to the image below:

   .. image:: static/images/nsx/HV_Fabric_Mismatch.png

   The **Fabric VLAN Configs** stage shows the VLANs tagged towards NSX-T
   transport nodes on fabric ToR leafs as shown below:

   .. image:: static/images/nsx/HV_Fabric_Config.png

   The **Common in Fabric and Hypervisor** stage shows that VLANs in the NSX-T
   transport nodes and the AOS fabric match.

   .. image:: static/images/nsx/Common_NSX-T_Fabric.png

   If the VLAN defined on the NSX-T uplink profile is missing on the AOS fabric,
   then VLAN mismatch anomalies are raised.

   .. image:: static/images/nsx/Uplink_Profile_Missing_Vlan.png

   Some examples of mismatches include the following:

   * If the configured VLAN NSX-T transport node is missing in the AOS fabric.
   * If the configured VLAN NSX-T transport node is in the AOS fabric, but the
     end VMs or servers are not part of this virtual network or VLAN.
   * If a segment is created in NSX-T for either an overlay or VLAN-based
     transport zone. It could be that the configured VLAN spanning the logical
     switch/segment on the transport node is missing on the AOS fabric.
   * If L2 bridging for VMs in different overlay logical segments is broken
     because one VM exists in one logical switch/segment and the other VM exists
     in a separate uplink logical switch/segment.

   In some scenarios, AOS can remediate a VLAN mismatch anomaly. If so, the
   **Remediate Anomalies** button will be shown on the probe details page.
   Example scenarios include:

   * NSX-T transport nodes use an uplink profile to define transport VLAN over
     which overlay tunnel comes up. AOS fabric could be missing the rack-local VN
     for transport VLAN on hypervisors. AOS can provide
     one-click remediation by creating a new rack-local virtual network with the
     proper VLAN ID in the fabric.

   * An AOS rack-local virtual network is defined with VLAN ID Y, however, the
     connected virtual infra nodes (i.e hypervisors) do not have the VLAN ID in
     the logical segment/switch. AOS can provide one-click remediation by removing
     the endpoint from the affected VLAN ID.

   **vCenter Integration**

   If VLAN config between vCenter and the AOS fabric is mismatched,
   an anomaly is raised.

   Some scenarios where AOS can remediate a VLAN mismatch include the following
   cases:

   * If the vCenter Distributed Virtual Switch (vDS) port group does not have a
     corresponding AOS rack-local VN (VLAN) for VLAN ID X. With one-click
     remediation, AOS creates a new rack-local virtual network (VLAN) with the
     proper VLAN ID.

   * If endpoint X in an AOS rack-local VN with VLAN ID Y, does not have a
     corresponding dVS port group. With one-click remediation, AOS removes the
     endpoint from the affected VLAN ID.

   .. note::
       vCenter vDS must be used with VLAN specific ID allocation on the port group
       for L2 network segmentation at the hypervisor level.

       In AOS, a VLAN-based rack-local virtual network is extending each VLAN
       segment defined on the vDS, across servers within the same rack. For
       example, vDS port group VLAN 10 = AOS rack-local virtual network with
       VLAN 10.

   **Anomaly Remediation**

   To remediate anomalies, go to the details page of the
   **Hypervisor and Fabric Vlan config mismatch** probe, and click
   **Remediate Anomalies**. AOS stages the necessary remediation for the anomalies.

   .. image:: static/images/nsx/Remediate_Anomalies.png

   Review the staged configuration, add any necessary resources (such as IP subnet
   address, virtual gateway IP, etc.), then commit the configuration;
   the anomaly will be resolved.

   .. image:: static/images/nsx/Vlan_Mismatch_Remediation.png
