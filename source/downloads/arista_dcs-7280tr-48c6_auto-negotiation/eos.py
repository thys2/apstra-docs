# Copyright 2014-present, Apstra, Inc. All rights reserved.
#
# This source code is licensed under End User License Agreement found in the
# LICENSE file at http://apstra.com/eula

"""Switch OS-level port map generator for EOS devices"""

import json

from aos.rendering.port_map import port_map_os_plugin, port_map_model_plugin
from aos.rendering.port_map_util import max_connector_port_speed
from aos.rendering.port_map_util import is_10G, is_40G, is_25G, is_50G, is_100G
from aos.schema.schema_util import LogicalDeviceTransformBuilder

from generator_base import PortMapGeneratorBase, normalize_args

import Tac
PortSpeed = Tac.Type('Aos::Resource::PortSpeed')
PortSetting = Tac.Type('Aos::IPFabric::PortSetting')
PortSettingType = Tac.Type('Aos::IPFabric::PortSetting::SettingType')


def make_global_param(port_group=None, select=''):
    return {
        'port_group': port_group if port_group is not None else -1,
        'select': select,
    }


def make_setting_param(intf_speed):
    """Creates and returns EOS-specific dictionary represented as a string
       to be used as the value for Port::setting.

       Args:
        intf_speed   Speed for interface stanza (Aos::Resource::PortSpeed)
    """
    assert intf_speed.unit.upper() == 'G', \
        'Unsupported port speed unit %s' % intf_speed.unit

    speed = {
        0: '',
        1: '1000full',
        10: '10000full',
        25: '25gfull',
        40: '40gfull',
        50: '50gfull',
        100: '100gfull',
    }[intf_speed.speed]

    return json.dumps({
        'interface':  {'speed': speed},
        'global': make_global_param(),
    })

def max_hcl_panel_port_count(hcl_panel):
    """Get the max per port intf count based on hcl.
    """
    return max([1] + [transform.toCount for transform in
                      hcl_panel.supportedTransformations.keys()])

@port_map_os_plugin('EOS')
class EosPortMapGenerator(PortMapGeneratorBase):
    """Port map generator plugin for Arista EOS devices"""

    def parse_logical_device_map_transform(self, hcl, transform):
        """Determines and returns port breakout, interface speed, and port
           setting for Arista EOS devices
        """
        hcl_panel = hcl.layout[transform.physical.panelId]
        transformation = transform.transformation
        per_port_intf_count = max_hcl_panel_port_count(hcl_panel)
        if transformation:
            intf_speed = transformation.toSpeed
        else:
            intf_speed = max_connector_port_speed(hcl_panel.type)

        port_setting = PortSetting()
        port_setting.type = PortSettingType.eosForceSpeed
        port_setting.param = make_setting_param(intf_speed)

        return per_port_intf_count, intf_speed, port_setting

    def create_interface_name(self, port_index, intf_index, hcl, transform):
        """Returns interface name taking into consideration Arista EOS's interface
           naming rules, in particular the rule of not appending /1 to non-breakout
           ports.
        """
        hcl_panel = hcl.layout[transform.physical.panelId]
        max_port_speed = max_connector_port_speed(hcl_panel.type)
        supported_transformations = hcl_panel.supportedTransformations.keys()
        transformation = transform.transformation
        card_slot = ''
        if hcl_panel.cardSlotId > 0:
            card_slot = str(hcl_panel.cardSlotId) + '/'
        if transformation:
            if is_10G(max_port_speed):
                interface_name = 'Ethernet%s%d' % (card_slot, port_index)
                position = (hcl_panel.cardSlotId, port_index, 1)
            elif is_10G(transformation.toSpeed):
                # For 1-to-1 25G sfp28 to 10G transformation, intf names do not
                # have '/1' suffix.
                if is_25G(transformation.fromSpeed) and transformation.toCount == 1:
                    interface_name = 'Ethernet%s%d' % (card_slot, port_index)
                else:
                    interface_name = 'Ethernet%s%d/%d' % (card_slot, port_index,
                                                          intf_index)
                position = (hcl_panel.cardSlotId, port_index, intf_index)
            elif is_25G(transformation.toSpeed):
                interface_name = 'Ethernet%s%d/%d' % (card_slot, port_index,
                                                      intf_index)
                position = (hcl_panel.cardSlotId, port_index, intf_index)
            elif is_40G(transformation.toSpeed):
                # 100G -> 3x40G: /1, /5, /9, i.e. 4n-3, where n = 1, 2, 3
                # are active, rest are inactive.
                # 100G -> 1x40G: /1 is active and rest are inactive
                interface_name = 'Ethernet%s%d/%d' % (card_slot, port_index,
                                                      intf_index)
                position = (hcl_panel.cardSlotId, port_index, intf_index)
            elif is_50G(transformation.toSpeed):
                # 100G -> 50G x2 /1, /3. /2 and /4 would be inactive
                interface_name = 'Ethernet%s%d/%d' % (card_slot, port_index,
                                                      intf_index)
                position = (hcl_panel.cardSlotId, port_index, intf_index)
            else:
                raise Exception('Unsupported transformation')
        else:
            for transform in supported_transformations:
                if ((is_40G(max_port_speed) or is_100G(max_port_speed)) and
                        max_hcl_panel_port_count(hcl_panel) > 1):
                    interface_name = 'Ethernet%s%d/%d' % (card_slot, port_index,
                                                          intf_index)
                    position = (hcl_panel.cardSlotId, port_index, intf_index)
                    break
            else:
                interface_name = 'Ethernet%s%d' % (card_slot, port_index)
                position = (hcl_panel.cardSlotId, port_index, 1)
        return position, interface_name

    def get_port_setting(self, position, port_setting, hcl_panel, transform):
        """Filters out unnecessary port settings based on Arista EOS rules"""
        if not hcl_panel.supportedTransformations:
            return PortSetting()
        transformation = None
        if transform:
            transformation = transform.transformation

        inactive_setting = PortSetting()
        inactive_setting.type = PortSettingType.inactive
        inactive_setting.param = make_setting_param(PortSpeed(0, 'G'))

        default_speed = max_connector_port_speed(hcl_panel.type)
        _, _, intf_index = position
        if transformation and transformation.toCount > 1:
            if is_100G(default_speed):
                if is_50G(transformation.toSpeed):
                    # If 100G -> 2x50G, then port setting is needed
                    # on /1 and /3. /2 and /4 will be inactive
                    if intf_index not in [1, 3]:
                        return inactive_setting
                elif is_40G(transformation.toSpeed):
                    # If 100G -> 1x40G or 3x40G, then port setting is needed
                    # on /1, for 1x40 and /1, /5, /9 for 3x40. Rest will be
                    # inactive
                    if intf_index not in [1, 5, 9]:
                        return inactive_setting
        else:
            # If no tranformation is provided then
            # for 40/100G ports that support tranformation,
            # render all 4 interfaces with only /1 having
            # port setting. The rest will be marked as 'inactive'
            # and configured with default config
            if is_40G(default_speed) or is_100G(default_speed):
                if hcl_panel.supportedTransformations.keys():
                    if intf_index != 1:
                        return inactive_setting

        return port_setting

    def add_builder_transformation(self, builder, panel):
        """Add transform to break 'unused' ports to
           10G interfaces to adhere to EOS default speed / breakout conventions
        """
        for trans in panel.supportedTransformations:
            # TODO(michael): Support other speed transformations for "unused" ports
            if is_40G(trans.fromSpeed) and is_10G(trans.toSpeed):
                builder.add_transformation(trans.fromCount, trans.fromSpeed.speed,
                                           trans.fromSpeed.unit, trans.toCount,
                                           trans.toSpeed.speed, trans.toSpeed.unit)


    def transform_unused_ports(self, hcl, panel_id, in_use_interfaces):
        """Creates logical to physical transform that breaks out 'unused' ports.
        """
        panel = hcl.layout[panel_id]
        port_count = panel.panelLayout.rowCount * panel.panelLayout.columnCount
        start_index = panel.portIndexing.startIndex
        end_index = start_index + port_count - 1

        builder = LogicalDeviceTransformBuilder(
            phys_panel_id=panel_id,
            phys_port_start=start_index, phys_port_end=end_index,
            logical_panel_id=0, logical_port_start=0, logical_port_end=0)

        self.add_builder_transformation(builder, panel)

        transform = Tac.newInstance('Aos::IPFabric::Transform', 'transform_unused')
        builder.entity_loads(transform)
        return transform

@port_map_model_plugin('EOS', 'DCS-7050QX-32S')
class Arista7050QX32SPortMapGenerator(EosPortMapGenerator):
    """Port map generator plugin for Arista 7050QX-32S taking into account
       hardware port groups Eth1-4 and Eth5/1-4
    """
    def generate_port_map(self, hcl, logical_device, logical_device_map):
        """Specific port map generator for Arista 7050QX-32S"""
        def physical_ports_mapped(start_port, end_port):
            """Determines if given physical ports are mapped by logical device map

               Args:
                start_port, end_port    port indices start_port <= end_port
                                        whose mappings is determined

               Returns:
                True if and only if at least one of the ports is mapped
            """
            for transform in logical_device_map.transform.itervalues():
                start = transform.physical.portRangeStart
                end = transform.physical.portRangeEnd
                if (start >= start_port and start <= end_port) or \
                        (start_port >= start and start_port <= end):
                    return True
            return False

        hcl, logical_device, logical_device_map = normalize_args(
            hcl, logical_device, logical_device_map)

        # Disable SFP+ ports if rendering from HCL only
        sfp_ports_active = logical_device_map and physical_ports_mapped(1, 4)
        if sfp_ports_active and physical_ports_mapped(5, 5):
            raise ValueError('Cannot use Eth1-4 concurrently with Eth5/1-4 on ' \
                             'Arista DCS-7050QX-32S')

        port_map = super(Arista7050QX32SPortMapGenerator, self).generate_port_map(
            hcl, logical_device, logical_device_map)

        if sfp_ports_active:
            port_with_global_setting = 'Ethernet1'
            ports_to_remove = ['Ethernet5/%d' % i for i in xrange(1, 4+1)]
        else:
            port_with_global_setting = 'Ethernet5/1'
            ports_to_remove = ['Ethernet%d' % i for i in xrange(1, 4+1)]

        port_map['ports'] = [
            port for port in port_map['ports']
            if port['display_name'] not in ports_to_remove
        ]

        for port in port_map['ports']:
            if port['display_name'] == port_with_global_setting:
                param = json.loads(port['setting']['param'])
                param['global'] = make_global_param(
                    port_group=1,
                    select='Et1-4' if sfp_ports_active else 'Et5/1-4',
                )
                port['setting']['param'] = json.dumps(param)
                break

        return port_map

@port_map_model_plugin('EOS', 'DCS-7280QRA-C72')
class Arista7280QR72CPortMapGenerator(EosPortMapGenerator):
    """Port map generator plugin for Arista 7280QR-C72. Ports alternate to qsfp28
       ports, if operating in 100G mode, must not be used.
    """
    def generate_port_map(self, hcl, logical_device, logical_device_map):
        """Specific port map generator for Arista 7280QR-C72"""
        def qsfp28_ports_mapped_with_100g_speed(port_index):
            """Determines if given qsfp28 port is in 100G mode.

               Args:
                   port_index: physical index of the port

               Returns:
                True if the port is in 100G mode, i.e. has no transformations.
            """
            for transform in logical_device_map.transform.itervalues():
                start = transform.physical.portRangeStart
                end = transform.physical.portRangeEnd
                if start <= port_index <= end:
                    if not transform.transformation:
                        return True
            return False

        hcl, logical_device, logical_device_map = normalize_args(
            hcl, logical_device, logical_device_map)

        qsfp28_port_indexes = range(11, 26, 2) + range(47, 62, 2)
        if logical_device_map:
            for index in qsfp28_port_indexes:
                if qsfp28_ports_mapped_with_100g_speed(index):
                    if qsfp28_ports_mapped_with_100g_speed(index+1):
                        raise ValueError('Cannot use alternate ports when using ' \
                            'qsfp28 ports in 100G mode')

        return super(Arista7280QR72CPortMapGenerator, self).generate_port_map(
            hcl, logical_device, logical_device_map)

    def create_interface_name(self, port_index, intf_index, hcl, transform):
        """Returns interface name taking into consideration Arista EOS's interface
           naming rules, in particular for this model, appending /1 to 40G/100G
           ports regardless of breakout capabilities.
        """
        hcl_panel = hcl.layout[transform.physical.panelId]
        card_slot = ''
        interface_name = 'Ethernet%s%d/%d' % (card_slot, port_index,
                                              intf_index)
        position = (hcl_panel.cardSlotId, port_index, intf_index)
        return position, interface_name


@port_map_model_plugin('EOS', 'DCS-7280TR-48C6')
class Arista7280TR48C6PortMapGenerator(EosPortMapGenerator):
    """Port map generator for Arista 7280TR-48C6 - first 48 10G ports are 10GBase-T.
       Do not add any port speed command for these ports so that they operate in
       speed auto-negotiate mode.
    """
    def generate_port_map(self, hcl, logical_device, logical_device_map):
        port_map = super(Arista7280TR48C6PortMapGenerator, self).generate_port_map(
            hcl, logical_device, logical_device_map)

        autoneg_ports = set('Ethernet%d' % i for i in xrange(1, 48+1))
        for port in port_map['ports']:
            if port['display_name'] in autoneg_ports:
                param = json.loads(port['setting']['param'])
                param['interface']['speed'] = ''
                port['setting']['param'] = json.dumps(param)

        return port_map
