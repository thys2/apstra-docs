=======
AOS CLI
=======
Apstra provides a command line interface tool (AOS CLI),
which augments functionality provided by the AOS Web interface.

.. important::

   AOS CLI is considered **experimental** and use of it must be under strict
   supervision of :doc:`Apstra Support <support>`.
   Do **not** use AOS CLI in production networks.

Installing AOS CLI
==================
AOS CLI is distributed as a Docker container.

0. Make sure you have Docker 17.12.0-ce installed on the system you intend to run
   AOS CLI on. The user can also load the Docker container on their AOS Sevrer VM.
1. Download the AOS-CLI Docker container from the Apstra Client Portal, https://portal.apstra.com/downloads/
2. Load the provided Docker image into Docker:

   .. code-block:: bash

       $ docker image load -i /path/to/aoscli/docker/image/tarball (example: docker image load -i aoscli-0.1.412.tar.gz)
       d235d91b668d: Loading layer [==============================>]  114.7kB/114.7kB
       e71e5f1262f5: Loading layer [==============================>]  34.33MB/34.33MB
       Loaded image: aoscli:<version>

Accessing AOS CLI
=================
#. Start AOS CLI with the following command:

   .. code-block:: bash

       docker run --rm -ti -v $HOME/aosclitmp:/mytmp aoscli:<version> -s <AOS server IP/hostname>
       EXAMPLE: docker run -ti aoscli:0.1.412  -s 172.20.23.3

#. AOS CLI comes with a built-in feature that auto-completes commands.
   Use the TAB key to learn about this tool and its functionality.

   .. image:: static/images/aos_cli/aos_cli_autocompletion.png

.. _cli_cabling_discovery:

Discovering Existing Networks
==============================
To discover your existing (a.k.a brownfield) leaf-spine or layer2 network,
and monitor it with AOS built-in anomalies and
:ref:`IBA probes <iba_probes_description>` follow the steps below:

#. Get devices in your network under read-only management in AOS.
   To ensure that AOS never pushes config to these devices you must
   choose the *telemetry only* operation mode.
   See :doc:`device_agents` for more information.

#. Run the command ``blueprint create-from-network`` to discover the network and
   create a Blueprint representing the intent behind this network. You must
   specify a seed spine by its device key (select from the dropdown in aos-cli).

   .. code-block:: bash

       aos> blueprint create-from-network --spine HSH17085002 --label bravo
       Discovering l3clos network among [u'172.20.1.7', u'172.20.1.6', u'172.20.1.8']
       Waiting for telemetry on 172.20.111.7
       Waiting for telemetry on 172.20.111.6
       Waiting for telemetry on 172.20.111.8
       Successfully created blueprint with id {u'id': u'2483e803-9473-76dcce53262f'}
       aos>

#. Review the created Blueprint in the :doc:`AOS Web interface <aos_ui>`,
   then commit the Blueprint. AOS automatically starts monitoring the network
   and reports the same set of built-in anomalies, as it does for a network
   deployed by AOS (a.k.a greenfield).

#. You can also create additional IBA probes to monitor other aspects of
   your network.

Rediscovery
-----------
After initial discovery, the generated blueprint is based on a snapshot of the state
of the network at the time of discovery. After this step, if the network changes,
you can rediscover the network to update the existing blueprint in place. Use command
``blueprint update-from-network`` to invoke the rediscovery, which essentially
repeats the same process as initial discovery except for updating an existing
blueprint as opposed to creating a new blueprint.

Layer2 Network Design
---------------------
To add support for layer2 networks in AOS,
you need to install a new reference design package.

.. code-block:: bash

    aos> ref-design list
    Design
    ----------------
    two_stage_l3clos

    aos> package upload --file aos_ref_design_layer2-0.1.0-py2-none-any.whl
    Successfully uploaded package aos_ref_design_layer2-0.1.0-py2-none-any.whl
    aos>
    aos> ref-design add-restarts-aos-server --package aos-ref-design-layer2
    Successfully installed reference design. Restarting AOS server (reinvoke the
    aos-cli session after AOS is back up)...

    aos> ref-design list
    Design
    ----------------
    two_stage_l3clos
    layer2
    aos>

Support Matrix
--------------
This section provides high-level information on what's supported to give you a
general idea. Each customer network is unique in certain aspects. Please work with
:doc:`Apstra Support <support>` to try the network discovery feature.

Supported AOS Server versions
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Network discovery feature is only tested on AOS version 2.3.1. If you have a
newer AOS version, contact Apstra Support to evaluate your case.

Supported spine leaf network features
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Following table lists the supported network features in spine leaf (a.k.a. L3 Clos)
networks

.. csv-table:: Network discovery supported spine leaf features
  :header-rows: 1
  :class: nowrap
  :file: network_discovery_l3clos_supported_features.csv


Supported layer2 network features
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Following table lists the supported network features in layer2 networks, which is a
new reference design. See `Layer2 Network Design`_ for more details.

.. csv-table:: Network discovery supported layer2 features
  :header-rows: 1
  :class: nowrap
  :file: network_discovery_layer2_supported_features.csv

Sample Commands
===============
This section describes a few sample commands present in AOS CLI.
Use auto-completion present in AOS CLI to explore the full list
of available commands.

Cabling override
----------------
As mentioned in the :doc:`Quick start guide <quick_start>`,
when it comes to cabling, you have two options:

- Let AOS generate the cabling, then you cable the devices accordingly.
- Have AOS use an already properly cabled network.

This section describes how you can do the latter by overriding AOS-generated
cabling (in a Blueprint) with the actual cabling.

The high-level flow is comprised of the following steps.
You'd generally follow these same steps as in the Quick start guide.

0. Make sure Logical Devices and Interface Maps are created to accurately reflect
   the cables you have in reality. Pay special attention to transceivers and port
   breakouts and ensure that the speed of all interfaces in the Interface Map
   match the speed of the actual transceiver on the ports.

1. After you complete the Blueprint build and devices are assigned, you commit the
   Blueprint (at this time, deploy_mode on all nodes is set to 'ready').
   AOS pushes discovery2 config by issuing needed port breakout commands.

2. Wait for config push to complete by monitoring the AOS Blueprint dashboard in
   the Web interface. If there are config deploy failures, fix the issue(s)
   before proceeding with the remaining steps. Depending on the failure,
   you may have to update your Logical Device(s) and/or Interface Map(s).

3. If you don't have any cabling anomalies, you are done and don't need cabling
   override. Assuming you have cabling anomalies, continue to the next step.

4. Run :ref:`cabling discover <cli_cabling_discover_command>` to save the actual
   cabling into a file. It's important to invoke the cabling discovery only
   *after* AOS has successfully pushed discovery2 config.

5. Examine the output file to ensure that the cabling is complete and as expected.
   You can edit this file manually.

6. Run :ref:`blueprint set-cabling <cli_set_cabling_command>` with that file to
   override the cabling in the Blueprint from this file. Optionally, you can use
   :ref:`blueprint show-cabling <cli_blueprint_show_cabling_command>` to verify
   cabling overrides.

7. In the AOS Web interface, commit the Blueprint to make cabling overrides
   effective. Note that Web interface shows that the Blueprint has uncommitted
   changes, but the actual changes are not viewable in the Web interface.

8. Go back to step #2.

Things to remember
~~~~~~~~~~~~~~~~~~
Keep in mind the following things while dealing with cabling to make sure you
don't run into any issues.

- Interface names used in cabling override must be part of the Interface Map.
  For example, if you use interface 'ethernet1/3/1', but the Interface Map does
  not have it (likely because 'ethernet1/3' is not broken out), AOS would fail.
- On a given device, interface names must be unique. For example, using
  'ethernet1/1' for more than one cable on a given device would result in error.
- Speed of the specified interface should match the speed of the link in the
  Blueprint.
- Role of the specified interface should match the role of the peer. For example,
  if interface 'ethernet1/2' has roles 'leaf, external_router' and you specify it
  on interface facing spine, AOS would generate error. In this case, you may
  have to update the Logical Device used.

Common issues
~~~~~~~~~~~~~
**Duplicate hostnames**

Without unique hostnames, discovered cables cannot be fully qualified
unambiguously. Make sure you are running discovery after all devices are
assigned to a Blueprint, in which case duplicate hostnames are unlikely to be
present.

   .. code-block:: bash

       aos> cabling discover
       Unexpected error: Traceback (most recent call last):
         File "aos_cli/command.py", line 378, in run_cmd
           parse_result.cmd.run(parse_result.values, context)
         File "aos_cli/cabling_cmds.py", line 367, in run
           bool(values.get('include-mgmt', False))
         File "aos_cli/cabling_cmds.py", line 311, in _fetch_cables
           'Cabling cannot be discovered with duplicate hostnames')
         File "aos_cli/util.py", line 67, in get_hostname_to_device_map
           err_prefix, json.dumps(host_overlaps, indent=2)))
       ValueError: Cabling cannot be discovered with duplicate hostnames. Duplicate hostnames:
       {
         "localhost": [
           "52540082D91C",
           "52540024FF43",
           "525400FED346"
         ]
       }

**set-cabling fails with no link available**

In the example below, the cable
:code:`('leaf-1-1', 'Ethernet1/7/3', 'server2', 'eth2')`
is not found in the Blueprint, because the node :code:`leaf-1-1` does not have
any peer whose hostname is :code:`server2`. You can alter your cabling file
manually or simply remove that line or understand the root cause for why
hostnames are different. Perhaps you invoked
:ref:`cabling discover <cli_cabling_discover_command>` before all
devices are assigned to Blueprint and AOS pushed discovery2 config.

.. code-block:: bash

    aos> blueprint set-cabling --blueprint rack-based-blueprint-731ae5f0 --file /mytmp/cables.csv
    Unexpected error: Traceback (most recent call last):
      File "aos_cli/command.py", line 378, in run_cmd
        parse_result.cmd.run(parse_result.values, context)
      File "aos_cli/cabling_cmds.py", line 171, in run
        self._process_line(bc, used_link_ids, row, reader, intf_updates)
      File "aos_cli/cabling_cmds.py", line 148, in _process_line
        '\n'.join(str(c) for c in bc.cables)))
    ValueError: No link in blueprint for ('leaf-1-1', 'Ethernet1/7/3', 'server2', 'eth2') at line 4
    Available Links:
    BlueprintCable(hostname=u'ext_router_c9b1eb5c', intf=u'eth1', ip=u'172.16.0.21/31', peer_hostname=u'leaf-2', peer_intf=u'swp1', peer_ip=u'172.16.0.20/31')
    BlueprintCable(hostname=u'leaf-1-1', intf=u'Ethernet1/1', ip=None, peer_hostname=u'leaf-1-2', peer_intf=u'Ethernet1/49', peer_ip=None)
    BlueprintCable(hostname=u'leaf-1-1', intf=u'Ethernet1/7/1', ip=u'172.16.0.7/31', peer_hostname=u'spine-1', peer_intf=u'swp3', peer_ip=u'172.16.0.6/31')
    BlueprintCable(hostname=u'leaf-1-1', intf=u'Ethernet1/7/2', ip=u'172.16.0.13/31', peer_hostname=u'spine-2', peer_intf=u'swp3', peer_ip=u'172.16.0.12/31')
    BlueprintCable(hostname=u'leaf-1-1', intf=u'Ethernet1/7/3', ip=None, peer_hostname=u'racktype1-server1', peer_intf=None, peer_ip=None)
    BlueprintCable(hostname=u'leaf-1-1', intf=u'Ethernet1/7/4', ip=None, peer_hostname=u'racktype1-server2', peer_intf=None, peer_ip=None)
    BlueprintCable(hostname=u'leaf-1-2', intf=u'Ethernet1/1', ip=u'172.16.0.9/31', peer_hostname=u'spine-1', peer_intf=u'swp2', peer_ip=u'172.16.0.8/31')
    BlueprintCable(hostname=u'leaf-1-2', intf=u'Ethernet1/2', ip=u'172.16.0.15/31', peer_hostname=u'spine-2', peer_intf=u'swp2', peer_ip=u'172.16.0.14/31')
    BlueprintCable(hostname=u'leaf-1-2', intf=u'Ethernet1/3', ip=None, peer_hostname=u'racktype1-server1', peer_intf=None, peer_ip=None)
    BlueprintCable(hostname=u'leaf-1-2', intf=u'Ethernet1/4', ip=None, peer_hostname=u'racktype1-server2', peer_intf=None, peer_ip=None)
    BlueprintCable(hostname=u'leaf-2', intf=u'swp2', ip=None, peer_hostname=u'racktype2-server1', peer_intf=None, peer_ip=None)
    BlueprintCable(hostname=u'leaf-2', intf=u'swp3', ip=u'172.16.0.11/31', peer_hostname=u'spine-1', peer_intf=u'swp1', peer_ip=u'172.16.0.10/31')
    BlueprintCable(hostname=u'leaf-2', intf=u'swp4', ip=u'172.16.0.17/31', peer_hostname=u'spine-2', peer_intf=u'swp1', peer_ip=u'172.16.0.16/31')

    aos>

Cabling related AOS CLI commands are described below:

.. _cli_set_cabling_command:

Set Cabling Command
~~~~~~~~~~~~~~~~~~~
To update cabling in the Blueprint based on cables specified in a CSV file,
run the command ``blueprint set-cabling``. Each line in this file represents
one cable in the format.

.. code-block:: bash

    <hostname1>, <interface1>, <hostname2>, <interface2>

This cabling file is typically a result of running the
command ``cabling discover``. However you can also manually create such a file,
or edit the file generated from the command ``cabling discover``.
``blueprint set-cabling`` command supports updating only a
subset of cables in the Blueprint.

.. code-block:: bash

    aos> blueprint set-cabling --blueprint rack-based-blueprint-731ae5f0 --file /mytmp/cables.csv
    Updating interfaces {u'4734ed56-0b0c-49db-9df7-56b50926b9c5': {'id': u'4734ed56-0b0c-49db-9df7-56b50926b9c5', 'if_name': 'Ethernet1/3'}}
    Successfully updated Blueprint with cabling data
    aos>

    admin@aos ~ $ cat ~/aosclitmp/cables.csv
    leaf-1-1,Ethernet1/3,leaf-1-2,Ethernet1/49
    admin@aos ~ $

.. _cli_cabling_discover_command:

Cabling discover command
------------------------
To discover cables from LLDP telemetry data of managed devices,
run the command ``cabling discover``. Specify the --file argument to write the
discovered cables to a file, which can then be used as input to
'blueprint set-cabling' command.

.. code-block:: bash

    aos> cabling discover
    WARNING: Not all interfaces are up. Discovered cabling may be incomplete. Following interfaces are shut
    {
      "leaf-1-2": {
        "Ethernet1/9": "down"
      },
      "leaf-1-1": {
        "Ethernet1/8": "down",
        "Ethernet1/9": "down"
      },
      "server3": {
        "eth1": "down"
      }
    }
    WARNING: Interface swp1 on device leaf-2 has unknown neighbor localhost
    Hostname    Interface      Peer hostname    Peer interface
    ----------  -------------  ---------------  ----------------
    leaf-1-1    Ethernet1/1    leaf-1-2         Ethernet1/49
    leaf-1-1    Ethernet1/7/1  spine-1          swp3
    leaf-1-1    Ethernet1/7/2  spine-2          swp3
    leaf-1-1    Ethernet1/7/3  server2          eth2
    leaf-1-1    Ethernet1/7/4  server1          eth1
    leaf-1-2    Ethernet1/1    spine-1          swp2
    leaf-1-2    Ethernet1/2    spine-2          swp2
    leaf-1-2    Ethernet1/3    server2          eth1
    leaf-1-2    Ethernet1/4    server1          eth2
    leaf-2      swp3           spine-1          swp1
    leaf-2      swp4           spine-2          swp1
    aos> cabling discover --file /mytmp/cables.csv
    WARNING: Not all interfaces are up. Discovered cabling may be incomplete. Following interfaces are shut
    {
      "leaf-1-2": {
        "Ethernet1/9": "down"
      },
      "leaf-1-1": {
        "Ethernet1/8": "down",
        "Ethernet1/9": "down"
      },
      "server3": {
        "eth1": "down"
      }
    }
    WARNING: Interface swp1 on device leaf-2 has unknown neighbor localhost
    Successfully wrote cabling info into file /mytmp/cables.csv
    aos>

.. _cli_blueprint_show_cabling_command:

Blueprint show cabling command
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
``blueprint show-cabling`` command displays/stores the list of cables present in the
Blueprint. This command is useful in a few scenarios.

- Verify that the cables are as you expect after ``blueprint set-cabling`` command
- Change just a few cables relative to what is in the Blueprint. To do this,
  run this command with --file option, edit the file and
  run ``blueprint set-cabling`` command

.. code-block:: bash

    aos> blueprint show-cabling --blueprint rack-based-blueprint-731ae5f0
    Hostname             Interface      Peer hostname      Peer interface
    -------------------  -------------  -----------------  ----------------
    ext_router_c9b1eb5c  eth1           leaf-2             swp1
    leaf-1-1             Ethernet1/3    leaf-1-2           Ethernet1/49
    leaf-1-1             Ethernet1/7/1  spine-1            swp3
    leaf-1-1             Ethernet1/7/2  spine-2            swp3
    leaf-1-1             Ethernet1/7/3  racktype1-server1
    leaf-1-1             Ethernet1/7/4  racktype1-server2
    leaf-1-2             Ethernet1/1    spine-1            swp2
    leaf-1-2             Ethernet1/2    spine-2            swp2
    leaf-1-2             Ethernet1/3    racktype1-server1
    leaf-1-2             Ethernet1/4    racktype1-server2
    leaf-2               swp2           racktype2-server1
    leaf-2               swp3           spine-1            swp1
    leaf-2               swp4           spine-2            swp1
    aos>


Blueprint clear cabling overrides
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
``blueprint clear-cabling-overrides`` command simply clears all cabling overrides
from the Blueprint. After this command, Blueprint would have AOS generated
cabling. This is useful to recover from errors by starting over from a clean state.

.. code-block:: bash

    aos> blueprint clear-cabling-overrides --blueprint rack-based-blueprint-731ae5f0
    Blueprint has following cabling overrides that will be cleared:
                                        Hostname    Interface          Peer hostname    Peer interface
    -------------------  -------------  ----------  -----------------  ---------------  ----------------
    ext_router_c9b1eb5c  eth1                       leaf-2             swp1
    leaf-1-1             Ethernet1/3                leaf-1-2           Ethernet1/49
    leaf-1-1             Ethernet1/7/1              spine-1            swp3
    leaf-1-1             Ethernet1/7/2              spine-2            swp3
    leaf-1-1             Ethernet1/7/3              racktype1-server1
    leaf-1-1             Ethernet1/7/4              racktype1-server2
    leaf-1-2             Ethernet1/1                spine-1            swp2
    leaf-1-2             Ethernet1/2                spine-2            swp2
    leaf-1-2             Ethernet1/3                racktype1-server1
    leaf-1-2             Ethernet1/4                racktype1-server2
    leaf-2               swp2                       racktype2-server1
    leaf-2               swp3                       spine-1            swp1
    leaf-2               swp4                       spine-2            swp1
    Successfully cleared matching cabling overrides from blueprint


Probes
------
AOS CLI provides management of :ref:`IBA probes <iba_probes_description>` and
includes many built-in probe templates that are parameterized probe definitions.

Probe definition is a JSON file containing the probe API payload described in
:ref:`Creating a Probe <iba_probes_creation>` section. The JSON file is processed
as a JINJA template thus allowing for parameterization. AOS CLI reads the variables
used in the probe template and offer them as CLI options for ``probe create`` command


.. code-block:: bash

    aos> probe create --blueprint <blueprint_id> --file <probe_json> <additional
    options if any>

To delete existing probe:

.. code-block:: bash

    aos> probe delete --blueprint <BP> [--id <probe ID>|--label <probe label regex>]


The following sections describe some of the probes that are built-in to AOS CLI.
Use the ``probe create`` command to see the full list.

Each section has a table listing the use cases and probe filenames. To create any
predefined probe use:

.. code-block:: bash

    aos> probe create --blueprint bp1 --file /usr/local/lib/python2.7/site-packages/aos_cli/resources/probes/interface_bandwidth_anomalies.j2 --percent_threshold 80

You can also give URI to any external probe definition that is outside the AOS CLI.
The ones built-in to the AOS CLI are selectable from the auto-completion dropdown.

Capacity Monitoring
~~~~~~~~~~~~~~~~~~~
.. list-table::
    :header-rows: 1
    :class: nowrap

    * - Use case
      - Probe filename(s)
    * - Monitor fabric link usage and raise anomalies if exceeding a threshold
      - interface_bandwidth_anomalies.j2
    * - Monitor ARP table, routing table usage and raise anomalies if exceeding a
        threshold
      - arp_usage_anomalies.j2
        unicast_route_usage_anomalies.j2
        multicast_route_usage_anomalies.j2

General Health
~~~~~~~~~~~~~~
.. list-table::
    :header-rows: 1
    :class: nowrap

    * - Use case
      - Probe filename(s)
    * - Monitor packet discards and raise anomalies on sustained discard rate
      - pkt_discard_anomalies.j2
    * - Monitor packet drops and raise anomalies if drops seen in both unicast and
        multicast packets
      - unicast_multicast_pkt_drop_anomalies.json
    * - Monitor MLAG domain config sanity and raise anomalies if inconsistent
      - mlag_domain_config_sanity_anomalies.json

Compliance and Security
~~~~~~~~~~~~~~~~~~~~~~~
.. list-table::
    :header-rows: 1
    :class: nowrap

    * - Use case
      - Probe filename(s)
    * - Ensure devices are running latest OS versions
      - os_version_anomalies.json

Packages
--------
AOS is extensible in terms of adding support for Network Operating Systems, adding
new telemetry collectors and so on. The extensions are sometimes referred to as
plugins, are distributed in `Python wheel packages`_, typically files with
``*.whl`` extension. One package can have one or more collectors for one of
more OS platforms.

To list packages that are currently available on AOS server, run the command
``package list``.

.. code-block:: bash

    aos> package list
    Name                     Version
    -----------------------  ---------
    aos-device-drivers       0.1.2
    aoscollectorlib          0.2.0
    aosstdcollectors-eos     0.1.0
    apstra-devicedriver-eos  0.1.0

To add a new package:

.. code-block:: bash

    aos> package upload --file /path/to/collector-plugin-0.1.0-py2-none-any.whl
    Successfully uploaded package /path/to/collector-plugin-0.1.0-py2-none-any.whl

To remove package:

.. code-block:: bash

    aos> package delete --name <package name>

.. _aos_cli_system_agents:

System Agents
-------------
System agents (aka proxy agents or offbox agents) are responsible for managing
devices under AOS.

to list system agents (the output is truncated), run the command
``system-agents list``.

.. code-block:: bash

    aos> system-agents list
    ID  Management IP      Packages              Serial Number  Connection State  Status Message
    --- -------------- ... --------------------  -------------  ----------------  --------------
    xxx 172.20.77.16       aosstdcollectors-eos  525400CDF839   connected         Connected to d

To create a system agent:

.. code-block:: bash

    aos> system-agents add --ip 172.20.77.16 --username admin --password secret \
    --platform eos \
    --packages apstra-devicedriver-eos,aosstdcollectors-eos,aoscollectorlib
    Successfully created agent for 172.20.77.16 with id
    {u'id': u'11629391-7223-4edd-8706-8a7a80e2f09c'}

to delete a system agent:

.. code-block:: bash

    aos> system-agents delete --id c6009e26-a434-4235-8832-3d23c29d7d32
    Successfully deleted agent c6009e26-a434-4235-8832-3d23c29d7d32

Telemetry Services
------------------

Telemetry services refer to collectors that are either built-in to AOS or custom ones
included in packages. After you upload packages to AOS, create system agents,
add telemetry service registry entries, you can enable/disable services on individual
system(s).

To populate telemetry service registry:

.. code-block:: bash

    aos> service-registry import-from --file http://<server_path>/json-schemas.tar.gz
    Successfully imported service registry entry for interface_details
    Successfully imported service registry entry for vlan
    Successfully imported service registry entry for pim_neighbor_count
    Successfully imported service registry entry for interface_iba
    Successfully imported service registry entry for stp
    Successfully imported service registry entry for vrf
    Successfully imported service registry entry for route_count
    Successfully imported service registry entry for vtep_counters
    Successfully imported service registry entry for sfp
    Successfully imported service registry entry for multicast_groups
    Successfully imported service registry entry for multicast_info
    Successfully imported service registry entry for bgp_vrf
    Successfully imported service registry entry for anycast_rp
    Successfully imported service registry entry for traceroute
    Successfully imported service registry entry for vxlan_info
    Successfully imported service registry entry for lldp_details
    Successfully imported service registry entry for mlag_domain
    Successfully imported service registry entry for ping
    Successfully imported service registry entry for pim_rp
    Successfully imported service registry entry for resource_usage
    Successfully imported service registry entry for interface_buffer
    Successfully imported service registry entry for acl_stats
    Successfully imported service registry entry for device_info
    Successfully imported service registry entry for bgp_iba
    Successfully imported service registry entry for table_usage


To enable service on particular system:

.. code-block:: bash

    aos> system service add --system <system_id> --name <collected_data> \
        --schema <schema>

To show data collected by particular service (the output is truncated):

.. code-block:: bash

    aos> system service data --system 525400CDF839 --service arp
    Service arp on system 525400CDF839 collected 2 items
    +-------------------+----------------------------------+---------------+-------------+
    | Identity          | Value                            | Last fetched  | Last Modif  |
    +-------------------+----------------------------------+---------------+-------------+
    | {u'ip_address':   | {u'interface_name': u'Ethernet2',| 2018-03-07T13 | 1970-01-01Z |
    |  u'203.0.113.14'} |  u'type': u'dynamicArp',         |               |             |
    |                   |  u'mac_address':                 |               |             |
    |                   |  u'52:54:00:84:3a:44'}           |               |             |
    +-------------------+----------------------------------+---------------+-------------+
    | {u'ip_address':   | {u'interface_name': u'Vlan2999', | 2018-03-07T13 | 1970-01-01Z |
    |  u'203.0.113.33'} |  u'type': u'dynamicArp',         |               |             |
    |                   |  u'mac_address':                 |               |             |
    |                   |  u'52:54:00:34:23:ac'}           |               |             |
    +-------------------+----------------------------------+---------------+-------------+

Set Blueprint Device Serial Numbers
-----------------------------------
The AOS CLI ``blueprint set-serial-numbers`` command does a batch assignment of
Managed Devices Serial Numbers (aka Device Keys) to Devices in a Blueprint.

.. note::

    This requires AOS 2.1 or later and AOS CLI
    container ``aoscli-0.1.200`` or later.

1. From the AOS Web interface, navigate to **Devices / Managed Devices** to see
   the list of Managed Devices.

2. Verify that the Devices have been acknowledged and that they
   are in ``OOS-READY`` state.

   .. image:: static/images/aos_cli/managed_devices.png

3. Create a CSV file with only the new devices that you want to assign.
   Existing assignments will not be changed. The format for the CSV file is
   ``label,device_serial_number``, where the label is the AOS device label or
   hostname in the Blueprint.

   .. image:: static/images/aos_cli/blueprint_labels.png

   .. code-block:: prompt

       admin@aos-server:~$ cd aosclitmp/
       admin@aos-server:~/aosclitmp$ cat devices.csv
       spine1,525400F79941
       spine2,5254005D9B0A
       l2_virtual_1_leaf,5254008BA653
       l2_virtual_2_leaf,5254008ADCFB
       l2_virtual_3_leaf,52540016B62A
       l2_virtual_4_leaf,52540031840C
       admin@aos-server:~/aosclitmp$

4. Start the AOS CLI and run the ``blueprint set-serial-numbers`` command.
   Use the ``--blueprint`` option to set the ID
   (for example, 51c6ee90-f28a-4ee4-8ce1-063437f27687) of the Blueprint.
   The CLI gives you the option to select from available Blueprints.
   Use the ``--file`` command to select the CSV file you created that will be
   in the ``/mytmp`` directory of the AOS CLI container.

   .. code-block:: prompt

       admin@aos-server:~/aosclitmp$ docker run --rm -ti -v $HOME/aosclitmp:/mytmp aoscli:0.1.200 -s 172.20.180.3
       Password [admin]:
       ----------------------------------------------------------------------------------------------------
       ****************************************************************************************************
       ************** WARNING: This tool is experimental and should not be used in production *************
       ****************************************************************************************************
       ----------------------------------------------------------------------------------------------------
       Welcome to AOS CLI! Press TAB for suggestions
       AOS Server URL: https://172.20.180.3:443
       aos> blueprint set-serial-numbers --blueprint 51c6ee90-f28a-4ee4-8ce1-063437f27687 --file /mytmp/devices.csv
       Updating 6 systems: {u'd8a4965e-df05-44d6-8edd-5ea470c1b353': {'system_id': '5254008BA653', 'id': u'd8a4965e-df05-44d6-8edd-5ea470c1b353', 'deploy_mode': 'ready'},
       u'5da8e3e0-2dc5-40c2-9ade-0277eedce312': {'system_id': '525400F79941', 'id': u'5da8e3e0-2dc5-40c2-9ade-0277eedce312', 'deploy_mode': 'ready'},
       u'0e9d3d5d-61c6-4b7a-96f4-ab11bc795ef0': {'system_id': '52540031840C', 'id': u'0e9d3d5d-61c6-4b7a-96f4-ab11bc795ef0', 'deploy_mode': 'ready'},
       u'f1f38944-152e-4e66-9222-b1ce8cd4a7c1': {'system_id': '5254008ADCFB', 'id': u'f1f38944-152e-4e66-9222-b1ce8cd4a7c1', 'deploy_mode': 'ready'},
       u'9b412958-1953-4791-a1b6-dc5cf4ac4f66': {'system_id': '5254005D9B0A', 'id': u'9b412958-1953-4791-a1b6-dc5cf4ac4f66', 'deploy_mode': 'ready'},
       u'24f8d5a7-a192-42cd-a477-4aaddd09b717': {'system_id': '52540016B62A', 'id': u'24f8d5a7-a192-42cd-a477-4aaddd09b717', 'deploy_mode': 'ready'}}
       Successfully updated blueprint with 6 serial numbers
       aos>

5. When the command has "successfully updated blueprint", in the AOS Web interface
   you will see the assignments in the Staged Blueprint.

   .. image:: static/images/aos_cli/blueprint_assignments.png

6. Commit the Uncommitted changes in the Staged Blueprint to the Active Blueprint.

Other Commands
--------------
AOS CLI has many more commands that are not covered here. Use TAB key to display
the list of available commands and options to explore and navigate through these.

.. _Python wheel packages: https://www.python.org/dev/peps/pep-0427/
