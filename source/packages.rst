========
Packages
========
AOS capabilities can be extended to add support for network operating systems
(NOS), new telemetry collectors, third party software, and more. Packages
(sometimes referred to as plugins) are uploaded to the AOS server, then they are
included in :doc:`agents <device_agents>` and
:doc:`agent profiles <agent_profiles>`. Valid package types include .egg, .whl
(Python wheel package) and .gz. One package can include one or more collectors
for one or more OS platforms.

Uploading Packages
==================
#. Download required package(s) from Apstra Downloads
   (https://portal.apstra.com/downloads).
#. From the AOS web interface, navigate to **Devices / System Agents /Packages**.

   .. image:: static/images/device_management/packages_330.png

#. Click **Upload Packages**, then, for each package to upload, either click
   **Choose File** and navigate to the downloaded file, or drag and drop
   the file into the dialog window.
#. Click **Upload**, then close the dialog to return to the list view.
