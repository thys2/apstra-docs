========================
Processor: Time in State
========================
The Time in State processor measures time when a value is in the range.
For each input DS, monitor it over the last time_window seconds. If at any
moment, for the state in state_range, the amount of time we have been in that
state over the last time_window seconds falls into a range specified in the
corresponding state_range entry, we set the corresponding output DS to 'true'.
Otherwise, the output DS for a given input DS is nominally 'false'. (previously
called 'time_in_state_check')

**Input Types** - Discrete-State(DS)

**Output Types** - Discrete-State (DS)

**Properties**

   Time Window (time_window)
     How long to monitor state. (seconds or an expression
     that evaluates to integer)
   State Range (state_range)
       Map state value to its allowed time range in seconds. dict mapping from a
       single possible state to a single range of time during the most recent
       time_window seconds that the value from input state is allowed to be in
       that state. At least one of the range object's two fields must be
       specified. The omitted field is regarded as "infinity". The fields are
       numbers (integers or floats) or expressions evaluated into numbers. State
       is a string or an expression that evaluates to string.

   .. include:: includes/processors/graph_query.rst

   .. include:: includes/processors/non_collector_graph_query.rst

   Anomaly MetricLog Retention Duration
      Retain anomaly metric data in MetricDb for specified duration in seconds
   Anomaly Metric Logging
      Enable metric logging for anomalies

   .. include:: includes/processors/enable_streaming.rst

   .. include:: includes/processors/raise_anomaly.rst

Time in State Example
---------------------
Config is set to:

.. code-block:: none

  time_window : 2 seconds
  state_range: { "down" : [{"max": 1},] }

The above configuration means that for the input DS, we will set
output to True and optionally raise an anomaly if the input is in the
"down" state for more-than one second out of the last two seconds.

In the sample below, certain values are capitalized to indicate what
has changed from the previous time.

Sample Input at time t=0

.. code-block:: none

  [if_name=eth0] : "up"
  [if_name=eth1] : "up"
  [if_name=eth3] : "up"

Sample Output at time t=0

.. code-block:: none

  [if_name=eth0] : "false"
  [if_name=eth1] : "false"
  [if_name=eth3] : "false"

Sample Input at time t=1:

.. code-block:: none

  [if_name=eth0] : "up"
  [if_name=eth1] : "down"
  [if_name=eth3] : "up"

Sample Output at time t=1

.. code-block:: none

  [if_name=eth0] : "false"
  [if_name=eth1] : "false"
  [if_name=eth3] : "false"

Sample Input at time t=2:

.. code-block:: none

  [if_name=eth0] : "up"
  [if_name=eth1] : "down"
  [if_name=eth3] : "up"

Sample Output at time t=2

.. code-block:: none

  [if_name=eth0] : "false"
  [if_name=eth1] : "true"
  [if_name=eth3] : "false"

Sample Input at time t=3:

.. code-block:: none

  [if_name=eth0] : "up"
  [if_name=eth1] : "up"
  [if_name=eth3] : "up"

Sample Output at time t=3

.. code-block:: none

  [if_name=eth0] : "false"
  [if_name=eth1] : "True"
  [if_name=eth3] : "false"

Sample Input at time t=4:

.. code-block:: none

  [if_name=eth0] : "up"
  [if_name=eth1] : "up"
  [if_name=eth3] : "up"

Sample Output at time t=4

.. code-block:: none

  [if_name=eth0] : "false"
  [if_name=eth1] : "false"
  [if_name=eth3] : "false"

If expressions are used for `min` or `max` fields for states specified in
the `state` property, then they are evaluated for each input item which results
into item-specific thresholds. Properties of the respective output items are
extended by `range_min` or `range_max` keys with calculated values.

If `state` key is an expression, output items are extended with `state` key.
The same applies for `time_window` property.

Configuration:

.. code-block:: none

    time_window : int(100/context.severity)
    state_range: { context.ref_state : [{"max": "int(20*(context.severity/5.0))"}] }

Sample Input at times t=0..6:

.. code-block:: none

  [if_name=eth0,severity=1,ref_state=down] : "down"
  [if_name=eth1,severity=2,ref_state=down] : "down"

Sample Output at time t=6:

.. code-block:: none

  [if_name=eth0,range_max=4,time_window=100,state=down] : "true"
  [if_name=eth1,range_max=8,time_window=50,state=down] : "false"
