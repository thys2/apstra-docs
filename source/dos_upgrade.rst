===============================
Device Operating System Upgrade
===============================
Device Operating Systems (aka Network Operating Systems, NOS) can be upgraded for
the following hardware devices directly from the AOS Web interface:

* Arista EOS (as of AOS 2.3)
* Cisco NX-OS (as of AOS 2.3)
* Cumulus Linux (as of AOS 3.1)
* Enterprise SONiC (as of AOS 3.3.0a)

.. note::
    * While Junos images can be uploaded to an AOS Server, Device Operating System
      upgrades are not yet supported for Junos devices.
    * This feature is not supported on virtualized switches.

.. warning::

    It is the user's responsibility to ensure they select a compatible Device OS
    Image for the devices to be upgraded. If the user tries to use an incompatible
    image and the upgrade fails, a deployment lock will not be released. Even if
    the user recovers the device, it may be necessary for the user to remove the
    device assignment from the AOS Blueprint, decommission and normalize the device
    under **Devices / Managed Devices** then reassign the device to the AOS
    Blueprint to release the deployment lock and make the device active again.
    Please contact :doc:`Apstra Global Support <support>` for assistance.

.. warning::

    When upgrading Device OS, the device has to be under Devices / Managed Devices
    and the Admin State has to be set as NORMAL. **Admin State MAINT/DECOMM must not
    be used** for Device OS upgrade as it may put the device into unrecoverable
    state.

Apstra will only test certain NOS upgrade paths. These are usually to a
recommended Device NOS version in a version of AOS, from the previous
recommended Device NOS version in the previous version of AOS, and between
recommended NOS version on the same version of AOS.

**Supported Device Operating System Upgrades**

* **AOS 3.3.0a**

  * NXOS:

    * From 7.0(3)I7(2), 7.0(3)I7(4) to 9.2(2)
    * From 7.0(3)I7(2), 7.0(3)I7(4) to 7.0(3)I7(7)
    * From 7.0(3)I7(2), 7.0(3)I7(4),7.0(3)I7(7) to 7.0(3)I7(8)

  * EOS:

    * From 4.20.11M to 4.21.5.1F or 4.21.9M or 4.22.3M

  * Cumulus Linux:

    * From 3.7.5, 3.7.11, 3.7.12 to 3.7.13
    * From 3.7.5, 3.7.11 to 3.7.12
    * From 3.7.5 to 3.7.11

  * Enterprise SONiC:

    * From SONiC 3.1.0-Enterprise_Base to SONiC 3.1.0a-Enterprise_Base

* **AOS 3.3.0**

  * NXOS:

    * From 7.0(3)I7(2), 7.0(3)I7(4) to 9.2(2)
    * From 7.0(3)I7(2), 7.0(3)I7(4) to 7.0(3)I7(7)
    * From 7.0(3)I7(2), 7.0(3)I7(4), 7.0(3)I7(7) to 7.0(3)I7(8)

  * EOS:

    * From 4.20.11M, 4.21.5.1F, 4.21.9M to 4.22.3M

  * Cumulus Linux:

    * From 3.7.5, 3.7.11, 3.7.12 to 3.7.13
    * From 3.7.5, 3.7.11 to 3.7.12
    * From 3.7.5 to 3.7.11

* **AOS 3.2.4**

  * NXOS:

    * From 9.2(2) to 9.3(3)
    * From 7.0(3)I7(7), 7.0(3)I7(8) to 9.3(3)

  * EOS:

    * From 4.20.11M, 4.21.5.1F, 4.22.3M to 4.23.4.2M
    * From 4.20.11M, 4.21.5.1F to 4.22.3M

  * Cumulus Linux:

    * From 3.7.5, 3.7.11, 3.7.12 to 3.7.13
    * From 3.7.5, 3.7.11 to 3.7.12
    * From 3.7.5 to 3.7.11

* **AOS 3.2**

  * NXOS:

    * From 7.0(3)I7(7) to 9.2(2)
    * From 7.0(3)I7(4) to 7.0(3)I7(7) or 9.2(2)

  * EOS:

    * From 4.20.11M to 4.21.5.1F or 4.22.3M

  * Cumulus Linux:

    * From 3.7.5 to 3.7.11

* **AOS 3.1**

  * NXOS:

    * From 7.0(3)I7(4) to 9.2(2)

  * EOS:

    * From 4.20.11M to 4.21.5.1F

  * Cumulus Linux:

    * From 3.7.4 to 3.7.5

* **AOS 3.0**

  * NXOS:

    * From 7.0(3)I7(2) to 7.0(3)I7(4)

  * EOS:

    * From 4.18.4.1F to 4.20.11M

Additional upgrade paths may be available. Contact
:doc:`Apstra Global Support <support>` for more information about
supported upgrade paths or to request support for a specific upgrade path.

.. warning::

    If you have manually created/cloned Device Profiles in the blueprints, they will
    not be updated on AOS upgrade even though the upgraded AOS release supports new
    device NOS versions. You need to follow the steps in the "Upgrading Device
    Operating System with Manually Cloned/Created Device Profiles" section below.

Upgrading Device Operating System
=================================
After the necessary Device :doc:`OS images <os_images>` have been uploaded or
registered, you can upgrade the device. You are responsible for managing the state
of any deployed devices being upgraded.
See :ref:`configuration_lifecycle:Deploy Modes` for more information.

.. important::

    NOS upgrade is not possible if agent's AOS version is different from
    controller's. As of AOS 3.3.0 release, if you try to upgrade NOS with different
    agent version, it does not show any warning but indicates "in progress" job
    state without doing anything.

.. warning::

    If you are upgrading a **Cumulus Linux device**, there **must** be a
    DHCP server on the device Management Network that is configured with a static
    DHCP assignment for the Cumulus Linux device.

    The Cumulus Linux device **must** get the same IP from the DHCP server as
    before the upgrade. AOS uses the Open Network Install Environment (ONIE)
    process to add a completely new Cumulus Linux to the device and return it
    to a factory default state. The eth0 management interface comes up
    configured for DHCP.

.. sidebar:: Agents

   If the Agent for your device is not on the
   list and needs to be created see :doc:`Agents <device_agents>` for
   instructions.

#. Navigate to **Devices / System Agents / Agents** to see the list of existing
   agents.
#. You can upgrade one or more devices at the same time. All selected devices must
   be of the same type and must be upgraded to the same image and version.
   You can use **Search criteria** to search for specific devices (such as all
   EOS devices). Select the device(s) to upgrade.
#. Click the **DOS Upgrade** button.

   .. image:: static/images/dos_upgrade/dos_upgrade7_320.png

#. The **Upgrade OS Image** dialog lists the available Device OS Images that match
   the selected devices. Select the appropriate OS Image and click
   **Upgrade OS Image** to upload the new image to the device.

   .. image:: static/images/dos_upgrade/dos_upgrade_8_310.png

#. You can monitor the upgrade status from the **Active Jobs** section at
   the bottom of the **Agents** page .

   .. image:: static/images/dos_upgrade/dos_upgrade_9_310.png

   .. note::

       For Cumulus Linux:

       * The local device username and password that was used to install the
         AOS Device System Agent is recreated.

       * The same Cumulus Linux license is reinstalled.

       * The Cumulus Linux license is reinstalled and the AOS configuration and
         deployment state is restored.

#. If a checksum is provided with the DOS image, AOS checks the image checksum
   after the upload is complete. If the checksum is incorrect, or if any other
   failures occur, the job state changes to **FAIL** and the device does not
   reboot. Click the Agent to view errors. You can also click the **Show Log**
   button to view the detailed Ansible job. If an upgrade fails, you must manually
   resolve the issue causing the failure. For example, with a checksum error,
   you must either correct the invalid checksum or register a new OS image with
   a correct checksum, then repeat the upgrade process.

#. If the checksum is correct and no other failures occur, the job state
   changes to **SUCCESS** and the device reboots.

#. When the device has rebooted with the new image and has reestablished its Agent
   connection with the AOS Controller, the upgrade is complete. The **Agents**
   page displays the new Device OS version.

   .. image:: static/images/dos_upgrade/dos_upgrade_10_310.png

Upgrading Device Operating System with Manually Cloned/Created Device Profiles
==============================================================================

AOS built-in Device Profiles are managed by Apstra, and the changes in the built-in
DPs are automatically propagated to the imported Interface Maps in the Blueprints
during the AOS upgrade. For example, AOS release 3.2.4 now supports Arista EOS 4.23
and the AOS 3.2.4 built-in DPs have the OS Version selector like
“4.(18|20|21|22|23)”, although AOS 3.2.2 built-in DPs have “4.(18|20|21|22)”.
During the AOS upgrade, the Interface Maps in the BPs which refer to the built-in
DPs are also automatically updated so that the user can upgrade Arista devices to
EOS version 4.23 after the AOS upgrade.

However, the manually created or cloned DPs are not managed by Apstra, and not
updated during the AOS upgrade, e.g. as below “Clone DCS-7160-48YC6_abc”.

 .. image:: static/images/dos_upgrade/dos_upgrade_4clonedDP_1.png

If you have manually cloned/created DPs and want to upgrade the OS Version selector,
you have to do the following steps.

#. Update the DP’s OS Version selector via AOS UI.

   .. image:: static/images/dos_upgrade/dos_upgrade_4clonedDP_2.png

#. Find the device_profile_nodes ID by using the AOS GraphQL API Explorer.

   Query Variables " { device_profile_nodes { id label } } " can be used. In this
   example, find the "id" for the label "Clone DCS-7160-48YC6_abc" which is
   "35a376ad-6ba1-42ec-bfe9-7810c56003d3".

   .. image:: static/images/dos_upgrade/dos_upgrade_4clonedDP_3.png

#. Update the Device Profile used in the BP via AOSCLI.

   You can use your BP ID, also use the node ID from step2, and set the proper
   model ID (e.g. "DCS-7160-48YC6"), and then execute.

   AOS-CLI command format:

   .. code-block:: text

      blueprint set-node-property --blueprint <your blueprint ID> --node_type
      device_profile --node <node ID from Step2> --property selector
      --value-fn '{"os_version":"4\.(18|20|21|22|23)\..*","model":"<your model>"
      ,"os": "EOS","manufacturer": "Arista"}'

   Example:

   .. code-block:: text

      aos> blueprint set-node-property --blueprint
      a74906ab-1c7a-42ee-bbea-7a0be2572bc2 --node_type device_profile
      --node 35a376ad-6ba1-42ec-bfe9-7810c56003d3 --property selector
      --value-fn '{"os_version":"4\.(18|20|21|22|23)\..*","model":"DCS-7160-48YC6",
      "os": "EOS","manufacturer": "Arista"}'

#. Commit the changes via AOS UI.

   .. image:: static/images/dos_upgrade/dos_upgrade_4clonedDP_4.png

   After the changes are committed, you should be able to upgrade the device
   operating system in the same manner as the devices associated with AOS built-in
   Device Profiles.
