=========================================================
VMs without Fabric Configured VLANs Probe (Virtual Infra)
=========================================================

Purpose
   Calculate VMs missing a VLAN on AOS and calculate VMs not backed by
   VLANs on AOS-managed leafs connected to hypervisors.
   (formerly known as VMs without AOS configured VLANs)

Source Processors
   VMs backed by Fabric VLANs (:doc:`generic graph collector <processor_generic_graph_collector>`)
      output stage: VMs backed by Fabric VLANs (number set)
      (generated from graph)

   VMs on hypervisors connected to Fabric (:doc:`generic graph collector <processor_generic_graph_collector>`)
      output stage: VMs on hypervisors connected to Fabric (number set)

Additional Processor(s)
   Differences between Fabric and Hypervisor (:doc:`set comparison <processor_set_comparison>`)
      input stage(s):
         VMs backed by Fabric VLANs (number set)

         VMs on hypervisors connected to Fabric (number set)

      output stage: VMs not backed by Fabric VLANs (number set)

   Affected VM Anomalies (:doc:`range <processor_range>`)
      input stage: VMs not backed by Fabric VLANs

      output stage: Affected VM Anomalies (discrete state set)

Example Usage
   **NSX-T Integration** - VMs participating in a particular network are attached
   to an NSX logical switch. In NSX transport zone controls to which hypervisors
   or ESXi host an NSX logical switch can span. To have VXLAN connectivity for
   these VMs they need to be part of the same transport zone. This predefined
   anomaly helps validate that all VLAN backend interfaces defined for NSX-T nodes
   are also configured on the ToR interfaces connecting that node to the fabric.

   VLAN probe anomaly checks for VLAN specification in case of NSX-T via one of
   the two methods below:

   Method One: When you have VMs that are connected to the NSX-T overlay, you can
   configure a bridge-backed logical switch to provide layer 2 connectivity with
   other devices or VMs. So via VLAN specification on NSX-T layer 2 bridges and
   fabric if respective VXLAN VN is not there, then AOS raises an anomaly.

   Method Two: Edge uplinks go out through VLAN logical switches. So let's say if
   the uplink VLAN logical switch has a particular VLAN ID and respective VLAN on
   ToR port connected to the hypervisor host is not configured then also this VLAN
   probe will raise anomalies and help detect such misconfiguration.

   The following is a simple topology where nsxcompute_001_server_001 and
   nsxedge_001_server001 are ESXi hosting VMs that are connected to the NSX-T
   overlay network.

   .. image:: static/images/nsx/NSX-T-Overlay-Network.png

   There is one VM on each ESXi host that needs a VXLAN VN endpoint on each leaf,
   i.e. nsxcompute_001_leaf1 and nsxedge_001_leaf1 to communicate on the overlay
   network.

   When VXLAN VNs assigned to ToR leafs are deleted, AOS raises VLAN misconfig
   anomalies as below under Fabric Health in the dashboard.

   .. image:: static/images/nsx/VLAN_Misconfig.png

   **VMs not backed by Fabric VLANs** shows VMs with VLAN missing.

   .. image:: static/images/nsx/VMs_Not_Backed_By_Fabric_Vlans.png

   **Affected VM Anomalies** shows VLAN missing in the AOS fabric.

   .. image:: static/images/nsx/VMs_Without_VLANs.png
