===============
Build - Virtual
===============

.. image:: static/images/blueprints/staged/virtual/resources_staged_330.png

From the :ref:`blueprint <access_blueprint>`, navigate to
**Staged / Virtual / Virtual Networks / Build**. (The build panel is on the right
side.)

Update Assignments
====================
#. A red status indicator means that resources need to be assigned. Resources may
   include virtual network SVI subnets for security zones, SVI subnets for MLAG
   domain, SVI subnet for virtual networks, VNI Virtual Network IDs, and VTEP IPs.
   Click the indicator, then click the **Update assignments** button.
#. Select a pool from which to pull the resources, then click the **Save** button.
   AOS assigns the required number of resources to the resource group
   automatically. When the red status indicator turns green, the resource
   assignment has been successfully staged.

Reset Resource Group Overrides
==============================
Certain blueprint operations require AOS to retain resource allocations even when
a device has been removed from a blueprint. The resource groups are overridden,
which mean that when a device is re-used, previously allocated resources are
re-used as well. Situations like this can (but do not always) result in build
errors. If you do not need to re-use the same resources, click the
**Reset resource group overrides** button to reset the resource group.

Manage Resource Pools
=====================
The resource assignment section has a convenient shortcut button,
**Manage resource pools**, that takes you to resource pool management.
There, you can monitor resource usage (new in AOS version 3.3.0)
and create additional resource pools, as needed.
