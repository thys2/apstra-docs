===============
Logical Devices
===============

Logical Device Overview
=======================
Networks can be designed in AOS before considering hardware vendors by using
logical devices (abstractions of physical devices) to specify common device form
factors such as number, speeds and roles of each port in one or more rack units
(panels). Some of the capabilities of logical devices include:

* Specifying speed and roles for specific ports (For example, the 48th port is
  always an L3 router edge, or the speed of the 10th port is always 1 Gbps).
* Preparing for port speed transformations (For example, transforming one - 40 GbE
  port into four - 10 GbE ports)
* Using non-standard port speeds (For example, when using a 1 GbE SFP in a 10 GbE
  port AOS correctly configures the underlying hardware.)
* Solving for automatic cable map generation that takes into account failure
  domains on modular systems (For example, a line card).

When hardware devices have been selected, logical devices can be mapped to them
with interface maps. Logical devices are also embedded into rack types and
rack-based templates.

.. image:: static/images/logical_devices/logical_device_330.png

**To access logical devices** - from the AOS web interface, navigate to
**Design /Logical Devices**. AOS ships with many predefined logical devices.

.. _logical_device_finished_example:

.. image:: static/images/logical_devices/logical_device_create3_330.png

**To see details** - click a logical device name. Logical devices include the
following details:

.. _logical_device_parameters:

Name
   64 characters or fewer to identify the logical device

Panel
   Port layout based on IP fabric, forwarding engine, line card (slot) or
   physical layout. A panel contains one or more port groups.

Port Group
   A group of ports with the same speed and role(s)

Number of ports
   Number of ports in the port group

Speed
   Speed of ports in the port group

Roles
   .. include:: includes/port_roles.rst
   .. important::
       **Access Switches** have limited support in AOS version 3.3.0. Please
       contact Apstra Support to learn more about this feature and the
       limitations.

.. sidebar:: Cloning Logical Device

   Instead of entering all details for a new logical device, you can clone an
   existing one, give it a new name and customize it.

Creating Logical Device
=======================
#. From the list view (Design / Logical Devices), click **Create Logical Device**
   and enter a name.
#. The default panel layout consists of 24 ports (2 rows of 12 ports each).
   For a different layout, select the number and arrangement of ports to match
   your requirements by dragging from the bottom-right corner of the layout.
#. Select the ports for the port group by dragging to select contiguous
   ports, or by clicking individual ports. Clicking a port again deselects it.
#. Select port speed, and applicable role(s) for the selected ports.
#. Click **Create Port Group** (bottom-middle) to create the port group.
#. If unassigned ports remain, repeat the previous two steps until all ports are
   assigned. For any ports that will not be used, assign them the *Unused* role.
#. To add a panel, click **Add Panel** (bottom-middle) and repeat the steps
   as for the first panel.
#. Click **Create** (bottom-right) to create the logical device and return to the
   list view.

.. _logical_device_use_case:

Creating Logical Device - Example
---------------------------------
Let's create a logical device with one panel containing one port group with 96 - 10
GbE ports and a second panel containing one port group with 8 - 40 GbE ports.

#. From the list view (Design / Logical Devices) click **Create Logical Device**.

   .. image:: static/images/logical_devices/logical_device_create1_330.png

#. A descriptive name is helpful when referring to the logical device later.
   For our example we entered **AOS-96x10-8x40-2**,
   which represents the following characteristics:

   * AOS - the operating system using this Logical Device

   * 96x10 - one panel with 96 - 10 GbE ports

   * 8x40 - one panel with 8 - 40 GbE ports

   * 2 - number of panels (rack units)

#. For the port group in the first panel, drag the bottom-right corner of the
   port layout to change the default 2x12 configuration to a 3x32 configuration.
   Leave the number of ports (96) and speed (10 Gbps) as is, and select
   **L2 Server** and **L3 Server** for roles (Connected to).

   .. image:: static/images/logical_devices/logical_device_create2_330.png

#. Click **Create Port Group** (bottom-middle), then click **Add Panel**
   (bottom-middle).
#. Drag the bottom-right corner of the port layout to change the configuration
   to 2x4. Leave the number or ports (8) as is, change the speed to **40 Gbps**,
   and connect them to **Superspine**, **Spine**, and **External Router**.
#. Click **Create Port Group**, then click **Create** (bottom-right). The
   finalized logical device can be seen in the logical device overview
   :ref:`above <logical_device_finished_example>`.

.. _logical_device_edit:

Editing Logical Device
======================
If a logical device is linked to an :doc:`interface map <interface_maps>`,
it cannot be changed.

To prevent potentially unintended changes to existing rack types or templates,
changes to logical devices do not affect them. If the intent is for a rack type
or template to use a modified logical device, then the rack type must
be :ref:`re-imported into the template <update_rack_type_template>`.

#. Either from the list view (Design / Logical Device) or the details view,
   click the **Edit** button for the logical device to edit.
#. Make your changes.

   * To change port group details, access the dialog by clicking its description.
   * To add or remove ports from a port group, drag from the bottom-right corner
     of the port group layout to resize it. If you added ports,
     enter port speed and role(s).
   * To remove a port group, click the delete button (upper-right).
   * To add a panel, click **Add Panel** and enter relevant port group details.

#. Click **Update** (bottom-right) to update it and return to the list view.

Deleting Logical Device
=======================
If a logical device is linked to an :doc:`interface map <interface_maps>`,
it cannot be deleted.

#. Either from the list view (Design / Logical Devices) or the details view,
   click the **Delete** button for the logical device to delete.
#. Click **Delete Logical Device** to delete it from the global catalog and return
   to the list view.
