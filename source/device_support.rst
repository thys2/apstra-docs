======================
Device and NOS Support
======================

Supported devices and recommended Device Network Operating System (NOS) versions
are listed below for different AOS versions.

..
    This list of versions is copied verbatim from the Product AOS Feature Matrix by
    NOS Google Spreadsheet

Arista EOS
==========

Supported Arista Devices
------------------------
DCS-7000 Series


Recommended EOS NOS Versions
----------------------------

.. list-table:: Arista EOS Recommended Version(s)
   :header-rows: 1

   * - AOS Version
     - EOS Version(s)
   * - 3.3.0.1
     - 4.24.3M, 4.22.3M, 4.21.9M
   * - 3.3.0 / 3.3.0a / 3.3.0c
     - 4.22.3M, 4.21.9M
   * - 3.2.4
     - 4.23.4.2M, 4.22.3M, 4.21.9M
   * - 3.2.3
     - 4.22.3M, 4.21.9M, 4.20.11M
   * - 3.2.2
     - 4.22.3M, 4.21.9M, 4.20.11M
   * - 3.2.1
     - 4.22.3M, 4.21.9M, 4.20.11M
   * - 3.2.0
     - 4.22.3M, 4.21.5.1F, 4.20.11M
   * - 3.1.1
     - 4.21.5.1F, 4.20.11M
   * - 3.1.0
     - 4.21.5.1F, 4.20.11M
   * - 3.0.1
     - 4.20.11M
   * - 3.0.0
     - 4.20.11M
   * - 2.3.1
     - 4.20.11M, 4.18.4.1F

Cisco NX-OS
===========

Supported Cisco Devices
-----------------------
Nexus 3000 or 9000 Platform

Recommended NX-OS Versions
--------------------------

.. list-table:: Cisco NX-OS Recommended Version(s)
   :header-rows: 1

   * - AOS Version
     - NX-OS Version(s)
   * - 3.3.0 / 3.3.0.1 / 3.3.0a / 3.3.0c
     - 9.2(2). 7.0(3)I7(8), 7.0(3)I7(7)
   * - 3.2.4
     - 9.3(3), 9.2(2), 7.0(3)I7(7)
   * - 3.2.3
     - 9.2(2), 7.0(3)I7(7)
   * - 3.2.2
     - 9.2(2), 7.0(3)I7(7)
   * - 3.2.1
     - 9.2(2), 7.0(3)I7(7)
   * - 3.2.0
     - 9.2(2), 7.0(3)I7(7)
   * - 3.1.1
     - 9.2(2), 7.0(3)I7(7)
   * - 3.1.0
     - 9.2(2), 7.0(3)I7(7)
   * - 3.0.1
     - 9.2(2), 7.0(3)I7(7)
   * - 3.0.0
     - 9.2(2), 7.0(3)I7(7)
   * - 2.3.1
     - 7.0(3)I7(4)

Cumulus Linux
=============

Supported Cumulus Devices
-------------------------
Cumulus Linux Supported Platforms with x86 CPU Processor

Recommended Linux Versions
--------------------------

.. list-table:: Cumulus Linux Recommended Version(s)
   :header-rows: 1

   * - AOS Version
     - Cumulus Linux Version(s)
   * - 3.3.0 / 3.3.0.1 / 3.3.0a / 3.3.0c
     - 3.7.13, 3.7.12
   * - 3.2.4
     - 3.7.13, 3.7.12
   * - 3.2.3
     - 3.7.11
   * - 3.2.3
     - 3.7.11
   * - 3.2.2
     - 3.7.11
   * - 3.2.1
     - 3.7.11
   * - 3.2.0
     - 3.7.11
   * - 3.1.1
     - 3.7.5
   * - 3.1.0
     - 3.7.5
   * - 3.0.1
     - 3.7.3
   * - 3.0.0
     - 3.7.3
   * - 2.3.1
     - 3.7.3

Juniper Junos
=============

Supported Juniper Devices
-------------------------
QFX10002, QFX5120, QFX5110, and QFX5100 Series

Recommended Junos NOS Versions
------------------------------

.. list-table:: Juniper Junos Recommended Version(s)
   :header-rows: 1

   * - AOS Version
     - Junos Version(s)
   * - 3.3.0 / 3.3.0.1 / 3.3.0a / 3.3.0c
     - 18.4R2 (QFX5100, QFX5110, QFX5120-48Y, QFX10002)

Enterprise SONiC
================

Supported Enterprise SONiC Devices
----------------------------------

The table below lists the only devices that are supported by both
SONiC Enterprise 3.1.0a/3.1.1 and Apstra AOS version 3.3.0a/3.3.0c.

.. list-table:: Enterprise SONiC Supported Devices on AOS Version 3.3.0a/3.3.0c
   :header-rows: 1

   * - Manufacturer
     - Model
   * - Dell
     - Z9332F-ON
   * - Dell
     - Z9264F-ON
   * - Dell
     - Z9100-ON
   * - Dell
     - S5296F-ON
   * - Dell
     - S5248F-ON
   * - Dell
     - S5232F-ON
   * - Edgecore/Accton
     - AS7816-64X
   * - Edgecore/Accton
     - AS7726-32X
   * - Edgecore/Accton
     - AS7712-32X
   * - Edgecore/Accton
     - AS7326-56X
   * - Edgecore/Accton
     - AS5712-54X

Recommended Enterprise SONiC Version
------------------------------------

Starting with AOS version 3.3.0a, only SONiC Enterprise is supported. No other
versions of SONiC, including SONiC community and earlier versions of Enterprise
SONiC are supported by AOS.

.. list-table:: Enterprise SONiC Recommended Version(s)
   :header-rows: 1

   * - AOS Version
     - SONiC Version(s)
   * - 3.3.0a
     - SONiC-OS-3.1.0a-Enterprise_Base
   * - 3.3.0c
     - SONiC-OS-3.1.1-Enterprise_Advanced

Requesting NOS Support
======================

Support for additional Network Device Network Operating Systems (NOS) versions may
be available. Contact :doc:`Apstra Global Support <support>` for more information
about supported devices and NOS.

To request support for NOS not listed, please contact your Apstra Sales
representative.
