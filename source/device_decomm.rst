===============
Removing Device
===============
After installing agents on devices and acknowledging them, they are managed by AOS
and can be assigned to blueprints. You can unassign devices from blueprints
and remove them from AOS management, as needed. See below for details.

Removing Device from Blueprint
==============================
#. :ref:`Remove the device assignment <system_id>` from the blueprint.

      .. image:: static/images/blueprints/staged/physical/device_unassign_one_330.png
         :scale: 60%

#. :doc:`Commit <uncommitted>` the change to remove the assignment. The device has
   been disassociated from the blueprint. It is still managed by AOS and it is
   ready and available to be assigned to the same blueprint or another one.

Removing Device from AOS Management
===================================
Devices that are no longer managed by AOS are disconnected from the AOS server
and removed from the AOS database.

.. important::
    Please follow the order of operations for successful device removal.

#. :ref:`Remove the device assignment <system_id>` from the blueprint
   (if it had been previously assigned).
#. :doc:`Commit <uncommitted>` the change to remove the device assignment from
   the blueprint, as applicable.
#. Set the :ref:`admin state <admin_state>` of the device to **DECOMM**.
#. :ref:`Uninstall the device agent <device_agents:uninstalling agent>`.

   .. note::
       Even after decommissioning and uninstalling the agent, the device still
       includes some interface configs that were added by AOS during discovery,
       including no shutdown, interface speeds for the device profile, and L3 mode
       config (e.g. no switchport), in addition to the original pristine config.

#. :ref:`Delete the device agent <device_agents:deleting agent>`.
#. :ref:`Delete the device <managed_devices:deleting system(s)>`
   from the managed devices list.

.. note::
    If you are decommissioning a faulty device and replacing it with another one
    for RMA using the same management IP address, please make sure that the
    original device has been decommissioned before the new device is assigned,
    as AOS expects each device to have a unique management IP address.

Replacing Device
================
If you are removing a device that will be replaced, follow the steps above
for removing a device from AOS management, then follow the steps for
:doc:`adding a device <device_add>`.
