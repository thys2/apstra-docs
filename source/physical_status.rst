======
Status
======
The physical view of the active blueprint shows the statuses for services and
deploy modes, deployment statuses for discovery, drain (as of AOS version 3.3.0)
and service, as well as traffic heat.

   .. image:: static/images/blueprints/active/physical/active_physical_status_330.png
