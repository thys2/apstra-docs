========================
Interface Flapping Probe
========================

.. image:: static/images/iba/if_flapping.svg

Found at /predefined_probes/fabric_interface_flapping

It first identifies all the fabric interfaces on all the leafs. Fabric interfaces
are defined as those facing spines. Then it collects operational status for each
interface and creates a time series out of it. Then it checks if the number of
state changes over configurable duration is within acceptable range (in this
case less than specified upper bound) and generates anomaly if that condition
is not satisfied. It then creates time series for this anomaly so that recent
history of existence and clearing of anomaly can be inspected. It also calculates
the percentage of interfaces on any given device that has flapping anomaly and if
that number is higher than the specified threshold it raises device level anomaly.
It then creates time series for this anomaly so that recent history can be
inspected.



"leaf fab int status" processor
-------------------------------

Purpose: wires in interface status telemetry for all fabric interfaces on the leafs.

Outputs of "leaf fab int status" processor
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

'leaf_if_status': set of operational states ("up" or "down"). Each set member
corresponds to a leaf fabric interface and has the following keys to identify
it: system_id (id of the leaf system, usually serial number), interface (name
of the interface).

"leaf fabric interface status history" processor
------------------------------------------------

Purpose: create recent history time series for each interface status In terms of
the number of samples, the time series will hold the smaller of: 1024 samples
or samples collected during the last 'total_duration' seconds (facade parameter).

Outputs of "leaf fabric interface status history" processor
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

leaf_fab_int_status_accumulate: set of interface status time series (for each spine
facing interface on each leaf). Each set member has the following keys to identify
it: system_id (id of the leaf system, usually serial number), interface (name of
the interface).


"leaf fabric interface flapping" processor
------------------------------------------

Purpose: Count the number of state changes in the leaf_fab_int_status_accumulate
("up" to "down" and "down" to "up"). If the count is higher than 'threshold'
facade parameter return "true", otherwise "false".


Outputs of "leaf fabric interface flapping" processor
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

if_status_flapping: set of statuses (for each spine facing interface on each leaf),
indicting if the interface has been flapping or not. Each set member has the
following keys to identify it: system_id (id of the leaf system, usually serial
number), interface (name of the interface).

"leaf fabric interface flapping anomaly" processor
--------------------------------------------------

Purpose: Export interface flapping as anomaly.

Outputs of "leaf fabric interface flapping anomaly" processor
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

leaf_fab_if_flap_anomaly: set of statuses (for each spine facing interface on each
leaf), indicting if the interface has been flapping or not. Each set member has the
following keys to identify it: system_id (id of the leaf system, usually serial
number), interface (name of the interface).


"anomaly flap leaf fabric history" processor
--------------------------------------------

Purpose: create recent history time series for each interface flapping anomaly.
The time series will hold up to 'anomaly_history_count' samples (facade parameter).

Outputs of "anomaly flap leaf fabric history" processor
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

anomaly_accumulate: set of interface flapping anomaly time series (for each spine
facing interface on each leaf). Each set member has the following keys to identify
it: system_id (id of the leaf system, usually serial number), interface (name of
the interface).


"percentage flapping per device interfaces" processor
-----------------------------------------------------

Purpose: Calculate percentage of interfaces that are flapping on any given device
under consideration.


Outputs of "percentage flapping per device interfaces" processor
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

'flapping_fab_int_perc': Set of numbers, each indicating the the percentage of
flapping interfaces on any given device under consideration. Each set member has
the following key to identify it: system_id (id of the leaf system, usually serial
number).

"system anomalous flapping" processor
-------------------------------------

Purpose: Evaluate if the percentage of flapping interface on any given device is
within acceptable range (less than max_flapping_interfaces_percentage facade
parameter) and generate system_flapping indication otherwise.

Outputs of "system anomalous flapping" processor
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

system_flapping: set of statuses for each leaf, indicting if the leaf has higher
then acceptable percentage of flapping interfaces. Each set member has
the following key to identify it: system_id (id of the leaf system, usually serial
number).

"system anomaly" processor
--------------------------

Purpose: Export system anomalous flapping as anomaly.

Outputs of "system anomaly" processor
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

system_flapping_anomaly: set of statuses for each leaf, indicting if the leaf has
higher then acceptable percentage of flapping interfaces. Each set member has
the following key to identify it: system_id (id of the leaf system, usually serial
number).

"system anomaly accumulate" processor
-------------------------------------

Purpose: create recent history time series for each leaf system flapping. In terms
of the number of samples, the time series will hold anomaly_history_count (facade
parameter).

Outputs of "system anomaly accumulate" processor
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

system_flapping_anomaly_accumulate: set of system flapping anomaly time series
(for each spine facing interface on each leaf). Each set member has the following
key to identify it: system_id (id of the leaf system, usually serial number).
