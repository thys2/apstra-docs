=========
ASN Pools
=========

ASN Pool Overview
=================
You can create resource pools during the design phase or just before you need them
during the build phase. When you assign resources to managed devices in your
network (blueprint), AOS pulls them automatically from the pool you specify. In
cases where you need to assign a specific network identifier you have the option
of assigning a resource individually. To prevent shortages as a network grows,
network design best practices recommend defining more resources than are required
for the initial design.

Superspines, spines, leafs and Layer 3 servers use BGP to exchange
routing information in the underlay. Autonomous system numbers (ASNs)
must be assigned to each device, either from the same pool or from
different pools. They can also be assigned individually if a specific resource
must be assigned to a specific device. ASN pools include the following details:

Pool Name
  To identify the resource pool.

Total Usage
  Percentage of ASNs in use for all ranges in the resource pool. (Hover over
  status bar to see number of ASNs in use and total number of ASNs
  in the pool.) (New in AOS version 3.3.0)

Range Usage
  The ASNs included in the range and the percentage that are in use. (Hover over
  status bar to see number of ASNs in use and total number of ASNs
  in that range.) (New in AOS version 3.3.0)

Status
  Indicates if the pool is in use.

Tags (optional)
  To enable filtering by user-specified categories.

From the AOS web interface, navigate to **Resources / ASN Pools**.

.. image:: static/images/resources/asn_pool_330.png

.. sidebar:: Cloning ASN Pool

   Instead of entering all details for a new ASN pool, you can clone an existing
   one, give it a new name and customize it.

Creating ASN Pool
=================
#. From the AOS web interface, navigate to **Resources / ASN Pools**, then click
   **Create ASN Pool**.
#. Enter a name, (optional) tags, and a range.

   * To specify an additional range, click **Add a range** and enter the range.

#. Click **Create** to create the pool and return to the list view.

When the blueprint is ready, you can assign resources by
:doc:`updating assignments <build_physical>` in the **Staged / Physical** view.

Editing ASN Pool
================
If any ASNs in an ASN pool are in use, they cannot be removed from the pool.

#. Either from the list view (Resources / ASN Pools) or the details view,
   click the **Edit** button for the pool to edit.
#. Make your changes.

   * To add a range, click **Add a range**.
   * To change a range, change the existing range numbers.
   * To remove a range, click the **X** to the right of the range to delete.

#. Click **Update** to update the pool and return to the list view.

Deleting ASN Pool
=================
If any ASNs in an ASN pool are in use, the pool cannot be deleted.

#. Either from the list view (Resources / ASN Pools) or the details view,
   click the **Delete** button for the pool to delete.
#. Click **Delete** to delete the pool and return to the list view.
