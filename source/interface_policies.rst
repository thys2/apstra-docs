==================
Interface Policies
==================

802.1X Server Port Authentication
=================================
**IEEE 802.1X** is an IEEE Standard for network port-based Network Access Control.
It is part of the IEEE 802.1 group of networking protocols. It provides an
authentication mechanism to devices wishing to attach to a LAN.

IEEE 802.1X defines the encapsulation of the Extensible Authentication Protocol
(EAP) over IEEE 802, which is known as "EAP over LAN" or EAPOL.

802.1X authentication involves three parties: a supplicant, an authenticator,
and an authentication server. The **supplicant** is a client device (such as a
server) that wishes to attach to the LAN. The term 'supplicant' is also used
interchangeably to refer to the software running on the client that provides
credentials to the authenticator. The **authenticator** is a network device which
provides a data link between the client and the network and can allow or block
network traffic between the two, such as an Ethernet switch or wireless access
point; and the **authentication server** is typically a trusted server that can
receive and respond to requests for network access, and can tell the authenticator
if the connection is to be allowed, and various settings that should apply to
that client's connection or setting. Authentication servers typically run software
supporting the RADIUS and EAP protocols. In some cases, the authentication server
software may be running on the authenticator hardware.

The authenticator acts as a security guard to a protected network. The supplicant
(i.e., client device) is not allowed access through the authenticator to the
protected side of the network until the supplicant’s identity has been validated
and authorized. With 802.1X port-based authentication, the supplicant must
initially provide the required credentials to the authenticator - these will have
been specified in advance by the network administrator and could include a user
name/password or a permitted digital certificate. The authenticator forwards these
credentials to the authentication server to decide whether access is to be granted.
If the authentication server determines the credentials are valid, it informs the
authenticator, which in turn allows the supplicant (client device) to access
resources located on the protected side of the network.

Extensions to 802.1X can also allow the authentication server to pass
port-configuration options to the authenticator. An example is using RADIUS
value-pair attributes to pass a VLAN ID, allowing the supplicant access to one of
several VLANs.

.. image:: static/images/blueprint_policies_tasks/interface_policy_dot1x_00.png
  :scale: 115%
  :align: center

(Source : Wikipedia, revised by Apstra)

.. versionadded:: 3.2
    802.1X server port authentication is introduced as a collection of interface
    policy settings. This feature allows AOS to manage 802.1X configuration on
    network devices.

.. warning::
    As of AOS 3.2, 802.1X interface policy is only supported on Arista EOS physical
    network devices.

This policy setting enables the network to require L2 servers in an AOS Blueprint
to authenticate to a RADIUS server before being provided access to the network.

The network operator may require clients to authenticate using EAP-TLS, Certificates,
simple username & password, or MAC Authentication bypass.

.. note::
    Support for encryption protocols, certificates, EAP, is negotiated between RADIUS
    supplicant and RADIUS server, and is not controlled by the switch.

After authentication occurs, a RADIUS server may optionally set a VLAN ID attribute
at authentication time to move the supplicant into a defined VLAN, known by a
leaf-specific VLAN ID.

This section describes the necessary tasks to create Interface Policies to be used
with AOS 802.1X server port authentication and dynamic VLAN allocation.

Refer to :doc:`Interface Policies <interface_policies>` for more details on
802.1X Server Port Authentication concept.

Common Scenarios
================
The following are some common scenarios for 802.1X port authentication.

Device supports 802.1X, credentials and VLAN are configured in Radius
---------------------------------------------------------------------

#. Device (Supplicant) connects to a port
#. Switch (Authenticator) mediates EAP negotiation between supplicant and Radius
   (Authentication Server)
#. Upon authentication, Radius sends an Access-Accept message to the switch which
   includes the VLAN number for the device
#. The switch adds the device port to the specified VLAN

Device supports 802.1X, but credentials are not configured in Radius
--------------------------------------------------------------------

#. Device (Supplicant) connects to a port
#. Switch (Authenticator) mediates EAP negotiation between supplicant and Radius
   (Authentication Server)
#. Finding no credential for the supplicant, Radius sends an Access-Reject message to
   the switch
#. The switch adds the device port to a designated Fallback (aka AuthFail/Parking)
   VLAN

Device does not support 802.1X, but the device MAC address is configured in Radius
----------------------------------------------------------------------------------

#. Device (Non-Supplicant) connects to a port
#. Switch (Authenticator) does not receive a reply to its EAP-Request Identity
   message, indicating no 802.1X support
#. Switch authenticates device's MAC address to Radius (Authentication Server)
#. Radius sends an Access-Accept message to the switch which includes the VLAN number
   for the device
#. The switch adds the device port to the specified VLAN

Device does not support 802.1X, and device MAC address is not configured in Radius
----------------------------------------------------------------------------------

#. Device (Non-Supplicant) connects to a port
#. Switch (Authenticator) does not receive a reply to its EAP-Request Identity
   message, indicating no 802.1X support
#. Switch authenticates device's MAC address to Radius (Authentication Server)
#. Radius does not find a record for the MAC address
#. Radius sends an Access-Reject or Access-Accept message to the switch without a
   VLAN
#. The switch adds the device port to a designated Fallback (aka AuthFail/Parking)
   VLAN

802.1X Interface Policy Workflow
================================
The following are the summary of 802.1X Interface Policy Workflow

#. Create virtual networks (e.g. Data VLAN, Fallback VLAN, Dynamic VLAN)
#. Create AAA servers
#. Create 802.1X interface policy
#. Edit 802.1X interface policy to assign ports and fallback VLANs

Creating Virtual Networks
=========================
From the Virtual view of your Staged Blueprint, click Virtual Network to see the
list of existing virtual networks. Click Create Virtual Networks button to create
virtual networks.

**Data VLAN (assigned to ports) :**

Interfaces will have 802.1X configuration if at least one VLAN is assigned to the
port. If a port does not have any VLANs assigned, 802.1X configuration will not be
rendered on the interface. The interface will be configured as a routed port.

**Dynamic VLAN (optional, assigned to leafs, not ports) :**

The RADIUS server itself optionally chooses the VLAN ID dynamically when the user
(supplicant) is authenticated and authorized.  AOS does not have control over Dynamic
VLAN assignment. This decision is made by RADIUS configuration, not by the switch
configuration.

**Fallback VLAN (optional, assigned to leafs, not ports) :**

Fallback VLAN can be assigned to the user (supplicant) in case of authentication
failure.
For fallback, the VLAN is controlled by the switch configuration.

.. image:: static/images/blueprint_policies_tasks/interface_policy_dot1x_04.png

A RADIUS dynamic VLAN or fallback VLAN must exist on the switch, but there is no
requirement to bind any endpoints to that VLAN. It only needs to exist on the switch.

.. note::
    Apstra suggests creating these virtual networks with a consistent VLAN ID amongst
    all leafs instead of using a resource pool.

Creating AAA Servers
====================
Create the AAA server.
See :ref:`Creating AAA Server <creating_aaa_server>` for more information.

Creating 802.1X Interface Policy
================================
From the Policies view of your Staged Blueprint, click Interface Policies then click
Create Interface Policy button.

.. image:: static/images/blueprint_policies_tasks/interface_policy_dot1x_08.png

An interface policy name allows you to describe it to other administrators.

.. note::
    Assigning interfaces or fallback VLANs to the policy is only possible after
    it is created.

Creating an interface policy will have a number of 802.1X options.

**Port Control :**

* Dot1x enabled - requires ports to authenticate EAPOL before given access to the
  network.
* Deny access - completely blocks the port, and no network access is permitted.
  An example use case of this is a quarantine configuration to quickly deactivate
  ports that may be infected.

**Host Mode :**

* Multi-host (default) allows all MAC addresses on the port to authenticate after the
  first successful authorization.  After the first host deauthorizes, all MACs on the
  port are deauthenticated.
* Single-host mode permits only a single host to authenticate and all other MACs are
  not permitted.

**MAC Auth Bypass:**

Optionally allows a switch to send the MAC address to the RADIUS server if the port
does not authenticate within the authentication timeout period. MAC Auth bypass (MAB)
requests are only sent if the client does not respond to radius requests, or if the
client fails authentication.

.. note::
    MAC Auth bypass must be configured alongside with 802.1X port control.

.. warning::
    MAC auth bypass failure behavior may be different between switch vendors and
    major switch models.

**Re-auth Timeout :**

Optionally configured as a time period (seconds). Reauthentication timeout causes
the switch to request any clients to re-authenticate to the network after the
timeout expires. This also re-triggers MAC Auth bypass.

.. note::
    If reauthentication timeout is not configured, then no related configuration
    is rendered on the switch. This means the switchport will be whatever the OS
    vendor default is. If a value is configured, AOS will enable 802.1X
    reauthentication on the port, and configure a time value.

.. image:: static/images/blueprint_policies_tasks/interface_policy_dot1x_09.png


Assigning Ports and Fallback VNs to the 802.1X Interface Policy
===============================================================
After the policy is created, you must add interfaces or dynamic VLANs to the
current policy.

From the Policies view of your Staged Blueprint, click Interface Policies
then scroll down to the Assign To section.

**Assign ports and interfaces :**

Click leaf names to expand interfaces, then click ports and interfaces to assign
them. Note that you cannot assign ports that are assigned to conflicting policies.

**Assign fallback VN :**

Assigning the fallback virtual network is leaf-specific. To re-use the fallback
on multiple leafs, you have to assign it to each leaf. Any VN that is assigned
to the leaf may be used as a fallback virtual network - there are no restrictions.

.. image:: static/images/blueprint_policies_tasks/interface_policy_dot1x_11b2.png


**Viewing the interface policy :**

After the policy is configured, the settings are now visible, including interfaces
those settings apply to.

.. note::
    AAA, Dot1x, and Dot1x interface configurations are now pushed to the leafs.
    The following is a part of sample config rendered by AOS for Arista EOS switch.

    .. code-block:: text

        leaf1#sh running-config section dot1x
        logging level DOT1X errors
        !
        aaa group server radius AOS_RADIUS_DOT1X
           server 172.20.191.5 vrf management
        !
        aaa authentication dot1x default group AOS_RADIUS_DOT1X
        aaa accounting dot1x default start-stop group AOS_RADIUS_DOT1X logging
        !
        interface Ethernet5
           switchport trunk allowed vlan 99
           switchport mode trunk
           switchport
           ipv6 enable
           ipv6 address auto-config
           ipv6 nd ra rx accept default-route
           dot1x pae authenticator
           dot1x reauthentication
           dot1x port-control auto
           dot1x timeout reauth-period 30
        !
        ..snip..
        !
        dot1x system-auth-control
        dot1x dynamic-authorization
