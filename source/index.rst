.. AOS documentation master file, created by
   sphinx-quickstart on Wed Jul 19 20:42:35 2017.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

.. Variables for documentation automation

.. |aos_version| replace:: AOS 3.3.1
.. |aos_server| replace:: 192.168.25.250

=======================================
Welcome to |aos_version| Documentation!
=======================================

.. raw:: html

   Licensed Apstra Customers can download current and previous versions of the Apstra AOS Documentation in PDF format from <a class="reference external" href="https://portal.apstra.com/downloads/" target="_parent">Apstra Downloads</a>.<p>

.. toctree::
   :maxdepth: 4

   quick_start
   aos_server
   aos_ui
   blueprints
   devices
   design
   resources
   external_systems
   platform
   favorites
   user
   guides
   reference
