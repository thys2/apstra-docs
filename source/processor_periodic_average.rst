===========================
Processor: Periodic Average
===========================
One N is created on output per each input. Each <period> seconds, the output is
set to the average of the input over the last <period> seconds. This is not
a weighted average.

**Input Types** - Number-Set (NS)

**Output Types** - Number-Set (NS)

**Properties**

   Period
      Size of the averaging period. (time in seconds, integer, or an expression
      that evaluates to time in seconds integer value)

   .. include:: includes/processors/graph_query.rst

   .. include:: includes/processors/non_collector_graph_query.rst

   .. include:: includes/processors/enable_streaming.rst

Periodic Average Example
------------------------

.. code-block:: none

  period: 2

Assume the following input at time t=1

.. code-block:: none

  [if_name=eth0] : 10
  [if_name=eth1] : 20
  [if_name=eth3] : 30

And following input at time t=1.5

.. code-block:: none

  [if_name=eth0] : 20
  [if_name=eth1] : 30
  [if_name=eth3] : 40

And the following at time t=2.1

.. code-block:: none

  [if_name=eth0] : 40
  [if_name=eth1] : 50
  [if_name=eth3] : 60

We would now have the following output:

.. code-block:: none

  [if_name=eth0] : 15
  [if_name=eth1] : 25
  [if_name=eth3] : 35

This output is the average over the last discrete period
of 2 seconds (time=0 to time=2).  Notice that the average is not weighted
by time;  frequently-occuring closely-spaced samples will
bias the average.

The next time the output would be updated would be at time t=4, in
which case it would contain the average of the input over the range [t=2, t=4],
a period of the configured two seconds.
