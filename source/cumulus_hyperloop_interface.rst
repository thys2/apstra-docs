========================================
Enable VXLAN Routing on Cumulus Tomahawk
========================================
Cumulus devices equipped with Tomahawk and Tomahawk+ ASICs do not natively support
Routing In and Out of Tunnels (RIOT). Therefore, switch ports for VXLAN
routing must be configured to use hyperloop interfaces. Hyperloop interfaces
recirculate packets through the ingress pipeline. As packets enter the VXLAN
tunnel they are encapsulated, as they exit the VXLAN tunnel they are decapsulated.

To set up the hyperloop interface, you'll create a device profile, make sure your
logical device has an unused port (as applicable), create an interface map,
update your managed devices, assign the new device profile to the device,
and deploy the device.

Creating a Device Profile for Hyperloop
=======================================
You can create a device profile from scratch, but it's more efficient to
:ref:`clone <device_profile_clone>` an existing one and change details that
are different.

#. Choose a device profile to clone from that is based on the device to be used
   for the hyperloop interface, and add 'hyperloop' to the name to distinguish it.

   .. image:: static/images/device_profiles/221_hyperloop_clone-dp.png

#. In the **Ports** section of the device profile that you are cloning, click the
   port that you want as the hyperloop port.

   .. image:: static/images/device_profiles/221_hyperloop_dp-port1.png

   .. warning::
     The remaining ports must be set to their native / default port speeds.
     If they are not, RIOT (and therefore VXLAN routing) will not work. For
     example, if you're using a 4 x 25G SFP28 breakout and you configure ports 1
     and 2 for hyperloop, then ports 3 and 4 must be set to the default of 25G. If
     you set them to 10G, you will have connectivity, but hyperloop will not work.

#. In the **Transformation** section, click the **Edit** button corresponding to
   that port.
#. In the **Settings** field, change the value for "speed" to ``"loopback"``,
   then click **Create**.

   In the example below, for "full speed" transformation (100Gbps on QSFP28 port),
   the value for "speed" was changed from the default speed of ``"100G"`` to
   ``"loopback"``.

   .. image:: static/images/device_profiles/221_hyperloop_dp-port2.png

Updating Logical Device for Hyperloop
=====================================
If you have a :doc:`logical device <logical_devices>` that includes the
hyperloop port, make sure the port is configured for an **Unused** role.

.. figure:: static/images/device_profiles/221_hyperloop_ld.png

Creating an Interface Map for Hyperloop
=======================================
Create an :doc:`Interface Map <interface_maps>` using the new device profile.

.. image:: static/images/device_profiles/221_hyperloop_im.png


Updating Managed Devices for Hyperloop
======================================
#. Navigate to the managed device that will use hyperloop.

   .. image:: static/images/device_profiles/221_hyperloop_md1.png

#. Click the **Edit** button (top-right) and select the new hyperloop
   device profile from the drop-down list.

   .. image:: static/images/device_profiles/221_hyperloop_md2.png

#. Click **Update** to save your changes.

Assigning Hyperloop Device Profile
==================================
In the blueprint, :ref:`assign the new device profile <staging_device_profiles>`
to the appropriate device, then deploy it.

Verifying Loopback Port
=======================
After deploying the device, verify that the hyperloop port is set to
"loopback" by looking at the device file ``/etc/cumulus/ports.conf``.

.. code-block:: prompt

    admin@border-1-leaf:mgmt-vrf:~$ sudo cat /etc/cumulus/ports.conf
    sudo: unable to resolve host border-1-leaf
    1=100G
    2=100G
    3=100G
    4=100G
    5=100G
    6=100G
    7=100G
    8=100G
    9=100G
    10=100G
    11=100G
    12=100G
    13=100G
    14=100G
    15=100G
    16=100G
    17=100G
    18=100G
    19=100G
    20=100G
    21=100G
    22=100G
    23=100G
    24=100G
    25=100G
    26=100G
    27=100G
    28=100G
    29=100G
    30=100G
    31=100G
    32=loopback
    admin@border-1-leaf:mgmt-vrf:~$
