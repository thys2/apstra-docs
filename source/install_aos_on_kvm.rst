=========================
Install AOS Server on KVM
=========================
Make sure that KVM is installed on your Linux distribution before continuing.

Method One: via Virtual Machine Manager
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
#. Click the **Create a new virtual machine** button.

   .. image:: static/images/server_installation/kvm_create.png
     :scale: 60%
#. Import the existing disk image.

   .. image:: static/images/server_installation/kvm_import.png

#. Specify the path for the qcow2 image and provide the following OS parameters.

   .. image:: static/images/server_installation/vm_browse.png
     :scale: 35%

#. Enter the required CPU and RAM values.

   .. image:: static/images/server_installation/vm_settings.png
     :scale: 35%

#. Select a network and provide respective bridge settings.

   .. image:: static/images/server_installation/vm_network.png
     :scale: 55%

#. When the VM is up and running you are ready to
   :doc:`Configuring AOS Server <configure_aos>`.
   Open a console to it with one of the KVM frontends (such as virt-manager).

   .. image:: static/images/server_installation/kvm_final.png

Method Two: Via Command Line
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
#. Linux KVM requires that the QEMU environment and bridge networking be installed
   and configured. Please refer to the following document as examples for QEMU
   install and configuration.

   **For RHEL**

   https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/7/html/virtualization_deployment_and_administration_guide/index

   **For Ubuntu**

   https://help.ubuntu.com/community/KVM/Installation

#. Uncompress the AOS "qcow2.gz" image with **gunzip**.

   .. code-block:: bash

      ubuntu@ubuntu:~$ gunzip aos_server_3.2.2-12.qcow2.gz
      ubuntu@ubuntu:~$

#. Create the VM with the ``virt-install`` command line tool. For example,
   installing the ``aos_server_3.2.2-12.qcow2`` image using existing bridge network
   (named br0).

   .. code-block:: bash

      ubuntu@ubuntu:~$ sudo virt-install --name=aos-server --disk=aos_server_3.2.2-12.qcow2 --os-type=linux --import --noautoconsole --vcpu=8 --ram=65535 --network=bridge=br0

      Starting install...
      Creating domain...
      Domain creation completed.
      ubuntu@ubuntu:~$ sudo virsh list
       Id    Name                           State
      ----------------------------------------------------
       1     aos-server                     running

      ubuntu@ubuntu:~$

#. Connect to the VM console. For example.

   .. code-block:: bash

      ubuntu@ubuntu:~$ sudo virsh console aos-server
      Connected to domain aos-server
      Escape character is ^]

      Apstra Operating System (AOS)

      aos-server login:

#. When the VM is up and running you are ready to
   :doc:`Configuring AOS Server <configure_aos>`.
