================
Processor: Union
================
The Union processor merges all input items into one set of items.
For each input item the processor leaves only signification keys, drops the
others and puts the result.

**Input Types** - Number-Set (NS), Discrete-State-Set (DSS), TS

**Output Types** - Number-Set (NS), Discrete-State-Set (DSS), TS

**Properties**

   .. include:: includes/processors/significant_keys.rst

   .. include:: includes/processors/enable_streaming.rst

Union Output Example
--------------------
Config is set to:

.. code-block:: none

    significant_keys: ["system_id"]

Consider we have inputs with device temperature information.

Input "in_1":

.. code-block:: none

    [system_id=leaf1,interface=eth1]: 45
    [system_id=leaf2,interface=eth0]: 52
    [system_id=leaf3,interface=eth0]: 61

Input "in_2":

.. code-block:: none

    [system_id=leaf4,interface=eth2]: 52
    [system_id=leaf5,interface=eth3]: 64

Input "in_3":

.. code-block:: none

    [system_id=leaf6,interface=eth3]: 41

Output will be the following.

Output "out":

.. code-block:: none

    [system_id=leaf1]: 45
    [system_id=leaf2]: 52
    [system_id=leaf3]: 61
    [system_id=leaf4]: 52
    [system_id=leaf5]: 64
    [system_id=leaf6]: 41
