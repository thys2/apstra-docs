.. _agent_profile_label:

==============
Agent Profiles
==============
Agent profiles enable the logical link between device credentials, a
key-value store to be used in the device configuration, and a selection of user
uploaded packages. With Agent profiles, you can configure parameters for a certain
class of devices that exist in the network and edit their device agent settings as
a group.

.. image:: static/images/device_management/agent_profiles_330.png

**To access agent profiles** - from the AOS web interface, navigate to
**Devices / System Agents / Agent Profiles**

.. image:: static/images/device_management/agent_profiles_detail_330.png

**To see details** - click an agent profile. Agent profiles include the following
details:

Name
  Identifies the profile..

Platform
  Device OS family..

Username / Password
  Admin/root username and password on the device..

Open Options
  Key-value list for custom variables that need to be passed to agents on
  the device..

Packages
  Admin-provided software packages stored on the AOS server that can be
  applied to each device agent that is created using the profile..

.. sidebar:: Cloning Agent Profile

   Instead of entering all details for a new agent profile, you can clone an
   existing one, give it a unique name and customize it.

Creating Agent Profile
======================
Before creating an agent profile, upload any :doc:`packages <packages>`
that are to be included in the agent profile.

#. From the list view (Devices / System Agents / Agent Profiles) click
   **Create Agent Profile**.
#. Enter a name, platform (EOS, CUMULUS, NXOS, SONIC, JUNOS), and optional
   username and password.
#. Add open options (optional).
#. Select package(s) (optional).
#. Click **Create** to create the agent profile and return to the list view.

Editing Agent Profile
=====================
#. Either from the list view (Devices / System Agents / Agent Profiles) or the
   details view, click the **Edit** button for the profile to edit.
#. Make your changes.
#. Click **Update** to update the profile and return to the list view.

Deleting Agent Profile
======================
#. Either from the list view (Devices / System Agents / Agent Profiles) or the
   details view, click the **Delete** button for the profile to delete.
#. Click **Delete** to delete the profile and return to the list view.
