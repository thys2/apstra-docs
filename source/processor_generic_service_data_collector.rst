=========================================
Processor: Generic Service Data Collector
=========================================
The Generic Service Data Collector processor collects data supplied by a custom
service that is not 'lldp', 'bgp' or 'interface'. Service name is specified as
'service_name', service specific key is specified as 'key', 'data_type' to
specifies if the collected data is numbers or discrete state values, and
'value_map' for the specific data could be specified as well.

**Input Types** - No inputs. This is a source processor.

**Output Types** - Discrete-State-Set (DSS), Number-Set (NS),
TS (based on data_type)

**Properties**

   .. include:: includes/processors/data_type.rst

   .. include:: includes/processors/graph_query.rst

   .. include:: includes/processors/value_map.rst

   Key (key)
     Expression mapping from graph query to whatever key is necessary for the
     service.

   .. include:: includes/processors/service_name.rst

   .. include:: includes/processors/system_id.rst

   Execution count
     Number of times the data collection is done.

   .. include:: includes/processors/service_input.rst

   .. include:: includes/processors/service_interval.rst

   .. include:: includes/processors/additional_keys.rst

   .. include:: includes/processors/enable_streaming.rst
