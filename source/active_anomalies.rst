===================
Anomalies (Service)
===================
This section covers service Anomalies. For analytics anomalies see
:doc:`IBA Anomalies <anomalies_probes>`.

Discovery Anomalies
===================
To demonstrate anomalies during the discovery phase, cabling errors have been
deliberately configured in the example below so that alarms are triggered.

.. image:: static/images/telemetry/discovery/2_dashboard_anomaly_322.png

To see the list of ten cabling anomalies, click the **Cabling** gauge on the
dashboard.

.. image:: static/images/telemetry/discovery/3_active_anomalies_322.png

To see the anomalies in the topology view, click **Active**.

.. image:: static/images/telemetry/discovery/4_active_topology_anomalies_322.png

To see the topology view of the anomalies affecting spine1, click **Spine1** in
the topology.

.. image:: static/images/telemetry/discovery/5_active_topology_spine1_anomalies_322.png

You can see the cabling violations on spine1. In the right panel, click the red
status indicator for **All Services** to see a comparison of expectations and
actual. If other anomalies existed in addition to the cabling anomalies,
they would be shown in the list below as well.

.. image:: static/images/telemetry/discovery/6_spine1_telemetry_anomalies_322.png

To see additional details specific to LLDP only, click LLDP.

.. image:: static/images/telemetry/discovery/7_spine1_telemetry_lldp_322.png

To see how to resolve these cabling issues, see
:doc:`Cabling Discovery <links>`.

.. _configuration_deviation:

Configuration Deviation
=======================
AOS continuously compares device running configurations with the
:doc:`Golden Config <configuration_lifecycle>`. If a config deviation is found a
configuration anomaly is raised. Typically such deviations are seen when changes
were made outside of AOS (by using the device CLI), or attempting to deploy
configuration on a switch that is not able to take the change. These anomalies
remain active until either the anomalous configuration is removed from the device
or the anomaly is suppressed.

#. From the Blueprint dashboard, any configuration deviations are displayed in the
   **Deployment Status** section.

   .. image:: static/images/anomalies/config_dev1_320.png
       :width: 400px

#. Click **Config Dev.** to see the list of node(s) with anomalies.

   .. image:: static/images/anomalies/config_dev2_320.png

#. Click a node name to see the device telemetry page, then click **Config** to
   see a side-by-side comparison of the actual config to the golden config.
   (The difference is not shown in the image below.)

   .. image:: static/images/anomalies/config_dev3_320.png

#. If you would like to keep the configuration difference, click **Accept Changes**.
   Note This **suppresses** the configuration anomaly, and does not affect "Intended"
   or AOS rendered config. the primary purpose of "Accept Changes" is to mitigate
   *cosmetic* configuration anomalies.

   .. important::
        Accepting the actual configuration cannot be used as a backdoor into AOS
        to implement out-of-band changes to the network (manual CLI). **AOS does
        not support OOB changes.** For custom changes, use
        :doc:`Configlets <configlets>`.

   .. warning::

     * Depending on the change, Out of Band changes can be overwritten by AOS. There
       is no way to avoid this. As such, OOB changes should always be avoided.
     * Using *Accept Changes* does **not** make the OOB change persistent. In the
       event of a full config push or AOS writing to the same config, all OOB
       changes are discarded.

   .. warning::

     Do not use **Accept Changes** on Cumulus Linux, unless suppressing a cosmetic
     anomaly. On Cumulus, AOS maintains device configuration using updates to
     various files, contents of which are overwritten. Any Blueprint commit
     requiring a change to a file that has out-of-band (OOB) changes results in
     those changes being discarded.

#. To make the actual configuration conform to the intended configuration, click
   **Apply Full Config**, then click **Confirm**. Applying the full config erases
   the device's current (unintended) configuration before re-applying the complete
   intended configuration. A full configuration push does not include any OOB
   changes, and therefore erases them, regardless of their "Accepted" state.

   .. warning::
       Applying a full config is a disruptive operation and will result in a
       temporary loss of service to the device.

   .. warning::

       Never directly modify any AOS rendered config that affects routing and
       connectivity. Doing so can potentially impact the network's operation. When
       in doubt, contact Apstra Support.

#. After resolving the config deviation anomaly, either by accepting changes or
   applying a full config, the actual config matches the golden config and the
   anomaly is cleared.

   .. image:: static/images/anomalies/config_dev4_320.png

.. _config_deviation_and_configlets:

Config Deviation and Configlets
-------------------------------
If an improperly-configured configlet causes AOS deployment errors (when the
command is rejected by the device), a **service config deployment** failure occurs.
In this case, follow the steps below to resolve the anomaly.

#. From the blueprint, navigate to **Staged / Catalog / Configlets** and
   delete the configlet.
#. Click **Uncommitted** and commit the change. The configuration deviation remains
   because the golden config is empty. The golden config is the running config of
   the device after *successful* deployment of AOS-rendered config. If deployment
   fails there is no golden config, thus causing the config deviation.
#. Click **Dashboard**, then click **Config Dev.** (in the **Deployment Status**
   section).
#. Click the node name, then select **Accept Changes** to notify AOS that the
   failure can be ignored.
