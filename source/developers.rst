==========
Developers
==========

Developer documentation and tools are available for AOS users in the **Developers**
option in the AOS UI **Platform** menu.

.. image:: static/images/developers/developers_1_330.png

API Documentation
=================

The **Documentation** section includes links to the AOS in-product API
documentation.

**Platform REST API Documentation** includes API documentation for APIs used
outside of an AOS Blueprint (e.g. AOS Global Catalog Logical Devices).

**Reference Designs Two stage L3 Clos** includes API documentation for APIs used
in the standard AOS L3 Clos Blueprint (e.g. AOS Blueprint Virtual-Networks).

**Reference Designs enterprise** is for future functionality and is not currently
supported by Apstra.

API Examples
------------

Examples for using the Apstra AOS API can be found in one of these sections.

.. toctree::
   :maxdepth: 1

   resource_pools_api
   configlet_api
   property_set_api
   external_routers_api
   interface_descriptions
   iba_probes_api
   api_rci
   api_aos_cluster
   python_api_examples

Tools
=====

Documentation for available Apstra AOS Developer Tools can be found in one of
these sections.

.. toctree::
   :maxdepth: 1

   rest_api_explorer
