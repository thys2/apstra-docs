=============================
Processor: Interface Counters
=============================
The Interface Counters processor selects interfaces according to the configuration
and outputs counter stats of the specified types (such as 'tx_bytes').

**Input Types** - No inputs. This is a source processor.

**Output Types** - Number-Set (NS)

**Properties**

   Counter Type (counter_type)
     A type of an interface counter. enum of: tx_unicast_packets,
     tx_broadcast_packets, tx_multicast_packets, tx_bytes, tx_error_packets,
     tx_discard_packets, rx_unicast_packets, rx_broadcast_packets,
     rx_multicast_packets, rx_bytes, rx_error_packets, rx_discard_packets.

   .. include:: includes/processors/graph_query.rst

   Interface (interface)
     Expression mapping from graph query to interface name, e.g. "iface.if_name"
     if "iface" is a name in the graph query.

   .. include:: includes/processors/system_id.rst

   .. include:: includes/processors/additional_keys.rst

   .. include:: includes/processors/enable_streaming.rst

Interface Counter Example
-------------------------

.. code-block:: none

      graph_query: "node("system", name="system").out("hosted_interfaces").
                    node("interface", name="iface").out("link").
                    node("link", role="spine_leaf")"
      counter_type: "rx_bytes"
      system_id: "system.system_id"
      interface: "interface.if_name"
      role: "system.role"

In this example, we create a NSS that has an entry for rx_bytes (per second) per
every interface in the system. Each entry is implicitly tagged by "system_id" and
"interface". Furthermore, as we have specified an additional property,
each entry is also tagged by role of the system.

.. code-block:: none
    :caption: Sample Data

    [system_id=spine1,role=spine,key=eth0]: 10
    [system_id=spine2,role=spine,key=eth1]: 11
    [system_id=leaf0,role=leaf, key=swp1]: 12
