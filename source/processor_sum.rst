==============
Processor: Sum
==============
The Sum processor groups as described by **Group by** property,
then calculates sum and outputs one for each group.

**Input Types** - Number-Set (NS), NSTS

**Output Types** - Number-Set (NS)

**Properties**

   .. include:: includes/processors/group_by.rst

   .. include:: includes/processors/enable_streaming.rst

Sum Output Example
------------------
See :doc:`standard deviation example <processor_standard_deviation>` -
it is the same except we calculate sum instead of std deviation.
