============================================================
VMware NSX-T Inventory mapping to AOS Virtual Infrastructure
============================================================

Overview
=========

AOS is able to connect to the NSX-T API to gather information about the
inventory in terms of hosts, clusters, VMs, portgroups, vDS/N-vDS, and NICs
within the NSX-T environment. AOS can integrate with NSX-T to provide AOS admins
visibility into the application workloads (aka VMs) running and alert them
about any inconsistencies that would affect workload connectivity. **AOS
Virtual Infrastructure visibility** helps provide underlay/overlay correlation
visibility and use IBA analytics for overlay/underlay.


One cannot view the NSX Inventory in AOS until the NSX-T manager is associated
to a blueprint.

.. image:: static/images/nsx_inventory_mapping/AOS_NSX_Inventory.png
  :scale: 40%
  :align: center


As per above screenshot inventory collection for NSX-T is done via AOS
extensible telemetry collector.

NSX-T Networking Terminology and correlation
=============================================

NSX-T uses the following terminology for their control plane and data plane
components. Also please find respective correlation with respect to AOS.

TRANSPORT ZONES
---------------

Transport Zones (TZ) are used to define a group of ESXi hosts that can
communicate with one another on a physical network.

.. image:: static/images/nsx_inventory_mapping/Transport_zone.png
  :scale: 40%
  :align: center


There are two types of Transport Zones:

#.  Overlay Transport Zone: This transport zone can be used by both transport
    nodes or NSX edges.When an ESXi host or NSX-T Edge transport node is added
    to an Overlay transport zone, an N-VDS is installed on the ESXi host or
    NSX Edge Node.
#.  VLAN Transport Zone: It can be used by NSX Edge and host transport
    nodes for its VLAN uplinks.

Each Hypervisor Hosts can only belong to one Transport Zone at a given
point of time.

A newly created VLAN VN tagged towards an  interface in AOS fabric corresponds
to a VLAN based transport zone as per the screenshots below:

.. image:: static/images/nsx_inventory_mapping/VLAN_VN.png
  :scale: 40%
  :align: center


Here tagged VLAN VN is mapped to the respective Transport Zone in NSX-T with
traffic type as VLAN.

.. image:: static/images/nsx_inventory_mapping/VLAN_TZ.png
  :scale: 40%
  :align: center

N-VDS
------

It is a NSX managed virtual distributed switch which provides the underlying
forwarding and is the data plane of the transport nodes.

A few notables about N-VDS virtual switches include:

* pnics are physical ports on the ESXi host
* pnics can be bundled to form a link aggregation (LAG)
* uplinks are logical interfaces of an N-VDS
* uplinks are assigned pnics or LAGs

.. image:: static/images/nsx_inventory_mapping/N-VDS.png
  :scale: 40%
  :align: center

Here TEP are Tunnel Endpoints used for the NSX overlay networking
(geneve encapsulation/decapsulation). P1/P2 are pNICs mapped to the
uplink profile(U1/U2).

N-VDS  are instantiated at the Hypervisor level and can be thought of Virtual
switch connected to the ToR physical leafs as below:

.. image:: static/images/nsx_inventory_mapping/N-VDS_HV.png
  :scale: 40%
  :align: center


Transport Node
---------------
It is a node capable of participating in an NSX-T Data Center overlay or
VLAN networking.

.. image:: static/images/nsx_inventory_mapping/TN.png
  :scale: 20%
  :align: center


VMs hosted on different Transport nodes communicate seamlessly across the
overlay network. A transport node can belong to:

* Multiple VLAN transport zones.
* At most one overlay transport zone with a standard N-VDS.

This can be compared to setting end hosts(servers)  in an AOS blueprint to be
part of  VLAN (leaf-local) or VXLAN (inter-leaf) Virtual Network.

NSX Edge Node
-------------

The NSX Edge provides routing services and connectivity to networks that are
external to the NSX-T deployment. It is required for establishing external
connectivity from the NSX-T domain, through a Tier-0 router via BGP or static
routing.

NSX Edge VMs have uplinks towards ToR leaves needing a separate VLAN transport
zone. AOS fabric must be configured with the corresponding
VLAN Virtual Network.


.. image:: static/images/nsx_inventory_mapping/nsx_edge.png
  :scale: 40%
  :align: center


NSX Controller Cluster
----------------------

It provides control plane functions for NSX-T Data Center logical switching
and routing components.

NSX Manager
-----------

It is a node that hosts the API services, the management plane, and the
agent services.

NSX Inventory Model
===================

.. image::static/images/nsx_inventory_mapping/nsx_inventory.png
  :scale: 40%
  :align: center


* In NSX-T Transport nodes are hypervisor hosts and they can be correlated to
  server nodes in a Blueprint connected to the ToR leafs. In NSX-T Data Center,
  ESXi hosts are prepared as Transport Node which allows nodes to exchange
  traffic for virtual networks on AOS Fabric or amongst network on nodes. You
  must ensure hypervisors (ESXi) networking stack is sending LLDP packets to
  aid the correlation of ESXi hosts with server nodes in the blueprint.

* PNIC is the  actual physical network adapter on ESXi or hypervisor host.
  Hypervisor PNICs can be correlated to the server interface on the Blueprint.
  LAG or Teaming configuration is done on the links mapped  to these physical
  NICs. This can be correlated to bond configuration done on the ToR leafs
  towards the end servers.

* In NSX-T integration with AOS VM virtual networks are discovered. These can
  be correlated to blueprint virtual networks. In case VMs need to communicate
  with each other over tunnels between hypervisors VMs are connected to the
  same logical switch in NSX-T(called N-VDS). Each logical switch has a
  virtual network identifier (VNI), like a VLAN ID. This corresponds to VXLAN
  VNIs as in AOS fabric physical infrastructure.

*  The NSX-T Uplink Profile defines the network interface configuration facing
   the fabric in terms of LAG and LACP config on PNIC interfaces. The uplink
   profile is mapped in Transport node for the links from the hypervisor/ESXi
   towards  top-of-rack switches in AOS Fabric.

*  VNIC defines Virtual Interface of transport nodes or VMs. N-VDS switch does
   mapping of physical NICs to such uplink virtual interfaces. These Virtual
   Interfaces can be correlated to server interface ports of AOS Fabric.

Exact Model details and Relationship
====================================

**Hypervisor**
--------------

* **Hostname:** Fqdn attribute of transport node
* **Hypervisor_id:** Id attribute of transport node
* **Label:** Display name attribute of transport node
* **version:** NSX-T version installed on the transport node

In AOS  to obtain NSX-T API response for respective hypervisor hosts and
understand the correlation you can use graph query. To open the GraphQL
Explorer, you can click the “>_” button

.. image::static/images/nsx_inventory_mapping/graph.png
  :scale: 40%
  :align: center


After that in the graph explorer we can type a graph query on the left as per
the screenshot below using GraphQL:

.. image::static/images/nsx_inventory_mapping/HV_IDs.png
  :scale: 40%
  :align: center


To check for respective Label for the transport nodes below query can be used:

Request:

.. code-block:: text

    {
     hypervisor_nodes{
     label
     }
     }

Response:

.. code-block:: json


 {
  "data": {
    "hypervisor_nodes": [
      {
        "label": "zz-karun-nsxt.cvx.2485377892354-357746820-TN-2"
      },
      {
        "label": "zz-AndyF-nsxt.cvx.2485377892354-4240714876-TN-2"
      }
    ]
  }
 }





Hypervisors which act as Transport Nodes can be visualized in AOS under
**Active** tab with **Has Hypervisor = Yes** option as below:

.. image::static/images/nsx_inventory_mapping/Active_HV.png
  :scale: 40%
  :align: center

To obtain respective hostname for the transport nodes below query can be used:


Request:

.. code-block:: text

    {
     hypervisor_nodes {
     hostname
     }
     }

Response:

.. code-block:: json

 {
  "data": {
    "hypervisor_nodes": [
      {
        "hostname": "localhost"
      },
      {
        "hostname": "ubuntu-bionic-nsxt"
      }
    ]
  }
 }

**Hypervisor PNIC**
-------------------

* **MAC address:** Physical address attribute of transport node’s interface
* **Switch_id:** Switch name attribute of transport node’s transport zone
* **Label:** Interface id attribute of transport node’s interface
* **Neighbor_name:** System name attribute of transport node’s interface
  lldp neighbor
* **Neighbor_intf:** Name attribute of transport node’s interface lldp
  neighbor
* **MTU:** MTU attribute of transport node’s interface


Physical NICs are selected for uplink profile dedicated for the Overlay
Network. NSX-T Uplink Profile defines the network interface configuration for
the PNIC interfaces facing the AOS fabric in terms of LAG and LACP config.

.. image::static/images/nsx_inventory_mapping/PNIC_TN.png
  :scale: 40%
  :align: center


So the uplink profile is mapped in Transport node for the links from the
NSX-T logical switch of the hypervisor/ESXi hosts. It points towards
top-of-rack switches in AOS Fabric.

NSX-API Request/Response to check MAC address for the Transport node
interfaces.

Request:

.. code-block:: text

  {
  pnic_nodes {
  id mac_address
  }
  }

Response:

.. code-block:: json

 {
   "data": {
    "pnic_nodes": [
      {
        "id": "1e2162c3-9ce6-4f35-afc2-217bb48ced49",
        "mac_address": "52:54:00:88:41:28"
      },
      {
        "id": "9752a438-1939-4648-bc8e-0494addf7c7e",
        "mac_address": "52:54:00:04:d5:4f"
      }
    ]
  }
 }



.. image::static/images/nsx_inventory_mapping/AOS_MAC.png
  :scale: 40%
  :align: center


The MAC address shown in above example is learned on a LAG interface in AOS
Fabric towards the NSX-T Transport Node. It is the MAC address of the ESXi host
pNICs having LAG bond towards ToR leafs in AOS fabric.

Please find below NSX-API Request/Response to check Switch name attribute of
transport node’s transport zone.

Request:

.. code-block:: text

  {
  pnic_nodes {
  id switch_id
  }
  }

Response:

.. code-block:: json

 {
  "data": {
    "pnic_nodes": [
      {
        "id": "82586be7-2998-401f-82ba-11afa5bb9730",
        "switch_id": "zz-cvx-nsxt.cvx.2485377892354-2902673742"
      },
      {
        "id": "0043d742-405a-454f-9e9b-695d5dd14608",
        "switch_id": "zz-cvx-nsxt.cvx.2485377892354-2902673742"
      }
    ]
  }
 }

Switch ID attribute of the respective transport zone are read by NSX-T API
from NSX manager as below:


.. image::static/images/nsx_inventory_mapping/switch_id.png
  :scale: 40%
  :align: center


NSX-API Request/Response to check Transport node’s interface.

Request:

.. code-block:: text

  {
  pnic_nodes {
  id label
  }
  }

Response:

.. code-block:: json

 {
   "data": {
    "pnic_nodes": [
      {
        "id": "82586be7-2998-401f-82ba-11afa5bb9730",
        "label": "eth2"
      },
      {
        "id": "0043d742-405a-454f-9e9b-695d5dd14608",
        "label": "eth1"
      },
      {
        "id": "b91a5725-7500-489b-a454-e05d7c311525",
        "label": "eth0"
      }
    ]
  }
 }

Transport nodes has the mapping of physical NICs which can be seen returned as
labels according to above NSX-T API response.

.. image::static/images/nsx_inventory_mapping/TNs_Label.png
  :scale: 40%
  :align: center


Please find below NSX-API Request/Response to check  Transport node’s LLDP
neighbor System name attribute.

Request:

.. code-block:: text

  {
  pnic_nodes {
  id neighbor_name
  }
  }

Response:

.. code-block:: json

  {
    "data": {
      "pnic_nodes": [
        {
          "id": "82586be7-2998-401f-82ba-11afa5bb9730",
          "neighbor_name": "leaf-2-525400C6DD2B"
        },
        {
          "id": "0043d742-405a-454f-9e9b-695d5dd14608",
          "neighbor_name": "leaf-2-525400C6DD2B"
        },
        {
          "id": "b91a5725-7500-489b-a454-e05d7c311525",
          "neighbor_name": "spine-1"
        },
        {
          "id": "f77575fb-44ea-4ec7-9913-1c75b7af87bc",
          "neighbor_name": "leaf-1-5254004D5560"
        },
        {
          "id": "628d0f86-4bc1-4faf-8f3f-f1deb92ceee2",
          "neighbor_name": "leaf-2-525400C6DD2B"
        },
        {
          "id": "1e2162c3-9ce6-4f35-afc2-217bb48ced49",
          "neighbor_name": "leaf-1-5254004D5560"
        }
      ]
    }
   }

Here Leaf1/2 are LLDP neighbors to the Transport nodes.

To obtain respective transport node’s LLDP neighbor interface name attribute
below query can be used:

Request:

.. code-block:: text

  {
  pnic_nodes {
  id neighbor_intf
  }
  }

Response:

.. code-block:: json

 {
   "data": {
    "pnic_nodes": [
      {
        "id": "82586be7-2998-401f-82ba-11afa5bb9730",
        "neighbor_intf": "swp4"
      },
      {
        "id": "0043d742-405a-454f-9e9b-695d5dd14608",
        "neighbor_intf": "swp3"
      },
      {
        "id": "b91a5725-7500-489b-a454-e05d7c311525",
        "neighbor_intf": "eth0"
      }
    ]
  }
 }

NSX-API Request/Response to check the MTU attribute of Transport
node’s interface.

Request:

.. code-block:: text

  {
  pnic_nodes {
  id mtu
  }
  }


Response:

.. code-block:: json

 {
  "data": {
    "pnic_nodes": [
      {
        "id": "82586be7-2998-401f-82ba-11afa5bb9730",
        "mtu": 1600
      },
      {
        "id": "0043d742-405a-454f-9e9b-695d5dd14608",
        "mtu": 1600
      }
    ]
  }
 }

MTU size of 1600 or greater is needed on any network that carries Geneve
overlay traffic must. Hence in the NSX-T reply we can notice MTU value 1600 on
network interfaces towards Transport nodes.

**VNIC**
-------------------

* **MAC address:** Physical address attribute of transport node’s or VM's
  Virtual interface
* **Label:** VNIC label attribute of transport node
* **Ipv4_addr:** IP address attribute of transport node’s virtual interface
* **Traffic_types:** It is derived from transport node’s virtual interface type
* **MTU:** MTU attribute of transport node’s virtual interface

Please find below NSX-API Request/Response to check VNIC mac address
attribute. This can be of transport node’s interface Virtual Interface or can
be for the Virtual Interface of the VMs. For transport nodes under Host
Switches  select the Virtual NIC that matches the MAC address of the VM NIC
attached to the uplink port group.

Request:

.. code-block:: text

  {
  vnic_nodes{
  id mac_address
  }
  }

Response:

.. code-block:: json

 {
   "data": {
    "vnic_nodes": [
      {
        "id": "c84d8636-c28b-4db3-8747-37fadca4c7aa",
        "mac_address": "1e:5c:3b:a2:ea:c3"
      },
      {
        "id": "7d5826d8-0622-4a45-88d7-6b1e88bac62f",
        "mac_address": "ca:0f:93:24:24:43"
      }
    ]
  }
 }

NSX-API Request/Response to check VNIC label which signifies interface
id attribute of transport node’s virtual interface or device name
attribute of virtual machine’s virtual interface.

Request:

.. code-block:: text

  {
  vnic_nodes{
  id label
  }
  }

Response:

.. code-block:: json

 {
   "data": {
    "vnic_nodes": [
      {
        "id": "c84d8636-c28b-4db3-8747-37fadca4c7aa",
        "label": "hyperbus"
      },
      {
        "id": "7d5826d8-0622-4a45-88d7-6b1e88bac62f",
        "label": "nsx-switch.0"
      },
      {
        "id": "473c2b7d-ab2f-41cd-9a4b-fcf2eb248fd6",
        "label": "nsx-switch.0"
      },
      {
        "id": "9553390b-754e-45ef-8976-e63396d554ee",
        "label": "nsx-vtep0.0"
      },
      {
        "id": "a00bb649-5032-462f-97e7-b6c4f5f1ac86",
        "label": "nsx-vtep0.0"
      }
    ]
  }
 }

Please find below NSX-API Request/Response to check VNIC Ipv4 address which
signifies ip address attribute of transport node’s virtual interface or
for the virtual interface of logical port.

Request:

.. code-block:: text

  {
  vnic_nodes{
  id ipv4_addr
  }
  }

Response:

.. code-block:: json

 {
   "data": {
    "vnic_nodes": [
      {
        "id": "9553390b-754e-45ef-8976-e63396d554ee",
        "ipv4_addr": "192.168.1.13"
      },
      {
        "id": "a00bb649-5032-462f-97e7-b6c4f5f1ac86",
        "ipv4_addr": "192.168.1.12"
      }
    ]
  }
 }

.. image::static/images/nsx_inventory_mapping/VTEP_TN.png
  :scale: 40%
  :align: center

Here “192.168.1.13” and “192.168.1.12” are ipv4 addresses for the bridge
interface of the host transport nodes i.e **"nsx-vtep0.0"** which acts as a
virtual tunnel endpoint (VTEP) of the transport node. Each hypervisor has a
Virtual Tunnel Endpoint (VTEP) responsible for encapsulating the VM traffic
inside a VLAN header and routing the packet to a destination VTEP for
further processing. This can be compared to VXLAN Virtual Network anycast
GW VTEP IP.

::

  nsx-vtep0.0: flags=4163<UP,BROADCAST,RUNNING,MULTICAST>  mtu 1600
      inet 192.168.1.12  netmask 255.255.255.224  broadcast 192.168.1.31
      inet6 fe80::c8ec:50ff:fe69:536  prefixlen 64  scopeid 0x20<link>
      ether ca:ec:50:69:05:36  txqueuelen 1000  (Ethernet)
      RX packets 60312  bytes 3975194 (3.9 MB)
      RX errors 0  dropped 0  overruns 0  frame 0
      TX packets 31215  bytes 2675310 (2.6 MB)
      TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0
  admin@localhost:~$

NSX-API Request/Response to check traffic types for the transport node’s
virtual interface. Traffic type for the transport node can be overlay type
as per the example below or it can be of VLAN type. One can add both the
VLAN and overlay NSX Transport Zones to the Transport Nodes.

VLAN based Transport zone is mainly for uplink based traffic. In case VMs
on different Hypervisor hosts need to communicate to each other then
overlay network should be used. It can be compared to VXLAN Virtual
network in AOS Fabric.

Request:

.. code-block:: text

  {
  vnic_nodes{
  id traffic_types
  }
  }

Response:

.. code-block:: json

 {
   "data": {
    "vnic_nodes": [
      {
        "id": "9553390b-754e-45ef-8976-e63396d554ee",
        "traffic_types": [
          "overlay"
        ]
      },
      {
        "id": "a00bb649-5032-462f-97e7-b6c4f5f1ac86",
        "traffic_types": [
          "overlay"
        ]
      }
    ]
  }
 }

NSX-API Request/Response to obtain the mtu size for the transport node. MTU
size for networks that carry overlay traffic must be size of 1600 or greater
as it carries Geneve overlay traffic. N-VDS and TEP kernel interface all
should have the same jumbo frame MTU size(i.e 1600 or greater).

Request:

.. code-block:: text

  {
  vnic_nodes{
  id mtu
  }
  }

Response:

.. code-block:: json

 {
   "data": {
    "vnic_nodes": [
      {
        "id": "9553390b-754e-45ef-8976-e63396d554ee",
        "mtu": 1600
      },
      {
        "id": "a00bb649-5032-462f-97e7-b6c4f5f1ac86",
        "mtu": 1600
      }
    ]
  }
 }

.. image::static/images/nsx_inventory_mapping/MTU_TN.png
  :scale: 40%
  :align: center


So Virtual Interface i.e NSX VTEP and vswitch should have mtu of 1600 as per
screenshot above.

**Port channel policy**
-----------------------

* **Label:** Name attribute of the host switch uplink lag profile
* **Mode:** Mode attribute of host switch uplink lag profile
* **Hashing_algorithm:** Load balance algorithm attribute of host switch
  uplink lag profile

An uplink profile is mapped in a Transport node on the NSX-T side with
policies for the links from the hypervisor hosts to NSX-T logical switches.


.. image::static/images/nsx_inventory_mapping/TN_Port_Profile.png
  :scale: 40%
  :align: center


The links from the Hypervisor hosts to NSX-T logical switches can comprise
of the LAG or Teaming configuration which must be tied to physical NICs.

NSX-API Request/Response to check the logical switch uplink
LAG profile attribute.

Request:

.. code-block:: text

  {
  port_channel_nodes {
  id label
  } id port_channel_policy_nodes {
  id label
  }
  }

Response:

.. code-block:: json

 {
  "data": {
    "port_channel_nodes": [
      {
        "id": "bd86666b-239d-4baa-8715-d73ca40d7100",
        "label": null
      },
      {
        "id": "ff5a5b6b-a103-471a-bbfd-ee3dc8c6e1c7",
        "label": null
      }
    ],
    "id": "rack-based-blueprint-9dfa0044",
    "port_channel_policy_nodes": [
      {
        "id": "59f60d47-ca48-441d-a4a4-e570af7bdb72",
        "label": "PTEST-LAG"
      }
    ]
  }
 }

Uplink profile label can also be matched with one retrieved from the GUI in
NSX-T Manager as below:

.. image::static/images/nsx_inventory_mapping/Uplink_Profile.png
  :scale: 40%
  :align: center


Please find below NSX-API Request/Response to check the LACP mode attribute
for the uplink LAG profile.

Request:

.. code-block:: text

  {
  port_channel_nodes {
  id
  } id port_channel_policy_nodes {
  id mode
  }
  }

Response:

.. code-block:: json

 {
   "data": {
    "port_channel_nodes": [
      {
        "id": "bd86666b-239d-4baa-8715-d73ca40d7100"
      },
      {
        "id": "ff5a5b6b-a103-471a-bbfd-ee3dc8c6e1c7"
      }
    ],
    "id": "rack-based-blueprint-9dfa0044",
    "port_channel_policy_nodes": [
      {
        "id": "59f60d47-ca48-441d-a4a4-e570af7bdb72",
        "mode": "active"
      }
    ]
  }
 }

.. image::static/images/nsx_inventory_mapping/LAG.png
  :scale: 40%
  :align: center


NSX-API Request/Response to check load balancing algorithm attribute of
host switch uplink profile.

Request:

.. code-block:: text

  {
  port_channel_nodes {
  id
  } id port_channel_policy_nodes {
  id hashing_algorithm
  }
  }

Response:

.. code-block:: json

 {
  "data": {
    "port_channel_nodes": [
      {
        "id": "bd86666b-239d-4baa-8715-d73ca40d7100"
      },
      {
        "id": "ff5a5b6b-a103-471a-bbfd-ee3dc8c6e1c7"
      }
    ],
    "id": "rack-based-blueprint-9dfa0044",
    "port_channel_policy_nodes": [
      {
        "id": "59f60d47-ca48-441d-a4a4-e570af7bdb72",
        "hashing_algorithm": "srcMac"
      }
    ]
  }
 }

From the LAG profile screenshot above it can be validated that it is using
Source MAC Address based load balancing algorithm.

**Vnet**
---------

* **Vn_type:**  Transport type attribute of transport zone
* **Label:** Display name attribute of logical switch
* **switch_label:** Switch name attribute of transport zone
* **Vlan:** Vlan attribute of logical switch for vlan transport zone
* **Vni:** vni attribute of logical switch for overlay transport zone

To obtain respective transport type attribute of the transport zone below query
can be used. This mainly signifies the type of traffic for a transport zone
which can be Overlay or VLAN type.



Request:

.. code-block:: text

  {
  vnet_nodes {
  id vn_type
  } id
  }


Response:

.. code-block:: json

    {
      "data": {
        "vnet_nodes": [
          {
            "id": "a3320cc6-601e-4a81-abe9-8464ae054f18",
            "vn_type": "overlay"
          },
          {
            "id": "6bdd7cd9-82eb-433d-8360-076d9daddd1b",
            "vn_type": "vlan"
          }
        ],
        "id": "rack-based-blueprint-9dfa0044"
      }
    }

Traffic type can also be identified in NSX-T Manager GUI as below:

.. image::static/images/nsx_inventory_mapping/TZ_traffic_type.png
  :scale: 40%
  :align: center


NSX-API Request/Response to check the display name  of the N-VDS logical
switch.

Request:

.. code-block:: text

  {
  vnet_nodes {
  id label
  } id
  }

Response:

.. code-block:: json

 {
  "data": {
    "vnet_nodes": [
      {
        "id": "241ce8e1-b31d-4093-a1a3-2f99a29ac2f9",
        "label": "mahi-nsxt-kvm-ls"
      },
      {
        "id": "fef41435-ac20-4c4d-81c0-b7f3059d977b",
        "label": "zz-cvx-nsxt.cvx.2485377892354-2902673742_1000"
      },
      {
        "id": "6bdd7cd9-82eb-433d-8360-076d9daddd1b",
        "label": "zz-cvx-nsxt.cvx.2485377892354-2902673742_VLAN-100-UPLINK-PROFILE-LAG"
      }
    ],
    "id": "rack-based-blueprint-9dfa0044"
  }
 }

Here as per API response above “zz-cvx-nsxt.cvx.2485377892354-2902673742_1000”
is the respective logical switch associated with the transport zone.

.. image::static/images/nsx_inventory_mapping/LS_TZ.png
  :scale: 40%
  :align: center


Please find below NSX-API Request/Response to check VLAN ID attribute of a VLAN
based logical switch for the transport zone.

Request:

.. code-block:: text

  {
  vnet_nodes {
   id vlan
   } id
   }

Response:

.. code-block:: json

 {
  "data": {
    "vnet_nodes": [
      {
        "id": "e0b29951-7739-4ecb-8c87-5725a61f669a",
        "vlan": 123
      },
      {
        "id": "cdd0c6d5-fecb-44d8-84c4-06c685e8ef14",
        "vlan": 2000
      },
      {
        "id": "fef41435-ac20-4c4d-81c0-b7f3059d977b",
        "vlan": 1000
      },
      {
        "id": "6bdd7cd9-82eb-433d-8360-076d9daddd1b",
        "vlan": 200
      }
    ],
    "id": "rack-based-blueprint-9dfa0044"
  }
 }


.. image::static/images/nsx_inventory_mapping/LS_VLAN_ID_AOS.png
  :scale: 40%
  :align: center


Here in AOS Fabric VNI IDs 1000 and 2000 represent such VXLAN Virtual network
for  east-west L2 stretched traffic. Bridge backed logical switch on NSX-T
should have the same VLAN IDs defined.


NSX-API Request/Response to check the VNI attribute of logical switch of NSX-T

Request:

.. code-block:: text

  {
  vnet_nodes {
   id vni
   } id
   }

Response:

.. code-block:: json

 {
  "data": {
    "vnet_nodes": [
      {
        "id": "a3320cc6-601e-4a81-abe9-8464ae054f18",
        "vni": 67595
      },
      {
        "id": "b7923224-659b-4075-b69b-3edeb5726a32",
        "vni": 67589
      },
      {
        "id": "18b81c81-8ae1-46b1-83ca-05cd5b364a1c",
        "vni": 67584
      }
    ],
    "id": "rack-based-blueprint-9dfa0044"
  }
 }
