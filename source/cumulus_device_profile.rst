======================
Cumulus Device Profile
======================

Background
----------

The Device Profiles (DP) are a way of allowing a new device to be recognized and
usable by AOS. They essentially capture much of the device specific semantics which
are required not only to be discovered by AOS but also run network configs that work
well for the datapath once inside the blueprint.

The Logical Device(LD) provides an abstracted view of a network device to be used
for a given intent. The user is able to make a logical representation of a device
including all information about how it can be used, including roles.

Interface Maps(IM) maps the physical ports as available in a DP to a Logical Device
to generate and run port configs on the device.

It is clear that the sanity of the DP is crucial to ensure successful config deploys
and effective network traffic passage.

The user can create, edit, delete, list DPs during the design phase of AOS. This DP
is then used in the creation of Interface Maps (IM), which get directly used inside
the AOS config rendering engine when the Blueprints are deployed.

This document tries to record all the necessary knowledge required to create (and
edit) a semantically correct Cumulus DP, so that not only does it pass the
validations in place in AOS which ensure the right DP is created in the database,
but also honors the vendor semantic requirement applicable to the device so that
it does not result in deploy failure when the generated configuration is pushed to
the network device.

Problem Statement
-----------------

A user who creates the DP needs to have the device specification (usually released
by the vendor). It does not stop there. The user also needs to learn a few things
about how to translate these specifications into AOS DP data model so as to create
the valid and config-friendly JSON. This is because the DP is a vendor semantics
aware data structure.

When there is a new device to be added to AOS, which is not already part of
production, one needs to go through the laborious process of first learning the
specifications and then tailoring it to the AOS semantics, which has been found to
be non-trivial without a guide which explains the purpose, meaning and semantic
requirements behind each and every DP data model field.

Solution
--------

This document will exhaustively iterate over each and every field in the DP and
record the way one should use it when creating a new DP.

The high level data model is the same for all DPs, i.e. there are the same keys for
every vendor's DP, just the way we get the values might differ, or might be loaded
with a vendor constraint.

The document will try and enlist the following:

* The schema of the DP and the nested elements inside the DP.
* The meaning of each key value pair in the schema.
* The vendor specific recipe the values are populated.
* List any constraints, corner cases which need to be considered, especially for
  port configurations for certain (group of) models.
* Any lessons learnt along the way creating those DPs already in production useful
  in creating future ones.

User Interface
--------------

The AOS UI which helps the user to create the DP, while it is intelligent to warn
the user against some semantic validations, is not wholly capable of ensuring deep
vendor specific constraints and requirements (See Inter port constraints section).
If the user can take care of having the exact vendor specification, then the AOS UI
will certainly help the user create a semantically valid DP which can be inserted
into AOS database successfully without any errors.

This document will enlist what it takes to get the vendor specifications and make
them part of AOS DP data model. Once the user has these ready, AOS UI can be used
to populate the fields and create the DP successfully.

Alternatively, the user can write his own Python code which takes into account the
vendor specifications, normalize it as per AOS DP data model and generate the JSON
to then import this into AOS UI.

DP Data Model
-------------

This section discusses the general DP data model in order to set the ground for
Cumulus specific details. Some of the Optional fields might not apply to Cumulus
and we will discuss those in more detail in subsequent sections.

Here is the front panel of the device Edgecore AS5712-54X as seen on the vendor's
`data specification <https://www.edge-core.com/productsInfo.php?cls=1&cls2=8&cls3=44&id=15>`_.
Some of the elements of the device are marked to highlight how they map in the AOS
device profile model.

.. image:: static/images/device_profiles/edgecore_as5712-54x.png
    :target: _images/edgecore_as5712-54x.png

.. csv-table::
  :file: device-profiles-cumulus1.csv
  :class: nowrap
  :header-rows: 1

Getting selector information from the device
````````````````````````````````````````````

Entering the correct information in all the 4 fields of the selector is critical
for the device to get matched to the device profile.

In this section we will describe how AOS gets each of this information in detail.
In the following table, mostly  column 4 is enough for us to get all the 4 fields.
Column 5 discusses a little more in detail how AOS gets these fields and processes
in Python. Reading Column 5 helps the user gain certainty, nevertheless it is not
necessary if one logs into the box and is able to read the file, type the commands
and manually human-read this simple information.

Reference: Please read
`this link <https://docs.cumulusnetworks.com/cumulus-linux/Monitoring-and-Troubleshooting/Monitoring-System-Hardware/>`_`
to understand the Cumulus TLV codes for the sys eeprom fields.

General Example output of a typical Cumulus device syseeprom information.

.. code-block:: prompt

    admin@leaf-1-1:mgmt-vrf:~$ decode-syseeprom
    TlvInfo Header:
       Id String:    TlvInfo
       Version:      1
       Total Length: 168
    TLV Name             Code Len Value
    -------------------- ---- --- -----
    Manufacture Date     0x25  19 10/23/2015 23:11:35
    Diag Version         0x2E   7 2.0.1.4
    Label Revision       0x27   4 R01I
    Platform Name        0x28  27 x86_64-accton_as5712_54x-r0
    ONIE Version         0x29  13 2014.08.00.05
    Manufacturer         0x2B   6 Accton
    Manufacture Country  0x2C   2 TW
    Base MAC Address     0x24   6 CC:37:AB:62:F3:4B
    Serial Number        0x23  14 571254X1541008
    Part Number          0x22  13 FP1ZZ5654001A
    Product Name         0x21  15 5712-54X-O-AC-F
    MAC Addresses        0x2A   2 74
    Vendor Name          0x2D   8 Edgecore
    CRC-32               0xFE   4 0x95E4178F
    (checksum valid)
    admin@leaf-1-1:mgmt-vrf:~$

Now, let's try and apply these to an example in AOS, let's pick Accton 5712-54X.

.. csv-table::
  :file: device-profiles-cumulus2.csv
  :class: nowrap
  :header-rows: 1

Model and manufacturer/vendor
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block:: prompt

    admin@leaf-1-1:mgmt-vrf:~$ decode-syseeprom
    TlvInfo Header:
       Id String:    TlvInfo
       Version:      1
       Total Length: 168
    TLV Name             Code Len Value
    -------------------- ---- --- -----
    Manufacture Date     0x25  19 10/23/2015 23:11:35
    Diag Version         0x2E   7 2.0.1.4
    Label Revision       0x27   4 R01I
    Platform Name        0x28  27 x86_64-accton_as5712_54x-r0
    ONIE Version         0x29  13 2014.08.00.05
    Manufacturer         0x2B   6 Accton
    Manufacture Country  0x2C   2 TW
    Base MAC Address     0x24   6 CC:37:AB:62:F3:4B
    Serial Number        0x23  14 571254X1541008
    Part Number          0x22  13 FP1ZZ5654001A
    Product Name         0x21  15 5712-54X-O-AC-F
    MAC Addresses        0x2A   2 74
    Vendor Name          0x2D   8 Edgecore
    CRC-32               0xFE   4 0x95E4178F
    (checksum valid)
    admin@leaf-1-1:mgmt-vrf:~$

OS Version
^^^^^^^^^^

.. code-block:: prompt

    admin@leaf-1-1:mgmt-vrf:~$ cat /etc/lsb-release
    DISTRIB_ID="Cumulus Linux"
    DISTRIB_RELEASE=3.7.3
    DISTRIB_DESCRIPTION="Cumulus Linux 3.7.3"
    admin@leaf-1-1:mgmt-vrf:~$

OS Family
^^^^^^^^^

.. code-block:: prompt

    admin@leaf-1-1:mgmt-vrf:~$ cat /etc/os-release
    NAME="Cumulus Linux"
    VERSION_ID=3.7.3
    VERSION="Cumulus Linux 3.7.3"
    PRETTY_NAME="Cumulus Linux"
    ID=cumulus-linux
    ID_LIKE=debian
    CPE_NAME=cpe:/o:cumulusnetworks:cumulus_linux:3.7.3
    HOME_URL="http://www.cumulusnetworks.com/"
    SUPPORT_URL="http://support.cumulusnetworks.com/"
    admin@leaf-1-1:mgmt-vrf:~$

Using the above gotten information to Accton 5712 54X’s selector as displayed
in UI:

.. image:: static/images/device_profiles/device-profiles-cumulus2.png

Some common default values as used in AOS
`````````````````````````````````````````

What makes the AOS SDK generators fast tools when it comes to generating the JSONs
is using some of the knowledge specific to a vendor and re-using them in Python code
to generate more than one DP.

If you look at the list of key value pairs that make the DP, there are a lot!
However, on careful observation, we have found some commonalities across models
belonging to the same vendor. We call these defaults. If one can confirm if it is
safe to use this default value for a particular key, then it saves some effort in
the overall process.

.. note::

    While every field in the DP should be populated per device carefully
    consulting the specification, one can also use these defaults as a reference
    when populating the fields.

.. csv-table::
  :file: device-profiles-cumulus3.csv
  :class: nowrap
  :header-rows: 1

Capabilities
------------

The hardware and software capabilities are relatively straight forward, if the user
has access to the device specification, it is easy to fetch these fields.

The software capabilities for Cumulus, lxc and onie is mostly set to true.

Here are some commonly found values on Cumulus devices (this data is based on the
devices AOS already supports):

.. csv-table::
  :file: device-profiles-cumulus4.csv
  :class: nowrap
  :header-rows: 1

.. note::

    AOS does not require the user to configure any extra configurables for
    "supported features" for Cumulus device profiles as of AOS 3.1.1. Thus the UI
    says "no supported features".

Port specific semantics
-----------------------

Every interface per transformation per port per device profile, requires a setting.

This port setting is used by the configuration renderer(jinja files) to correctly
configure the interfaces on the device. The DP allows the user to configure these
for the device. Thus, it is very important these setting fields are populated
correctly in the DP. In this section, we will look at what is the schema AOS
enforces for Cumulus port settings.

Example port setting:

.. code-block:: json

    {
      "interface": {
        "speed": "10000",
        "lane_map": "13"
      }
    }

Where is this field found: If you have 32 ports, each port capable of 40G and
4x10G, you will have:

* 2 transformations per port
* 1 active interface in 1st transformation; setting can be found in each of these
  interfaces
* 4 active interfaces in the 2nd transformation; setting can be found in each of
  these interfaces

Interface Name semantics on Cumulus
```````````````````````````````````

The rules for naming an interface, as of AOS 3.1.1, are as follows:

- Arguments: port_id(required), lane_id(optional, applies only in case of a broken
  out port)

Example of an interface name on Cumulus, **swp49s0**.
First get the lane_suffix:

Example: **s0**

If lane_id:
  Lane_suffix = "s%d" % lane_id
Else:
  Lane_suffix = ""

Then build the name, using port id and lane_suffix
Name = "*'swp%d%s' % (port_id, lane_suffix)*"

Examples:
Interfaces for 4x10G transformation on port id 49:
- **swp49s0**
- **swp49s1**
- **swp49s2**
- **swp49s3**
Interfaces for 40G transformation on port id 49:
- **swp49**

Port setting schema on Cumulus
``````````````````````````````

.. csv-table::
  :file: device-profiles-cumulus5.csv
  :class: nowrap
  :header-rows: 1

Editing interface setting per interface on UI
`````````````````````````````````````````````

The setting contains two important pieces of information, the command and the speed.

The "command" goes underneath the interface paragraph in /etc/network/interfaces,
and "speed" is the value that goes to ports.conf.

- I know my port breakout capability
- I am ready to create ports in AOS UI
- I am in the DP->edit->ports->port
- How do I populate the Setting?

.. image:: static/images/device_profiles/device-profiles-cumulus3.png

.. csv-table::
  :file: device-profiles-cumulus6.csv
  :class: nowrap
  :header-rows: 1

Auto-negotiation for 10GBaseT ports
```````````````````````````````````

On the 10GBaseT, Cumulus supports auto negotiation for some devices.

To incorporate this into the DP, on that port which supports the 10G->1x1G breakout,
for the 1G transformation, have the interface setting as follows:

.. code-block:: json

    {
      "interface": {
        "command": "link-speed 1000",
        "speed": ""
      }
    }

Note the "speed" inside the "interface" setting is set to "". This allows the
interface to be configured in auto-neg mode and device will auto-negotiate the
interface speed.

In the IM, pick the auto-neg transformation for this port.

How to make the interface as auto neg in the DP:

#. Go to the 10GBaseT port

#. Create a 1G transformation as follows:

    - state: active
    - speed: max speed possible for interface speed
    - setting->interface->speed: empty
    - name: as per interface name rules

For example, on the Dell S4148T-ON, the ports from 1-24 and 31-54 are 10GBaseT ports.
Thus, Transformation #2, configures the speed in the interface setting as "".

.. image:: static/images/device_profiles/device-profiles-cumulus1.png

Inter port constraints
----------------------

To capture all the essence of ports of a particular device, not only does the user
need to read the device specification, but also the OS specific port rules.
Fortunately, Cumulus packages these rules in the ports.conf in the dpkg bundle.

When we are writing device profiles for new Cumulus devices, the procedure ideally
to be followed is:

#. To generate a list of ports(with correct transformations and interfaces) in the
   DP, a good way is to have access to the ports.conf and work our way backwards
   to then generate the ports list in the DP such that the Cumulus OS likes it.
   To know the detailed port constraints, read the ports.conf of that particular
   model in the dpkg.

#. run the following command on any Cumulus box(even VX) to get access to ports.conf:
   ``dpkg -L cumulus-platform  | grep ports.conf``

    Pay attention to the comments sections which lists the port breakout constraints
    usually. It looks something like this:

    ports.conf --

    This file controls port aggregation and subdivision.  For example, QSFP+
    ports are typically configurable as either one 40G interface or four
    10G/1000/100 interfaces.  This file sets the number of interfaces per port
    while /etc/network/interfaces and ethtool configure the link speed for each
    interface.

    You must restart switchd for changes to take effect.

    The DELL S6010 has:

        32 QSFP ports numbered 1-32
        These ports are configurable as 40G, split into 4x10G ports or
        disabled.

        The X pipeline covers QSFP ports 1 through 16 and the Y pipeline
        covers QSFP ports 17 through 32.

        The Trident2+ chip can only handle 52 logical ports per pipeline.

        This means 13 is the maximum number of 40G ports you can ungang
        per pipeline, with the remaining three 40G ports set to
        "disabled". The 13 40G ports become 52 unganged 10G ports, which
        totals 52 logical ports for that pipeline.

    QSFP+ ports
    <port label 1-32> = [4x10G|40G|disabled]

#. Use the comments when writing a new Cumulus DP.

Dell Z9100-ON
`````````````

Port Constraint:

The Dell Z9100 has:
  32 QSFP28 ports numbered 1-32
    These ports are configurable as 100G, 50G, 40G, 2x50G, 4x25G, 4x10G
    or disabled.
  Two SFP+ ports. These ports are configurable as 10G or disabled.
    The system can only handle 128 logical ports.
    This means that if all 32 QSFP28 ports are broken out into
    4x25G or 4x10G mode, the two 10G ports (33 and 34) must be
    set to "disabled".

To honor this, in the DP, the ports 33-34 should provide a transformation with the
interface with disabled setting.

.. code-block:: json

    {
      "interface": {
        "command": "",
        "speed": "disabled"
      }
    }

IM constraint:

If all the 32 QSFP28 ports are broken out into 4x25G or 4x10G, then the two 33-34
need to be disabled. So explicitly pick the disabled transformation for 33-34 in
this case.

Dell 4128F
``````````

Port Constraint:

The Dell S4128 has:
  28 SFP+ ports numbered 1-24 and 27-30
    These ports are configurable as 10G, or groups of four adjacent
    ports can be configured as 40G.
  2 QSFP28 ports numbered 25-26
    These ports are configurable as 100G, 50G, 40G, or split into
    2x50G, 4x25G, or 4x10G ports.

.. note::

    SFP+ ports 13, 14, 23, and 24 are not in a group of four adjacent ports and
    cannot be configured as part of a ganged 40G.

AOS does not support the configuration of ganged ports.

For the 1G broken out interface, the speed inside the interface setting should be
"1G" and not "10G" (which usually is for other cumulus devices)

To honor this, in the DP, the ports 33-34 should provide a transformation with the
interface with disabled setting.

.. code-block:: json

    {
      "interface": {
        "command": "",
        "speed": "disabled"
      }
    }

IM constraint:

If all the 32 QSFP28 ports are broken out into 4x25G or 4x10G, then the two 33-34
need to be disabled. So explicitly pick the disabled transformation for 33-34 in
this case.

Dell 4148T
``````````

Port Constraint:

The Dell S4148T has:
  48 10GT ports numbered 1-24 and 31-54
    These ports are not configurable.
  2 QSFP+ ports numbered 27-28
    These ports are configurable as 40G or split into 4x10G ports.
  4 QSFP28 ports numbered 25-26 and 29-30
    These ports are configurable as 100G, 50G, 40G, or split into
    2x50G, 4x25G, or 4x10G ports.

.. note::

    The two QSFP+ ports are DISABLED by default, and the four QSFP28 ports
    are configured for 100G by default.  In order to enable the two QSFP+
    ports for 40G or 4x10G, all four QSFP28 ports must also be configured
    for either 40G or 4x10G.

In the DP, to honor this constraint we need a transformation with "disabled" setting
as follows for the ports 27-28

.. code-block:: json

    {
      "interface": {
        "command": "",
        "speed": "disabled"
      }
    }

IM constraint:

In order to enable the two QSFP+ ports(27-28) for 40G or 4x10G, all four QSFP28
ports must also be configured for either 40G or 4x10G.

Dell 4148F
``````````

Port Constraint:

The Dell S4148 has:
  48 SFP+ ports numbered 1-24 and 31-54
    These ports are configurable as 10G, or groups of four adjacent
    ports can be configured as 40G.
  2 QSFP+ ports numbered 27-28
    These ports are configurable as 40G or split into 4x10G ports.
  4 QSFP28 ports numbered 25-26 and 29-30
    These ports are configurable as 100G, 50G, 40G, or split into
    2x50G, 4x25G, or 4x10G ports.

.. note::

    The two QSFP+ ports are DISABLED by default, and the four QSFP28 ports
    are configured for 100G by default.  In order to enable the two QSFP+
    ports for 40G or 4x10G, all four QSFP28 ports must also be configured
    for either 40G or 4x10G.

The 2 QSFP+ and 4 QSFP28 have same rules as Dell 4148T.
The 48 SFP+ ports have same 1G setting constraints as Dell 4128F, i.e.

.. code-block:: json

    {
      "interface": {
        "command": "link-speed 1000",
        "speed": "10G"
      }
    }

Edgecore 6812_32x_O
```````````````````

Port Constraint:

This file controls port aggregation and subdivision. For example, QSFP+
ports are typically configurable as either one 40G interface or four
10G/1000/100 interfaces.  This file sets the number of interfaces per port
while /etc/network/interfaces and ethtool configure the link speed for each
interface.

You must restart switchd for changes to take effect.

Accton AS6812_32X has:
  32 QSFP+ ports numbered 1-32
    These ports are configurable as 40G, split into 4x10G ports or
    disabled.

  The X pipeline covers QSFP ports 1-16 and the Y pipeline
  covers QSFP ports 17-32.

  The Trident2+ chip can only handle 52 logical ports per pipeline.

  This means 13 is the maximum number of 40G ports you can ungang
  per pipeline, with the remaining three 40G ports set to
  "disabled". The 13 40G ports become 52 unganged 10G ports, which
  totals 52 logical ports for that pipeline.

To honor this constraint, in the DP, all the 40g->4x10G ports need to have a
transformation with an interface with diaabled setting as follows:

.. code-block:: json

    {
      "interface": {
        "command": "",
        "speed": "disabled"
      }
    }

IM Constraint:

When picking 10G interfaces, make sure there are only a maximum of 52 10G
interfaces (coming from the 13 ports) for this IM. Also for the rest of the 3
leftover 40G ports, explicitly select the "disabled" transformation.

This way, the port config will be generated correctly as per port constraints.

Mellanox 2700
`````````````

Port Constraint:

Odd numbered ports can do full breakout up to 4x10G and 4x25G.

Even numbered ports can only have upto 2x50G interfaces and should be disabled if
immediately preceding odd-numbered port is broken out into 4 interfaces. (Source:
https://docs.mellanox.com/display/sn2000pub/Cable+Installation)

.. image:: static/images/device_profiles/mellanox_msn2700_port_constraints.png

How does this translate in the DP?

The odd numbered ports, like port 1, have following transformations:

+------------------------+-----------------------------+
| #1 (100 Gbps, default) | swp1                        |
+------------------------+-----------------------------+
| #2 (50 Gbps)           | swp1s0 swp1s1               |
+------------------------+-----------------------------+
| #3 (50 Gbps)           | swp1                        |
+------------------------+-----------------------------+
| #4 (40 Gbps)           | swp1                        |
+------------------------+-----------------------------+
| #5 (25 Gbps)           | swp1s0 swp1s1 swp1s2 swp1s3 |
+------------------------+-----------------------------+
| #6 (10 Gbps)           | swp1s0 swp1s1 swp1s2 swp1s3 |
+------------------------+-----------------------------+
| #7 (1 Gbps)            | swp1s0 swp1s1 swp1s2 swp1s3 |
+------------------------+-----------------------------+

.. image:: static/images/device_profiles/mellanox_msn2700_port_constraints1.png

Whereas the even numbered ports, like port 2, have following transformations.

+------------------------+-----------------------------+
| #1 (100 Gbps, default) | swp2                        |
+------------------------+-----------------------------+
| #2 (50 Gbps)           | swp2s0 swp2s1               |
+------------------------+-----------------------------+
| #3 (50 Gbps)           | swp2                        |
+------------------------+-----------------------------+
| #4 (40 Gbps)           | swp2                        |
+------------------------+-----------------------------+
| #5 (100 Gbps)          | swp2                        |
+------------------------+-----------------------------+

Note that even ports do not have the 10G and 25G transformations. Additionally,
they have a "disabled" interface in transformation 5 with the following setting:

.. code-block:: json

    {
      "interface": {
        "command": "",
        "speed": "disabled"
      }
    }

.. image:: static/images/device_profiles/mellanox_msn2700_port_constraints2.png

IM constraints:

If you want to include an odd numbered port which is broken out into 4 interfaces,
then make sure you also pick the disabled transformation of the following even
numbered port.

For example in the Interface Map Mellanox_MSN2700_Cumulus__AOS-48x10_8x100-1

Because we are using the ports: [1,3,5,7,9,11,13,15,17,19,21,23] to generate the
4x10G interfaces, we also pick the 5th transformation(disabled) for ports
[2,4,6,8,10,12,14,16,18,20,22,24]. Thus on the UI, for the Interface Map, you see
12 unused interfaces corresponding to these disabled interfaces.

Only if these are included in the Interface Map, will AOS generate the right
ports.conf honoring the above Cumulus Mellanox 2700 constraint. Refer to the use
case :ref:`Handling Inter Port Constraints - Disabled Ports <im_4_disabled-dp-ports>`
for more details on Interface Map creation.

Debugging and Recovery
----------------------

Device mismatch

Usually this is seen at the very beginning of device’s lifecycle. If a device profile
is not being picked by the device, then it is advisable to check the 4 fields as
entered in the selector section of the DP.

Deploy errors

One of the reasons deploy errors are seen are because of incorrect port
configurations. This could be either the ports were configured with incorrect
speeds, or the OS specific port constraints were not handled in the DP or in the IM.

A possible flow for root cause would be:

#. Check the DP for obvious port capabilities errors. Is the port really capable
   of the speeds the DP has configured. The /var/log/switchd logs on the device
   are a good resource to parse for ERROR messages.

#. Check if the DP has configured autoneg or disabled interfaces correctly. Autoneg
   and disabled can both be expressed in the interface setting field.

#. The port constraints can be read from the following files:
   ``admin@leaf1:mgmt-vrf:~$ dpkg -L cumulus-platform  | grep ports.conf``

Appendix
--------

Mapping the DP to LD in the IM

The mapping specifies how exactly the user wants to map the physical ports to the
logical interfaces.

The 5 fields on the mapping are: [DP portID, DP transformationID, DP InterfaceID,
LD panelID, LD portID].

These five fields allow the user to map the DP exactly to the LD. The first three
fields specify which port, transformation and interface in the DP is the interface
in the IM referencing:

#. (port id, transformation id) - all active interfaces in the given transformation
   are used; active interfaces are mapped in the order they appear in the
   transformation.

#. (port id, transformation id, interface id) - If the user specifies the third
   field in the mapping, the particular interface in the transformation is used in
   mapping.

Example for DP JSON for Edgecore / Accton AS5712-54X
----------------------------------------------------

.. image:: static/images/device_profiles/edgecore_as5712-54x.png
    :target: _images/edgecore_as5712-54x.png

.. code-block:: json

    {
      "hardware_capabilities": {
        "userland": 64,
        "ram": 16,
        "asic": "T2",
        "form_factor": "1RU",
        "ecmp_limit": 64,
        "cpu": "x86"
      },
      "created_at": "2019-08-30T05:53:38.369782Z",
      "last_modified_at": "2019-08-30T05:53:38.369782Z",
      "selector": {
        "os_version": "(3\\.[5-7]\\.\\d+)",
        "model": "5712-54X-O.*",
        "os": "Cumulus",
        "manufacturer": "Accton"
      },
      "software_capabilities": {
        "onie": true,
        "lxc_support": true
      },
      "id": "Accton_5712-54X-O_Cumulus",
      "slot_count": 0,
      "label": "Accton 5712-54X-O",
      "ports": [
        {
          "panel_id": 1,
          "slot_id": 0,
          "connector_type": "sfp",
          "row_id": 1,
          "transformations": [
            {
              "is_default": false,
              "interfaces": [
                {
                  "state": "active",
                  "setting": "{\"interface\": {\"command\": \"\", \"speed\": \"10G\"}}",
                  "speed": {
                    "unit": "G",
                    "value": 10
                  },
                  "name": "swp1",
                  "interface_id": 1
                }
              ],
              "transformation_id": 1
            },
            {
              "is_default": true,
              "interfaces": [
                {
                  "state": "active",
                  "setting": "{\"interface\": {\"command\": \"link-speed 1000\", \"speed\": \"10G\"}}",
                  "speed": {
                    "unit": "G",
                    "value": 1
                  },
                  "name": "swp1",
                  "interface_id": 1
                }
              ],
              "transformation_id": 2
            }
          ],
          "port_id": 1,
          "failure_domain_id": 1,
          "column_id": 1
        },
        {
          "panel_id": 1,
          "slot_id": 0,
          "connector_type": "sfp",
          "row_id": 2,
          "transformations": [
            {
              "is_default": false,
              "interfaces": [
                {
                  "state": "active",
                  "setting": "{\"interface\": {\"command\": \"\", \"speed\": \"10G\"}}",
                  "speed": {
                    "unit": "G",
                    "value": 10
                  },
                  "name": "swp2",
                  "interface_id": 1
                }
              ],
              "transformation_id": 1
            },
            {
              "is_default": true,
              "interfaces": [
                {
                  "state": "active",
                  "setting": "{\"interface\": {\"command\": \"link-speed 1000\", \"speed\": \"10G\"}}",
                  "speed": {
                    "unit": "G",
                    "value": 1
                  },
                  "name": "swp2",
                  "interface_id": 1
                }
              ],
              "transformation_id": 2
            }
          ],
          "port_id": 2,
          "failure_domain_id": 1,
          "column_id": 1
        },
        {
          "panel_id": 1,
          "slot_id": 0,
          "connector_type": "sfp",
          "row_id": 1,
          "transformations": [
            {
              "is_default": false,
              "interfaces": [
                {
                  "state": "active",
                  "setting": "{\"interface\": {\"command\": \"\", \"speed\": \"10G\"}}",
                  "speed": {
                    "unit": "G",
                    "value": 10
                  },
                  "name": "swp3",
                  "interface_id": 1
                }
              ],
              "transformation_id": 1
            },
            {
              "is_default": true,
              "interfaces": [
                {
                  "state": "active",
                  "setting": "{\"interface\": {\"command\": \"link-speed 1000\", \"speed\": \"10G\"}}",
                  "speed": {
                    "unit": "G",
                    "value": 1
                  },
                  "name": "swp3",
                  "interface_id": 1
                }
              ],
              "transformation_id": 2
            }
          ],
          "port_id": 3,
          "failure_domain_id": 1,
          "column_id": 2
        },
        {
          "panel_id": 1,
          "slot_id": 0,
          "connector_type": "sfp",
          "row_id": 2,
          "transformations": [
            {
              "is_default": false,
              "interfaces": [
                {
                  "state": "active",
                  "setting": "{\"interface\": {\"command\": \"\", \"speed\": \"10G\"}}",
                  "speed": {
                    "unit": "G",
                    "value": 10
                  },
                  "name": "swp4",
                  "interface_id": 1
                }
              ],
              "transformation_id": 1
            },
            {
              "is_default": true,
              "interfaces": [
                {
                  "state": "active",
                  "setting": "{\"interface\": {\"command\": \"link-speed 1000\", \"speed\": \"10G\"}}",
                  "speed": {
                    "unit": "G",
                    "value": 1
                  },
                  "name": "swp4",
                  "interface_id": 1
                }
              ],
              "transformation_id": 2
            }
          ],
          "port_id": 4,
          "failure_domain_id": 1,
          "column_id": 2
        },
        {
          "panel_id": 1,
          "slot_id": 0,
          "connector_type": "sfp",
          "row_id": 1,
          "transformations": [
            {
              "is_default": false,
              "interfaces": [
                {
                  "state": "active",
                  "setting": "{\"interface\": {\"command\": \"\", \"speed\": \"10G\"}}",
                  "speed": {
                    "unit": "G",
                    "value": 10
                  },
                  "name": "swp5",
                  "interface_id": 1
                }
              ],
              "transformation_id": 1
            },
            {
              "is_default": true,
              "interfaces": [
                {
                  "state": "active",
                  "setting": "{\"interface\": {\"command\": \"link-speed 1000\", \"speed\": \"10G\"}}",
                  "speed": {
                    "unit": "G",
                    "value": 1
                  },
                  "name": "swp5",
                  "interface_id": 1
                }
              ],
              "transformation_id": 2
            }
          ],
          "port_id": 5,
          "failure_domain_id": 1,
          "column_id": 3
        },
        {
          "panel_id": 1,
          "slot_id": 0,
          "connector_type": "sfp",
          "row_id": 2,
          "transformations": [
            {
              "is_default": false,
              "interfaces": [
                {
                  "state": "active",
                  "setting": "{\"interface\": {\"command\": \"\", \"speed\": \"10G\"}}",
                  "speed": {
                    "unit": "G",
                    "value": 10
                  },
                  "name": "swp6",
                  "interface_id": 1
                }
              ],
              "transformation_id": 1
            },
            {
              "is_default": true,
              "interfaces": [
                {
                  "state": "active",
                  "setting": "{\"interface\": {\"command\": \"link-speed 1000\", \"speed\": \"10G\"}}",
                  "speed": {
                    "unit": "G",
                    "value": 1
                  },
                  "name": "swp6",
                  "interface_id": 1
                }
              ],
              "transformation_id": 2
            }
          ],
          "port_id": 6,
          "failure_domain_id": 1,
          "column_id": 3
        },
        {
          "panel_id": 1,
          "slot_id": 0,
          "connector_type": "sfp",
          "row_id": 1,
          "transformations": [
            {
              "is_default": false,
              "interfaces": [
                {
                  "state": "active",
                  "setting": "{\"interface\": {\"command\": \"\", \"speed\": \"10G\"}}",
                  "speed": {
                    "unit": "G",
                    "value": 10
                  },
                  "name": "swp7",
                  "interface_id": 1
                }
              ],
              "transformation_id": 1
            },
            {
              "is_default": true,
              "interfaces": [
                {
                  "state": "active",
                  "setting": "{\"interface\": {\"command\": \"link-speed 1000\", \"speed\": \"10G\"}}",
                  "speed": {
                    "unit": "G",
                    "value": 1
                  },
                  "name": "swp7",
                  "interface_id": 1
                }
              ],
              "transformation_id": 2
            }
          ],
          "port_id": 7,
          "failure_domain_id": 1,
          "column_id": 4
        },
        {
          "panel_id": 1,
          "slot_id": 0,
          "connector_type": "sfp",
          "row_id": 2,
          "transformations": [
            {
              "is_default": false,
              "interfaces": [
                {
                  "state": "active",
                  "setting": "{\"interface\": {\"command\": \"\", \"speed\": \"10G\"}}",
                  "speed": {
                    "unit": "G",
                    "value": 10
                  },
                  "name": "swp8",
                  "interface_id": 1
                }
              ],
              "transformation_id": 1
            },
            {
              "is_default": true,
              "interfaces": [
                {
                  "state": "active",
                  "setting": "{\"interface\": {\"command\": \"link-speed 1000\", \"speed\": \"10G\"}}",
                  "speed": {
                    "unit": "G",
                    "value": 1
                  },
                  "name": "swp8",
                  "interface_id": 1
                }
              ],
              "transformation_id": 2
            }
          ],
          "port_id": 8,
          "failure_domain_id": 1,
          "column_id": 4
        },
        {
          "panel_id": 1,
          "slot_id": 0,
          "connector_type": "sfp",
          "row_id": 1,
          "transformations": [
            {
              "is_default": false,
              "interfaces": [
                {
                  "state": "active",
                  "setting": "{\"interface\": {\"command\": \"\", \"speed\": \"10G\"}}",
                  "speed": {
                    "unit": "G",
                    "value": 10
                  },
                  "name": "swp9",
                  "interface_id": 1
                }
              ],
              "transformation_id": 1
            },
            {
              "is_default": true,
              "interfaces": [
                {
                  "state": "active",
                  "setting": "{\"interface\": {\"command\": \"link-speed 1000\", \"speed\": \"10G\"}}",
                  "speed": {
                    "unit": "G",
                    "value": 1
                  },
                  "name": "swp9",
                  "interface_id": 1
                }
              ],
              "transformation_id": 2
            }
          ],
          "port_id": 9,
          "failure_domain_id": 1,
          "column_id": 5
        },
        {
          "panel_id": 1,
          "slot_id": 0,
          "connector_type": "sfp",
          "row_id": 2,
          "transformations": [
            {
              "is_default": false,
              "interfaces": [
                {
                  "state": "active",
                  "setting": "{\"interface\": {\"command\": \"\", \"speed\": \"10G\"}}",
                  "speed": {
                    "unit": "G",
                    "value": 10
                  },
                  "name": "swp10",
                  "interface_id": 1
                }
              ],
              "transformation_id": 1
            },
            {
              "is_default": true,
              "interfaces": [
                {
                  "state": "active",
                  "setting": "{\"interface\": {\"command\": \"link-speed 1000\", \"speed\": \"10G\"}}",
                  "speed": {
                    "unit": "G",
                    "value": 1
                  },
                  "name": "swp10",
                  "interface_id": 1
                }
              ],
              "transformation_id": 2
            }
          ],
          "port_id": 10,
          "failure_domain_id": 1,
          "column_id": 5
        },
        {
          "panel_id": 1,
          "slot_id": 0,
          "connector_type": "sfp",
          "row_id": 1,
          "transformations": [
            {
              "is_default": false,
              "interfaces": [
                {
                  "state": "active",
                  "setting": "{\"interface\": {\"command\": \"\", \"speed\": \"10G\"}}",
                  "speed": {
                    "unit": "G",
                    "value": 10
                  },
                  "name": "swp11",
                  "interface_id": 1
                }
              ],
              "transformation_id": 1
            },
            {
              "is_default": true,
              "interfaces": [
                {
                  "state": "active",
                  "setting": "{\"interface\": {\"command\": \"link-speed 1000\", \"speed\": \"10G\"}}",
                  "speed": {
                    "unit": "G",
                    "value": 1
                  },
                  "name": "swp11",
                  "interface_id": 1
                }
              ],
              "transformation_id": 2
            }
          ],
          "port_id": 11,
          "failure_domain_id": 1,
          "column_id": 6
        },
        {
          "panel_id": 1,
          "slot_id": 0,
          "connector_type": "sfp",
          "row_id": 2,
          "transformations": [
            {
              "is_default": false,
              "interfaces": [
                {
                  "state": "active",
                  "setting": "{\"interface\": {\"command\": \"\", \"speed\": \"10G\"}}",
                  "speed": {
                    "unit": "G",
                    "value": 10
                  },
                  "name": "swp12",
                  "interface_id": 1
                }
              ],
              "transformation_id": 1
            },
            {
              "is_default": true,
              "interfaces": [
                {
                  "state": "active",
                  "setting": "{\"interface\": {\"command\": \"link-speed 1000\", \"speed\": \"10G\"}}",
                  "speed": {
                    "unit": "G",
                    "value": 1
                  },
                  "name": "swp12",
                  "interface_id": 1
                }
              ],
              "transformation_id": 2
            }
          ],
          "port_id": 12,
          "failure_domain_id": 1,
          "column_id": 6
        },
        {
          "panel_id": 1,
          "slot_id": 0,
          "connector_type": "sfp",
          "row_id": 1,
          "transformations": [
            {
              "is_default": false,
              "interfaces": [
                {
                  "state": "active",
                  "setting": "{\"interface\": {\"command\": \"\", \"speed\": \"10G\"}}",
                  "speed": {
                    "unit": "G",
                    "value": 10
                  },
                  "name": "swp13",
                  "interface_id": 1
                }
              ],
              "transformation_id": 1
            },
            {
              "is_default": true,
              "interfaces": [
                {
                  "state": "active",
                  "setting": "{\"interface\": {\"command\": \"link-speed 1000\", \"speed\": \"10G\"}}",
                  "speed": {
                    "unit": "G",
                    "value": 1
                  },
                  "name": "swp13",
                  "interface_id": 1
                }
              ],
              "transformation_id": 2
            }
          ],
          "port_id": 13,
          "failure_domain_id": 1,
          "column_id": 7
        },
        {
          "panel_id": 1,
          "slot_id": 0,
          "connector_type": "sfp",
          "row_id": 2,
          "transformations": [
            {
              "is_default": false,
              "interfaces": [
                {
                  "state": "active",
                  "setting": "{\"interface\": {\"command\": \"\", \"speed\": \"10G\"}}",
                  "speed": {
                    "unit": "G",
                    "value": 10
                  },
                  "name": "swp14",
                  "interface_id": 1
                }
              ],
              "transformation_id": 1
            },
            {
              "is_default": true,
              "interfaces": [
                {
                  "state": "active",
                  "setting": "{\"interface\": {\"command\": \"link-speed 1000\", \"speed\": \"10G\"}}",
                  "speed": {
                    "unit": "G",
                    "value": 1
                  },
                  "name": "swp14",
                  "interface_id": 1
                }
              ],
              "transformation_id": 2
            }
          ],
          "port_id": 14,
          "failure_domain_id": 1,
          "column_id": 7
        },
        {
          "panel_id": 1,
          "slot_id": 0,
          "connector_type": "sfp",
          "row_id": 1,
          "transformations": [
            {
              "is_default": false,
              "interfaces": [
                {
                  "state": "active",
                  "setting": "{\"interface\": {\"command\": \"\", \"speed\": \"10G\"}}",
                  "speed": {
                    "unit": "G",
                    "value": 10
                  },
                  "name": "swp15",
                  "interface_id": 1
                }
              ],
              "transformation_id": 1
            },
            {
              "is_default": true,
              "interfaces": [
                {
                  "state": "active",
                  "setting": "{\"interface\": {\"command\": \"link-speed 1000\", \"speed\": \"10G\"}}",
                  "speed": {
                    "unit": "G",
                    "value": 1
                  },
                  "name": "swp15",
                  "interface_id": 1
                }
              ],
              "transformation_id": 2
            }
          ],
          "port_id": 15,
          "failure_domain_id": 1,
          "column_id": 8
        },
        {
          "panel_id": 1,
          "slot_id": 0,
          "connector_type": "sfp",
          "row_id": 2,
          "transformations": [
            {
              "is_default": false,
              "interfaces": [
                {
                  "state": "active",
                  "setting": "{\"interface\": {\"command\": \"\", \"speed\": \"10G\"}}",
                  "speed": {
                    "unit": "G",
                    "value": 10
                  },
                  "name": "swp16",
                  "interface_id": 1
                }
              ],
              "transformation_id": 1
            },
            {
              "is_default": true,
              "interfaces": [
                {
                  "state": "active",
                  "setting": "{\"interface\": {\"command\": \"link-speed 1000\", \"speed\": \"10G\"}}",
                  "speed": {
                    "unit": "G",
                    "value": 1
                  },
                  "name": "swp16",
                  "interface_id": 1
                }
              ],
              "transformation_id": 2
            }
          ],
          "port_id": 16,
          "failure_domain_id": 1,
          "column_id": 8
        },
        {
          "panel_id": 1,
          "slot_id": 0,
          "connector_type": "sfp",
          "row_id": 1,
          "transformations": [
            {
              "is_default": false,
              "interfaces": [
                {
                  "state": "active",
                  "setting": "{\"interface\": {\"command\": \"\", \"speed\": \"10G\"}}",
                  "speed": {
                    "unit": "G",
                    "value": 10
                  },
                  "name": "swp17",
                  "interface_id": 1
                }
              ],
              "transformation_id": 1
            },
            {
              "is_default": true,
              "interfaces": [
                {
                  "state": "active",
                  "setting": "{\"interface\": {\"command\": \"link-speed 1000\", \"speed\": \"10G\"}}",
                  "speed": {
                    "unit": "G",
                    "value": 1
                  },
                  "name": "swp17",
                  "interface_id": 1
                }
              ],
              "transformation_id": 2
            }
          ],
          "port_id": 17,
          "failure_domain_id": 1,
          "column_id": 9
        },
        {
          "panel_id": 1,
          "slot_id": 0,
          "connector_type": "sfp",
          "row_id": 2,
          "transformations": [
            {
              "is_default": false,
              "interfaces": [
                {
                  "state": "active",
                  "setting": "{\"interface\": {\"command\": \"\", \"speed\": \"10G\"}}",
                  "speed": {
                    "unit": "G",
                    "value": 10
                  },
                  "name": "swp18",
                  "interface_id": 1
                }
              ],
              "transformation_id": 1
            },
            {
              "is_default": true,
              "interfaces": [
                {
                  "state": "active",
                  "setting": "{\"interface\": {\"command\": \"link-speed 1000\", \"speed\": \"10G\"}}",
                  "speed": {
                    "unit": "G",
                    "value": 1
                  },
                  "name": "swp18",
                  "interface_id": 1
                }
              ],
              "transformation_id": 2
            }
          ],
          "port_id": 18,
          "failure_domain_id": 1,
          "column_id": 9
        },
        {
          "panel_id": 1,
          "slot_id": 0,
          "connector_type": "sfp",
          "row_id": 1,
          "transformations": [
            {
              "is_default": false,
              "interfaces": [
                {
                  "state": "active",
                  "setting": "{\"interface\": {\"command\": \"\", \"speed\": \"10G\"}}",
                  "speed": {
                    "unit": "G",
                    "value": 10
                  },
                  "name": "swp19",
                  "interface_id": 1
                }
              ],
              "transformation_id": 1
            },
            {
              "is_default": true,
              "interfaces": [
                {
                  "state": "active",
                  "setting": "{\"interface\": {\"command\": \"link-speed 1000\", \"speed\": \"10G\"}}",
                  "speed": {
                    "unit": "G",
                    "value": 1
                  },
                  "name": "swp19",
                  "interface_id": 1
                }
              ],
              "transformation_id": 2
            }
          ],
          "port_id": 19,
          "failure_domain_id": 1,
          "column_id": 10
        },
        {
          "panel_id": 1,
          "slot_id": 0,
          "connector_type": "sfp",
          "row_id": 2,
          "transformations": [
            {
              "is_default": false,
              "interfaces": [
                {
                  "state": "active",
                  "setting": "{\"interface\": {\"command\": \"\", \"speed\": \"10G\"}}",
                  "speed": {
                    "unit": "G",
                    "value": 10
                  },
                  "name": "swp20",
                  "interface_id": 1
                }
              ],
              "transformation_id": 1
            },
            {
              "is_default": true,
              "interfaces": [
                {
                  "state": "active",
                  "setting": "{\"interface\": {\"command\": \"link-speed 1000\", \"speed\": \"10G\"}}",
                  "speed": {
                    "unit": "G",
                    "value": 1
                  },
                  "name": "swp20",
                  "interface_id": 1
                }
              ],
              "transformation_id": 2
            }
          ],
          "port_id": 20,
          "failure_domain_id": 1,
          "column_id": 10
        },
        {
          "panel_id": 1,
          "slot_id": 0,
          "connector_type": "sfp",
          "row_id": 1,
          "transformations": [
            {
              "is_default": false,
              "interfaces": [
                {
                  "state": "active",
                  "setting": "{\"interface\": {\"command\": \"\", \"speed\": \"10G\"}}",
                  "speed": {
                    "unit": "G",
                    "value": 10
                  },
                  "name": "swp21",
                  "interface_id": 1
                }
              ],
              "transformation_id": 1
            },
            {
              "is_default": true,
              "interfaces": [
                {
                  "state": "active",
                  "setting": "{\"interface\": {\"command\": \"link-speed 1000\", \"speed\": \"10G\"}}",
                  "speed": {
                    "unit": "G",
                    "value": 1
                  },
                  "name": "swp21",
                  "interface_id": 1
                }
              ],
              "transformation_id": 2
            }
          ],
          "port_id": 21,
          "failure_domain_id": 1,
          "column_id": 11
        },
        {
          "panel_id": 1,
          "slot_id": 0,
          "connector_type": "sfp",
          "row_id": 2,
          "transformations": [
            {
              "is_default": false,
              "interfaces": [
                {
                  "state": "active",
                  "setting": "{\"interface\": {\"command\": \"\", \"speed\": \"10G\"}}",
                  "speed": {
                    "unit": "G",
                    "value": 10
                  },
                  "name": "swp22",
                  "interface_id": 1
                }
              ],
              "transformation_id": 1
            },
            {
              "is_default": true,
              "interfaces": [
                {
                  "state": "active",
                  "setting": "{\"interface\": {\"command\": \"link-speed 1000\", \"speed\": \"10G\"}}",
                  "speed": {
                    "unit": "G",
                    "value": 1
                  },
                  "name": "swp22",
                  "interface_id": 1
                }
              ],
              "transformation_id": 2
            }
          ],
          "port_id": 22,
          "failure_domain_id": 1,
          "column_id": 11
        },
        {
          "panel_id": 1,
          "slot_id": 0,
          "connector_type": "sfp",
          "row_id": 1,
          "transformations": [
            {
              "is_default": false,
              "interfaces": [
                {
                  "state": "active",
                  "setting": "{\"interface\": {\"command\": \"\", \"speed\": \"10G\"}}",
                  "speed": {
                    "unit": "G",
                    "value": 10
                  },
                  "name": "swp23",
                  "interface_id": 1
                }
              ],
              "transformation_id": 1
            },
            {
              "is_default": true,
              "interfaces": [
                {
                  "state": "active",
                  "setting": "{\"interface\": {\"command\": \"link-speed 1000\", \"speed\": \"10G\"}}",
                  "speed": {
                    "unit": "G",
                    "value": 1
                  },
                  "name": "swp23",
                  "interface_id": 1
                }
              ],
              "transformation_id": 2
            }
          ],
          "port_id": 23,
          "failure_domain_id": 1,
          "column_id": 12
        },
        {
          "panel_id": 1,
          "slot_id": 0,
          "connector_type": "sfp",
          "row_id": 2,
          "transformations": [
            {
              "is_default": false,
              "interfaces": [
                {
                  "state": "active",
                  "setting": "{\"interface\": {\"command\": \"\", \"speed\": \"10G\"}}",
                  "speed": {
                    "unit": "G",
                    "value": 10
                  },
                  "name": "swp24",
                  "interface_id": 1
                }
              ],
              "transformation_id": 1
            },
            {
              "is_default": true,
              "interfaces": [
                {
                  "state": "active",
                  "setting": "{\"interface\": {\"command\": \"link-speed 1000\", \"speed\": \"10G\"}}",
                  "speed": {
                    "unit": "G",
                    "value": 1
                  },
                  "name": "swp24",
                  "interface_id": 1
                }
              ],
              "transformation_id": 2
            }
          ],
          "port_id": 24,
          "failure_domain_id": 1,
          "column_id": 12
        },
        {
          "panel_id": 1,
          "slot_id": 0,
          "connector_type": "sfp",
          "row_id": 1,
          "transformations": [
            {
              "is_default": false,
              "interfaces": [
                {
                  "state": "active",
                  "setting": "{\"interface\": {\"command\": \"\", \"speed\": \"10G\"}}",
                  "speed": {
                    "unit": "G",
                    "value": 10
                  },
                  "name": "swp25",
                  "interface_id": 1
                }
              ],
              "transformation_id": 1
            },
            {
              "is_default": true,
              "interfaces": [
                {
                  "state": "active",
                  "setting": "{\"interface\": {\"command\": \"link-speed 1000\", \"speed\": \"10G\"}}",
                  "speed": {
                    "unit": "G",
                    "value": 1
                  },
                  "name": "swp25",
                  "interface_id": 1
                }
              ],
              "transformation_id": 2
            }
          ],
          "port_id": 25,
          "failure_domain_id": 1,
          "column_id": 13
        },
        {
          "panel_id": 1,
          "slot_id": 0,
          "connector_type": "sfp",
          "row_id": 2,
          "transformations": [
            {
              "is_default": false,
              "interfaces": [
                {
                  "state": "active",
                  "setting": "{\"interface\": {\"command\": \"\", \"speed\": \"10G\"}}",
                  "speed": {
                    "unit": "G",
                    "value": 10
                  },
                  "name": "swp26",
                  "interface_id": 1
                }
              ],
              "transformation_id": 1
            },
            {
              "is_default": true,
              "interfaces": [
                {
                  "state": "active",
                  "setting": "{\"interface\": {\"command\": \"link-speed 1000\", \"speed\": \"10G\"}}",
                  "speed": {
                    "unit": "G",
                    "value": 1
                  },
                  "name": "swp26",
                  "interface_id": 1
                }
              ],
              "transformation_id": 2
            }
          ],
          "port_id": 26,
          "failure_domain_id": 1,
          "column_id": 13
        },
        {
          "panel_id": 1,
          "slot_id": 0,
          "connector_type": "sfp",
          "row_id": 1,
          "transformations": [
            {
              "is_default": false,
              "interfaces": [
                {
                  "state": "active",
                  "setting": "{\"interface\": {\"command\": \"\", \"speed\": \"10G\"}}",
                  "speed": {
                    "unit": "G",
                    "value": 10
                  },
                  "name": "swp27",
                  "interface_id": 1
                }
              ],
              "transformation_id": 1
            },
            {
              "is_default": true,
              "interfaces": [
                {
                  "state": "active",
                  "setting": "{\"interface\": {\"command\": \"link-speed 1000\", \"speed\": \"10G\"}}",
                  "speed": {
                    "unit": "G",
                    "value": 1
                  },
                  "name": "swp27",
                  "interface_id": 1
                }
              ],
              "transformation_id": 2
            }
          ],
          "port_id": 27,
          "failure_domain_id": 1,
          "column_id": 14
        },
        {
          "panel_id": 1,
          "slot_id": 0,
          "connector_type": "sfp",
          "row_id": 2,
          "transformations": [
            {
              "is_default": false,
              "interfaces": [
                {
                  "state": "active",
                  "setting": "{\"interface\": {\"command\": \"\", \"speed\": \"10G\"}}",
                  "speed": {
                    "unit": "G",
                    "value": 10
                  },
                  "name": "swp28",
                  "interface_id": 1
                }
              ],
              "transformation_id": 1
            },
            {
              "is_default": true,
              "interfaces": [
                {
                  "state": "active",
                  "setting": "{\"interface\": {\"command\": \"link-speed 1000\", \"speed\": \"10G\"}}",
                  "speed": {
                    "unit": "G",
                    "value": 1
                  },
                  "name": "swp28",
                  "interface_id": 1
                }
              ],
              "transformation_id": 2
            }
          ],
          "port_id": 28,
          "failure_domain_id": 1,
          "column_id": 14
        },
        {
          "panel_id": 1,
          "slot_id": 0,
          "connector_type": "sfp",
          "row_id": 1,
          "transformations": [
            {
              "is_default": false,
              "interfaces": [
                {
                  "state": "active",
                  "setting": "{\"interface\": {\"command\": \"\", \"speed\": \"10G\"}}",
                  "speed": {
                    "unit": "G",
                    "value": 10
                  },
                  "name": "swp29",
                  "interface_id": 1
                }
              ],
              "transformation_id": 1
            },
            {
              "is_default": true,
              "interfaces": [
                {
                  "state": "active",
                  "setting": "{\"interface\": {\"command\": \"link-speed 1000\", \"speed\": \"10G\"}}",
                  "speed": {
                    "unit": "G",
                    "value": 1
                  },
                  "name": "swp29",
                  "interface_id": 1
                }
              ],
              "transformation_id": 2
            }
          ],
          "port_id": 29,
          "failure_domain_id": 1,
          "column_id": 15
        },
        {
          "panel_id": 1,
          "slot_id": 0,
          "connector_type": "sfp",
          "row_id": 2,
          "transformations": [
            {
              "is_default": false,
              "interfaces": [
                {
                  "state": "active",
                  "setting": "{\"interface\": {\"command\": \"\", \"speed\": \"10G\"}}",
                  "speed": {
                    "unit": "G",
                    "value": 10
                  },
                  "name": "swp30",
                  "interface_id": 1
                }
              ],
              "transformation_id": 1
            },
            {
              "is_default": true,
              "interfaces": [
                {
                  "state": "active",
                  "setting": "{\"interface\": {\"command\": \"link-speed 1000\", \"speed\": \"10G\"}}",
                  "speed": {
                    "unit": "G",
                    "value": 1
                  },
                  "name": "swp30",
                  "interface_id": 1
                }
              ],
              "transformation_id": 2
            }
          ],
          "port_id": 30,
          "failure_domain_id": 1,
          "column_id": 15
        },
        {
          "panel_id": 1,
          "slot_id": 0,
          "connector_type": "sfp",
          "row_id": 1,
          "transformations": [
            {
              "is_default": false,
              "interfaces": [
                {
                  "state": "active",
                  "setting": "{\"interface\": {\"command\": \"\", \"speed\": \"10G\"}}",
                  "speed": {
                    "unit": "G",
                    "value": 10
                  },
                  "name": "swp31",
                  "interface_id": 1
                }
              ],
              "transformation_id": 1
            },
            {
              "is_default": true,
              "interfaces": [
                {
                  "state": "active",
                  "setting": "{\"interface\": {\"command\": \"link-speed 1000\", \"speed\": \"10G\"}}",
                  "speed": {
                    "unit": "G",
                    "value": 1
                  },
                  "name": "swp31",
                  "interface_id": 1
                }
              ],
              "transformation_id": 2
            }
          ],
          "port_id": 31,
          "failure_domain_id": 1,
          "column_id": 16
        },
        {
          "panel_id": 1,
          "slot_id": 0,
          "connector_type": "sfp",
          "row_id": 2,
          "transformations": [
            {
              "is_default": false,
              "interfaces": [
                {
                  "state": "active",
                  "setting": "{\"interface\": {\"command\": \"\", \"speed\": \"10G\"}}",
                  "speed": {
                    "unit": "G",
                    "value": 10
                  },
                  "name": "swp32",
                  "interface_id": 1
                }
              ],
              "transformation_id": 1
            },
            {
              "is_default": true,
              "interfaces": [
                {
                  "state": "active",
                  "setting": "{\"interface\": {\"command\": \"link-speed 1000\", \"speed\": \"10G\"}}",
                  "speed": {
                    "unit": "G",
                    "value": 1
                  },
                  "name": "swp32",
                  "interface_id": 1
                }
              ],
              "transformation_id": 2
            }
          ],
          "port_id": 32,
          "failure_domain_id": 1,
          "column_id": 16
        },
        {
          "panel_id": 1,
          "slot_id": 0,
          "connector_type": "sfp",
          "row_id": 1,
          "transformations": [
            {
              "is_default": false,
              "interfaces": [
                {
                  "state": "active",
                  "setting": "{\"interface\": {\"command\": \"\", \"speed\": \"10G\"}}",
                  "speed": {
                    "unit": "G",
                    "value": 10
                  },
                  "name": "swp33",
                  "interface_id": 1
                }
              ],
              "transformation_id": 1
            },
            {
              "is_default": true,
              "interfaces": [
                {
                  "state": "active",
                  "setting": "{\"interface\": {\"command\": \"link-speed 1000\", \"speed\": \"10G\"}}",
                  "speed": {
                    "unit": "G",
                    "value": 1
                  },
                  "name": "swp33",
                  "interface_id": 1
                }
              ],
              "transformation_id": 2
            }
          ],
          "port_id": 33,
          "failure_domain_id": 1,
          "column_id": 17
        },
        {
          "panel_id": 1,
          "slot_id": 0,
          "connector_type": "sfp",
          "row_id": 2,
          "transformations": [
            {
              "is_default": false,
              "interfaces": [
                {
                  "state": "active",
                  "setting": "{\"interface\": {\"command\": \"\", \"speed\": \"10G\"}}",
                  "speed": {
                    "unit": "G",
                    "value": 10
                  },
                  "name": "swp34",
                  "interface_id": 1
                }
              ],
              "transformation_id": 1
            },
            {
              "is_default": true,
              "interfaces": [
                {
                  "state": "active",
                  "setting": "{\"interface\": {\"command\": \"link-speed 1000\", \"speed\": \"10G\"}}",
                  "speed": {
                    "unit": "G",
                    "value": 1
                  },
                  "name": "swp34",
                  "interface_id": 1
                }
              ],
              "transformation_id": 2
            }
          ],
          "port_id": 34,
          "failure_domain_id": 1,
          "column_id": 17
        },
        {
          "panel_id": 1,
          "slot_id": 0,
          "connector_type": "sfp",
          "row_id": 1,
          "transformations": [
            {
              "is_default": false,
              "interfaces": [
                {
                  "state": "active",
                  "setting": "{\"interface\": {\"command\": \"\", \"speed\": \"10G\"}}",
                  "speed": {
                    "unit": "G",
                    "value": 10
                  },
                  "name": "swp35",
                  "interface_id": 1
                }
              ],
              "transformation_id": 1
            },
            {
              "is_default": true,
              "interfaces": [
                {
                  "state": "active",
                  "setting": "{\"interface\": {\"command\": \"link-speed 1000\", \"speed\": \"10G\"}}",
                  "speed": {
                    "unit": "G",
                    "value": 1
                  },
                  "name": "swp35",
                  "interface_id": 1
                }
              ],
              "transformation_id": 2
            }
          ],
          "port_id": 35,
          "failure_domain_id": 1,
          "column_id": 18
        },
        {
          "panel_id": 1,
          "slot_id": 0,
          "connector_type": "sfp",
          "row_id": 2,
          "transformations": [
            {
              "is_default": false,
              "interfaces": [
                {
                  "state": "active",
                  "setting": "{\"interface\": {\"command\": \"\", \"speed\": \"10G\"}}",
                  "speed": {
                    "unit": "G",
                    "value": 10
                  },
                  "name": "swp36",
                  "interface_id": 1
                }
              ],
              "transformation_id": 1
            },
            {
              "is_default": true,
              "interfaces": [
                {
                  "state": "active",
                  "setting": "{\"interface\": {\"command\": \"link-speed 1000\", \"speed\": \"10G\"}}",
                  "speed": {
                    "unit": "G",
                    "value": 1
                  },
                  "name": "swp36",
                  "interface_id": 1
                }
              ],
              "transformation_id": 2
            }
          ],
          "port_id": 36,
          "failure_domain_id": 1,
          "column_id": 18
        },
        {
          "panel_id": 1,
          "slot_id": 0,
          "connector_type": "sfp",
          "row_id": 1,
          "transformations": [
            {
              "is_default": false,
              "interfaces": [
                {
                  "state": "active",
                  "setting": "{\"interface\": {\"command\": \"\", \"speed\": \"10G\"}}",
                  "speed": {
                    "unit": "G",
                    "value": 10
                  },
                  "name": "swp37",
                  "interface_id": 1
                }
              ],
              "transformation_id": 1
            },
            {
              "is_default": true,
              "interfaces": [
                {
                  "state": "active",
                  "setting": "{\"interface\": {\"command\": \"link-speed 1000\", \"speed\": \"10G\"}}",
                  "speed": {
                    "unit": "G",
                    "value": 1
                  },
                  "name": "swp37",
                  "interface_id": 1
                }
              ],
              "transformation_id": 2
            }
          ],
          "port_id": 37,
          "failure_domain_id": 1,
          "column_id": 19
        },
        {
          "panel_id": 1,
          "slot_id": 0,
          "connector_type": "sfp",
          "row_id": 2,
          "transformations": [
            {
              "is_default": false,
              "interfaces": [
                {
                  "state": "active",
                  "setting": "{\"interface\": {\"command\": \"\", \"speed\": \"10G\"}}",
                  "speed": {
                    "unit": "G",
                    "value": 10
                  },
                  "name": "swp38",
                  "interface_id": 1
                }
              ],
              "transformation_id": 1
            },
            {
              "is_default": true,
              "interfaces": [
                {
                  "state": "active",
                  "setting": "{\"interface\": {\"command\": \"link-speed 1000\", \"speed\": \"10G\"}}",
                  "speed": {
                    "unit": "G",
                    "value": 1
                  },
                  "name": "swp38",
                  "interface_id": 1
                }
              ],
              "transformation_id": 2
            }
          ],
          "port_id": 38,
          "failure_domain_id": 1,
          "column_id": 19
        },
        {
          "panel_id": 1,
          "slot_id": 0,
          "connector_type": "sfp",
          "row_id": 1,
          "transformations": [
            {
              "is_default": false,
              "interfaces": [
                {
                  "state": "active",
                  "setting": "{\"interface\": {\"command\": \"\", \"speed\": \"10G\"}}",
                  "speed": {
                    "unit": "G",
                    "value": 10
                  },
                  "name": "swp39",
                  "interface_id": 1
                }
              ],
              "transformation_id": 1
            },
            {
              "is_default": true,
              "interfaces": [
                {
                  "state": "active",
                  "setting": "{\"interface\": {\"command\": \"link-speed 1000\", \"speed\": \"10G\"}}",
                  "speed": {
                    "unit": "G",
                    "value": 1
                  },
                  "name": "swp39",
                  "interface_id": 1
                }
              ],
              "transformation_id": 2
            }
          ],
          "port_id": 39,
          "failure_domain_id": 1,
          "column_id": 20
        },
        {
          "panel_id": 1,
          "slot_id": 0,
          "connector_type": "sfp",
          "row_id": 2,
          "transformations": [
            {
              "is_default": false,
              "interfaces": [
                {
                  "state": "active",
                  "setting": "{\"interface\": {\"command\": \"\", \"speed\": \"10G\"}}",
                  "speed": {
                    "unit": "G",
                    "value": 10
                  },
                  "name": "swp40",
                  "interface_id": 1
                }
              ],
              "transformation_id": 1
            },
            {
              "is_default": true,
              "interfaces": [
                {
                  "state": "active",
                  "setting": "{\"interface\": {\"command\": \"link-speed 1000\", \"speed\": \"10G\"}}",
                  "speed": {
                    "unit": "G",
                    "value": 1
                  },
                  "name": "swp40",
                  "interface_id": 1
                }
              ],
              "transformation_id": 2
            }
          ],
          "port_id": 40,
          "failure_domain_id": 1,
          "column_id": 20
        },
        {
          "panel_id": 1,
          "slot_id": 0,
          "connector_type": "sfp",
          "row_id": 1,
          "transformations": [
            {
              "is_default": false,
              "interfaces": [
                {
                  "state": "active",
                  "setting": "{\"interface\": {\"command\": \"\", \"speed\": \"10G\"}}",
                  "speed": {
                    "unit": "G",
                    "value": 10
                  },
                  "name": "swp41",
                  "interface_id": 1
                }
              ],
              "transformation_id": 1
            },
            {
              "is_default": true,
              "interfaces": [
                {
                  "state": "active",
                  "setting": "{\"interface\": {\"command\": \"link-speed 1000\", \"speed\": \"10G\"}}",
                  "speed": {
                    "unit": "G",
                    "value": 1
                  },
                  "name": "swp41",
                  "interface_id": 1
                }
              ],
              "transformation_id": 2
            }
          ],
          "port_id": 41,
          "failure_domain_id": 1,
          "column_id": 21
        },
        {
          "panel_id": 1,
          "slot_id": 0,
          "connector_type": "sfp",
          "row_id": 2,
          "transformations": [
            {
              "is_default": false,
              "interfaces": [
                {
                  "state": "active",
                  "setting": "{\"interface\": {\"command\": \"\", \"speed\": \"10G\"}}",
                  "speed": {
                    "unit": "G",
                    "value": 10
                  },
                  "name": "swp42",
                  "interface_id": 1
                }
              ],
              "transformation_id": 1
            },
            {
              "is_default": true,
              "interfaces": [
                {
                  "state": "active",
                  "setting": "{\"interface\": {\"command\": \"link-speed 1000\", \"speed\": \"10G\"}}",
                  "speed": {
                    "unit": "G",
                    "value": 1
                  },
                  "name": "swp42",
                  "interface_id": 1
                }
              ],
              "transformation_id": 2
            }
          ],
          "port_id": 42,
          "failure_domain_id": 1,
          "column_id": 21
        },
        {
          "panel_id": 1,
          "slot_id": 0,
          "connector_type": "sfp",
          "row_id": 1,
          "transformations": [
            {
              "is_default": false,
              "interfaces": [
                {
                  "state": "active",
                  "setting": "{\"interface\": {\"command\": \"\", \"speed\": \"10G\"}}",
                  "speed": {
                    "unit": "G",
                    "value": 10
                  },
                  "name": "swp43",
                  "interface_id": 1
                }
              ],
              "transformation_id": 1
            },
            {
              "is_default": true,
              "interfaces": [
                {
                  "state": "active",
                  "setting": "{\"interface\": {\"command\": \"link-speed 1000\", \"speed\": \"10G\"}}",
                  "speed": {
                    "unit": "G",
                    "value": 1
                  },
                  "name": "swp43",
                  "interface_id": 1
                }
              ],
              "transformation_id": 2
            }
          ],
          "port_id": 43,
          "failure_domain_id": 1,
          "column_id": 22
        },
        {
          "panel_id": 1,
          "slot_id": 0,
          "connector_type": "sfp",
          "row_id": 2,
          "transformations": [
            {
              "is_default": false,
              "interfaces": [
                {
                  "state": "active",
                  "setting": "{\"interface\": {\"command\": \"\", \"speed\": \"10G\"}}",
                  "speed": {
                    "unit": "G",
                    "value": 10
                  },
                  "name": "swp44",
                  "interface_id": 1
                }
              ],
              "transformation_id": 1
            },
            {
              "is_default": true,
              "interfaces": [
                {
                  "state": "active",
                  "setting": "{\"interface\": {\"command\": \"link-speed 1000\", \"speed\": \"10G\"}}",
                  "speed": {
                    "unit": "G",
                    "value": 1
                  },
                  "name": "swp44",
                  "interface_id": 1
                }
              ],
              "transformation_id": 2
            }
          ],
          "port_id": 44,
          "failure_domain_id": 1,
          "column_id": 22
        },
        {
          "panel_id": 1,
          "slot_id": 0,
          "connector_type": "sfp",
          "row_id": 1,
          "transformations": [
            {
              "is_default": false,
              "interfaces": [
                {
                  "state": "active",
                  "setting": "{\"interface\": {\"command\": \"\", \"speed\": \"10G\"}}",
                  "speed": {
                    "unit": "G",
                    "value": 10
                  },
                  "name": "swp45",
                  "interface_id": 1
                }
              ],
              "transformation_id": 1
            },
            {
              "is_default": true,
              "interfaces": [
                {
                  "state": "active",
                  "setting": "{\"interface\": {\"command\": \"link-speed 1000\", \"speed\": \"10G\"}}",
                  "speed": {
                    "unit": "G",
                    "value": 1
                  },
                  "name": "swp45",
                  "interface_id": 1
                }
              ],
              "transformation_id": 2
            }
          ],
          "port_id": 45,
          "failure_domain_id": 1,
          "column_id": 23
        },
        {
          "panel_id": 1,
          "slot_id": 0,
          "connector_type": "sfp",
          "row_id": 2,
          "transformations": [
            {
              "is_default": false,
              "interfaces": [
                {
                  "state": "active",
                  "setting": "{\"interface\": {\"command\": \"\", \"speed\": \"10G\"}}",
                  "speed": {
                    "unit": "G",
                    "value": 10
                  },
                  "name": "swp46",
                  "interface_id": 1
                }
              ],
              "transformation_id": 1
            },
            {
              "is_default": true,
              "interfaces": [
                {
                  "state": "active",
                  "setting": "{\"interface\": {\"command\": \"link-speed 1000\", \"speed\": \"10G\"}}",
                  "speed": {
                    "unit": "G",
                    "value": 1
                  },
                  "name": "swp46",
                  "interface_id": 1
                }
              ],
              "transformation_id": 2
            }
          ],
          "port_id": 46,
          "failure_domain_id": 1,
          "column_id": 23
        },
        {
          "panel_id": 1,
          "slot_id": 0,
          "connector_type": "sfp",
          "row_id": 1,
          "transformations": [
            {
              "is_default": false,
              "interfaces": [
                {
                  "state": "active",
                  "setting": "{\"interface\": {\"command\": \"\", \"speed\": \"10G\"}}",
                  "speed": {
                    "unit": "G",
                    "value": 10
                  },
                  "name": "swp47",
                  "interface_id": 1
                }
              ],
              "transformation_id": 1
            },
            {
              "is_default": true,
              "interfaces": [
                {
                  "state": "active",
                  "setting": "{\"interface\": {\"command\": \"link-speed 1000\", \"speed\": \"10G\"}}",
                  "speed": {
                    "unit": "G",
                    "value": 1
                  },
                  "name": "swp47",
                  "interface_id": 1
                }
              ],
              "transformation_id": 2
            }
          ],
          "port_id": 47,
          "failure_domain_id": 1,
          "column_id": 24
        },
        {
          "panel_id": 1,
          "slot_id": 0,
          "connector_type": "sfp",
          "row_id": 2,
          "transformations": [
            {
              "is_default": false,
              "interfaces": [
                {
                  "state": "active",
                  "setting": "{\"interface\": {\"command\": \"\", \"speed\": \"10G\"}}",
                  "speed": {
                    "unit": "G",
                    "value": 10
                  },
                  "name": "swp48",
                  "interface_id": 1
                }
              ],
              "transformation_id": 1
            },
            {
              "is_default": true,
              "interfaces": [
                {
                  "state": "active",
                  "setting": "{\"interface\": {\"command\": \"link-speed 1000\", \"speed\": \"10G\"}}",
                  "speed": {
                    "unit": "G",
                    "value": 1
                  },
                  "name": "swp48",
                  "interface_id": 1
                }
              ],
              "transformation_id": 2
            }
          ],
          "port_id": 48,
          "failure_domain_id": 1,
          "column_id": 24
        },
        {
          "panel_id": 1,
          "slot_id": 0,
          "connector_type": "qsfp",
          "row_id": 1,
          "transformations": [
            {
              "is_default": true,
              "interfaces": [
                {
                  "state": "active",
                  "setting": "{\"interface\": {\"command\": \"\", \"speed\": \"40G\"}}",
                  "speed": {
                    "unit": "G",
                    "value": 40
                  },
                  "name": "swp49",
                  "interface_id": 1
                }
              ],
              "transformation_id": 1
            },
            {
              "is_default": false,
              "interfaces": [
                {
                  "state": "active",
                  "setting": "{\"interface\": {\"command\": \"\", \"speed\": \"4x10G\"}}",
                  "speed": {
                    "unit": "G",
                    "value": 10
                  },
                  "name": "swp49s0",
                  "interface_id": 1
                },
                {
                  "state": "active",
                  "setting": "{\"interface\": {\"command\": \"\", \"speed\": \"4x10G\"}}",
                  "speed": {
                    "unit": "G",
                    "value": 10
                  },
                  "name": "swp49s1",
                  "interface_id": 2
                },
                {
                  "state": "active",
                  "setting": "{\"interface\": {\"command\": \"\", \"speed\": \"4x10G\"}}",
                  "speed": {
                    "unit": "G",
                    "value": 10
                  },
                  "name": "swp49s2",
                  "interface_id": 3
                },
                {
                  "state": "active",
                  "setting": "{\"interface\": {\"command\": \"\", \"speed\": \"4x10G\"}}",
                  "speed": {
                    "unit": "G",
                    "value": 10
                  },
                  "name": "swp49s3",
                  "interface_id": 4
                }
              ],
              "transformation_id": 2
            }
          ],
          "port_id": 49,
          "failure_domain_id": 1,
          "column_id": 25
        },
        {
          "panel_id": 1,
          "slot_id": 0,
          "connector_type": "qsfp",
          "row_id": 2,
          "transformations": [
            {
              "is_default": true,
              "interfaces": [
                {
                  "state": "active",
                  "setting": "{\"interface\": {\"command\": \"\", \"speed\": \"40G\"}}",
                  "speed": {
                    "unit": "G",
                    "value": 40
                  },
                  "name": "swp50",
                  "interface_id": 1
                }
              ],
              "transformation_id": 1
            },
            {
              "is_default": false,
              "interfaces": [
                {
                  "state": "active",
                  "setting": "{\"interface\": {\"command\": \"\", \"speed\": \"4x10G\"}}",
                  "speed": {
                    "unit": "G",
                    "value": 10
                  },
                  "name": "swp50s0",
                  "interface_id": 1
                },
                {
                  "state": "active",
                  "setting": "{\"interface\": {\"command\": \"\", \"speed\": \"4x10G\"}}",
                  "speed": {
                    "unit": "G",
                    "value": 10
                  },
                  "name": "swp50s1",
                  "interface_id": 2
                },
                {
                  "state": "active",
                  "setting": "{\"interface\": {\"command\": \"\", \"speed\": \"4x10G\"}}",
                  "speed": {
                    "unit": "G",
                    "value": 10
                  },
                  "name": "swp50s2",
                  "interface_id": 3
                },
                {
                  "state": "active",
                  "setting": "{\"interface\": {\"command\": \"\", \"speed\": \"4x10G\"}}",
                  "speed": {
                    "unit": "G",
                    "value": 10
                  },
                  "name": "swp50s3",
                  "interface_id": 4
                }
              ],
              "transformation_id": 2
            }
          ],
          "port_id": 50,
          "failure_domain_id": 1,
          "column_id": 25
        },
        {
          "panel_id": 1,
          "slot_id": 0,
          "connector_type": "qsfp",
          "row_id": 1,
          "transformations": [
            {
              "is_default": true,
              "interfaces": [
                {
                  "state": "active",
                  "setting": "{\"interface\": {\"command\": \"\", \"speed\": \"40G\"}}",
                  "speed": {
                    "unit": "G",
                    "value": 40
                  },
                  "name": "swp51",
                  "interface_id": 1
                }
              ],
              "transformation_id": 1
            },
            {
              "is_default": false,
              "interfaces": [
                {
                  "state": "active",
                  "setting": "{\"interface\": {\"command\": \"\", \"speed\": \"4x10G\"}}",
                  "speed": {
                    "unit": "G",
                    "value": 10
                  },
                  "name": "swp51s0",
                  "interface_id": 1
                },
                {
                  "state": "active",
                  "setting": "{\"interface\": {\"command\": \"\", \"speed\": \"4x10G\"}}",
                  "speed": {
                    "unit": "G",
                    "value": 10
                  },
                  "name": "swp51s1",
                  "interface_id": 2
                },
                {
                  "state": "active",
                  "setting": "{\"interface\": {\"command\": \"\", \"speed\": \"4x10G\"}}",
                  "speed": {
                    "unit": "G",
                    "value": 10
                  },
                  "name": "swp51s2",
                  "interface_id": 3
                },
                {
                  "state": "active",
                  "setting": "{\"interface\": {\"command\": \"\", \"speed\": \"4x10G\"}}",
                  "speed": {
                    "unit": "G",
                    "value": 10
                  },
                  "name": "swp51s3",
                  "interface_id": 4
                }
              ],
              "transformation_id": 2
            }
          ],
          "port_id": 51,
          "failure_domain_id": 1,
          "column_id": 26
        },
        {
          "panel_id": 1,
          "slot_id": 0,
          "connector_type": "qsfp",
          "row_id": 2,
          "transformations": [
            {
              "is_default": true,
              "interfaces": [
                {
                  "state": "active",
                  "setting": "{\"interface\": {\"command\": \"\", \"speed\": \"40G\"}}",
                  "speed": {
                    "unit": "G",
                    "value": 40
                  },
                  "name": "swp52",
                  "interface_id": 1
                }
              ],
              "transformation_id": 1
            },
            {
              "is_default": false,
              "interfaces": [
                {
                  "state": "active",
                  "setting": "{\"interface\": {\"command\": \"\", \"speed\": \"4x10G\"}}",
                  "speed": {
                    "unit": "G",
                    "value": 10
                  },
                  "name": "swp52s0",
                  "interface_id": 1
                },
                {
                  "state": "active",
                  "setting": "{\"interface\": {\"command\": \"\", \"speed\": \"4x10G\"}}",
                  "speed": {
                    "unit": "G",
                    "value": 10
                  },
                  "name": "swp52s1",
                  "interface_id": 2
                },
                {
                  "state": "active",
                  "setting": "{\"interface\": {\"command\": \"\", \"speed\": \"4x10G\"}}",
                  "speed": {
                    "unit": "G",
                    "value": 10
                  },
                  "name": "swp52s2",
                  "interface_id": 3
                },
                {
                  "state": "active",
                  "setting": "{\"interface\": {\"command\": \"\", \"speed\": \"4x10G\"}}",
                  "speed": {
                    "unit": "G",
                    "value": 10
                  },
                  "name": "swp52s3",
                  "interface_id": 4
                }
              ],
              "transformation_id": 2
            }
          ],
          "port_id": 52,
          "failure_domain_id": 1,
          "column_id": 26
        },
        {
          "panel_id": 1,
          "slot_id": 0,
          "connector_type": "qsfp",
          "row_id": 1,
          "transformations": [
            {
              "is_default": true,
              "interfaces": [
                {
                  "state": "active",
                  "setting": "{\"interface\": {\"command\": \"\", \"speed\": \"40G\"}}",
                  "speed": {
                    "unit": "G",
                    "value": 40
                  },
                  "name": "swp53",
                  "interface_id": 1
                }
              ],
              "transformation_id": 1
            },
            {
              "is_default": false,
              "interfaces": [
                {
                  "state": "active",
                  "setting": "{\"interface\": {\"command\": \"\", \"speed\": \"4x10G\"}}",
                  "speed": {
                    "unit": "G",
                    "value": 10
                  },
                  "name": "swp53s0",
                  "interface_id": 1
                },
                {
                  "state": "active",
                  "setting": "{\"interface\": {\"command\": \"\", \"speed\": \"4x10G\"}}",
                  "speed": {
                    "unit": "G",
                    "value": 10
                  },
                  "name": "swp53s1",
                  "interface_id": 2
                },
                {
                  "state": "active",
                  "setting": "{\"interface\": {\"command\": \"\", \"speed\": \"4x10G\"}}",
                  "speed": {
                    "unit": "G",
                    "value": 10
                  },
                  "name": "swp53s2",
                  "interface_id": 3
                },
                {
                  "state": "active",
                  "setting": "{\"interface\": {\"command\": \"\", \"speed\": \"4x10G\"}}",
                  "speed": {
                    "unit": "G",
                    "value": 10
                  },
                  "name": "swp53s3",
                  "interface_id": 4
                }
              ],
              "transformation_id": 2
            }
          ],
          "port_id": 53,
          "failure_domain_id": 1,
          "column_id": 27
        },
        {
          "panel_id": 1,
          "slot_id": 0,
          "connector_type": "qsfp",
          "row_id": 2,
          "transformations": [
            {
              "is_default": true,
              "interfaces": [
                {
                  "state": "active",
                  "setting": "{\"interface\": {\"command\": \"\", \"speed\": \"40G\"}}",
                  "speed": {
                    "unit": "G",
                    "value": 40
                  },
                  "name": "swp54",
                  "interface_id": 1
                }
              ],
              "transformation_id": 1
            },
            {
              "is_default": false,
              "interfaces": [
                {
                  "state": "active",
                  "setting": "{\"interface\": {\"command\": \"\", \"speed\": \"4x10G\"}}",
                  "speed": {
                    "unit": "G",
                    "value": 10
                  },
                  "name": "swp54s0",
                  "interface_id": 1
                },
                {
                  "state": "active",
                  "setting": "{\"interface\": {\"command\": \"\", \"speed\": \"4x10G\"}}",
                  "speed": {
                    "unit": "G",
                    "value": 10
                  },
                  "name": "swp54s1",
                  "interface_id": 2
                },
                {
                  "state": "active",
                  "setting": "{\"interface\": {\"command\": \"\", \"speed\": \"4x10G\"}}",
                  "speed": {
                    "unit": "G",
                    "value": 10
                  },
                  "name": "swp54s2",
                  "interface_id": 3
                },
                {
                  "state": "active",
                  "setting": "{\"interface\": {\"command\": \"\", \"speed\": \"4x10G\"}}",
                  "speed": {
                    "unit": "G",
                    "value": 10
                  },
                  "name": "swp54s3",
                  "interface_id": 4
                }
              ],
              "transformation_id": 2
            }
          ],
          "port_id": 54,
          "failure_domain_id": 1,
          "column_id": 27
        }
      ]
    }

References
----------

[Reading syseeprom on Cumulus device] https://docs.cumulusnetworks.com/cumulus-linux/Monitoring-and-Troubleshooting/Monitoring-System-Hardware/

[Edgecore AS5712-54X-0] https://www.edge-core.com/productsInfo.php?cls=1&cls2=8&cls3=44&id=15
