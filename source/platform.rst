========
Platform
========

.. toctree::
   :maxdepth: 3

   managing_users
   managing_roles
   syslog_configuration
   streaming_architecture
   receiver_configuration
   event_log
   aos_cluster
   developers
   support
