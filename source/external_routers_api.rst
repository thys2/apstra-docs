==============================
External Routers API Reference
==============================

Routers are assigned to the blueprint similar to resource pools, but are not
‘allocated’ in a similar way internally as they can be shared between blueprints,
and no resource allocation is done in the back-end.

The router requires a slot assignation of the name ‘to_external_router_link_ips’
as well.

Create a router
_______________

An example payload for an external router:

.. code-block:: json

    {
        "id": "example_router1",
        "display_name": "example_router1",
        "address": "198.51.100.1",
        "asn": 65534,
        "tags": ["default"]
    }

.. code-block:: text
    :caption: Patch router:

    curl 'https://192.168.25.250/api/resources/external-routers' -H 'AuthToken: eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6ImFkbWluIiwiY3JlYXRlZF9hdCI6IjIwMTctMDUtMzFUMDA6MjI6MDcuNTIwMTgzWiIsInNlc3Npb24iOiJjOTliOGVlOS05Y2NjLTRjZTAtYTY5NS0wODI3N2ZkYjA0ZDYifQ.FnJMR3crPoD0-lQRXnpPOJ8TCsRG9Wr-DaddnAIj6ko' --data-binary '{"display_name":"example_router1","address":"198.51.100.1","asn":65534}' --compressed --insecure

Assign a router to a blueprint
______________________________
Routers can be assigned to a blueprint with a PATCH to https://aos-server/api/blueprints/\<bp_id>/external-router-assigments with the JSON payload:

.. code-block:: json

    {"assignments":{"<router_id>":"router1"}

Each additional assigned router in the blueprint will be assigned the name of
router1, router2, router3, etc.  The router ID is learned from the graph.

.. code-block:: text

    curl 'https://192.168.25.250/api/blueprints/4c1e69c6-97bd-4c99-9504-7818f138b17f/external-router-assignments' -X PATCH -H -H 'AuthToken: eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6ImFkbWluIiwiY3JlYXRlZF9hdCI6IjIwMTctMDUtMzFUMDA6MjI6MDcuNTIwMTgzWiIsInNlc3Npb24iOiJjOTliOGVlOS05Y2NjLTRjZTAtYTY5NS0wODI3N2ZkYjA0ZDYifQ.FnJMR3crPoD0-lQRXnpPOJ8TCsRG9Wr-DaddnAIj6ko' '{"assignments":{"7c700fe6-404f-4f86-959d-3a1fe33a7938":"router1"}}' --compressed --insecure


Unassign a router to a blueprint
_________________________________
Unassigning the router is done by posting *null* to the router node ID to the API endpoint https://aos-server/api/blueprints/\<bp_id>/external-router-assignments with JSON payload:

.. code-block:: json

    {
        "assignments": {
            "7c700fe6-404f-4f86-959d-3a1fe33a7938": null
        }
    }

.. code-block:: text

    curl 'https://192.168.25.250/api/blueprints/4c1e69c6-97bd-4c99-9504-7818f138b17f/external-router-assignments' -X PATCH -H 'AuthToken: eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6ImFkbWluIiwiY3JlYXRlZF9hdCI6IjIwMTctMDUtMzFUMDA6MjI6MDcuNTIwMTgzWiIsInNlc3Npb24iOiJjOTliOGVlOS05Y2NjLTRjZTAtYTY5NS0wODI3N2ZkYjA0ZDYifQ.FnJMR3crPoD0-lQRXnpPOJ8TCsRG9Wr-DaddnAIj6ko' -H 'Accept: application/json; charset=utf-8'  --data-binary '{"assignments":{"7c700fe6-404f-4f86-959d-3a1fe33a7938":null}}' --compressed --insecure

List routers assigned to a blueprint
____________________________________
Obtaining a list of router assignments (and their node ID) is done by viewing the API endpoint with an HTTP GET to https://aos-server/api/blueprints/\<bp_id>/external-router-assignments.
The response will contain router ID slots for assignment.


.. code-block:: json

    {
        "assignments": {
            "7c700fe6-404f-4f86-959d-3a1fe33a7938": null
        }
    }

.. code-block:: text

    curl 'https://192.168.25.250/api/blueprints/4c1e69c6-97bd-4c99-9504-7818f138b17f/external-router-assignments' -H 'AuthToken: eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6ImFkbWluIiwiY3JlYXRlZF9hdCI6IjIwMTctMDUtMzFUMDA6MjI6MDcuNTIwMTgzWiIsInNlc3Npb24iOiJjOTliOGVlOS05Y2NjLTRjZTAtYTY5NS0wODI3N2ZkYjA0ZDYifQ.FnJMR3crPoD0-lQRXnpPOJ8TCsRG9Wr-DaddnAIj6ko' -H 'Accept: application/json; charset=utf-8' --compressed --insecure

.. code-block:: json

    {
        "assignments": {
            "7c700fe6-404f-4f86-959d-3a1fe33a7938": null
        }
    }
