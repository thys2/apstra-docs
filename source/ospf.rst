=========================================
OSPF Routing Protocol for External Router
=========================================
As of AOS 3.2, Apstra extends capability to support OSPF for route exchange between
the DC fabric and external routers (External Connectivity Points; ECP's) for
customers who have limitations running BGP between fabric and external routers.

AOS supports OSPF ECP's on the following vendors:

* Arista EOS
* Cisco NX-OS
* Cumulus Linux

.. note::
    * Border leafs are always OSPF ASBRs and always part of an non-backbone area.
    * The external routers are OSPF ABRs and part of the backbone area.
    * OSPF v2 must be supported (OSPFv3 is not supported)
    * OSPF External Router connections are not yet supported for Junos devices.

For each AOS Security Zone(VRF), user should be able to add external OSPF
endpoints (same as we have for external BGP neighbors)

Import and Assign External Router
---------------------------------
Import an external router from the external router catalog and assign the external
router links to leaf or spine interfaces, as if you were for BGP, to complete
system assignment. See :ref:`Staging External Routers <staging_external_routers>`
for steps.

Add/Modify OSPF External Connectivity Points
--------------------------------------------
As of AOS 3.2, If you are connecting OSPF External Routers in an Blueprint, you
must add OSPF External Connectivity Points (ECP) in the Security Zone.

#. From the **Virtual** view of your **Staged** Blueprint, click
   **Security Zones**.
#. Click the VRF name corresponding to the Security Zone to edit.

   .. image:: static/images/extending_security_zone_to_external_OSPF_router/extend_OSPF_SZ_1_320.png

#. ECP appears under the **External Connectivity Points** section. Click on
   the **Add Connectivity Point** to add a new ECP for external OSPF router.

#. Fill in the required information.

   .. image:: static/images/extending_security_zone_to_external_OSPF_router/extend_OSPF_SZ_2_320.png


   Connectivity Type
      Both L2 and L3 connectivity points are supported, with same semantics as BGP
      for IP assignment Define whether the ECP is L2 or L3. This must match the type
      defined when you :ref:`assign external routers to a blueprint
      <staging_external_routers>`.

   Routing Protocol
      When creating a connectivity point underneath a security zone, choose one
      of the two "protocol" options, where "BGP" is the default.

   OSPF Settings
      Customer can specify custom OSPF peering parameters and route import/export
      policies.

      "VLAN ID"
          The tagged VLAN ID when ECP is a L2 SVI interface.

      "IPv4 Subnet"
          The IPv4 subnet used for the ECP. This must be assigned and cannot be
          assigned via a Resource Pool.

      "OSPF Area ID"
          An OSPF Area ID is 32-bit integer or IP address that extenal router and
          this router will participate in. All connectivity points of type OSPF in
          a VRF must be in same "OSPF Area ID" and it will raise API errors if
          they are not.

      "MTU Ignore"
          All NOS defaults to performing an MTU check between neighbor. Check MTU
          Ignore if you want to disable MTU check between neighbors.

      "Bidirectional Forwarding Detection (BFD)"
          BFD is disabled by default. Check BFD to enable BFD for faster OSPF
          failure detection mechanisms.

      "Hello/Dead Interval Timer"
          Hello and Dead intervals must match between neighbors. Define custom
          interval timers if required.

      .. image:: static/images/extending_security_zone_to_external_OSPF_router/extend_OSPF_SZ_3_320.png


      "MD5 key ID/key"
          Clear-text passwords are generally discouraged, Therefore customers can
          specify the MD5 password.
          A key ID, used for gracefully migrating MD5 keys,must match between
          neighbors. When MD5 authentication is chosen both the key and the
          associated key-ID must be specified by the user.

      "Network type (Broadcast/Point-to-Point)"
          Customer can specify their network type Broadcast or Point-to-Point.

   Links
      Select the links to use for the ECP.

   .. image:: static/images/blueprint_virtual_tasks/300_security-zone-5.png

   Resources
      For L3 ECP, AOS automatically assigns IP for each ECP device from IPv4
      Subnet. For L2 ECP, you must manually assign IP addresses for each node.
      If IPv6 is enabled for the ECP, enter the IPv6 addresses for each node.

   .. image:: static/images/blueprint_virtual_tasks/300_security-zone-9.png

   Routing Policies / Enable Routing Policies Overrides
      If checked, you can override the default Routing Policies defined when
      creating the Security Zone. Refer to the `Add Security Zones` section
      above for more information.

   .. image:: static/images/blueprint_virtual_tasks/300_security-zone-6.png

.. note::
   OSPF specific timers other than Hello/Dead interval such as SPF throttling,
   LSA throttling, LSA group pacing, LSA arrival can be customized using configlets.

   AOS do not support changing the default Router Priority value.


.. note::
    Apstra does not directly support Static Routing and ISIS for external router
    connectivity.
