======================
AOS Web Interface (UI)
======================
After the :doc:`AOS server <server_installation>` is deployed, you can design,
build, deploy, operate and validate the network from the AOS web interface.

Accessing AOS Web Interface
===========================

#. From the latest web browser version of Google Chrome or Mozilla FireFox,
   enter the URL ``https://<aos_server_ip>`` where ``<aos_server_ip>``
   is the IP address of the AOS server, or a DNS name that resolves to
   the IP address of the AOS server.

#. If a security warning appears, click **Advanced** and **Proceed to ...** the
   site. The warning occurs because the SSL certificate that was generated during
   installation is self-signed.

   .. important::

     For security, please replace the default self-signed :ref:`SSL
     certificate <replacing-aos-server-certificate>` with one from your own
     certificate authority.

#. From the AOS server login page, enter username **admin** and password **admin**
   to go to the main page of the AOS web interface.

   .. important::

     For security, please :ref:`change the web interface
     password <change_user_password>` for **admin** after you first log in.

     We recommend that you also change the operating system (OS) password.

     #. Log into the configuration tool: ``admin@aos-server:~$ aos_config``.
     #. Choose **Local credentials** and change the OS password.
        You can also change the web interface password from here by choosing
        **WebUI credentials**.

For guidance on designing and building the network in AOS, see
:ref:`Getting Started <design_build_guide>`.

.. image:: static/images/blueprints/blueprint_first_screen_330.png

Resetting Admin Password
========================
To recover a forgotten or lost admin password for the web interface, log into the
AOS server as the default admin user via ssh, and type the command
``aos_reset_admin_password``.

.. code-block:: prompt

    admin@aos-server:~$ aos_reset_admin_password
    Resetting UI "admin" user password to default "admin"
    Successfully reset admin's password
    admin@aos-server:~$

.. important::

  For security, please :ref:`change the admin password <change_user_password>`
  after resetting it to the default.

.. _replacing-aos-server-certificate:

Replacing AOS Server Certificate
================================

.. sidebar:: Encryption Only

    The certificate is used only for encrypting the AOS web server and AOS REST
    API, not for any internal device-controller connectivity. Apstra supports and
    recommends replacing the default SSL certificate.

A unique self-signed certificate is automatically generated on each AOS server
at first boot. The default certificate files are stored
on the AOS server at ```/etc/aos/nginx.conf.d``.

The HTTPS certificate is not retained in AOS system backups.
Backups of the ``/etc/aos`` folder must be performed manually when performing
system backups.

* ``nginx.crt``  - public key for webserver
* ``nginx.key``  - private key for webserver

Replacing Existing Certificate with Signed Certificate
------------------------------------------------------

1. Back up existing OpenSSL keys.

  .. code:: bash

    admin@aos-server:/$ sudo -s
    [sudo] password for admin:

    root@aos-server:/# cd /etc/aos/nginx.conf.d
    root@aos-server:/etc/aos/nginx.conf.d# cp nginx.crt nginx.crt.old
    root@aos-server:/etc/aos/nginx.conf.d# cp nginx.key nginx.key.old

2. Create a new OpenSSL private key with the built-in `openssl` command.

   .. code:: bash

       root@aos-server:/etc/aos/nginx.conf.d# openssl genrsa -out nginx.key 2048
       Generating RSA private key, 2048 bit long modulus
       .............+++
       ......+++
       e is 65537 (0x10001)
   .. warning::

       Do not attempt to modify the default ``nginx.crt`` or ``nginx.key``
       filenames. These values are referenced from nginx's configuration file, and
       Apstra may replace these files in the future as part of a service upgrade,
       so the filenames must
       be predictable. Moreover, do not make configuration changes to nginx.conf, as
       this file may be replaced during AOS upgrade.

3. Create a certificate signing request.

   If your certificate requires Subject Alternative Name (SAN), you will need
   your own OpenSSL template, which is beyond the scope of this document.
   If you need more advanced certificate support please contact support.

   .. warning::

       If you have created custom OpenSSL configuration files for advanced
       certificate requests, do not leave then in the nginx configuration folder,
       as nginx will attempt to load them `(*.conf)` on service startup,
       causing a service failure.

   .. code-block:: bash

       root@aos-server:/etc/aos/nginx.conf.d# openssl req -new -sha256 -key nginx.key -out nginx.csr
       You are about to be asked to enter information that will be incorporated
       into your certificate request.
       What you are about to enter is what is called a Distinguished Name or a DN.
       There are quite a few fields but you can leave some blank
       For some fields there will be a default value,
       If you enter '.', the field will be left blank.
       -----
       Country Name (2 letter code) [AU]:US
       State or Province Name (full name) [Some-State]:California
       Locality Name (eg, city) []:Menlo Park
       Organization Name (eg, company) [Internet Widgits Pty Ltd]:Apstra, Inc
       Organizational Unit Name (eg, section) []:
       Common Name (e.g. server FQDN or YOUR name) []:aos-server.apstra.com
       Email Address []:support@apstra.com

       Please enter the following 'extra' attributes
       to be sent with your certificate request
       A challenge password []:
       An optional company name []:

4. Submit your Certificate Signing Request (nginx.csr) to
   your Certificate Authority.

   The required steps are outside the scope of this document - CA instructions
   differ per implementation. Any valid SSL certificate will work.

   The example below is of self-signing the certificate.

   .. code-block:: bash

       root@aos-server:/etc/aos/nginx.conf.d# openssl req -x509 -sha256 -days 3650 -key nginx.key -in nginx.csr -out nginx.crt
       root@aos-server:/etc/aos/nginx.conf.d#

5. Verify that the SSL certificates match: private key, public key, and CSR.

   .. code-block:: bash

       root@aos-server:/etc/aos/nginx.conf.d# openssl rsa -noout -modulus -in nginx.key | openssl md5
       (stdin)= 60ac4532a708c98d70fee0dbcaab1e75

       root@aos-server:/etc/aos/nginx.conf.d# openssl req -noout -modulus -in nginx.csr | openssl md5
       (stdin)= 60ac4532a708c98d70fee0dbcaab1e75

       root@aos-server:/etc/aos/nginx.conf.d# openssl x509 -noout -modulus -in nginx.crt | openssl md5
       (stdin)= 60ac4532a708c98d70fee0dbcaab1e75

6. Restart the nginx container to load the new certificate.

   .. code-block:: bash

      root@aos-server:/etc/aos/nginx.conf.d# docker restart aos_nginx_1
      aos_nginx_1
      root@aos-server:/etc/aos/nginx.conf.d

   Confirm the new certificate in your web browser. You can check that the new
   certificate common name matches 'aos-server.apstra.com'

   .. |blueprint_edit_button| image:: static/images/aos_webserver_certificate/validate_cert.png
       :height: 20px

Replacing Existing Certificate with Self-Signed Certificate
-----------------------------------------------------------
Users on AOS versions 3.1.0 and earlier that use macOS Catalina and Google Chrome
cannot accept the default self-signed HTTPS/SSL certificate that is provided by
Google Chrome. The self-signed certificate must be replaced. (AOS bug AOS-14708).

1. Back up existing OpenSSL keys.

   .. code-block:: bash

      admin@aos-server:/$ sudo -s
      [sudo] password for admin:

      root@aos-server:/# cd /etc/aos/nginx.conf.d
      root@aos-server:/etc/aos/nginx.conf.d# cp nginx.crt nginx.crt.old
      root@aos-server:/etc/aos/nginx.conf.d# cp nginx.key nginx.key.old

2. Verify a Random Number Generator seed file .rnd exists in /home/admin. If not,
   create one.

   .. code-block:: bash

      root@aos-server:~# touch /home/admin/.rnd
      root@aos-server:~#

3. Generate a new OpenSSL private key and self-signed certificate.

   .. code-block:: bash

      root@aos-server:/etc/aos/nginx.conf.d# openssl req -newkey rsa:2048 -nodes -keyout nginx.key -x509 -days 824 -out nginx.crt -addext extendedKeyUsage=serverAuth -addext subjectAltName=DNS:apstra.com
      Generating a RSA private key
      ...........................................+++++
      .....................................................................................+++++
      writing new private key to 'nginx.key'
      -----
      You are about to be asked to enter information that will be incorporated
      into your certificate request.
      What you are about to enter is what is called a Distinguished Name or a DN.
      There are quite a few fields but you can leave some blank
      For some fields there will be a default value,
      If you enter '.', the field will be left blank.
      -----
      Country Name (2 letter code) [AU]:US
      State or Province Name (full name) [Some-State]:California
      Locality Name (eg, city) []:Menlo Park
      Organization Name (eg, company) [Internet Widgits Pty Ltd]:Apstra, Inc
      Organizational Unit Name (eg, section) []:
      Common Name (e.g. server FQDN or YOUR name) []:aos-server.apstra.com
      Email Address []:support@apstra.com
      root@aos-server:/etc/aos/nginx.conf.d#

4. Restart the nginx container to load the new certificate.

   .. code-block:: bash

      root@aos-server:/etc/aos/nginx.conf.d# docker restart aos_nginx_1
      aos_nginx_1
      root@aos-server:/etc/aos/nginx.conf.d

Checking AOS Web Interface Version
==================================
From the AOS web interface, navigate to **Platform / About**.

Updating AOS Web Interface
==========================
You can install an optional AOS server UI update to add additional UI
functionality. This is independent of the AOS server backend and does not affect
the state of the AOS server or the established configuration.

#. Upload the AOS server UI update file to the AOS server. For this example, the
   file is named **aos-web-ui_2.2.0-67.run**.

#. Change to the root user and run the following file.

   .. code-block:: prompt

       admin@aos-server:~$ sudo -s
       [sudo] password for admin:
       root@aos-server:~# bash aos-web-ui_2.2.0-67.run
       Verifying archive integrity... All good.
       Uncompressing AOS WebUI installer  100%
       ### Backing up existing AOS WebUI into /opt/aos/frontend/snapshot/2018-02-25_20-34-15 ...
       ### Copying AOS WebUI file into aos_controller_1 ...
       ### Initializing new AOS WebUI ...
       ### Done!
       root@aos-server:~#

#. During update, the current UI is copied to the ``/opt/aos/frontend/snapshot/``
   snapshot directory.

#. From the AOS web interface, navigate to **Platform / About** to confirm that
   the **AOS UI version** has been updated.

Restoring AOS Web Interface Version
===================================
#. You can restore the previous AOS web interface version at any time without
   affecting the state of the AOS server. From the snapshot directory,
   run the ``webui_restore`` file.

   .. code-block:: prompt

       root@aos-server:~# cd /opt/aos/frontend/snapshot/2018-02-25_20-34-15
       root@aos-server:/opt/aos/frontend/snapshot/2018-02-25_20-34-15# ls
       aos-web-ui.zip  webui_restore
       root@aos-server:/opt/aos/frontend/snapshot/2018-02-25_20-34-15# ./webui_restore
       ### Copying AOS WebUI file into aos_controller_1...
       ### Initializing AOS WebUI...
       ### Done!
       root@aos-server:/opt/aos/frontend/snapshot/2018-02-25_20-34-15#

#. From the AOS web interface, navigate to **Platform / About** to confirm that
   the **AOS UI version** has been rolled back.
