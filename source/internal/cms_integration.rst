===================
AOS CMS Integration
===================

.. uml::

    @startuml
    title AOS - CMS/vSwitch integration (VM spin up)
    skinparam ParticipantPadding 20
    skinparam BoxPadding 10
    hide footbox

    actor "CMS admin" as admin
    box "CMS"
      participant CMS
      participant "AOS plugin" as aos_plugin
    end box
    box "AOS"
      participant AOS
    end box
    box "TOR Switch"
      participant "AOS agent" as switch_agent
      participant "NOS" as switch_os
    end box
    box "L2 Server"
      participant server_agent
      participant vSwitch
      participant Hypervisor
    end box

    admin -> CMS: Create new VM
    note over admin, CMS: In group "database"
    CMS --> aos_plugin: Create endpoint
    aos_plugin -> AOS: Create endpoint
      note over aos_plugin, AOS
        Group: "database"
        Server: server01
        MAC: YYY
      end note
    AOS --> aos_plugin: Done
    aos_plugin --> CMS: Done
      note over aos_plugin, AOS: Endpoint ZZZ created for MAC YYY on server01
    AOS -> switch_agent: Configure HW switch(es)
      note over AOS, switch_agent
        Select VLAN/VNI/VTEP/SVI resources
        Deploy required changes e.g. VLAN 10 on Ethernet3
      end note
    switch_agent -> switch_os: Do it
    switch_agent --> AOS: Done

    CMS -> Hypervisor: Spin up VM
    CMS <-- Hypervisor: Done
    vSwitch --> server_agent: Detect new interface
      note over server_agent, vSwitch
        AOS agent detect new interafce
        knows it is AOS endpoint ZZZ
        (from MAC YYY)
      end note
    server_agent -> vSwitch: Configure vSwitch
      note over server_agent, vSwitch
        Create VLAN 10 on leaf facing interface
        Put VM interface in VLAN
      end note
    AOS <-- server_agent: Report VM/if state


     == VM operational ==

    AOS <-> server_agent: Report state
    note over AOS, server_agent: Continuously report VM interface state
    @endl
