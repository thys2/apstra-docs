###################
Documentation HOWTO
###################
This file is used as a higher-level 'howto' for consuming this
documentation as used by apstra internal staff.

For a generalized ReStructuredText primer, visit
docutils.sourceforge.net/docs/user/rst/quickstart.html and
http://www.sphinx-doc.org/en/stable/rest.html



Documentation standards
=======================
This section describes how the actual RST should be authored, and what the standards
are.


Most of this is taken out from the documentation style guide on Sphinx-based
documentation
http://documentation-style-guide-sphinx.readthedocs.io/en/latest/style-guide.html

Code and console output samples
-------------------------------

Code samples should only make use of the ``code-block`` sphinx directive.

Code-block text can be indented 4-characters in to be included.

The code-sample block includes some options.

    * Always make use of a language for code-block.  When in doubt, use 'text'
    * Available linters are examples like ``ini``, ``json``, ``python``, ``text``.

.. code-block:: text

    .. code-block:: text
        :caption:  Some example caption

        When text is indented underneath a code-block directive, it renders
        in a pretty code-block.

    .. code-block:: python
        :emphasize-lines: 3, 5-6

        import os

        print os.getcwd()
        print 'foo'
        files = os.listdir(os.getcwd())
        print files


Example output:

.. code-block:: text
    :caption:  Some example caption

    When text is indented underneath a code-block directive, it renders
    in a pretty code-block.

.. code-block:: python
    :emphasize-lines: 3, 5-6

    import os

    print os.getcwd()
    print 'foo'
    files = os.listdir(os.getcwd())
    print files


RST supports a default directive by a line followed by two colons -- let's not use
this in our docs.  The reason for this is that it breaks future linters.


.. code-block:: text
    :caption: Don't do this

    Some example text::

        This is pre-formatted text


This project has a custom Pygments lexxer that will automatically transform various
prompts into pretty formats.

As an example:

.. code-block:: text

    .. code-block:: prompt

        apt-get -y install latexmk
        apt-get -y install python-pip

Will display these lines, which can be copied by the user:

.. code-block:: prompt

    apt-get -y install latexmk
    apt-get -y install python-pip

Filenames
---------
Use only lowercase alphanumeric characters and _ (underscore) symbol.

Section headers
---------------
Sphinx makes use of 'automatic depth' for section headers.
For example, these are the section titles

.. code-block:: text
    :caption: example documentation headers

    ##################
    H1: document title
    ##################

    Introduction text.


    *********
    Sample H2
    *********

    Sample content.


    **********
    Another H2
    **********

    Sample H3
    =========

    Sample H4
    ---------

    Sample H5
    ^^^^^^^^^

    Sample H6
    """""""""

    And some text.

Line Length
-----------
Line-length is enforced at `85` characters.  This is enforced by a tox.ini doc8
linter.  85-characters allows compatibility with the pylintrc in the leblon project
so we don't have to change our IDE settings.  The default line-length for doc8
and rst-lint is `79` characters. Enjoy the extra 6 bytes!

Line length fails linting in the `rst-lint` and `doc8` package when using literal
directives (text with :: at the end, followe dby indented text).

Line length linting passes when using ``code-block`` directives, so use these
instead.  Code-block directives do not have restrictions on line-length.

Be wary when embedding text and code samples, as it can cause PDF to look terrible.

UI documentation
----------------

When documenting 'breadcrumbs' in the AOS Web UI, use single tics '`' character.

This is just a documentation style guide.

Tables
------

Tables can be complex to write with lots of overhead when trying to format the table
itself.  Focus on ease-of-updating rather than readability within the RST document.

For example, the AOS network requirements document included below:

.. include:: /includes/network_requirements.rst



Document Development
====================

There is infrastructure set up on the internal Jenkins server to spin off build
processes.

After writing a documentation update, submit the documentation change to bitbucket.
https://bitbucket.org/apstrktr/doctools/ -- you can even do this online!

You can get started without having to install these dependencies yourself by
running ``make`` in the doctools root folder - only requirement is Docker should
be installed.

After this is done, run ``start.sh``.

Required packages
-----------------

Required packages on your ubuntu server should be installed first.

The texlive packages are fairly large, so be paitent.


.. code-block:: prompt

    sudo apt-get update && sudo apt-get install -y \
        python2.7 \
        python-pip \
        graphviz \
        texlive-latex-recommended \
        texlive-fonts-recommended \
        texlive-latex-extra \
        latexmk


To build documentation locally, follow these steps:

.. code-block:: prompt

    git clone git@bitbucket.org:apstrktr/doctools.git
    virtualenv venv
    source venv/bin/activate


.. code-block:: text
    :emphasize-lines: 1,9,15
    :caption: Installing development environment

    jp@ApstraVM /tmp/sample $ git clone git@bitbucket.org:apstrktr/doctools.git
    Cloning into 'doctools'...
    remote: Counting objects: 776, done.
    remote: Compressing objects: 100% (764/764), done.
    remote: Total 776 (delta 531), reused 0 (delta 0)
    Receiving objects: 100% (776/776), 1.98 MiB | 1.53 MiB/s, done.
    Resolving deltas: 100% (531/531), done.
    Checking connectivity... done.
    jp@ApstraVM /tmp/sample $ cd doctools
    jp@ApstraVM /tmp/sample/doctools $ virtualenv venv
    Running virtualenv with interpreter /usr/bin/python2
    New python executable in /tmp/sample/doctools/venv/bin/python2
    Also creating executable in /tmp/sample/doctools/venv/bin/python
    Installing setuptools, pkg_resources, pip, wheel...done.
    jp@ApstraVM /tmp/sample/doctools $ source venv/bin/activate
    (venv) jp@ApstraVM /tmp/sample/doctools $ pip install -r requirements.txt
    Collecting alabaster==0.7.10 (from -r requirements.txt (line 1))
      Using cached alabaster-0.7.10-py2.py3-none-any.whl
    Collecting astroid==1.5.3 (from -r requirements.txt (line 2))
      Using cached astroid-1.5.3-py2.py3-none-any.whl
    Collecting Babel==2.4.0 (from -r requirements.txt (line 3))
    ...
    Successfully installed <package list>

Running tests
-------------

In production, sphinx-build will be running with the ``-W`` option to treat warnings
as errors.  If there are warnings during the sphinx build process, jenkins will not
publish documentation updates.

To test documentation, run ``tox`` from the doc root..  This runs a simple
documentation linter for various ReStructuredText typos.

The first time that Tox runs it will install any dependencies and cache them
to the ``.tox`` folder.

.. code-block:: text

    (venv) jp@ApstraVM ~/PycharmProjects/doctools/docs $ tox
    lint installed: astroid==1.5.3,backports.functools-lru-cache==1.4,chardet==3.0.4,configparser==3.5.0,doc8==0.8.0,docutils==0.14,enum34==1.1.6,isort==4.2.15,lazy-object-proxy==1.3.1,mccabe==0.6.1,pbr==3.1.1,py==1.4.34,pylint==1.7.2,pytest==3.2.2,restructuredtext-lint==1.1.1,singledispatch==3.4.0.3,six==1.10.0,stevedore==1.26.0,wrapt==1.10.11
    lint runtests: PYTHONHASHSEED='2653954144'
    lint runtests: commands[0] | doc8
    Scanning...
    Validating...
    ========
    Total files scanned = 21
    Total files ignored = 1013
    Total accumulated errors = 0
    Detailed error counts:
        - doc8.checks.CheckCarriageReturn = 0
        - doc8.checks.CheckIndentationNoTab = 0
        - doc8.checks.CheckMaxLineLength = 0
        - doc8.checks.CheckNewlineEndOfFile = 0
        - doc8.checks.CheckTrailingWhitespace = 0
        - doc8.checks.CheckValidity = 0
    ______________________________________________________________ summary ______________________________________________________________
      lint: commands succeeded
      congratulations :)

Committing changes
------------------

After making RST file changes, use standard ``git`` commands (or your GIT UI).

Eg ``git add -u`` to add all changed files, and
``git commit -m "Added externalrouters"``.


Jenkins autobuild
=================

Architecturally, the project is built in two docker phases for production usage.

First, a docker container is built that contains the low-level ubuntu and python
dependencies necessary for a successful build.

Scheduling a jenkins build
--------------------------

First, log into jenkins at http://jenkins.dc1.apstra.com:8080

Then click on the `Documentation tab \ doctools-ci project`

.. image:: /static/images/doc_howto/jenkins_project.png

Click on 'Build with parameters' on the left.

.. image:: /static/images/doc_howto/jenkins_project2.png

The default options are okay for now.

.. image:: /static/images/doc_howto/jenkins_build_parameters.png

You will observe the build in progress:

.. image:: /static/images/doc_howto/jenkins_build_progress.png

If there aren't any errors, there will be a nice green icon, and the documentation
on docs.dc1.apstra.com will be updated.

Architecture
------------
Overview
________
First, we can test all of the below steps with simply running the ``make`` command
in the doctools source folder.

This will locally make all of the product documentation containers.

After this is done, you can then run ``./start.sh`` to start the local nginx
container.

This will not push to docker registry or update docserver unless the environment
variable ``DOCKER_REGISTRY_PUSH`` is equal to ``"true"``


1. Jenkins building
___________________

On Jenkins, we can manually initiate a 'build' job to update docs.dc1.apstra.com
following a successful build.

``../../jenkins_autobuild.sh``

2. Builder container (docweb_build)
___________________________________

Install python dependencies, install PIP dependencies, then build the documentation

Each step is cached - so large ubuntu dependencies will only have to build once.

Packages are installed, then simple RST linters are run first (tox).

Sphinx then builds with ``-W`` (warnings-as-errors) to ensure the build passes.

``Dockerfile.build``


3. Docs container (docweb)
__________________________

In the file ``jenkins_build.sh``, we copy data out from the docker container
into the staging folder.  Within the second build stage, these staging files
are copied into the NGINX image for hosting.

This image is what runs on the docs.dc1.apstra.com webserver.

``Dockerfile.run``


4. Push to docker registry
__________________________

Once these containers are built, they are pushed to the docker registry.
This allows the AOS build process and the documentation website to obtain updates.

5. Update and restart docs.dc1.apstra.com
_________________________________________
Within the jenkins autobuild script, ``sshpass`` is used to restart the docker
container on `docs.dc1.apstra.com`.


Release notes
=============

.. note::
    Section outdated... Moving to JIRA API.. Automation coming soon
    I now have a script under releasenotes/jira_release_notes.py to pull
    JIRA information


This is a small tool to assist with creation of technical information regarding
release notes and the
hardware compatibility list.

It is broken up in a few small peices:

util\hcl.py -- connects to AOS and downloads HCL entries (live)
util\releasenotes.py -- Uses saved RSS (search) filters from JIRA for known issues
and fixed issues.
config.yml -- User-specified information about new and removed features, AOS Server
information, and version information
templates\ - folder containing jinja2 templates for various rendering output formats.

To add a new output format just create a file.

Major output formats today are HTML Files and Confluence wiki markup syntax for
copy-paste.

# To update the HCL matrix
1. Modify the AOS server IP address, hostname, port, username, and password.
2. Run hcl.py - this will connect to AOS, pull the HCL, and populate it to
input/hcl.yml.

# To update  release notes:

NEW:
run jira_release_notes.py
This will prompt you for a username & password to connect to the Jira REST API to
download release note data.

OLD:
Obtain the search result filters of JIRA filters SearchRequest-17701.xml and
SearchRequest-17702.xml
Save these to the 'input' folder (replace existing files)

run releasenotes.py


# To render templates, run render.py
This will use the input files at input/hcl.yml, input/known_issues.yml,
input/fixed_issues.yml, and config.yml

Output files will be in 'output' directory.

How to quickly fix a typo
=========================

#. Go to http://docs.dc1.apstra.com/ and find the page you want to change.
#. Click "Edit on Bitbucket" in the top-right.
#. Click "Edit"
#. Make you changes
#. Click "Commit"
#. Check "Create a pull request for this change"
#. Add reviewers (at least JP and Jonathan G.). For JP, type "senior" for
   auto-completion to work (needs 3 characters).
#. Click "Commit" again
#. Thank you!


