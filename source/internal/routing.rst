******************
Leaf-Spine Routing
******************

Introduction

What is a reference design?

While this is not a full reference design document, this is particular to a
two-stage L3 CLOS network.  A two-stage L3 CLOS environment is made up of a
series of spines and a series of routers.

AOS 1.2 ships with a default reference design, 'two stage l3 clos'.


+++++++++++++++++
Two-Stage L3 CLOS
+++++++++++++++++
This architecture is designed to maximize throughput and performance both
*east-west* and *north-south* in the datacenter.

East-West
   East-west architecture describes server-to-server traffic in the datacenter.
    * This traffic traverses through uplinked leafs, through spines, to other leafs,
      then back to the server is is destined for.
    * Traffic is not guaranteed to be symmetric - that is, a different path for
      transmit vs receive is acceptable
    * Connections between spine <-> leaf require enough bandwidth to allow for
      every server to communicate at the same rate at the same time

    .. graphviz::

        digraph G {
            label="East-West traffic";
            labelloc="top";
            {
                spine1
                spine2
                rank=same
            }

            {
                leaf1
                leaf2
                rank=same
            }

            {
                server1
                server2
                rank=same
            }

            subgraph fabric {
                edge [arrowhead=none]
                spine1 -> leaf1;
                spine1 -> leaf2;
                spine2 -> leaf1;
                spine2 -> leaf2;
                leaf1 -> server1;
                leaf2 -> server2;
            }
            subgraph transmit {
                edge [color=green];
                server1 -> leaf1 -> spine1 -> leaf2 -> server2;
            }
            subgraph receive {
                edge [color=red];
                server2 -> leaf2 -> spine2 -> leaf1 -> server1;
            }
        }

North-South
  Describes traffic from servers existing the datacenter (to an external router)
   * Requires all leafs, spines, and interfaces connected to the router to allow for
     1:1 oversubscription
   * External routers generate default routes to inject into datacenter fabric

   .. graphviz::

        digraph G {
            label="North-South datacenter traffic";
            labelloc="top";
            {
                default [label="0.0.0.0"]
            } ->
            {
                router1
                router2
                rank=same
            } ->
            {
                spine1
                spine2
                rank=same
            } ->

            {
                leaf1
                leaf2
                rank=same
            } ->
            {
                server1
                server2
                rank=same
            } [style=invis]

            subgraph fabric {
                edge [arrowhead=none]
                default [label="0.0.0.0"];
                default -> router1;
                default -> router2;
                router1 -> spine1;
                router1 -> spine2;
                router2 -> spine1;
                router2 -> spine2;
                spine1 -> leaf1;
                spine1 -> leaf2;
                spine2 -> leaf1;
                spine2 -> leaf2;
                leaf1 -> server1;
                leaf2 -> server2;
            }
            subgraph transmit {
                edge [color=green];
                server1 -> leaf1 -> spine1 -> router1 -> default;
            }
            subgraph receive {
                edge [color=red];
                default -> router2 -> spine2 -> leaf1 -> server1;
            }
        }

ECMP
====
AOS will render and expect multiple, equal-cost multipath routes in a datacenter
fabric.

The requirements for ECMP on the AOS reference design are relaxed, and BGP options
such as **foo** are used.

.. todo:: Show 'show ip route' output of an ECMP route from an Arista EOS switch

For as many links are available in the datacenter fabric, AOS will make the most
use of the network.

Consider the topology below --

.. graphviz::

    digraph G {
        label="Server-Server ECMP";
        labelloc="top";
        node [shape=record];
        edge [arrowhead=none];

            {
                default [label="0.0.0.0"]
            } ->
            {
                router1
                router2
                rank=same
            } ->
            {
                spine1
                spine2
                rank=same
            } ->

            {
                leaf1
                leaf2
                leaf3
                leaf4
                rank=same
            } ->
            {
                server1
                server2
                server3
                server4
                server5
                server6
                server7
                server8
                rank=same
            } [style=invis]
        subgraph fabric {
            edge [color=gray];
            default -> router1;
            default -> router2;
            router1 -> spine1;
            router1 -> spine2;
            router2 -> spine1;
            router2 -> spine2;
            #spine1 -> leaf1;
            #spine1 -> leaf2;
            spine1 -> leaf3;
            #spine1 -> leaf4;
            #spine2 -> leaf1;
            spine2 -> leaf2;
            spine2 -> leaf3;
            #spine2 -> leaf4;
            spine1 -> leaf1;
            #spine1 -> leaf2;
            spine1 -> leaf3;
            spine1 -> leaf4;
            spine2 -> leaf1;
            spine2 -> leaf2;
            spine2 -> leaf3;
            spine2 -> leaf4;
        }

        #leaf1 -> server1;
        #leaf1 -> server2;
        #leaf2 -> server3;
        #leaf2 -> server4;
        leaf3 -> server5;
        leaf3 -> server6;
        #leaf4 -> server7;
        #leaf4 -> server8;

        subgraph traffic {
            edge [color = green];
            server1 -> leaf1 -> spine2 -> leaf4 -> server7;
            server4 -> leaf2 -> spine1 -> leaf1 -> server2;
            server8 -> leaf4 -> spine1 -> leaf2 -> server3;
        }
    }

There are 2 links configured between each spine - let's assume all links are
running at 10Gbps.
We have three traffic flows:  *server1->server7*, *server4->server2*,
*server8->server3*.

This traffic flow uses only a small portion of the total available throughput on
the wire.  Even if **all** of the servers were pushing traffic at 100% of the wire,
there will still be available throughput on the wire.

At no point in time can any server flow cause traffic disruption for any other
server - there are more
than enough links to go around.

.. todo::

    Insert headroom data snapshot of this topology with available headroom to
    demonstrate ECMP

Router placement

Routers can be placed on either a spine or a leaf, but not both.

When placing routers on spines, the router must be connected to all spines.


Default route expectations (0.0.0.0)

.. graphviz::

    digraph G {
        node [
            shape=record, style="filled", fillcolor="#003399",
            fontcolor="#e6f2ff", color="#0066ff", fontname="Helvetica-Narrow"];
            edge [color="#483D8B", fontsize="11", labelfontcolor="#808080",
            arrowhead="none"
        ];
        default_route [label="0.0.0.0" shape=ellipse];
        default_route -> router1;
        default_route -> router2;

        router1 -> spine1 [taillabel=" .1", headlabel=".0"];
        router2 -> spine1 [taillabel=" .3", headlabel=".2"];
        router1 -> spine2 [taillabel=" .5", headlabel=".4"];
        router2 -> spine2 [taillabel=" .7", headlabel=".6"];

        spine1 -> leaf1 [taillabel=" .8", headlabel=".9"];
        spine1 -> leaf2 [taillabel=" .10", headlabel=".11"];
        spine2 -> leaf1 [taillabel=" .12", headlabel=".13"];
        spine2 -> leaf2 [taillabel=" .14", headlabel=".15"];

        leaf1 -> server1 [taillabel=" .16", headlabel=".17"];
        leaf1 -> server2 [taillabel=" .18", headlabel=".19"];
        leaf2 -> server3 [taillabel=" .20", headlabel=".21"];
        leaf2 -> server4 [taillabel=" .22", headlabel=".23"];
    }

AOS calculates the path to the default route with a shortcut.  Since we know this
is a two-stage l3 clos network, we know that if a network is not attached on a leaf,
then we must assume it’s attached via uplink spines.

Route Telemetry rendering enumerates all interfaces on all nodes

Route Telemetry
---------------

AOS will generate *telemetry expectations* based on the graph blueprint.  These
telemetry expectations are used whenever the blueprint is committed, and
re-calculated.  Only certain device roles will render telemetry expectations.

Device roles

    * All connected networks between a leaf and an L3 server in a deployed state,
      with the exception of servers hosted on the leaf itself.
    * All VLAN SVI virtual networks with active endpoints on remote leafs
      Next-hops will be all spine_leaf interfaces that the leaf is connected to.
    * AOS can make a design assumption based on the two-stage-l3-clos architecture,
      and knows that the only way to reach another leaf is through all of its spine
      uplinks.

.. graphviz::

    digraph G {
        label="Leaf to Server& SVI route expectations";
        labelloc="top";
        edge [arrowhead=none]
        {
            spine1;
            spine2;
            rank=same
        } ->
        {
            leaf1;
            leaf2;
            rank=same
        } ->
        {
            server;
            svi;
            rank=same
        } [style=invis]

        spine1 -> leaf2;
        spine2 -> leaf2;

        subgraph next_hops {
            edge [dir=back, arrowhead=normal, color=blue];
            spine1 -> leaf1;
            spine2 -> leaf1;
        }

        subgraph expected {
            edge [color=green];
            leaf2 -> server;
            leaf2 -> svi;
        }
     }


If a spine or a leaf has any links connected to an external router, it is marked as
 a ‘router connected device’. Other devices in the L3CLOS network will then
 install a 0.0.0.0/0 route facing this external router.




What is two-stage l3 clos ?
What is a clos?

Designing for oversubscription


Routing policy in AOS
=====================
Fds

Prefix-list functionality
-------------------------
AOS dynamically creates *prefix-lists* based on the graph blueprint.  A few critical
prefix-lists are used:

RoutesToExt
    A list of routes that AOS will allow to be advertised to an external router

RoutesFromExt
    A list of routes that AOS will *accept* from an external router


AS-Path filtering
-----------------
add/remove/update
Prefix-list compression
Prefix-list sources

When AOS renders


Config samples
Path-hunting protection
Internal vs External routes

MLAG redundancy over peer-link
Engineering section:
Route-policy objects
This section describes the ‘route_policy’ object in the AOS graph blueprint.
Telemetry
fsdfsd
Link roles

Device roles


CAlculating next-hops

Graph shortcuts

Anomalies - what we can do
Anomalies - What we can’t do

Idea gathering
