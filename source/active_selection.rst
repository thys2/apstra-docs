=========
Selection
=========
Selecting an element provides information about the element.
From the selection you can see information about telemetry, device, properties,
and if a hypervisor is hosted on the node, VMs.

.. image:: static/images/blueprints/active/physical/active_selection_panel_330.png
