=====
Racks
=====
AOS provides full control over the growth of your network by enabling you to add,
export, edit, and delete complete racks in a running blueprint. This flexible
fabric expansion (FFE) feature is supported on 3-stage and 5-stage Clos networks.
These capabilities apply to racks in 5-stage topologies, but not to the pods
themselves. It is not possible to add or remove pods, superspine planes or
superspine devices in a running blueprint, so ensure that the day 0 template is
correct.

.. image:: static/images/blueprints/staged/physical/racks/staged_physical_rack_330.png

Embedded Rack Types
===================
Rack Types have timestamps in the Blueprints. When the Blueprints are modified via
"Flexible Fabric Expansion (FFE)" operations such as Changing Link Speeds,
Adding Servers, Adding & Removing Links, etc., AOS will modify the Embedded Rack
Types accordingly then update the timestamps.

In the example below, both Rack Types have the timestamp "2021-01-12 10:20".

.. image:: static/images/blueprints/staged/physical/racks/rack_update_01.png

And the next screenshot is taken after the "Add Leaf to Server Links" FFE operation.
Although the Rack Type name is the same, the contents of the Rack Type has been
modified by AOS.

.. image:: static/images/blueprints/staged/physical/racks/rack_update_03.png

Changing Rack Name
==================
You may want to use your own rack naming schema (for example, your rack names
could be based on their physical locations). In these cases you can modify the
existing rack names (as of AOS version 3.3.0).

#. From the blueprint, navigate to **Staged / Physical / Racks**.
#. Choose the rack that needs a name change.
#. In **Rack Properties** (right panel) click the **Edit** button for the rack name.
#. Change the name and click the **Save** button to stage the change.

.. _add_rack:

Adding Rack
===========
The easiest and fastest way to expand your network is to add a rack.

#. From the blueprint, navigate to **Staged / Physical / Racks**.
#. Click the **Add Racks** button (+), and if your blueprint is for a 5-stage
   topology, select the pod that needs a rack.
#. From the **Rack Type** drop-down list, select a rack type to preview and
   validate. To view a different preview, select a different rack type.
#. Enter the number of racks to add.
#. If you uncheck **Keep existing cabling in the fabric after change**, AOS
   re-calculates port assignments. This may result in the need for re-cabling.
   When in doubt, leave the box checked.
#. Click **Add** to stage the rack addition and return to the list view.
#. :ref:`Assign device profiles <staging_device_profiles>` and
   :ref:`system IDs (serial numbers) <staging_devices>` to the new rack(s).
#. Commit the changes to your blueprint to configure the rack(s) and complete the
   fabric expansion.

Exporting Rack
==============
Some changes cannot be made directly in the blueprint rack type (for example,
changing port channel id ranges). In these cases, you can export the rack type
to the global catalog, update it there, then from the blueprint, edit the rack
type to use the modified one from the global catalog.

#. From the blueprint, navigate to **Staged / Physical / Racks**.
#. Click the **Export rack to global catalog** button.
#. Give the **Rack Type** a unique name.
#. Click **Export** to export the rack type to the global catalog.
#. Navigate to **Design / Rack Types** and edit the rack type from there.

.. _edit_rack:

Editing Rack
============
Existing running racks can be changed while preserving many of their
characteristics (as of AOS version 3.2.0). Depending on the change, editing a rack
may require re-assigning device profiles and system IDs (serial numbers).

.. warning::

  * (Re)assigning device profiles and system IDs results in a full config push to
    those devices when committing the blueprint. This is service impacting.
  * When editing a rack it is not always possible to retain virtual network
    endpoints. If not, you will have to re-assign the endpoints. When in doubt, you
    can always review your changes before committing. If there are undesired changes
    you can revert them from the **Uncommitted** tab.

.. important::

  Editing a rack is not a simple 'Delete & re-add' operation: it is not a complete
  replacement of a rack. Instead, when editing a rack, AOS tries to retain as much
  of the original data (such as virtual network endpoints,
  leaf / server / link names, etc) as possible. If such preservation is not needed,
  it is recommended perform a 'Delete & Add' instead of an Edit.

Typically, a rack edit operation involves the following steps:

#. :doc:`Ensure that a Rack exists in AOS<rack_types>` that meets your design
   requirements.
#. From the blueprint, navigate to **Staged / Physical / Racks**.
#. Click the **Edit** button for the rack to edit.
#. From the **New Rack Type** drop-down list, select the required rack type.
#. If new devices were added, :ref:`assign device profiles
   <staging_device_profiles>` and :ref:`system IDs (serial numbers)
   <staging_devices>` to them.
#. Optionally, review the **Incremental Config** to see the changes AOS will push
   to the device(s). If devices were assigned, a full config push is performed.
#. Commit the changes to the blueprint. AOS will push all required configuration
   changes to the devices in the modified Rack.

.. note::

     Virtual network (VN) endpoints will remain as long as the server and link
     labels between the old and new rack type are the same.

.. _delete_rack:

Deleting Rack
=============
Before deleting a rack that has live traffic on it, you may want to take its
devices out-of-service gracefully by draining them. See the
:doc:`device draining guide <device_drain>` for more information.

#. From the blueprint, navigate to **Staged / Physical / Racks**.
#. Click the **Delete** button (trash can) for the rack to delete.

   * If you will be adding a rack back into your system, leave the
     **Keep existing cabling in the fabric after change** box checked.
   * If you will *not* be replacing the rack in your system, uncheck the
     **Keep existing cabling in the fabric after change** box. Otherwise,
     the intent will not match the actual topology anymore, and you will
     encounter anomalies, such as for cabling and BGP.

#. Click **Delete Rack** to stage the deletion and return to the list view.
#. Commit the changes to the blueprint. Configuration on any running devices
   will be erased and the devices will be ready to be decommissioned.
