=======================
Processor: Match String
=======================
The Max String processor checks that a string matches a regular expression.
It accepts text series on input, for each series it configures a check that
verifies if the input value matches the configured regular expression. Regular
expression syntax is PCRE-compatible. Note that regexp matching is done in a
partial mode, so if the full match is needed, regular expression needs to be
specified accordingly. The output series contains anomaly values, such as
'false' and 'true'.

**Input Types** - Time-Series (TS), TSTS

**Output Types** - Discrete-State-Set (DSS)

**Properties**

   .. include:: includes/processors/graph_query.rst

   .. include:: includes/processors/non_collector_graph_query.rst

   Regular Expression (regexp)
      Expression that evaluates to a PCRE-compatible regular expression.
   Anomaly MetricLog Retention Duration
      Retain anomaly metric data in MetricDb for specified duration in seconds
   Anomaly MetricLog Retention Size
      Maximum allowed size, in bytes of anomaly metric data to store in MetricDB
   Anomaly Metric Logging
      Enable metric logging for anomalies

   .. include:: includes/processors/enable_streaming.rst

   .. include:: includes/processors/raise_anomaly.rst

Match String Example
--------------------

.. code-block:: none

    regexp: "os_version_pattern"

Sample Input (TS)

.. code-block:: none

  [device=leaf1,os_version_pattern=^4.[7-9].[0-9]+$] : 4.1
  [device=leaf2,os_version_pattern=^4.[7-9].[0-9]+$] : 4.7

Sample Output (DSS):

.. code-block:: none

  [device=leaf1,os_version_pattern=^4.[7-9].[0-9]+$,regex=^4.[7-9].[0-9]+$] : "true"
  [device=leaf2,os_version_pattern=^4.[7-9].[0-9]+$,regex=^4.[7-9].[0-9]+$] : "false"
