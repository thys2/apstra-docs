=============================
Property Set API Reference
=============================
For full API documentation, view the Platform API reference under the AOS Web UI
This is a targeted section to demonstrate property sets API similarly to the UI.

Property sets live in http://aos-server:8888/api/property-sets and are
referenced by ID.


.. code-block:: json
    :caption: Property Set schema

        {
          "items": [
            {
              "label": "string",
              "values": {
                "additionalProp1": "string",
                "additionalProp2": "string",
                "additionalProp3": "string"
              },
              "id": "string"
            }
          ]
        }


API - Creating a property set
_____________________________
To create a property set, POST to https://aos-server/api/property-sets with a
valid JSON structure representing the property set.
Creating a property set this way only allows it to be available for assignation
in the AOS Web UI - it is not required in this method for the REST API to assign
to a blueprint. See the assigning a property set section for more details.

A POST will create a new property set.
A PUT will overwrite an existing property set. PUT requires the URL of the
property set.
https://aos-server:8888/api/design/property-sets/{id}

.. code-block:: text

    curl -H "AuthToken: EXAMPLE" -d '{"values": {"NTP_SRV1": "192.168.1.1", "NTP_SRV1": "192.168.1.1"}, "label": "NTP-servers"}' -X POST "http://aos-server:8888/api/design/property-sets"

The response will contain the ID of the newly created property-set
``{"id": "73223e81-a451-4e7f-91fb-fb476f4b9fc8"}``

API - Deleting a property set
_____________________________
Deleting a property set requires an HTTP DELETE to the property set by URL
http://aos-server:8888/api/design/property-sets/{id}


.. code-block:: text
    :caption: CURL Example - HTTP DELETE

    curl -H "AuthToken: EXAMPLE" -X DELETE "http://aos-server:8888/api/design/property-sets/73223e81-a451-4e7f-91fb-fb476f4b9fc8"

A successful DELETE has an empty response ``{}``


API - Assigning a property set
______________________________
Assigning a property set to a blueprint requires an HTTP POST to the blueprint by URL
http://aos-server:8888/api/blueprints/{blueprint_ID}/property-sets

.. code-block:: json

    {
         "id": "73223e81-a451-4e7f-91fb-fb476f4b9fc8"
    }

The response will contain the ID of the assigned property-sets
``{"id": "73223e81-a451-4e7f-91fb-fb476f4b9fc8"}``

CURL Example - API HTTP PUT
____________________________

.. code-block:: text

    curl "http://aos-server:8888/api/blueprints/e4068e99-813c-4290-b7cc-e145d85a98a8/property-sets" -X PUT -H "AuthToken: EXAMPLE" -H "Content-Type: application/json; charset=utf-8" --data "{"id":"73223e81-a451-4e7f-91fb-fb476f4b9fc8"}"

Response

.. code-block:: json

    {"id": "73223e81-a451-4e7f-91fb-fb476f4b9fc8"}

API - Unassigning a property set
________________________________
Deleting a property set requires an HTTP DELETE to the blueprint property set by URL
http://aos-server:8888/api/blueprints/{blueprint_ID}/property-sets{id}


.. code-block:: text
    :caption: CURL PUT example

    curl "http://aos-server:8888/api/blueprints/e4068e99-813c-4290-b7cc-e145d85a98a8/property-sets/73223e81-a451-4e7f-91fb-fb476f4b9fc8" -X DELETE -H "AuthToken: EXAMPLE"

A successful DELETE has an empty response ``{}``
