==========================
|dashboard_icon| Dashboard
==========================

.. |dashboard_icon| image:: static/images/icons/dashboard_icon.png

Blueprint Dashboard Overview
============================

The blueprint dashboard shows the overall health and status of a committed
blueprint. Statuses are indicated by color: green for succeeded, yellow for
pending, and red for failed. The deployment status section includes deployment
statuses for service config, discovery config, and (as of AOS version 3.3.0) drain
config. The anomalies section includes statuses for all probes, IP fabric,
external routing, L2 connectivity, liveness, deployment status, and route
verification. The nodes status section includes statuses for deployment,
BGP, cabling, config, interface, liveness, route, and hostname.

.. _access_blueprint:

From the :doc:`AOS web interface <aos_ui>`, click **Blueprints**
(in left menu), then click the name of a blueprint to go to its dashboard.

.. image:: static/images/blueprint-tasks/blueprint_summary_330.png

.. image:: static/images/blueprints/dashboard/dashboard_blueprint_330.png

Deleting Blueprint
==================
AOS administrators can delete blueprints.

#. From the blueprint, navigate to **Dashboard**, then click **Delete Blueprint**
   (top-right).

   .. image:: static/images/blueprints/dashboard/dashboard_delete_blueprint_330.png

#. Enter the blueprint name, then click **Delete** to  delete the blueprint
   and go to the blueprint summary view.
