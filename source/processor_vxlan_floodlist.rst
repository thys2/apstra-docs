==========================
Processor: VXLAN Floodlist
==========================
The VXLAN Floodlist processor generates a configuration containing expectations
of vxlan floodlist routes.

**Input Types**

**Output Types**

**Properties**

   Execution count
      Number of times the data collection is done.

   .. include:: includes/processors/service_input.rst

   .. include:: includes/processors/service_interval.rst

   .. include:: includes/processors/service_name.rst

   .. include:: includes/processors/enable_streaming.rst
