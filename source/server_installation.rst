=======================
AOS Server Installation
=======================
Apstra Operating System (AOS) is delivered pre-installed on a single virtual
machine (VM) in OVA and QCOW2 formats. Registered support users can download AOS
Server and agent software from https://portal.apstra.com. Before deploying AOS,
refer to
:doc:`AOS Server Requirements and Other References <server_requirements>`
for information about hypervisor requirements, VM resource requirements,
network protocols, network client services, server hardening
and the AOS configuration file.

.. note::

    Apstra provides the following examples of hypervisor installation. The user
    will need to seek assistance from their hypervisor provider (e.g. VMware,
    Ubuntu, RedHat, Microsoft) or the open-source community for specific
    hypervisor use instructions.

1. Install the AOS Server VM on one of the following Hypervisors:

.. toctree::
   :maxdepth: 2

   install_aos_on_vmware
   install_aos_on_kvm
   install_aos_on_hyperv
   install_aos_on_virtualbox

2. Configure the AOS Server

.. toctree::
   :maxdepth: 2

   configure_aos
