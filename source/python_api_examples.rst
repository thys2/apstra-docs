Using API From Python
---------------------
Following is examples of Python3 code using the AOS API.

API User Login
``````````````

.. code-block:: python

    import requests, sys

    # IP of Cloudlabs AOS Server
    aos_server = '172.16.90.3'
    username = 'admin'
    password = 'aos aos'

    # authenticate and get a auth token
    url = 'https://' + aos_server + '/api/user/login'
    headers = { 'Content-Type':"application/json", 'Cache-Control':"no-cache" }
    data = '{ \"username\":\"' + username + '\", \"password\":\"' + password + '\" }'
    response = requests.request("POST", url, data=data, headers=headers, verify=False)
    print('POST',url,response.status_code)
    if response.status_code != 201:
        sys.exit('error: authentication failed')
    auth_token = response.json()['token']
    print(auth_token)
    headers = { 'AuthToken':auth_token, 'Content-Type':"application/json", 'Cache-Control':"no-cache" }

API Blueprints
``````````````

.. code-block:: python

    # get blueprint ID ... assuming there is only one
    url = 'https://' + aos_server + '/api/blueprints'
    response = requests.request('GET', url, headers=headers, verify=False)
    print('GET', url, response.status_code)
    blueprint_id = response.json()['items'][0]['id']
    blueprint_name = response.json()['items'][0]['label']
    print(blueprint_name, blueprint_id)

API Blueprints Racks
````````````````````

.. code-block:: python

    # get a list of racks
    bound_to = ''
    url = 'https://' + aos_server + '/api/blueprints/' + blueprint_id + '/racks'
    response = requests.request('GET', url, headers=headers, verify=False)
    print('GET', url, response.status_code)
    for item in response.json()['items']:
            bound_to += '{\"system_id\":\"' + item['leafs'][0]['id'] + '\"},'
    bound_to = bound_to[:-1]
    print(bound_to)

API Blueprints Security-Zones
`````````````````````````````

.. code-block:: python

    # get security zone ID ... assuming there is only one
    url = 'https://' + aos_server + '/api/blueprints/' + blueprint_id + '/security-zones'
    response = requests.request('GET', url, headers=headers, verify=False)
    print('GET', url, response.status_code)
    for item in response.json()['items']:
        if(response.json()['items'][item]['vrf_name'] != 'default'):
            security_zone_name = response.json()['items'][item]['vrf_name']
            security_zone_id = item
            break
    print(security_zone_name, security_zone_id)

API Blueprints Virtual-Networks
```````````````````````````````

.. code-block:: python

    # create a virtual network
    vn_name = "My-VN"
    url = 'https://' + aos_server + '/api/blueprints/' + blueprint_id + '/virtual-networks'
    data = '{\"label\":\"' + vn_name + '\",\"vn_type\":\"vxlan\",\"bound_to\":[' + bound_to + '],\"security_zone_id\":\"' + security_zone_id + '\"}'
    print(data)
    response = requests.request('POST', url, data=data, headers=headers, verify=False)
    print('POST', url, response.status_code)

Running Python
``````````````

The proceeding Python3 code can be run on the Cloudlabs AOS Server. Use the
``python3.6`` command to run the Python script.

.. code-block:: bash

    admin@aos-server:~$ python3.6 test.py
    POST https://192.168.3.3/api/user/login 201
    eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6ImFkbWluIiwiY3JlYXRlZF9hdCI6IjIwMjAtMDItMjFUMTc6NDM6NTkuNTU1MDQzIiwidXNlcl9zZXNzaW9uIjoiMmU0Y2YwODktNzZmYS00NDg4LTlhNmItYWViYjc3MmQyNDE2IiwiZXhwIjoxNTgyMzkzNDM5fQ.GWsy292pfpPVpisbQNKc3EHrDlxh1OUmpQaQ-dF-mwY
    GET https://192.168.3.3/api/blueprints 200
    neil-blueprint cbfe7a43-4da7-4b2c-90a2-ea0bae4ed79a
    GET https://192.168.3.3/api/blueprints/cbfe7a43-4da7-4b2c-90a2-ea0bae4ed79a/racks 200
    {"system_id":"2cbb0fc0-5f87-4671-8d8b-e909cbf84fdd"},{"system_id":"98002bb9-d0a9-484c-86e7-2aac2b926bf7"},{"system_id":"73bd231c-f78e-499f-bf98-fa80c1102a4a"},{"system_id":"19fb6155-e9eb-4ae7-b5b3-933416f0e3cd"}
    GET https://192.168.3.3/api/blueprints/cbfe7a43-4da7-4b2c-90a2-ea0bae4ed79a/security-zones 200
    Finance 4aaa4499-3194-4904-a1ae-daabbe3ed329
    {"label":"My-VN","vn_type":"vxlan","bound_to":[{"system_id":"2cbb0fc0-5f87-4671-8d8b-e909cbf84fdd"},{"system_id":"98002bb9-d0a9-484c-86e7-2aac2b926bf7"},{"system_id":"73bd231c-f78e-499f-bf98-fa80c1102a4a"},{"system_id":"19fb6155-e9eb-4ae7-b5b3-933416f0e3cd"}],"security_zone_id":"4aaa4499-3194-4904-a1ae-daabbe3ed329"}
    POST https://192.168.3.3/api/blueprints/cbfe7a43-4da7-4b2c-90a2-ea0bae4ed79a/virtual-networks 201
    admin@aos-server:~$
