======
Agents
======

Agents Overview
===============

Devices connected to AOS are managed with device agents. AOS provides an
integrated agent installer that automates installation and verification of AOS
agents on network devices. Use this installer in environments without a
:doc:`ZTP Server <apstra_ztp>` or for one-off agent installs. The agents handle
configuration management, device-to-server communication, and telemetry collection.

On-box Agents
-------------

On-box agents are installed on the device itself. AOS supports on-box agents for
Arista EOS, Cisco NX-OS, Cumulus Linux, and Enterprise SONiC. On-box
agents require a user with **full admin / root privileges** which AOS will use to
configure the box. It is recommended to create a dedicated user on the device using
:doc:`ZTP <apstra_ztp>` or other means.

.. important::
    It is not possible to change to a different user without complete
    re-onboarding of the device. Changing the password involves several steps
    (password change on the device, device agents and 'Pristine config')
    and is not straightforward. If a password change is required, it is
    recommended to contact :doc:`Apstra Global Support <support>`.

Off-box Agents
--------------

An AOS off-box agent is not installed on the device; it runs on AOS server-side,
and communicates with the device through its API to deploy configuration and
collect :doc:`telemetry data <telemetry>`. AOS supports off-box agents for
Arista EOS (eos), Cisco NX-OS (nxos) and Juniper Junos (junos). Before you can
enable these devices for off-box deployment, you must configure them a specific
way, and upload required packages.

Agent Details
-------------

AOS agents include the following details:

    Device Addresses (25 max)
        Management IP(s) of the device(s) to add

    Agent Type
        Onbox
           Installed on the switch.

           Platforms: Arista EOS, Cisco NX-OS, Cumulus Linux, and Enterprise SONiC.

        Offbox
           Runs on the AOS server and communicates with the switch through the
           device API.

           Platforms: Arista EOS (eos), Cisco NX-OS (nxos), Juniper Junos (junos)

    Operation Mode
        Full Control
           Deploys configuration and collects telemetry

        Telemetry Only
           Only telemetry is collected

    Platform
        For off-box agents only: Enter ``eos``, ``nxos``, or ``junos`` exactly, as
        applicable.

    Username and Password
        Check these boxes if they are not provided by an
        :doc:`agent profile <agent_profiles>`.

    Enabled? (AOS version 3.1 and earlier)
        Check this box to activate. Leave blank when you want to create the agent,
        but don't want to run it yet.

    Agent Profile (optional)
        Useful when you have several devices with the same profile.
        :doc:`Create system agent profiles <agent_profiles>` before creating agents.

    Job to run after creation (AOS version 3.2 and later)
        Select **Install** (default) to install the AOS on-box agent on the device.
        Select **Check** to skip the install of the AOS on-box agent on the device.
        The device will be listed and the AOS on-box agent can be installed later.

    Install Requirements (AOS version 3.1 and earlier)
        Unchecked (default)
            User must manually install any dependencies to successfully create
            the agent.
        Checked
            AOS automatically installs any dependencies. Internet access is
            required per pip or apt package managers.

    Intent (AOS version 3.1 and earlier)
        Install

        Uninstall

    Install Policy (AOS 3.1 and earlier)
        Automatic

        Once

    Packages
        If you have previously installed packages, they will be available here for
        selection. If you selected an :doc:`agent profile <agent_profiles>` that
        has packages associated with it, they will be listed here as well.

    Open Options (Off-box agents only)
        This passes configured parameters to the AOS off-box agent. An example,
        may be to use HTTPS as the API connection from the AOS off-box agent to
        the device API. Use the following key-values in this case:

        +----------+----------+
        | key      | value    |
        +----------+----------+
        | proto    | https    |
        +----------+----------+
        | port     | 443      |
        +----------+----------+

From the AOS web interface, navigate to **Devices / Agents**.

.. image:: static/images/device_management/agent_330.png

Before Creating Agent
=====================
Before creating an agent, make sure that there is IP connectivity on the management
network between the devices and AOS, that login credentials exist on the devices,
and that any packages that your device requires is uploaded to the AOS controller.

.. _minimum_required_configuration:

On-Box Agent Minimum Configuration
----------------------------------

Arista EOS and Cisco NX-OS devices require the below minimal configuration
before installing AOS on-box device system agents. Other than Management Network
and privileged user access, Cumulus Linux and SONiC have no specific
configuration requirements. AOS on-box device system agents are not supported on
Juniper Junos devices.

    .. code-block:: prompt
        :caption: Arista EOS On-box Agent Minimum Configuration

        !
        service routing protocols model multi-agent
        !
        aaa authorization exec default local
        !
        username admin privilege 15 role network-admin secret <admin-password>
        !
        interface Management1
           ip address <address>/<cidr>
        !
        ip route vrf management 0.0.0.0/0 <management-default-gateway>
        !

    .. code-block:: prompt
        :caption: Cisco NX-OS On-box Agent Minimum Configuration

        !
        copp profile strict
        !
        username admin password <admin-password> role network-admin
        !
        vrf context management
          ip route 0.0.0.0/0 <management-default-gateway>
        !
        interface mgmt0
          ip address <address>/<cidr>
        !

Off-Box Agent Minimum Configuration
-----------------------------------

.. Off-box agents will be supported on SONiC after 3.3.0a

Arista EOS, Cisco NX-OS, and Juniper Junos devices require the below minimal
configuration before installing AOS on-box device system agents. AOS off-box
device system agents are not supported on Cumulus Linux and SONiC devices.

    .. code-block:: prompt
        :caption: Arista EOS Off-box Agent Minimum Configuration

        !
        service routing protocols model multi-agent
        !
        aaa authorization exec default local
        !
        username admin privilege 15 role network-admin secret <admin-password>
        !
        vrf definition management
           rd 100:100
        !
        interface Management1
           vrf forwarding management
           ip address <address>/<cidr>
        !
        ip route vrf management 0.0.0.0/0 <management-default-gateway>
        !
        management api http-commands
           protocol http
           no shutdown
           !
           vrf management
              no shutdown
        !

    .. code-block:: prompt
        :caption: Cisco NX-OS Off-box Agent Minimum Configuration

        !
        feature nxapi
        feature bash-shell
        feature scp-server
        feature evmed
        copp profile strict
        nxapi http port 80
        !
        username admin password <admin-password> role network-admin
        !
        vrf context management
          ip route 0.0.0.0/0 <management-default-gateway>
        !
        nxapi http port 80
        !
        interface mgmt0
          ip address <address>/<cidr>
        !

    .. code-block:: prompt
        :caption: Juniper Junos Off-box Agent Minimum Configuration

        system {
            }
            services {
                ssh {
                    root-login allow;
                }
                netconf {
                    ssh;
                }
            }
        }
        routing-instances {
            mgmt_junos {
                routing-options {
                    static {
                        route 0.0.0.0/0 next-hop <management-default-gateway>;
                    }
                }
            }
        }

Uploading Packages
------------------
Before creating an agent, upload any :doc:`packages <packages>`
that are to be included in the agent.

.. sidebar:: Upgrading Packages

   Upload a package with an incremented version, and include it in the system
   agent or agent profile.


Retain Pre-existing Configuration
---------------------------------
On Cisco NX-OS platforms, you can ensure that pre-existing configuration is
retained when a device is brought under AOS management. See
:doc:`Device AAA support <device_aaa>` for details.

Creating Agent
==============

On-box
------

If you're creating system agents for multiple devices that share configuration
parameters, it is useful to create a :doc:`system agent profile <agent_profiles>`.
This allows you to configure the parameters only once.

#. From the AOS web interface, navigate to **Devices / System Agents / Agents**,
   then click **Create Onbox Agent(s)**.

   **Creating Agent in AOS Version 3.2.0**

   .. image:: static/images/device_management/320_system_agent_profile_create.png

   **Creating Agent in AOS Version 3.1.1**

   .. image:: static/images/device_management/311_system_agent_profile_create.png

#. Specify `agent details`_.
#. Click **Create**. While the task is active you can view its progress at the
   bottom of the screen in the **Active Jobs** section. The job status changes
   from **Initialized** to **In Progress** to **Succeeded**.

Off-box
-------

If you're creating system agents for multiple devices that share configuration
parameters, it is useful to create a :doc:`system agent profile <agent_profiles>`.
This allows you to configure the parameters only once.

#. From the AOS web interface, navigate to **Devices / System Agents / Agents**,
   click the **OFFBOX** tab, then click **Create Offbox Agent(s)**.
#. Specify `agent details`_.

   * For **Platform**, as of AOS version 3.3.0, entries are case-sensitive.
     Enter one of the following: ``nxos``, ``eos``, ``junos``.
   * Provide login credentials, OR select a previously created agent profile.

     * If an agent profile is selected, make sure it has the required packages
       associated to it.
     * If an agent profile is not selected, make sure required packages are
       uploaded, and check the ones needed.

   .. image:: static/images/device_management/320_system_agent_profile_create_offbox_nxos.png

#. Click **Create**. While the task is active you can view its progress at the
   bottom of the screen in the **Active Jobs** section. The job status changes
   from **Initialized** to **In Progress** to **Succeeded**.

Agents on L2 Servers
--------------------
You can install AOS device agents on L2 servers running CentOS or Ubuntu (as of
AOS version 2.3). Installing an agent on an L2 server follows the same steps as
described above with a few specific requirements.

* **Operation Mode** for L2 servers must be **TELEMETRY ONLY** since they do not
  establish L3 peering with leafs they only provide telemetry data.
* Certain services (such as LLDP) require that additional packages be installed.
  Check the **Install Requirements** checkbox to install them.

After the agent is running on the device you can add the device to a blueprint and
deploy it in the same way as any other device. See
:ref:`Staging Device Profiles <staging_device_profiles>` for details.

.. note::

    Only servers using **eth0** as the management interface are able to
    connect to the AOS server. If you require a different naming schema
    you must install the agent manually. Refer to the
    :doc:`Server Agent <server_agent>` documentation for more information.

    Also, please keep in mind that telemetry information collected from servers
    is limited compared to the data provided from network devices.

Uninstalling Agent
==================
#. From the AOS web interface, navigate to **Devices / System Agents / Agents**.
#. Click the **Uninstall** button for the agent to uninstall.
#. Click **Confirm** to start the uninstall process and return to the list view.

Editing Agent
=============
#. From the AOS web interface, navigate to **Devices / System Agents / Agents**.
#. Click the **Edit** button for the agent to edit.
#. Make your changes.
#. Click **Update** to update the agent and return to the list view.

Deleting Agent
==============
#. From the AOS web interface, navigate to **Devices / System Agents / Agents**.
#. Click the **Delete** button for the agent to delete.
#. Click **Delete** to delete the agent and return to the list view.

Upgrading Device Operating System
=================================

.. toctree::
   :maxdepth: 1

   dos_upgrade

Vendor Specific Agent Information
=================================

.. toctree::
   :maxdepth: 1

   arista_device_agent
   cisco_device_agent
   cumulus_device_agent
   sonic_device_agent
   server_agent
