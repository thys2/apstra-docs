=====================
External Routes Probe
=====================
To troubleshoot external network connectivity problems, AOS provides a predefined
IBA probe that automatically activates the collection of received or advertised
routes across all BGP sessions established with external routers. This predefined
IBA probe uses IBA Ingestion Filters introduced in AOS version 3.1.

To start troubleshooting external routes follow these steps:

#. From an AOS Blueprint, go to **Analytics**, click on **Create Probe**, and
   click on **Instantiate Predefined Probe**.

#. Select "External routes"

   .. image:: static/images/iba/iba_1_310.png

#. The predefined probe has the following parameters that you can configure at
   creation time or update subsequently:

   * **AFI**: IPv4 or IPv6

   * **Type**: Either "advertised-routes" or "received-routes"

   * **Security Zone (VRF)**: All or specific name

   * **Prefix**: Only routes matching the prefix

   * **Filter options**: exact or longer

   * **More-specific prefixes mask**: Match more-specific prefixes from a parent
     prefix, up until le_mask prefix length.

   * **Less-specific prefixes mask**: Match less-specific prefixes from a parent
     prefix, up from ge_mask to the prefix length of the route.

When you click "Create", AOS collects external routes and
present routes as in one single stage output table (mixing received, used and
advertised routes).

.. image:: static/images/iba/iba_2_310.png
