===================
Interface Telemetry
===================
Interface telemetry compares intent with the up/down state of physical interfaces.
It does not include LLDP, LAG or any other attachment information.

Viewing Interface Telemetry
===========================

#. Navigate to **Devices / Telemetry** to see the device telemetry list view.

   .. image:: static/images/telemetry/interface1_list_view_322.png

#. Click **INTERFACE** to see the detailed device list.

   .. image:: static/images/telemetry/interface2_interface_322.png

#. Click an individual device name to see its telemetry page, then click
   **Interface** to see interface telemetry.

   .. image:: static/images/telemetry/interface3_telemetry_322.png

#. You can locate the details of any unintended interface states here and attend
   to them.
