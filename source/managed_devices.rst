===============
Managed Devices
===============

Devices with installed :doc:`AOS device agents <device_agents>` appear in the
managed devices list. From the AOS web interface, navigate to
**Devices / Managed Devices**.

   .. image:: static/images/devices/managed_devices_330.png

From **Managed Devices**, you can perform tasks, such as acknowledging devices (to
bring them under AOS management), setting admin states, updating user
configuration, changing associated device profiles or admin states, and deleting
devices. See the :doc:`device guides <device_guide>` for detailed information
about device management in AOS.

.. _ack_device:

Acknowledge Device(s)
=====================
Acknowledging devices puts them in the **Ready** state and signals the intent to
have AOS manage them.

#. From the AOS web interface, navigate to **Devices / Managed Devices** and
   select the device(s) to be managed by AOS.
#. Above where you just clicked, click the checkmark to **Acknowledge** the
   selected device(s).
#. Click **Confirm** to acknowledge the device(s) and return to the list view.
   The **Acknowledged?** field for the device changes to a green checkmark and
   the device state changes to **OOS-READY**.

.. _admin_state:

Setting Admin State on Devices
==============================
#. From the AOS web interface, navigate to **Devices / Managed Devices** and
   select the device(s) to update.
#. Click the button for the state to change the selection(s) to
   (MAINT, NORMAL, DECOMM).
#. Click **Confirm** to set the admin state and return to the list view.

.. important::

  If you are decommissioning a device, after setting the admin state to
  **DECOMM**, you must uninstall the device agent. See the device guide for
  :ref:`removing a device <device_decomm:removing device from aos management>`.
  If you would subsequently like AOS to manage the device you can
  :doc:`add the device <device_add>` back to AOS.

Updating User Config
====================
#. From the AOS web interface, navigate to **Devices / Managed Devices** and
   select the device(s) to update.
#. Click the **Update user config** button, then to override device profile, admin
   state, and/or location, check the appropriate boxes.
#. Click **Confirm** to update the user config and return to the list view.

.. _edit_system:

Editing System
==============
There may be cases where you'll need to change managed device details. For two
examples, see the :ref:`Modular Devices and Device Profiles <modular_device>` and
:ref:`Hyperloop Interface <hyperloop>` sections below.

To edit a managed device:

#. From the AOS web interface, navigate to **Devices / Managed Devices** and
   select the device key for the device to update.

   .. image:: static/images/devices/managed_device_key_330.png

#. Click the **Edit** button (top-right) and select a different device
   profile, admin state, and/or location, as applicable.

   .. image:: static/images/devices/managed_devices_edit_system_330.png

#. Click **Update** to update the device and return to the list view.

.. _modular_device:

Modular Devices and Device Profiles
-----------------------------------
Multiple :doc:`device profiles <device_profile>` may exist for one modular device
model to represent different line card configurations.

.. image:: static/images/devices/managed_devices_modular_330.png

AOS selects the first device profile, based on the selector model field, that it
encounters that matches the model of the device chassis (e.g. DCS-7504N).

.. image:: static/images/devices/managed_devices_modular_system_330.png

AOS currently does not match device profiles based on line card configuration.

When using a modular device in your network, check that the correct device profile
is associated with it. If necessary change it by following the steps above for
:ref:`editing a system <edit_system>`. This must be done prior to acknowledging the
managed device and assigning it to an AOS blueprint.

.. image:: static/images/devices/managed_devices_modular_edit_system_330.png

.. _hyperloop:

Hyperloop Interface
-------------------
If you want to set up a hyperloop interface to
:doc:`enable VXLAN Routing on a Cumulus device equipped with Tomahawk
<cumulus_hyperloop_interface>` so it supports Routing In and Out of
Tunnels (RIOT) you must change the device profile used in the managed device.

Editing Pristine Config
=======================

.. warning::
  The Pristine Config should *never* be modified directly unless there is no
  alternative. Changes are not validated. Contact
  :doc:`Apstra Global Support <support>` for assistance as needed.

Modifying Pristine Config is a local operation, and does not lead to a change
to the running device configuration. Changes are applied on the next full config
push. Persistent changes to a configuration are best applied with
:doc:`configlets <configlets>`.

#. From the AOS web interface, navigate to **Devices / Managed Devices** and
   select the **Device Key** of the device.
#. Click the **Pristine Config** tab (top-left), then click the
   **Edit pristine config** button (under running-config on the left).
#. Make your changes.
#. Click **Update** to apply the changes.

.. image:: static/images/device_management/pristine_update_1.png

Updating Pristine Config from Device
====================================

It is also possible to update (customize) the pristine config by copying the
running device configuration. To use this function, the device should be out of
service (OOS-READY or OOS-MAINT). Once you have your device in OOS state, please
make sure that all the necessary changes are done manually via CLI on the device
before updating pristine config from the device.

#. Unassign the device from the blueprint.
#. Make any necessary changes to the running device configuration via CLI.
#. From the AOS web interface, navigate to **Devices / Managed Devices** and
   select the **Device Key** of the device.
#. Click the **Pristine Config** tab (top-left), then click the
   **Update From Device** button (top-right).
#. Click **Update** to update Pristine Config from the device.

.. image:: static/images/device_management/pristine_update_4.png

.. note::
  After the "Update From Device" operation, please verify the Pristine Config again.
  As you have copied the running config of the device in OOS state which should be
  the device "Discovery-1" config, it may include additional configuration such as
  interface "speed" commands. You can edit Pristine Config again and delete those
  additional configuration manually. Contact
  :doc:`Apstra Global Support <support>` for assistance as needed.

Deleting System(s)
==================

.. important::
    Devices that have been acknowledged cannot simply be deleted - as there is
    still an active agent on the device talking to the AOS server, they would
    re-appear within seconds. For details on removing a device from AOS, see the
    :doc:`device guides <device_decomm>`.

After decommissioning a device and uninstalling its agent, remove it from the
managed devices list.

#. From the AOS web interface, navigate to **Devices / Managed Devices** and
   select the device(s) to remove.
#. Click the **Delete system(s)** button, then click **Confirm** to remove the
   device(s) and return to the list view.

Managed Device Telemetry
========================
#. From the AOS web interface, navigate to **Devices / Managed Devices** and
   select the **Device Key** of the device.
#. Click the **Telemetry** tab (top-left), then click a tab to see the relevant
   telemetry.

From the **Config** tab, you can **Apply Full Config** or **Accept Changes**, as
applicable. You can also
:ref:`apply full config from the blueprint that it's used in <apply_full_config>`.
See :ref:`Configuration Deviation <configuration_deviation>` for more information
about when to apply a full config.
