===============
Device Profiles
===============

.. toctree::
   :maxdepth: 2
   :caption: Vendor-specific Device Profile Information

   cumulus_device_profile
   sonic_device_profile
   juniper_device_profile

Device Profiles Overview
========================

Hardware Capabilities
---------------------
As of version 3.1.0, AOS supports configuring device-specific hardware
features. Device Profiles are where you express how you want to configure
the capabilities of the device. AOS supports three such capabilities that
affect configuration rendering. Some feature capabilities have different behaviors
across NOS versions and thus, capabilities can be expressed per NOS
version. By default, the version matches all supported versions.

.. image:: static/images/device_profiles/edit_view_caps_310.png
.. image:: static/images/device_profiles/editable_features_310.png
.. image:: static/images/device_profiles/general_caps_view_310.png

Control Plane Policing
----------------------
When CoPP is enabled for a specific version, AOS renders strict copp
profile config on the NX-OS devices resulting in the following configuration
rendering::

    terminal dont-ask
    copp profile strict

This terminal dont-ask config is needed only when enabling the copp
profile strict config, since we do not want NX-OS to wait for confirmation::

    switch(config)# copp profile strict
    This operation can cause disruption of control traffic. Proceed (y/n)? [no] ^C
    switch(config)#
    switch(config)# terminal dont-ask
    switch(config)# copp profile strict
    switch(config)#

Autonomous Systems Sequencing Support
-------------------------------------
Autonomous Systems Sequencing Support applies to the autonomous system (AS) path.
When enabled, AOS sequences into the entry list to resequence and render the
right AS config. For platforms, such as Cisco3172, that do not support sequence
numbers, disabling this feature ensures that AOS removes the AS sequence
numbers from the device model dictionary to avoid addition and negation in the
event AOS resequences something. In such scenarios, there is no requirement to
render anything on these platforms, because we cannot even sequence the entry.

If **Sequence Numbers Support** is enabled, the device supports sequence numbers
and AOS can use the sequence number to generate config as follows::

    ip as-path access-list MyASN seq 5 permit ^$
    ip as-path access-list Rtr seq 5 permit ^3
    ip as-path access-list Srvr seq 15 permit _103$

Note that the numbers 5 and 15 are sequence numbers applicable to devices
that support AS sequencing.

Interface Breakout
------------------
Fundamentally, this field is telling whether ports on a specific module in a
device can be broken down to lower speed split ports or not.

In AOS 3.1.0, the field is used only by NX-OS Device Profiles. However, any
vendor OS which wants to express breakout capability of a port can use this field.

Enabling  breakout for a specific module indicates that the ports on that module
in a device can be broken down to lower speed split ports.
If the value for a particular module (let's say, here, 1) is set to True, then
AOS renders the following config::

    no interface breakout module 1
    !

Because the negation command is always applicable per module, this particular
capability in the DP also has to be configured per module.
There are a few advantages in making the capability per module:

* In modular systems, not all linecards have breakout capable ports.
* In non-modular systems, the breakout capable ports may not always be in
  module 1.

Assumptions

AOS assumes we first un-breakout all ports that are breakout-capable, and then
apply the proper breakout commands according to intent.
This is based on another assumption that the global negation command
“no interface breakout module <module_number>" always can be successfully
applied to a module with breakout capable ports. However, we recognize that this
assumption may be broken in future versions of NX-OS, or with a certain
combination of cables / transceivers inserted into breakout-capable ports.

Historical Context

With a particular version of NX-OS the POAP stage would apply breakout config on
those ports which are breakout capable. POAP behavior, introduced in 7.0(3)I4(1)
POAP, determines which breakout map (for example, 10gx4, 50gx2, 25gx4, or 10gx2)
brings up the link connected to the DHCP server. If breakout is not
supported on any of the ports, POAP skips the dynamic breakout process. After
the breakout loop completes, POAP proceeds with the DHCP discovery phase as
normal. AOS reverts any such breakout config that might have been done during
the POAP stage to ensure that the ports are put back to default speed by applying
the negation command.

To do this, AOS runs the following global negation command, which is idempotent
when applied on ports that are not broken out::

    no interface breakout module <module_number>

.. sidebar:: Interface Maps

   When you create :doc:`Interface Maps <interface_maps>`,
   you'll be assigning Device Profiles (and Logical Devices) to them.
   Make sure Device Profiles exist for all physical devices you'll be using.

Device Profiles (DP) specify the details of hardware devices that AOS supports.
As additional hardware models are qualified, they are added to the
:doc:`list of supported devices <device_support>`.

Listing Device Profiles
=======================
From the AOS Web interface, navigate to **Devices / Device Profiles**
to see the list of Device Profiles. AOS ships with many pre-defined profiles.
To sort the list, click the header of the category you'd like to sort.
Click the header again to reverse the sort order.

.. image:: static/images/device_profiles/device_profile_list_310.png

Viewing a Device Profile
========================

.. sidebar:: Device Profile Details

   AOS does not necessarily use all this information for modeling. It's made
   available to other AOS API orchestration tools for collection and use.

From the list view (**Devices / Device Profiles**) click a Device Profile name
to view its details. Device Profile parameters include:

.. _device_profile_parameters:

Summary Section
   Label
      Name of the Device Profile

   Slot count
      Number of slots or modules on the device.
      Modular switches have multiple slots.

   Start from ID

----

Selector Section
   Device-specific information to match the hardware device to the
   Device Profile

   Manufacturer
      Name of the manufacturer

   Model
      Determines whether a Device Profile can be applied to specific hardware.
      Select from the drop-down list or use a Regular Expression (regex) to
      define one that's not on the list.

   OS family
      Defines how configuration is generated, how telemetry commands
      are rendered, and how configuration is deployed on a device.
      Select a currently supported OS family (CentOS, Cumulus, EOS,
      NX-OS, SONiC, Ubuntu GNU/Linux) from the drop-down list or add
      an OS family for any other Operating System.

   Version
      Determines whether a Device Profile can be applied to specific hardware.
      Select from the drop-down list or define your own regex.

----

Capabilities Section
   The hardware and software capabilities defined in this section can be
   leveraged in other parts of AOS to adapt the generated configuration,
   or to prevent an incompatible situation.

   Hardware Capabilities
      With the exception of ECMP, Hardware Capabilities modify configuration
      rendering or deployment.

      CPU (cpu:string)
         Architecture of the CPU. Example: ``x86``

      Userland (bits) (userland:integer)
         Type of userland (application binary/kernel) that is supported.
         Example: ``64``

      RAM (GB) (ram:integer)
         Amount of memory that the device has. Example: ``16``

      ECMP limit (ecmp_limit:integer)
         Maximum number of Equal Cost Multi Path routes.
         This field changes BGP configuration on the device (ecmp max-paths).
         Example: ``64``

      Form factor (form_factor:string)
         Amount of rack space the device uses. Examples: ``1RU``, ``2RU``,
         ``6RU``, ``7RU``, ``11RU``, ``13RU``

      ASIC (asic:string)
         Type of Chipset (ASIC) on the switch. Examples: `` ``, ``T2``,
         ``T2(3)``, ``T2(6)``, ``Arad(3)``, ``Alta``, ``TH``, ``Spectrum``,
         ``XPliant XP80``, ``ASE2``, ``Jericho``

      Supported Features (as of 3.1.0, Cisco only)
         COPP
            When Control Plane Policing is enabled, strict CoPP profile config
            is rendered for the specified NX-OS version. Multiple versions
            can be specified.

            Enabled for all versions by default (except Cisco 3172PQ NXOS,
            which is disabled by default).

         Breakout
            Enable breakout to indicate that ports on the specified module can
            be broken out to lower speed split ports. Each module is
            specified individually.

            Disabled for the following devices with modules incapable
            of breaking out ports: Cisco 3172PQ NXOS, Cisco 9372TX NXOS,
            Cisco C9372PX NXOS, Cisco C9396PX NXOS, Cisco NXOSv

            You can express breakout capability of a port for any vendor OS.

         Sequence Numbers Support (for Autonomous System (AS) path)
            Enable for the ability to sequence into the entry list to resequence
            and render the right AS config.

            Enabled for all Cisco Device Profiles by default (except Cisco
            3172PQ NXOS, which does not support sequence numbers).

         Other supported features known to AOS include ``vxlan``, ``bfd``,
         ``vrf_limit``, ``vtep_limit``, ``floodlist_limit``, ``max_l2_mtu``,
         and ``max_l3-mtu``. They can be included in the backend using the
         following format:

         key : value :: feature : feature_properties    Example: 32 vtep_limit: 32

   Software Capabilities
      LXC (lxc_support: boolean)
         Select if the device supports LXC containers.
      ONIE (onie: boolean)
         Select if the device supports ONIE.

----

Ports Section
   Defines the types of available ports, their capabilities and how they
   are organized.

   Every port contains a collection of supported speed transformations. Each
   transformation represents the breakout capability (such as 1-40GBe port
   breaking out to 4-10GBe ports), and hence contains a collection of
   interfaces.

   Example: If port 1 is a QSFP28 100->4x10, 100->1x40 breakout capable port,
   then port 1 has a collection of three transformations, one each for 4x10,
   1x40 and 1x100 breakouts. The transformation element in the collection which
   represents the 4x10 has a collection of 4 interfaces, 1x40 and 1x100 has a
   collection of 1 interface.

   Port Index (port_id: integer)
      Indicates a unique port in the collection of ports in the Device Profile.

   Row Index (row_id: integer)
      Represents the top-to-bottom dimensions of the port panel. Shows where
      the port is placed in the device's panel. For instance, in a panel with
      two rows and many columns the row index is either ``1`` or ``2``.

   Column Index (column_id: integer)
      Represents the left-to-right dimensions of the port panel. Shows where
      the port is placed in the device's panel. For instance, in a panel with
      thirty-two ports and two rows, the column index is in the range of
      ``1`` through ``16``.

   Panel Index (panel_id: integer)
      Indicates the panel that the port belongs to given the physical layout
      of ports in the device specification

   Slot ID (slot_id: integer)
      Represents the module that the port belongs to. A modular switch has
      more than one slot. In fixed function network function devices,
      Slot ID is usually ``0``.

   Failure Domain (failure_domain_id: integer)
      Indicates if multiple panels are relying on the same hardware
      components. Used when creating the cabling plan to ensure that two uplinks
      are not attached to the same failure domain.

   Connector Type (connector_type: string)
      Port transceiver type. Speed capabilities of the port are directly
      related to the connector type, given that certain connector types can
      run in certain speeds. For instance, ``sfp``, ``sfp28``, ``qsfp``,
      ``qsfp28``.

   Transformations (transformations: list)
      Possible breakouts for the port. Every entry is a specific supported
      speed. Each transformation has a collection of interfaces.

      Number of interfaces (interfaces:list)
         Dependent on the breakout capability of the port. For a
         transformation representing a certain breakout speed, the interfaces
         contain information about the interface names and interface settings
         with which the device intends to be configured.

         The ``setting`` information is crucial for configuring the interfaces
         correctly on the device. Thus, AOS 2.3.1 uses a vendor specific
         schema to govern the user input of the interface setting and catch
         any potential validation errors at time of create and edit of
         device profiles.

         Based on the OS information you enter in the device profile's
         selector field, the Web interface displays the applicable fields that
         you need to enter for the settings. The fields vary with the
         vendor OS (as found in examples below). The ``setting`` is validated
         based on the vendor specific schema (as listed below) when a Device
         Profile is created or edited.

         The actual and expected vendor specific schema for the settings
         is as follows::


             eos_port_setting = Dict({
                 'interface': Dict({
                     'speed': Enum([
                         '', '1000full', '10000full', '25gfull', '40gfull',
                         '50gfull', '100gfull',
                     ])}),
                 'global': Dict({
                     'port_group': Integer(),
                     'select': String()
                     })
                 })

             nxos_port_setting = Dict({
                 'interface': Dict({
                     'speed': Enum([
                         '', '1000', '10000', '25000', '40000', '50000',
                         '100000',
                     ])}),
                 'global': Dict({
                     "port_index": Integer(),
                     "speed": String(),
                     "module_index": Integer()
                     })
                 })

             junos_port_setting = Dict({
                 'interface': Dict({
                     'speed': Enum([
                         '', 'disabled', '1g', '10g', '25g', '40g', '50g', '100g'
                     ])}),
                 'global': Dict({
                     'speed': Enum([
                         '', '1g', '10g', '25g', '40g', '50g', '100g'
                     ]),
                     "port_index": Optional(Integer()),
                     "fpc": Optional(Integer()),
                     "pic": Optional(Integer())
                     })
                 })

             opx_port_setting = Dict({
                 'interface': Dict({
                     "command": String(),
                     "speed": String(),
                     "mode": String(),
                     "default_speed": String()
                     })
                 })

             sonic_port_setting = Dict({
                 'interface': Dict({
                     "command": Optional(String()),
                     "speed": String(),
                     "lane_map": Optional(String())
                     })
                 })

             cumulus_port_setting = Dict({
                 'interface': Dict({
                     'speed': String(),
                     'command': String()
                 })
             })

Vendor Specific Schema for Port Settings
----------------------------------------
As of version 2.3.1 AOS supports vendor specific schema for port settings in
Device Profiles and Interface Maps. Prior to AOS 2.3.1, the setting field
in the Device Profile and Interface Map was a string whose contents were
not bound by any schema. The dictionary inside the string
would be built differently for different vendors. The lack of schema for these
vendor-based settings pose multiple problems on how you would configure
features like auto-negotiation on various vendor switches.

AOS now allows you to create a well-defined schema for the port settings that
provide you with the flexibility to choose the keys and values for the port
settings in expectation of effective configuration rendering. You have better
control over selecting certain keys and values in the design phase to expect a
corresponding config in the deploy phase.

For example, to turn auto negotiation on, the vendor-based schema serves as a
bound on what you can select in-order to enable auto neg all the way on the device
interfaces.

Introduction of vendor-based schema for port settings does not only provide you
a flexible way to configure the ports with features, but also allows AOS backend
to elegantly consume the information in the DP and IM to calculate the rendering
of these interfaces thus making the Jinja templates simpler.

Creating a Device Profile
=========================

.. important::

   Device Profiles contain extensive
   :ref:`hardware model details <device_profile_parameters>`. When creating a
   Device Profile, ensure that the profile accurately describes all hardware
   characteristics. When in doubt, contact :doc:`Apstra Support <support>`
   for assistance.

#. From the list view (**Devices / Device Profiles**),
   click **Create Device Profile** to see the dialog for creating
   a Device Profile.
#. If you've created JSON payload you can import it by clicking on **Import
   Device Profile** and selecting the file. Otherwise, continue to the next step.
#. Enter a Device Profile name.
#. Configure the Device Profile to match the characteristics of the
   physical device. See above for
   :ref:`parameter descriptions <device_profile_parameters>`
#. Click **Create** to create the Device Profile.

.. _device_profile_clone:

Cloning a Device Profile
========================
Instead of creating a Device Profile from scratch that is similar to one that
already exists, you can clone that existing one and change the few details
that are different.

#. Either from the list view (**Devices / Device Profiles**), or the details view,
   click the **Clone** button corresponding to the Device Profile
   to clone. You'll see the dialog for cloning a Device Profile.
#. Enter a unique name for the new Device Profile, and make your changes.
#. Click **Create** (bottom-right) to create the Device Profile.

.. warning::
  AOS built-in Device Profiles are managed and updated by Apstra, and the changes
  are automatically propagated to the imported Interface Maps in the Blueprints on
  AOS upgrade. However, manually created or cloned Device Profiles are not managed
  or updated by Apstra.

Editing a Device Profile
========================

.. important::
  Changes in built-in Device Profiles made by users are not reflected on AOS upgrade.
  Hence, cloning the built-in Device Profiles instead of editing is recommended.
.. warning::
  Manually changing a Device Profile can lead to a mismatch between the profile's
  stated capabilities and the device's actual capabilities, potentially leading to
  unexpected results.

If a Device Profile is referenced by an
:doc:`Interface Map <interface_maps>`, you may not be able to
change it if it would adversely affect that Interface Map.

#. Either from the list view (**Devices / Device Profiles**) or the details view,
   click the **Edit** button corresponding to the Device Profile to edit. You'll
   see the dialog for editing a Device Profile.
#. Make your changes.
#. Click **Update** (bottom-right) to update the Device Profile.

Use Case: Enabling Auto-negotiation
-----------------------------------
This example shows how to set a port on an **Arista DCS-7050TX-72Q** device
to auto-negotiate its speed.

#. From the AOS Web interface, navigate to **Devices / Device Profiles**
   to see the list of existing Device Profiles.
#. Click the **Edit** button corresponding to **Arista DCS-7050TX-72Q** to
   see the dialog for editing that Device Profile.
#. Click **Ports** (left) to see a graphical representation of the ports.

   .. figure:: static/images/device_profiles/231_vendor-specific-schema1.png
      :align: center

      Device Profile - Port View

#. Select one or more ports in the panel to see port details.
#. In the **Transformations** section, click the **Edit** button
   corresponding to the port to be edited.

   .. figure:: static/images/device_profiles/231_vendor-specific-schema2.png
      :align: center

      Device Profile - Port Details View

#. Populate the port setting field with the string below. An empty string in the
   *speed* field signifies that the port is set to auto-negotiate. AOS models the
   maximum speed for the port to the speed in the fabric.

   .. code-block:: json
       :caption: Port setting

       {"interface": {"speed": ""}, "global": {"select": "", "port_group": -1}}

   You can edit the fields, such as *command*, inside the port settings
   instead. AOS validates the command contents before the update is applied.

   **Arista Devices**

   Enabling auto-negotiation on an Arista device results in an automatically
   updated port setting in AOS, because the schema for enabling auto-negotiation
   on an Arista device is well-defined, as shown below.

   .. code-block:: text
       :caption: Port setting

       { "interface":
           { "speed": "<String> example ”},
           "global": "<Dict> example" {
               "select": <String> example “”,
               "port_group": <Integer> example -1}
           “command”: <to turn on auto neg on the port>
       }

7. Click **Update** to save your changes.

Deleting a Device Profile
=========================
If a Device Profile is referenced by an
:doc:`Interface Map <interface_maps>`, you cannot delete it.

#. Either from the list view (**Devices / Device Profiles**) or the details view,
   click the **Delete** button corresponding to the Device Profile to delete.
   You'll see the dialog for deleting a Device Profile.
#. Click **Delete Device Profile** to delete the Device Profile.

Device Profiles and REST API
============================
You can also work with Device Profiles via REST API. From the AOS Web interface,
navigate to **Platform / Developers** to access REST API documentation.
