=========
Favorites
=========

Adding favorites enables you to quickly access frequented pages.

.. image:: static/images/favorites/favorites_330.png

**To add a favorite** - click the star in the upper-left corner of the page.
Leave the default name, or rename it, then click **Add**. The outlined star
becomes a shaded star.

**To remove a favorite** - click the shaded star on the saved page.
The star becomes an outline.

**To access favorites** - from the AOS web interface, click **Favorites**
in the main menu.

**To go directly to a favorite page** - click the name in the **Favorites**
menu. Up to five saved pages appear in the side menu.

**To see all favorites** - click **Show more** to go to the user profile where
you can link to all favorite pages and change their names.
