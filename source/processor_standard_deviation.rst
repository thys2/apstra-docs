=============================
Processor: Standard Deviation
=============================
The Standard Deviation processor groups as described by **Group by**, calculates
the standard deviation, then outputs one standard deviation per group.

**Input Types** - Number-Set (NS), NSTS

**Output Types** - Number-Set (NS)

**Properties**

   .. include:: includes/processors/group_by.rst

   DDoF (ddof)
     Delta Degrees of Freedom, standard deviation correction value, is used to
     correct divisor (N - DDoF) in calculations, e.g. DDoF=0 - uncorrected sample
     standard deviation, DDoF=1 - corrected sample standard deviation.

   .. include:: includes/processors/enable_streaming.rst

Standard Deviation Example
--------------------------

.. code-block:: none

      group_by: ["role", "system_id"]
      ddof: 1

Also assume an NS input of

.. code-block:: none

      [role:fabric, system_id:spine1, if_name=eth0] :10
      [role:fabric, system_id:spine1, if_name=eth1] :11
      [role:server, system_id:spine1, if_name=eth3] :12
      [role:server, system_id:spine1, if_name=eth4] :13
      [role:fabric, system_id:spine2, if_name=eth0] :14
      [role:fabric, system_id:spine2, if_name=eth1] :15
      [role:server, system_id:spine2, if_name=eth3] :16
      [role:server, system_id:spine2, if_name=eth4] :17

Given the above, the output would be a number-set of

.. code-block:: none

      [role:fabric, system_id:spine1] : stddev([10, 11])
      [role:fabric, system_id:spine2] : stddev([14, 15])
      [role:server, system_id:spine1] : stddev([12, 13])
      [role:server, system_id:spine2] : stddev([16, 17])
