=============
Property Sets
=============

Property Set Overview
=====================
Property sets are collections of key-value pairs that are imported into blueprint
catalogs for use in configlets and IBA probes.

For example, instead of hard-coding values for NTP servers, SMTP servers, and
syslog servers, you can define them in a property set, associate the property
set with a configlet, and import both of them into a blueprint catalog.

For :doc:`Intent-Based Analytics <iba>`, you can parameterize macro level SLAs
or individual business units by applying property sets to IBA probe definitions.

.. image:: static/images/property_set/property_sets_330.png

**To access property sets** - from the AOS web interface, navigate to
**Design / Property Sets**.

**To see details** - click a property set name. Property sets include the following
details:

Name
   Identifies the property set

Properties
   key-value pair(s)

.. sidebar:: Cloning Property Set

   Instead of entering all details for a new property set, you can clone an
   existing one, give it a unique name and customize it.

Creating Property Set
=====================
#. From the list view (Design / Property Sets), click **Create Property Set**,
   then enter a name.
#. Enter a key in the left property field and a value in the right property field.
   Do not add curly braces {{ }} to the key; they are added for you.

   * To add an additional property, click **Add a Property**.

#. Click **Create** to create the property set and return to the list view.

Editing Property Set
====================
To prevent potentially unintended changes to existing blueprints, changes to
property sets in the global catalog do not affect property sets in the blueprint
catalog. If the intent is for a blueprint to use a modified property set, then the
property set must be re-imported into the blueprint.

#. From the list view (Design / Property Sets), click the name
   of the property set to edit.
#. Click the **Edit** button (top-right) .
#. Make your changes.
#. Click **Update** (bottom-right) to update the Property Set.

Deleting Property Set
=====================
If a property set is assigned to a :doc:`configlet <configlets>`, it
cannot be deleted.

#. Either from the list view (Design / Property Sets) or the details view,
   click the **Delete** button for the property set to delete.
#. Click **Delete** to delete the property set from the global catalog and
   return to the list view.
