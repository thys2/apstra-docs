Creating a LAG on a Server
===========================
Creating a LAG on a server requires a little bit of network configuration and
troubleshooting ability.  This document helps to describe both.

.. image:: static/images/server_lag/server_lag.png

Prerequisites
-------------

Ubuntu 14.04 LTS
Installation of packages 'ifenslave' and 'vlan' on top of stock 14.04 LTS
server install --  ``apt -y install ifenslave vlan``

* `ifenslave` gives ubuntu network stack ability to use 'bond' interfaces and loads
  bond driver when it is invoked
* `vlan` gives ubuntu the ability to specify vlans (.VLANID suffix on interfaces)
  and loads vlan driver when invoked

A server connected to two LACP-capable switches running intel 'e1000' network driver
nics (virtio is not supported for lacp)

Server Configuration
--------------------
There may be some bug/race condition that prevents bond interfaces from coming up
properly in Ubuntu 14.04.  Manual up/down scripts may be required for manual
ifenslaving.

https://bugs.launchpad.net/ubuntu/+source/ifenslave-2.6/+bug/14153023

Below, this configuration contains workarounds.

.. code-block:: text
    :caption: ``/etc/network/interfaces``

    auto eth0
    iface eth0 inet dhcp


    allow-hotplug eth1
    iface eth1 inet manual
     up ifenslave bond0 $IFACE
     down ifenslave -d bond0 $IFACE
     bond-master bond0


    allow-hotplug eth2
    iface eth2 inet manual
     up ifenslave bond0 $IFACE
     down ifenslave -d bond0 $IFACE
     bond-master bond0


    auto bond0
    iface bond0 inet static
     address 192.168.100.4
     netmask 255.255.255.0
     bond-mode 802.3ad
     bond-slaves eth1 eth2
     bond-lacp-rate fast
     bond-xmit_hash_policy layer3+4
     bond-miimon 100
     bond-downdelay 200
     bond-updelay 200


    auto bond0.101
    iface bond0.101 inet static
     address 192.168.101.4
     netmask 255.255.255.0


    auto bond0.102
    iface bond0.102 inet static
     address 192.168.102.4
     netmask 255.255.255.0

Validation
----------
* Ensure that 'partner mac' is not 00:00:00:00:00:00
  This implies there is no LACP peer
* Ensure that all slave interfaces are 'up'
* Check Transmit hash policy is layer3+4 for performance
* Check LACP rate
  Fast on one side and slow on the other side may be an issue


.. code-block:: prompt

    root@ubuntu1:~# cat /proc/net/bonding/bond0
    Ethernet Channel Bonding Driver: v3.7.1 (April 27, 2011)

    Bonding Mode: IEEE 802.3ad Dynamic link aggregation
    Transmit Hash Policy: layer3+4 (1)
    MII Status: up
    MII Polling Interval (ms): 100
    Up Delay (ms): 0
    Down Delay (ms): 0

    802.3ad info
    LACP rate: fast
    Min links: 0
    Aggregator selection policy (ad_select): stable
    Active Aggregator Info:
            Aggregator ID: 1
            Number of ports: 2
            Actor Key: 33
            Partner Key: 1
            Partner Mac Address: 02:0c:29:34:0d:f2

    Slave Interface: eth1
    MII Status: up
    Speed: 10000 Mbps
    Duplex: full
    Link Failure Count: 0
    Permanent HW addr: 00:0c:29:72:2b:44
    Aggregator ID: 1
    Slave queue ID: 0

    Slave Interface: eth2
    MII Status: up
    Speed: 10000 Mbps
    Duplex: full
    Link Failure Count: 0
    Permanent HW addr: 00:0c:29:72:2b:4e
    Aggregator ID: 1
    Slave queue ID: 0


There are a few quick tests we can run to ensure a bond is functioning normally.

Procfs diagnostics
------------------

.. code-block:: prompt
    :caption: Operational state of a bond

    root@ubuntu1:# cat /sys/devices/virtual/net/bond0/operstate
    up

.. code-block:: prompt
    :caption: Checking members of bonds

    root@ubuntu1:# cat /sys/devices/virtual/net/bond0/bonding/slaves
    eth1 eth2


.. code-block:: prompt
    :caption: Ensuring bond is 802.3ad lacp (not ad-select or other lb)

    root@ubuntu1:# cat /sys/devices/virtual/net/bond0/bonding/mode
    802.3ad 4


.. code-block:: prompt
    :caption: Check LACP rate

    root@ubuntu1:# cat /sys/devices/virtual/net/bond0/bonding/lacp_rate
    fast 1

.. code-block:: prompt
    :caption: Checking bond speed (aggregate)

    root@ubuntu1:# cat /sys/devices/virtual/net/bond0/speed
    20000
