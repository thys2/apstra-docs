==================
Creating Blueprint
==================
Before creating a blueprint, confirm that a template exists that meets the
structure and policy needs of your network. If needed, you can
:doc:`create a custom template <templates>`.

#. From the :doc:`AOS web interface <aos_ui>`, click **Blueprints**, then click
   **Create Blueprint**.

   .. image:: static/images/blueprint-tasks/blueprint_create_330.png

#. Enter a name and select a template from the **Template** drop-down list. A
   preview shows template parameters, topology preview, structure, external
   connectivity, and policies.
#. Click **Create** to create the blueprint and return to the blueprint summary
   view.

The next step after creating a blueprint is to :doc:`build <build_physical>` the
network in the **Staged** view.

.. image:: static/images/blueprints/staged/blueprint_dashboard2stage_330.png
