=======================
Configlet API Reference
=======================
For full API documentation, view the Platform API reference under the AOS Web UI
This is a targeted section to demonstrate configlet API similarly to the UI.
The main difference between the Web UI and REST API is that the AOS API does not
make any use of the configlets stored under api/design/configlets when working with
a blueprint.  Design-configlets are meant for consumption under the UI.  When
working with configlets on the API, work directly with the blueprint.

Configlets live in http://aos-server/api/design/configlets and are
referenced by ID.


.. code-block:: json
    :caption: Configlet schema

    {
      "ref_archs": [
        "two_stage_l3clos"
      ],
      "created_at": "string",
      "last_modified_at": "string",
      "id": "string",
      "generators": [
        {
          "config_style": "string",
          "template_text": "string",
          "negation_template_text": "string"
        }
      ],
      "display_name": "string",
      "section": "string"
    }


API - Creating a configlet
__________________________
To create a configlet, POST to https://aos-server/api/design/configlets with a
valid JSON structure representing the configlet.
Creating a configlet this way only allows it to be available for assignation in
the AOS Web UI - it is not required in this method for the REST API to assign to a
blueprint.  See the assigning a configlet section for more details.

A POST will create a new configlet.
A PUT will overwrite an existing configlet. PUT requires the URL of the configlet.
https://aos-server/api/design/configlets/{id}

.. code-block:: text

    curl -H "AuthToken: EXAMPLE" -d '{"display_name":"DNS","ref_archs":["two_stage_l3clos"],"section":"system","generators":[{"config_style":"eos","template_text":"ip name-server 192.168.1.1","negation_template_text":"no ip name-server 192.168.1.1"}]}' -X POST "http://aos-server/api/design/configlets"

The response will contain the ID of the newly created configlet
``{"id": "995446c7-de7d-46bb-a88a-786839556064"}``

API - Deleting a configlet
__________________________
Deleting a configlet requires an HTTP DELETE to the configlet by URL
http://aos-server/api/design/configlets/{id}


.. code-block:: text
    :caption: CURL Example - HTTP DELETE

    curl -H "AuthToken: EXAMPLE" -X DELETE "http://aos-server/api/design/configlets/995446c7-de7d-46bb-a88a-786839556064"

A successful DELETE has an empty response ``{}``


API - Assigning a configlet
___________________________
Assigning a configlet to a blueprint requires assignation of device conditions as
well as embedding the configlet details.
When assigning a configlet to a blueprint, the configlets available as design
resources aren’t necessary.  These are only used for UI purposes.

The assigned configlet lives in https://aos-server/api/blueprints/blueprint_id/configlets

JSON Syntax for putting a configlet to a blueprint.  Basically, this is just an
‘items’ dictionary element containing a list of configlet schemas.

.. code-block:: json

    {
      "items": [
        {
          "template_params": [
            "string"
          ],
          "configlet": {
            "generators": [
              {
                "config_style": "string",
                "template_text": "string",
                "negation_template_text": "string"
              }
            ],
            "section": "string",
            "display_name": "string"
          },
          "condition": "string"
        }
      ]
    }



CURL Example - HTTP PUT
_______________________

.. code-block:: text

    curl "http://aos-server/api/blueprints/e4068e99-813c-4290-b7cc-e145d85a98a8/configlets" -X PUT -H "AuthToken: EXAMPLE" -H "Content-Type: application/json; charset=utf-8" --data "[{""configlet"":{""generators"":[{""config_style"":""eos"",""template_text"":""ip name-server 192.168.1.1"",""negation_template_text"":""no ip name-server 192.168.1.1""}],""section"":""system"",""display_name"":""DNS""},""condition"":""role==spine""},{""configlet"":{""generators"":[{""config_style"":""eos"",""template_text"":""ip name-server 192.168.1.1"",""negation_template_text"":""no ip name-server 192.168.1.1""}],""section"":""system"",""display_name"":""DNS""},""condition"":""role==leaf""}]"

Response

.. code-block:: json

    {"items": [{"configlet": {"generators": [{"config_style": "eos", "template_text": "ip name-server 192.168.1.1", "negation_template_text": "no ip name-server 192.168.1.1"}], "section": "system", "display_name": "DNS"}, "condition": "role==spine"}, {"configlet": {"generators": [{"config_style": "eos", "template_text": "ip name-server 192.168.1.1", "negation_template_text": "no ip name-server 192.168.1.1"}], "section": "system", "display_name": "DNS"}, "condition": "role==leaf"}]}

API - Unassigning a configlet
_____________________________
To unassign a configlet, remove it from the items list by PUT with an empty json
post.


.. code-block:: text
    :caption: CURL PUT example

    curl "http://aos-server/api/blueprints/e4068e99-813c-4290-b7cc-e145d85a98a8/configlets" -X PUT -H "AuthToken: EXAMPLE" -H "Content-Type: application/json; charset=utf-8" --data ""

The response will be an empty json set once the configlet is deleted: `{"items": []}`
