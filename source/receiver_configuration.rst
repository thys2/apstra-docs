======================
Receiver Configuration
======================

The AOS Telegraf input plugin can be used to receive streaming telemetery from
AOS. `Telegraf <https://github.com/influxdata/telegraf>`_ is an agent for collecting,
processing, aggregating, and writing metrics. This is the component of
AOSOM-Streaming that handles the reception of the protobuf messages from AOS. The
Telegraf platform consists of input and output plugins that customers can choose from
for aggregating and storing metrics to different backend databases. The AOS input
plugin for Telegraf will deserialize the protobuf stream and create metrics that can
then be sent to a particular backend database, e.g., Prometheus, InfluxDB, or
Elasticsearch.

Configuring Using Telegraf Plugin
=================================

The configuration described here assumes we are using the AOS Telegraf input plugin.
Configuring streaming receivers in AOS can be done through the AOS web interface
(see below), or handled by
the Telegraf plugin by providing it the AOS credentials. A separate AOS account with
only the streaming credentials is recommended. If the configuration is done through
the GUI, then there is no need to supply credentials in the Telegraf config file.

The easiest way to run the Telegraf receiver is in a docker container. The
docker-compose.yml snippet below shows the configuration for the Telegraf
container. This will pull the latest Apstra supported Telegraf container from Docker
Hub.

.. code-block:: text

    # Telegraf container config
      telegraf-prom:
        image: apstra/telegraf:latest
        command: telegraf
        volumes:
          - ./config/telegraf-prom.toml:/etc/telegraf/telegraf.conf
        ports:
          - '9999:9999'


The Telegraf configuration file - ./config/telegraf-prom.toml - is mapped to
/etc/telegraf/telegraf.conf on the container. The input and output plugin
configurations are shown in the snippet below. The output plugin is configured for
the Prometheus client and will listen on port 9126. The input plugin is configured
for AOS.

.. code-block:: text

    # Configuration for Prometheus server to expose metrics
    [[outputs.prometheus_client]]
      listen = ":9126"
      expiration_interval = "0"

    [[inputs.aos]]
      address = "10.1.1.200"
      port = 9999
      streaming_type = [ "perfmon", "alerts", "events" ]
      aos_server = "$AOS_SERVER"
      aos_port = $AOS_PORT
      aos_login = "$AOS_LOGIN"
      aos_password = "$AOS_PASSWORD"

AOS Plugin Configuration
------------------------
| *address* - specifies the IP address of the streaming receiver
| *port* - specifies the port that the streaming receiver will be listening on
| *streaming_type* - specifies the type of data to be streamed from AOS to
  this receiver
| The rest of these parameters are only necessary if we want the AOS Telegraf
  plugin to configure the streaming receivers in AOS via the API.
| *aos_server* - specifies the IP address of the AOS server
| *aos_port* - should always be 443
| *aos_login* - AOS username
| *aos_password* - AOS password

Configuring Using AOS Web Interface
===================================

The AOS server can be configured to stream alerts, events and perfmon, or any
combination thereof. Each data type is sent to a streaming receiver over its own
TCP socket. Even if all three data types are configured for the same streaming
receiver, there will be three connections created between the AOS server and the
streaming receiver. This also allows for all three types to be sent to three
different streaming receivers.

**To access receivers** - from the AOS web interface, navigate to
**Platform / Streaming / Receivers** to see the list view.
(In previous AOS versions receivers were called
streaming endpoints.)

**To sort** - click one of the sortable headers. To reverse the sort order,
click the header again.

Receivers include the following details:

* Hostname - Hostname

* Port - default: 4444

* Message Type - alerts, events, perfmon

* Sequencing Mode - unsequenced, sequenced

.. image:: static/images/platform/platform_streaming_receivers_list_322.png

Creating Receiver
-----------------
#. From the AOS web interface, navigate to **Platform / Streaming / Receivers**,
   then click **Create Receiver**.
#. Enter/select required values.
#. Click **Create** to create the receiver and return to the list view.

Deleting Receiver
-----------------
#. From the AOS web interface, navigate to **Platform / Streaming / Receivers**,
   then click the delete button corresponding to the receiver to delete.
#. Click **Delete** to delete the receiver from the system and return to the
   list view.
