===================
OSPF Sessions Probe
===================
Customers are alerted in case of intent violation wrt external OSPF
peering status and route expectations.
To troubleshoot external network OSPF connectivity problems, AOS provides a
predefined IBA probe that Obtain telemetry status for the OSPF sessions.
This probe will show the number of missing ospf sessions.

To start troubleshooting OSPF neighbor state follow these steps:

#. From an AOS Blueprint, go to **Analytics**, click on **Create Probe**, and
   click on **Instantiate Predefined Probe**.

#. Select "OSPF Summary"

   .. image:: static/images/iba/ospf_summary_iba_1_320.png

When you click "Create", AOS collects OSPF sessions and
present neighbors as in one single stage output table (session status,
expected sessions, active sessions and missing sessions).

.. image:: static/images/iba/ospf_session_status_iba_1_320.png
.. image:: static/images/iba/ospf_missing_sessions_iba_1_320.png
