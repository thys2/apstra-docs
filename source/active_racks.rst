=====
Racks
=====

.. image:: static/images/blueprints/active/physical/active_rack_330.png

From the blueprint, navigate to **Active / Physical / Racks** to see details about
the racks in the active blueprint. You can search for specific nodes or links and
select a layer to see anomalies, deploy modes, deployment status. traffic heat
and more. Click a rack type to see a preview.

Changing Rack Name
==================
You may want to use your own rack naming schema (for example, your rack names
could be based on their physical locations). In these cases you can modify the
existing rack names (as of AOS version 3.3.0).

#. From the blueprint, navigate to **Active / Physical / Racks**.
#. Choose the rack that needs a name change.
#. In **Rack Properties** (right panel) click the **Edit** button for the rack
   name.
#. Change the name and click the **Save** button to stage the change.

Rack names can also be changed from the staged view of the blueprint.
