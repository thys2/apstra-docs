========
IP Pools
========

IP Pool Overview
================
You can create resource pools during the design phase or just before you need them
during the build phase. When you assign resources to managed devices in your
network (blueprint), AOS pulls them automatically from the pool you specify. In
cases where you need to assign a specific network identifier you have the option
of assigning a resource individually. To prevent shortages as a network grows,
network design best practices recommend defining more resources than are required
for the initial design.

IP addresses are used in the following situations:

**Loopback IPs - Spines** -  Every spine requires an IP address for the loopback
IP. The loopback IP is used as the BGP Router ID. If a spine peers with an
external router, the router peers with the spine loopback IP.

**Loopback IPs - Leafs** - Every leaf requires an IP address for the loopback IP.
The loopback IP is used as the BGP router ID. If a leaf peers with an external
router, the router peers with the leaf loopback IP.

**SVI Subnets - MLAG Domain** - A Switch Virtual Interfaces (SVI) subnet for an
MLAG domain is used to allocate an IP address between MLAG leaf switches.

**Link IPs - Spines <-> Leafs** -  Link IPs are used between spines and leafs to
build the L3-CLOS fabric. These IPs are necessary for BGP peering between
spines and leafs, and represent the 'fabric' of the network.

**Link IPs - To External Router** - IP addresses facing the external router are
used to statically-route the external router loopback and route across that link.

----

IP pools include the following details:

Pool Name
  To identify the resource pool.

Total Usage
  Percentage of IP addresses in use for all subnets in the resource pool. (Hover
  over status bar to see number of IP addresses in use and total number of
  IP addresses in the pool.) (New in AOS version 3.3.0)

Per Subnet Usage
  The IP addresses included in the subnet and the percentage that are in use.
  (Hover over status bar to see number of IP addresses in use and total
  number of IP addresses in that subnet.) (New in AOS version 3.3.0)

Status
  Indicates if the pool is in use or not.

Tags (optional)
  To enable filtering by user-specified categories.

From the AOS web interface, navigate to **Resources / IP Pools**.

.. image:: static/images/resources/ip_pool_330.png

Creating IPv4 Pool
==================

.. important::
    If you configure a switch with an invalid IP block you may receive
    an error during the deploy phase. AOS does not constrain the range of
    IP address blocks. It is your responsibility to specify valid
    IP addresses. For example, specifying the erroneous multicast
    subnet 224.0.0.0/4 would be accepted, but it would
    result in an unsuccessful deployment.

.. sidebar:: Cloning IP Pool

   Instead of entering all details for a new IP pool, you can clone an existing
   one, give it a new name and customize it.

#. From the list view (Resources / IP Pools) click **Create IP Pool**, then
   enter a name and valid subnet.

   * To specify an additional subnet, click **Add a Subnet**.

#. Click **Create** to create the pool and return to the list view.

When the blueprint is ready, you can assign resources by
:doc:`updating assignments <build_physical>` in the **Staged / Physical** view.

Warning for Overlapping IP Pools
--------------------------------
As of AOS version 3.2.1 if you assign IP pools to a blueprint that already has
those IP pools assigned (same range or overlapping range), then a warning is
raised in the AOS web interface.

For example, if overlapping subnets are created in an IP pool and
then assigned in a blueprint under a security zone for leaf
loopback and external router link IP address space, AOS identifies
the duplicate IP addresses and raises an alert.

.. image:: static/images/ip/ip_pool_overlapping.png

.. image:: static/images/ip/overlapping_pool_assignment_sz.png

.. image:: static/images/ip/external_links_overlap.png

Similarly, this feature detects if you re-use IP addresses from an IP pool
and manually assign them as loopback for an external router or use them in any
other part of the blueprint.

.. image:: static/images/ip/Leaf_loopback.png
.. image:: static/images/ip/external_router_overlapping_ip.png
.. image:: static/images/ip/manual_assign_overlap.png

In all such scenarios warning messages about duplicate/overlap IP addresses
are raised prompting you to fix them. However, warnings do not prevent you
from committing the blueprint without fixing the warnings.

.. image:: static/images/ip/commit_with_overlap.png

Editing IPv4 Pool
=================
If any IP addresses in a pool are in use, they cannot be removed from the pool.

#. Either from the list view (Resources / IP Pools) or the details view,
   click the **Edit** button for the pool to edit.
#. Make your changes.

   * To add a subnet, click **Add a subnet**.
   * To change a subnet, change the subnet value.
   * To remove a subnet, click the **X** to the right of the subnet to delete.

#. Click **Update** to update the pool and return to the list view.

Deleting IPv4 Pool
==================
If any IP addresses in an IP pool are in use, the pool cannot be deleted.

#. Either from the list view (Resources / IP Pools) or the details view,
   click the **Delete** button for the pool to delete.
#. Click **Delete** to delete the pool and return to the list view.
