======================
Processor: EVPN Type 3
======================
The EVPN Type 3 processor generates a configuration containing
expectations of EVPN type 3 routes.

**Input Types** - Number-Set (NS), Discrete-State-Set (DSS)

**Output Types** - NSTS, DSSTS

**Properties**

   Execution count
      Number of times the data collection is done.
   Monitored VNs
      What VNs are to be monitored. Specify ``*`` to monitor
      all the VNs or list the desired ones, e.g. "1-3,6,8,10-13".
   Service Interval
      Telemetry collection interval in seconds.
   Service name
      Name of the custom collector service.

   .. include:: includes/processors/enable_streaming.rst
