==================
Hostname Telemetry
==================
AOS validates the device's hostname against intent. Mismatches result in anomalies.

Viewing Hostname Telemetry
==========================

#. Navigate to **Devices / Telemetry** to see the device telemetry list view.

   .. image:: static/images/telemetry/hostname1_list_view_322.png

#. Click **HOSTNAME** to see the detailed device list.

   .. image:: static/images/telemetry/hostname2_hostname_322.png

#. Click an individual device name to see its telemetry page, then click
   **Hostname** to see hostname telemetry.

   .. image:: static/images/telemetry/hostname3_telemetry_322.png

#. You can locate the details of any hostname mismatches here and attend to them.
