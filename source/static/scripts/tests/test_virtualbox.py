#!/usr/bin/env python
# -*- coding: utf-8 -*-
""" Tests for Virtualbox lab creation scripts
    These tests do not actually run Virtualbox commands
"""
from __future__ import unicode_literals
import unittest
from textwrap import dedent
import os
from mock import patch, call
import docs.source.static.scripts.virtualbox as vb


# pylint: disable-msg=line-too-long
class VirtualBoxTests(unittest.TestCase):
    """ Unit tests for Virtualbox scripts """

    def test_sanitize_filename(self):
        """ Ensure all 'sanitize filename' functions operate as expected"""
        # Normal files
        self.assertEqual(
            vb.sanitize_filename(u'foo'),
            u'foo')
        # files with spaces
        self.assertEqual(
            vb.sanitize_filename(u'bar 123'),
            u'bar 123')
        # Unicode and non-ascii replaced to _
        self.assertEqual(
            vb.sanitize_filename(u'This is a "いくつかのファイル名123?" test'),
            u'This is a _いくつかのファイル名123__ test')

        # leading space and trailing dot
        self.assertEqual(
            vb.sanitize_filename(u' foo bar# 123.'),
            u'_foo bar_ 123_')

    @patch('docs.source.static.scripts.virtualbox.execute')
    def test_get_vm_storage_path(self, execute):
        """ Tests VBoxManage listsystemproperties for Default Machine folder """

        sample_output = dedent('''\
        Maximum Devices per Floppy Port: 2
        Default machine folder:          /TEST_SAMPLE/MOCKED
        Raw-mode Supported:              yes
        Exclusive HW virtualization use: on
        ''')
        execute.return_value = (sample_output, None, 0)
        # Results found
        self.assertEqual(
            '"/TEST_SAMPLE/MOCKED"',
            vb.get_vm_storage_path())

        # No results
        execute.return_value = (None, None, 1)
        self.assertRaisesRegexp(ValueError,
                                'No output from VBoxManage',
                                vb.get_vm_storage_path)

        # Command returned a result, but no machine folder
        execute.return_value = ('str', None, 0)
        self.assertRaisesRegexp(ValueError,
                                'Could not find VBoxManage list',
                                vb.get_vm_storage_path)

    @patch('docs.source.static.scripts.virtualbox.execute')
    def test_vm_list(self, execute):
        """ VM List output should be consistent """
        sample_output = dedent('''\
        "arista-4.18-template" {f4e4048b-a819-4a5d-a0f7-03852709d1b7}
        "aos-server-2.0" {88a0d30f-21f8-446c-b280-a0604402596b}
        "arista-spine2" {a7616c83-44a1-4adc-b0a1-f8577064059b}
        ''')

        expected = ['arista-4.18-template', 'aos-server-2.0', 'arista-spine2']

        execute.return_value = (sample_output, None, 0)
        self.assertEqual(
            expected,
            vb.get_vm_list())
        execute.assert_called_with('{vbox} list vms'.format(vbox=vb.VBOX_CMD))

        # Command didn't return any result
        execute.return_value = (None, None, 1)
        self.assertRaisesRegexp(ValueError,
                                'Could not get VM list from',
                                vb.get_vm_list)

        # Command returned an empty result (No VMs) but did return a result
        execute.return_value = ('', None, 0)
        self.assertEqual(
            [],
            vb.get_vm_list())

    @patch('docs.source.static.scripts.virtualbox.execute')
    def test_clone_vm(self, execute):
        """ Cloning VM tests """
        execute.return_value = (
            'Machine has been successfully cloned as',
            '0%...10%...20%...30%...40%...50%...60%...70%...80%...90%...100%',
            0)

        result = vb.clone_vm('template', 'target')

        # Ensure clone task is initiated properly
        execute.assert_called_with('{vbox} clonevm template '
                                   '--name target --register --mode machine'.
                                   format(vbox=vb.VBOX_CMD))
        self.assertTrue(result)

        # Clone task fails if template name is missing
        execute.return_value = (
            '',
            '''
            VBoxManage: error: Could not find a registered machine named 'SAMPLE'
            VBoxManage: error: Details: code VBOX_E_OBJECT_NOT_FOUND (0x80bb0001), foo
            VBoxManage: error: Context: "FindMachine(Bstr(pszSrcName).raw(), bar
            ''', 1)

        self.assertRaisesRegexp(ValueError,
                                'VM Template SAMPLE not found',
                                vb.clone_vm, 'SAMPLE', 'target')

        # Clone task fails if destination disk exists
        execute.return_value = (
            '',
            '''
            VBoxManage: error: Machine settings file '/vm/arista-spine2/arista-spine2.vbox' already exists
            VBoxManage: error: Details: code VBOX_E_FILE_ERROR (0x80bb0004), component MachineWrap, interface IMachine, callee nsISupports
            VBoxManage: error: Context: "CreateMachine(bstrSettingsFile.raw(), Bstr(pszTrgName).raw(), ComSafeArrayAsInParam(groups), NULL, createFlags.raw(), trgMachine.asOutParam())" at line 472 of file VBoxManageMisc.cpp
            ''',
            1)

        self.assertRaisesRegexp(ValueError,
                                'VM Template SAMPLE target target already exists',
                                vb.clone_vm, 'SAMPLE', 'target')

        # Some other failure
        execute.return_value = ('', 'error', 1)
        self.assertRaises(ValueError, vb.clone_vm, 'SAMPLE', 'target')

    @patch('docs.source.static.scripts.virtualbox.execute')
    def test_unregister_vm(self, execute):
        """ Test removing VMs """
        sample_output = 'test'
        execute.return_value = (sample_output, None, 0)

        vb.unregister_vm('test_vm')
        execute.assert_called_with('{vbox} unregistervm test_vm --delete'.
                                   format(vbox=vb.VBOX_CMD))

        # VMs that do not exist don't need to raise any errors.
        execute.return_value = (
            '',
            '''
            VBoxManage: error: Could not find a registered machine named 'SAMPLE'
            VBoxManage: error: Details: code VBOX_E_OBJECT_NOT_FOUND (0x80bb0001), component VirtualBoxWrap, interface IVirtualBox, callee nsISupports
            VBoxManage: error: Context: "FindMachine(Bstr(VMName).raw(), machine.asOutParam())" at line 152 of file VBoxManageMisc.cpp
            ''',
            1
        )
        self.assertTrue(vb.unregister_vm('test_vm'))

        # some other failure
        execute.return_value = (None, None, 4)

        self.assertRaisesRegexp(ValueError, 'Failed to unregister VM test_vm:',
                                vb.unregister_vm, 'test_vm')

    @patch('docs.source.static.scripts.virtualbox.shutil.rmtree')
    def test_clean_vm_path(self, rmtree):
        """ Test clearing filesystem"""
        result = vb.clean_vm_path('/vm', 'test_vm')
        if os.name == 'nt':
            rmtree.assert_called_with('/vm\\test_vm')
        else:
            rmtree.assert_called_with('/vm/test_vm')
        self.assertTrue(result)

        # mock a folder or file that doesn't exist
        rmtree.side_effect = OSError('File doesnt exist or something')
        result = vb.clean_vm_path('/vm', 'test_vm')
        self.assertFalse(result)

        rmtree.side_effect = None
        # Mock a failure of extracting vm path or test vm
        self.assertRaisesRegexp(AssertionError, 'VM name cannot be none',
                                vb.clean_vm_path, '/vm', None)
        self.assertRaisesRegexp(AssertionError, 'VM name cannot be none',
                                vb.clean_vm_path, '/vm', '')

        self.assertRaisesRegexp(AssertionError, 'VM path cannot be none',
                                vb.clean_vm_path, None, 'vm_name')
        self.assertRaisesRegexp(AssertionError, 'VM path cannot be none',
                                vb.clean_vm_path, '', 'vm_name')

    def test_execute(self):
        """ Test executing filesystem operations """
        # assuming (hopefully) 'dir' exists on most systems...

        out, err, exit_code = vb.execute('dir')

        self.assertEqual(0, exit_code)
        self.assertEqual('', err)
        self.assertIsNotNone(out)

        out, err, exit_code = vb.execute('dir AFJSDHHAGFF$#HG')

        # 1 for linux 2 for windows...
        self.assertNotEqual(0, exit_code)
        self.assertNotEqual('', err)
        self.assertIsNotNone(out)

        # An invalid command should raise an exception
        self.assertRaises(OSError, vb.execute, 'invalid_command')

    def test_get_vm_name(self):
        """ Ensure VM name responses are consistent"""
        self.assertEqual(
            'arista-spine1',
            vb.get_vm_name('spine1', 'arista'))
        self.assertEqual(
            'cumulus-rack1-leaf1',
            vb.get_vm_name('rack1-leaf1', 'cumulus'))
        self.assertEqual(
            'ubuntu-router1',
            vb.get_vm_name('router1', 'ubuntu'))


    @patch('docs.source.static.scripts.virtualbox.execute')
    def test_unregister_disks_for_vm(self, execute):
        """ Unregister disk tests """
        execute.return_value = ('', '', 0)
        self.assertIsNone(
            vb.unregister_disks_for_vm('/vm', 'test')
        )
        if os.name == 'nt':
            execute.assert_has_calls([
                call('{vbox} closemedium "/vm\\\\test\\\\test-disk1.vmdk" --delete'.
                     format(vbox=vb.VBOX_CMD)),
                call('{vbox} closemedium "/vm\\\\test\\\\test.vdi" --delete'.
                     format(vbox=vb.VBOX_CMD))
            ])
        else:
            execute.assert_has_calls([
                call('{vbox} closemedium "/vm/test/test-disk1.vmdk" --delete'.
                     format(vbox=vb.VBOX_CMD)),
                call('{vbox} closemedium "/vm/test/test.vdi" --delete'.
                     format(vbox=vb.VBOX_CMD))
            ])

        # VM file doesn't exist
        execute.return_value = (
            '',
            '''
            VBoxManage: error: Could not find file for the medium '/vm/test/test-disk1.vmdk' (VERR_FILE_NOT_FOUND)
            VBoxManage: error: Details: code VBOX_E_FILE_ERROR (0x80bb0004), component MediumWrap, interface IMedium, callee nsISupports
            VBoxManage: error: Context: "OpenMedium(Bstr(pszFilenameOrUuid).raw(), enmDevType, enmAccessMode, fForceNewUuidOnOpen, pMedium.asOutParam())" at line 178 of file VBoxManageDisk.cpp
            ''',
            1
        )
        self.assertIsNone(
            vb.unregister_disks_for_vm('/vm', 'test')
        )

        # VM is running
        execute.return_value = (
            '',
            '''
            VBoxManage: error: Cannot delete storage: medium '/vm/ubuntu-router1/ubuntu-router1.vdi' is still attached to the following 1 virtual machine(s): 73623828-40bd-4d69-84f0-d0666d3533d6
            VBoxManage: error: Details: code VBOX_E_OBJECT_IN_USE (0x80bb000c), component MediumWrap, interface IMedium, callee nsISupports
            VBoxManage: error: Context: "DeleteStorage(pProgress.asOutParam())" at line 1545 of file VBoxManageDisk.cpp
            VBoxManage: error: Failed to delete medium. Error code Unknown Status -2135228404 (0x80bb000c)
            VBoxManage: error: Medium '/vm/ubuntu-router1/ubuntu-router1.vdi' cannot be closed because it is still attached to 1 virtual machines
            VBoxManage: error: Details: code VBOX_E_OBJECT_IN_USE (0x80bb000c), component MediumWrap, interface IMedium, callee nsISupports
            VBoxManage: error: Context: "Close()" at line 1554 of file VBoxManageDisk.cpp
            ''',
            1
        )
        self.assertRaisesRegexp(ValueError, 'Disk in use .*, unregister VM first',
                                vb.unregister_disks_for_vm, '/vm', 'in_use')

        execute.return_value = ('', 'error output', 1)
        self.assertRaisesRegexp(ValueError, 'Failed to unregister disk',
                                vb.unregister_disks_for_vm, '/vm', 'test')

if __name__ == "__main__":
    unittest.main()
