#!/usr/bin/env python
# -*- coding: utf-8 -*-
""" Virtualbox tool to create and deploy a topology """
from __future__ import unicode_literals
# Copyright Apstra 2017, Confidential and Proprietary
import subprocess
import re
from collections import OrderedDict
import logging
import shutil
import os
import time
import shlex

logger = logging.getLogger(__name__)  # pylint: disable=invalid-name
FORMAT = "%(asctime)-15s %(levelname)s %(filename)s:" \
         "%(funcName)s:%(lineno)d %(message)s"
logging.basicConfig(level=logging.DEBUG, format=FORMAT)
if os.name == 'nt':
    MGMT_NET_NAME = '"VirtualBox Host-Only Ethernet Adapter"'
else:
    MGMT_NET_NAME = 'vboxnet0'
MGMT_NETWORK = '192.168.56.0/24'
MGMT_IP = '192.168.56.1'
MGMT_NETMASK = '255.255.255.0'
MGMT_DHCP = True

MGMT_NIC_TYPE = 'hostonly'  # hostonly, natnetwork, bridged

DEVICES = OrderedDict({
    'spine1': {
        'links': [
            MGMT_NET_NAME,
            'spine1_rack1-leaf1',
            'spine1_rack1-leaf2',
            'spine1_rack2-leaf1',
            'spine1_rack2-leaf2',
            'unused',
            'spine1_router1',
            'unused',
        ],
        'role': 'switch',
    },
    'spine2': {
        'links': [
            MGMT_NET_NAME,
            'spine2_rack1-leaf1',
            'spine2_rack1-leaf2',
            'spine2_rack2-leaf1',
            'spine2_rack2-leaf2',
            'unused',
            'spine2_router1',
            'unused',
        ],
        'role': 'switch',
    },
    'rack1-leaf1': {
        'links': [
            MGMT_NET_NAME,
            'spine1_rack1-leaf1',
            'spine2_rack1-leaf1',
            'rack1-leaf1_rack1-leaf2_A',
            'rack1-leaf1_rack1-leaf2_B',
            'rack1-leaf1_rack1-server1',
            'rack1-leaf1_rack1-server2',
            'rack1-leaf1_router1',
        ],
        'role': 'switch',
    },
    'rack1-leaf2': {
        'links': [
            MGMT_NET_NAME,
            'spine1_rack1-leaf2',
            'spine2_rack1-leaf2',
            'rack1-leaf1_rack1-leaf2_A',
            'rack1-leaf1_rack1-leaf2_B',
            'rack1-leaf2_rack1-server1',
            'rack1-leaf2_rack1-server2',
            'rack1-leaf2_router1',
        ],
        'role': 'switch',
    },
    'rack2-leaf1': {
        'links': [
            MGMT_NET_NAME,
            'spine1_rack2-leaf1',
            'spine2_rack2-leaf1',
            'rack2-leaf1_rack2-leaf2_A',
            'rack2-leaf1_rack2-leaf2_B',
            'rack2-leaf1_rack2-server1',
            'rack2-leaf1_rack2-server2',
            'rack2-leaf1_router1',
        ],
        'role': 'switch',
    },
    'rack2-leaf2': {
        'links': [
            MGMT_NET_NAME,
            'spine1_rack2-leaf2',
            'spine2_rack2-leaf2',
            'rack2-leaf1_rack2-leaf2_A',
            'rack2-leaf1_rack2-leaf2_B',
            'rack2-leaf2_rack2-server1',
            'rack2-leaf2_rack2-server2',
            'rack2-leaf2_router1',
        ],
        'role': 'switch',
    },
    'router1': {
        'links': [
            MGMT_NETWORK,
            'spine1_router1',
            'spine1_router2',
            'rack1-leaf1_router1',
            'rack1-leaf2_router1',
            'rack2-leaf1_router1',
            'rack2-leaf2_router1'
        ],
        'role': 'server',
    },
    'rack1-server1': {
        'links': [
            MGMT_NET_NAME,
            'rack1-leaf1_rack1-server1',
            'rack1-leaf2_rack1-server1',
        ],
        'role': 'server',
    },
    'rack1-server2': {
        'links': [
            MGMT_NET_NAME,
            'rack1-leaf1_rack1-server2',
            'rack1-leaf2_rack1-server2',
        ],
        'role': 'server',
    },
    'rack2-server1': {
        'links': [
            MGMT_NET_NAME,
            'rack2-leaf1_rack1-server1',
            'rack2-leaf2_rack1-server1',
        ],
        'role': 'server',
    },
    'rack2-server2': {
        'links': [
            MGMT_NET_NAME,
            'rack2-leaf1_rack1-server2',
            'rack2-leaf2_rack1-server2',
        ],
        'role': 'server',
    },
})
# switch OS types to clone
OS_TYPES = ('cumulus', 'arista')

# Virtualbox templates to clone
TEMPLATES = {
    'cumulus': {
        'memory': 2048,
        'template': 'cumulus-3.3-template',
        'role': 'switch',
    },
    'arista': {
        'memory': 2560,
        'template': 'arista-4.18-template',
        'role': 'switch',
    },
    'ubuntu': {
        'memory': 512,
        'template': 'ubuntu-template',
        'role': 'server',
    },
    'centos': {
        'memory': 512,
        'template': 'centos-6.8-template',
        'role': 'server'
    }
}

if os.name == 'nt':
    VBOX_CMD = os.path.join(os.environ['VBOX_MSI_INSTALL_PATH'], 'VBoxManage.exe')
    # Shlex hack for Windows
    VBOX_CMD = VBOX_CMD.replace('\\', '\\\\')
else:
    VBOX_CMD = 'VBoxManage'


def sanitize_filename(filename):
    """ Sanitizes filenames of VirtualBox filenames

    Cleans up filenames to be compatible with filesystem filenames.
    Does not handle path mangling, only filenames themselves.

        Permitted:
            - Basic ASCII
            - Unicode from Latin-1 alphabetic to the end of Hangul
            - Skips anything that can be considered encoding characters
            - Leading and trailing dots, slashes, spaces, etc

        Replaced:
            - All characters that are not permitted are replaced with '_'

    Same as Virtualbox sanitiseMachineFilename from VirtualBox
    src/VBox/Main/src-server/VirtualBoxImpl.cpp

    See https://www.virtualbox.org/browser/vbox/trunk

    Args:

        filename (unicode): Filename to sanitize

    Returns:
        (unicode): Sanitized filename

    Examples:
        >>> print sanitize_filename('foo')
        foo
        >>> print sanitize_filename('bar 123')
        bar 123
        >>> print sanitize_filename(u'This is a "いくつかのファイル名123？" test')
        This is a _いくつかのファイル名123__ test
        >>> print sanitize_filename(' foo bar# 123.')
        _foo bar_ 123_
    """

    allowed = [r'\(', r'\)', r'_', r'\ ', r'\.', r'\-',  # Some special chars okay
               r'0-9', r'A-Z', r'a-z',  # Simple ascii
               r'\u00a0-\ud7af']  # Unicode Latin-1 to end of Hangul
    pattern = r'([^%s])' % r"|".join(r'%s' % p for p in allowed)
    filename = re.sub(pattern, '_', filename)

    # leading dot, hypen, or spaces to underscore "_"
    filename = re.sub(r"^(\.|\-|\ )", "_", filename)

    # trailing dot or space to underscore  "_"
    filename = re.sub(r"(\.|\ )$", "_", filename)
    return filename


def get_vm_list():
    """ Gets a list of registered VMs on Virtualbox

        Returns:
            (list): List of virtualbox VM names
    """
    cmd = '{vbox_cmd} list vms'.format(vbox_cmd=VBOX_CMD)
    out, _, exit_code = execute(cmd)
    if out is None or exit_code != 0:
        raise ValueError, 'Could not get VM list from %s' % cmd
    vm_list = re.findall(r'\"(?P<vm>.*)\"', out, re.MULTILINE)
    return vm_list


def get_running_vm_list():
    """ Get a list of running VMs in VirtualBox

        Returns:
            (list): List of running VirtualBox VM names
    """
    cmd = '{vbox_cmd} list runningvms'.format(vbox_cmd=VBOX_CMD)
    out, _, exit_code = execute(cmd)
    if out is None or exit_code != 0:
        raise ValueError, 'Could not get VM list from %s' % cmd
    vm_list = re.findall(r'\"(?P<vm>.*)\"', out, re.MULTILINE)
    return vm_list


def execute(command):
    """ Executes a shell command and returns its result

    Args:
        command (str): Command to execute on the shell

    Returns:
        out, err, exit_code:  stdout, stderr, and error code of shell command
    Raises:
        OSError if the command failed to execute
    """
    cmd = shlex.split(command)
    try:
        proc = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        out, err = proc.communicate()

        exit_code = proc.poll()
        logger.debug('Status %s from %s', exit_code, command)
        return out, err, exit_code

    except OSError as exc:
        raise OSError('Failed to run %s: %s' % (command, exc))


# VBoxManage tools
def get_vm_name(device, os_type):
    """ Builds a predictable VM name based on the OS type.  Looks at name of
        device and returns 'ubuntu-{device}' if we think it should be a server vm

        Args:
            device (str): device from global DEVICES, eg 'spine2'
            os_type (str): os type from OS_TYPES, eg 'cumulus'

        Returns:
            (str): Formatted string representing the device

        Examples:
            >>> get_vm_name('spine1', 'cumulus')
            cumulus-spine1
            >>> get_vm_name('router', 'arista')
            ubuntu-router1

    """
    return '{os_type}-{device}'.format(
        os_type=os_type, device=device)


def modify_vm(vm_name, command):
    """ Modifes a non-running VM """
    vbox = '{vbox} modifyvm {vm_name} '.format(
        vbox=VBOX_CMD, vm_name=vm_name)
    enqueue_cmd(vbox + command)


def control_vm(vm_name, command):
    """ Controls a running VM """
    vbox = '{vbox} controlvm {vm_name} '.format(
        vbox=VBOX_CMD, vm_name=vm_name)
    enqueue_cmd(vbox + command)


def enqueue_cmd(command):
    """ Quick wrapper for command queuing """
    err, out, exit_code = execute(command)
    if exit_code != 0:
        raise ValueError('Failed to run VM command: %s\nOutput:%s\nError:%s',
                         command, out, err)


def get_vm_storage_path():
    """ Gets the Virtualbox default machine path that new VMs are created in

        Returns:
            (str): Default VM path

        Example:
            >>> get_vm_storage_path()
            '/vm'
    """
    out, _, _ = execute('{vbox} list systemproperties'.format(vbox=VBOX_CMD))
    if not out:
        raise ValueError('No output from VBoxManage list systemproperties')
    pattern = r'.*Default machine folder: \s+(?P<folder>.*)$'
    match = re.search(pattern, out, re.MULTILINE)

    if match and 'folder' in match.groupdict():
        return '"{}"'.format(match.groupdict()['folder'].strip())

    raise ValueError, 'Could not find VBoxManage list systemproperties ' \
                      'default machine folder'


def set_nic_settings():
    """ Wires all NICs based on cable map from DEVICES global """
    vm_list = get_vm_list()
    running_vm_list = get_running_vm_list()

    def config_mgmt_nic(nic_index, is_running):
        """ Configures a management NIC based on index
            Looks at MGMT_NIC_TYPE settings
        """
        if MGMT_NIC_TYPE == 'hostonly':
            if not is_running:
                modify_vm(vm_name, '--nic{index} hostonly'.format(index=nic_index))
                modify_vm(vm_name, '--hostonlyadapter{index} {mgmt_net_name}'.
                          format(index=index, mgmt_net_name=MGMT_NET_NAME))
            else:
                control_vm(vm_name, 'nic{index} hostonly {mgmt_net_name}'.format(
                    index=nic_index, mgmt_net_name=MGMT_NET_NAME))

        elif MGMT_NIC_TYPE == 'natnetwork':
            if not is_running:
                modify_vm(vm_name, '--nic{index} natnetwork'.format(index=nic_index))
                modify_vm(vm_name, '--natnet{index} {mgmt_net_name}'.
                          format(index=index, mgmt_net_name=MGMT_NET_NAME))
            else:
                control_vm(vm_name, '--nic{index} natnetwork'.format(
                    index=nic_index))
                control_vm(vm_name, '--natnet{index} {mgmt_net_name}'.
                           format(index=index, mgmt_net_name=MGMT_NET_NAME))

    for device, device_data in DEVICES.iteritems():
        os_types = [t for t, d in TEMPLATES.iteritems()
                    if d['role'] == device_data['role']]
        for os_type in os_types:
            vm_name = get_vm_name(device, os_type)
            ports = device_data['links']
            index = 0

            if vm_name not in vm_list:
                logging.warning('%s is not registered in Virtualbox', vm_name)
                continue
            else:
                logging.info('Connecting VM %s', vm_name)

            is_running = vm_name in running_vm_list
            for port in ports:
                index = index + 1

                if index == 1:
                    config_mgmt_nic(index, is_running)
                    continue
                elif vm_name not in running_vm_list:
                    modify_vm(vm_name, '--nic{index} intnet'.format(index=index))
                    modify_vm(vm_name, '--intnet{index} {network}'.format(
                        index=index, network=port))
                    modify_vm(vm_name, '--nictype{index} virtio'.format(
                        index=index))
                    modify_vm(vm_name, '--nicpromisc{index} allow-all'.format(
                        index=index))
                    # Should only change the MAC address if it's a new VM - in which
                    # clonevm will do for us anyway by default
                    #modify_vm(vm_name, '--macaddress{index} auto'.format(
                    #    index=index))

                elif vm_name in running_vm_list:
                    control_vm(vm_name,
                               'nic{index} intnet {network}'.format(
                                   index=index, network=port))
                    control_vm(vm_name, 'nicpromisc{index} allow-all'.format(
                        index=index))
                else:
                    raise ValueError('VM name %s does not exist' % vm_name)


def unregister_disks_for_vm(path, vm_name):
    """ Unregisters possible VM disk names for specified VM.  Guesses filename of
        disk instead of parsing command output.  May be fragile :)

        Args:
            path (str): VM Storage root for given VM
            vm_name (str): Virtual machine name
    """

    def unregister(suffix):
        """ Unregisters VM based on guessed disk suffix

            Args:
                suffix (str): VM Disk suffix, eg '.vdi'
        """

        disk_name = '"{}{}"'.format(os.path.join(path.replace('"', ''),
                                                 sanitize_filename(vm_name),
                                                 sanitize_filename(vm_name)), suffix)
        if os.name == 'nt':
            disk_name = disk_name.replace('\\', '\\\\')
        cmd = '{vbox} closemedium {disk_name} --delete'.format(vbox=VBOX_CMD,
                                                               disk_name=disk_name)
        _, err, exit_code = execute(cmd)
        if err and exit_code != 0:
            if 'VERR_FILE_NOT_FOUND' in err or 'VERR_PATH_NOT_FOUND' in err:
                logger.debug('File %s not found', disk_name)
            elif 'VBOX_E_OBJECT_IN_USE' in err:
                raise ValueError('Disk in use %s, unregister VM first \n%s',
                                 disk_name, err)
            else:
                raise ValueError('Failed to unregister disk %s\n%s' % (cmd, err))
        else:
            logger.info('Unregistered disk %s', disk_name)
            # would there be more than one type registered?

    unregister('-disk1.vmdk')
    unregister('.vdi')


def unregister_vm(vm_name, clean=False):
    """ Unregisters and deletes a registered VM from virtualbox

        Args:
               vm_name (str): Virtual machine name to unregister and delete

        Raises:
            ValueError if deleting the VM was not successful

        Returns:
            bool: True if VM was unregistered properly
    """
    cmd = '{vbox} unregistervm {vm_name} --delete'.format(
        vbox=VBOX_CMD, vm_name=vm_name)
    out, err, exit_code = execute(cmd)
    success = False
    if exit_code == 0 and out is not None:
        logger.info('Unregistered VM %s', vm_name)
        return True
    if exit_code != 0:
        if err is not None:
            if 'VBOX_E_OBJECT_NOT_FOUND' in err:
                logger.debug('VM %s not found', vm_name)
                return True
            if 'VBOX_E_INVALID_OBJECT_STATE' in err:
                logger.debug('VM %s still running', vm_name)
                if clean:
                    shut_down_vm(vm_name)
                    time.sleep(5)  # Give VM enough time to properly shut down
                    success = unregister_vm(vm_name, clean=False)
                else:
                    raise (ValueError, "Failed to unregister VM %s - still running: "
                                       "\n%s\n%s",
                           vm_name, cmd, err)
    if not success:
        raise ValueError('Failed to unregister VM %s: %s\n%s\n%s' %
                         (vm_name, cmd, out, err))


def clean_vm_path(path, vm_name):
    """ Deletes a VM path and all of its contents

        Args:
            path (str): Filesystem path to delete VMs from
            vm_name (str): VM name to delete from the filesystem
        Returns:
            True if the path was successfully removed
    """
    assert vm_name, "VM name cannot be none"
    assert path, "VM path cannot be none"
    try:
        shutil.rmtree(os.path.join(path, vm_name))
        return True
    except OSError:
        return False


def shut_down_vm(vm_name):
    """ Shuts down a VM

        Args:
            vm_name (str): VM to shut down
        Returns:
            True if VM is shut down

    """
    cmd = '{vbox} controlvm {vm_name} poweroff'.format(
        vbox=VBOX_CMD, vm_name=vm_name)

    _, err, exit_code = execute(cmd)
    if exit_code == 0:
        logger.info('VM %s stopped successfully', vm_name)
        return True
    else:
        if 'is not currently running' in err:
            logger.debug('VM %s is not running', vm_name)
            return True
    raise ValueError('VM %s not stopped:\n%s\n%s', vm_name, cmd, err)


def set_memory_for_vm(vm_name, memory):
    """ Sets memory for a given VM Name
        Args:
            memory (int): Memory to set
        Returns:
            True if successful
        Raises:
            ValueError if VirtualBox failed for some reason
    """

    cmd = '{vbox} modifyvm {vm_name} '.format(
        vbox=VBOX_CMD, vm_name=vm_name)

    _, err, exit_code = execute(cmd + '--memory {}'.format(memory))
    if exit_code == 0:
        logger.info('VM %s memory set successfully to %s', vm_name, memory)
        return True
    raise ValueError('VM %s memory not set:\n%s\n%s', vm_name, cmd, err)


def clone_vm(template, target):
    """ Clones a specified VM template to a new target VM name.

    Args:
        template (str): Template to source the clone from
        target (str): Destination VM name to clone to

    Raises:
        ValueError if the clone fails
    """
    cmd = '{vbox} clonevm {template} --name {target} --register --mode machine'. \
        format(vbox=VBOX_CMD, template=template, target=target)
    out, err, exit_code = execute(cmd)
    if exit_code == 0:
        if 'Machine has been successfully cloned' in out:
            logger.info('Cloned VM template %s to %s', template, target)
            return True
    if exit_code != 0 and err:
        if 'VBOX_E_OBJECT_NOT_FOUND' in err:
            raise ValueError('VM Template %s not found' % template)
        elif 'VBOX_E_FILE_ERROR' in err:
            raise ValueError('VM Template %s target %s already exists\n'
                             'Try deleting the VM' % (template, target))
        elif 'VBOX_E_INVALID_OBJECT_STATE' in err:
            raise ValueError('VM Template %s is running\n'
                             'Try stopping the template' % template)
    raise ValueError('Failed to clone VM %s\n%s\n%s', cmd, out, err)


def clone_vms_from_template(clean=False):
    """ Clones all VMs from their base templates.
        Args:
            clean (bool): If True, will delete VMs before cloning them. Default false
    """
    vm_list = get_vm_list()
    running_vm_list = get_running_vm_list()
    vm_machine_path = get_vm_storage_path()

    for device, device_data in DEVICES.iteritems():
        os_types = [t for t, d in TEMPLATES.iteritems()
                    if d['role'] == device_data['role']]
        for os_type in os_types:
            vm_name = get_vm_name(device, os_type)

            if vm_name in vm_list and not clean:
                logging.warning('%s already exists, skipping clone', vm_name)
            template = TEMPLATES[os_type]['template']
            memory = TEMPLATES[os_type]['memory']

            if template not in vm_list:
                logger.error('Template %s not found', template)
                continue

            if clean:
                unregister_vm(vm_name, clean)
                unregister_disks_for_vm(vm_machine_path, vm_name)
                clean_vm_path(vm_machine_path, vm_name)

            # don't clone VMs that already exist
            if vm_name not in vm_list or clean:
                clone_vm(template, vm_name)
            else:
                logger.info('VM %s already exists, skipping clone', vm_name)

            if vm_name not in running_vm_list:
                set_memory_for_vm(vm_name, memory)
            else:
                logger.info('VM %s already running, not changing memory', vm_name)


def create_nat_mgmt_network():
    """ Creates 'natnetwork' type management network"""

    def create_or_modify(action):
        """ Adds the network if it's new, modifies if it exists """
        cmd = '{vbox} natnetwork {action} --netname {mgmt_net_name} ' \
              '--network {mgmt_network} --enable --dhcp {dhcp}'.format(
                  vbox=VBOX_CMD,
                  action=action, mgmt_net_name=MGMT_NET_NAME,
                  mgmt_network=MGMT_NETWORK,
                  dhcp='on' if MGMT_DHCP else 'off')
        return cmd

    cmd = create_or_modify('add')
    out, err, exit_code = execute(cmd)

    if exit_code != 0:
        if 'server already exists' in err:
            # try modifying
            cmd = create_or_modify('modify')
            out, err, exit_code = execute(cmd)
            if exit_code == 0:
                logger.info('Successfully modified management network\n %s', cmd)
                return
        raise ValueError("Could not create mgmt network: %s\n%s\n%s", cmd, out, err)
    else:
        logger.info('Successfully added management network\n %s', cmd)


def create_hostonly_if(expected_name):
    """ Creates hostonly interface """
    cmd = '{vbox} hostonlyif create'.format(vbox=VBOX_CMD)
    out, err, exit_code = execute(cmd)

    # on windows, interface name has quotations around
    expected_name = expected_name.replace('"', '')
    if exit_code == 0:
        pattern = r'Interface \'{}\' was successfully created'.format(expected_name)
        match = re.search(pattern, out)
        if match:
            if expected_name not in out:
                raise ValueError('Wrong interface name created: %s\n%s', cmd, out)
            logger.info('Interface %s created successfully', expected_name)
            return True

    raise ValueError('Interface %s was not created: %s\n%s\n%s',
                     expected_name, cmd, out, err)


def create_hostonly_mgmt_network(failed=False):
    """ Creates a hostonly management network if it does not exist, and modifies
        it if it does.
    """
    cmd = '{vbox} hostonlyif ipconfig {mgmt_net_name} ' \
          '-ip {mgmt_ip} --netmask {mgmt_netmask}' \
        .format(vbox=VBOX_CMD, mgmt_net_name=MGMT_NET_NAME, mgmt_ip=MGMT_IP,
                mgmt_netmask=MGMT_NETMASK)

    _, err, exit_code = execute(cmd)

    if exit_code != 0:
        if "could not be found" in err and not failed:
            create_hostonly_if(MGMT_NET_NAME)
        if not failed:
            create_hostonly_mgmt_network(failed=True)
        if failed:
            raise ValueError('Creation of host-only network failed twice: %s\n%s',
                             cmd, err)
    else:
        logger.info('Creation of network %s successful', MGMT_NET_NAME)


def remove_dhcp_servers():
    """ Removes any extra DHCP servers that will conflict with the lab """
    cmd = '{vbox} dhcpserver remove --ifname {mgmt_net_name}'. \
        format(vbox=VBOX_CMD, mgmt_net_name=MGMT_NET_NAME)

    _, err, exit_code = execute(cmd)

    if exit_code == 0:
        logger.info('Removed DHCP server for %s', MGMT_NET_NAME)
        return True
    if exit_code != 0:
        if 'DHCP server does not exist' in err:
            logger.debug('DHCP Server %s does not exist', MGMT_NET_NAME)
            return True
    raise ValueError('Could not remove DHCP server: %s\n%s', cmd, err)


def main():
    """ Build and connect all VMs """
    # create_nat_mgmt_network()
    create_hostonly_mgmt_network()
    remove_dhcp_servers()
    clone_vms_from_template(clean=True)
    set_nic_settings()


if __name__ == '__main__':
    main()
