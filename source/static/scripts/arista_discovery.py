#!/usr/bin/env python
# Copyright 2014-present, Apstra, Inc. All rights reserved.
#
# This source code is licensed under End User License Agreement found in the
# LICENSE file at http://apstra.com/eula

""" Script to perform discovery against Arista switches for Apstra AOS and help
with tech support
"""
# pylint: disable=import-error, invalid-name

import json
import zipfile
from EapiClientLib import EapiClient, EapiException

CLI_CMDS = {
    "lldp": [
        "show lldp neighbors",
        "show lldp neighbors detail",
        ],
    "bgp": [
        "show run section bgp",
        "show ip bgp summary",
        "show ip bgp neighbors",
        "show ip bgp",
        "show ip prefix-list",
        "show route-map",
        "show bfd neighbors",
        "show bfd neighbors history",
        "show ip as-path access-list",
        ],
    "interfaces": [
        "show interfaces status",
        "show interfaces counters",
        "show interfaces counters errors",
        "show interfaces capabilities",
        "show interfaces capabilities default",
        ],
    "vxlan": [
        "show running-config section vxlan",
        "show vxlan address-table",
        "show vxlan flood vtep",
        "show vxlan vtep"
    ],
    "routing": [
        "show ip route",
        "show ip interface",
        "show mac address-table",
        "show ip arp",
        "show vrrp",
        "show vrrp all",
    ],
    "mlag": [
        "show run section mlag",
        "show mlag",
        "show mlag interfaces",
        "show mlag detail",
        "show mlag config-sanity all"
    ],
    "spanning-tree": [
        "show running-config section spanning-tree",
        "show spanning-tree",
        "show spanning-tree detail",
        "show spanning-tree blockedports",
        "show spanning-tree mst",
        "show spanning-tree root",
    ],
    "device": [
        "show clock",
        "show hostname",
        "show hosts",
        "show version",
        "show inventory",
        "show environment all",
        "show extensions",
        "show extensions detail",
        "show installed-extensions",
        "show boot",
        "more flash:boot-config",
        "more flash:boot-extensions",
        "show daemon",
        "show system coredump",
        "bash timeout 5 free -m",
    ],
    "filesystem": [
        "dir /all all-filesystems",
        "dir /all flash:.aos",
        "bash timeout 5 df -h",
    ],
    "events": [
        "show event-monitor mac",
        "show event-monitor arp",
        "show logging 5000",
        "bash timeout 5 sudo tail -500 /var/log/messages",
    ],
    "config": [
        "show running-config",
        "show startup-config",
        "more flash:.aos/checkpoint_file",
        "more flash:.aos/aos_arista_config_file",
    ],
    "port-channel": [
        "show running-config section port-channel",
        "show port-channel detail all-ports",
        "show port-channel limits",
        "show port-channel all-ports",
        "show port-channel summary",
    ],
    "aos": [
        "more flash:aos-config",
        "bash timeout 5 ls /var/log/aos",
        "bash timeout 5 sudo service aos status",
        "bash timeout 5 sudo systemctl aos status",
        "dir file:/var/log/aos",
        "more file:/var/log/aos/DeviceKeeperAgent.err",
        "more file:/var/log/aos/DeploymentProxyAgent.err",
        "more file:/var/log/aos/DeviceTelemetryAgent.err",
        "more file:/var/log/aos/CounterProxyAgent.err",
    ],
    "processes": [
        "show processes",
        "show processes top memory once",
    ],
    "platform": [
        "show platform xp status",
        "show platform sand compatibility",
        "show platform sand voq tail-drop",
        "show platform sand capabilities",
        "show platform sand recirculation mapping",
        "show platform petraA tcam summ",
        "show platform arad vxlan vtep encapsulation",
        "show platform arad ip rout",
        "show platform arad vxlan",
        "show platform arad mac-address-table",
        "show platform fap",
        "show platform fap arp",
        "show platform trident system detail",
        "show platform trident interface map",
        "show platform trident mac-address-table",
        "show platform trident vxlan vtep detail",
        "show platform trident l3 software next-hops",
        "show platform jericho mapping",
        "show platform fm6000 vxlan",
        "show platform fm6000 tcam usag",
        "show platform fm6000 l3 hardware routes",
        "show platform fm6000 vxlan next-hop software",
        "show platform fm6000 mapper mac",
        "show platform fm6000 agileports",
        "show hardware access-list region",
        "show hardware tcam profile",
        "show hardware capacity",
    ]
}


def show_eapi_data(data):
    """ Prints EAPI Response data """
    response = ''
    for result in data['result']:
        # text
        if 'output' in result:
            response += result['output']
        # json
        else:
            response += json.dumps(result, indent=4)
    return response

def eapi_cmds():
    """ Run and execute Arista discovery commands for Apstra AOS """

    with zipfile.ZipFile('/tmp/arista_discovery.zip', 'w') as zip_archive:
        client = EapiClient()
        for section, commands in CLI_CMDS.iteritems():
            output = ['{:=^79}'.format(section)]
            for cmd in commands:
                try:
                    print 'Collecting information for %s.txt - %s' % (section, cmd)
                    # Text
                    output.append('{:=^79}'.format(cmd))
                    data = client.runCmds(1, ['enable', cmd], 'text')
                    output.append(show_eapi_data(data))

                    # Json
                    output.append('{:=^79}'.format(cmd + ' | json'))
                    data = client.runCmds(1, ['enable', cmd], 'json')
                    output.append(show_eapi_data(data))

                except EapiException as e:
                    output.append('Could not run %s - \n%s' % (cmd, e))
            zip_archive.writestr(section + '.txt', '\n'.join(output))
    print zip_archive.printdir()
    print '\nZip file /tmp/arista_discovery.zip completed'

def main():
    """ Collect command outputs"""
    eapi_cmds()


if __name__ == "__main__":
    main()
