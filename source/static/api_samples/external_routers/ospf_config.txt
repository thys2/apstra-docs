ip route 10.0.0.2/32 10.60.61.1 bond0.61
ip route 10.0.0.3/32 10.61.61.1 bond1.61
ip route 10.0.0.4/32 10.60.62.1 bond0.62
ip route 10.0.0.5/32 10.61.62.1 bond1.62
ip route 10.0.0.6/32 10.60.60.1 bond0.60
ip route 10.0.0.7/32 10.61.60.1 bond1.60
ipv6 route ::/0 Null0
!
interface bond0.60
 ip ospf area 51
!
interface bond0.62
 ip ospf area 51
!
interface bond1.60
 ip ospf area 51
!
interface bond1.62
 ip ospf area 51
!
router ospf
 redistribute static route-map STATIC_TO_OSPF
!
ip prefix-list DEFAULT_V4 seq 5 permit 0.0.0.0/0
ipv6 prefix-list DEFAULT_V6 seq 5 permit ::/0
!
route-map STATIC_TO_OSPF permit 10
 match ip address prefix-list DEFAULT_V4
!
route-map STATIC_TO_OSPF permit 20
 match ipv6 address prefix-list DEFAULT_V6
