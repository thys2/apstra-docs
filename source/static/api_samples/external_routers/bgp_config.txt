router bgp 100
  bgp router-id 9.0.0.1
  neighbor 172.20.0.4 remote-as 65416
  neighbor 172.20.0.4 ebgp-multihop 2
  neighbor 172.20.0.4 timers 1 3
  neighbor 172.20.0.4 timers connect 5
  neighbor 172.20.0.4 default-originate
  neighbor 172.20.0.4 soft-reconfiguration inbound
  neighbor 172.20.0.4 update-source lo:1
  neighbor 172.20.0.4 route-map PREPEND-FABRIC-PREFIX out

  neighbor 172.20.0.6 remote-as 65417
  neighbor 172.20.0.6 ebgp-multihop 2
  neighbor 172.20.0.6 timers 1 3
  neighbor 172.20.0.6 timers connect 5
  neighbor 172.20.0.6 default-originate
  neighbor 172.20.0.6 soft-reconfiguration inbound
  neighbor 172.20.0.6 update-source lo:1
  neighbor 172.20.0.6 route-map PREPEND-FABRIC-PREFIX out
!
ip route 172.20.0.4/32 10.0.0.0
ip route 172.20.0.5/32 10.0.0.2
!
route-map PREPEND-FABRIC-PREFIX permit 10
  set as-path prepend 100 100 100 100
