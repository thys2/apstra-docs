==================================================
Hypervisor Redundancy Checks Probe (Virtual Infra)
==================================================

Purpose
   Detect hypervisor redundancy.

Source Processors
   Hypervisor and connected leaf (:doc:`generic graph collector <processor_generic_graph_collector>`)
      output stage: hypervisor_and_leaf (text set) (generated from graph)

   Hypervisor pnic and vnet (:doc:`generic graph collector <processor_generic_graph_collector>`)
      output stage: hypervisor_pnic_vnet (text set) (generated from graph)

Additional Processor(s)
   Hypervisor and leaf count (:doc:`set count <processor_set_count>`)
      input stage: hypervisor_and_leaf

      output stage: hypervisors_leaf_count (number set)

   Hypervisor vnet pnic count (:doc:`set count <processor_set_count>`)
      input stage: hypervisors_pnic_vnet

      output stage: hypervisors_vnet_pnic_count (number set)

   Hypervisor without ToR Switch redundancy (:doc:`range <processor_range>`)
      input stage: hypervisors_leaf_count

      output stage: hypervisor_single_leaf_anomaly (discrete state set)

   Networks without link redundancy (:doc:`range <processor_range>`)
      input stage: hypervisors_vnet_pnic_count

      output stage: hypervisor_vnet_single_pnic_anomaly (discrete state set)

Example Usage
   **NSX-T Integration** - AOS raises an anomaly in cases without redundancy or a
   single point of failure (SPOF) in hypervisor connectivity. Examples include:

   * NSX-T transport nodes with a single non-LAG uplink towards ToR Leafs in the
     AOS fabric can result in a single point of failure (SPOF) for overlay traffic.
   * NSX-T transport nodes with a single LAG uplink with both members going to a
     single ToR leaf can result in a single point of failure (SPOF).
   * Lack of redundancy between AOS fabric LAG dual-leafs and ESXi hosts.

   .. image:: static/images/nsx/spof_anomaly.png
