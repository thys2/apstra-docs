==============
Processor: Max
==============
The Max processor groups as described by **Group by**,
then finds the maximum value and outputs it for each group.

**Input Types** - Number-Set (NS), NSTS

**Output Types** - Number-Set (NS)

**Properties**

   .. include:: includes/processors/group_by.rst

   .. include:: includes/processors/enable_streaming.rst

Max Example
-----------
Assume a configuration of:

.. code-block:: none

    group_by: ["system_id"]

Sample Input:

.. code-block:: none

    [system_id=leaf0,if_name=swp40] : 10
    [system_id=leaf0,if_name=swp41] : 11
    [system_id=leaf0,if_name=swp42] : 15
    [system_id=spine0,if_name=eth15] : 32
    [system_id=spine0,if_name=eth16] : 30
    [system_id=spine0,if_name=eth17] : 36

Output "out":

.. code-block:: none

    [system_id=leaf0] : 15
    [system_id=spine0] : 36
