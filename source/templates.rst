=========
Templates
=========

Template Overview
=================
Templates combine powerful logic to build networks using policy intent and
AOS components such as rack types and logical devices. AOS includes two types of
templates: rack-based and pod-based.

Rack-based templates import one or more rack types that define how servers connect
to top-of-rack switches (or a pair of ToR switches); logical devices define
the spines.

Pod-based templates are used to create large, 5-stage Clos networks, essentially
combining multiple rack-based templates using an additional layer of
superspines. The following images show examples of 5-stage Clos architectures
built using pod-based templates (Superspine links are not shown for readability
purposes). See :doc:`5stage` for more information.

.. figure:: static/images/5stage/topology1.png

    Single plane, dual superspine

.. figure:: static/images/5stage/topology2.png

    4 x plane, 4 x superspine

After a template is created, it can be used to create as many different blueprints
as needed to build specific networks.

.. image:: static/images/templates/templates_330.png

**To go to templates** - from the AOS web interface, navigate to
**Design / Templates**. AOS ships with numerous predefined templates. To see more
information, click a template name. Templates include the following details:

Common Parameters
   Name - 64 characters or fewer

   Type - RACK BASED or POD BASED

Policies
   ASN Allocation Scheme (spine) (rack-based only)
      Unique - applies to 3-stage designs. A different ASN is assigned to each
      spine.

      Single - applies to 5-stage designs. One ASN is assigned to all spines
      within a pod, and another ASN is assigned to all superspines.

   Routing Policy (import) (rack-based only)
      Default Only - accepts 0.0.0.0/0 BGP route

      ALL - accepts all routes - It sends an **Internet full table** (700k routes),
      which may cause a crash or other undesirable behavior for the AOS fabric
      network devices that are attached to external routers. Verify that your
      network devices can accept the appropriate number of routes.

   Overlay Control Protocol - defines the inter-rack virtual network overlay
   protocol used in the AOS fabric. This cannot be changed after a blueprint is
   deployed.

      Static VXLAN - uses static VXLAN routing the Head End Replication (HER)
      flooding to distribute Layer 2 virtual network traffic between racks.

      MP-EBGP EVPN - uses EVPN family eBGP sessions between device loopbacks to
      exchange EVPN routes for hosts (Type 2) and networks (Type 5). Only
      homogeneous, single-vendor EVPN fabrics are supported. VXLAN/EVPN
      capabilities for inter-rack virtual networks are dependent on the make and
      model of the network devices being used. See
      :doc:`Virtual Networks <virtual_networks>` for more information.

   Spine to Leaf links Type (or for 5-stage, Spine to Superspine Links)
      IPv4 - uses addresses from IPv4 resource pools.

      IPv6 - uses addresses from IPv6 resource pools. IPv6 is not supported when
      MP-EBGP EVPN overlay control protocol is specified.

      IPv4-IPv6 - dual-stack

Structure
   For Rack-based Templates:
      Rack Types - type of rack and number of each selected rack type. Specifying
      an ESI-based rack type in a rack-based templates without EVPN is invalid.

      Spine Logical Device and Count - type and number of spine logical devices

      External Links Count and Speed - number of spine links and speed to
      any external routers

      Superspine Link Count and Speed - number and speed of links to any
      superspines

   For Pod-based Templates:
      Pods - type of rack-based template and number of each selected template

      Superspine Logical Device - type of logical device

      Plane - number of planes and number of superspines per plane

      External Links Count and Speed - number of links and speed to any external
      routers

.. sidebar:: Cloning Template

   Instead of entering all details for a new template, you can clone an existing
   one, give it a new name and customize it.

.. _create_rack-based_template:

Creating Rack Based Template
============================
You can build a multi-rack environment by selecting multiple rack types,
but you cannot mix Layer 2 and Layer 3 racks in the same template.

#. If your design requires :doc:`rack types <rack_types>` and/or
   :doc:`logical devices <logical_devices>` that are not in the global catalog,
   create them before proceeding.
#. From the list view (Design / Templates), click **Create Template**, enter a
   name (64 characters or fewer) and select **RACK BASED**.
#. Select applicable policies.
#. Select a :doc:`rack type <rack_types>` from the drop-down list and select how
   many of that type to include in the template. Notice that as you enter
   information, the topology preview on the right changes accordingly.

   * To add another rack, click **Add racks**.

#. Select the **Spine Logical Device** from the drop-down list, then select the
   number of them to include in the template. Make sure to select one that
   provides a sufficient number of spine ports for your design. For 5-stage
   designs, make sure to select a logical device that includes the **Superspine**
   role.
#. For **Spine External Connectivity**, enter the number and connection speed of
   any external links.
#. For 5-stage designs, enter the number and connection speed of links for
   **Superspine Connectivity**.
#. Click **Create** to create the template. It can now be used to
   :doc:`create a blueprint <bp_create>`.

.. _create_pod-based_template:

Creating Pod Based Template
===========================
A pod-based template consists of multiple rack-based templates; it is essentially
a "template of templates" used to build :doc:`5-stage Clos networks <5stage>`.

#. If your design requires :doc:`templates <templates>`,
   :doc:`rack types <rack_types>` and/or :doc:`logical devices <logical_devices>`
   that are not in the global catalog, create them before proceeding.
#. From the list view (Design / Templates), click **Create Template**, enter a
   name (64 characters or fewer) and select **POD BASED**.
#. Select applicable policies.
#. Select a pod from the drop-down list and select the number of that type of pod.
   Notice that as you enter information, the topology preview on the right changes
   accordingly.

   * To add another type of pod, click **Add pods** and select another pod from
     the drop-down list.

#. Select a **Superspine Logical Device** from the drop-down list.
#. Select the number of planes and the number of superspines per plane.
#. Enter the number and connection speed of any external links.
#. Click **Create** to create the template. It can now be used to
   :doc:`create a blueprint <bp_create>`.

The example below shows a pod-based template with three pods and two planes, each
containing two superspines:

.. image:: static/images/5stage/superspine_template.png

Editing Template
================
Changes to templates in the global catalog do not affect templates that have
already been used to create blueprints, thereby preventing potentially unintended
changes to those blueprints.

#. Either from the list view (Design / Templates) or the details view, click
   the **Edit** button (top-right) for the template to update.
#. Make your changes.

  * If you've modified a rack type that's used in a rack type template and you
    want it be updated in the template, you need to delete the rack type in the
    template (click **X** to the right of the template), then immediately select
    the same rack type from the drop-down list before saving the changes.

#. Click **Update** (bottom-right) to update the template.

.. _update_rack_type_template:

Updating Rack Type in Rack Type Template
----------------------------------------
Changes to rack types in the global catalog do not affect templates that have
already imported them, thereby preventing potentially unintended changes to those
templates. If your intent *is* for the template to use the modified rack type,
then you must re-import the rack type into the template.

#. Either from the list view (Design / Templates) or the details view, click
   the **Edit** button (top-right) for the template to update.
#. Click the **X** to the right of the rack type to remove it.
   Don't save the template yet.
#. Select the same rack type from the drop-down list.
#. Click **Update** (bottom-right) to update the template with the modified rack
   type.

Deleting Template
=================
Do not delete a template if it is referenced by a blueprint.

#. Either from the list view (Design / Templates) or the details view, click the
   **Delete** button for the template to delete.
#. Click **Delete** to delete the template.
