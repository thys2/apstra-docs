===============
Getting Started
===============

Up and Running with Apstra AOS
==========================================
Welcome! To get started, you'll install and configure the AOS server software.
Then for security purposes, we recommend that you replace the SSL certificate and
change default passwords. Follow the links below to get up and running with AOS.

#. Ensure that the server you're going to use for the AOS server meets
   :doc:`requirements <server_requirements>`.
#. Follow the steps for :doc:`installing the AOS server <server_installation>`.
#. Go to the :doc:`AOS web interface (UI) <aos_ui>` to replace the SSL certificate
   and change default passwords. Then you'll be ready to design, build, deploy,
   operate and validate networks in AOS.

.. _design_build_guide:

Design / Build Physical Network
===============================
Depending on the complexity of your design, other tasks may be required in
addition to the ones included in this general workflow. You can work with devices,
design, resources and external systems in any order before proceeding to the
blueprint section.

Devices
-------

#. AOS uses :doc:`device profiles <device_profile>` to represent devices. AOS
   ships with many predefined device profiles. Check the list, and if one that
   you need is not included, you can create it.

   ::

     Devices / Device Profiles

#. Create and install :doc:`agents <device_agents>` for the devices in your
   network. If you have many of the same devices using the same configuration you
   might consider creating :doc:`agent profiles <agent_profiles>`, which can
   streamline the task of creating agents.


   ::

     Devices / System Agents / Agents

#. When agents are created, they appear in the managed devices list, but they are
   not yet managed by AOS. (If you have a :ref:`modular device <modular_device>`
   in your network, you may need to change the associated device profile. It's
   best to do this before acknowledging.)
   :doc:`Acknowledge the devices <managed_devices>` so AOS can manage them.

   ::

     Devices / Managed Devices

Design
------
#. AOS uses :doc:`logical devices <logical_devices>` to create abstractions of
   physical devices. Check existing logical devices for ones that meet your
   requirements and create them as needed for your design.

   ::

      Design / Logical devices

#. To create a link between physical devices (device profiles) and logical
   devices, :doc:`interface maps <interface_maps>` are used. Check that existing
   interface maps meet your needs, and if not, you can create them.

   ::

     Design / Interface maps

#. AOS uses :doc:`rack types <rack_types>` as logical representations of racks. If
   a rack type that you need for your design is not included with the predefined
   ones, you can create one.

   ::

      Design / Rack Types

#. :doc:`Templates <templates>` are used to build typical rack designs. Check the
   predefined templates and if one doesn't meet the needs of your design,
   you can create one.

   ::

      Design / Templates

Resources
---------
Create pools for resources (:doc:`ASNs <asn_pools>`,
:doc:`IPv4 addresses <ip_pools>`, :doc:`IPv6 addresses <ipv6_pools>`) that
will be used in the network.

::

   Resources

External Systems
----------------
Create :doc:`external routers <external_routers>` to represent switches that the
network uses for traffic exiting the fabric.

::

  External Systems / External Routers

Blueprints
----------
#. Create a :doc:`blueprint <bp_create>` based on a predefined template
   or one that you created in the design section.

   ::

      Blueprints

#. :doc:`Build <staged_physical>` the network by assigning resources, device
   profiles, device system IDs, external routers, and, as applicable, configlets.

   ::

       Blueprints / <your_blueprint_name> / Staged / Physical / Build

#. Review the AOS-calculated :doc:`cabling map <links>` and cable up the physical
   devices according to the map. If you have a set of pre-cabled switches, ensure
   that you have configured interface maps according to the actual cabling so that
   AOS-calculated cabling matches actual cabling. You can also use
   :doc:`AOS CLI <aos_cli>` to discover existing cabling and override the
   blueprint with that cabling.

   ::

       Blueprints / <your_blueprint_name> / Staged / Physical / Links

#. When all assignments have been made and the blueprint is error-free,
   :doc:`commit <uncommitted>` the blueprint. Committing a blueprint signals AOS
   to work on the intent and realize it on the network by pushing configuration
   changes on assigned devices.

   ::

       Blueprints / <your_blueprint_name> / Uncommitted

#. Review the blueprint dashboard for anomalies. If you have cabling anomalies,
   the likely reason is a mismatch in AOS-calculated cabling and actual cabling.
   Either re-cable the switches, recreate the blueprint with appropriate interface
   maps or use AOS CLI to override the cabling in the blueprint with discovered
   cabling.

   ::

       Blueprints / Dashboard

Next Steps
----------
After your AOS deployment is running, you can proceed to
:doc:`build the virtual environment <staged_virtual>` with virtual networks and
security zones, as needed. For more information about working with AOS, such as
using :doc:`intent-based analytics <iba_tutorial>`, see our :doc:`guides <guides>`.
