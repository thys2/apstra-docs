###################
Nutanix Integration
###################

============================
Nutanix Integration Overview
============================
The integration provides for a cloudlike experience for Nutanix users thereby
providing simplicity and end-to-end automation on top of multi-vendor and
intent-based physical underlay managed by Apstra AOS. It also lets network
operators see Nutanix VMs, VM ports and physical ports.

AOS Nutanix integration helps identify if an issue exists on the fabric or on the
virtual infrastructure. It eliminates manual config validation tasks between the
Nutanix servers and the ToR switches.

Nutanix integration consists of two main components:

 - AOS virtual infra integration, which provides visibility into Nutanix cluster
   from the AOS web interface;
 - Nutanix proxy service -- independent service that runs alongside AOS that allows
   automated provisioning of underlay fabric VLANs, based on user actions performed
   in Nutanix Prism.

------------------
Supported Versions
------------------
Nutanix integration is currently available as limited availability release, and
supports the following versions of Nutanix:

 - 2019.02.11 LTS
 - Prism: 5.11
 - Nutanix Cluster Check (NCC): 3.6.4
 - Nutanix Life Cycle Manager (LCM): 2.1.4139

=========================
Virtual Infra Integration
=========================

------------------------------
Creating Virtual Infra Manager
------------------------------

.. note::

    We recommend creating a dedicated Nutanix manager
    user with the **Auditor** role.

#. From the AOS web interface, navigate to
   **External Systems / Virtual Infra Managers**.
#. Click **Create Virtual Infra Managers**, enter the **Nutanix Manager** IP
   address or DNS name and credentials, then click **Create**. While AOS is
   connecting to the **Nutanix Manager** the state is **DISCONNECTED**. After
   successful connection, the state changes to **CONNECTED**..

   .. image:: static/images/nutanix/Nutanix-CreateVirtualInfraDialog.png

   .. note::

        Once connected, you won't be able to disable the service by default.
        To disconnect from **Nutanix Manager** navigate to
        **External Systems / Virtual infra Managers** and select the **delete**
        option under **Actions**.

----------------------------------------------
Adding Nutanix Manager to the Active Blueprint
----------------------------------------------
#. From the blueprint, navigate to **Staged / Virtual / Virtual Infra**.
#. Select the **Nutanix Manager** to be added, then click **Add Virtual Infra**.
   The remediation policy is used when **Remediate anomalies** is clicked in the
   probes.

   .. image:: static/images/nutanix/Nutanix-AddVirtualInfraManager.png

#. Enter the **Virtual Infra Manager**.
#. Specify the **VN type**. If the VXLAN virtual network extends to different ToRs
   in a fabric, select **VXLAN** (inter-rack). Otherwise, you can leave the default
   VN type of **VLAN** (rack-local).
#. Specify the **Security Zone**. If the VN type is VLAN, then the security policy
   can be set to default. Otherwise, you must specify a security zone under which
   the VXLAN virtual networks exist.
#. Click **Create** to stage the Virtual Infra Manager and return to the list view.

   .. image:: static/images/nutanix/Nutanix-NewVirtualInfraAdded.png

---------------------------------------------------
Viewing Virtual Machines running on Nutanix Servers
---------------------------------------------------
From the blueprint, navigate to the **Active / Query / VMs** to see VM details.

   .. image:: static/images/nutanix/Nutanix-ShowVMs.png

   VM Name
       The virtual machine name that is hosted on Nutanix managed server.

   Hosted On
       The Nutanix host on which the virtual machine is hosted.

   Hypervisor Hostname
       The hypervisor hostname on which virtual machine is hosted and
       is connected to the leaf ToRs in a fabric.

   Hypervisor Version
       The OS software version running on the hypervisor.

   VM IP
       The IP address as reported by Nutanix. If the IP address is not
       available this field is empty.

   Leaf:Interface
       System ID for the interface on the leaf to which Nutanix host is
       connected to and on which VM resides.

   Port Group Name:VLAN ID
       Unused.

   MAC Addresses
       MAC address of the VM connected to the AOS fabric.

   Virtual Infra address
      IP address of the Nutanix server added to a blueprint.


------------------------------------
Validating Virtual Infra Integration
------------------------------------

.. include:: includes/virtual_infra_validation.rst

-----------------------------------
Disabling Virtual Infra Integration
-----------------------------------

.. include:: includes/integration_disable.rst

=========================
Nutanix Proxy Integration
=========================
After enabling and configuring the Apstra AOS Nutanix Prism proxy service, it
subscribes to virtual network and virtual machine lifecycle events
(a.k.a webhooks) published by Nutanix Prism. Upon receiving the notifications
from registered events, Apstra AOS adds or removes the VLANs on appropriate
ports on the physical network switches.

Apstra AOS and this integration support EVPN VXLAN offering any VM, any host, any
rack flexibility.

.. image:: static/images/nutanix/Nutanix-ProxySolutionDiagram.png
    :align: center

The list of events handled by proxy service:

 - subnet create;
 - subnet delete;
 - subnet update;
 - VM power on;
 - VM delete;
 - VM migrate.

----------------------------
Installing AOS Nutanix Proxy
----------------------------
#. Download Nutanix/AOS proxy service debian package using the link
   provided by Apstra Support.
#. Install AOS Nutanix proxy service by running::

    sudo dpkg -i aos-nutanix-proxy-<version>.deb

#. View the AOS Nutanix proxy version that is installed using::

    sudo dpkg -l aos-nutanix-proxy

-------------------------
Running AOS Nutanix Proxy
-------------------------
#. Provide the required configuration options in ``/etc/aos/nutanix/config.yaml``.

#. Start proxy::

    sudo systemctl start aos-nutanix-proxy

#. Check status::

    sudo systemctl status aos-nutanix-proxy

#. View logs::

    sudo journalctl -u aos-nutanix-proxy

#. Stop proxy::

    sudo systemctl stop aos-nutanix-proxy

--------------------------------------------
Assigning SVI Subnet for Nutanix Integration
--------------------------------------------
Nutanix integration **requires** that an SVI subnet be allocated to
virtual networks *before* proxy service is started.

#. Navigate to **Staged / Virtual / Virtual Networks**, and click
   **Create Virtual Networks**.

   .. image:: static/images/nutanix/Nutanix-CreateVN.png

#. Create a DHCP-enabled VXLAN virtual network in the security zone that is
   planned for the Nutanix VLANs.

   .. image:: static/images/nutanix/Nutanix-CreateVNDialog.png

#. From the **Build** section of the virtual networks list view, assign the IPs
   from a pool to the SVI subnets. (Prior to AOS version 3.3.0, SVI subnets were
   assigned from the security zone build section.)
