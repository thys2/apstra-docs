=====
Tasks
=====
Blueprint task history is available on the **Tasks** tab of the **Staged**
Blueprint (as of version 3.2). Blueprint task details include type of
task, task status (succeeded, failed, in progress), date/time started,
date/time last updated, and the duration of the task. For any failed tasks, you
can click to see error messages.
