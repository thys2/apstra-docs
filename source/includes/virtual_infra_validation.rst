.. as of 11/18/20 the 3 integration docs have different heading levels
   so "Virtual Infra Validation" is on those docs instead of here.

After enabling virtual infrastructure integration, you can perform validation
with intent-based analytics for virtual infra. AOS ships with two predefined
analytics dashboards for virtual infra that instantiate the relevant predefined
virtual infra probes as listed below:

Virtual Infra Fabric Health Check Dashboard
   :doc:`Hypervisor MTU Mismatch Probe <probe_hypervisor_mtu_mismatch>`

   :doc:`Hypervisor MTU Threshold Check Probe
   <probe_hypervisor_mtu_threshold_check>`

   :doc:`Hypervisor and Fabric LAG Config Mismatch Probe
   <probe_hypervisor_and_fabric_lag_config_mismatch>`

   :doc:`Hypervisor and Fabric VLAN Config Mismatch Probe
   <probe_hypervisor_and_fabric_vlan_config_mismatch>`

   :doc:`Hypervisor Missing LLDP Config Probe
   <probe_hypervisor_missing_lldp_config>`

   :doc:`VMs without Fabric Configured VLANs Probe
   <probe_vms_without_fabric_configured_vlans>`

Virtual Infra Redundancy Check Dashboard
   :doc:`Hypervisor Redundancy Checks Probe <probe_hypervisor_redundancy_checks>`

See :doc:`Analytics Dashboard <dashboard_analytics>` for information about working
with dashboards and :ref:`Instantiating Predefined Probe <instantiate_probe>`
for information about predefined probes.
