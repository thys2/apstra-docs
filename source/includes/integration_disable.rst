.. for virtual infra integrations: vmware, nsx, nutanix

#. From the blueprint, navigate to **Staged / Virtual / Virtual Infra**,
   then click the **Delete** button for the virtual infra to delete.
#. Navigate to **Uncommitted** and commit the deletion.
#. From the **External Systems** menu, navigate to **Virtual Infra Managers**,
   then click the **Delete** button for the virtual infra manager to delete.
