.. list-table:: AOS Hardware compatibility list
    :header-rows: 1
    :class: nowrap

    * - Name
      - Manufacturer
      - Model
      - OS Family
      - Version

    * - Accton 5712-54X-O
      - Accton
      - 5712-54X-O.*
      - Cumulus
      - (3\.[4-6]\.\d+)

    * - Accton 6712-32X-O
      - Accton
      - 6712-32X-O.*
      - Cumulus
      - (3\.[4-6]\.\d+)

    * - Arista 7060CX-32S
      - Arista
      - DCS-7060CX-32S
      - EOS
      - 4\.(18|20)\..*

    * - Arista 7060CX2-32S
      - Arista
      - DCS-7060CX2-32S
      - EOS
      - 4\.(18|20)\..*

    * - Arista DCS-7050QX-32
      - Arista
      - DCS-7050QX-32
      - EOS
      - 4\.(16|18|20)\..*

    * - Arista DCS-7050QX-32S
      - Arista
      - DCS-7050QX-32S
      - EOS
      - 4\.(16|18|20)\..*

    * - Arista DCS-7050SX-64
      - Arista
      - DCS-7050SX-64
      - EOS
      - 4\.(16|18|20)\..*

    * - Arista DCS-7050TX-48
      - Arista
      - DCS-7050TX-48
      - EOS
      - 4\.(16|18|20)\..*

    * - Arista DCS-7050TX-64
      - Arista
      - DCS-7050TX-64
      - EOS
      - 4\.(16|18|20)\..*

    * - Arista DCS-7150S-24
      - Arista
      - DCS-7150S-24
      - EOS
      - 4\.(16|18|20)\..*

    * - Arista DCS-7150S-52
      - Arista
      - DCS-7150S-52
      - EOS
      - 4\.(16|18|20)\..*

    * - Arista DCS-7150S-64
      - Arista
      - DCS-7150S-64
      - EOS
      - 4\.(16|18|20)\..*

    * - Arista DCS-7160YC-72
      - Arista
      - DCS-7160-48YC6
      - EOS
      - 4\.(18|20)\..*

    * - Arista DCS-7250QX-64
      - Arista
      - DCS-7250QX-64
      - EOS
      - 4\.(16|18|20)\..*

    * - Arista DCS-7280SE-64
      - Arista
      - DCS-7280SE-64
      - EOS
      - 4\.(16|18|20)\..*

    * - Arista DCS-7280SE-68
      - Arista
      - DCS-7280SE-68
      - EOS
      - 4\.(16|18|20)\..*

    * - Arista DCS-7280SE-72
      - Arista
      - DCS-7280SE-72
      - EOS
      - 4\.(16|18|20)\..*

    * - Arista DCS-7280TR-48C6
      - Arista
      - DCS-7280TR-48C6
      - EOS
      - 4\.(18|20)\..*

    * - Arista DCS-7504N 4x7500R-36Q
      - Arista
      - DCS-7504N
      - EOS
      - 4\.(18|20)\..*

    * - Arista DCS-7508N 8x7500R-36CQ
      - Arista
      - DCS-7508N
      - EOS
      - 4\.18\..*

    * - Arista DCS-7508N 8x7500R-36Q
      - Arista
      - DCS-7508N
      - EOS
      - 4\.(18|20)\..*

    * - Arista vEOS
      - Arista
      - vEOS
      - EOS
      - 4\.(16|18|20)\..*

    * - Celestica RedstoneXP
      - Celestica
      - RedstoneXP
      - Cumulus
      - (3\.[4-6]\.\d+)

    * - Cisco 3164PQ
      - Cisco
      - C3164PQ
      - NXOS
      - 7\.0\(3\)I[2-7]\(\w[a-z]?\)

    * - Cisco 3172PQ
      - Cisco
      - 3172
      - NXOS
      - 7\.0\(3\)I[2-7]\(\w[a-z]?\)

    * - Cisco 9236C
      - Cisco
      - C9236C
      - NXOS
      - 7\.0\(3\)I[4-7]\(\w[a-z]?\)

    * - Cisco 93180YC-EX
      - Cisco
      - 93180YC-EX
      - NXOS
      - 7\.0\(3\)I[4-7]\(\w[a-z]?\)

    * - Cisco 9332PQ
      - Cisco
      - C9332PQ
      - NXOS
      - 7\.0\(3\)I[2-7]\(\w[a-z]?\)

    * - Cisco 9372PX
      - Cisco
      - C9372PX
      - NXOS
      - 7\.0\(3\)I[2-7]\(\w[a-z]?\)

    * - Cisco 9372TX
      - Cisco
      - C9372TX
      - NXOS
      - 7\.0\(3\)I[2-7]\(\w[a-z]?\)

    * - Cisco 9396PX
      - Cisco
      - C9396PX
      - NXOS
      - 7\.0\(3\)I[2-7]\(\w[a-z]?\)

    * - Cisco NXOSv
      - Cisco
      - NX-OSv
      - NXOS
      - 7\.0\(3\)I[2-7]\(\w[a-z]?\)

    * - Cumulus VX
      - Cumulus
      - VX
      - Cumulus
      - (3\.[4-6]\.\d+)

    * - Dell S3048-ON
      - Dell
      - S3048ON
      - Cumulus
      - (3\.[4-6]\.\d+)

    * - Dell S4048-ON
      - Dell
      - S4048ON
      - Cumulus
      - (3\.[4-6]\.\d+)

    * - Dell S4048-ON OPX
      - Dell
      - S4048ON
      - OPX
      - (2\.2\.\d+)

    * - Dell S6000-ON
      - Dell
      - S6000-ON
      - Cumulus
      - (3\.[4-6]\.\d+)

    * - Dell S6000-ON OPX
      - (Dell|DELL)
      - S6000-ON
      - OPX
      - (2\.2\.\d+)

    * - Edgecore AS7712-32X
      - Edgecore
      - 7712-32X-O.*
      - Cumulus
      - (3\.[4-6]\.\d+)

    * - Generic Server 1RU 1x10G
      - Generic Manufacturer
      - Generic Model
      - Ubuntu GNU/Linux
      - (14\.04) LTS

    * - Generic Server 1RU 1x10G Centos
      - Generic Manufacturer
      - Generic Model
      - CentOS
      - 6\.8

    * - Generic Server 1RU 1x10G Xenial
      - Generic Manufacturer
      - Generic Model
      - Ubuntu GNU/Linux
      - (16\.04) LTS

    * - Generic Server 1RU 1x1G
      - Generic Manufacturer
      - Server 1x1G
      - Ubuntu GNU/Linux
      - (14\.04) LTS

    * - Generic Server 1RU 1x1G Centos
      - Generic Manufacturer
      - Server 1x1G
      - CentOS
      - 6\.8

    * - Generic Server 1RU 1x1G Xenial
      - Generic Manufacturer
      - Server 1x1G
      - Ubuntu GNU/Linux
      - (16\.04) LTS

    * - Generic Server 1RU 1x25G
      - Generic Manufacturer
      - Server 1x25G
      - Ubuntu GNU/Linux
      - (14\.04) LTS

    * - Generic Server 1RU 1x25G Centos
      - Generic Manufacturer
      - Server 1x25G
      - CentOS
      - 6\.8

    * - Generic Server 1RU 1x25G Xenial
      - Generic Manufacturer
      - Server 1x25G
      - Ubuntu GNU/Linux
      - (16\.04) LTS

    * - Generic Server 1RU 1x40G
      - Generic Manufacturer
      - Server 1x40G
      - Ubuntu GNU/Linux
      - (14\.04) LTS

    * - Generic Server 1RU 1x40G Centos
      - Generic Manufacturer
      - Server 1x40G
      - CentOS
      - 6\.8

    * - Generic Server 1RU 1x40G Xenial
      - Generic Manufacturer
      - Server 1x40G
      - Ubuntu GNU/Linux
      - (16\.04) LTS

    * - Generic Server 1RU 2x10G
      - Generic Manufacturer
      - Server 2x10G
      - Ubuntu GNU/Linux
      - (14\.04) LTS

    * - Generic Server 1RU 2x10G Centos
      - Generic Manufacturer
      - Server 2x10G
      - CentOS
      - 6\.8

    * - Generic Server 1RU 2x10G Xenial
      - Generic Manufacturer
      - Server 2x10G
      - Ubuntu GNU/Linux
      - (16\.04) LTS

    * - Generic Server 1RU 2x1G
      - Generic Manufacturer
      - Server 2x1G
      - Ubuntu GNU/Linux
      - (14\.04) LTS

    * - Generic Server 1RU 2x1G Centos
      - Generic Manufacturer
      - Server 2x1G
      - CentOS
      - 6\.8

    * - Generic Server 1RU 2x1G Xenial
      - Generic Manufacturer
      - Server 2x1G
      - Ubuntu GNU/Linux
      - (16\.04) LTS

    * - Generic Server 1RU 4x10G
      - Generic Manufacturer
      - Server 4x10G
      - Ubuntu GNU/Linux
      - (14\.04) LTS

    * - Generic Server 1RU 4x10G Centos
      - Generic Manufacturer
      - Server 4x10G
      - CentOS
      - 6\.8

    * - Generic Server 1RU 4x10G Xenial
      - Generic Manufacturer
      - Server 4x10G
      - Ubuntu GNU/Linux
      - (16\.04) LTS

    * - Generic Server 1RU 4x1G
      - Generic Manufacturer
      - Server 4x1G
      - Ubuntu GNU/Linux
      - (14\.04) LTS

    * - Generic Server 1RU 4x1G Centos
      - Generic Manufacturer
      - Server 4x1G
      - CentOS
      - 6\.8

    * - Generic Server 1RU 4x1G Xenial
      - Generic Manufacturer
      - Server 4x1G
      - Ubuntu GNU/Linux
      - (16\.04) LTS

    * - Juniper QFX5100-24Q-AA
      - Juniper
      - QFX5100[e]?-24Q-.*
      - Junos
      - 1[4-5].*

    * - Mellanox MSN2100
      - Mellanox
      - MSN2100
      - Cumulus
      - (3\.[4-6]\.\d+)

    * - OPX Virtual
      - Dell
      - S6000-VM
      - OPX
      - (2\.2\.\d+)

    * - Qci LY6
      - Qci
      - LY6
      - Cumulus
      - (3\.[4-6]\.\d+)

    * - Qci LY8
      - Qci
      - LY8
      - Cumulus
      - (3\.[4-6]\.\d+)

    * - Qci LY9
      - Qci
      - LY9
      - Cumulus
      - (3\.[4-6]\.\d+)

    * - SnapRoute Wedge100
      - SnapRoute
      - Wedge100
      - FlexSwitch
      - 1\.0\.0\.[0-9]+

    * - SnapRoute vFlexSwitch
      - SnapRoute
      - Ubuntu
      - FlexSwitch
      - 1\.0\.0\.[0-9]+

