.. list-table:: AOS Hardware compatibility list for Ubuntu GNU/Linux
    :header-rows: 1
    :class: nowrap

    * - Name
      - Manufacturer
      - Model
      - OS Family
      - Version

    * - Generic Server 1RU 1x10G
      - Generic Manufacturer
      - Generic Model
      - Ubuntu GNU/Linux
      - (14\.04) LTS

    * - Generic Server 1RU 1x10G Xenial
      - Generic Manufacturer
      - Generic Model
      - Ubuntu GNU/Linux
      - (16\.04) LTS

    * - Generic Server 1RU 1x1G
      - Generic Manufacturer
      - Server 1x1G
      - Ubuntu GNU/Linux
      - (14\.04) LTS

    * - Generic Server 1RU 1x1G Xenial
      - Generic Manufacturer
      - Server 1x1G
      - Ubuntu GNU/Linux
      - (16\.04) LTS

    * - Generic Server 1RU 1x25G
      - Generic Manufacturer
      - Server 1x25G
      - Ubuntu GNU/Linux
      - (14\.04) LTS

    * - Generic Server 1RU 1x25G Xenial
      - Generic Manufacturer
      - Server 1x25G
      - Ubuntu GNU/Linux
      - (16\.04) LTS

    * - Generic Server 1RU 1x40G
      - Generic Manufacturer
      - Server 1x40G
      - Ubuntu GNU/Linux
      - (14\.04) LTS

    * - Generic Server 1RU 1x40G Xenial
      - Generic Manufacturer
      - Server 1x40G
      - Ubuntu GNU/Linux
      - (16\.04) LTS

    * - Generic Server 1RU 2x10G
      - Generic Manufacturer
      - Server 2x10G
      - Ubuntu GNU/Linux
      - (14\.04) LTS

    * - Generic Server 1RU 2x10G Xenial
      - Generic Manufacturer
      - Server 2x10G
      - Ubuntu GNU/Linux
      - (16\.04) LTS

    * - Generic Server 1RU 2x1G
      - Generic Manufacturer
      - Server 2x1G
      - Ubuntu GNU/Linux
      - (14\.04) LTS

    * - Generic Server 1RU 2x1G Xenial
      - Generic Manufacturer
      - Server 2x1G
      - Ubuntu GNU/Linux
      - (16\.04) LTS

    * - Generic Server 1RU 4x10G
      - Generic Manufacturer
      - Server 4x10G
      - Ubuntu GNU/Linux
      - (14\.04) LTS

    * - Generic Server 1RU 4x10G Xenial
      - Generic Manufacturer
      - Server 4x10G
      - Ubuntu GNU/Linux
      - (16\.04) LTS

    * - Generic Server 1RU 4x1G
      - Generic Manufacturer
      - Server 4x1G
      - Ubuntu GNU/Linux
      - (14\.04) LTS

    * - Generic Server 1RU 4x1G Xenial
      - Generic Manufacturer
      - Server 4x1G
      - Ubuntu GNU/Linux
      - (16\.04) LTS

