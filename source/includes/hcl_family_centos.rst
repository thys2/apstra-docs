.. list-table:: AOS Hardware compatibility list for CentOS
    :header-rows: 1
    :class: nowrap

    * - Name
      - Manufacturer
      - Model
      - OS Family
      - Version

    * - Generic Server 1RU 1x10G Centos
      - Generic Manufacturer
      - Generic Model
      - CentOS
      - 6\.8

    * - Generic Server 1RU 1x1G Centos
      - Generic Manufacturer
      - Server 1x1G
      - CentOS
      - 6\.8

    * - Generic Server 1RU 1x25G Centos
      - Generic Manufacturer
      - Server 1x25G
      - CentOS
      - 6\.8

    * - Generic Server 1RU 1x40G Centos
      - Generic Manufacturer
      - Server 1x40G
      - CentOS
      - 6\.8

    * - Generic Server 1RU 2x10G Centos
      - Generic Manufacturer
      - Server 2x10G
      - CentOS
      - 6\.8

    * - Generic Server 1RU 2x1G Centos
      - Generic Manufacturer
      - Server 2x1G
      - CentOS
      - 6\.8

    * - Generic Server 1RU 4x10G Centos
      - Generic Manufacturer
      - Server 4x10G
      - CentOS
      - 6\.8

    * - Generic Server 1RU 4x1G Centos
      - Generic Manufacturer
      - Server 4x1G
      - CentOS
      - 6\.8

