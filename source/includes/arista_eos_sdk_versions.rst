
.. list-table::
    :class: nowrap
    :header-rows: 1

    * - EOS Version
      - Minimum SDK Version
      - Example EOS SDK RPM filename

    * - 4.16.6M
      - 1.10.0.1
      - EosSdk-1.10.0.1-4.16.6M.i686.rpm

    * - 4.17.7M
      - No support
      - Arista has stated they will not provide a fix for this issue in 4.17
        train.  Do not deploy MLAG on 4.17.

    * - 4.18.1.1F+
      - 1.12.3
      - Install EosSdk-1.12.3-4.18.1.1F.i686.rpm, or the version that maps to your
        version of EOS available from Arista.com

    * - 4.18.3F+
      - 1.12.3 is built-in to EOS
      - EOS SDK 1.12.3 is now integrated into the base EOS image, so no installation
        is required.
