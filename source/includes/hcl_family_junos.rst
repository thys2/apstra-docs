.. list-table:: AOS Hardware compatibility list for Junos
    :header-rows: 1
    :class: nowrap

    * - Name
      - Manufacturer
      - Model
      - OS Family
      - Version

    * - Juniper QFX5100-24Q-AA
      - Juniper
      - QFX5100[e]?-24Q-.*
      - Junos
      - 1[4-5].*

