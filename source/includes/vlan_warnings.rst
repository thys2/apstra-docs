
.. warning::
    Be aware of internal VLAN allocation ranges and avoid overlapping with
    reserved ranges.  AOS does not prevent the user from configuring reserved
    vlan ranges, and this may result in a deployment failure if you configure
    a vlan ID that is reserved.

Reserved VLAN ranges
    Cisco
        Cisco reserves VLAN ranges (3968-4095) for internal features like multicast
        and diagnostics.

        The default vlan range can be modified b Cisco with the command ``system
        vlan XXXX reserve``, where X is the first vlan ID to be reserved in groups
        of 128.

        .. code-block:: text

            system vlan 3000 reserve

        https://www.cisco.com/c/en/us/td/docs/switches/datacenter/nexus9000/sw/6-x/layer2/configuration/guide/b_Cisco_Nexus_9000_Series_NX-OS_Layer_2_Switching_Configuration_Guide/b_Cisco_Nexus_9000_Series_NX-OS_Layer_2_Switching_Configuration_Guide_chapter_011.html#concept_C062B9A4721044F0B85C108A615CB2E8

    Arista
        Arista reserves VLAN ranges beginning at VLAN ID 1006 and up.  Each L3
        interface, multicast group, and other internal resources may cause additional
        internal VLAN IDs to be used.

        This policy can be changed:

        .. code-block:: text

            vlan internal alocation policy DIRECTION [RANGE_VLAN]

        https://www.arista.com/en/um-eos/eos-section-18-4-vlan-configuration-commands#ww1153489

    Cumulus
        Cuimulus reserves VLAN ranges starting from 3000-3999.  Each physical port,
        linux bridge, and L3 internal uses an internal VLAN ID.

        The VLAN range can be modified by editing ``/etc/cumulus/switchd.conf``
        and setting the setting ``resv_vlan_range```. After making the change,
        you will have to reset the switch with ``systemctl restart switchd.service``

        https://docs.cumulusnetworks.com/display/DOCS/VLAN-aware+Bridge+Mode+for+Large-scale+Layer+2+Environments

The VLAN Name must be alphanumeric, under 32 characters, and not use special
characters (This is rendered as a vlan-name on devices)
