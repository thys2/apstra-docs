.. important::

    Only in rare exceptions is it needed to follow the manual process of Agent
    installation. In almost all cases agents should be installed by creating System
    Agents in the UI. Installing Agents manually is more bespoke, more effort and
    prone to user error.

    In-depth understanding of the various device states, configuration stages and
    Agent operation is required when attempting manual Agent installation.

    When in doubt, contact :doc:`Apstra Global Support <support>`.
