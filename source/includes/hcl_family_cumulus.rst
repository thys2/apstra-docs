.. list-table:: AOS Hardware compatibility list for Cumulus
    :header-rows: 1
    :class: nowrap

    * - Name
      - Manufacturer
      - Model
      - OS Family
      - Version

    * - Accton 5712-54X-O
      - Accton
      - 5712-54X-O.*
      - Cumulus
      - (3\.[4-6]\.\d+)

    * - Accton 6712-32X-O
      - Accton
      - 6712-32X-O.*
      - Cumulus
      - (3\.[4-6]\.\d+)

    * - Celestica RedstoneXP
      - Celestica
      - RedstoneXP
      - Cumulus
      - (3\.[4-6]\.\d+)

    * - Cumulus VX
      - Cumulus
      - VX
      - Cumulus
      - (3\.[4-6]\.\d+)

    * - Dell S3048-ON
      - Dell
      - S3048ON
      - Cumulus
      - (3\.[4-6]\.\d+)

    * - Dell S4048-ON
      - Dell
      - S4048ON
      - Cumulus
      - (3\.[4-6]\.\d+)

    * - Dell S6000-ON
      - Dell
      - S6000-ON
      - Cumulus
      - (3\.[4-6]\.\d+)

    * - Edgecore AS7712-32X
      - Edgecore
      - 7712-32X-O.*
      - Cumulus
      - (3\.[4-6]\.\d+)

    * - Mellanox MSN2100
      - Mellanox
      - MSN2100
      - Cumulus
      - (3\.[4-6]\.\d+)

    * - Qci LY6
      - Qci
      - LY6
      - Cumulus
      - (3\.[4-6]\.\d+)

    * - Qci LY8
      - Qci
      - LY8
      - Cumulus
      - (3\.[4-6]\.\d+)

    * - Qci LY9
      - Qci
      - LY9
      - Cumulus
      - (3\.[4-6]\.\d+)

