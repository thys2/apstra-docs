.. used in iba processors (generic service data collector, )

System ID
  Expression mapping from graph query to a system_id, e.g.
  "system.system_id" if "system" is a name in the graph query.
