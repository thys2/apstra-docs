.. used in iba processors (generic service data collector)

Data Type
      Type of data the service collects: numbers (ns) (such as device
      temperature), discrete states (dss) (such as device status),
      text or tables
