   Non-collector processors containing the ``graph_query`` configuration
   parameter, can be parameterized to use data from arbitrary nodes in
   the graph, such as property set nodes (as of AOS version 3.0).
   Property sets allow you to parameterize macro level SLAs for individual
   business units. In the example below, ``graph_query`` matches a node of
   type ``property_set`` with label ``probe_propset``. It's accessed using
   the special ``query_result`` variable, where Index ``0`` means it's
   the first node in query results. If a query returned ``N`` nodes, they
   could be accessed using indices starting from ``0`` to ``N-1``. ``ps``
   is what the actual node is referred to in the query; the rest depends
   on the structure of the node. The ``int()`` casting is required because
   values of ``property_set`` nodes are strings. Here it's assumed that a
   property set node has the label ``probe_propset`` and that the value
   ``accumulate_duration`` was already created.

   .. code-block:: none

        graph_query: [node("property_set", label="probe_propset", name="ps")]
        duration: int(query_result[0]["ps"].values["accumulate_duration"])

   Another example is a that probes can validate a compliance requirement;
   the compliance value may change over time and/or it
   can be used by more than one probe. Also, a probe can validate NOS
   versions on devices. In this case, property sets can be used to define
   the current NOS version requirement. If it changes tomorrow: change
   the property set value, instead of going under the probe stage.
