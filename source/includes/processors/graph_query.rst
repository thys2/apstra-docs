.. used in iba processors (accumulate, detailed interface counters, extensible
   service data collector, generic graph collector, generic service data
   collector, interface counters, match string, periodic average, range, service
   data collector, state, time in state)

Graph Query (graph_query)
   One or more queries on graph specified as strings, or a list of
   such queries. (String will be deprecated in a future release.)
   Multiple queries should provide all the named nodes referenced
   by the expression fields (including additional_properties). Graph
   query is executed on the "operation" graph. Results of the queries
   can be accessed using the "query_result" variable with the appropriate
   index. For example, if querying property set nodes under name
   "ps", the result will be available as "query_result[0]["ps"]".

   In collector processors (``*_collector``, ``if_counter``) it is
   used to choose a set of nodes for further processing (for example,
   all leafs, or all interfaces between leaf and spines)

   In other processors it is used for general parameterization
   and it is only supported as a list of queries.

   .. code-block:: none
      :caption: Fabric Interfaces Example

         graph_query: "node("system", role="leaf", name="system").
                       out("hosted_interfaces").
                       node("interface", name="iface").out("link").
                       node("link", role="spine_leaf")"

   .. code-block:: none
      :caption: Leafs and Spines using two queries Example

         graph_query: ["node("system", role="leaf", name="system")",
                       "node("system", role="spine", name="system")"]
