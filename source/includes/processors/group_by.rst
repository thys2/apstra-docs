.. used in iba processors (average,
   match count, match percentage, max, min, set count, standard deviation, sum)

Group by (group_by)
   Accepts a list of property names to group input items into output
   items, produces only one output group for the empty list.Most
   processors take input and produce output. Many of them produce
   one output per input (for example, if input is a DSS, output is a
   DSS of same size). However, some processors reduce the size of
   the output relative to the size of the input. Effectively, they
   partition the input into groups, run some calculation on each of
   the groups that produce a single value per each group, and use
   that as output. Clearly, the size of the output set depends on
   the grouping scheme. We call such processors **grouping processors**
   and they all take the **Group by** configuration parameter.

   In the case of an empty list, the input is considered to be a
   single group; thus, the output is of size 1 and either N, DS,
   or TS. If a list of property names is specified, for example
   ["system_id", "iface_role"], or a single property is specified,
   for example ["system_id"], we divide the input into groups such
   that for each group, every item in the group has the same values
   for the given list of property names. See the
   :doc:`standard deviation processor <processor_standard_deviation>`
   example for how this works.

   The output type of a processor depends on a value of the group_by
   parameter; for an empty list, a processor produces a single value
   result, such as N, DS, or T, and for grouping by one or more
   properties it returns a set result, such as NS, DSS, or TS.
