.. used in iba processors (comparison, ratio, set comparison, subtract, union)

Significant Keys (significant_keys)
   List of keys to map items from the inputs for applying the specified
   operation. It is typically used by processors that take multiple inputs
   and perform operations on them. When inputs have the same sets of keys
   it does not need to be specified. When inputs have different sets of
   keys, it must be specified and it must allow only 1:1 items mapping
   from the given inputs, otherwise the probe will go into error state.
