.. used in iba processors (service service, data collector)

Value Map
   A mapping of discrete-state values to human readable strings.
   A dictionary with all possible Discrete-State-Set states mapped
   to human-readable representation; applicable for Discrete-State-Set
   data (that is, when data_type is 'dss') only.

   .. code-block:: json
       :caption: Sample value map for interface status

       {
           "0": "unknown",
           "1": "down",
           "2": "up",
           "3": "missing"
       }
