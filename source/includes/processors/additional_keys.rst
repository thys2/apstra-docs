.. used in iba processors (extensible service data collector, generic graph collector,
   interface counters, service data collector,

Additional Keys
  Each additional key/value pair is used to extend properties of output stages
  where value is considered as an expression executed in context of the graph
  query and its result is used as a property value with respective key.
  The value of this property is evaluated for each item to
  associate items with metrics provided by a corresponding collector service.
  The association is done by keys because each collector reports a set of
  metrics where each metric is identified by a key in a format that is specific
  for each collector.
