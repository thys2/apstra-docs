.. used in iba processors (generic service data collector, )

Service input (service_input)
   Data to pass to telemetry collectors, if any. Can be an expression.
