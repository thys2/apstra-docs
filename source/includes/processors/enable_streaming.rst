.. used in iba processors (accumulate, average, comparison,
   match count, match percentage, match string, max, min, range, ratio,
   service data collector, set comparison,
   set count, standard deviation, state, subract, sum, time in state, union)

Enable Streaming (enable_streaming)
   Makes samples of output stages streamed if enabled. An optional boolean that
   defaults to False. If set to True, all output stages of this processor are
   streamed in the generic protobuf schema.
