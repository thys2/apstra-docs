.. used in iba processors (match string, range, state, time in state)

Raise Anomaly (raise_anomaly)
   Outputs “true” and “false” values, “true” meaning an
   appropriate item is anomalous, and "false" meaning the item
   is not anomalous. When Raise Anomaly is set to True, an actual
   anomaly is generated in addition to a sample in the output.
