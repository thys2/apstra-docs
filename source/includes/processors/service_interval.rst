.. used in iba processors (generic service data collector, )

Service interval (service_interval)
   Telemetry collection interval in seconds. Can be an expression.
