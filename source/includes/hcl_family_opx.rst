.. list-table:: AOS Hardware compatibility list for OPX
    :header-rows: 1
    :class: nowrap

    * - Name
      - Manufacturer
      - Model
      - OS Family
      - Version

    * - Dell S4048-ON OPX
      - Dell
      - S4048ON
      - OPX
      - (2\.2\.\d+)

    * - Dell S6000-ON OPX
      - (Dell|DELL)
      - S6000-ON
      - OPX
      - (2\.2\.\d+)

    * - OPX Virtual
      - Dell
      - S6000-VM
      - OPX
      - (2\.2\.\d+)

