.. comment
   This is a commonly-sourced reference of tcp/ip network requirements to avoid
   repetition of the same information in many documents

.. list-table:: AOS Server Network Protocol Requirements
   :header-rows: 1
   :class: nowrap

   * - Source
     - Destination
     - Protocol
     - Description
   * - User workstation
     - AOS Server
     - tcp/22 (ssh)
     - CLI access to the server
   * - User workstation
     - AOS Server
     - tcp/80 (http)
     - Redirects to tcp/443 (https)
   * - User workstation
     - AOS Server
     - tcp/443 (https)
     - AOS Web UI and REST API
   * - Network Device for AOS device agents
     - AOS Server
     - tcp/80 (http)
     - Redirects to tcp/443 (https)
   * - Network Device or Off-box Agent
     - AOS Server
     - tcp/443 (https)
     - Device agent installation and upgrade, Rest API
   * - Network Device or Off-box Agent
     - AOS Server
     - tcp/29730-29739
     - AOS Agent binary protocol (Sysdb)
   * - ZTP Server
     - AOS Server
     - tcp/443 (https)
     - Rest API for Device System Agent Install
   * - AOS Server
     - Network Devices
     - tcp/22 (ssh)
     - Device agent installation and upgrade
   * - AOS Off-box Agent
     - Network Devices
     - tcp/443 (https)
       tcp/9443 (nxapi)
       tcp/830 (for Junos)
     - Management from AOS Off-box Agent
