Superspine
    Port is configured to face a superspine device. Applies to 5-stage
    Clos datacenter fabric only.
Spine
    Port is configured to face a spine device.
Leaf
    Port is configured to face a leaf device.
Access (Not supported in AOS 3.3.0)
    Port is configured to face an access device.
L2 Server
    Port is configured to face an L2 server and trunk towards it,
    with SVI for VLANs, LAG/MLAG for single and dual-attached servers.
L3 Server
    Port is configured to face an L3 server with routing on the host (BGP).
External Router
    Port is configured to face a router that is not managed by AOS.
Peer
    Port is configured as a peer link between two leaf devices.
Unused
    AOS does not attempt to render configuration on a port with the unused role
    (ex: a dead port)
