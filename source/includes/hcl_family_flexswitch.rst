.. list-table:: AOS Hardware compatibility list for FlexSwitch
    :header-rows: 1
    :class: nowrap

    * - Name
      - Manufacturer
      - Model
      - OS Family
      - Version

    * - SnapRoute Wedge100
      - SnapRoute
      - Wedge100
      - FlexSwitch
      - 1\.0\.0\.[0-9]+

    * - SnapRoute vFlexSwitch
      - SnapRoute
      - Ubuntu
      - FlexSwitch
      - 1\.0\.0\.[0-9]+

