===============
User Management
===============
Users with the **administrator** role can create, edit, delete and log out
locally-defined users, as well as change their passwords.

.. image:: static/images/user_management/users_330.png

User profiles include the following details and options:

* Username
* First Name (optional)
* Last Name (optional)
* Email (optional)
* Password
* Roles

.. sidebar:: Cloning User Profiles

    Instead of entering all details for a new user, you can clone an existing one,
    give it a new name and customize it.

Creating User Profile
=====================
#. From the AOS web interface, navigate to **Platform / User Management / Users**,
   then click **Create User**.
#. Enter a username and password, then select one or more roles. If custom roles
   have been created, they appear as options along with the predefined roles that
   ship with AOS. You can see the permissions specified for each of the roles at
   **Platform / User Management / Roles**.
#. Click **Create** to create the user profile and return to the list view.

Use Case 1: Only Create Virtual Networks (not Including Allocating Resources)
-----------------------------------------------------------------------------
To limit a user's role to only creating virtual networks and looking at blueprint
details, assign them the role as described in
:ref:`Role Management: Use Case 3 <vn_role>`.

Use Case 2: Create Virtual Networks and Allocate Resources
----------------------------------------------------------
To allow a user to create virtual networks and allocate resources to them, you
must assign them multiple roles. See
:ref:`role management use case 3A and 4 <vn_role3a>` for more information.

.. image:: static/images/user_management/user_vn_330.png

Editing User Profile
====================
#. Either from the list view (Platform / User Management / Users) or the details
   view, click the **Edit** button for the user profile.
#. Change roles and/or other details.
#. Click **Update** to update the user profile and return to the list view.

.. _change_user_password:

Changing User Password
======================
#. From the list view (Platform / User Management / Users) click a username, click
   the **Change Password** button (top-right), then enter the new password, twice.
#. Click **Change Password** to update the password.

Logging Out User
================
From the list view (Platform / User Management / Users) click the **Log Out**
button for the user.

.. sidebar:: AOS REST API and User Profiles

    In addition to using the AOS web interface to manage user profiles AOS REST
    API can also be used. Navigate to **Platform / Developers** for documentation
    and tools. See the **aaa** section for user-related APIs.

Deleting User Profile
=====================
User **admin** cannot be deleted.

#. Either from the list view (Platform / User Management / Users) or the details
   view, click the **Delete** button for the user profile.
#. Click **Delete** to delete the user profile and return to the list view.
