==============================
|uncommitted_icon| Uncommitted
==============================

.. |uncommitted_icon| image:: static/images/icons/uncommitted_icon.png

Uncommitted Overview
====================
When a blueprint has pending changes, the **Uncommitted** tab shows a yellow
status indicator, or if there are build errors, a red status indicator. From
**Uncommitted**, you can review **Logical Diff**, **Full Nodes Diff**,
**Build Errors**, and **Warnings**.

Full Nodes Diff shows all uncommitted changes in one place, organized by node
type, change type and raw data. You can sort and search the diffs, then preview
the changed element. Full node requires a fair amount of resources and time to
generate.

If any build errors exist, they must be resolved before you can commit changes.
When they have been resolved, the status indicator on the **Build Errors** tab
changes from red to green, and the **Commit** button turns from gray to black.

   .. image:: static/images/blueprints/blueprint_rackbased_build_incomplete.png
       :width: 200px

   becomes...

   .. image:: static/images/blueprints/blueprint_rackbased_build_complete.png
       :width: 200px

.. _commit:

Committing Staged Changes
=========================
Any build errors must be resolved before changes to a blueprint can be committed.

#. From the blueprint, navigate to **Uncommitted** and review changes as needed.
#. Click **Commit** to go to the dialog where you can add a description and commit
   changes.

   .. image:: static/images/blueprints/uncommitted/uncommitted_commit_330.png

#. We recommend that you enter a revision description to identify the
   changes. These descriptions are displayed in the **Revisions** section of
   :doc:`Time Voyager <bp_rollback>`. If you don't add a description now you
   can always add one later. If you need to roll back to a previous revision, this
   description helps to determine the appropriate revision. Currently, specific
   diffs between revisions are not displayed, so the description is the only
   change information available for that revision.
#. Click **Commit** to push the staged changes to the active blueprint and create
   a revision.
#. While the task is active, you can click **Active Tasks** at the bottom of the
   screen for information about task progress. (Additional task history is
   available in the blueprint at **Staged / Tasks**.)

When a blueprint has been committed and devices have been deployed, the network is
up and running. However, networks are not static and can require modifications as
they evolve. Due to AOS's approach of *the network as a single entity* this is
extremely easy; AOS generates all required device configurations and pushes them
to the devices when the change is committed. We call this
*Flexible Fabric Expansion* (FFE).

Reverting Staged Changes
========================
If you decide not to commit staged changes to a blueprint, you can discard them.

From the blueprint, navigate to **Uncommitted**, then click **Revert**. In some
cases, you might also need to :ref:`reset_resource_group_overrides`.

.. image:: static/images/blueprints/uncommitted/uncommitted_revert_330.png
