##################
AOS Feature Matrix
##################

.. list-table:: Fabric Connectivity
   :header-rows: 1

   * - AOS Feature
     - EOS
     - NX-OS
     - Cumulus
     - Junos
     - SONiC
   * - L3 Clos
     - Yes
     - Yes
     - Yes
     - Yes
     - Yes
   * - 5-stage Clos
     - Yes
     - Yes
     - Yes
     - Yes
     - Yes
   * - IPv6 Fabric RFC-5549 (non-EVPN)
     - Yes
     - Yes
     - Yes
     - Roadmap
     - Yes

.. list-table:: Device Management
   :header-rows: 1

   * - AOS Feature
     - EOS
     - NX-OS
     - Cumulus
     - Junos
     - SONiC
   * - On-box agent
     - Yes
     - Yes
     - Yes
     - Not possible
     - Yes
   * - Off-box agent
     - Yes
     - Contact Support
     - Contact Support
     - Yes
     - Roadmap
   * - Telemetry extensibility
     - Yes
     - Yes
     - Yes
     - Yes
     - Yes
   * - Apstra ZTP
     - Yes
     - Yes
     - Yes
     - Yes
     - Yes
   * - Device OS upgrade
     - Yes
     - Yes
     - Yes
     - Roadmap
     - Yes
   * - Traffic draining (maintenance mode)
     - Yes
     - Yes
     - Yes
     - Yes
     - Yes

.. list-table:: Access
   :header-rows: 1

   * - AOS Feature
     - EOS
     - NX-OS
     - Cumulus
     - Junos
     - SONiC
   * - Access layer of switches
     - Roadmap
     - Roadmap
     - Roadmap
     - Limited
     - Roadmap
   * - LAG
     - Yes
     - Yes
     - Yes
     - Yes
     - Yes
   * - MLAG/vPC
     - Yes
     - Yes
     - Yes
     - Roadmap
     - Yes
   * - EVPN ESI (with LACP)
     - Roadmap
     - Roadmap
     - Roadmap
     - Yes
     - Not possible
   * - 802.1x
     - Yes
     - Roadmap
     - Roadmap
     - Yes
     - Not possible
   * - VLANs
     - Yes
     - Yes
     - Yes
     - Yes
     - Yes
   * - Static VXLAN
     - Yes
     - Yes
     - Not possible
     - Roadmap
     - Not possible
   * - EVPN VXLAN (3-stage and 5-stage)
     - Yes
     - Yes
     - Yes
     - Yes
     - Yes
   * - BGP to AOS-managed server
     - Yes
     - Yes
     - Yes
     - Roadmap
     - Roadmap
   * - IPv4 DHCP relay
     - Yes
     - Yes
     - Yes
     - Yes
     - Yes
   * - IPv6 DHCP relay
     - Yes
     - Yes
     - Yes
     - Roadmap
     - Yes
   * - EVPN DCI
     - Yes
     - Yes
     - Yes
     - Yes
     - Yes
   * - IPv6 for applications (with EVPN and IPv4 fabric)
     - Yes
     - Yes
     - Yes
     - Roadmap
     - Yes
   * - Group-based Policy (ACL on ToRs)
     - Yes
     - Yes
     - Roadmap
     - Roadmap
     - Roadmap

.. list-table:: External Router Connectivity
   :header-rows: 1

   * - AOS Feature
     - EOS
     - NX-OS
     - Cumulus
     - Junos
     - SONiC
   * - BGP loopback peering (type:L2/L3)
     - Yes
     - Yes
     - Yes
     - Limited
     - Limited
   * - BGP interface peering (type:L2/L3)
     - Yes
     - Yes
     - Yes
     - Limited
     - Limited
   * - Address family: IPv4, IPv6 or Dual Stack
     - Yes
     - Yes
     - Yes
     - Limited
     - Yes
   * - OSPF peering
     - Yes
     - Yes
     - Yes
     - Roadmap
     - Roadmap

.. list-table:: Routing Policies
   :header-rows: 1

   * - AOS Feature
     - EOS
     - NX-OS
     - Cumulus
     - Junos
     - SONiC
   * - Import all routes or default route or extra routes only
     - Yes
     - Yes
     - Yes
     - Yes
     - Yes
   * - Export loopback, link and VN IP. Export extra routes
     - Yes
     - Yes
     - Yes
     - Yes
     - Yes
   * - Export aggregate prefixes
     - Yes
     - Yes
     - Yes
     - Yes
     - Yes
   * - Export L3 server link subnets
     - Yes
     - Yes
     - Yes
     - Yes
     - Yes
   * - Custom import/export Policies
     - Yes
     - Yes
     - Yes
     - Yes
     - Yes

.. list-table:: Miscellaneous
   :header-rows: 1

   * - AOS Feature
     - EOS
     - NX-OS
     - Cumulus
     - Junos
     - SONiC
   * - Configlets
     - Yes
     - Yes
     - Yes
     - Limited
     - Yes
   * - FFE: add racks/add links/change speed, etc.
     - Yes
     - Yes
     - Yes
     - Yes
     - Yes
   * - Mixed leaf/spine link speed
     - Yes
     - Yes
     - Yes
     - Yes
     - Yes

.. list-table:: L2 Managed Server
   :header-rows: 1

   * - AOS Feature
     - L2 Managed Server
   * - Supported OS Versions
     - Ubuntu 16.04.3, CentOS 7.5, Ubuntu 18.04
   * - Built-in Telemetry (no validation): LLDP, interface, hostname, memory, CPU
     - Yes
   * - Extensible Telemetry
     - Yes
