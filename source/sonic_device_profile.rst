====================
SONiC Device Profile
====================

Background
==========
The Device Profiles (DP) are a way of getting a new device to be recognized and
usable inside AOS. They essentially capture much of the device specific semantics
which are required not only to be discovered by AOS but also run network configs
that work well for the datapath once inside the blueprint.

The Logical Device(LD) provides an abstracted view of a network device to be used
for a given intent. The user is able to make a logical representation of a device
including all information about how it can be used, including roles.

Interface Maps(IM) maps the physical ports as available in a DP to a Logical
Device to generate and run port configs on the device.

It is clear that the sanity of the DP is crucial to ensure successful config
deploys and effective network traffic passage.

The DP is a REST entity, thus allowing the user to create, edit, delete, list
during the design phase of AOS. This DP is then used in the creation of Interface
Maps (IM), which get directly used inside the AOS config rendering engine when the
Blueprints are deployed.

This document tries to record all the necessary knowledge required to create (and
edit) a semantically correct Sonic DP, so that not only does it pass the
validations in place in AOS which ensure the right DP is created in the database,
but also honors the vendor semantic requirement applicable to the device so that
it does not result in deploy failure when the generated configuration is pushed to
the network device.

Problem Statement
=================
A user who creates the DP needs to have the device specification (usually released
by the vendor). It does not stop there. The user also needs to learn a few things
about how to translate these specifications into AOS DP data model so as to create
the valid and config-friendly JSON. This is because the DP is a vendor semantics
aware data structure.

When there is a new device to be added to AOS, which is not already part of
production, one needs to go through the laborious process of first learning the
specifications and then tailoring it to the AOS semantics, which has been found to
be non-trivial without a guide which explains the purpose, meaning and semantic
requirements behind each and every DP data model field.

Solution
========
This document will exhaustively iterate over each and every field in the DP and
record the way one should use it when creating a new DP.

The high level data model is the same for all DPs, i.e. there are the same keys
for every vendor’s DP, just the way we get the values might differ, or might be
loaded with a vendor constraint. The document will try and enlist the following:

* The schema of the DP and the nested elements inside the DP.
* The meaning of each key value pair in the schema.
* The vendor specific recipe the values are populated.
* List any constraints, corner cases which need to be considered, especially for
  port configurations for certain (group of) models.
* Any lessons learnt along the way creating those DPs already in production useful
  in creating future ones.

User Interface
==============
The AOS UI which helps the user to create the DP, while it is intelligent to warn
the user against some semantic validations, is not wholly capable of ensuring deep
vendor specific constraints and requirements. If the user can take care of having
the exact vendor specification, then the AOS UI will certainly help the user
create a semantically valid DP which can be inserted into AOS database
successfully without any errors.

This document will enlist what it takes to get the vendor specifications and make
them part of AOS DP data model. Once the user has these ready, AOS UI can be used
to populate the fields and create the DP successfully.

Alternatively, the user can write his own Python code which takes into account the
vendor specifications, normalize it as per AOS DP data model and generate the json
to then import this into AOS UI.

Selector information
====================
Entering the correct information in all the 4 fields of the selector is critical
for the device to get matched to the device profile.

.. list-table::
    :header-rows: 1
    :class: nowrap

    * - Selector Field
      - Value
      - Command to get the information on device

    * - model
      - 0x21
      - show platform syseeprom

    * - manufacturer
      - If 0x2D in syseeprom, 0x2D else 0x2B
      - show platform syseeprom

    * - OS family
      - SONiC
      - Show version

    * - version
      - .*
      - Show version

Capabilities
============
The hardware and software capabilities are relatively straight forward, if the
user has access to the device specification, it is easy to fetch these fields.
The LXC and onie fields are set to True by default

Here are some commonly found values in sonic devices(this data is based on the
devices AOS already supports)

.. list-table::
    :header-rows: 1
    :class: nowrap

    * - Selector Field
      - Value
      - Command to get the information on device

    * - userland
      - 64 (int)
      - This does not affect config as of AOS 3.1.1

    * - form_factor
      - ‘1RU’ (string)
      - This does not affect config as of AOS 3.1.1

    * - ecmp_limit
      - 64 (int)
      - This does not affect config as of AOS 3.1.1

    * - asic
      - ‘T2’ (string)
      - This does not affect config as of AOS 3.1.1

    * - cpu
      - ‘x86’ (string)
      - This does not affect config as of AOS 3.1.1

    * - ram
      - 16 (int) (Note, the unit is in GB)
      - This does not affect config as of AOS 3.1.1

    * - onie
      - True (bool)
      - This does not affect config as of AOS 3.1.1

    * - lxc
      - True (bool)
      - This does not affect config as of AOS 3.1.1


.. note::
   On supported_features in Sonic DPs, AOS does not require the user to configure
   any extra configurables for “supported features” for Sonic device profiles as
   of AOS 3.1.1. Thus the UI says “no supported features”.

Port Specific Semantics
=======================

Interface naming conventions
----------------------------
Sonic follows the naming conventions as per the sonic port name file as found
Azure SONIC on the github master.
https://github.com/Azure/SONiC/blob/master/doc/sonic-port-name.md

If a user wants to author a SONIC device profile, then he/she needs to read
through the device specific port_config.ini (for example,
sonic-buildimage/device/mellanox/x86_64-mlnx_msn2100-r0/ACS-MSN2100/port_config.ini)
file and follow the instructions in the above link to come up with the right
interface names.

The interface names are explicitly mentioned in the port_config.ini and whatever
is given there is what will be used by SONiC and AOS will need to have DP with
matching interface names which will be used to generate the PORT configs in the
configuration file (config_db.json) . For this document purposes, port_config.ini
and config_db.json should have the same interface naming standard. The user should
use those interface names in their DP along with the lane numbers provided in the
port_cfg.ini file. Once a device profile has been generated based on the
aforementioned steps, AOS will use that along with the LD to generate the
Interface Map (IM). AOS as part of its validation will make sure that the IM
(which describes the port and its speeds) are indeed available and supported under
“/usr/share/sonic/device/ x86_64-mlnx_msn2100-r0/ACS-MSN2100/port_config.ini” .
This validation is performed to make sure SONiC NOS stack does not fail due to
unsupported port configuration (in config_db.json) getting wrongly generated in
AOS due to wrong DP. So it is important that the end user makes sure the DP that
is generated for a SONiC platform has the correct interface names and lane maps as
reflected in port_config.ini file for that particular platform.  A platform may
have a few different port_config.ini files part of different HWSKUs for that
platform. AOS will try to validate the generated port configs with any of the
available options for that platform. AOS currently does not use the Dynamic Port
breakout feature which is on-going in the SONiC project.

Debugging and recovery
======================

Device mismatch
---------------
Usually this is seen at the very beginning of device’s lifecycle. If a device
profile is not being picked by the device, then it is advisable to check the 4
fields as entered in the selector section of the DP.

Deploy errors
-------------
One of the reasons deploy errors are seen are because of incorrect port
configurations. This could be either the ports were configured with incorrect
speeds, or the OS specific port constraints were not handled in the DP or in the IM.

A possible flow for root cause would be:

* Check the DP for obvious port capabilities errors. Is the port really capable of
  the speeds the DP has configured. The device specific port_config.ini Sonic open
  source project is a  good resource to parse for ERROR messages.

* Check if the DP has configured autoneg or disabled interfaces correctly. Autoneg
  and disabled can both be expressed in the interface setting field.

* When debugging the interface names and lane mapping, please take a look at the
  corresponding port_config.ini. As an example for AS5712-54X edgecore/accton box
  we can get the port_config.ini file that has the details like lane/name/alias at
  https://github.com/Azure/sonic-buildimage/tree/master/device/accton/x86_64-accton_as5712_54x-r0/Accton-AS5712-54X

* The naming constraints can be read from the SoNIC official documentation. For
  example if the user wants to generate the interface names for Accton 5712 54X
  running SONIC, the port_config.ini is the authority.
  https://github.com/Azure/sonic-buildimage/blob/master/device/accton/x86_64-accton_as5712_54x-r0/Accton-AS5712-54X/port_config.ini
  Sometimes the device might have inter-port constraints. For sonic, it is kind of
  all laid out in the port_config.ini file. There may be multiple port_config.ini
  files for a specific platform and a specific manufacturer with each
  port_config.ini file residing in their on HWSKU folders in the sonic image (like
  the one I had pointed above). The ability to try out different port speeds on
  (outside of what is listed in the port_config.ini) will need knowledge of the
  chipset and also the physical switch manufacturer to see what can be achieved.
  This information may not be available in any white papers unless we ask the
  vendors.

Example of DP and port_config.ini
=================================

Port_config.ini from sonic-buildimage is below for Dell_Z9100
(x86_64-dell_z9100_c2538-r0/Force10-Z9100-C32)

.. code-block:: prompt

   # name          lanes                 alias              index
   Ethernet0       49,50,51,52           hundredGigE1/1     1
   Ethernet4       53,54,55,56           hundredGigE1/2     2
   Ethernet8       57,58,59,60           hundredGigE1/3     3
   Ethernet12      61,62,63,64           hundredGigE1/4     4
   Ethernet16      65,66,67,68           hundredGigE1/5     5
   Ethernet20      69,70,71,72           hundredGigE1/6     6
   Ethernet24      73,74,75,76           hundredGigE1/7     7
   Ethernet28      77,78,79,80           hundredGigE1/8     8
   Ethernet32      37,38,39,40           hundredGigE1/9     9
   Ethernet36      33,34,35,36           hundredGigE1/10    10
   Ethernet40      45,46,47,48           hundredGigE1/11    11
   Ethernet44      41,42,43,44           hundredGigE1/12    12
   Ethernet48      81,82,83,84           hundredGigE1/13    13
   Ethernet52      85,86,87,88           hundredGigE1/14    14
   Ethernet56      89,90,91,92           hundredGigE1/15    15
   Ethernet60      93,94,95,96           hundredGigE1/16    16
   Ethernet64      97,98,99,100          hundredGigE1/17    17
   Ethernet68      101,102,103,104       hundredGigE1/18    18
   Ethernet72      105,106,107,108       hundredGigE1/19    19
   Ethernet76      109,110,111,112       hundredGigE1/20    20
   Ethernet80      21,22,23,24           hundredGigE1/21    21
   Ethernet84      17,18,19,20           hundredGigE1/22    22
   Ethernet88      29,30,31,32           hundredGigE1/23    23
   Ethernet92      25,26,27,28           hundredGigE1/24    24
   Ethernet96      117,118,119,120       hundredGigE1/25    25
   Ethernet100     113,114,115,116       hundredGigE1/26    26
   Ethernet104     125,126,127,128       hundredGigE1/27    27
   Ethernet108     121,122,123,124       hundredGigE1/28    28
   Ethernet112     5,6,7,8               hundredGigE1/29    29
   Ethernet116     1,2,3,4               hundredGigE1/30    30
   Ethernet120     13,14,15,16           hundredGigE1/31    31
   Ethernet124     9,10,11,12            hundredGigE1/32    32


Translate port_config to a port-to-lane_map data structure using parse.py script:

.. code-block:: prompt

   Parse.py
   =========
   #!/usr/bin/python
   # Copyright (c) 2017 Apstrktr, Inc.  All rights reserved.
   # Apstrktr, Inc. Confidential and Proprietary.
   #
   # This source code is licensed under End User License Agreement found in the
   # LICENSE file at http://apstra.com/eula

   # pylint: disable=line-too-long

   import sys
   from pprint import pprint

   # Run the program as ./parse.py <path_to_sonic_platform_port_config.ini>
   # ex: ./parse.py sonic-buildimage/device/mellanox/x86_64-mlnx_msn2100-r0/ACS-MSN2100/port_config.ini
   def get_lanemap(buf):
       if not buf:
           return None
       d = {}
       interface_indices = []
       for line in buf.split('\n'):
           if line.startswith('#'):
               continue
           words = line.split(' ')
           words = [word for word in words if len(word)]
           if not len(words):
               continue
           intf = words[0][8:]
           lane = words[1].split(',')
           interface_indices.append(intf)
           if len(lane) > 1:
               one = 'Ethernet' + str(intf)
               two = 'Ethernet' + str(int(intf)+1)
               three = 'Ethernet' + str(int(intf)+2)
               four = 'Ethernet' + str(int(intf)+3)
               d.update({one:lane[0]})
               d.update({two:lane[1]})
               d.update({three:lane[2]})
               d.update({four:lane[3]})
           else:
               d.update({words[0]:words[1]})
       return {'interface_names' : interface_indices, 'lane_mapping' : d}

   def parse_portconfig(f):
       buf = ''
       with open(f, 'r') as stream:
           buf = stream.read()
       return {'<Platform>': get_lanemap(buf)}


   if __name__ == '__main__':
       assert len(sys.argv) > 1, "Missing port_config.ini in cmdline"
       print "Collecting lane information from ", sys.argv[1]
       pprint(parse_portconfig(sys.argv[1]))
       print "========================================================================================="
       print "              Substitute <Platform> with an identifier for the platform"
       print "     Append the dump into sdk/device-profile/sonic.py's sonic_device_info dictionary"
       print "========================================================================================="


   To run parse.py

   parse.py  <Path to the port_config.ini file from sonic_buildimage>

   Example:

   parse.py sonic-buildimage/device/dell/x86_64-dell_z9100_c2538-r0/Force10-Z9100-C32/port_config.ini

   Collecting lane information from  sonic-buildimage/device/dell/x86_64-dell_z9100_c2538-r0/Force10-Z9100-C32/port_config.ini
   {'<Platform>': {'interface_names': ['0',
                                       '4',
                                       '8',
                                       '12',
                                       '16',
                                       '20',
                                       '24',
                                       '28',
                                       '32',
                                       '36',
                                       '40',
                                       '44',
                                       '48',
                                       '52',
                                       '56',
                                       '60',
                                       '64',
                                       '68',
                                       '72',
                                       '76',
                                       '80',
                                       '84',
                                       '88',
                                       '92',
                                       '96',
                                       '100',
                                       '104',
                                       '108',
                                       '112',
                                       '116',
                                       '120',
                                       '124'],
                   'lane_mapping': {'Ethernet0': '49',
                                    'Ethernet1': '50',
                                    'Ethernet10': '59',
                                    'Ethernet100': '113',
                                    'Ethernet101': '114',
                                    'Ethernet102': '115',
                                    'Ethernet103': '116',
                                    'Ethernet104': '125',
                                    'Ethernet105': '126',
                                    'Ethernet106': '127',
                                    'Ethernet107': '128',
                                    'Ethernet108': '121',
                                    'Ethernet109': '122',
                                    'Ethernet11': '60',
                                    'Ethernet110': '123',
                                    'Ethernet111': '124',
                                    'Ethernet112': '5',
                                    'Ethernet113': '6',
                                    'Ethernet114': '7',
                                    'Ethernet115': '8',
                                    'Ethernet116': '1',
                                    'Ethernet117': '2',
                                    'Ethernet118': '3',
                                    'Ethernet119': '4',
                                    'Ethernet12': '61',
                                    'Ethernet120': '13',
                                    'Ethernet121': '14',
                                    'Ethernet122': '15',
                                    'Ethernet123': '16',
                                    'Ethernet124': '9',
                                    'Ethernet125': '10',
                                    'Ethernet126': '11',
                                    'Ethernet127': '12',
                                    'Ethernet13': '62',
                                    'Ethernet14': '63',
                                    'Ethernet15': '64',
                                    'Ethernet16': '65',
                                    'Ethernet17': '66',
                                    'Ethernet18': '67',
                                    'Ethernet19': '68',
                                    'Ethernet2': '51',
                                    'Ethernet20': '69',
                                    'Ethernet21': '70',
                                    'Ethernet22': '71',
                                    'Ethernet23': '72',
                                    'Ethernet24': '73',
                                    'Ethernet25': '74',
                                    'Ethernet26': '75',
                                    'Ethernet27': '76',
                                    'Ethernet28': '77',
                                    'Ethernet29': '78',
                                    'Ethernet3': '52',
                                    'Ethernet30': '79',
                                    'Ethernet31': '80',
                                    'Ethernet32': '37',
                                    'Ethernet33': '38',
                                    'Ethernet34': '39',
                                    'Ethernet35': '40',
                                    'Ethernet36': '33',
                                    'Ethernet37': '34',
                                    'Ethernet38': '35',
                                    'Ethernet39': '36',
                                    'Ethernet4': '53',
                                    'Ethernet40': '45',
                                    'Ethernet41': '46',
                                    'Ethernet42': '47',
                                    'Ethernet43': '48',
                                    'Ethernet44': '41',
                                    'Ethernet45': '42',
                                    'Ethernet46': '43',
                                    'Ethernet47': '44',
                                    'Ethernet48': '81',
                                    'Ethernet49': '82',
                                    'Ethernet5': '54',
                                    'Ethernet50': '83',
                                    'Ethernet51': '84',
                                    'Ethernet52': '85',
                                    'Ethernet53': '86',
                                    'Ethernet54': '87',
                                    'Ethernet55': '88',
                                    'Ethernet56': '89',
                                    'Ethernet57': '90',
                                    'Ethernet58': '91',
                                    'Ethernet59': '92',
                                    'Ethernet6': '55',
                                    'Ethernet60': '93',
                                    'Ethernet61': '94',
                                    'Ethernet62': '95',
                                    'Ethernet63': '96',
                                    'Ethernet64': '97',
                                    'Ethernet65': '98',
                                    'Ethernet66': '99',
                                    'Ethernet67': '100',
                                    'Ethernet68': '101',
                                    'Ethernet69': '102',
                                    'Ethernet7': '56',
                                    'Ethernet70': '103',
                                    'Ethernet71': '104',
                                    'Ethernet72': '105',
                                    'Ethernet73': '106',
                                    'Ethernet74': '107',
                                    'Ethernet75': '108',
                                    'Ethernet76': '109',
                                    'Ethernet77': '110',
                                    'Ethernet78': '111',
                                    'Ethernet79': '112',
                                    'Ethernet8': '57',
                                    'Ethernet80': '21',
                                    'Ethernet81': '22',
                                    'Ethernet82': '23',
                                    'Ethernet83': '24',
                                    'Ethernet84': '17',
                                    'Ethernet85': '18',
                                    'Ethernet86': '19',
                                    'Ethernet87': '20',
                                    'Ethernet88': '29',
                                    'Ethernet89': '30',
                                    'Ethernet9': '58',
                                    'Ethernet90': '31',
                                    'Ethernet91': '32',
                                    'Ethernet92': '25',
                                    'Ethernet93': '26',
                                    'Ethernet94': '27',
                                    'Ethernet95': '28',
                                    'Ethernet96': '117',
                                    'Ethernet97': '118',
                                    'Ethernet98': '119',
                                    'Ethernet99': '120'}}}
   =========================================================================================
                 Substitute <Platform> with an identifier for the platform
        Append the dump into sdk/device-profile/sonic.py's sonic_device_info dictionary
   =========================================================================================



The output from above will become a dictionary entry in sonic_device_info in the
sonic device_profile generator python file.


Corresponding Device Profile generated in AOS:

.. code-block:: json

   {
     "hardware_capabilities": {
       "asic": "TH",
       "cpu": "x86",
       "ecmp_limit": 64,
       "form_factor": "1RU",
       "ram": 16,
       "userland": 64
     },
     "id": "Force10-Z9100_SONiC",
     "label": "Dell Force10-Z9100_SONiC",
     "ports": [
       {
         "column_id": 1,
         "connector_type": "qsfp28",
         "failure_domain_id": 1,
         "panel_id": 1,
         "port_id": 0,
         "row_id": 1,
         "slot_id": 0,
         "transformations": [
           {
             "interfaces": [
               {
                 "interface_id": 1,
                 "name": "Ethernet0",
                 "setting": "{\"interface\": {\"speed\": \"100000\", \"lane_map\": \"49,50,51,52\"}}",
                 "speed": {
                   "unit": "G",
                   "value": 100
                 },
                 "state": "active"
               }
             ],
             "is_default": true,
             "transformation_id": 1
           },
           {
             "interfaces": [
               {
                 "interface_id": 1,
                 "name": "Ethernet0",
                 "setting": "{\"interface\": {\"speed\": \"40000\", \"lane_map\": \"49,50,51,52\"}}",
                 "speed": {
                   "unit": "G",
                   "value": 40
                 },
                 "state": "active"
               }
             ],
             "is_default": false,
             "transformation_id": 2
           }
         ]
       },
       {
         "column_id": 1,
         "connector_type": "qsfp28",
         "failure_domain_id": 1,
         "panel_id": 1,
         "port_id": 1,
         "row_id": 2,
         "slot_id": 0,
         "transformations": [
           {
             "interfaces": [
               {
                 "interface_id": 1,
                 "name": "Ethernet4",
                 "setting": "{\"interface\": {\"speed\": \"100000\", \"lane_map\": \"53,54,55,56\"}}",
                 "speed": {
                   "unit": "G",
                   "value": 100
                 },
                 "state": "active"
               }
             ],
             "is_default": true,
             "transformation_id": 1
           },
           {
             "interfaces": [
               {
                 "interface_id": 1,
                 "name": "Ethernet4",
                 "setting": "{\"interface\": {\"speed\": \"40000\", \"lane_map\": \"53,54,55,56\"}}",
                 "speed": {
                   "unit": "G",
                   "value": 40
                 },
                 "state": "active"
               }
             ],
             "is_default": false,
             "transformation_id": 2
           }
         ]
       },
       {
         "column_id": 2,
         "connector_type": "qsfp28",
         "failure_domain_id": 1,
         "panel_id": 1,
         "port_id": 2,
         "row_id": 1,
         "slot_id": 0,
         "transformations": [
           {
             "interfaces": [
               {
                 "interface_id": 1,
                 "name": "Ethernet8",
                 "setting": "{\"interface\": {\"speed\": \"100000\", \"lane_map\": \"57,58,59,60\"}}",
                 "speed": {
                   "unit": "G",
                   "value": 100
                 },
                 "state": "active"
               }
             ],
             "is_default": true,
             "transformation_id": 1
           },
           {
             "interfaces": [
               {
                 "interface_id": 1,
                 "name": "Ethernet8",
                 "setting": "{\"interface\": {\"speed\": \"40000\", \"lane_map\": \"57,58,59,60\"}}",
                 "speed": {
                   "unit": "G",
                   "value": 40
                 },
                 "state": "active"
               }
             ],
             "is_default": false,
             "transformation_id": 2
           }
         ]
       },
       {
         "column_id": 2,
         "connector_type": "qsfp28",
         "failure_domain_id": 1,
         "panel_id": 1,
         "port_id": 3,
         "row_id": 2,
         "slot_id": 0,
         "transformations": [
           {
             "interfaces": [
               {
                 "interface_id": 1,
                 "name": "Ethernet12",
                 "setting": "{\"interface\": {\"speed\": \"100000\", \"lane_map\": \"61,62,63,64\"}}",
                 "speed": {
                   "unit": "G",
                   "value": 100
                 },
                 "state": "active"
               }
             ],
             "is_default": true,
             "transformation_id": 1
           },
           {
             "interfaces": [
               {
                 "interface_id": 1,
                 "name": "Ethernet12",
                 "setting": "{\"interface\": {\"speed\": \"40000\", \"lane_map\": \"61,62,63,64\"}}",
                 "speed": {
                   "unit": "G",
                   "value": 40
                 },
                 "state": "active"
               }
             ],
             "is_default": false,
             "transformation_id": 2
           }
         ]
       },
       {
         "column_id": 3,
         "connector_type": "qsfp28",
         "failure_domain_id": 1,
         "panel_id": 1,
         "port_id": 4,
         "row_id": 1,
         "slot_id": 0,
         "transformations": [
           {
             "interfaces": [
               {
                 "interface_id": 1,
                 "name": "Ethernet16",
                 "setting": "{\"interface\": {\"speed\": \"100000\", \"lane_map\": \"65,66,67,68\"}}",
                 "speed": {
                   "unit": "G",
                   "value": 100
                 },
                 "state": "active"
               }
             ],
             "is_default": true,
             "transformation_id": 1
           },
           {
             "interfaces": [
               {
                 "interface_id": 1,
                 "name": "Ethernet16",
                 "setting": "{\"interface\": {\"speed\": \"40000\", \"lane_map\": \"65,66,67,68\"}}",
                 "speed": {
                   "unit": "G",
                   "value": 40
                 },
                 "state": "active"
               }
             ],
             "is_default": false,
             "transformation_id": 2
           }
         ]
       },
       {
         "column_id": 3,
         "connector_type": "qsfp28",
         "failure_domain_id": 1,
         "panel_id": 1,
         "port_id": 5,
         "row_id": 2,
         "slot_id": 0,
         "transformations": [
           {
             "interfaces": [
               {
                 "interface_id": 1,
                 "name": "Ethernet20",
                 "setting": "{\"interface\": {\"speed\": \"100000\", \"lane_map\": \"69,70,71,72\"}}",
                 "speed": {
                   "unit": "G",
                   "value": 100
                 },
                 "state": "active"
               }
             ],
             "is_default": true,
             "transformation_id": 1
           },
           {
             "interfaces": [
               {
                 "interface_id": 1,
                 "name": "Ethernet20",
                 "setting": "{\"interface\": {\"speed\": \"40000\", \"lane_map\": \"69,70,71,72\"}}",
                 "speed": {
                   "unit": "G",
                   "value": 40
                 },
                 "state": "active"
               }
             ],
             "is_default": false,
             "transformation_id": 2
           }
         ]
       },
       {
         "column_id": 4,
         "connector_type": "qsfp28",
         "failure_domain_id": 1,
         "panel_id": 1,
         "port_id": 6,
         "row_id": 1,
         "slot_id": 0,
         "transformations": [
           {
             "interfaces": [
               {
                 "interface_id": 1,
                 "name": "Ethernet24",
                 "setting": "{\"interface\": {\"speed\": \"100000\", \"lane_map\": \"73,74,75,76\"}}",
                 "speed": {
                   "unit": "G",
                   "value": 100
                 },
                 "state": "active"
               }
             ],
             "is_default": true,
             "transformation_id": 1
           },
           {
             "interfaces": [
               {
                 "interface_id": 1,
                 "name": "Ethernet24",
                 "setting": "{\"interface\": {\"speed\": \"40000\", \"lane_map\": \"73,74,75,76\"}}",
                 "speed": {
                   "unit": "G",
                   "value": 40
                 },
                 "state": "active"
               }
             ],
             "is_default": false,
             "transformation_id": 2
           }
         ]
       },
       {
         "column_id": 4,
         "connector_type": "qsfp28",
         "failure_domain_id": 1,
         "panel_id": 1,
         "port_id": 7,
         "row_id": 2,
         "slot_id": 0,
         "transformations": [
           {
             "interfaces": [
               {
                 "interface_id": 1,
                 "name": "Ethernet28",
                 "setting": "{\"interface\": {\"speed\": \"100000\", \"lane_map\": \"77,78,79,80\"}}",
                 "speed": {
                   "unit": "G",
                   "value": 100
                 },
                 "state": "active"
               }
             ],
             "is_default": true,
             "transformation_id": 1
           },
           {
             "interfaces": [
               {
                 "interface_id": 1,
                 "name": "Ethernet28",
                 "setting": "{\"interface\": {\"speed\": \"40000\", \"lane_map\": \"77,78,79,80\"}}",
                 "speed": {
                   "unit": "G",
                   "value": 40
                 },
                 "state": "active"
               }
             ],
             "is_default": false,
             "transformation_id": 2
           }
         ]
       },
       {
         "column_id": 5,
         "connector_type": "qsfp28",
         "failure_domain_id": 1,
         "panel_id": 1,
         "port_id": 8,
         "row_id": 1,
         "slot_id": 0,
         "transformations": [
           {
             "interfaces": [
               {
                 "interface_id": 1,
                 "name": "Ethernet32",
                 "setting": "{\"interface\": {\"speed\": \"100000\", \"lane_map\": \"37,38,39,40\"}}",
                 "speed": {
                   "unit": "G",
                   "value": 100
                 },
                 "state": "active"
               }
             ],
             "is_default": true,
             "transformation_id": 1
           },
           {
             "interfaces": [
               {
                 "interface_id": 1,
                 "name": "Ethernet32",
                 "setting": "{\"interface\": {\"speed\": \"40000\", \"lane_map\": \"37,38,39,40\"}}",
                 "speed": {
                   "unit": "G",
                   "value": 40
                 },
                 "state": "active"
               }
             ],
             "is_default": false,
             "transformation_id": 2
           }
         ]
       },
       {
         "column_id": 5,
         "connector_type": "qsfp28",
         "failure_domain_id": 1,
         "panel_id": 1,
         "port_id": 9,
         "row_id": 2,
         "slot_id": 0,
         "transformations": [
           {
             "interfaces": [
               {
                 "interface_id": 1,
                 "name": "Ethernet36",
                 "setting": "{\"interface\": {\"speed\": \"100000\", \"lane_map\": \"33,34,35,36\"}}",
                 "speed": {
                   "unit": "G",
                   "value": 100
                 },
                 "state": "active"
               }
             ],
             "is_default": true,
             "transformation_id": 1
           },
           {
             "interfaces": [
               {
                 "interface_id": 1,
                 "name": "Ethernet36",
                 "setting": "{\"interface\": {\"speed\": \"40000\", \"lane_map\": \"33,34,35,36\"}}",
                 "speed": {
                   "unit": "G",
                   "value": 40
                 },
                 "state": "active"
               }
             ],
             "is_default": false,
             "transformation_id": 2
           }
         ]
       },
       {
         "column_id": 6,
         "connector_type": "qsfp28",
         "failure_domain_id": 1,
         "panel_id": 1,
         "port_id": 10,
         "row_id": 1,
         "slot_id": 0,
         "transformations": [
           {
             "interfaces": [
               {
                 "interface_id": 1,
                 "name": "Ethernet40",
                 "setting": "{\"interface\": {\"speed\": \"100000\", \"lane_map\": \"45,46,47,48\"}}",
                 "speed": {
                   "unit": "G",
                   "value": 100
                 },
                 "state": "active"
               }
             ],
             "is_default": true,
             "transformation_id": 1
           },
           {
             "interfaces": [
               {
                 "interface_id": 1,
                 "name": "Ethernet40",
                 "setting": "{\"interface\": {\"speed\": \"40000\", \"lane_map\": \"45,46,47,48\"}}",
                 "speed": {
                   "unit": "G",
                   "value": 40
                 },
                 "state": "active"
               }
             ],
             "is_default": false,
             "transformation_id": 2
           }
         ]
       },
       {
         "column_id": 6,
         "connector_type": "qsfp28",
         "failure_domain_id": 1,
         "panel_id": 1,
         "port_id": 11,
         "row_id": 2,
         "slot_id": 0,
         "transformations": [
           {
             "interfaces": [
               {
                 "interface_id": 1,
                 "name": "Ethernet44",
                 "setting": "{\"interface\": {\"speed\": \"100000\", \"lane_map\": \"41,42,43,44\"}}",
                 "speed": {
                   "unit": "G",
                   "value": 100
                 },
                 "state": "active"
               }
             ],
             "is_default": true,
             "transformation_id": 1
           },
           {
             "interfaces": [
               {
                 "interface_id": 1,
                 "name": "Ethernet44",
                 "setting": "{\"interface\": {\"speed\": \"40000\", \"lane_map\": \"41,42,43,44\"}}",
                 "speed": {
                   "unit": "G",
                   "value": 40
                 },
                 "state": "active"
               }
             ],
             "is_default": false,
             "transformation_id": 2
           }
         ]
       },
       {
         "column_id": 7,
         "connector_type": "qsfp28",
         "failure_domain_id": 1,
         "panel_id": 1,
         "port_id": 12,
         "row_id": 1,
         "slot_id": 0,
         "transformations": [
           {
             "interfaces": [
               {
                 "interface_id": 1,
                 "name": "Ethernet48",
                 "setting": "{\"interface\": {\"speed\": \"100000\", \"lane_map\": \"81,82,83,84\"}}",
                 "speed": {
                   "unit": "G",
                   "value": 100
                 },
                 "state": "active"
               }
             ],
             "is_default": true,
             "transformation_id": 1
           },
           {
             "interfaces": [
               {
                 "interface_id": 1,
                 "name": "Ethernet48",
                 "setting": "{\"interface\": {\"speed\": \"40000\", \"lane_map\": \"81,82,83,84\"}}",
                 "speed": {
                   "unit": "G",
                   "value": 40
                 },
                 "state": "active"
               }
             ],
             "is_default": false,
             "transformation_id": 2
           }
         ]
       },
       {
         "column_id": 7,
         "connector_type": "qsfp28",
         "failure_domain_id": 1,
         "panel_id": 1,
         "port_id": 13,
         "row_id": 2,
         "slot_id": 0,
         "transformations": [
           {
             "interfaces": [
               {
                 "interface_id": 1,
                 "name": "Ethernet52",
                 "setting": "{\"interface\": {\"speed\": \"100000\", \"lane_map\": \"85,86,87,88\"}}",
                 "speed": {
                   "unit": "G",
                   "value": 100
                 },
                 "state": "active"
               }
             ],
             "is_default": true,
             "transformation_id": 1
           },
           {
             "interfaces": [
               {
                 "interface_id": 1,
                 "name": "Ethernet52",
                 "setting": "{\"interface\": {\"speed\": \"40000\", \"lane_map\": \"85,86,87,88\"}}",
                 "speed": {
                   "unit": "G",
                   "value": 40
                 },
                 "state": "active"
               }
             ],
             "is_default": false,
             "transformation_id": 2
           }
         ]
       },
       {
         "column_id": 8,
         "connector_type": "qsfp28",
         "failure_domain_id": 1,
         "panel_id": 1,
         "port_id": 14,
         "row_id": 1,
         "slot_id": 0,
         "transformations": [
           {
             "interfaces": [
               {
                 "interface_id": 1,
                 "name": "Ethernet56",
                 "setting": "{\"interface\": {\"speed\": \"100000\", \"lane_map\": \"89,90,91,92\"}}",
                 "speed": {
                   "unit": "G",
                   "value": 100
                 },
                 "state": "active"
               }
             ],
             "is_default": true,
             "transformation_id": 1
           },
           {
             "interfaces": [
               {
                 "interface_id": 1,
                 "name": "Ethernet56",
                 "setting": "{\"interface\": {\"speed\": \"40000\", \"lane_map\": \"89,90,91,92\"}}",
                 "speed": {
                   "unit": "G",
                   "value": 40
                 },
                 "state": "active"
               }
             ],
             "is_default": false,
             "transformation_id": 2
           }
         ]
       },
       {
         "column_id": 8,
         "connector_type": "qsfp28",
         "failure_domain_id": 1,
         "panel_id": 1,
         "port_id": 15,
         "row_id": 2,
         "slot_id": 0,
         "transformations": [
           {
             "interfaces": [
               {
                 "interface_id": 1,
                 "name": "Ethernet60",
                 "setting": "{\"interface\": {\"speed\": \"100000\", \"lane_map\": \"93,94,95,96\"}}",
                 "speed": {
                   "unit": "G",
                   "value": 100
                 },
                 "state": "active"
               }
             ],
             "is_default": true,
             "transformation_id": 1
           },
           {
             "interfaces": [
               {
                 "interface_id": 1,
                 "name": "Ethernet60",
                 "setting": "{\"interface\": {\"speed\": \"40000\", \"lane_map\": \"93,94,95,96\"}}",
                 "speed": {
                   "unit": "G",
                   "value": 40
                 },
                 "state": "active"
               }
             ],
             "is_default": false,
             "transformation_id": 2
           }
         ]
       },
       {
         "column_id": 9,
         "connector_type": "qsfp28",
         "failure_domain_id": 1,
         "panel_id": 1,
         "port_id": 16,
         "row_id": 1,
         "slot_id": 0,
         "transformations": [
           {
             "interfaces": [
               {
                 "interface_id": 1,
                 "name": "Ethernet64",
                 "setting": "{\"interface\": {\"speed\": \"100000\", \"lane_map\": \"97,98,99,100\"}}",
                 "speed": {
                   "unit": "G",
                   "value": 100
                 },
                 "state": "active"
               }
             ],
             "is_default": true,
             "transformation_id": 1
           },
           {
             "interfaces": [
               {
                 "interface_id": 1,
                 "name": "Ethernet64",
                 "setting": "{\"interface\": {\"speed\": \"40000\", \"lane_map\": \"97,98,99,100\"}}",
                 "speed": {
                   "unit": "G",
                   "value": 40
                 },
                 "state": "active"
               }
             ],
             "is_default": false,
             "transformation_id": 2
           }
         ]
       },
       {
         "column_id": 9,
         "connector_type": "qsfp28",
         "failure_domain_id": 1,
         "panel_id": 1,
         "port_id": 17,
         "row_id": 2,
         "slot_id": 0,
         "transformations": [
           {
             "interfaces": [
               {
                 "interface_id": 1,
                 "name": "Ethernet68",
                 "setting": "{\"interface\": {\"speed\": \"100000\", \"lane_map\": \"101,102,103,104\"}}",
                 "speed": {
                   "unit": "G",
                   "value": 100
                 },
                 "state": "active"
               }
             ],
             "is_default": true,
             "transformation_id": 1
           },
           {
             "interfaces": [
               {
                 "interface_id": 1,
                 "name": "Ethernet68",
                 "setting": "{\"interface\": {\"speed\": \"40000\", \"lane_map\": \"101,102,103,104\"}}",
                 "speed": {
                   "unit": "G",
                   "value": 40
                 },
                 "state": "active"
               }
             ],
             "is_default": false,
             "transformation_id": 2
           }
         ]
       },
       {
         "column_id": 10,
         "connector_type": "qsfp28",
         "failure_domain_id": 1,
         "panel_id": 1,
         "port_id": 18,
         "row_id": 1,
         "slot_id": 0,
         "transformations": [
           {
             "interfaces": [
               {
                 "interface_id": 1,
                 "name": "Ethernet72",
                 "setting": "{\"interface\": {\"speed\": \"100000\", \"lane_map\": \"105,106,107,108\"}}",
                 "speed": {
                   "unit": "G",
                   "value": 100
                 },
                 "state": "active"
               }
             ],
             "is_default": true,
             "transformation_id": 1
           },
           {
             "interfaces": [
               {
                 "interface_id": 1,
                 "name": "Ethernet72",
                 "setting": "{\"interface\": {\"speed\": \"40000\", \"lane_map\": \"105,106,107,108\"}}",
                 "speed": {
                   "unit": "G",
                   "value": 40
                 },
                 "state": "active"
               }
             ],
             "is_default": false,
             "transformation_id": 2
           }
         ]
       },
       {
         "column_id": 10,
         "connector_type": "qsfp28",
         "failure_domain_id": 1,
         "panel_id": 1,
         "port_id": 19,
         "row_id": 2,
         "slot_id": 0,
         "transformations": [
           {
             "interfaces": [
               {
                 "interface_id": 1,
                 "name": "Ethernet76",
                 "setting": "{\"interface\": {\"speed\": \"100000\", \"lane_map\": \"109,110,111,112\"}}",
                 "speed": {
                   "unit": "G",
                   "value": 100
                 },
                 "state": "active"
               }
             ],
             "is_default": true,
             "transformation_id": 1
           },
           {
             "interfaces": [
               {
                 "interface_id": 1,
                 "name": "Ethernet76",
                 "setting": "{\"interface\": {\"speed\": \"40000\", \"lane_map\": \"109,110,111,112\"}}",
                 "speed": {
                   "unit": "G",
                   "value": 40
                 },
                 "state": "active"
               }
             ],
             "is_default": false,
             "transformation_id": 2
           }
         ]
       },
       {
         "column_id": 11,
         "connector_type": "qsfp28",
         "failure_domain_id": 1,
         "panel_id": 1,
         "port_id": 20,
         "row_id": 1,
         "slot_id": 0,
         "transformations": [
           {
             "interfaces": [
               {
                 "interface_id": 1,
                 "name": "Ethernet80",
                 "setting": "{\"interface\": {\"speed\": \"100000\", \"lane_map\": \"21,22,23,24\"}}",
                 "speed": {
                   "unit": "G",
                   "value": 100
                 },
                 "state": "active"
               }
             ],
             "is_default": true,
             "transformation_id": 1
           },
           {
             "interfaces": [
               {
                 "interface_id": 1,
                 "name": "Ethernet80",
                 "setting": "{\"interface\": {\"speed\": \"40000\", \"lane_map\": \"21,22,23,24\"}}",
                 "speed": {
                   "unit": "G",
                   "value": 40
                 },
                 "state": "active"
               }
             ],
             "is_default": false,
             "transformation_id": 2
           }
         ]
       },
       {
         "column_id": 11,
         "connector_type": "qsfp28",
         "failure_domain_id": 1,
         "panel_id": 1,
         "port_id": 21,
         "row_id": 2,
         "slot_id": 0,
         "transformations": [
           {
             "interfaces": [
               {
                 "interface_id": 1,
                 "name": "Ethernet84",
                 "setting": "{\"interface\": {\"speed\": \"100000\", \"lane_map\": \"17,18,19,20\"}}",
                 "speed": {
                   "unit": "G",
                   "value": 100
                 },
                 "state": "active"
               }
             ],
             "is_default": true,
             "transformation_id": 1
           },
           {
             "interfaces": [
               {
                 "interface_id": 1,
                 "name": "Ethernet84",
                 "setting": "{\"interface\": {\"speed\": \"40000\", \"lane_map\": \"17,18,19,20\"}}",
                 "speed": {
                   "unit": "G",
                   "value": 40
                 },
                 "state": "active"
               }
             ],
             "is_default": false,
             "transformation_id": 2
           }
         ]
       },
       {
         "column_id": 12,
         "connector_type": "qsfp28",
         "failure_domain_id": 1,
         "panel_id": 1,
         "port_id": 22,
         "row_id": 1,
         "slot_id": 0,
         "transformations": [
           {
             "interfaces": [
               {
                 "interface_id": 1,
                 "name": "Ethernet88",
                 "setting": "{\"interface\": {\"speed\": \"100000\", \"lane_map\": \"29,30,31,32\"}}",
                 "speed": {
                   "unit": "G",
                   "value": 100
                 },
                 "state": "active"
               }
             ],
             "is_default": true,
             "transformation_id": 1
           },
           {
             "interfaces": [
               {
                 "interface_id": 1,
                 "name": "Ethernet88",
                 "setting": "{\"interface\": {\"speed\": \"40000\", \"lane_map\": \"29,30,31,32\"}}",
                 "speed": {
                   "unit": "G",
                   "value": 40
                 },
                 "state": "active"
               }
             ],
             "is_default": false,
             "transformation_id": 2
           }
         ]
       },
       {
         "column_id": 12,
         "connector_type": "qsfp28",
         "failure_domain_id": 1,
         "panel_id": 1,
         "port_id": 23,
         "row_id": 2,
         "slot_id": 0,
         "transformations": [
           {
             "interfaces": [
               {
                 "interface_id": 1,
                 "name": "Ethernet92",
                 "setting": "{\"interface\": {\"speed\": \"100000\", \"lane_map\": \"25,26,27,28\"}}",
                 "speed": {
                   "unit": "G",
                   "value": 100
                 },
                 "state": "active"
               }
             ],
             "is_default": true,
             "transformation_id": 1
           },
           {
             "interfaces": [
               {
                 "interface_id": 1,
                 "name": "Ethernet92",
                 "setting": "{\"interface\": {\"speed\": \"40000\", \"lane_map\": \"25,26,27,28\"}}",
                 "speed": {
                   "unit": "G",
                   "value": 40
                 },
                 "state": "active"
               }
             ],
             "is_default": false,
             "transformation_id": 2
           }
         ]
       },
       {
         "column_id": 13,
         "connector_type": "qsfp28",
         "failure_domain_id": 1,
         "panel_id": 1,
         "port_id": 24,
         "row_id": 1,
         "slot_id": 0,
         "transformations": [
           {
             "interfaces": [
               {
                 "interface_id": 1,
                 "name": "Ethernet96",
                 "setting": "{\"interface\": {\"speed\": \"100000\", \"lane_map\": \"117,118,119,120\"}}",
                 "speed": {
                   "unit": "G",
                   "value": 100
                 },
                 "state": "active"
               }
             ],
             "is_default": true,
             "transformation_id": 1
           },
           {
             "interfaces": [
               {
                 "interface_id": 1,
                 "name": "Ethernet96",
                 "setting": "{\"interface\": {\"speed\": \"40000\", \"lane_map\": \"117,118,119,120\"}}",
                 "speed": {
                   "unit": "G",
                   "value": 40
                 },
                 "state": "active"
               }
             ],
             "is_default": false,
             "transformation_id": 2
           }
         ]
       },
       {
         "column_id": 13,
         "connector_type": "qsfp28",
         "failure_domain_id": 1,
         "panel_id": 1,
         "port_id": 25,
         "row_id": 2,
         "slot_id": 0,
         "transformations": [
           {
             "interfaces": [
               {
                 "interface_id": 1,
                 "name": "Ethernet100",
                 "setting": "{\"interface\": {\"speed\": \"100000\", \"lane_map\": \"113,114,115,116\"}}",
                 "speed": {
                   "unit": "G",
                   "value": 100
                 },
                 "state": "active"
               }
             ],
             "is_default": true,
             "transformation_id": 1
           },
           {
             "interfaces": [
               {
                 "interface_id": 1,
                 "name": "Ethernet100",
                 "setting": "{\"interface\": {\"speed\": \"40000\", \"lane_map\": \"113,114,115,116\"}}",
                 "speed": {
                   "unit": "G",
                   "value": 40
                 },
                 "state": "active"
               }
             ],
             "is_default": false,
             "transformation_id": 2
           }
         ]
       },
       {
         "column_id": 14,
         "connector_type": "qsfp28",
         "failure_domain_id": 1,
         "panel_id": 1,
         "port_id": 26,
         "row_id": 1,
         "slot_id": 0,
         "transformations": [
           {
             "interfaces": [
               {
                 "interface_id": 1,
                 "name": "Ethernet104",
                 "setting": "{\"interface\": {\"speed\": \"100000\", \"lane_map\": \"125,126,127,128\"}}",
                 "speed": {
                   "unit": "G",
                   "value": 100
                 },
                 "state": "active"
               }
             ],
             "is_default": true,
             "transformation_id": 1
           },
           {
             "interfaces": [
               {
                 "interface_id": 1,
                 "name": "Ethernet104",
                 "setting": "{\"interface\": {\"speed\": \"40000\", \"lane_map\": \"125,126,127,128\"}}",
                 "speed": {
                   "unit": "G",
                   "value": 40
                 },
                 "state": "active"
               }
             ],
             "is_default": false,
             "transformation_id": 2
           }
         ]
       },
       {
         "column_id": 14,
         "connector_type": "qsfp28",
         "failure_domain_id": 1,
         "panel_id": 1,
         "port_id": 27,
         "row_id": 2,
         "slot_id": 0,
         "transformations": [
           {
             "interfaces": [
               {
                 "interface_id": 1,
                 "name": "Ethernet108",
                 "setting": "{\"interface\": {\"speed\": \"100000\", \"lane_map\": \"121,122,123,124\"}}",
                 "speed": {
                   "unit": "G",
                   "value": 100
                 },
                 "state": "active"
               }
             ],
             "is_default": true,
             "transformation_id": 1
           },
           {
             "interfaces": [
               {
                 "interface_id": 1,
                 "name": "Ethernet108",
                 "setting": "{\"interface\": {\"speed\": \"40000\", \"lane_map\": \"121,122,123,124\"}}",
                 "speed": {
                   "unit": "G",
                   "value": 40
                 },
                 "state": "active"
               }
             ],
             "is_default": false,
             "transformation_id": 2
           }
         ]
       },
       {
         "column_id": 15,
         "connector_type": "qsfp28",
         "failure_domain_id": 1,
         "panel_id": 1,
         "port_id": 28,
         "row_id": 1,
         "slot_id": 0,
         "transformations": [
           {
             "interfaces": [
               {
                 "interface_id": 1,
                 "name": "Ethernet112",
                 "setting": "{\"interface\": {\"speed\": \"100000\", \"lane_map\": \"5,6,7,8\"}}",
                 "speed": {
                   "unit": "G",
                   "value": 100
                 },
                 "state": "active"
               }
             ],
             "is_default": true,
             "transformation_id": 1
           },
           {
             "interfaces": [
               {
                 "interface_id": 1,
                 "name": "Ethernet112",
                 "setting": "{\"interface\": {\"speed\": \"40000\", \"lane_map\": \"5,6,7,8\"}}",
                 "speed": {
                   "unit": "G",
                   "value": 40
                 },
                 "state": "active"
               }
             ],
             "is_default": false,
             "transformation_id": 2
           }
         ]
       },
       {
         "column_id": 15,
         "connector_type": "qsfp28",
         "failure_domain_id": 1,
         "panel_id": 1,
         "port_id": 29,
         "row_id": 2,
         "slot_id": 0,
         "transformations": [
           {
             "interfaces": [
               {
                 "interface_id": 1,
                 "name": "Ethernet116",
                 "setting": "{\"interface\": {\"speed\": \"100000\", \"lane_map\": \"1,2,3,4\"}}",
                 "speed": {
                   "unit": "G",
                   "value": 100
                 },
                 "state": "active"
               }
             ],
             "is_default": true,
             "transformation_id": 1
           },
           {
             "interfaces": [
               {
                 "interface_id": 1,
                 "name": "Ethernet116",
                 "setting": "{\"interface\": {\"speed\": \"40000\", \"lane_map\": \"1,2,3,4\"}}",
                 "speed": {
                   "unit": "G",
                   "value": 40
                 },
                 "state": "active"
               }
             ],
             "is_default": false,
             "transformation_id": 2
           }
         ]
       },
       {
         "column_id": 16,
         "connector_type": "qsfp28",
         "failure_domain_id": 1,
         "panel_id": 1,
         "port_id": 30,
         "row_id": 1,
         "slot_id": 0,
         "transformations": [
           {
             "interfaces": [
               {
                 "interface_id": 1,
                 "name": "Ethernet120",
                 "setting": "{\"interface\": {\"speed\": \"100000\", \"lane_map\": \"13,14,15,16\"}}",
                 "speed": {
                   "unit": "G",
                   "value": 100
                 },
                 "state": "active"
               }
             ],
             "is_default": true,
             "transformation_id": 1
           },
           {
             "interfaces": [
               {
                 "interface_id": 1,
                 "name": "Ethernet120",
                 "setting": "{\"interface\": {\"speed\": \"40000\", \"lane_map\": \"13,14,15,16\"}}",
                 "speed": {
                   "unit": "G",
                   "value": 40
                 },
                 "state": "active"
               }
             ],
             "is_default": false,
             "transformation_id": 2
           }
         ]
       },
       {
         "column_id": 16,
         "connector_type": "qsfp28",
         "failure_domain_id": 1,
         "panel_id": 1,
         "port_id": 31,
         "row_id": 2,
         "slot_id": 0,
         "transformations": [
           {
             "interfaces": [
               {
                 "interface_id": 1,
                 "name": "Ethernet124",
                 "setting": "{\"interface\": {\"speed\": \"100000\", \"lane_map\": \"9,10,11,12\"}}",
                 "speed": {
                   "unit": "G",
                   "value": 100
                 },
                 "state": "active"
               }
             ],
             "is_default": true,
             "transformation_id": 1
           },
           {
             "interfaces": [
               {
                 "interface_id": 1,
                 "name": "Ethernet124",
                 "setting": "{\"interface\": {\"speed\": \"40000\", \"lane_map\": \"9,10,11,12\"}}",
                 "speed": {
                   "unit": "G",
                   "value": 40
                 },
                 "state": "active"
               }
             ],
             "is_default": false,
             "transformation_id": 2
           }
         ]
       }
     ],
     "selector": {
       "manufacturer": "Dell|DELL",
       "model": "Z9100-ON",
       "os": "SONiC",
       "os_version": ".*"
     },
     "slot_count": 0,
     "software_capabilities": {
       "lxc_support": false,
       "onie": true
     }
   }
