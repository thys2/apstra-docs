========
Topology
========
To access the active topology, from the blueprint, navigate to
**Active / Physical / Topology**.

Topology Views
==============
Topologies can be viewed at three levels of detail: grouped, compact, and
full. (Grouped and compact are new in AOS version 3.3.0.) Green elements in the
topology indicate no anomalies. Red elements indicate anomalies, which must be
resolved before committing changes.

Grouped
-------
* To show servers, click the **Show Servers** check box.
* To show node name (and hostname as applicable) hover over an element (square).
* To show node details, select the node by either clicking its element or by
  selecting it from the **Selected Node** drop-down list.

.. image:: static/images/blueprints/active/physical/active_topology_grouped_330.png

Compact
-------
* To show servers, click the **Show Servers** check box.
* To show node name (and hostname as applicable) hover over an element (square).
* To show node details, select the node by either clicking its element or by
  selecting it from the **Selected Node** drop-down list.

.. image:: static/images/blueprints/active/physical/active_topology_compact_330.png

Full
----
* To make topology elements larger, click the **Expand Nodes** check box.
* To show the links between elements, click the **Show Links** check box.
* To show node name, and hostname as applicable, hover over an element.
* To change the labels (name, hostname, S/N) that are shown in the topology, select
  a different label from the **Topology Label** drop-down list.
* To show node details, select the node by either clicking its element in the
  topology or by selecting it from the **Selected Node** drop-down list.

.. image:: static/images/blueprints/active/physical/active_topology_full_330.png

Topology Selection Views
========================
When a node is selected from the topology view (by clicking its element in
the topology, or by selecting it from the **Selected Nodes** drop-down list), the
node topology is shown. The selected node can be viewed in three different ways:
neighbors view, links view, or (new in AOS version 3.3.0) headroom view. Telemetry
and other device properties are shown in the
:doc:`selection panel <active_selection>` on the right.

Neighbors View
--------------
* Aggregated server-leaf links are encircled (new in AOS version 3.3.0).
* To show unused ports, click the **Show Unused Ports** check box (new in AOS
  version 3.3.0).
* To change the labels (name, hostname, S/N) that are shown in the topology, select
  a different label from the **Topology Label** drop-down list.
* To change the layer that is shown in the topology, select a different layer
  (intent, traffic heat) from the **Layer** drop-down list.
* Choose to show all neighbors or only specific ones.

.. image:: static/images/blueprints/active/physical/active_topology_neighbors_330.png

Links View
----------

.. image:: static/images/blueprints/active/physical/active_topology_links_330.png

Headroom View
-------------

.. versionadded:: 3.3.0

  Headroom View is available in the AOS Blueprint Active Topology view.

To view the Headroom View in the AOS Blueprint Active Topology, go to Active →
Physical → Topology, select the layer **Traffic Heat** to view Traffic Heat view
on top of Physical topology.

.. image:: static/images/blueprints/active/physical/active_topology_headroom_1.png

.. note::

    **Device Traffic probe** must be enabled for Headroom View layer. If user
    disables or deletes the probe, the Layer Traffic Heat" under the Active
    topology will disappear. See :doc:`Device Traffic Probe <probe_headroom>` for
    more info.

The view uses different colors to describe the capacity, where different color means
different available/used capacity.

.. image:: static/images/blueprints/active/physical/active_topology_headroom_color.png

The node colors are based on the current System level TX/RX, averaged to 2min
by default. If the aggregated TX or RX across all the device interfaces is
< 20%: color **Green** . If between 21-40%, color **Yellow** etc.
For each 20% difference, capacity is shown with a different color.

.. note::

    The server coloring is calculated based on the interface counters of the leaf
    ports facing that server.

Traffic Heat view is available at all three levels of detail: grouped, compact,
and full. See :ref:`active_topology:Topology Views` to know more about levels.

.. image:: static/images/blueprints/active/physical/active_topology_headroom_compact_grouped.png

If any of a device deployed ports is > 81% of its capacity in either RX or TX,
a new "Alert" icon is shown on the device.

.. image:: static/images/blueprints/active/physical/active_topology_headroom_2.png

Mousing over the node will show the exact aggregated values.

.. image:: static/images/blueprints/active/physical/active_topology_headroom_3.png

.. note::

    On the grouped/compact view, we show this alert by coloring the right top
    corner of the square.

To view RX/TX per interface for a single node, click on the node.

.. image:: static/images/blueprints/active/physical/active_topology_headroom_4.png

To view Headroom View layer, click on the node and select **Headroom View** .
Hovering over to a link will display available Headroom capacity for that link.

.. image:: static/images/blueprints/active/physical/active_topology_headroom_5.png

Traffic History
---------------
To view traffic history on top of physical topology.

#. Select **Time Series**.
#. Select **Aggregation**: Users can select 2, 5, 10, 20 or 30 minutes to check
   traffic at specific time in the past but can also select larger aggregation
   such as 1 hour or 1 day to see traffic average across longer period of time.
#. Use Bottom Slider to select a past period of time to look at. The bottom left
   date+time is how far back user can go, based on data retention time.
   The bottom  right date+time is when the user open the time series.
#. Use top slider to navigate across time in the selected period.
#. See Node Colors changing based on System level Traffic Heat.

   .. image:: static/images/blueprints/active/physical/active_topology_headroom_6.png
