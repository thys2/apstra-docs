================
Processor: Range
================
The Range processor checks that a value is in a range. According to the
specified range, it configures a check for the input series. This check returns
an anomaly value if a series aggregation value, such as a last value, sum, avg
etc., is in the range. This aggregation type is configured by the 'property'
attribute, which is set to 'value' if not specified. The output series contains
anomaly values, such as 'true' and 'false'. (Previously called
'not_in_range' and 'range_check'.)

As of AOS version 3.1, the range processor was enhanced to generate the output
of True when the input matches the specified criteria - this is the opposite of
previous behavior.

**Input Types** - Number-Set (NS), NSTS

**Output Types** - Discrete-State-Set (DSS)

**Properties**

   Property
      A property of input items which is used to check against the range.
      Enum of either value, sample_count, sum, avg
   Anomalous Range (range)
      Numeric range, either min or max is optional. Float type is acceptable only
      with property "std_dev", other property values require integers. Min and max
      can be expressions evaluated into numeric values.

   .. include:: includes/processors/graph_query.rst

   .. include:: includes/processors/non_collector_graph_query.rst

   Anomaly MetricLog Retention Duration
      Retain anomaly metric data in MetricDb for specified duration in seconds
   Anomaly MetricLog Retention Size
      Maximum allowed size, in bytes of anomaly metric data to store in MetricDB
   Anomaly Metric Logging
      Enable metric logging for anomalies

   .. include:: includes/processors/enable_streaming.rst

   .. include:: includes/processors/raise_anomaly.rst

Range Example
-------------

.. code-block:: none

    range: {"min": 35, "max": 45}
    property: "value"

Sample Input (NS)

.. code-block:: none

  [if_name=eth0] : 23
  [if_name=eth1] : 55
  [if_name=eth3] : 37

Sample Output (DSS)

.. code-block:: none

  [if_name=eth0] : "false"
  [if_name=eth1] : "false"
  [if_name=eth3] : "true"

If expressions are used for `min` or `max` fields of the `range` property, then
they are evaluated for each input item which results into item-specific
thresholds. Properties of the respective output item are extended by `range_min`
or `range_max` properties with calculated values.

.. code-block:: none

    range: {"max": "speed * 0.7"}
    property: "value"

Sample Input (NS)

.. code-block:: none

  [if_name=eth0,speed=10000000000] : 800000000
  [if_name=eth1,speed=1000000000] : 800000000

Sample Output (DSS)

.. code-block:: none

  [if_name=eth0,speed=10000000000,range_max=7000000000] : "false"
  [if_name=eth1,speed=1000000000,range_max=700000000] : "true"
