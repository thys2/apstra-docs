======================
Juniper Device Profile
======================

Overview
========
.. versionadded:: 3.3.0
    AOS now supports Juniper QFX10002, QFX5110 and QFX5100 series Junos devices.

Apstra AOS provides Device Profiles for Juniper QFX10002, QFX5110 and QFX5100
series Junos devices.

This will document any constraints for these AOS Device Profiles.

Juniper QFX10002
================

The 36-port Juniper QFX10002-36Q and 72-port QFX10002-72Q are supported by AOS.
Both of these model have a port constraint where only certain ports can be used
with QSFP28 100G transceivers.

.. image:: static/images/juniper_device_profile/qfx10002_dp_1_330.png

If these ports are used as 100G, then the adjacent QSFP 40G ports cannot be used.
The AOS Device Profile cannot automatically disable the adjacent QSFP 40G ports.
The user must create an AOS Interface Map with these ports unused and disabled.

For the QFX10002 when creating the AOS Interface Map, after selecting the 100G
ports, AOS will display an option to the user. "*Do you want to select the disabled
interfaces for unused device profile ports?*"

.. image:: static/images/juniper_device_profile/qfx10002_dp_2_330.png

For 100G ports on the QFX10002, the user must click the **OK** option for this so
the unused QSFP ports are disabled and cannot be used.
