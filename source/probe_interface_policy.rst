=============================
Interface Policy 802.1x Probe
=============================
To monitor 802.1X supplicants and interface authentication, AOS provides a predefined
IBA probe named Interface Policy Probe.

AOS users can make use of probes to maintain 802.1X networks. From the Probes
view of your Analytics Blueprint, click Instantiate Predefined Probe button,
then find Interface policy 802.1X in the list that pops up, then click Create.
This probe is not activated by default.

.. image:: static/images/blueprint_policies_tasks/interface_policy_probe_dot1x_01.png
.. image:: static/images/blueprint_policies_tasks/interface_policy_probe_dot1x_02.png

The 802.1X hosts probe gives a fast view of network 802.1X MAC Addresses,
authorization status, ports, and dynamic VLAN information.

.. image:: static/images/blueprint_policies_tasks/interface_policy_probe_dot1x_03.png
