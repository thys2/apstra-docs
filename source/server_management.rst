=====================
AOS Server Management
=====================

Monitoring AOS Server via CLI
=============================
An AOS Server CLI command ``sudo service aos status`` can be used for
simple status check.

.. code-block:: prompt
      :emphasize-lines: 1

      admin@aos-server:~$ sudo service aos status
      * aos.service - LSB: Start AOS management system
         Loaded: loaded (/etc/init.d/aos; generated)
         Active: active (exited) since Tue 2020-07-28 00:35:38 UTC; 2h 13min ago
           Docs: man:systemd-sysv-generator(8)
          Tasks: 0 (limit: 4915)
         CGroup: /aos.service

      Jul 28 00:35:35 aos-server systemd[1]: Starting LSB: Start AOS management system...
      Jul 28 00:35:36 aos-server aos[1040]: net.core.wmem_max = 33554432
      Jul 28 00:35:37 aos-server aos[1040]: Creating aos_sysdb_1 ...
      Jul 28 00:35:37 aos-server aos[1040]: Creating aos_nginx_1 ...
      Jul 28 00:35:37 aos-server aos[1040]: Creating aos_auth_1  ...
      Jul 28 00:35:37 aos-server aos[1040]: Creating aos_controller_1 ...
      Jul 28 00:35:37 aos-server aos[1040]: Creating aos_metadb_1     ...
      Jul 28 00:35:38 aos-server aos[1040]: [240B blob data]
      Jul 28 00:35:38 aos-server systemd[1]: Started LSB: Start AOS management system.
      admin@aos-server:~$

An AOS Server script ``aos_controller_health_check`` is available for
further error collection. The command runs a search for known error
signatures in AOS server logs, such as agent crashes, and returns the
output. The information can be used for troubleshooting.

Usage:

.. code-block:: text
      :emphasize-lines: 1

      admin@aos-server:~$ docker exec aos_controller_1 aos_controller_health_check
      admin@aos-server:~$

The script outputs errors it finds. If no errors are found, it returns no output.

Restarting AOS Server
=====================

.. sidebar:: Telemetry Alarm

      AOS device agents may temporarily log a "liveness" telemetry alarm
      if AOS Server is down, but this clears in a minute or two after
      AOS services are restored.

If you need to restart AOS Server, you can simply reboot the VM, or use the
service command ``sudo service aos stop``.

.. code-block:: prompt
      :emphasize-lines: 1,2

      admin@aos-server:~$ sudo service aos stop
      admin@aos-server:~$ sudo service aos start
      admin@aos-server:~$

Reinstalling AOS Server
=======================

This is a *nuclear* option that wipes and reinstalls AOS on an existing server.
This is mostly helpful when doing a *proof of concept* or demo install. If you
have problems that require you to reinstall AOS, please contact support@apstra.com.

.. warning::
    The below procedure removes ALL AOS data from an AOS Server VM.  Use with
    care.

#. Obtain the AOS "Installer" .run file from Apstra Customer Portal Downloads,
   https://portal.apstra.com/downloads

   .. image:: static/images/server_installation/aos_run_file_310.png

#. Stop AOS service, if possible.

   .. code-block:: prompt
      :emphasize-lines: 1

      admin@aos-server:~$ sudo service aos stop
      admin@aos-server:~$

#. Delete the AOS Server database.

   .. code-block:: prompt
      :emphasize-lines: 1

      admin@aos-server:~$ sudo rm -rf /var/lib/aos/db/*
      admin@aos-server:~$

#. Remove the aos-compose package

   .. code-block:: prompt
      :emphasize-lines: 1

      admin@aos-server:~$ sudo dpkg -r aos-compose
      (Reading database ... 110457 files and directories currently installed.)
      Removing aos-compose (3.3.0-660) ...
      Processing triggers for ureadahead (0.100.0-21) ...
      Processing triggers for systemd (237-3ubuntu10.41) ...
      admin@aos-server:~$

#. Reinstall AOS from the .run file.

   .. code-block:: prompt
      :emphasize-lines: 1

      admin@aos-server:~$ sudo bash aos_3.3.0-662.run
      Verifying archive integrity... All good.
      Uncompressing AOS installer  100%
      610bd1ae69b7: Loading layer [==================================================>]  52.44MB/52.44MB
      87db235c4ff8: Loading layer [==================================================>]  211.3MB/211.3MB
      668b88b6cd3d: Loading layer [==================================================>]  117.3MB/117.3MB
      b1dd55ca7fd9: Loading layer [==================================================>]  20.63MB/20.63MB
      3f8ebc7f1fae: Loading layer [==================================================>]  4.608kB/4.608kB
      Loaded image: aos:3.3.0-662
      AOS[2020-07-28_02:58:36]: Installing AOS 3.3.0-662 package
      admin@aos-server:~$

   The user can now restore an aos_backup or build a new AOS Blueprint.

AOS Server Database Management
==============================

The AOS database is stored in a single folder in AOS Server at
``/var/lib/aos/db``. The AOS database can be easily copied between AOS Servers.
The AOS Server container and related AOS database containers are run under
Docker.

Requirements

* AOS Source and Target database versions must be the same version of AOS.
  If AOS versions are different, please contact support before proceeding
  so we can work with you to verify the procedure.

* AOS Source and Target must have the same IP address after starting AOS to ensure
  device agents can 'call home' properly after database restoration.  AOS can be
  restored to a different IP address, but each device agent will need to be
  reconfigured (/mnt/flash/aos-config, /etc/aos/aos.conf) to point to
  the new AOS IP.

.. warning::
    Any changes you make *within* AOS Server are *not* stored in the
    AOS backup.

Backing up AOS Database
-----------------------
Backups can be performed while AOS is running. They are saved in the AOS server as
a dated snapshot: ``/var/lib/aos/snapshot/``.

If the AOS server needs to be restored or if its disk image becomes corrupt, the
AOS server is lost, including any backups. We recommend that you periodically move
backups from the AOS server to a secure location.

If you've scheduled cron jobs to periodically backup AOS DB, make sure to rotate
these files off the AOS server to keep the AOS server VM disk from becoming full.

Back up the AOS server by typing ``aos_backup`` at the prompt.

If all IBA probes have been disabled, the following message appears.

.. code-block:: prompt
      :emphasize-lines: 1

      admin@aos-server:~$ sudo aos_backup
      ====================================================================
        Backup operation completed successfully.
      ====================================================================
      New AOS snapshot: 2020-07-28_20-56-26
      admin@aos-server:~$

.. note::

   If many IBA probes are enabled or if any other DB "write" tasks are in progress,
   they may not be included in the AOS backup, and the following message appears.

   .. code-block:: prompt
      :emphasize-lines: 1

      admin@aos-server:~$ sudo aos_backup
      =====================================================================
      Warning:
       Backup operation has been completed successfully. However AOS state
      has been changed while this script was running, which means some
      changes might not have been captured in the snapshot created in this
      backup. You may choose to invoke aos_backup script again if you wish
      to capture these changes right now instead of waiting for the next
      backup operation.
      =====================================================================
      New AOS snapshot: 2019-12-06_16-15-57
      admin@aos-server:~$

   In this case, disable IBA probes and run the ``aos_backup`` command again.

.. note::

   The AOS backup/restore currently does not support Devices/OS Images
   information. The Devices/OS Images information will be lost after the AOS
   restore operation.

Validating the backup
---------------------
The backup is stored in ``/var/lib/aos/snapshot/<date>/aos.data.tar.gz``

Copy the contents of the AOS snapshot directory to your backup infrastructure.
This also includes the aos_restore script.

.. code-block:: prompt
      :emphasize-lines: 1

      admin@aos-server:~$ sudo ls -lah /var/lib/aos/snapshot/
      total 20K
      drwx------ 5 root root 4.0K Jul 28 20:58 .
      drwxr-xr-x 7 root root 4.0K Jul 28 02:43 ..
      drwx------ 2 root root 4.0K Jul 28 02:43 2020-07-28_02-43-12
      drwx------ 2 root root 4.0K Jul 28 20:56 2020-07-28_20-56-26
      drwx------ 2 root root 4.0K Jul 28 20:58 2020-07-28_20-58-54
      admin@aos-server:~$

Restoring AOS Database
----------------------

.. note::
    If restoring an AOS backup to a new AOS Server which uses a different network
    interface for AOS access (e.g. eth1 vs eth0), the ``metadb`` variable in
    the ``[controller]`` section of the ``/etc/aos/aos.conf`` configuration
    file needs to be edited and the AOS Server needs to be restarted.

#. Confirm the contents of the snapshot folder exists somewhere on the filesystem.
   For example, we have copied the restoration data to */tmp/aos_test_restore*.

   .. code-block:: prompt
      :emphasize-lines: 1

      admin@aos-server:~$ sudo ls -lah /var/lib/aos/snapshot/2020-07-28_20-56-26/
      total 21M
      drwx------ 2 root root 4.0K Jul 28 20:56 .
      drwx------ 5 root root 4.0K Jul 28 20:58 ..
      -rw------- 1 root root  21M Jul 28 20:56 aos.data.tar.gz
      -rwxr-xr-x 1 root root 1.3K Jul 28 20:56 aos_restore
      -rw------- 1 root root    1 Jul 28 20:56 comment.txt
      admin@aos-server:~$

#. Run the AOS restore command from the snapshotted restoration file. The restore
   process first does a new backup of the current AOS DB.

   .. code-block:: prompt
      :emphasize-lines: 1

      admin@aos-server:~$ sudo bash /var/lib/aos/snapshot/2020-07-28_20-56-26/aos_restore
      ====================================================================
        Backup operation completed successfully.
      ====================================================================
      New AOS snapshot: 2020-07-28_20-58-54
      (Reading database ... 110457 files and directories currently installed.)
      Removing aos-compose (3.3.0-660) ...
      Processing triggers for ureadahead (0.100.0-21) ...
      Processing triggers for systemd (237-3ubuntu10.41) ...
      tar: Removing leading `/' from member names
      /etc/aos/aos.conf
      /etc/aos-credential/secret_key
      /var/lib/aos/db/
      /var/lib/aos/db/_Main-000000005f1f7314-000dd7b1-checkpoint
      /var/lib/aos/db/_AosController-000000005f1f7314-00035dd2-checkpoint-valid
      /var/lib/aos/db/_Central-000000005f1f7313-000ab5f7-checkpoint-valid
      /var/lib/aos/db/_Metadb-000000005f1f7312-000a27e1-checkpoint
      /var/lib/aos/db/_AosSysdb-000000005f1f7312-000a31be-checkpoint-valid
      /var/lib/aos/db/_Credential-000000005f1f7312-000ea6ba-log
      /var/lib/aos/db/_Central-000000005f1f7313-000ab5f7-log
      /var/lib/aos/db/_Auth-000000005f1f7313-0001e8cf-log-valid
      /var/lib/aos/db/_Metadb-000000005f1f7312-000a27e1-log-valid
      /var/lib/aos/db/_Auth-000000005f1f7313-0001e8cf-checkpoint-valid
      /var/lib/aos/db/_Metadb-000000005f1f7312-000a27e1-checkpoint-valid
      /var/lib/aos/db/_Main-000000005f1f7314-000dd7b1-log
      /var/lib/aos/db/_Auth-000000005f1f7313-0001e8cf-log
      /var/lib/aos/db/_Metadb-000000005f1f7312-000a27e1-log
      /var/lib/aos/db/_AosSysdb-000000005f1f7312-000a31be-log-valid
      /var/lib/aos/db/_AosAuth-000000005f1f7312-000a0e46-log-valid
      /var/lib/aos/db/_AosSysdb-000000005f1f7312-000a31be-log
      /var/lib/aos/db/blueprint_backups/
      /var/lib/aos/db/blueprint_backups/37321b9c-25b1-4111-849b-522a3852949d/
      /var/lib/aos/db/blueprint_backups/37321b9c-25b1-4111-849b-522a3852949d/48/
      /var/lib/aos/db/blueprint_backups/37321b9c-25b1-4111-849b-522a3852949d/48/graph.json.zip
      /var/lib/aos/db/blueprint_backups/37321b9c-25b1-4111-849b-522a3852949d/48/graph.md5sum
      /var/lib/aos/db/_Credential-000000005f1f7312-000ea6ba-log-valid
      /var/lib/aos/db/_Credential-000000005f1f7312-000ea6ba-checkpoint
      /var/lib/aos/db/_Main-000000005f1f7314-000dd7b1-checkpoint-valid
      /var/lib/aos/db/_Central-000000005f1f7313-000ab5f7-log-valid
      /var/lib/aos/db/_AosController-000000005f1f7314-00035dd2-log-valid
      /var/lib/aos/db/_AosAuth-000000005f1f7312-000a0e46-checkpoint-valid
      /var/lib/aos/db/_AosSysdb-000000005f1f7312-000a31be-checkpoint
      /var/lib/aos/db/_AosController-000000005f1f7314-00035dd2-checkpoint
      /var/lib/aos/db/_AosAuth-000000005f1f7312-000a0e46-checkpoint
      /var/lib/aos/db/.devpi/
      /var/lib/aos/db/.devpi/server/
      /var/lib/aos/db/.devpi/server/.nodeinfo
      /var/lib/aos/db/.devpi/server/.secret
      /var/lib/aos/db/.devpi/server/.sqlite
      /var/lib/aos/db/.devpi/server/.serverversion
      /var/lib/aos/db/.devpi/server/.event_serial
      /var/lib/aos/db/_AosController-000000005f1f7314-00035dd2-log
      /var/lib/aos/db/_Main-000000005f1f7314-000dd7b1-log-valid
      /var/lib/aos/db/_Central-000000005f1f7313-000ab5f7-checkpoint
      /var/lib/aos/db/_Auth-000000005f1f7313-0001e8cf-checkpoint
      /var/lib/aos/db/_Credential-000000005f1f7312-000ea6ba-checkpoint-valid
      /var/lib/aos/db/_AosAuth-000000005f1f7312-000a0e46-log
      /var/lib/aos/anomaly/
      /var/lib/aos/anomaly/_Anomaly-000000005f1f7313-00060aba-log
      /var/lib/aos/anomaly/_Anomaly-000000005f1f7313-00060aba-checkpoint-valid
      /var/lib/aos/anomaly/_Anomaly-000000005f1f7313-00060aba-log-valid
      /var/lib/aos/anomaly/_Anomaly-000000005f1f7313-00060aba-checkpoint
      /opt/aos/aos-compose.deb
      /opt/aos/frontend_images/
      /opt/aos/frontend_images/aos-web-ui.zip
      Selecting previously unselected package aos-compose.
      (Reading database ... 110440 files and directories currently installed.)
      Preparing to unpack /opt/aos/aos-compose.deb ...
      Unpacking aos-compose (3.3.0-660) ...
      Setting up aos-compose (3.3.0-660) ...
      Processing triggers for ureadahead (0.100.0-21) ...
      Processing triggers for systemd (237-3ubuntu10.41) ...
      Starting aos_sysdb_1      ... done
      Starting aos_controller_1 ... done
      Starting aos_nginx_1      ... done
      Starting aos_auth_1       ... done
      Starting aos_metadb_1     ... done
      admin@aos-server:~$

Validating the restore
-----------------------
AOS Database has been restored and migrated to a new server. The entire system
state has been copied over from the backed up AOS installation
to the new AOS target.

.. code-block:: prompt
      :emphasize-lines: 1,17

      admin@aos-server:~$ sudo service aos status
      * aos.service - LSB: Start AOS management system
         Loaded: loaded (/etc/init.d/aos; generated)
         Active: inactive (dead)
           Docs: man:systemd-sysv-generator(8)

      Jul 28 00:36:32 aos-server aos[1078]: [240B blob data]
      Jul 28 00:36:32 aos-server systemd[1]: Started LSB: Start AOS management system.
      Jul 28 02:45:45 aos-server systemd[1]: Stopping LSB: Start AOS management system...
      Jul 28 02:45:46 aos-server aos[4968]: Stopping aos_controller_1 ...
      Jul 28 02:45:46 aos-server aos[4968]: Stopping aos_metadb_1     ...
      Jul 28 02:45:46 aos-server aos[4968]: Stopping aos_auth_1       ...
      Jul 28 02:45:46 aos-server aos[4968]: Stopping aos_sysdb_1      ...
      Jul 28 02:45:46 aos-server aos[4968]: Stopping aos_nginx_1      ...
      Jul 28 02:45:58 aos-server aos[4968]: [240B blob data]
      Jul 28 02:45:58 aos-server systemd[1]: Stopped LSB: Start AOS management system.
      admin@aos-server:~$ docker ps -a
      CONTAINER ID        IMAGE                      COMMAND                  CREATED              STATUS              PORTS               NAMES
      8bc9c1dd7a3a        aos:3.3.0-660              "/usr/bin/aos_launch…"   About a minute ago   Up About a minute                       iba141638ea
      b0191320d2bd        aos:3.3.0-660              "/usr/sbin/aos_launc…"   About a minute ago   Up About a minute                       aos-offbox-172_20_34_8-f
      136736759f45        aos:3.3.0-660              "/usr/sbin/aos_launc…"   About a minute ago   Up About a minute                       aos-offbox-172_20_34_10-f
      00a12eb03ae5        aos:3.3.0-660              "/usr/sbin/aos_launc…"   About a minute ago   Up About a minute                       aos-offbox-172_20_34_11-f
      c9b18cd4f55a        aos:3.3.0-660              "/usr/sbin/aos_launc…"   About a minute ago   Up About a minute                       aos-offbox-172_20_34_7-f
      90f35781d2a0        aos:3.3.0-660              "/usr/sbin/aos_launc…"   About a minute ago   Up About a minute                       aos-offbox-172_20_34_12-f
      f5c2d249176b        aos:3.3.0-660              "/usr/sbin/aos_launc…"   About a minute ago   Up About a minute                       aos-offbox-172_20_34_9-f
      ab6c532a37ad        aos:3.3.0-660              "/usr/bin/aos_launch…"   20 hours ago         Up 2 minutes                            aos_controller_1
      8e6fd8ae8f08        aos:3.3.0-660              "/usr/bin/aos_launch…"   20 hours ago         Up 2 minutes                            aos_metadb_1
      5b6359e21386        aos:3.3.0-660              "/usr/bin/aos_launch…"   20 hours ago         Up 2 minutes                            aos_auth_1
      f665ce206f46        aos:3.3.0-660              "/usr/bin/aos_launch…"   20 hours ago         Up 2 minutes                            aos_sysdb_1
      335dec5fba44        nginx:1.14.2-upload-echo   "nginx -g 'daemon of…"   20 hours ago         Up 2 minutes                            aos_nginx_1
      admin@aos-server:~$

Resetting AOS Database
----------------------
This procedure deletes *all* data on AOS server to a fresh state.

.. code-block:: prompt
      :emphasize-lines: 1,2,3

      admin@aos-server:~$ sudo service aos stop
      admin@aos-server:~$ sudo rm -rf /var/lib/aos/db/*
      admin@aos-server:~$ sudo service aos start
      admin@aos-server:~$

Migrating AOS Server Database
-----------------------------

.. warning::
    If you bring up a new AOS Server with the same IP address as your old AOS
    Server without any configuration, when AOS Device Agents re-register with
    the new AOS Server they will revert to an unconfigured "Quarantined" state.
    You must isolate the new AOS Server from the network while you change its
    IP address, restore the AOS Database and restart the server.

If the user wishes to maintain the same IP address for the new AOS Server, they
will need to bring a new AOS Server VM with the same version as the original AOS
Server, with a temporary IP address. After migrating an ``aos_backup`` to the new
server, the original AOS server will be shut down and the IP address will be changed
to the original IP on the new server. This process is recommended for users using
AOS "onbox" Device System Agents.

If users use a new AOS Server with a new IP address, for AOS "onbox" Device System
Agents, the user will need to manually reconfigure the `aos.conf` file for each
AOS "onbox" Device System Agent. This is not required for AOS "offbox" Device
System Agents.

To migrate an active instance of AOS from one server to another:

#. Backup the original AOS Server using the ``sudo aos_backup`` command and copy the
   snapshot to the new server using a temporary IP on the new AOS Server.

   .. code-block:: prompt
      :emphasize-lines: 1

      admin@aos-server:~$ sudo aos_backup
      ====================================================================
        Backup operation completed successfully.
      ====================================================================
      New AOS snapshot: 2020-07-27_22-49-34
      admin@aos-server:~$

#. Compress and move the snapshot directory to the new AOS Server. This example
   uses the ``scp`` command to copy the file to the new AOS Server using a
   different IP address.

   .. code-block:: prompt
      :emphasize-lines: 1,6,7

      admin@aos-server:~$ sudo tar zcvf aos_backup.tar.gz -C /var/lib/aos/snapshot/ 2020-07-27_22-49-34
      2020-07-27_22-49-34/
      2020-07-27_22-49-34/comment.txt
      2020-07-27_22-49-34/aos_restore
      2020-07-27_22-49-34/aos.data.tar.gz
      admin@aos-server:~$ sudo chown admin:admin aos_backup.tar.gz
      admin@aos-server:~$ scp aos_backup.tar.gz admin@172.20.203.4:
      Apstra Operating System (AOS) Virtual Appliance

      Password:
      aos_backup.tar.gz                                   100%   20MB 140.9MB/s   00:00
      admin@aos-server:~$

#. After the snapshot has been removed from the old AOS Server, disconnect the
   old AOS Server by stopping the AOS service or completely shutting the AOS
   Server VM down.

   .. code-block:: prompt
      :emphasize-lines: 1

      admin@aos-server:~$ sudo service aos stop
      admin@aos-server:~$

#. If the user wants to use the same IP address, they will need to manually
   reconfigure the eth0 interface on the new AOS Server to the IP address of the
   old AOS Server. See
   :doc:`Configuring Static Management IP Address <configure_aos>` for more
   information.

#. On the new AOS Server, uncompress the tar.gz file.

   .. code-block:: prompt
      :emphasize-lines: 1

      admin@aos-server:~$ tar zxvf aos_backup.tar.gz
      2020-07-27_22-49-34/
      2020-07-27_22-49-34/comment.txt
      2020-07-27_22-49-34/aos_restore
      2020-07-27_22-49-34/aos.data.tar.gz
      admin@aos-server:~$

#. Restore the AOS Database on the new AOS Server with the
   snapshot ``aos_restore`` command.

   .. code-block:: prompt
      :emphasize-lines: 1,2

      admin@aos-server:~$ cd 2020-07-27_22-49-34
      admin@aos-server:~/2020-07-27_22-49-34$ sudo bash aos_restore
      [sudo] password for admin:
      ====================================================================
        Backup operation completed successfully.
      ====================================================================
      New AOS snapshot: 2020-07-27_23-07-13
      Stopping aos_sysdb_1      ... done
      Stopping aos_auth_1       ... done
      Stopping aos_controller_1 ... done
      Stopping aos_nginx_1      ... done
      Stopping aos_metadb_1     ... done
      (Reading database ... 110457 files and directories currently installed.)
      Removing aos-compose (3.3.0-658) ...
      Processing triggers for ureadahead (0.100.0-21) ...
      Processing triggers for systemd (237-3ubuntu10.41) ...
      tar: Removing leading `/' from member names
      /etc/aos/aos.conf
      /etc/aos-credential/secret_key
      /var/lib/aos/db/
      /var/lib/aos/db/_AosController-000000005f1f376f-0003998b-checkpoint
      /var/lib/aos/db/_AosSysdb-000000005f1f376d-000a90ba-log-valid
      /var/lib/aos/db/_Main-000000005f1f376f-000569a8-checkpoint
      /var/lib/aos/db/_Central-000000005f1f376e-000da3de-checkpoint-valid
      /var/lib/aos/db/_Central-000000005f1f376e-000da3de-log
      /var/lib/aos/db/_Main-000000005f1f376f-000569a8-log-valid
      /var/lib/aos/db/_AosAuth-000000005f1f376d-000a40ff-log
      /var/lib/aos/db/_Auth-000000005f1f376e-000f2d35-log-valid
      /var/lib/aos/db/_Auth-000000005f1f376e-000f2d35-checkpoint-valid
      /var/lib/aos/db/_Metadb-000000005f1f376d-000cb9a9-checkpoint-valid
      /var/lib/aos/db/_Central-000000005f1f376e-000da3de-checkpoint
      /var/lib/aos/db/_Metadb-000000005f1f376d-000cb9a9-log
      /var/lib/aos/db/_Credential-000000005f1f376e-000d740e-log-valid
      /var/lib/aos/db/_AosAuth-000000005f1f376d-000a40ff-checkpoint-valid
      /var/lib/aos/db/_Metadb-000000005f1f376d-000cb9a9-checkpoint
      /var/lib/aos/db/_Main-000000005f1f376f-000569a8-log
      /var/lib/aos/db/_AosSysdb-000000005f1f376d-000a90ba-checkpoint-valid
      /var/lib/aos/db/_AosController-000000005f1f376f-0003998b-log-valid
      /var/lib/aos/db/_Auth-000000005f1f376e-000f2d35-checkpoint
      /var/lib/aos/db/_AosSysdb-000000005f1f376d-000a90ba-log
      /var/lib/aos/db/_AosSysdb-000000005f1f376d-000a90ba-checkpoint
      /var/lib/aos/db/_AosAuth-000000005f1f376d-000a40ff-log-valid
      /var/lib/aos/db/blueprint_backups/
      /var/lib/aos/db/blueprint_backups/6b90ccfd-a1e0-4473-83e7-d62bce24635f/
      /var/lib/aos/db/blueprint_backups/6b90ccfd-a1e0-4473-83e7-d62bce24635f/47/
      /var/lib/aos/db/blueprint_backups/6b90ccfd-a1e0-4473-83e7-d62bce24635f/47/graph.json.zip
      /var/lib/aos/db/blueprint_backups/6b90ccfd-a1e0-4473-83e7-d62bce24635f/47/graph.md5sum
      /var/lib/aos/db/_Central-000000005f1f376e-000da3de-log-valid
      /var/lib/aos/db/_Auth-000000005f1f376e-000f2d35-log
      /var/lib/aos/db/_Credential-000000005f1f376e-000d740e-log
      /var/lib/aos/db/_Credential-000000005f1f376e-000d740e-checkpoint
      /var/lib/aos/db/_Credential-000000005f1f376e-000d740e-checkpoint-valid
      /var/lib/aos/db/.devpi/
      /var/lib/aos/db/.devpi/server/
      /var/lib/aos/db/.devpi/server/.nodeinfo
      /var/lib/aos/db/.devpi/server/.secret
      /var/lib/aos/db/.devpi/server/.sqlite
      /var/lib/aos/db/.devpi/server/.serverversion
      /var/lib/aos/db/.devpi/server/.event_serial
      /var/lib/aos/db/_AosController-000000005f1f376f-0003998b-log
      /var/lib/aos/db/_Main-000000005f1f376f-000569a8-checkpoint-valid
      /var/lib/aos/db/_Metadb-000000005f1f376d-000cb9a9-log-valid
      /var/lib/aos/db/_AosAuth-000000005f1f376d-000a40ff-checkpoint
      /var/lib/aos/db/_AosController-000000005f1f376f-0003998b-checkpoint-valid
      /var/lib/aos/anomaly/
      /var/lib/aos/anomaly/_Anomaly-000000005f1f36a4-000aaa68-checkpoint-valid
      /var/lib/aos/anomaly/_Anomaly-000000005f1f331b-0000e8eb-checkpoint
      /var/lib/aos/anomaly/_Anomaly-000000005f1f376f-00002176-checkpoint
      /var/lib/aos/anomaly/_Anomaly-000000005f1f376f-00002176-log
      /var/lib/aos/anomaly/_Anomaly-000000005f1f331b-0000e8eb-log
      /var/lib/aos/anomaly/_Anomaly-000000005f1f2abc-0000a867-log
      /var/lib/aos/anomaly/_Anomaly-000000005f1f331b-0000e8eb-checkpoint-valid
      /var/lib/aos/anomaly/_Anomaly-000000005f1f2abc-0000a867-checkpoint
      /var/lib/aos/anomaly/_Anomaly-000000005f1f36a4-000aaa68-checkpoint
      /var/lib/aos/anomaly/_Anomaly-000000005f1f376f-00002176-log-valid
      /var/lib/aos/anomaly/_Anomaly-000000005f1f36a4-000aaa68-log
      /var/lib/aos/anomaly/_Anomaly-000000005f1f331b-0000e8eb-log-valid
      /var/lib/aos/anomaly/_Anomaly-000000005f1f2abc-0000a867-checkpoint-valid
      /var/lib/aos/anomaly/_Anomaly-000000005f1f2abc-0000a867-log-valid
      /var/lib/aos/anomaly/_Anomaly-000000005f1f36a4-000aaa68-log-valid
      /var/lib/aos/anomaly/_Anomaly-000000005f1f376f-00002176-checkpoint-valid
      /opt/aos/aos-compose.deb
      /opt/aos/frontend_images/
      /opt/aos/frontend_images/aos-web-ui.zip
      Selecting previously unselected package aos-compose.
      (Reading database ... 110440 files and directories currently installed.)
      Preparing to unpack /opt/aos/aos-compose.deb ...
      Unpacking aos-compose (3.3.0-658) ...
      Setting up aos-compose (3.3.0-658) ...
      Processing triggers for ureadahead (0.100.0-21) ...
      Processing triggers for systemd (237-3ubuntu10.41) ...
      Starting aos_nginx_1      ... done
      Starting aos_sysdb_1      ... done
      Starting aos_controller_1 ... done
      Starting aos_metadb_1     ... done
      Starting aos_auth_1       ... done
      admin@aos-server:~/2020-07-27_22-49-34$

#. The ``aos_restore`` command automatically starts the AOS Service when it is
   finished restoring the AOS Database. Verify the AOS Server is running.

   .. code-block:: prompt
      :emphasize-lines: 1

      admin@aos-server:~/2020-07-27_22-49-34$ service aos status
      * aos.service - LSB: Start AOS management system
         Loaded: loaded (/etc/init.d/aos; generated)
         Active: active (exited) since Mon 2020-07-27 20:23:09 UTC; 2h 45min ago
           Docs: man:systemd-sysv-generator(8)
          Tasks: 0 (limit: 4915)
         CGroup: /aos.service
      admin@aos-server:~/2020-07-27_22-49-34$

#. Verify that your devices are online in their "Active" state.

   .. image:: static/images/backup_and_restore/devices_active.png

Resetting AOS Server VM Password
================================

.. sidebar:: Your AOS Fabric is safe.

      You'll need to reboot the AOS Server VM, but any deployed AOS Fabric
      will not be impacted.

If you lose your **admin** password for the AOS Server VM,
and *you still have console access to the AOS Server VM*,
you can reset the password.

#. Attach to the AOS Server console and send a "reset" signal to the VM.
   To access the GRUB menu, immediately press the **esc** or **shift** key
   in the console on reboot.

   .. image:: static/images/troubleshooting/230_aos-vm-password-recovery1.png

#. Select **Advanced options for Ubuntu**. For AOS 2.3 and later,
   use username **admin** and password **apstra**.

   .. image:: static/images/troubleshooting/230_aos-vm-password-recovery2.png

3. At the next GRUB menu, select the first **(recovery mode)** option.

   .. image:: static/images/troubleshooting/230_aos-vm-password-recovery3.png

4. From the **Recovery Menu**, select **root**, then press **Enter** to enter
   a root shell prompt.

   .. image:: static/images/troubleshooting/230_aos-vm-password-recovery5.png

5. At the root shell prompt enter ``mount -o rw,remount /`` then ``passwd admin``
   to reset the password for the default **admin** user for the AOS Server
   VM Linux CLI.

6. Enter ``sync`` then ``reboot`` to reboot the VM.

   .. image:: static/images/troubleshooting/230_aos-vm-password-recovery4.png

7. After reboot, you can log into the AOS Server VM Linux CLI as user **admin**
   with the new password.
