=================================
Install AOS Server on VMware ESXi
=================================

#. Log into vCenter.

   .. image:: static/images/server_installation/esxi_login.png

#. Right-click your target deployment environment, and click
   **Deploy OVF Template**.

   .. image:: static/images/server_installation/esxi_deploy.png

#. Click **Choose Files** and locate the AOS Installation OVF file.

   .. image:: static/images/server_installation/esxi_browse.png

#. Specify **Unique Name and Target Location** for the VM.

   .. image:: static/images/server_installation/esxi_name.png

#. Apstra recommends Thick Provisioning for the AOS Server.

   .. image:: static/images/server_installation/esxi_datastore.png

#. Map the AOS Management network to enable it to reach the virtual networks
   that AOS will manage on ESXi.

   .. image:: static/images/server_installation/esxi_network.png

#. Verify that all details are correct, then click **Finish**.
   When the VM is up and running you are ready to
   :doc:`Configuring AOS Server <configure_aos>`.

   .. image:: static/images/server_installation/esxi_final.png
