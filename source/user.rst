====
User
====
As a user, you can manage your own password, name, email and favorites.

.. image:: static/images/user/user_profile_330.png

Profile
=======

Changing Your Profile Password
------------------------------
#. From any page in the AOS web interface, click your username (bottom-left),
   then click **Profile** to see your profile page.
#. Click the **Change Password** button (top-right), enter your current password,
   then enter your new password, twice.
#. Click **Change Password** to update your password and return to your profile.

Changing Your Profile Name/Email
--------------------------------
#. From any page in the AOS web interface, click your username (bottom-left),
   then click **Profile** to see your profile page.
#. Click the **Edit** button (top-right), then change your name and/or email,
   as applicable.
#. Click **Save** to update your details and return to your profile.

Managing Favorites
------------------
From any page in the AOS web interface, click your username (bottom-left),
then click **Profile** to see your profile page including all the pages that
have been saved as favorites.

* To access a favorite, click its link.
* To change the name of a link, click the **Edit label** button, change the
  name, then click **Update**.
* To remove a page from your favorites list, click the **Remove** button
  (trash can), then click **Delete**.

Log Out
=======
From any page in the AOS web interface, click your username (bottom-left),
then click **Log Out**.
