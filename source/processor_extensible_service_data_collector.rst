============================================
Processor: Extensible Service Data Collector
============================================
The Extensible Service Data Collector processor collects data supplied by a
custom service that is not 'lldp', 'bgp' or 'interface'.

**Input Types** - No inputs. This is a source processor.

**Output Types** - NSTS, DSSTS

**Properties**

   .. include:: includes/processors/data_type.rst

   .. include:: includes/processors/graph_query.rst

.. _ingestion_filter:

   Ingestion filter (ingestion_filter)
      New (reserved) key. Ingestion filter determines what metrics from the
      collector make it into the probe. As of AOS version 3.0, we support a
      degenerate case of ingestion filter, that is, probe specifies full
      identities of all metrics that need to be ingested. With this feature, you
      can ingest metrics that satisfy a criterion that is expressed using an
      ingestion filter.

      Ingestion filter is authored by probe authors, evaluated by AOS server
      component that is responsible for ingesting raw telemetry into stage outputs
      within the probes. It is also propagated as a collection filter to the
      telemetry collector plugins.

      Keys available to express in the filter are same as the metric identity keys.

      * No metric identity key can exist directly under "properties". AOS should
        raise validation error if any metric identity key is mistakenly specified
        directly under properties.

      * Any missing metric identity key under "ingestion_filter" is assumed to
        match.

      * Only explicitly specified keys under "ingestion_filter" can be referenced
        by the rest of the probe configuration. This is to enhance probe
        readability and allow AOS to provide better overall validation.

      * The data_type must be one of the table data types.

      * Existing reserved key "keys" is now made optional and can be omitted.
        The key names should exactly match those specified in the schema of the
        corresponding service definition.

   Keys (keys)
      List of keys that are significant for specifying data elements for this
      service
   Query Group by (query_group_by)
      List (of strings) of node and relationship names used in the graph query to
      group query results by. Each element in this list represents a named node or
      relationship matcher in the ``graph_query`` field.It is not an expression
      to be consistent with existing ``group_by`` field in grouping processors.
      Non-expression is simple and more intuitive.

      When grouping is active (``query_group_by`` is not null), query results are
      d by the specified list of names, where one output item is created per
      each group. In this case, the expressions can only access matcher names
      specified in ``query_group_by`` and the query results for each group are
      accessed using a new ``group_items`` variable. The ``group_items`` variable
      is a list of query results, where each result has named nodes/relationships,
      not present in ``query_group_by``.

      The following table describes the behavior for various values of this field:

      +-------------------------+----------------------------------------------+
      | Value of query_group_by | Semantics                                    |
      | field                   |                                              |
      +-------------------------+----------------------------------------------+
      |                         | No grouping is done. This is equivalent to   |
      | Omitted or provided as  | current behavior of                          |
      | json null               | extensible_data_collector.                   |
      | (ala None in Python)    | Using ‘group_items’ in this case is not      |
      |                         | permitted and results in probe error state.  |
      +-------------------------+----------------------------------------------+
      | Empty list ([])         | Produces one group containing all the query  |
      |                         | results.                                     |
      +-------------------------+----------------------------------------------+
      | One or more             | The query results are grouped by the         |
      | matcher names           | specified nodes or relationships. If this    |
      |                         | list covers all available matchers in the    |
      |                         | query, the number of groups is equal to the  |
      |                         | number of query results.                     |
      +-------------------------+----------------------------------------------+

   .. include:: includes/processors/value_map.rst

   .. include:: includes/processors/service_name.rst

   .. include:: includes/processors/system_id.rst

   Execution count
      Number of times the data collection is done.

   .. include:: includes/processors/service_input.rst

   .. include:: includes/processors/service_interval.rst

   .. include:: includes/processors/additional_keys.rst

   .. include:: includes/processors/enable_streaming.rst
