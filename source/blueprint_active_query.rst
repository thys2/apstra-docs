=====
Query
=====
You can search for MAC addresses, IP addresses and VMs by using the
query feature in the active blueprint.

.. image:: static/images/blueprints/active/query/query_overview_330.png

Searching the Active Blueprint
==============================
#. From the blueprint, navigate to **Active / Query**.
#. Click **MAC**, **ARP**, or **VMs** depending on your query.
#. Click **Query:All**, enter search criteria, and click **Apply** to see results.
