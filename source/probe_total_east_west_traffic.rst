=============================
Total East/West Traffic Probe
=============================

.. image:: static/images/iba/eastwest.svg

Found at /predefined_probes/eastwest_traffic

It first identifies all the server facing interfaces and all external router facing
interfaces. It then collects samples for each, generates time series and calculates
average traffic across configurable time interval. It then perform accumulation to
calculate total server traffic, total external router traffic (south-north) and
derives total east-west traffic as the difference between the two. It then provides
time series showing how these three totals changed over time.


"leaf server traffic counters" processor
----------------------------------------

Purpose: wires in interface traffic samples (measured in bytes per second) for
traffic received on leafs from the servers

Outputs of "leaf server traffic counters" processor
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

'server_traffic_counters': set of traffic samples (for each server facing
interface on each leaf) in the receive direction. Each set member has the
following keys to identify it: system_id (id of the leaf system,
usually serial number), interface (name of the interface).

"server traffic average" processor
----------------------------------

Purpose: Calculate average server traffic during period specified by
average_period facade parameter. Unit is bytes per second.

Outputs of "server traffic average" processor
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

'server_traffic_avg': set of traffic average values (for each server
facing interface on each leaf) in the receive direction. Each set member has
the following keys to identify it: system_id (id of the leaf system,
usually serial number), interface (name of the interface).

"external router south-north link traffic" processor
----------------------------------------------------

Purpose: wires in interface traffic samples (measured in bytes per second) for
traffic sent to external routers

Outputs of "external router south-north link traffic" processor
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

'external router south-north links traffic average': set of traffic samples
(for each external router facing interface on each device). Each set member
has the following keys to identify it: system_id (id of the system,
usually serial number), interface (name of the interface).


"external router south-north links traffic average" processor
-------------------------------------------------------------

Purpose: Calculate average traffic for each interface facing external
router traffic during period specified by average_period facade parameter.
Unit is bytes per second.

Outputs of "external router south-north links traffic average" processor
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

'ext_router_interface_traffic_avg': set of traffic average values (for each
external router facing interface on each device). Each set member has
the following keys to identify it: system_id (id of the leaf system,
usually serial number), interface (name of the interface).

"total server traffic" processor
--------------------------------

Purpose: Calculate total server traffic by summing average traffic on each
interface attached to servers, in receive direction. Unit is bytes per second.

Outputs of "total server traffic" processor
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

'total_server_traffic': total server traffic average in bytes per second.

"server generated traffic average" processor
--------------------------------------------

Purpose: Calculate total average server traffic over average_period seconds,
which is a facade parameter. Unit is bytes per second.

Outputs of "server generated traffic average" processor
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

'total_server_generated_traffic_average': total server traffic average in
bytes per second.

"total server traffic history" processor
-----------------------------------------

Purpose: create recent history time series out showing how total average server
traffic changed over time. In terms of the number of samples, the time series
will hold history_sample_count values (facade parameter). Unit is bytes per second.

Outputs of "total server traffic history" processor
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

total_server_traffic_history: time series showing total average server traffic
over recent history. Unit is bytes per second.

"south-north traffic" processor
-------------------------------

Purpose: Calculate total traffic by summing average traffic on each interface
facing external router. Unit is bytes per second.

Outputs of "south-north traffic" processor
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

'total_outgoing_traffic': total south-north traffic average in bytes per second.



"outgoing_traffic_average" processor
------------------------------------

Purpose: Calculate total south-north traffic over average_period seconds,
which is a facade parameter. Unit is bytes per second.

Outputs of "outgoing_traffic_average" processor
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

'total_outgoing_traffic_average': total south-north traffic average in
bytes per second.

"south-north traffic history" processor
---------------------------------------

Purpose: create recent history time series showing how total average south-north
traffic changed over time. In terms of the number of samples, the time series
will hold history_sample_count values (facade parameter). Unit is bytes per second.

Outputs of "south-north traffic history" processor
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

total_outgoing_traffic_timeseries: time series showing total average server traffic
over recent history. Unit is bytes per second.

"east-west traffic" processor
---------------------------------------

Purpose: Subtract south-north traffic from total server traffic to obtain total
east-west traffic average, in bytes per second.

Outputs of "east-west traffic" processor
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

'eastwest_traffic': Total average east-west traffic over last average_period
seconds, in bytes per second.

"east-west traffic history" processor
---------------------------------------

Purpose: create recent history time series showing how total average east-west
traffic changed over time. In terms of the number of samples, the time series
will hold history_sample_count values (facade parameter). Unit is bytes per second.

Outputs of "east-west traffic history" processor
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

eastwest_traffic_history: time series showing how total average east-west traffic
changed over recent history. Unit is bytes per second.
