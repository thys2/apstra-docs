====================================================
Hypervisor MTU Threshold Check Probe (Virtual Infra)
====================================================

Purpose
   Detect virtual infra interfaces with maximum transmission units (MTU) below a
   specified threshold (default: 1600).

Source Processor
   Interface MTU (:doc:`generic graph collector <processor_generic_graph_collector>`)
      output stage: Interface MTU (number set) (generated from graph)

Additional Processor(s)
   MTU below threshold (:doc:`range <processor_range>`)
      input stage: Interface MTU

      output stage: MTU below threshold (discrete state set)

Example Usage
   **NSX Integration** - To carry VXLAN-encapsulated overlay traffic, an MTU
   greater than 1600 is recommended. AOS detects when NSX-T transport nodes
   connected to ToR leafs are below the specified threshold.

   To support Geneve encapsulation, the MTU configuration on NSX-T nodes involved
   in an overlay transport zone must have a valid MTU setting on the ESXi host.
   The image below shows hypervisors with the MTU above the threshold.

   .. image:: static/images/nsx/MTU_Above_Threshold.png

   If any of the hypervisors were below the threshold, the expected value would
   change to **true** and an anomaly would be raised.
