=============================
Install AOS Server on Hyper-V
=============================
Kindly ensure Microsoft Hyper-V Manager is available in the Hyper-V systems
based environment. These instructions are based on using Hyper-V Manager on a
Windows Server 2016 Datacenter Edition:

#. In **Hyper-V Manager**, in the **Actions** panel, click **New**, then click
   **Virtual Machine**.

   .. image:: static/images/server_installation/Hyper-V_new_VM.png

#. Specify the VM name.

   .. image:: static/images/server_installation/Hyper-V_AOS_VM.png

#. Specify the generation type as **Generation1**.

   .. image:: static/images/server_installation/Hyper-V_AOS_VM_Generation.png

#. Provide the required vCPU and RAM values. The example below allocates 2vCPU
   and 8 GB RAM. However, in production the VM resources should be allocated
   based on :doc:`AOS Server Resource Requirements <server_requirements>`.

   .. image:: static/images/server_installation/Hyper-V_AOS_VM_resource.png

   .. image:: static/images/server_installation/Hyper-V_AOS_VM_vCPU.png

#. To set up a network environment, select **Virtual Switch Manager** and create
   the virtual switch. Under VM settings select the appropriate virtual switch
   under Hyper-V network adapter. This should help the AOS server connect on the
   management network to the devices managed by it. In the example below, the
   management network is residing on Internal Network **Virtual Switch**. However,
   in production setup, select the appropriate virtual switch as per your
   deployment environment.

   .. image:: static/images/server_installation/Hyper-V_AOS_VM_vswitch.png

   .. image:: static/images/server_installation/Hyper-V_AOS_VM_vswitch_internal.png

   .. image:: static/images/server_installation/Hyper-V_AOS_VM_network_adapter.png

#. Locate and select the disk image in the **VHD** or **VHDX** format on your
   computer:

   .. image:: static/images/server_installation/Hyper-V_AOS_VM_disk_image.png

#. When the VM is up and running you are ready to
   :doc:`configure AOS <configure_aos>` Open a console to it using Hyper-V Manager.
   Docker daemon is found running properly on the AOS server after
   configuring the AOS server is completed.

   .. image:: static/images/server_installation/Hyper-V_AOS_VM_docker_status.png
