===================================
AOS Device Agent Configuration File
===================================

Controller Section
==================

.. code-block:: ini

    [controller]
    # <metadb> provides directory service for AOS. It must be configured properly
    # for a device to connect to AOS controller.
    metadb = tbt://aos-server:29731
    # Use <web> to specify AOS web server IP address or name. This is used by
    # device to make REST API calls to AOS controller. It is assumed that AOS web
    # server is running on the same host as metadb if this option is not specified
    web =
    # <interface> is used to specify the management interface.This is currently
    # being used only on server devices and the AOS agent on the server device will
    # not come up unless this is specified.
    interface =

metadb
------
Agent Server Discovery is a client-server model.  The AOS Device agent registers
directly to the AOS server via the ``metadb`` connection. The AOS server can be
discovered from static IP or DNS.

**Dynamic DNS** - By default, AOS device agents point to the DNS entry
**aos-server**, relying on dhcp-provided DNS resolution and hostname resolution.
On the AOS server, if the *metadb* connection entry points to a DNS entry, then
the AOS agents must be able to resolve that DNS entry as well.
DNS must be configured so aos-server resolves to an interface on the AOS
server itself, and so the agents are configured with
``metadb = tbt://aos-server:29731``

**Static DNS** - We can add a static DNS entry pointing directly to the IP of
aos-server. Add a static DNS entry, or use a DNS Nameserver configuration on the
device.

.. code-block:: text
    :caption: Arista and Cisco static hostname

    localhost(config)#ip host aos-server 192.168.25.250


.. code-block:: text
    :caption: Cumulus and Linux static hostnames ``/etc/hosts``

    192.168.25.250 aos-server

.. code-block:: text
    :caption: Obtaining the IP from the AOS Server

    admin@aos-server:~# ip addr show dev eth0
    2: eth0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc pfifo_fast state UP qlen 1000
        link/ether 08:00:27:8a:39:05 brd ff:ff:ff:ff:ff:ff
        inet 192.168.59.250/24 brd 192.168.59.255 scope global eth0
        inet6 fe80::a00:27ff:fe8a:3905/64 scope link
        valid_lft forever preferred_lft forever

Then the agents will be configured with ``metadb = tbt://aos-server:29731``

web
---
In a future release, the AOS REST API will be able to run on a separate server
from the AOS server itself. This feature is for Apstra internal usage only.

interface
---------
The device agent source interface applies to Linux servers only (Ubuntu, CentOS).
This source IP is the server interface that the device agent uses when registering
with AOS. For example, on a server, to bind the device agent to *eth1* instead of
the default *eth0*, specify ``interface = eth1``.

Service Section
===============

.. code-block:: ini

    [service]
    # AOS device agent by default starts in "telemetry-only" mode.Set following
    # variable to 1 if you want AOS agent to manage the configuration of your
    # device.
    enable_configuration_service = 0
    # When managing device configuration AOS agent will restore backup config if it
    # fails to connect to AOS controller in <backup_config_restoration_timeout>,
    # specified as <hh:mm:ss>. Set it to 00:00:00 to disable backup restoration
    backup_config_restoration_timeout = 00:00:00

The service section manages specific agent configuration related to configuration
rendering and telemetry services.

enable_configuration_service
----------------------------
This field specifies the operation mode of the device agent: telemetry only or
full control.

``enable_configuration_service = 0`` Leaving the default value of ``0``
allows AOS to push telemetry (alerts) only. Configuration files are not modified.
This ensures that AOS does not modify any configurations unless specified by the
network administrator.

``enable_configuration_service = 1`` Setting this field to ``1`` allows AOS to
fully manage the device agent configuration, including pushing discovery and full
intent-based configuration.

backup_config_restoration_timeout
---------------------------------
By design, AOS does not *store* configuration on the device. This prevents a
device from booting up and immediately participating in fabric that may not be
properly configured yet. Following system startup, after the discovery phase
completes, AOS configures the AOS device agent.

``backup_restoration_timeout = 00:00:00`` This disabled state (default) keeps the
AOS device agent from replacing the running configuration if it cannot contact the
AOS server. Any previous configuration state will not be restored.

``backup_restoration_timeout = 00:15:00`` Any value other than the default
``00:00:00`` enables the AOS agent to boot and replace the running configuration
with the most known previous state after the specified period of time
(fifteen minutes in this example). Specifically, the files from /.aos/rendered/
are restored to the system after the configuration restore period expires.

Logrotate Section
=================

.. code-block:: ini

    [logrotate]

    # AOS has builtin log rotate functionality. You can disable it by setting
    # <enable_log_rotate> to 0 if you want to use linux logrotate utility to manage
    # your log files. AOS agent reopens log file on SIGHUP
    enable_log_rotate = 1
    # Log file will be rotated when its size exceeds <max_file_size>
    max_file_size = 1M
    # The most recent <max_kept_backups> rotated log files will be saved. Older
    # ones will be removed. Specify 0 to not save rotated log files, i.e. the log
    # file will be removed as soon as its size exceeds limit.
    max_kept_backups = 5
    # Interval, specified as <hh:mm:ss>, at which log files are checked for
    # rotation.
    check_interval = 1:00:00

AOS logs to the ``/var/log/aos`` folder under a series of files. AOS implements
its own method of log rotation to prevent /var/log/aos from filling up. Log
rotation can be enabled ``1`` or disabled ``0``.  Each individual log file is
rotated when it approaches the appropriate maximum size. Log rotation occurs by
default every hour.

Device Info Section
===================

.. code-block:: ini

    [device_info]
    # <model> is used to specify the device's hardware model to be reported to AOS
    # device manager. This is only used by servers, so can be ignored for non-
    # server devices such as switches. By default a server reports "Generic Model"
    # which matches a particular HCL entry's selector::model value in AOS. Specify
    # another model for the server to be classified as a different HCL entry.
    model = Generic Model

model
-----
The device info section is used to modify the default device model of servers as
they register to AOS. For example, ``Server 2x10G`` changes the server to a
dual-attached L3 server. All valid options for ``model`` include:

* Generic Model
* Server 2x10G
* Server 1x25G
* Server 1x40G
* Server 4x10G

Device Profile Section
======================

.. code-block:: ini

   # <device_profile_id> is used to specify the device profile to be associated to
   # the device. Selector in the specified device profile should match the
   # reported device facts.
   device_profile_id =
   [credential]
   username = admin
