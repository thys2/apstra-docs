================
Virtual Networks
================

Virtual Networks (VN) are collections of L2 forwarding domains.  In AOS managed
fabric, a Virtual Network can be constructed using either VLANs or VXLANs.

VLAN (Rack-local VN)
  - A scope of a single rack
  - Can be either a single leaf or leaf pair
  - Can be deployed in L2-only mode or with a L3-gateway (SVI) IP address hosted
    on the rack leaf

VXLAN (Inter-rack VN)
  - Fabric-wide scope for ubiquitous L2
  - Can be combination of single rack leaf or leaf pair (MLAG)
  - Deployed in L2-only mode
  - L3-gateway functionality

.. sidebar:: AOS API Samples

   For AOS API samples for creating AOS VNs with server endpoints, see
   https://github.com/Apstra/aos-api-samples

Refer to the the :doc:`AOS Feature Matrix <feature_matrix>` for complete AOS
Virtual Networks feature compatibility for supported Network Operating Systems (NOS).

Virtual Networks are managed on a per Blueprint basis.

**To access virtual networks** - from the blueprint, navigate to
**Staged / Virtual / Virtual Networks**.

.. image:: static/images/blueprint_virtual_tasks/blueprint_virtual_tasks_5_310.png

Creating Virtual Networks
-------------------------
A VLAN requires a name, VLAN ID, and an optional specification of Layer3 intent.
A VLAN based virtual network can be deployed in a pure L2-mode (eg, isolated cluster
networks for database replication), or with L3 (SVI provisioned) with first-hop
redundancy. AOS also supports DHCP Relay which is configured under
:doc:`Security Zones <security_zones>`.

A VXLAN requires a name and a VNID (defined below) and an optional specification of
Layer3 intent.

Create Single Rack-local VLAN Based Virtual Network
```````````````````````````````````````````````````

#. From the blueprint, navigate to **Staged / Virtual / Virtual Networks**, then
   click **Create Virtual Networks**.

   .. image:: static/images/blueprint_virtual_tasks/300_virtual-networks-2.png

#. Fill in the required information

   * **Type**
       **Rack-local** for VLAN Based Virtual Networks, **Inter-rack** for
       VXLAN/EVPN based Virtual Network.

   * **Name**
       32 characters or fewer. Underscore, dash and alphanumeric characters only.

   * **Security Zone**
       VLAN Based "rack-local" Virtual Networks are currently only allowed in the
       **Default** Security Zone.

   * **Default VLAN ID**
       The L2 VLAN ID on the switch the virtual network is assigned. This can
       either be explicitly assigned or left blank to have AOS auto-assign from a
       static pool with the range of 2-4094.

       .. note::

          Using VLAN ID 1 is supported, but will need to be explicitly assigned.
          However, best practices do not recommend using VLAN ID 1 (default) for
          any active virtual network.

       .. note::

          Different network device vendors have varying requirements for "reserved"
          VLAN ID ranges. Cumulus VLAN-aware Bridge Mode reserves a VLAN ID range
          by default from 3000 to 3999. Cisco NXOS reserves VLAN ID range from
          3968 to 4094. Arista, by default, will use a VLAN ID range from 1006 to
          4094 for internal VLANs for routed ports.

       .. note::

          For Arista EOS devices, the user can modify the "reserved" VLAN ID range
          with the EOS ``vlan internal allocation policy`` configuration command.

          .. code-block:: text

              l2-virtual-ext-002-leaf1(config)#vlan internal allocation policy ascending range 3001 3999
              l2-virtual-ext-002-leaf1(config)#exit
              l2-virtual-ext-002-leaf1#show vlan internal allocation policy
              Internal VLAN Allocation Policy: ascending
              Internal VLAN Allocation Range: 3001-3999
              l2-virtual-ext-002-leaf1#

          This EOS configuration can be applied to all EOS devices using AOS
          System Configlets **prior** to the configuration and deployment of Virtual
          Networks.

   * **Enable DHCP Service**
       If set to 'Enabled', and the blueprint template has DHCP enabled, a DHCP
       relay forwarder will be configured on the SVI. This option also implies L3
       routing on this SVI.

   * **IPv4 Connectivity**
       Enables SVI IPv4 routing features for this Virtual Network.

   * **IPv4 Subnet**

       If **IPv4 Connectivity** is enabled, this is the IP address range used on
       this Virtual Network. This can be explicitly set with an IPv4 subnet (e.g.
       192.168.100.0/24), an IPv4 CIDR length (e.g. /26) for auto-assignment of a
       subnet with the specified length or left blank for auto-assignment of a /24
       subnet network. For auto-assignment, the IP is automatically derived from
       the assigned Virtual Networks SVI Pool (more on this described below).

       .. note::

           If creating multiple "batch" VLAN networks, you can only leave
           **IPv4 Subnet** blank or specify a IPv4 CIDR length (e.g. /26).

   * **IPv6 Connectivity**
       Enables SVI IPv6 routing features for this Virtual Network.

       .. note::

          This option is only available if support for IPv6 Applications is enabled.
          Also, if the Template used for the Blueprint used Spine to Leaf Links
          Type of "IPv4" only, then IPv6 cannot be used in the Default Security Zone
          and VLAN Virtual Networks.

   * **IPv6 Subnet**

       If **IPv6 Connectivity** is enabled, this is the IP address range used on
       this Virtual Network. This can be explicitly set with an IPv4 subnet (e.g.
       2001:4de0::/64), an IPv6 CIDR length (e.g. /56) for auto-assignment of a
       subnet with the specified length or left blank for auto-assignment of a /64
       subnet network. For auto-assignment, the IP is automatically derived from the
       assigned Virtual Networks SVI Pool (more on this described below).

       .. image:: static/images/blueprint_virtual_tasks/300_virtual-networks-3.png

       .. note::

          If creating multiple "batch" VLAN networks, you can only leave
          **IPv6 Subnet** blank or specify a IPv6 CIDR length (e.g. /56).

   * **Default Endpoint Types**
       This allows you to set all endpoints (servers) to a common VLAN
       type (**Unassigned**, **Untagged**, **VLAN Tagged**) when the VN is created.
       This configuration is not persistent and can be changed by editing the VN
       endpoint configuration.

   * **Assigned To**
       Select the rack the VN is to be assigned to. For 5-stage Clos networks, the
       PODs are shown, and the Leaf devices within each POD can be selected to
       extend the VN to those devices. For MLAG racks, the leaf pair is shown. If
       you select more than one rack, AOS automatically creates multiple
       "Rack-local" VLAN Based Virtual Networks via the "Batch" process described
       below. Each rack has option to specify the **VLAN ID** (used for "Batch VN
       Creation") and **IPv4** field used for racks to set the first-hop-redudancy
       IP address for the SVI (VRRP, VARP, etc). If left blank AOS auto-assigns the
       MLAG SVI from the SVI pool selected:

       .. image:: static/images/blueprint_virtual_tasks/300_virtual-networks-4.png

#. Click **Create** to stage the new virtual network and return to the list view.
#. If IPv4 or IPv6 resources are required for Network SVI Subnets you must assign
   resources, from the virtual networks build section, before the staged blueprint
   can be committed. (Prior to AOS version 3.3.0, SVI subnets were assigned from
   the security zone build section.)

VLAN Network Endpoints
``````````````````````

To modify Endpoints (Servers), click the 'Manage Endpoints' button. You can edit
the Endpoints in either a "Topology" or "Node" mode. "Topology" mode shows a
graphical representation of the VN. "Nodes" mode shows a list of the node in
the rack.

In the "Topology" mode you can click on the Endpoint to cycle through the
available Endpoint modes **Unassigned** (grey), **VLAN Tagged** (blue-green) or
**Untagged** (purple).

.. image:: static/images/virtual_networks/virtual_network_vlan_manage_endpoints_1_22.png

In the "Nodes" mode you can click on the Endpoint mode to cycle through the
available Endpoint modes **Unassigned** (grey), **VLAN Tagged** (blue-green) or
**Untagged** (purple).

.. image:: static/images/virtual_networks/virtual_network_vlan_manage_endpoints_2_22.png

For VLAN based Virtual Networks only one "untagged" VLAN may be assigned per
endpoint, since a switch port cannot have two native VLANs. If the server is
dual-attached, a port-channel (bond) and MLAG configuration will be generated
automatically. A server facing port-channel (bond) configuration will not be
created until a Virtual Network is created for the endpoint.

All "tagged" VLANs are 802.1q encapsulated towards the server.

Add or remove a Virtual Network from an interface
`````````````````````````````````````````````````

To add or remove a Virtual Network (new or existing) to an interface or
port-channel we need to identify in a first instance the interfaces (physical
switch ports in case of port-channels too) from the Topology tab.

   .. image:: static/images/virtual_networks/add_vlan_port_1.png

Then, from Staged --> Virtual --> Virtual Networks, we select the VN that we want
to add to the port-channel or we create a new one using the "Create Virtual
Networks" option.

 If this is a new VLAN we specify where this VLAN will be configured, in what
 switch/rack and if we desire, we can assign it to the server facing ports too
 by selecting VLAN Tagged (if it is a trunk) or Untagged (if it is an access port)
 in the "Server Logical Link" section (see capture below), or if we prefer to
 this later and select specific ports we can leave it as "Unassigned".

in this case, we will configured in the rack_1_001 leaf pair, and the VLAN ID
is 60. We will leave it as unassigned so we can apply it to our port-channel
in a different step:

   .. image:: static/images/virtual_networks/add_vlan_port_2.png

After the VLAN has been created, click the VLAN name (the same applies if
this VLAN was already defined before):

Now apply it to the desired port:

- click on the switch port as in the capture
- select the leaf pair
- Click on VLAN Tagged to assign it to a port-channel with more VLANs (trunk)

   .. image:: static/images/virtual_networks/add_vlan_port_4.png

Finally, commit the change.

You can use "Revert" rather than "Commit" if you are not happy with the change,
or once committed, you can restore the previous setup from the "Time Voyager" tab
(this will restore your blueprint to a previous state).

To DELETE the VLAN from that port, proceed in the same way by clicking on
"Unassigned" instead of "VLAN tagged" as in the example above on the actual ports.


Create Inter-rack VXLAN/EVPN based Virtual Networks
```````````````````````````````````````````````````

Depending on the control plane selected when configuring the Template, either
Static VXLAN Routing or MP-EBGP EVPN, you can configure Inter-rack Virtual
Networks. MP-EBGP EVPN provides a control plane for VXLAN routing. References to
VXLAN in this documentation refers to either type of control plane.

.. note::

    The VXLAN/EVPN capabilities for Inter-rack Virtual Networks are highly
    dependent on the Make and Model of the Network Devices being used. Refer to the
    the :doc:`AOS Feature Matrix <feature_matrix>` and
    :ref:`evpn_support_addendum:AOS EVPN Support Addendum` for information. Contact
    your Network Device Vendor or Apstra Support if you need detailed capability
    information for your device.

#. From the blueprint, navigate to **Staged / Virtual / Virtual Networks**, then
   click **Create Virtual Networks**.

   .. image:: static/images/blueprint_virtual_tasks/300_virtual-networks-8.png

#. Fill in the required information

   * **Type**
       **Rack-local** for VLAN Based Virtual Networks, **Inter-rack** for
       VXLAN/EVPN based Virtual Network.

   * **Name**
       32 characters or fewer. Underscore, dash and alphanumeric characters only.

   * **Security Zone**
       This is the Security Zone the VN is associated with. For VN with Layer3 SVI,
       the SVI will be associated with a VRF for each Security Zone isolating the VN
       SVI from other VN SVI in other Security Zones. If you do not select a
       Security Zone, AOS assigns the new VN to the Default Security Zone.

   * **VNI ID**
       The L2 VXLAN ID on the switch the virtual network is assigned. This can be
       explicitly assigned or you may leave this blank and let AOS auto-assign a
       VNID from a VNID pool created in the **Resources** tab. You can batch
       create up to 40 virtual networks by entering their VNI IDs in this field
       (ranges or individual VNI IDs separated by commas, e.g. 5555-5560, 7777).
       If more than 40 virtual networks are required, we recommend committing the
       first 40 VNs before creating additional ones.

   * **Enable DHCP Service**
       If set to 'Enabled', and the blueprint template has DHCP enabled, a DHCP
       relay forwarder will be configured on the SVI. This option also implies L3
       routing on this SVI.

   * **IPv4 Connectivity**
       Enables SVI IPv4 routing features for this Virtual Network.

   * **IPv4 Subnet**
       If **IPv4 Connectivity** is enabled, this is the IP address range used on
       this Virtual Network. This can be explicitly set with an IPv4 subnet (e.g.
       192.168.100.0/24), a IPv4 CIDR length (e.g. /26) for auto-assignment of a
       subnet with the specified length or left blank for auto-assignment of a /24
       subnet network. For auto-assignment, the IP is automatically derived from
       the assigned Virtual Networks SVI Pool (more on this described down below).

   * **IPv6 Connectivity**
       Enables SVI IPv6 routing features for this Virtual Network.

       .. note::

          This option is only available if support for IPv6 Applications is enabled.
          Also, if the Template used for the Blueprint used Spine to Leaf Links
          Type of "IPv4" only, then IPv6 cannot be used in the Default Security Zone.

   * **IPv6 Subnet**
       If **IPv6 Connectivity** is enabled, this is the IP address range used on
       this Virtual Network. This can be explicitly set with an IPv4 subnet (e.g.
       2001:4de0::/64), an IPv6 CIDR length (e.g. /56) for auto-assignment of a
       subnet with the specified length or left blank for auto-assignment of a /64
       subnet network. For auto-assignment, the IP is automatically derived from
       the assigned Virtual Networks SVI Pool (more on this described down below).

       .. image:: static/images/blueprint_virtual_tasks/300_virtual-networks-9.png

   * **Default Endpoint Types**
       This allows you to set all endpoints (servers) to a common VLAN
       type (**Unassigned**, **Untagged**, **VLAN Tagged**) when the VN is created.
       This configuration is not persistent and can be changed by editing the VN
       endpoint configuration.

   * **Assigned To**
       Select the rack the VXLAN VN is to be assigned to. For MLAG racks, the leaf
       pair is shown. Each rack has option to specify the **VLAN ID** (used for
       "Batch VN Creation") and **IPv4** field used for racks to set the
       first-hop-redudancy IP address for the SVI (VRRP, VARP, etc). If left blank
       AOS auto-assigns the MLAG SVI from the SVI pool selected.

       .. image:: static/images/blueprint_virtual_tasks/300_virtual-networks-10.png

#. Click **Create** to stage the new virtual network and return to the list view.
#. If IPv4 or IPv6 resources are required for Network SVI Subnets you must assign
   resources, from the virtual networks build section, before the staged blueprint
   can be committed. (Prior to AOS version 3.3.0, SVI subnets were assigned from
   the security zone build section.)

   .. image:: static/images/blueprint_virtual_tasks/virtual_networks_11.png

#. After creating the VXLAN virtual network you must assign the VTEP IP from
   one of the IP pools created in the **Build** tab. Each single leaf Node needs
   one VTEP IP and an "anycast" VTEP IP for all switches in the Virtual Network. For
   MLAG leaf-pair Nodes, a common VTEP IP is assigned for the leaf-pair as well
   as the "anycast" VTEP IP for all switches in the Virtual Network.

Server Per-port Virtual Networks
--------------------------------

.. versionadded:: 2.3
    Racks and Servers can be configured to allow for Server
    Ports to have connections to different Virtual Networks (VN). Racks can be
    designed with multiple leafs, or a single leaf with different logical links.
    Server links can then be defined how they connect to these leafs. When creating
    or editing VNs, the VN endpoints can be modified on a per-port basis, associating
    the port to the VN in an **Unassigned**, **Untagged** and **VLAN Tagged** mode.

Create Per-port Rack Types
``````````````````````````

Refer to `Create Rack Types <rack_types.html#create-rack-types>`_ for instructions
on how to create Rack Types which support Servers with multiple connections allowing
for per-port VN.

When creating an L2 Rack Type to be used, you can create a Rack Type with either
multiple Leafs or a single leaf with different logical links.

 .. image:: static/images/virtual_networks/23_multi-leaf-rack-type_1.png

Next, define a Server Group in the Rack Type which has multiple Links
to the Leafs that were created. The Link Labels will be used with creating
VN. If different Server Per-port VN configurations will be used, you must
create unique Link Labels.

 .. image:: static/images/virtual_networks/23_multi-leaf-rack-type_2.png

The resulting Rack Type shows the Leafs and how the Servers are connected to
these leafs.

Create Server Per-port Virtual Networks
```````````````````````````````````````

When Per-port Rack Type(s) have been added to a Template and a Blueprint has been
created and deployed from the Template, you can create Virtual Networks. If any
Per-port Rack Types are associated with the Template and Blueprint, the
**Default Endpoint Types** field lists the defined Link Labels. You can
select **Unassigned**, **Untagged** and **VLAN Tagged** as the default mode for
each Link Label. The same options are available for both VLAN (leaf-local) and
VXLAN (inter-leaf) VN.

 .. image:: static/images/virtual_networks/23_per-port-vn_1.png

To edit the endpoint after the VN has been created, select the VN from the
**Virtual Networks** list and go to **Endpoints**. By default the associated
devices and ports are displayed in the **Topology View**. The VN Endpoints are
displayed on a leaf per-port basis. Click on the appropriate leaf port then select
the endpoint mode to use between **Unassigned**, **Untagged** and **VLAN Tagged**.

 .. image:: static/images/virtual_networks/23_per-port-vn_2.png

The **Topology View** has a search field to allow you to search for specific
endpoints.

 .. image:: static/images/virtual_networks/23_per-port-vn_4.png

Editing VLAN Virtual Network
----------------------------
#. From the blueprint, navigate to **Staged / Virtual / Virtual Networks**, then
   click the name of the VN to edit.

   .. image:: static/images/blueprint_virtual_tasks/virtual_networks_7.png

#. Click the **Edit** button, and make your changes.
#. Click **Update** to stage the changes and return to the list view.

Deleting Virtual Network
-------------------------
#. From the blueprint, navigate to **Staged / Virtual / Virtual Networks**, then
   either from the list view or the details view, click the **Delete** button
   (trash can) for the VN to delete.

   .. image:: static/images/blueprint_virtual_tasks/blueprint_virtual_tasks_6_310.png

#. Click **Delete** to stage the deletion and return to the list view.

Moving Endpoints Assignments
````````````````````````````

To overwrite an existing untagged (server) port by a new virtual network,
follow these steps:

#. From the blueprint, navigate to **Staged / Virtual / Virtual Networks**, then
   click the name of the virtual network, and select the tagged port to change.
#. Set the port to "Untagged". As this port was already assigned to a different
   virtual network, you will see a warning before forcing the move. Click
   "Force Move" and commit.

As an example, we have these two virtual networks named VLAN2 and VLAN3:

.. image:: static/images/blueprint_virtual_tasks/blueprint_virtual_tasks_16.png

Currently, our server is attached to port swp5, where VLAN3 has been configured
as untagged.

.. image:: static/images/blueprint_virtual_tasks/blueprint_virtual_tasks_17.png

If you would like to change this port to VLAN2 as untagged, proceed as described
above. Click on "VLAN2" in the **Virtual Networks tab**, and select server port
swp5, which is currently configured as "Tagged".
Click "Untagged" as displayed in the following capture:

.. image:: static/images/blueprint_virtual_tasks/blueprint_virtual_tasks_18.png

And confirm by clicking on "Force Move" to overwrite the previous Virtual
Network VLAN3 assignment.

.. image:: static/images/blueprint_virtual_tasks/blueprint_virtual_tasks_19.png

To complete the change you will need to confirm it from the **Uncommitted Tab**

Updating Blueprint
```````````````````

Any virtual network changes made to the Staged Blueprint can be examined by clicking
the **Uncommitted** ribbon button.  Here you have opportunity to view and verify any
intended changes prior to deploying to the **Active** Blueprint.

In this example, we can verify the virtual network "VLAN101" was removed from the
staged blueprint and can be deployed by clicking on the rocket icon. If you do not
want to deploy the change and want to revert the staged changes to the original
blueprint, click the revert "undo" icon.

.. image:: static/images/blueprint_virtual_tasks/blueprint_virtual_tasks_8_310.png

VLANs can be added and deleted, and endpoints can be added and removed while
the blueprint is deployed.

Virtual Network Telemetry
````````````````````````````

When virtual network endpoints are created on servers, expectations are set for the
leaf interfaces facing those servers.

.. todo::
  Virtual Infra
