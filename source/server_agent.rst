============
Server Agent
============
.. Variables for automation
.. highlight:: text

We recommend using the :doc:`AOS web interface <aos_ui>` for tasks related to
:doc:`device agents <device_agents>`. However, if you are using a server
interface naming schema different to ``eth0`` or if you would like to manually
install the device agent for any other reason, this document guides you through
that process.

Knowledge of the various device states and configuration stages is vital for
managing AOS day-to-day operations. See :doc:`configuration_lifecycle` and
:doc:`aos_agent_configuration_file` for details.

In a first instance, you will need to optionally install the desired
extra telemetry packages in order for AOS to show related information.

After this step, you can go to the next section **L3 Server Agent** or the
last section **Manual Agents installation on Servers** if you are just
installing a L2 server agent. This last section will be required in both
cases.

.. warning::

  User is responsible for the verification and installation steps for
  the manual agent installation procedure.

CentOS extra Packages
---------------------

Install the following packages in CentOS if you would like to collect
the related telemetry information.

    .. code-block:: prompt

        yum install -y epel-release
        yum install -y lldpd
        yum install -y python-ipaddr
        yum install -y libselinux-python
        yum install -y iproute


Ubuntu extra Packages
---------------------

Install the following packages in Ubuntu if you would like to collect
the related telemetry information.

    .. code-block:: prompt

        apt update
        apt install -y lldpd
        apt install -y python-ipaddr
        apt install -y libc-ares2
        apt install -y iproute2


L3 Server Agent
---------------

If you are installing a L3 Server agent you will need to go through the
FRR installation procedure described on this section. Otherwise you can go
to the next section **Manual Agents installation on Servers** directly.

CentOS
======

#. Delete and clean any previous Routing related software from the server

    .. code-block:: prompt

        yum erase -y quagga
        yum erase -y frr
        yum erase -y frr-pythontools

#. Delete existing frr user/groups

    .. code-block:: prompt

        userdel frr
        groupdel frrvty

#. Create FRR group

    .. code-block:: prompt

        groupadd frr
        groupadd frrvty

#. Ensure the FRR user configuration

    .. code-block:: prompt

        useradd --shell /sbin/nologin --create-home --home-dir /var/run/frr/ --comment 'FRRouting suite' -g frr -G frrvty frr
        getent passwd frr

#. Make sure that you get the proper FRR image and Install FRR

    .. code-block:: prompt

        yum install -y https://github.com/FRRouting/frr/releases/download/frr-6.0.2/frr-6.0.2-01.el6.x86_64.rpm
        yum install -y https://github.com/FRRouting/frr/releases/download/frr-6.0.2/frr-pythontools-6.0.2-01.el6.x86_64.rpm

Ubuntu
======

#. Delete and clean any previous Routing related software from the server

    .. code-block:: prompt

        service quagga stop
        apt remove -y quagga
        service frr stop
        apt remove -y frr
        apt remove -y frr-pythontools

#. Delete existing FRR user/groups

    .. code-block:: prompt

        userdel frr
        groupdel frrvty

#. Verify your current version by running ``lsb_release -rs`` and make sure you
   get the FRR packages according to this version. Install FRR

   This example shows a FRR download for version 16.04:

    .. code-block:: prompt

        wget -O frr_6.0.2-0.ubuntu16.04.1_amd64.deb -t 3 -T 3 https://github.com/FRRouting/frr/releases/download/frr-6.0.2/frr_6.0.2-0.ubuntu16.04.1_amd64.deb
        dpkg -i --force-confnew ./frr_6.0.2-0.ubuntu16.04.1_amd64.deb
        wget -O frr-pythontools_6.0.2-0.ubuntu16.04.1_all.deb -t 3 -T 3 https://github.com/FRRouting/frr/releases/download/frr-6.0.2/frr-pythontools_6.0.2-0.ubuntu16.04.1_all.deb
        dpkg -i --force-confnew ./frr-pythontools_6.0.2-0.ubuntu16.04.1_all.deb
        chown frr /etc/frr/vtysh.conf
        chgrp frr /etc/frr/vtysh.conf

#. Configure LLDP

    .. code-block:: prompt

        mkdir -p /etc/lldpd.d
        grep 'configure lldp portidsubtype ifname' /etc/lldpd.d/port_info.conf
        echo 'configure lldp portidsubtype ifname' > /etc/lldpd.d/port_info.conf
        service lldpd restart


Manual Agents installation on Servers
-------------------------------------

The following procedure applies for L2 and L3 Servers. This last will need
some extra steps that are described in the previous section **L3 Server Agent**.
Please ensure that they are accomplished before proceeding further.

To install the agent manually please follow the next steps:

#. Ensure that the server can reach the AOS Server IP.
#. Delete any previous aos.run file if any.

    .. code-block:: prompt

        rm -f /tmp/aos.run

#. Download the agent from the AOS Server, replace the ``aos_version`` by the
   running AOS version and the exact build (i.e 3.2.1-298), you can verify
   this information from the **AOS Dashboard --> Platform --> About** and the
   ``aos_server_ip`` variable by the actual AOS Server IP.

    .. code-block:: prompt

        curl -o /tmp/aos.run -k -O https://<aos_server_ip>/device_agent_images/
        aos_device_agent_<aos_version>.run

#. Run the installer.

    .. code-block:: prompt

        /bin/bash /tmp/aos.run -- --no-start

#. Modify the aos.conf file, you will need to update the desired interface
   among other parameters, please refer to this document for details
   :doc:`aos_agent_configuration_file`.
   In this aos.conf file it is important to enable the configuration service
   if you are configuring a L3 Server only. If it is a L2 Server the default
   setting (disabled) is correct.

   .. code-block:: prompt

       enable_configuration_service = 1

#. Start the service.

   .. code-block:: prompt

      sudo aos service start

If everything went as expected, the server will show up in the
**Managed Devices** tab in the AOS Dashboard. Then acknowledge and assign to the
desired blueprint following the standard procedure.

The following capture shows an example of L2 Server agent with
**Telemetry only**. L3 Server would display **Full Control**.

.. image:: static/images/device_management/server_agent1.png

Click on the device key and then go to the Telemetry tab to see the collected
information.

.. image:: static/images/device_management/server_agent2.png
