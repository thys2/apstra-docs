======================================
Processor: Detailed Interface Counters
======================================
The Detailed Interface Counters processor selects interfaces according to the
configuration and outputs all available interface related counters
(e.g tx_bits, rx_bits etc) and interface utilization.

**Input Types** - No inputs. This is a source processor.

**Output Types**

**Properties**

   .. include:: includes/processors/graph_query.rst

   Interface
      Expression mapping from graph query to interface name,
      e.g. "iface.if_name" if "iface" is a name in the graph query.
   Port Speed
      Expression mapping from graph query to link speed in bits per second,
      e.g. "functions.speed_to_bits(link.speed)" if "link" is a name
      in the graph query.

   .. include:: includes/processors/system_id.rst

   Period
      Size of the averaging period. (seconds)

   .. include:: includes/processors/additional_keys.rst

   .. include:: includes/processors/enable_streaming.rst
