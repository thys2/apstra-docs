# -*- coding: utf-8 -*-
"""
    sphinx.ext.autosectionlabel
    ~~~~~~~~~~~~~~~~~~~~~~~~~~~

    Allow reference sections by :ref: role using its title.

    :copyright: Copyright 2007-2017 by the Sphinx team, see AUTHORS.
    :license: BSD, see LICENSE for details.
"""

# pylint: disable=import-error,invalid-name,missing-docstring

from docutils import nodes
from sphinx.util import logging
from sphinx.util.nodes import clean_astext

logger = logging.getLogger(__name__)


def node_get_depth(node):
    i = 0
    cur_node = node
    while cur_node.parent != node.document:
        cur_node = cur_node.parent
        i += 1
    return i


def labels_by_section(labels):
    return [(i[0], i[2]) for i in labels.values()]


def register_sections_as_label(app, document):
    labels = app.env.domaindata['std']['labels']
    section_labels = labels_by_section(labels)
    anonlabels = app.env.domaindata['std']['anonlabels']
    for node in document.traverse(nodes.section):
        if (app.config.autosectionlabel_max_depth and
                node_get_depth(node) > app.config.autosectionlabel_max_depth):
            continue
        labelid = node['ids'][0]
        docname = app.env.docname
        if app.config.autosectionlabel_prefix_document:
            name = nodes.fully_normalize_name(docname + ':' + node[0].astext())
        else:
            name = nodes.fully_normalize_name(node[0].astext())
        sectname = clean_astext(node[0])

        # skip sections that have labels already
        if (docname, sectname) in section_labels:
            continue

        if name in labels:
            logger.warning('duplicate label %s, ' % name + 'other instance '
                           'in ' + app.env.doc2path(labels[name][0]),
                           location=node)

        anonlabels[name] = docname, labelid
        labels[name] = docname, labelid, sectname


def setup(app):
    app.add_config_value('autosectionlabel_prefix_document', False, 'env')
    app.add_config_value('autosectionlabel_max_depth', None, 'env')
    app.connect('doctree-read', register_sections_as_label)
