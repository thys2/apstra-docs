""" Custom lexxers for 'show command' outputs"""
from pygments.lexer import RegexLexer, bygroups, include
from pygments.token import Name, Text, Comment

class ApstraPromptLexer(RegexLexer):
    """ Pygmentizes prompts on configuration files
        All this should do is highlight commands after prompts

        eg:
        localhost(config)# hostname spine1
        ^         ^        ^
        |         |         |
        hostname  Mode      Code

        Should also prompt for command prompts.
    """

    name = 'prompt'
    aliases = ['*.rst', '*.txt', '*.config']

    tokens = {
        'config-prompt': [
            # ! this is a comment
            (r'(\!)(.*)?', bygroups(Text, Comment)),

            #localhost(config)# hostname spine1
            #^         ^        ^
            #|         |         |
            #hostname  Mode      Code
            (r'(.*)(\()(.*)(\))(#|\>)(.*)$',
             #localhost(config)# hostname spine1
             bygroups(Name.Variable, Text, Name.Variable,
                      Text, Text, Name.Function)),
            # Generic output
            # will match on any line that does not contain '#' - not perfect...
            #(r'([^#].+)', bygroups(Com+ment))
        ],
        'shell-prompt': [
            # localhost# command_output

            (r'([\w+|\-|\_]+)(#|\>|\$)(.*)', bygroups(Name.Variable,
                                                      Text, Name.Function)),

            # Bash prmopt
            (r'(.*)(@)(.*)(:)(.*)(\#|\$)(.*)', bygroups(Name.Variable, Text,
                                                        Name.Variable, Text,
                                                        Name.Variable,
                                                        Text, Name.Function)),

            # [guestshell@guestshell ~]$
            (r'(\[)(.*)(@)(.*)(.*)(\])(\$|#)(.*)', bygroups(Text, Name.Variable,
                                                            Text, Name.Variable,
                                                            Name.Variable, Text,
                                                            Text, Name.Function)),

            # [root@host2:/vmfs/volumes/a25f43f6-f2c3bc72/arista_spine1] cmd
            (r'(\[)(.*)(@)(.*)(:)(.*)(\])(.*)', bygroups(Text, Name.Variable, Text,
                                                         Name.Variable, Text,
                                                         Name.Variable, Text,
                                                         Name.Function)),
        ],
        'root': [
            include('config-prompt'),
            include('shell-prompt'),
            # Fallback for text
            (r'.*\n', Text),
        ]
    }

def setup(app):
    """ Custom extension stuff goes here.
    """
    app.add_lexer('prompt', ApstraPromptLexer())
