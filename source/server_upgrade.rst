===================
AOS Server Upgrade
===================

AOS version 3.3.0 is only available for new installations with new AOS blueprints.

Supported Upgrade Paths
=======================

.. WARNING::

    "**.0**" major feature release versions (e.g. AOS 3.3.0, 3.2.0, 3.1.0) are for
    new installations ONLY. You CANNOT UPGRADE to major feature release versions
    (starting with AOS version 3.1.0). Supported upgrades include maintenance
    release versions "**.1**" and later (e.g. AOS 3.1.1, 3.1.2, etc.). See below
    for supported upgrade path details.

.. sidebar:: Check your AOS Version

    From the AOS Web interface, navigate to **Platform / About**

    From your AOS VM CLI run the command
    ``service aos show_version``.

To AOS 3.2.4-65, the following upgrade paths are supported:

* AOS 3.2.3-46 to AOS 3.2.4-65 (All)
* AOS 3.2.2.4-9 to AOS 3.2.4-65 (All)
* AOS 3.2.2.3-3 to AOS 3.2.4-65 (All)
* AOS 3.2.2.2-3 to AOS 3.2.4-65 (All)
* AOS 3.2.2.1-2 to AOS 3.2.4-65 (All)
* AOS 3.2.2-12 to AOS 3.2.4-65 (All)
* AOS 3.2.1-298 to AOS 3.2.4-65 (All)
* AOS 3.2.0-242 to AOS 3.2.4-65 (All)
* AOS 3.1.1-179 to AOS 3.2.4-65 (All)

To AOS 3.2.3-46, the following upgrade paths are supported:

* AOS 3.2.2-12 to AOS 3.2.3-46 (All)
* AOS 3.2.1-298 to AOS 3.2.3-46 (All)
* AOS 3.2.0.2-4 to AOS 3.2.3-46 (All)
* AOS 3.2.0.1-2 to AOS 3.2.3-46 (All)
* AOS 3.2.0-242 to AOS 3.2.3-46 (All)
* AOS 3.1.1-179 to AOS 3.2.3-46 (All)

To AOS 3.2.2-12, the following upgrade paths are supported:

* AOS 3.2.1-298 to AOS 3.2.2-12 (All)
* AOS 3.2.0.2-4 to AOS 3.2.2-12 (All)
* AOS 3.2.0.1-2 to AOS 3.2.2-12 (All)
* AOS 3.2.0-242 to AOS 3.2.2-12 (All)
* AOS 3.1.1-179 to AOS 3.2.2-12 (All)

To AOS 3.2.1-298, the following upgrade paths are supported:

* AOS 3.2.0-242 to AOS 3.2.1-298 (All)
* AOS 3.2.0.1-2 to AOS 3.2.1-298 (All)
* AOS 3.1.1-179 to AOS 3.2.1-298 (All)

To AOS 3.1.1-179, the following upgrade paths are supported:

* AOS 2.3.1-129 (or higher) to AOS 3.1.1-179 (All)
* AOS 3.0.0-151 (or higher) to AOS 3.1.1-179 (All)
* AOS 3.0.1-96 to AOS 3.1.1-179 (All)
* AOS 3.0.2-133 to AOS 3.1.1-179 (All)
* AOS 3.1.0-206 to AOS 3.1.1-179 (All)

.. note::

   A number a Linux kernel and Nginx security updates have been added to AOS 3.1.1.
   In order to receive Ubuntu Linux OS and Nginx updates, the user **must** do a
   VM to VM upgrade.

To AOS 3.0.1-96, the following upgrade paths are supported:

* AOS 2.2.1-166 to AOS 3.0.1-96 (VM to VM)
* AOS 2.3.0-181 to AOS 3.0.1-96 (All)
* AOS 2.3.1-129 to AOS 3.0.1 (All)
* AOS 2.3.2-130 to AOS 3.0.1-96 (All)

Additional upgrade paths may be available. Contact
:doc:`Apstra Global Support <support>` for more information about
supported upgrade paths.

Known Limitations and Issues
============================

* In AOS 3.2.x and earlier versions, AOS VMs deployed from the same image share
  same SSH keys. Due to the way AOS VM is packaged, all AOS instances installed
  from the same OVA/QCOW image have the same SSH host keys. As a result, this
  issue allows an attacker to more easily bypass remote host verification when a
  user connects by SSH to what is believed to be a previously used AOS host but is
  really the attacker's host performing a man-in-the-middle attack. To update the
  SSH Host Keys can be found at `Updating SSH Host Keys`_.

* To AOS 3.2.x, as NCLU syntax is no longer used in Cumulus Interface type
  configlets since 3.2.0, when upgrading from AOS 3.1.1 or prior release, Cumulus
  Interface type configlets must be modified in Linux command style to exactly
  match /etc/network/interfaces syntax.

* To AOS 3.1.1, due to AOS bug (AOS-15213), the AOS Device System Agent
  upgrade process for Cumulus Linux devices will cause the device `switchd` process
  to restart temporarily disrupting traffic on the device. Please contact Apstra
  Global Support for a maintenance version of AOS 3.1.1 which will support upgrades
  of AOS networks with Cumulus Linux devices without disruption.

* To AOS 3.1.1, because of a known AOS bug (AOS-14681), AOS health check on the
  controller fails and indicates that RCI (Root Cause Identification)-related
  agents have terminated after the upgrade. To prevent this from happening,
  disable RCI before upgrading. Please allow 10 minutes after disabling RCI before
  starting the upgrade. After the upgrade is complete, the user can re-enable RCI.

* To AOS 3.1.1, a number a Linux kernel and security updates have been added to
  the Ubuntu 18.04 base OS in AOS 3.1.1. In order to receive Ubuntu Linux OS
  updates, the user **must** do a VM to VM upgrade.

* To AOS 3.1.1, AOS bug (AOS-14708) is fixed where users using macOS Catalina and
  Google Chrome, users cannot accept default self-signed HTTPS/SSL certificate
  provided with Google Chrome. However, upgrading users must do a VM to VM
  upgrade or generate a new self-signed HTTPS/SSL certificate. See
  :ref:`replacing-aos-server-certificate` for more information.

* To any AOS version, saved AOS show tech files are discarded (AOS-14416). To
  prevent file loss (in case they are subsequently needed) download show tech
  files before upgrading AOS.

* To any AOS version, configlets are not updated by AOS upgrade. User has to
  manually maintain configlets.

* To any AOS version, built-in Device Profiles and Interface Maps are updated to
  the shipped built-in IMs and DPs of the target AOS release without warning.
  Any changes in built-in DPs and IMs made by users are not reflected. This is known
  as AOS-13125.

* To any AOS version, ensure there are no AOS Configuration Deviation Anomalies
  prior to starting the upgrade. If there are Configuration Deviation Anomalies,
  the devices may restart processes which may cause traffic disruption on the device.

* To any AOS version, ensure there are no devices in "Undeploy" Deploy Mode.
  Upgrade cannot proceed when some devices are set to Undeploy.

* To any AOS version, user **must** delete Device AAA/TACACS configlets from the
  Blueprint before upgrading AOS, Device Agent, or NOS. User can re-apply the
  configlets after the upgrades.

Check release notes for known limitations and issues.

API Changes
-----------

* All API changes introduced in 2.3.1 are backward compatible with 2.3.0.
* The following non-backward compatible API changes exist between 2.2.1 and 2.3.1:

    * See `Rack Types and Rack Based Templates`_
    * See `Virtual Network Creation`_

Rack Types and Rack Based Templates
-----------------------------------

Advanced rack design, introduced in AOS 2.3, allows users to define rack types
with non-MLAG leaf pairs, multiple MLAG leaf pairs, etc. The API for both
rack types and rack-based templates have been modified in a breaking fashion.
The breaking change is in the JSON payloads in the CRUD APIs.

::

    POST/PUT/GET /api/design/rack-types
    POST/PUT/GET /api/design/templates

Virtual Network Creation
-------------------------
Per-port virtual networks, introduced in AOS 2.3, allow users to create virtual
network endpoints on individual interfaces of L2 servers that have multiple
leaf-facing interfaces. For example, given an L2 server that has 1 link per leaf to
a non-MLAG leaf pair, users can create virtual network endpoints on one or both of
the leaf-facing server interfaces.

The virtual network facade API has been updated for this. The breaking change is in
the JSON payload where the virtual network endpoints are specified. Instead of
specifying a system node ID, API client must now specify an interface node ID.

AOS Server In-Place VM upgrade
===============================

.. sidebar:: Same VM: Pros and Cons

      **Pro** - No need to deploy a new VM, but the VM memory
      may need to be increased.

      **Con** - OS updates are not part of the AOS in-place upgrade procedure.
      If OS updates are required, you can migrate to a different VM (VM-VM).

Make sure you have the following AOS System permissions:

* AOS Operating System admin user privileges.
* AOS admin user group permissions.

AOS Server In-Place VM upgrade consists of:

#. :ref:`Pre-Upgrade Validation <pre-upgrade-same>`
#. :ref:`AOS Server In-Place Upgrade To AOS 3.2 <server-upgrade2-same>`
#. :ref:`AOS Server In-Place Upgrade To AOS 3.1 <server-upgrade-same>`
#. :ref:`AOS System Agents In-Place Upgrade <agent-upgrade-same>`
#. :ref:`AOS Server Cluster In-Place VM Upgrade <cluster-upgrade-same>`

.. _pre-upgrade-same:

Pre-Upgrade Validation (Same VM)
-----------------------------------
#. Verify whether the upgrade is supported for your version.
   See `Supported Upgrade Paths`_.
#. Verify that you have the required VM resources for upgrading.
   Run ``free -h`` and verify < 50% utilization before starting the upgrade.

   .. code-block:: prompt

      admin@aos-server:~$ free -m
                    total        used        free      shared  buff/cache   available
      Mem:          64422        5663       54129           6        4628       58078
      Swap:          4331           0        4331
      admin@aos-server:~$

#. If utilization is > 50%, gracefully shutdown AOS Server, add resources,
   and restart AOS Server.
#. Validate AOS system health by logging in as **admin** and running the following
   command:

   .. code-block:: prompt

      admin@aos-server:~$ service aos status
      * aos.service - LSB: Start AOS management system
         Loaded: loaded (/etc/init.d/aos; generated)
         Active: active (exited) since Sun 2019-10-20 19:45:08 UTC; 6min ago
           Docs: man:systemd-sysv-generator(8)
          Tasks: 0 (limit: 4915)
         CGroup: /aos.service

      admin@aos-server:~$

#. For each AOS Blueprint, review service and probe anomalies, resolve open
   anomalies as much as possible, and take notes of the remaining ones.
#. Perform a backup of the old AOS Server by running the ``sudo aos_backup``
   command:

   .. code-block:: prompt

     admin@aos-server:~$ sudo aos_backup
     [sudo] password for admin:
     ====================================================================
       Backup operation completed successfully.
     ====================================================================
     New AOS snapshot: 2019-10-19_00-05-06
     admin@aos-server:~$

#. Copy the backup files from '/var/lib/aos/snapshot/<snapshot_name>' to an
   external location.

.. _server-upgrade2-same:

AOS Server In-Place Upgrade To AOS 3.2
---------------------------------------
#. After you've performed the pre-upgrade validation, download the AOS Server
   installer **.run** image (e.g. aos_3.2.1-298.run)
   and transfer it to the AOS Server.

   .. code-block:: prompt

      admin@aos-server:~$ ls -l *run
      -rw------- 1 root root 800916510 Apr  2 17:45 aos_3.2.1-298.run

#. Start the upgrade by running the AOS Server installer **.run**
   (e.g. aos_3.2.1-298.run) image.

   .. code-block:: prompt

      admin@aos-server:~$ sudo bash aos_3.2.1-298.run
      [sudo] password for admin:
      Verifying archive integrity... All good.
      Uncompressing AOS installer  100%
      =====================================================================
        Backup operation completed successfully.
      =====================================================================
      AOS[2020-04-09_23:12:33]: Loading AOS 3.2.1-298 image
      AOS[2020-04-09_23:13:32]: Initiating upgrade pre-checker
      AOS[2020-04-09_23:13:33]: Initiating docker library import DONE
      AOS[2020-04-09_23:14:31]: Preparing to retrieve data from running AOS Server. DONE
      AOS[2020-04-09_23:14:52]: Retrieving data from running AOS Server. This step can take up to 10 minutes DONE
      AOS[2020-04-09_23:17:24]: Importing retrieved state to AOS pre-checker. This step can take up to 20 minutes DONE
      Waiting for blueprint <evpn-veos-virtual> processing to finish.. Done
      Summary saved to /tmp/aos-upgrade-config-summary-2020.04.09-231738

#. [new in AOS 3.2] Review a summary of configuration pushed to devices during this
   AOS Upgrade. Page through output then hit **q**.

   .. code-block:: prompt

                                AOS Upgrade Summary
      =======================================================================
      This is a summary of configuration pushed to devices logically grouped
      into sections.  Use 'q' to exit this view.  For more device specific
      configurations, use the menu after quitting this view


      BLUEPRINT: evpn-veos-virtual
      (test-evpn.veos.2485377892357-1139715540 - evpn-veos-virtual)
                                        Section                                                   Systems
      =================================================================================================================
      UPGRADE_BGP_AF_ROUTE_MAPS                                                     spine1 [5054003468A8, 172.20.98.12]
      ~~~~~~~~~~~~~~~~~~~~~~~~~                                                     leaf1 [505400DBF2ED, 172.20.98.11]
      Move route-maps from 'router bgp' context into per-address-family and         spine2 [5054007AA372, 172.20.98.13]
      per-neighbor route-maps to account for advanced external routing policy       leaf3 [5054000F612A, 172.20.98.14]
      config.  Without this change, route-map policies are ambiguous between        leaf2 [5054004ED91F, 172.20.98.15]
      ipv4, ipv6, and evpn address-families.  This operation is performed
      atomically using EOS 'configure session' feature to prevent traffic
      loss during the change when the configuration is temporarily removed.

      configure session bgp_af_route_map_upgrade
      router bgp 64514
        default neighbor 172.16.0.13 route-map MlagPeer out
        default neighbor 198.51.100.2 route-map RoutesFromExt in
        default neighbor 198.51.100.2 route-map RoutesToExt out
        default neighbor l3clos-l route-map AdvLocal out
        address-family ipv4
          neighbor 172.16.0.13 route-map MlagPeer out
          neighbor 198.51.100.2 route-map RoutesFromExt in
          neighbor 198.51.100.2 route-map RoutesToExt out
          neighbor l3clos-l route-map AdvLocal out
          exit
        default neighbor l3clos-l route-map AdvLocal out
        address-family ipv6
          neighbor l3clos-l route-map AdvLocal out
          exit
        vrf blue
          default neighbor 172.16.0.15 route-map MlagL3PeerIn in
          default neighbor 172.16.0.15 route-map MlagL3PeerOut out
          default neighbor 198.51.100.2 route-map RoutesFromExt-blue in
          default neighbor 198.51.100.2 route-map RoutesToExt-blue out
          address-family ipv4
            neighbor 172.16.0.15 route-map MlagL3PeerIn in
            neighbor 172.16.0.15 route-map MlagL3PeerOut out
            neighbor 198.51.100.2 route-map RoutesFromExt-blue in
            neighbor 198.51.100.2 route-map RoutesToExt-blue out
            exit
          exit
        vrf blue
          default neighbor fc01:a05:198:51:100::2 route-map RoutesFromExt-blue in
          default neighbor fc01:a05:198:51:100::2 route-map RoutesToExt-blue out
          default neighbor fc01:a05:fab::7 route-map MlagL3PeerIn in
          default neighbor fc01:a05:fab::7 route-map MlagL3PeerOut out
          address-family ipv6
            neighbor fc01:a05:198:51:100::2 route-map RoutesFromExt-blue in
            neighbor fc01:a05:198:51:100::2 route-map RoutesToExt-blue out
            neighbor fc01:a05:fab::7 route-map MlagL3PeerIn in
            neighbor fc01:a05:fab::7 route-map MlagL3PeerOut out
            exit
          exit
        vrf red
          default neighbor 172.16.0.17 route-map MlagL3PeerIn in
          default neighbor 172.16.0.17 route-map MlagL3PeerOut out
          default neighbor 198.51.100.2 route-map RoutesFromExt-red in
          default neighbor 198.51.100.2 route-map RoutesToExt-red out
          address-family ipv4
            neighbor 172.16.0.17 route-map MlagL3PeerIn in
            neighbor 172.16.0.17 route-map MlagL3PeerOut out
            neighbor 198.51.100.2 route-map RoutesFromExt-red in
            neighbor 198.51.100.2 route-map RoutesToExt-red out
            exit
          exit
        vrf red
          default neighbor fc01:a05:198:51:100::2 route-map RoutesFromExt-red in
          default neighbor fc01:a05:198:51:100::2 route-map RoutesToExt-red out
          default neighbor fc01:a05:fab::d route-map MlagL3PeerIn in
          default neighbor fc01:a05:fab::d route-map MlagL3PeerOut out
          address-family ipv6
            neighbor fc01:a05:198:51:100::2 route-map RoutesFromExt-red in
            neighbor fc01:a05:198:51:100::2 route-map RoutesToExt-red out
            neighbor fc01:a05:fab::d route-map MlagL3PeerIn in
            neighbor fc01:a05:fab::d route-map MlagL3PeerOut out
            exit
          exit
        exit
      commit
      no configure session bgp_af_route_map_upgrade
      configure terminal

      MLAG_PEER_POLICY_COMMUNITIES                                                  leaf1 [505400DBF2ED, 172.20.98.11]
      ~~~~~~~~~~~~~~~~~~~~~~~~~~~~                                                  leaf2 [5054004ED91F, 172.20.98.15]
      AOS 3.2.0 adds support for OSPF connectivity points. As part of
      loop-prevention, BGP communities are used to satisfy loop prevention
      for mutual redistribution between OSPF and BGP.  When OSPF redistributed
      routes are advertised across an MLAG Peer link (SVI or L3 Peer link), these
      community values must be preserved to be used by table-map filtering.
      This upgrade plugin ensures mlag leafs send BGP communities between
      each other.  Future AOS releases will also increase usage of communities.

      router bgp 64514
        neighbor mlag-peer send-community
        neighbor mlag-peer send-community extended
        exit

      (END)

#. [new in AOS 3.2] Use AOS Upgrade Interactive Menu to display config change
   summary, list all devices with config changes, dump all config changes, and so on.
   You can continue with AOS upgrade, or quit AOS upgrade.

   .. code-block:: prompt

        AOS Upgrade: Interactive Menu
      ==================================================
      <Device SN> - display config changes using a
                    specific device serial number
      (s)ummary   - display config change summary
      (l)ist      - list all devices with config changes
      (d)ump      - dump all config changes to a file
      (c)ontinue  - continue with AOS upgrade
      (q)uit      - quit AOS upgrade

      aos-upgrade (h for help)# s

      aos-upgrade (h for help)# l
      Blueprint: evpn-veos-virtual
      (test-evpn.veos.2485377892357-1139715540 - evpn-veos-virtual)
        spine1 [5054003468A8, 172.20.98.12]
        leaf1 [505400DBF2ED, 172.20.98.11]
        spine2 [5054007AA372, 172.20.98.13]
        leaf3 [5054000F612A, 172.20.98.14]
        leaf2 [5054004ED91F, 172.20.98.15]

      aos-upgrade (h for help)# d
      Dumping all configs to /tmp/aos-upgrade-configs-2020.04.09-232155
      BLUEPRINT: evpn-veos-virtual
      (test--evpn.veos.2485377892357-1139715540 - evpn-veos-virtual)
      [1/5] 23:21:55 system: 5054003468A8
      [2/5] 23:22:05 system: 505400DBF2ED
      [3/5] 23:22:07 system: 5054007AA372
      [4/5] 23:22:08 system: 5054000F612A
      [5/5] 23:22:10 system: 5054004ED91F

      aos-upgrade (h for help)#

   .. note::

      (d)ump : It dumps all config changes to a file. As of AOS 3.2.1 it takes
      about 4~5 seconds per device, i.e. 4~5 minutes for 60 devices, only for the
      first time, due to AOS bug (AOS-16728).

   .. warning::
       AOS upgrade is a disruptive process. When upgrading to the same VM,
       if you select **c** to continue, you cannot *rollback* the upgrade.
       The only way to return to the previous version is to reinstall a new VM
       with the previous version and restore the AOS database from backup.

#. AOS confirms that the upgrade was successful.
   You can also check the current AOS version in the AOS Web interface at
   **Platform / About**.

   .. code-block:: prompt

      aos-upgrade (h for help)# h

        AOS Upgrade: Interactive Menu
      ==================================================
      <Device SN> - display config changes using a
                    specific device serial number
      (s)ummary   - display config change summary
      (l)ist      - list all devices with config changes
      (d)ump      - dump all config changes to a file
      (c)ontinue  - continue with AOS upgrade
      (q)uit      - quit AOS upgrade

      aos-upgrade (h for help)# c

      AOS[2020-04-09_23:23:21]: Loading AOS Device Installer image
      AOS[2020-04-09_23:24:40]: Stopping upgrade pre-checker
      cc4590d6a718: Loading layer [==================================================>]  65.58MB/65.58MB
      8c98131d2d1d: Loading layer [==================================================>]  991.2kB/991.2kB
      03c9b9f537a4: Loading layer [==================================================>]  15.87kB/15.87kB
      1852b2300972: Loading layer [==================================================>]  3.072kB/3.072kB
      583f37d385c1: Loading layer [==================================================>]  85.08MB/85.08MB
      fc0141fa6aa5: Loading layer [==================================================>]  3.584kB/3.584kB
      Loaded image: nginx:1.14.2-upload-echo
      AOS[2020-04-09_23:25:11]: Removing installed (3.1.1-179) AOS package
      AOS[2020-04-09_23:25:12]: Installing AOS 3.2.1-298 package
      =====================================================================
        AOS upgrade successful. Please find logs at:
        /var/tmp/aos_upgrade_logs_20200409_232331.tgz
      =====================================================================
      admin@aos-server:~$
      admin@aos-server:~$ service aos show_version
      3.2.1-298

.. _server-upgrade-same:

AOS Server In-Place Upgrade To AOS 3.1
---------------------------------------
#. After you've performed the pre-upgrade validation, download the AOS Server
   installer **.run** image (e.g. aos_3.1.1-179.run)
   and transfer it to the AOS Server.

   .. code-block:: prompt

      admin@aos-server:~$ ls -l
      total 810076
      -rw------- 1 admin admin 829510572 Oct 15 20:45 aos_3.1.1-179.run
      admin@aos-server:~$

#. Start the upgrade by running the AOS Server installer **.run**
   (e.g. aos_3.1.1-179.run) image.

   .. code-block:: prompt

      admin@aos-server:~$ sudo bash aos_3.1.1-179.run
      Verifying archive integrity... All good.
      Uncompressing AOS installer  100%
      =====================================================================
        Backup operation completed successfully.
      =====================================================================
      Loading AOS 3.1.1-179 image
      Initiating upgrade pre-checker
      AOS[2019-10-21_04:16:09]: Initiating docker library import DONE
      AOS[2019-10-21_04:16:29]: Preparing to retrieve data from running AOS Server. DONE
      AOS[2019-10-21_04:20:19]: Retrieving data from running AOS Server. This step can take up to 10 minutes DONE
      AOS[2019-10-21_04:26:52]: Importing retrieved state to AOS pre-checker. This step can take up to 20 minutes DONE

#. Review any device configuration changes planned to be pushed after the
   upgrade. Page through output then hit **q**. Then answer **(y/n)** to continue.

   .. warning::
       This is a disruptive process. When upgrading to the same VM,
       if you select **y** to continue, you cannot *rollback* the upgrade.
       The only way to return to the previous version is to reinstall a new VM
       with the previous version and restore the AOS database from backup.

   .. code-block:: prompt

       Device configuration will be updated for the following device(s):
       ================================================================================
       Device: 525400144A92 (FQDN: l2-virtual-ext-001-leaf1, Management IP Address: 172.20.51.9)
       ================================================================================
       Additional configuration that would be pushed on device agent upgrade:
       {
         "hostname": [{
           "filename": "/etc/hostname",
           "data": [
             "l2-virtual-ext-001-leaf1"
           ]
         },
         {
           "command": "/bin/hostname -F /etc/hostname"
         },
         {
           "command": "systemctl reset-failed lldpd.service"
         },
         {
           "command": "/usr/sbin/service lldpd restart"
         }
         ],  "interfaces": [{
           "filename": "/etc/network/interfaces",
           "data": [
             "# This file was generated by AOS. Do not edit by hand.",
             "#",
             "# The loopback interface",
             "auto lo",
             "iface lo inet loopback",
             "    address 10.0.0.0/32",
             "",
             "# Fabric interfaces",
             "auto swp1",
             "iface swp1",
             "    address 10.0.0.7/31",
             "    alias facing_spine1:swp1",
             "",
             "auto swp2",
             "iface swp2",
             "    address 10.0.0.15/31",
             "    alias facing_spine2:swp1",
             "",
             "# L3Edge interfaces",
       :
       Do you want to continue?(y/n):

#. AOS confirms that the upgrade was successful.
   You can also check the current AOS version in the AOS Web interface at
   **Platform / About**.

   .. code-block:: prompt

      Loading AOS Device Installer image
      a1aa3da2a80a: Loading layer [==================================================>]  65.56MB/65.56MB
      ef1a1ec5bba9: Loading layer [==================================================>]  991.2kB/991.2kB
      6c3332381368: Loading layer [==================================================>]  15.87kB/15.87kB
      e80c789bc6ac: Loading layer [==================================================>]  3.072kB/3.072kB
      21e2934acccd: Loading layer [==================================================>]  86.59MB/86.59MB
      43fd92dffcd9: Loading layer [==================================================>]  3.584kB/3.584kB
      Loaded image: nginx:1.14.2-upload
      Removing installed (3.0.1-96) AOS package
      Installing AOS 3.1.1-179 package
      =====================================================================
        AOS upgrade successful. Please find logs at:
        /var/tmp/aos_upgrade_logs_20191021_043637.tgz
      =====================================================================
      admin@aos-server:~$ service aos show_version
      3.1.1-179
      admin@aos-server:~$

.. _agent-upgrade-same:

AOS System Agents In-Place Upgrade
-----------------------------------
When you upgrade AOS Server to a new version, you must also upgrade the
device agents version to match the AOS Server version.

AOS 3.2
```````

#. Log in to the AOS Web interface as **admin**.
#. Navigate to **Devices / System Agents / Agents**, select the devices that require
   upgrading, and click the 'Install' button. This will re-initiate the installation
   process, which includes a version check. If there is a version mismatch, the agent
   will automatically be upgraded.

   .. image:: static/images/aos_upgrade/321_agent_upgrade.png

#. On the Dashboard, under Liveness, verify there are no Anomalies shown for the
   upgraded devices.

AOS 3.1 and earlier
```````````````````

#. Log in to the AOS Web interface as **admin**.
#. Navigate to **Devices / System Agents / Agents**, and
   enable the System Agents again, in a rolling fashion, to trigger the upgrade
   of the System Agents.

   .. image:: static/images/aos_upgrade/310_aos-upgrade-2.png

#. Wait for the upgrade to complete.

   .. image:: static/images/aos_upgrade/310_aos-upgrade-3.png

#. Verify that the new version is running on the System Agents.

   .. image:: static/images/aos_upgrade/310_aos-upgrade-4.png

#. When no liveness anomalies are present, you have successfully
   updated AOS Server.

   .. image:: static/images/aos_upgrade/310_aos-upgrade-5.png

.. note::

     If, for some reason, if the user needs to "roll back" to the previous version
     of AOS, the user must build a new VM with the previous version of AOS and
     restore the configuration to that VM. Please contact
     :doc:`Apstra Global Support <support>` for assistance.


.. _cluster-upgrade-same:

AOS Server Cluster In-Place VM Upgrade
--------------------------------------
Upgrade process steps described above can also be used for In-Place VM upgrade for
AOS servers Cluster. The workflow to upgrade AOS Cluster will be as:

#. :ref:`Pre-Upgrade Validation <pre-upgrade-same>`
#. Download the aos.run file on AOS controller and worker nodes.
#. Install the aos.run in the controller node first. Once the installation
   is complete, the worker nodes will disconnect from the controller, and the state
   of worker node changes to **FAILED**.
#. :ref:`AOS System Agents In-Place Upgrade <agent-upgrade-same>`
#. Then install the aos.run file in the worker AOS nodes. Once the installation is
   complete, the worker nodes will reconnect to the controller and state will show
   **ACTIVE** in AOS Cluster.

.. note::

    **FAILED** state means that off-box agents and IBA probe containers
    located on that worker node will be unavailable, but devices managed
    by the off-box agents will remain in service.

Upgrading AOS Server onto a Different VM (VM-VM)
================================================

.. sidebar:: Different VM: Pros and Cons

      **Pro** - Include any Ubuntu 18.04 OS updates (ex: security updates).

      **Con** - Requires more infra level tasks (new VM deployment).

The upgrade operation requires the following AOS System permissions:

  * AOS Operating System admin user privileges.
  * AOS admin user group permissions.

Upgrading AOS Server onto a different VM consists of:

#. :ref:`Pre-Upgrade Validation <pre-upgrade-diff>`
#. :ref:`Deploy New AOS Server <deploy-new>`
#. :ref:`Import State <import-state>`
#. :ref:`AOS Server cluster Upgrade to a Different VM<cluster-upgrade-different>`
#. :ref:`AOS Server Rollback (in case of failure) <rollback>`
#. :ref:`AOS System Agents Upgrade <agent-upgrade-diff>`
#. :ref:`Proxy and DNS updates, Shutdown of old AOS Server <proxy>`

.. _pre-upgrade-diff:

Pre-Upgrade Validation (Different VM)
-------------------------------------
#. Verify whether the upgrade is supported for your version.
   See :doc:`Supported Upgrade Paths <server_upgrade>`.
#. Validate AOS system health by logging in as **admin** and running the following
   command:

   .. code-block:: prompt

     admin@aos-server:~$ service aos status
     * aos.service - LSB: Start AOS management system
        Loaded: loaded (/etc/init.d/aos; generated)
        Active: active (exited) since Mon 2019-01-21 22:11:36 UTC; 22h ago
          Docs: man:systemd-sysv-generator(8)
       Process: 6465 ExecStop=/etc/init.d/aos stop (code=exited, status=0/SUCCESS)
       Process: 6828 ExecStart=/etc/init.d/aos start (code=exited, status=0/SUCCESS)

#. For each AOS Blueprint, review service and probe anomalies, resolve open
   anomalies as much as possible, and take notes of the remaining ones.
#. Log in as **root**, and perform a backup of the old AOS Server by
   running the ``sudo aos_backup`` command:

   .. code-block:: prompt

     root@aos-server:/# aos_backup
     ====================================================================
       Backup operation completed successfully.
     ====================================================================
     New AOS snapshot: 2019-01-08_15-24-15

#. Copy the backup files from '/var/lib/aos/snapshot/<snapshot_name>' to an
   external location.

.. _deploy-new:

Deploy New AOS Server (Different VM)
------------------------------------

.. note:: AOS server upgrade procedure does not include the migration of any
  customization done in `/etc/aos/aos.conf` file. If you had performed any
  customization of this file in the current AOS Server that needs to be migrated
  (ex: updated the `metadb` field to use a different network interface) you must
  re-apply the change yourself in the new AOS VM.

#. Download the AOS Server image from the portal.
#. Deploy a new AOS Server VM and configure the new AOS Server VM with a
   new IP address (same or new FQDN may be used), See
   :doc:`AOS Server Installation <server_installation>` for instructions.

   .. note::
       Make sure your new VM has sufficient server resources. See
       :ref:`server_requirements:AOS Server VM Resources`
       for more information.

#. Verify the new AOS server has SSH access to the old AOS server.
#. Verify the new AOS server can reach the Systems Agents.
   See :ref:`network-security-requirements`.
#. Verify the new AOS server can reach any used external system
   (ex: NTP, DNS, vSphere server, LDAP or TACACs+ server, etc).

.. _import-state:

Import State (Different VM)
---------------------------

.. note::
    Avoid API/GUI write operations on the old AOS Server during and after
    the upgrade step (see next steps) as these updates won't be automatically
    copied over to the new AOS Server.

#. Log in to the new AOS VM as **admin**.
#. Start the "Import State" operations on the new AOS server by running the
   ``sudo aos_import_state`` script where `--ip-address` is the old AOS server
   IP address. You will be prompted for the SSH admin password and for the root
   password of the old AOS server.

   .. code-block:: prompt

       root@aos-server:/home/admin# aos_import_state --ip-address 10.10.10.10 --username admin
       SSH password for remote AOS VM:
       Root password for remote AOS VM:
       AOS[20190108_232845]: Preparing to retrieve data from remote AOS Server.
       AOS[20190108_232858]: Retrieving data from remote AOS Server. This step can take upto 10 minutes
       AOS[20190108_232958]: Successfully retrieved data from remote AOS Server.
       AOS[20190108_232959]: Importing retrieved state to AOS. This step can take upto 20 minutes

   .. note::
       The time for completion depends on the size of the Blueprint and also the AOS
       Server VM Resources. As of AOS 3.2.2-12, if the database import exceeds 20
       minutes, the operations may fail as "Timed Out" error.
       In such a case, the timeout value (seconds) can be extended as follows.

       .. code-block:: prompt

           root@aos-server:/home/admin# AOS_UPGRADE_DOCKER_EXEC_TIMEOUT=3000 aos_import_state --ip-address 10.10.10.10 --username admin

       Contact :doc:`Apstra Global Support <support>` for more information.

#. You will be prompted to approve the device configuration
   changes. Review any device configuration changes planned to be pushed after the
   upgrade.

   .. note::
       Hit **q** to come out of the config review mode and continue

   .. code-block:: prompt

       ================================================================================
       Device: 525400001DD0 (FQDN: spine-1, Management IP Address: 172.20.182.14)
       ================================================================================
       Additional configuration that would be pushed on device agent upgrade:
       interface Ethernet1
         description facing_rack-002-leaf1:Ethernet3
       !
       interface Ethernet2
         description facing_rack-001-leaf2:swp4
       !
       interface Ethernet3
         description facing_rack-001-leaf1:swp2
       !
       ================================================================================
       ================================================================================
       Device: 52540009BE9D (FQDN: spine-2, Management IP Address: 172.20.182.13)
       ================================================================================
       Additional configuration that would be pushed on device agent upgrade:
       interface Ethernet1
         description facing_rack-002-leaf1:Ethernet4
       !
       interface Ethernet2
         description facing_rack-001-leaf2:swp3
       !
       interface Ethernet3
         description facing_rack-001-leaf1:swp4
       !
       ================================================================================
       All existing onbox system agents will be disabled
       AOS controller will not automatically upgrade AOS device agents. Use system agents to upgrade AOS device agents.
       The incremental configurations that will be pushed to the device is saved at /tmp/tmpJL92fp

#. [new in AOS 3.2] Use AOS Upgrade Interactive Menu to display config change
   summary, list all devices with config changes, dump all config changes, and so on.
   You can continue with AOS upgrade, or quit AOS upgrade from the menu.

   .. code-block:: prompt

        AOS Upgrade: Interactive Menu
      ==================================================
      <Device SN> - display config changes using a
                    specific device serial number
      (s)ummary   - display config change summary
      (l)ist      - list all devices with config changes
      (d)ump      - dump all config changes to a file
      (c)ontinue  - continue with AOS upgrade
      (q)uit      - quit AOS upgrade

      aos-upgrade (h for help)#

#. [AOS 3.1 and prior] Approve the configuration changes and proceed by entering
   **y** to the next question (press “n” to interrupt the upgrade).

   .. code-block:: prompt

       root@aos-server:/home/admin# aos_import_state --ip-address 172.20.145.3 --username admin
       SSH password for remote AOS VM:
       Root password for remote AOS VM:
       AOS[20190109_214903]: Preparing to retrieve data from remote AOS Server.
       AOS[20190109_214915]: Retrieving data from remote AOS Server. This step can take upto 10 minutes
       AOS[20190109_214926]: Successfully retrieved data from remote AOS Server.
       AOS[20190109_214927]: Importing retrieved state to AOS. This step can take upto 20 minutes
       Do you want to continue?(y/n):

#. Verify that the prompt shows "Importing state successful" message.

   .. code-block:: prompt

       AOS[20190109_221646]: Importing state successful.
       AOS[20190109_221646]: Please find logs at /var/tmp/aos_upgrade_logs_20190109_221646.tgz

#. Log in as admin into the new AOS Server web interface and verify all
   the System Agents are in “Disabled” mode.

   .. note::
       At this point, it is expected to have liveness anomalies raised against the
       System Agents until the complete upgrade is done.

.. _cluster-upgrade-different:

AOS Server cluster Upgrade to a Different VM (using Import State)
------------------------------------------------------------------

There are two possible scenarios for AOS server cluster upgrade by utilizing the
**aos_import_state** script.

**Scenario-1**: New VM for controller node however reuse of worker nodes VMs.

**Scenario-2**: New VMs are introduced for controller as well as worker node VMs.

For **Scenario-1**, When New VM is used for controller node VM but the worker VMs
are being reused, the workflow to follow is :

#. :ref:`Pre-Upgrade Validation <pre-upgrade-diff>`
#. Deploy New AOS Server VM only for controller node with required AOS version and
   resources as per requirement.
   See :ref:`Deploy New AOS Server <deploy-new>`
#. Import the AOS state from running AOS instance to new AOS instance using
   **aos_import_state** script. Once the import is complete, the upgraded instance
   of AOS would show the state of worker nodes as **INACTIVE**.
   Run **aos_import_state** script on the new AOS instance controller node.
   See :ref:`Import State <import-state>`
#. Upgrade system agents
   See :ref:`AOS System Agents Upgrade <agent-upgrade-diff>`
#. Download aos.run for the new AOS version in the worker VMs.
#. Execute aos.run file for the new AOS version in the worker nodes. Once upgrade is
   complete for worker nodes, the Cluster state changes to **ACTIVE**.
#. Remove the old AOS controller VM.


For **Scenario-2**, If new VMs are introduced for controller as well as  worker node
VMs, the workflow to follow is :

#. :ref:`Pre-Upgrade Validation <pre-upgrade-diff>`
#. Create new AOS VMs for controller and worker nodes with the required version of
   AOS. For example, if the existing deployment has 2 worker AOS VMs and an AOS
   controller, create 3 new AOS VMs.
   See :ref:`Deploy New AOS Server <deploy-new>`
#. Designate one of the new VMs to be the new AOS controller.
#. Execute “aos_import_state” from the new controller VM. Specify the
   **--cluster-node-address-mapping** argument to map between old AOS Cluster worker
   VMs and the new AOS Cluster worker VMs.
   See :ref:`Import State <import-state>`
   If there were two worker nodes with IP 1.1.1.1 and 1.1.1.2 in old AOS Cluster
   and the new AOS Cluster worker node VMs have IP 2.2.2.1 and 2.2.2.2, specify the
   argument as

   .. code-block:: prompt

       aos_import_state --ip-address <old_aos_controller> --username admin --password
       admin --cluster-node-address-mapping 1.1.1.1 2.2.2.1 --cluster-node-address-mapping
       1.1.1.2 2.2.2.2.

#. Upgrade system agents
   See :ref:`AOS System Agents Upgrade <agent-upgrade-diff>`
#. After completion of **aos_import_state** script and **system agents upgrade**, the
   new AOS controller node will show AOS cluster with new worker node IPs.
#. Remove the old AOS controller and worker VM nodes.

.. note::

    It is possible to specify a subset of worker VMs as well in Step 2.
    That is, in the above scenario, if the argument
    **--cluster-node-address-mapping 1.1.1.2 2.2.2.2** is not specified, then the
    new AOS controller VM will come up with worker nodes as 2.2.2.1 and 1.1.1.2.
    To upgrade the worker node in 1.1.1.2, the aos.run file can be used.

.. _rollback:

AOS Server Rollback (Different VM)
-----------------------------------

In the case of a failed AOS Server upgrade, or other malfunction,
you may rollback to the old AOS VM, as long as the System Agents upgrade
has not yet started. To do so:

#. Gracefully shutdown the new AOS Server.
#. Start the old AOS Server VM.

.. _agent-upgrade-diff:

AOS System Agents Upgrade (Different VM)
----------------------------------------
The Agent Upgrade process for a different VM is the same as for a same-VM upgrade.
See :ref:`AOS System Agents Upgrade <agent-upgrade-same>`.

.. _proxy:

Proxy and DNS updates, Shutdown of old AOS Server (Different VM)
----------------------------------------------------------------
#. Update any DNS entries to include the new AOS server IP/FQDN based on your
   configuration.
#. In case a proxy is used for AOS Server, make sure the proxy now points to
   the new AOS server.
#. Gracefully shutdown the old AOS VM. You have successfully upgraded AOS.

Updating SSH Host Keys
======================
.. warning::
  In AOS 3.2.x and prior versions, due to the way AOS VM was packaged, all AOS
  instances installed from the same OVA/QCOW image have the same SSH host keys.

  As a result, this issue allows an attacker to more easily bypass remote host
  verification when a user connects by SSH to what is believed to be a previously
  used AOS host but is really the attacker's host performing a man-in-the-middle
  attack.

To update the the SSH Host Keys on an AOS Server, follow this procedure to
generate new SSH Host Keys for a new or existing AOS Server VM:

1. Remove the existing SSH Host Keys

  .. code-block:: bash

      admin@aos-server:~$ sudo rm /etc/ssh/ssh_host*

2. Configure new SSH Host Keys

  .. code-block:: bash

      admin@aos-server:~$ sudo dpkg-reconfigure openssh-server
      Creating SSH2 RSA key; this may take some time ...
      2048 SHA256:EWRFcs4V6BmOILR3T2Psxng1uE0qXQ/z9IKkXrnLpJs root@aos-server (RSA)
      Creating SSH2 ECDSA key; this may take some time ...
      256 SHA256:THaXEia8VW6Jfw6OBXFegu1Cav0zcGSVOy9RkNOPxf4 root@aos-server (ECDSA)
      Creating SSH2 ED25519 key; this may take some time ...
      256 SHA256:0HOn0nnF+7oRaF5HggI4vWeyxT+UNsHcbvNpBJdaKhQ root@aos-server (ED25519)

3. Restart the SSH server process

  .. code-block:: bash

      admin@aos-server:~$ sudo systemctl restart ssh

Q & A
=====

* **Why are System Agents temporarily in Disabled mode in the new AOS Server after
  the upgrade?**
  It tells the AOS Server to not perform any commit onto the System Agents as the
  System Agents are running an older version. By enabling the System Agents again in
  the new AOS Server, the System Agents gets upgraded to match AOS Server version.

* **Will the device configurations change after the AOS Server upgrade?**
  Device configuration changes may be suggested when running the AOS upgrade script:
  manually verify and commit these changes if accepted.

* **How does getting a new IP impact existing VMware vSphere integration?**
  Make sure any firewall between the new AOS Server and the vSphere server is
  allowing the connection. No other impact as AOS server initiates the connection
  with vSphere Server, not vice-versa.
