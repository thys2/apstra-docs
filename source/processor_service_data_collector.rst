=================================
Processor: Service Data Collector
=================================
The Service Data Collector processor collects data from the specified service.
For example, 'bgp' service would be the status of BGP sessions.
Objects to be monitored are configured via the graph query and key. In the BGP
example, key should evaluate to locallp, localAs, remoteIp, or remote As. For
interface-based services such as 'interface' and 'lldp', key is an interface name.

**Input Types** - No inputs. This is a source processor.

**Output Types** - Discrete-State-Set (DSS), Number-Set (NS)

**Properties**

   .. include:: includes/processors/graph_query.rst

   .. include:: includes/processors/service_name.rst

   Keys
      List of property names which values will be used as a key parameters for
      the service. Expression mapping from graph query to whatever key is
      necessary for the service. For lldp it's a string with interface name.
      For bgp it's a tuple like (src_addr, src_asn, dst_addr, dst_asn, vrf_name,
      addr_family), where addr_family should be one of ipv4, ipv6, or evpn. For
      interface it is a string with interface name.

   .. include:: includes/processors/system_id.rst

   .. include:: includes/processors/additional_keys.rst

   .. include:: includes/processors/enable_streaming.rst

.. _service_data_collector_example:

Service Data Collector Example
------------------------------

.. code-block:: none

      service_name: "interface"
      graph_query: "node("system", name="system").out("hosted_interfaces").
                    node("interface", name="iface").out("link").
                    node("link", role="spine_leaf")"
      system_id: "system.system_id"
      key: "interface.if_name"
      role: "system.role"

In this example, we create a DSS that has an entry for every fabric interface
in the system.  Each entry is implicitly tagged by "system_id" and
"key" (where key happens to be the interface name for the interface
service).  Furthermore, as we have specified an additional property "role",
each entry is also tagged by system role.

.. code-block:: none
    :caption: Sample Data

    [system_id=spine1,role=spine,key=eth0]: "up"
    [system_id=spine2,role=spine,key=eth1]: "down"
    [system_id=leaf0,role=leaf, key=swp1]: "up"
