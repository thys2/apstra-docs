=============================================
Hypervisor MTU Mismatch Probe (Virtual Infra)
=============================================
Purpose
   Detect maximum transmission unit (MTU) value deviations across hypervisor
   physical network adapters (pnics).

Source Processor
   Interface MTU (:doc:`generic graph collector <processor_generic_graph_collector>`)
      output stage: Interface MTU (number set) (generated from graph)

Additional Processor(s)
   Check MTU mismatch between hypervisors (:doc:`standard deviation <processor_standard_deviation>`)
      input stage: Interface MTU

      output stage: Hypervisor MTU Deviation (number set)

   MTU Mismatch (:doc:`range <processor_range>`)
      input stage: Hypervisor MTU Deviation (number set)

      output stage: MTU Mismatch (discrete state set)

Example Usage
   **NSX Integration** - If validation fails between NSX-T nodes and AOS in terms
   of mismatch of minimum configured MTU to support Geneve encapsulation or if the
   VLANs defined on NSX-T nodes are not configured on ToR leaf interfaces
   connecting an NSX node to the fabric, then AOS raises anomalies.
