======================
Processor: EVPN Type 5
======================
The EVPN Type 5 processor generates a configuration containing
expectations of EVPN type 5 routes.

**Input Types** - Number-Set (NS), Discrete-State-Set (DSS)

**Output Types** - NSTS, DSSTS

**Properties**

   Execution count
      Number of times the data collection is done.
   Service input
      Data to pass to telemetry collectors, if any.
   Service Interval
      Telemetry collection interval in seconds.
   Service name
      Name of the custom collector service.

   .. include:: includes/processors/enable_streaming.rst
