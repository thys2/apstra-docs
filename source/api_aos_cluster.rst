================
AOS Cluster APIs
================
In addition to using the AOS web interface to check the health of AOS VMs, you
can also use AOS REST API.

Health Checking AOS VMs via AOS REST API
========================================
From the AOS web interface, navigate to **Platform / Developers** to access REST
API documentation. From there you can access cluster APIs.

.. code-block:: text

   /api/cluster/nodes/{node_id}  .. Get AOS slave node status.
   /api/cluster/nodes/{node_id}/errors  .. Retrieve error for an AOS cluster node.

Here is an example of REST API with curl command:

.. code-block:: text

   curl -X GET "https://172.20.159.3/api/cluster/nodes/AosController/errors" -H "accept: application/json"

If no error occurs, the output is as follows:

.. code-block:: text

   {
     "state": "active",
     "errors": []
   }

If the agent process has rebooted, the error is shown as follows:

.. code-block:: text

   {
     "state": "active",
     "errors": [
       "agentReboot"
     ]
   }
