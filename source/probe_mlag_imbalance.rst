====================
MLAG Imbalance Probe
====================

.. image:: static/images/iba/mlag.svg

Found at /predefined_probes/mlag_imbalance

It first identifies all the interfaces across the data center that participate in
server to leaf MLAGs. It then collects samples for each, generates time series and
calculates average traffic across configurable time interval for each interface.
It then calculates the following 3 types of imbalances by calculating standard
deviation across the following sets: (1) set of all interfaces participating in
MLAG (on both leafs), (2) set of all interfaces within a port channel on a given
leaf, (3) set of port channels in an MLAG. It then generates anomaly if any of these
imbalances is out of range. It then also calculates percentage of MLAGs per rack
(leaf pair) that have imbalance as well as percentage of port channels on each leaf
that have imbalance.


"mlag interface traffic" processor
----------------------------------

Purpose: wires in interface traffic samples (measured in bytes per second) for
all leaf interfaces that are part of an MLAG. Unit is bytes per second.

Outputs opf "mlag interface traffic" processor
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

'mlag_int_traffic': set of traffic samples (for each mlag interface on each
leaf). Each set member has the following keys to identify it: mlag_id, server
(label of the server node), leaf (label of the leaf node), rack (label of
the rack), system_id (leaf serial number), interface (name of the interface).

"mlag interface traffic history" processor
------------------------------------------

Purpose: create recent history time series out of traffic samples from the
mlag_int_traffic output. In terms of the number of samples, the time series
will hold the smaller of: 100 samples or samples collected during the last
'total_duration' seconds (facade parameter). Samples unit is bytes per second.

Outputs of "mlag interface traffic history" processor
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

mlag_int_traffic_history: set of traffic samples time series (for each mlag
interface on the leafs).  Each set member has the following keys to identify
it: mlag_id, server (label of the server node), leaf (label of the leaf node),
rack (label of the rack), system_id (leaf serial number), interface (name of
the interface). Samples unit is bytes per second.



"mlag interface traffic average" processor
------------------------------------------

Purpose: Calculate average traffic during period specified by average_period
facade parameter. Unit is bytes per second.

Outputs of "mlag interface traffic average" processor
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

'mlag_int_traffic_avg': set of traffic average values (for each spine
facing interface on each leaf). Each set member has the following keys to identify
it: mlag_id, server (label of the server node), leaf (label of the leaf node),
rack (label of the rack), system_id (leaf serial number), interface (name of
the interface). Unit is bytes per second.


"mlag interface traffic imbalance" processor
--------------------------------------------

Purpose: Calculate standard deviation between traffic averages on all interfaces
belonging to a given MLAG. Unit is bytes per second.

Outputs of "mlag interface traffic imbalance" processor
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

'mlag_int_traffic_imbalance': set of numbers, one for each mlag_id, each indicating
standard deviation of the average traffic on each interface that is part of this
mlag. Each set member has the following keys to identify it: rack, mlag_id. Unit is
bytes per second.


"live mlag imbalance" processor
-------------------------------

Purpose: Evaluate if the mlag imbalance as measured by standard deviation for the
average traffic on each member interface is within acceptable range. In this case
acceptable range is between 0 and std_max facade parameter (in bytes per second
unit).

'live_mlag_imbalance': set of true/false values, each indicating if mlag
imbalance for the average traffic on each member interface is within acceptable
range for each mlag. Each set member has the following keys to identify it:
rack, mlag_id.

"mlag interface imbalance anomaly" processor
--------------------------------------------

Purpose: Export 'live mlag imbalance' state as anomaly.

Outputs of "mlag interface imbalance anomaly" processor
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

'mlag_int_imbalance_anomaly': Set of boolean values, each indicating the
presence or absence of live mlag imbalance. Each set member has the following
keys to identify it: rack, mlag_id.


"mlag imbalance per rack percent" processor
-------------------------------------------

Purpose: Calculate percentage of mlags on a given rack that have imbalance
anomaly.

Outputs of "mlag imbalance per rack percent" processor
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

'mlag_imbalance_rack_perc': Set of numbers, each indicating the percentage
of mlags with imbalance on each rack. Each set member has the following
key to identify it: rack.

"port-channel interface std-dev" processor
------------------------------------------

Purpose: Calculate standard deviation between traffic averages on all interfaces
belonging to a port channel. Unit is bytes per second.

Outputs of "port-channel interface std-dev" processor
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

'port_channel_int_std_dev': set of numbers, one for each port channel identified
by mlag_id, leaf pair. Each number each indicates standard deviation of the average
traffic on each interface that is part of this port channel. Each set member has
the following keys to identify it: rack, mlag_id, leaf. Unit is bytes per second.

"live port-channel imbalance" processor
---------------------------------------

Purpose: Evaluate if the port channel imbalance as measured by standard deviation
for the average traffic on each member interface is within acceptable range. In
this case acceptable range is between 0 and std_max facade parameter (in bytes
per second unit).

'live_port_channel_imbalance': set of true/false values, each indicating if port
channel imbalance for the average traffic on each member interface is within
acceptable range for each mlag. Each set member has the following keys to identify
it: rack, mlag_id, leaf.

"port-channel imbalance anomaly" processor
------------------------------------------

Purpose: Export 'live_port_channel_imbalance' state as anomaly.

Outputs of "port-channel imbalance anomaly" processor
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

'port_channel_links_anomaly': Set of boolean values, each indicating the
presence or absence of live port channel imbalance. Each set member has the
following keys to identify it: rack, mlag_id, leaf.

"port-channel imbalance per rack" processor
-------------------------------------------

Purpose: Calculate percentage of port channels on a given rack that have imbalance
anomaly.

Outputs of "port-channel imbalance per rack" processor
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

'port_channel_imbalance_per_rack': Set of numbers, each indicating the percentage
of port channels with imbalance on each rack. Each set member has the following
key to identify it: rack, mlag_id, leaf.

"mlag port-channel traffic std-dev" processor
---------------------------------------------

Purpose: Calculate standard deviation between traffic averages on both port
channels belonging to a mlag. Unit is bytes per second.

Outputs of "mlag port-channel traffic std-dev" processor
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

'mlag_port_channel_imbalance': set of numbers, one for each mlag identified
by mlag_id, rack pair. Each number each indicates standard deviation of the average
traffic on each port channel that is part of this mlag. Each set member has
the following keys to identify it: rack, mlag_id. Unit is bytes per second.

"live mlag port-channel imbalance" processor
--------------------------------------------

Purpose: Evaluate if the mlag imbalance as measured by standard deviation
for the average traffic on each member port channel is within acceptable range. In
this case acceptable range is between 0 and std_max facade parameter (in bytes
per second unit).

'mlag_port_channel_imbalance_out_of_range': set of true/false values, each
indicating if mlag imbalance between the average traffic on each member port
channel is within acceptable range for each mlag. Each set member has the
following keys to identify it: rack, mlag_id.



"mlag_port-channel imbalance anomaly" processor
-----------------------------------------------

Purpose: Export 'mlag_port_channel_imbalance_out_of_range' state as anomaly.

Outputs of "mlag_port-channel imbalance anomaly" processor
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

'mlag_port_channel_imbalance_anomaly': Set of boolean values, each indicating
the presence or absence of live mlag port-channel imbalance. Each set member
has the following keys to identify it: rack, mlag_id.

"mlag port-channel imbalance per rack" processor
------------------------------------------------

Purpose: Calculate percentage of mlags on a given rack that have port channel
imbalance anomaly.

Outputs of "mlag port-channel imbalance per rack" processor
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

'mlag_port_channel_imbalance_anomaly_per_rack': Set of numbers, each indicating
the percentage of port channels with imbalance on each rack. Each set member has
the following key to identify it: rack, mlag_id.

"port-channel total traffic" processor
--------------------------------------

Purpose: Calculate total traffic per port channel. Unit is byte per second.

Outputs of "port-channel total traffic" processor
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

'mlag_port_channel_total': Set of numbers, each indicating total traffic for
each port channel. Each set member has the following key to identify it:
rack, mlag_id, leaf. Unit is byte per second.
