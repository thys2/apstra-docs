================================
Install AOS Server on VirtualBox
================================

These instructions describe the steps involved with running AOS on VirtualBox. For
full details of using VirtualBox, please refer to the official
VirtualBox documentation at https://www.virtualbox.org/.

.. note::

    VirtualBox is meant for demonstration and lab purposes only. For production
    usage, please use a proper enterprise-scale virtualization solution.


#. Download the AOS OVA to your local workstation from the Support Portal at
   https://portal.apstra.com/downloads/

#. Start VirtualBox, select **File / Import Appliance**, navigate to the
   OVA file and select it for import.

#. Verify the appliance settings. By default VirtualBox will use 2 vCPUs. Ensure
   this is changed to 8 vCPUs. For lab / testing purposes, 8GB RAM will be
   sufficient:

   .. image:: static/images/server_installation/virtualbox_settings.png

#. Once the import is completed (this will take a while), power up the VM to verify
   it is available.

#. Ensure your VM settings match what is required. In particular, check the network
   settings for the adapter that is attached to your management network. If this
   value is not set correctly the AOS Server will not get an IP address.

   Note that by default VirtualBox will have one network adapter attached to the
   Bridged Adapter using the Active network. This means you should have full
   connectivity from your workstation to the VM (http & ssh) out of the box:

   .. image:: static/images/server_installation/virtualbox_network.png

#. Configure AOS following the steps described in
   :doc:`Configuring AOS Server <configure_aos>`.

#. Finally, verify connectivity using ssh from your workstation to the IP address
   of the VM's active network adapter, and by pointing a web browser to this same
   IP address (the IP address can be obtained by running *ifconfig -a* on the VM).
