==================
Processor: Average
==================
The Average processor groups as described by **Group by**, then calculates
averages and outputs one average for each group.

**Input Types** - Number-Set (NS), NSTS

**Output Types** - Number-Set (NS)

**Properties**

   .. include:: includes/processors/group_by.rst

   .. include:: includes/processors/enable_streaming.rst

Average Example
---------------
See :doc:`standard deviation example <processor_standard_deviation>` -
it is the same except we calculate average instead of standard deviation.
