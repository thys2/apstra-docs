===============================================================
Hypervisor and Fabric LAG Config Mismatch Probe (Virtual Infra)
===============================================================

Purpose
   Detect inconsistent LAG configs between fabric and virtual infra and
   calculate LAGs missing on hypervisors and AOS-managed leafs connected to
   hypervisors.

Source Processor
   Hypervisor NICs with LAG (:doc:`generic graph collector <processor_generic_graph_collector>`)
      output stage: Hypervisor NICs LAG Intent Status (discrete state set)
      (generated from graph)

Additional Processor(s)
   Hypervisor NIC LAG anomalies (:doc:`state <processor_state>`)
      input stage: Hypervisor NICs LAG Intent Status

      output stage: Hypervisor NIC LAG Mismatch Anomaly (discrete state set)

Example Usage
   **vSphere Integration** - This probe detects inconsistent LAG configs between
   fabric LAG dual-leafs and ESXi hosts. AOS collects LACP mode information from
   the AOS fabric LAG dual-leafs and also connects to vCenter API and collects
   LAG groups and members per hypervisor.

   .. note::
     Current validation is done on vCenter virtual Distributed Switches only, not
     on virtual Standard Switches. LLDP must be enabled on vCenter vDS switches.

   AOS raises an anomaly if any of the following occurs:

   * LAG member ports on ToR are connected to non-LAG physical ports on ESXi.
   * Non-LAG member ports on ToR are connected to LAG physical ports on ESXi.

   **NSX Integration** - Enabling this probe activates a continuous LAG
   validation between NSX-T transport nodes and AOS data center fabric. It
   validate that LAGs are properly configured between AOS fabric LAG dual-leafs
   and NSX-T transport nodes. The NSX-T uplink profile defines the network
   interface configuration facing the fabric in terms of LAG and LACP config.
   AOS validates and detects network interface misconfiguration between the
   transport node and the ToR switch.

   AOS raises anomalies in the following circumstances:

   * NSX-T transport nodes are not configured for LAG but ToR has LAG member
     ports in the AOS fabric.
   * ESXi hosts are dual-attached to ToR leafs but corresponding NSX-T
     transport nodes are “single-attached” or they are using “NIC-teaming” using
     active-standby or load-balanced config.

   #. Add NSX-T API user as an AOS Virtual Infra.
   #. Add NSX-T Manager in the AOS blueprint
      (External Systems / Virtual Infra Managers).
   #. Enable this probe (Hypervisor and Fabric LAG config mismatch).

   Let’s say in the NSX-T uplink profile, LAG is deleted but the fabric has LAG
   in terms of ToR leafs having LAG member ports. As a result in an AOS blueprint
   after enabling this probe LAG mismatch anomalies are raised.

   .. image:: static/images/nsx/LAG_Config_Mismatch.png

   Since the LAG on the NSX-T transport nodes has been deleted, there is a
   mismatch between physical network adapter (pnic) on ESXi host LAG configuration
   and LAG configuration on ToR leafs.
