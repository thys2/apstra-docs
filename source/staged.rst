====================
|staged_icon| Staged
====================

.. |staged_icon| image:: static/images/icons/staged_icon.png

When resources and devices are assigned or changed in a blueprint they are
visible in the staged view. Multiple changes can be viewed here before they
are pushed to the active blueprint. This staging area allows you to validate
that the pending changes are compliant with the intent, and that they work
together with available resources and devices before you deploy the network.

.. toctree::
   :maxdepth: 2

   staged_physical
   staged_virtual
   staged_policies
   staged_catalog
   staged_settings
   tasks
