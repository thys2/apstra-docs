#########################
AOS EVPN Support Addendum
#########################

This document outlines the caveats and limitations of deploying EVPN with AOS on
supported hardware and NOS. Even though EVPN is a standard, vendors implement
protocols in wildly different manners. On top of that, different ASICs support
varying feature sets impacting EVPN BGP VXLAN implementations, for example, support
for RIOT (Routing In and Out of Tunnels). This document serves to clearly state the
implementations Apstra supports for an EVPN deployment.

Vendor Device OS Support
########################

As of Apstra AOS version 3.3.0.1, Apstra supports EVPN on the following Vendor
Device Operating Systems only (aka Network Operating Systems, NOS). Refer to
:doc:`Device and NOS Support <device_support>` for recommended NOS versions.

Hardware ASIC Support
#####################

* Cisco Cloudscale
* Mellanox Spectrum A1
* Trident Trident2 (see below)
* Trident Trident2+ (see below)
* Trident Trident3 (see below)
* Trident Tomahawk (see below)
* Juniper Q5

.. list-table:: AOS EVPN ASIC Support
   :header-rows: 1
   :class: nowrap

   * - ASIC
     - Example Switches
     - Notes
   * - Arista Trident2
     - Arista DCS-7050
     - Can be used as Spine, Leaf, or Border Leaf. Must set up EOS Recirculation
       interface(s) to be used as a Layer3 Leaf (see `Arista VXLAN documentation <https://www.arista.com/assets/data/pdf/user-manual/um-eos/Chapters/VXLAN.pdf>`_
       for more information).
   * - Arista Trident3
     - DCS-7050CX3
     - Can be used as Spine, Leaf, or Border Leaf.
   * - Arista XP80
     - Arista DCS-7160
     - Can be used as Spine, Leaf, or Border Leaf.
   * - Arista Jericho
     - DCS-7280R
     - Can be used as Spine, Leaf, or Border Leaf.
   * - Cisco Cloudscale
     - Cisco 93180YC-EX
     - Can be used as Spine, Leaf, or Border Leaf
   * - Cisco Trident2 with ALE
     - Cisco 9396PX, 9372PX, 9332PQ, 9504
     - Can be used as Spine, Leaf, or Border Leaf (see `TCAM Carving in NXOS`_).
   * - Cisco Trident2+
     - Cisco 3132Q-V
     - Cannot be used as Border Leaf
   * - Cumulus Trident2+
     - Dell S4048T-ON, S6010-ON, EdgeCore AS5812-54X, AS6812-32X
     - Can be used as Spine, Leaf, or Border Leaf (see `Cumulus RN-766 Support`_)
       in Cumulus.
   * - Cumulus Maverick
     - Dell S4148F-ON, S4148T-ON
     - Can be used as Spine, Leaf, or Border Leaf (see `Cumulus RN-766 Support`_)
       in Cumulus.
   * - Cumulus Mellanox A1
     - Mellanox MSN2100, MSN2700 (Spectrum A1)
     - Can be used as Spine, Leaf, or Barder Leaf. Spectrum A0 not supported.
   * - Cumulus Tomahawk
     - Dell Z9100-ON, EdgeCore AS7712-32X
     - Can be used as Spine, Leaf, or Border Leaf (see `Cumulus RN-766 Support`_).
       Must set up Hyperloop interface(s) (see :doc:`cumulus_hyperloop_interface`)
       to be used as a Layer3 Leaf in Cumulus.
   * - Juniper Q5
     - Juniper QFX10002
     - Can be used as Spine, Leaf, or Border Leaf
   * - Juniper Trident2
     - Juniper QFX5100
     - Can be used as Spine or Layer2 Leaf
   * - Juniper Trident2+
     - Juniper QFX5110
     - Can be used as Spine, Leaf, or Border Leaf
   * - Juniper Trident3
     - Juniper QFX5120
     - Can be used as Spine, Leaf, or Border Leaf

Limitations
###########

EVPN Layer2 Limitations
=======================

* VLAN (Rack-local) Virtual networks must be in the default security zone.
* VxLAN (Inter-rack) Virtual networks cannot be part of the default security zone.

EVPN Layer3 Limitations
=======================

* External routers must connect to leaf devices
* Multi-zone security segmentations only support up to 16 security zones (VRFs) on
  Arista (HW Limitation)
* Inter security zone (VRF) routing must be handled on an external router
  (EVPN type 5 route leaking)
* All BGP sessions and loopback addresses are part of the default security zone.

Cumulus RN-766 Support
######################

In all current versions of Cumulus Linux; when using Broadcom Trident II+,
Trident3, and Maverick platforms in an external VXLAN routing environment; the
switch does not rewrite MAC addresses and TTL, so packets are dropped by the next
hop. See `Cumulus Linux Release Notes for RN-766 <https://support.cumulusnetworks.com/hc/en-us/articles/360007793174-Cumulus-Linux-3-7-Release-Notes#rn766>`_
for more information.

AOS automatically implements work-arounds for Cumulus
Linux RN-766 for the following criteria (as of AOS 2.3.2):

* Device profiles with "ASIC" field set to 'T2+', 'T3', or 'maverick' assigned to
  border-leaf device(s) in the AOS blueprint
* **Overlay Control Protocol** set to **MP-EBGP EVPN**
* External Connectivity Point (ECP) configured for default or new security zone
* External Connectivity Point (ECP) set to L2 mode, with Layer 2 VLAN based BGP
  peering with the external router
* External Connectivity Point (ECP) configured with VLAN ID, SVI Subnet, and SVI
  IPs for border-leaf(s) and router(s).
* User assigns VNI from a VNI pool to the AOS blueprint

AOS automatically allocates additional VNI and configuration for the Cumulus
Linux RN-766 work-around.

.. warning::
  The AOS RN-766 workaround is not supported on blueprints with MLAG L3 peer links
  deployed.

.. warning::
  The AOS RN-766 workaround is not supported for AOS blueprints with **Overlay
  Control Protocol** set to **Static VXLAN** and any L3 External Connectivity
  Points (ECP).

TCAM Carving in NXOS
####################

To successfully deploy EVPN on Cisco Nexus devices other then Cisco Cloudscale,
you must first configure Cisco NXOS TCAM carving. These other devices may include
Cisco NXOSv, or Cisco Nexus "Trident2" devices such as 9396PX, 9372PX, 9332PQ,
or 9504. On Cisco NXOS the ARP Suppression feature is used in order to minimize
ARP flooding.

See the following Knowledge Base article for details:

https://apstra.zendesk.com/hc/en-us/articles/360049704054

Apstra recommends that TCAM Carving be applied during the device management setup
or during Cisco Power-on Auto Provisioning (POAP). TCAM Carving requires a device
reboot. This all should be done prior to AOS device system agent installation.

Alternatively, TCAM Carving can be applied using AOS configlets during AOS blueprint
deployment. The devices will then need to be manually rebooted.

Use the ``show hardware access-list tcam region`` to show and verify TCAM
allocation on Cisco NX-OS.

Cisco NXOSv TCAM Carving
========================

.. code-block:: text
   :caption: NXOSv Configlet Template Text

    hardware access-list tcam region vacl 0
    hardware access-list tcam region racl 0
    hardware access-list tcam region arp-ether 256

.. code-block:: text
   :caption: NXOSv Configlet Negation Template Text

    no hardware access-list tcam region arp-ether 256
    no hardware access-list tcam region racl 0
    no hardware access-list tcam region vacl 0

Cisco Trident2 TCAM Carving
===========================

.. code-block:: text
  :caption: Trident2 Configlet Template Text

   hardware access-list tcam region l3qos 0
   hardware access-list tcam region arp-ether 256 double-wide

.. code-block:: text
  :caption: Trident2 Configlet Negation Template Text

   no hardware access-list tcam region l3qos 0
   no hardware access-list tcam region arp-ether 256 double-wide

Arista EOS VxLAN Routing
########################

Recirculation Interface for Arista Trident2 Devices
===================================================

VxLAN Routing for Trident2 devices (e.g. 7050QX-32) is supported but requires that
the user assign EOS recirculation interfaces to unused physical interfaces on the
device. The user can use AOS configlets to deploy this to all devices requiring
this configuration.

.. code-block:: text
  :caption: Template Text

    interface Recirc-Channel501
     switchport recirculation features vxlan
    interface Ethernet35
     traffic-loopback source system device mac
     channel-group recirculation 501
    interface Ethernet36
     traffic-loopback source system device mac
     channel-group recirculation 501

.. code-block:: text
  :caption: Negation Template Text

    interface Ethernet35
     no traffic-loopback source system device mac
     no channel-group recirculation 501
    interface Ethernet36
     no traffic-loopback source system device mac
     no channel-group recirculation 501
    no interface Recirc-Channel501

VxLAN Routing System Profile for Arista Jericho Devices
=======================================================

It is recommended when using VxLAN Routing for Jericho devices (e.g. 7280SR-48C6)
that the user assign EOS VxLAN Routing System Profile on the device.

Apstra recommends that Arista TCAM system profile be applied during the device
management setup or during Arista Zero-Touch Provisioning (ZTP). TCAM system profile
requires a device reboot. This all should be done prior to AOS device system agent
installation.

Alternatively, the user can use AOS configlets to deploy this to all devices
requiring this configuration and manually reboot the devices.

.. code-block:: text
  :caption: Template Text

    hardware tcam
       system profile vxlan-routing

.. code-block:: text
  :caption: Negation Template Text

    hardware tcam
       no system profile vxlan-routing

AOS Graph Node VTEP Types
#########################

Unicast VTEPs
=============
Unicast VTEPs do not apply to Cumulus and Arista.

Cisco Unicast VTEPs - Vendor Definition: Anycast VTEP
-----------------------------------------------------
**AOS IP Allocation**

Unique per leaf in MLAG pair

Not allocated to singleton switches

**MLAG Configuration**

.. code-block:: text
  :caption: rack1-leaf1

   interface loopback1
      IP address 10.0.0.1/32
      IP address 10.0.0.3/32 secondary
   interface nve1
      source-interface loopback1

.. code-block:: text
  :caption: rack1-leaf2

   interface loopback1
      IP address 10.0.0.2/32
      IP address 10.0.0.3/32 secondary
   interface nve1
      source-interface loopback1

**Single Switch Configuration**

.. code-block:: text

   interface loopback1
      IP address 10.0.0.1/32
   interface nve1
      source-interface loopback1

Logical VTEPs
=============

Arista Logical VTEPs
--------------------
**AOS IP Allocation**

Logical VTEP configured as primary IP on loopback1 interface for both MLAG
and singleton switches

All top of rack nodes share same logical VTEP IP:

* MLAG leafs share same logical VTEP IP
* Singleton leaf gets its own VTEP IP

**MLAG Configuration**

.. code-block:: text
  :caption: rack1-leaf1

   interface loopback1
      IP address: 10.0.0.1/32
      IP address: 10.0.0.4/32 secondary
   interface vxlan1
      vxlan source-interface loopback1

.. code-block:: text
  :caption: rack1-leaf2

   interface loopback1
      IP address: 10.0.0.1/32
      IP address: 10.0.0.4/32 secondary
   interface vxlan1
      vxlan source-interface loopback1

**Single Switch Configuration**

.. code-block:: text

   interface loopback1
      IP address: 10.0.0.5/32
      IP address 10.0.0.4/32 secondary
   interface vxlan1
      vxlan source-interface loopback1

Cumulus Logical VTEPs
---------------------
**AOS IP Allocation**

For MLAG (clagd) leafs, shared as clagd-vxlan-anycast-ip on lo interface, shared
on leaf1 and leaf2

For singleton leafs, configured as additional IP alongside loopback ip on iface lo

**MLAG Configuration**

.. code-block:: text
  :caption: rack1-leaf1

   auto lo
   iface lo inet loopback
      IP address: 10.0.0.6/32
      clagd-vxlan-anycast-ip 10.0.0.4/32

.. code-block:: text
  :caption: rack1-leaf2

   auto lo
   iface lo inet loopback
      IP address: 10.0.0.6/32
      clagd-vxlan-anycast-ip 10.0.0.4/32

**Single Switch Configuration**

.. code-block:: text

   auto lo
   iface lo inet loopback
      IP address: 10.0.0.6/32
      IP address: 10.0.0.7/32

Anycast VTEP
============
Anycast VTEPs do not apply to Cisco and Cumulus.

Arista Anycast VTEPs
--------------------
**AOS IP Allocation**

One anycast VTEP for entire blueprint, shared between all Arista leafs

Configured as secondary IP on loopback1 interface

**MLAG Configuration**

.. code-block:: text
  :caption: rack1-leaf1

   interface loopback1
      IP address 10.0.0.1/32
      IP address 10.0.0.5/32 secondary
   interface vxlan1
      vxlan source-interface loopback1


.. code-block:: text
  :caption: rack1-leaf2

   interface loopback1
      IP address 10.0.0.1/32
      IP address 10.0.0.5/32 secondary
   interface vxlan1
      vxlan source-interface loopback1

**Single Switch Configuration**

.. code-block:: text

   interface loopback1
      IP address 10.0.0.5/32
      IP address 10.0.0.4/32 secondary
   interface vxlan1
      vxlan source-interface loopback1
