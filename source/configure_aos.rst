======================
Configuring AOS Server
======================
After you've installed the AOS Server VM, boot
the VM to access the first boot configuration script.

Initial Setup
=============
(AOS Versions 2.3.1 and Later)

#. When you connect to AOS Server via CLI for the first time, a script runs
   automatically to assist you with basic settings. You can access the
   script at any time with the following command:

   .. code-block:: prompt

       admin@aos-server:~$ aos_config

   .. image:: static/images/server_installation/firstboot_script_initial.png
      :width: 450px

#. Choose **<Yes>** to change the default administrator password.
   For enhanced security, choose a strong password that has a minimum of fourteen
   characters, one uppercase character, one digit, and one that does not
   contain the current username in any form.

   .. warning::

      User *admin* has full root access. We *highly* recommend that you change
      the default password. Apstra cannot be held responsible for
      security-related incidents due to failing to change the default password.

#. After changing the **admin** password, choose **<Yes>** when asked
   if you want to start AOS service.

   .. image:: static/images/server_installation/firstboot_script_start_aos.png
      :width: 450px

#. The main menu appears. Choose **Local credentials** and change the password
   for the default CLI user **admin**.

   .. image:: static/images/server_installation/firstboot_script_main.png
      :width: 450px

#. Choose **WebUI credentials**, and change the password for the
   default Web UI user **admin**. AOS services must
   be up and running in order to change this password.

   .. image:: static/images/server_installation/firstboot_script_webui.png
       :width: 450px

#. Choose **Network** to change the machine's network settings.
   By default **DHCP** is used. If you change the default to **static**
   you'll have the option of providing a CIDR IP address, gateway,
   primary / secondary DNS and domain values.

#. After you've completed the configuration, choose **<Yes>** to restart
   network service, Docker and AOS service.

   .. image:: static/images/server_installation/firstboot_script_network.png
       :width: 450px

#. AOS Service is disabled by default. If you enable services from this
   configuration tool, it invokes ``/etc/init.d/aos``. This is the
   equivalent of running ``service aos start`` at the command line.

Configuring Static Management IP Address
========================================
(AOS Versions 2.3.0 and Later)

Log into the server as user **admin**. To statically configure the
management IP address, edit the ``/etc/netplan/01-netcfg.yaml``
file per standard Ubuntu 14.04 practice.

.. code-block:: text
    :caption: Configure IP address AOS 2.3.0 and later

    admin@aos-server:~$ sudo vi /etc/netplan/01-netcfg.yaml
    [sudo] password for admin:

    # This file describes the network interfaces available on your system
    # For more information, see netplan(5).
    network:
      version: 2
      renderer: networkd
      ethernets:
        eth0:
          dhcp4: no
          addresses: [192.168.59.250/24]
          gateway4: 192.168.59.1
          nameservers:
              search: [example.com, example.net]
              addresses: [69.16.169.11, 69.16.170.11]

Apply the IP address change with the ``sudo netplan apply`` command
or reboot AOS Server with the ``sudo reboot`` command.

See https://netplan.io/examples for more information on using netplan.

The user can also use the `sudo aos_config` CLI command to change the server network
configuration via netplan.

.. _chg_docker:

Updating SSH Host Keys
======================
.. warning::
  In AOS 3.2.x and prior versions, due to the way AOS VM was packaged, all AOS
  instances installed from the same OVA/QCOW image have the same SSH host keys.

  As a result, this issue allows an attacker to more easily bypass remote host
  verification when a user connects by SSH to what is believed to be a previously
  used AOS host but is really the attacker's host performing a man-in-the-middle
  attack.

To update the the SSH Host Keys on an AOS Server, follow this procedure to
generate new SSH Host Keys for a new or existing AOS Server VM:

#. Remove the existing SSH Host Keys

   .. code-block:: bash

      admin@aos-server:~$ sudo rm /etc/ssh/ssh_host*

#. Configure new SSH Host Keys

   .. code-block:: bash

      admin@aos-server:~$ sudo dpkg-reconfigure openssh-server
      Creating SSH2 RSA key; this may take some time ...
      2048 SHA256:EWRFcs4V6BmOILR3T2Psxng1uE0qXQ/z9IKkXrnLpJs root@aos-server (RSA)
      Creating SSH2 ECDSA key; this may take some time ...
      256 SHA256:THaXEia8VW6Jfw6OBXFegu1Cav0zcGSVOy9RkNOPxf4 root@aos-server (ECDSA)
      Creating SSH2 ED25519 key; this may take some time ...
      256 SHA256:0HOn0nnF+7oRaF5HggI4vWeyxT+UNsHcbvNpBJdaKhQ root@aos-server (ED25519)

#. Restart the SSH server process

   .. code-block:: bash

      admin@aos-server:~$ sudo systemctl restart ssh

Changing Default Docker Networks
================================
Docker containers used by AOS require two networks for internal connectivity.
These are configured automatically with a default IPv4 network address. These
networks are:

.. code-block:: text

    docker0: inet 172.17.0.1/16
    docker_gwbridge: inet 172.18.0.1/16

If these subnets are required elsewhere in the setup, modify the default values
to avoid conflicts.

docker0 network
---------------
To modify the default value for the docker0 network, update
the *bip* value with the subnet of your choice. If the json file does not already
exist, you can create it in the following format:

.. code-block:: text

    $ sudo vi /etc/docker/daemon.json

        {
          "bip": "172.26.0.1/16"
        }

    $ sudo service docker restart

docker_gwbridge network
-----------------------
To modify the default value for the docker_gwbridge network,
replace the subnet and gateway IP with the values of your choice:

.. code-block:: text

    $ sudo service aos stop
    $ docker swarm leave --force
    $ sudo service docker stop
    $ sudo ip link set docker_gwbridge down
    $ sudo ip link del dev docker_gwbridge
    $ sudo service docker start
    $ docker network rm docker_gwbridge
    $ docker network create \
      --subnet 10.11.0.0/16 \
      --gateway 10.11.0.1 \
      --opt com.docker.network.bridge.name=docker_gwbridge \
      --opt com.docker.network.bridge.enable_icc=false \
      --opt com.docker.network.bridge.enable_ip_masquerade=true \
      docker_gwbridge
    $ sudo service aos restart

AOS Server Configuration File
=============================
``/etc/aos/aos.conf``

Controller Section
------------------

.. code-block:: ini

      admin@aos-server:/etc/aos$ cat aos.conf
      [controller]
      metadb=eth0

      # Role for the controller. Set the option to "slave" in order to setup AOS as a
      # slave AOS. The options "metadb" and "node_id" should be also set while
      # setting "role" to "slave"
      role = controller
      # Id of the slave node. Empty in case the server is the controller. The ID is
      # generated by the controller.
      node_id =

Security Section
----------------

.. code-block:: ini

      [security]

      # ***EXPERIMENTAL FEATURE*** This feature should not be enabled without Apstra
      # engineering assistance. Enable secure connections for AOS system agents.
      enable_secure_sysdb_connection = 0

Log Rotate Section
------------------

.. code-block:: ini

      [logrotate]

      # AOS has builtin log rotate functionality. You can disable it by setting
      # <enable_log_rotate> to 0 if you want to use linux logrotate utility to manage
      # your log files. AOS agent reopens log file on SIGHUP
      enable_log_rotate = 1
      # Log file will be rotated when its size exceeds <max_file_size>
      max_file_size = 1M
      # The most recent <max_kept_backups> rotated log files will be saved. Older
      # ones will be removed. Specify 0 to not save rotated log files, i.e. the log
      # file will be removed as soon as its size exceeds limit.
      max_kept_backups = 5
      # Interval, specified as <hh:mm:ss>, at which log files are checked for
      # rotation.
      check_interval = 1:00:00

Auth Sysdb Log Rotator Section
------------------------------

.. code-block:: ini

      [auth_sysdb_log_rotator]

      # AOS has builtin auth sysdb persistence file rotation functionality. Default
      # value is 1 which means sysdb retention policy is enabled. You can disable it
      # by setting it to 0 and you also can enable it again by setting it to 1. All
      # retention policy parameters will be reloaded by restarting AOS service, or
      # sending SIGHUP signal to SysdbResourceManager agent via "sudo kill -s 1
      # $(pgrep -f SysdbResourceManager)"
      enable_auth_sysdb_rotate = 1
      # Maximum number of backup copies of valid auth sysdb persistence file groups
      # in /var/lib/aos/db. AOS will remove all the older groups. Default value is 5,
      # which means AOS will keep the latest 5 groups. Min value is 3. It should be
      # specified as a positive number or empty. Leaving it empty means no groups
      # number limitation. It will be set to default value if it is configured in
      # invalid format. It will be set to minimum value if it is configured to a
      # smaller value.
      max_kept_backups = 5
      # Maximum total size of valid auth sysdb persistence file groups in
      # /var/lib/aos/db. Default value is empty, which means no size limitation. It
      # should be specified as empty or a positive number ending with k/m/g (case
      # insensitive) or no suffix. Otherwise, it will be set to default value. AOS
      # will keep at least 3 valid groups no matter how <max_total_files_size> being
      # configured.
      max_total_files_size =
      # Interval, specified as <hh:mm:ss>, at which auth sysdb persistence files are
      # checked for rotation. Default value is 1:00:00. It will be set to default
      # value is it is configured in invalid format. Min value is 00:01:00. It will
      # be set to min value if it is configured to a smaller value. AOS also update
      # all the retention policy parameters per <check_interval> when it is enabled.
      check_interval = 1:00:00


Main Sysdb Log Rotator Section
------------------------------
Four parameters for configuring the main graph datastore retention policy.

.. code-block:: ini

      [main_sysdb_log_rotator]

      # AOS has builtin main sysdb persistence file rotation functionality. Default
      # value is 1 which means sysdb retention policy is enabled. You can disable it
      # by setting it to 0 and you also can enable it again by setting it to 1. All
      # retention policy parameters will be reloaded by restarting AOS service, or
      # sending SIGHUP signal to SysdbResourceManager agent via "sudo kill -s 1
      # $(pgrep -f SysdbResourceManager)"
      enable_main_sysdb_rotate = 1
      # Maximum number of backup copies of valid main sysdb persistence file groups
      # in /var/lib/aos/db. AOS will remove all the older groups. Default value is 5,
      # which means AOS will keep the latest 5 groups. Min value is 3. It should be
      # specified as a positive number or empty. Leaving it empty means no groups
      # number limitation. It will be set to default value if it is configured in
      # invalid format. It will be set to minimum value if it is configured to a
      # smaller value.
      max_kept_backups = 5
      # Maximum total size of valid main sysdb persistence file groups in
      # /var/lib/aos/db. Default value is empty, which means no size limitation. It
      # should be specified as empty or a positive number ending with k/m/g (case
      # insensitive) or no suffix. Otherwise, it will be set to default value. AOS
      # will keep at least 3 valid groups no matter how <max_total_files_size> being
      # configured.
      max_total_files_size =
      # Interval, specified as <hh:mm:ss>, at which main sysdb persistence files are
      # checked for rotation. Default value is 1:00:00. It will be set to default
      # value is it is configured in invalid format. Min value is 00:01:00. It will
      # be set to min value if it is configured to a smaller value. AOS also update
      # all the retention policy parameters per <check_interval> when it is enabled.
      check_interval = 1:00:00

``enable_main_sysdb_rotate = 1`` enables and disables the policy. Set to **1** to
enable the retention policy (default). If you enable the policy after it has been
disabled, you must restart the AOS server for it to be enabled again. Set to **0**
to disable the retention policy and keep all backups. AOS VM file disk utilization
issues may occur. The policy will be disabled during the next retention check
(``check_interval``). There is no need to restart the AOS server unless you want
to disable the policy immediately.

``max_kept_backups = 5`` maximum number of backups to store in ``/var/lib/aos/db``.
Leave default of **5** to keep the latest five backups. Set to an empty string to
keep an unlimited number of backups. Setting to an invalid number results in the
default value of **5**. Setting to a number smaller than **3** (the minimum)
results in the minimum value of **3**.

``max_total_files_size =`` maximum file group size to store in ``/var/lib/aos/db``
Leave default of an empty string for no size limitation. Set to a number ending
in k, m, or g (case-sensitve) or without a suffix.

The effect of ``max_kept_backups`` and ``max_total_files_size`` is cumulative.
For security, AOS keeps a minimum of three groups of valid Main Graph Datastore
persistence files.

``check_interval = 1:00:00`` time between retention checks and parameter updates
(if file has been updated) (format: ``<hh:mm:ss>``). Leave default of **1:00:00**
to check every hour. Setting to an invalid number results in the default value of
**1:00:00**. Setting to a number smaller than **00:01:00** (the minimum) results
in the minimum value of **1:00:00**.

Credential Sysdb Log Rotator Section
------------------------------------

.. code-block:: ini

      [credential_sysdb_log_rotator]

      # AOS has builtin credential sysdb persistence file rotation functionality.
      # Default value is 1 which means sysdb retention policy is enabled. You can
      # disable it by setting it to 0 and you also can enable it again by setting it
      # to 1. All retention policy parameters will be reloaded by restarting AOS
      # service, or sending SIGHUP signal to SysdbResourceManager agent via "sudo
      # kill -s 1 $(pgrep -f SysdbResourceManager)"
      enable_credential_sysdb_rotate = 1
      # Maximum number of backup copies of valid credential sysdb persistence file
      # groups in /var/lib/aos/db. AOS will remove all the older groups. Default
      # value is 5, which means AOS will keep the latest 5 groups. Min value is 3. It
      # should be specified as a positive number or empty. Leaving it empty means no
      # groups number limitation. It will be set to default value if it is configured
      # in invalid format. It will be set to minimum value if it is configured to a
      # smaller value.
      max_kept_backups = 5
      # Maximum total size of valid credential sysdb persistence file groups in
      # /var/lib/aos/db. Default value is empty, which means no size limitation. It
      # should be specified as empty or a positive number ending with k/m/g (case
      # insensitive) or no suffix. Otherwise, it will be set to default value. AOS
      # will keep at least 3 valid groups no matter how <max_total_files_size> being
      # configured.
      max_total_files_size =
      # Interval, specified as <hh:mm:ss>, at which credential sysdb persistence
      # files are checked for rotation. Default value is 1:00:00. It will be set to
      # default value is it is configured in invalid format. Min value is 00:01:00.
      # It will be set to min value if it is configured to a smaller value. AOS also
      # update all the retention policy parameters per <check_interval> when it is
      # enabled.
      check_interval = 1:00:00

Anomaly Sysdb Log Rotator Section
---------------------------------

.. code-block:: ini

      [anomaly_sysdb_log_rotator]

      # AOS has builtin anomaly sysdb persistence file rotation functionality.
      # Default value is 1 which means sysdb retention policy is enabled. You can
      # disable it by setting it to 0 and you also can enable it again by setting it
      # to 1. All retention policy parameters will be reloaded by restarting AOS
      # service, or sending SIGHUP signal to SysdbResourceManager agent via "sudo
      # kill -s 1 $(pgrep -f SysdbResourceManager)"
      enable_anomaly_sysdb_rotate = 1
      # Maximum number of backup copies of valid anomaly sysdb persistence file
      # groups in /var/lib/aos/db. AOS will remove all the older groups. Default
      # value is 5, which means AOS will keep the latest 5 groups. Min value is 3. It
      # should be specified as a positive number or empty. Leaving it empty means no
      # groups number limitation. It will be set to default value if it is configured
      # in invalid format. It will be set to minimum value if it is configured to a
      # smaller value.
      max_kept_backups = 5
      # Maximum total size of valid anomaly sysdb persistence file groups in
      # /var/lib/aos/db. Default value is empty, which means no size limitation. It
      # should be specified as empty or a positive number ending with k/m/g (case
      # insensitive) or no suffix. Otherwise, it will be set to default value. AOS
      # will keep at least 3 valid groups no matter how <max_total_files_size> being
      # configured.
      max_total_files_size =
      # Interval, specified as <hh:mm:ss>, at which anomaly sysdb persistence files
      # are checked for rotation. Default value is 1:00:00. It will be set to default
      # value is it is configured in invalid format. Min value is 00:01:00. It will
      # be set to min value if it is configured to a smaller value. AOS also update
      # all the retention policy parameters per <check_interval> when it is enabled.
      check_interval = 1:00:00

Device Image Management Section
-------------------------------

.. code-block:: ini

      [device_image_management]

      # Enable version compatibility check. By default version compatibility check is
      # enabled. A device will not connect to AOS if its version of AOS device agent
      # is not compatible with AOS controller
      enable_version_check = 1
      # Enable AOS device agent image auto upgrade. By default auto image upgrade is
      # disabled. With this option enabled a device can download an image from the
      # controller and upgrade itself if needed.
      enable_auto_upgrade = 0
      # A device will retry in specified timeout (in seconds) if it fails version
      # compatibility check or to download/install new image.
      retry_timeout = 600

Authentication Section
----------------------

.. code-block:: ini

      [authentication]

      # Enable authentication/authorization check. By default
      # authentication/authorization is enabled. You can disable it by setting enable
      # to 0
      enable = 1
      # Set token expiration time (in seconds). By default token will be expired
      # after 24 hours (86400 seconds).
      token_expiration = 86400

Device Config Management Section
--------------------------------

.. code-block:: ini

      [device_config_management]

      # Setting to push quarantine config to unacknowledged devices. By default it is
      # disabled as it causes traffic disruptions.Set the value to 1 to enable
      # pushing quarantine config, which shuts down all interfaces on the device.
      enable_push_quarantine_config = 0

Telemetry Init Section
----------------------

.. code-block:: ini

      [telemetry_init]

      # Number of initial BGP telemetry update rounds before anomaly detection is
      # started.
      bgp = 4
      # Number of initial interface telemetry update rounds before anomaly detection
      # is started.
      interface = 4
      # Number of initial LAG telemetry update rounds before anomaly detection is
      # started.
      lag = 4
      # Number of initial LLDP telemetry update rounds before anomaly detection is
      # started.
      lldp = 4
      # Number of initial route telemetry update rounds before anomaly detection is
      # started.
      route = 4
      # Number of initial MLAG telemetry update rounds before anomaly detection is
      # started.
      mlag = 4

Telemetry Global Config Section
-------------------------------

.. code-block:: ini

      [telemetry_global_config]

      # Python multithreading enable/disable knob for telemetry  collection
      multithreading_config = 1
      # Execution timeout for extensible telemetry collectors
      command_timeout = 120

Task API Section
----------------

.. code-block:: ini

      [task_api]

      # Default maximum time in seconds a task can stay in its current state.
      default_timeout = 600.0
      # Time in seconds a blueprint.create task can stay in its current state.Format:
      # "timeout_<task_type>"
      timeout_blueprint.create = 360.0
      # Time in seconds a blueprint.deploy task can stay in its current state.Format:
      # "timeout_<task_type>"
      timeout_blueprint.deploy = 300.0
      # Time in seconds blueprint.facade.* tasks can stay in their current state.
      # Specific facade task overrides prevail over this one.Format:
      # "timeout_<task_type>"
      timeout_blueprint.facade = 600.0
      # Maximum number of tasks, which allowed in the queue. When number of tasks
      # becomes higher this value, task rotation will be started.
      max_tasks_in_queue = 100
      # Maximum number of Bytes in data field which does not require compression. If
      # data size is greater than threshold data will be compressed before storing it
      # in sysdb.
      max_uncompressed_data_size = 1000

Statistics Section
------------------

.. code-block:: ini

      [statistics]

      # Enable or disable full validation for pod statistics. Disable if Racks and/or
      # Pods tabs load times are excessive
      pod_full_validation = enabled
