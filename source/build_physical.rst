================
Build (Physical)
================

Staging Resources
=================

.. image:: static/images/blueprints/staged/physical/resources_stage_330.png

From the :ref:`blueprint <access_blueprint>`, navigate to
**Staged / Physical / Build / Resources**. (The build panel is on the right side.)

Update Assignments
------------------
#. A red status indicator means that resources need to be assigned. Click the
   indicator, then click the **Update assignments** button.
#. Select a pool from which to pull the resources, then click the **Save** button.
   AOS assigns the required number of resources to the resource group
   automatically. When the red status indicator turns green, the resource
   assignment has been successfully staged.

You can also assign resources on a per-device basis (especially useful if you have
a predefined resource mapping). Select the device from the topology view or nodes
view, then assign the resource from the **Properties** section of the
**Selection** panel (right-side). (This is also where you can see the specific
resource that was assigned from a resource pool.)

.. _reset_resource_group_overrides:

Reset Resource Group Overrides
------------------------------
Certain blueprint operations require AOS to retain resource allocations even when
a device has been removed from a blueprint. The resource groups are overridden,
which mean that when a device is re-used, previously allocated resources are
re-used as well. For example, if you've deleted a rack, then you rollback to a
version with that rack, AOS must ensure that the same resources are used.
Otherwise, the topology would change (for example, it might have different IP
addresses). Situations like this can (but do not always) result in build errors.
Some other examples of where resource group overrides are used include:

* Other time voyager rollbacks.
* :doc:`Revert <uncommitted>` operations.
* Using the **Update Stated Cabling Map from LLDP** feature.

If you do not need to re-use the same resources, click the
**Reset resource group overrides** button to reset the resource group.

Reverting Example
^^^^^^^^^^^^^^^^^
After reverting the changes in a blueprint, resources become "sticky" and even
removing a pool or changing it to a new one doesn't affect the resulting config.
In the blueprint below, we can see old resources still assigned.

.. image:: static/images/blueprint_resources_groups_reset/revert.png

.. image:: static/images/blueprint_resources_groups_reset/old_resources.png

Click the **Reset resource group overrides** button. Then resources can be
unallocated, and new ones can be allocated, as applicable.

Manage Resource Pools
---------------------
The resource assignment section has a convenient shortcut button,
**Manage resource pools**, that takes you to resource pool management.
There, you can monitor resource usage (new in AOS version 3.3.0) and
create additional resource pools, as needed.

.. _staging_device_profiles:

Staging Device Profiles
=======================
#. From the :ref:`blueprint <access_blueprint>`, navigate to
   **Staged / Physical / Build / Device Profiles**.

   .. image:: static/images/blueprints/staged/physical/device_profiles_stage_330.png

#. Click a red status indicator, then click the
   **Change interface maps assignment** button (looks like an edit button).
   Device profiles are staged by assigning interface maps.

#. Select the appropriate interface map from the drop-down list for each node.
   Or, to assign the same interface map to multiple nodes, you can select the ones
   that use the same interface map (or all of them with one click), then select
   the interface map from the drop-down list located above the selections, and
   click **Assign Selected**.

   .. image:: static/images/blueprints/staged/physical/device_profiles_stage_dialog_330.png

#. Click **Update Assignments**. When the red status indicator turns green, the
   device profile assignments have been successfully staged.

.. _staging_devices:

Staging Devices
===============
Devices must have interface maps associated with them before they can have
system IDs assigned to them. See the previous section for details.
When a device is assigned to a blueprint, it performs discovery configuration.
During this phase all interfaces are changed to L3-only mode allowing
interfaces to be *up*. There is no BGP configuration, no routing expectations,
nothing that can influence the network. A device in *discovery* mode is benign;
it does not participate in the datacenter fabric, and it does not forward any
packets through it. You can then perform critical validations of network health
including viewing statistics for cabling, LLDP, transceivers and more. Any issues,
such as miscabling or physical link errors, cause a telemetry alarm. You can
address and correct the anomalies *before* deploying the device.

.. sidebar:: Sanitize the Device

   When resetting the system ID (serial number) Discovery-1 is re-applied. It is
   good practice to fully erase the device configuration and uninstall the AOS
   device agent before physically uninstalling the device.

Assigning System IDs and Deploy Modes to Multiple Devices
---------------------------------------------------------
#. From the blueprint, navigate to **Staged / Physical / Build / Devices**, and
   click the status indicator for **Assigned System IDs** (if the nodes list is
   not already displayed). Unassigned devices are indicated in yellow.

   .. image:: static/images/blueprints/staged/physical/devices_staged_330.png

#. Click the **Change System IDs assignments** button (below Assigned System IDs)
   and, for each node, select system IDs from the drop-down list.

   .. image:: static/images/blueprints/staged/physical/device_assign_dialog_330.png

   .. tip::
       If you don't see an expected serial number (system ID), you may still need
       to :ref:`acknowledge the device <ack_device>` (Devices / Managed Devices).

#. When a system ID is selected, the deploy mode changes to **Deploy** by default.
   If you don't want to stage the device to be deployed yet, change the deploy
   mode here. When you're ready to deploy the device, return here to set the
   deploy mode back to **Deploy**. See the next section for more information
   about deploy modes.

#. Click **Update Assignments** to stage the changes. Before the task is completed
   you can click **Active Tasks** at the bottom of the screen to see its progress.

   .. note::
       You can also use AOS CLI to bulk-assign system IDs to devices either with a
       CSV text file or the ``blueprint set-serial-numbers`` command.

.. _system_id:

Assigning System IDs to One Device
----------------------------------
#. From the blueprint, navigate to **Staged / Physical / Build / Devices**; if
   you don't see the nodes list, click the status indicator for
   **Assigned System IDs**.

   .. image:: static/images/blueprints/staged/physical/device_assign_one_330.png

#. Click a node name to see device details (deploy mode, serial number, hostname
   rendered, incremental and pristine config, as applicable).

   .. tip::
       Another way to access these device attributes is to click
       **Selected Nodes** (left-middle), then select a node name in the drop-down
       list.

   .. image:: static/images/blueprints/staged/physical/device_assign_one2_330.png

#. To assign a system ID, click the **Edit** button for **S/N**, select the system
   ID from the drop-down list, and click the **Save** button to stage the change.

   .. tip::
       If you don't see the expected serial number (system ID), you may still
       need to :doc:`acknowledge the device <managed_devices>`
       (Devices / Managed Devices).

#. If you want to remove an existing S/N instead of assigning one, click the
   **Edit** button for **S/N**, then click the red square to stage the change.

.. _change_deploy_modes:

Changing Deploy Mode on One Device
----------------------------------

#. From the blueprint, navigate to **Staged / Physical / Build / Devices**; if
   you don't see the nodes list, click the status indicator for
   **Assigned System IDs**.
#. Click a node name to see device details.
#. Click the **Edit** button for **Deploy Mode** and select a deploy mode.

   * Deploy - Adds service configuration and puts the device fully in service.
   * Ready - Adds discovery 2 configuration (hostnames, interface descriptions,
     port speeds / breakouts). Changing from deploy to ready removes service
     configuration.
   * Drain - Takes a device out of service gracefully for maintenance. See
     :doc:`Draining Device Traffic <device_drain>` for more information.

     .. tip::

        While TCP sessions drain (which could take some time, especially for EVPN
        blueprints) BGP anomalies are expected. When configuration deployment is
        complete the temporary anomalies are resolved. To ensure switches are
        completely drained before undeploying them, you could
        :ref:`instantiate <instantiate_dashboard>` the drain validation dashboard
        to monitor progress.


   * Undeploy - Removes AOS-rendered configuration. If a device is carrying
     traffic it is best to first put the device into drain mode (and commit the
     change). When the device is completely drained, proceed to undeploy the
     device.

#. Click the **Save** button to stage the change.
#. When you are ready, :doc:`commit <uncommitted>` the changes.

.. tip::

  You can also change deploy modes from **Staged / Physical / Nodes**. See
  :ref:`Setting Deploy Mode for Multiple Nodes <change_node_deploy_mode>` for
  more information.

Changing Hostname on One Device
-------------------------------
#. From the blueprint, navigate to **Staged / Physical / Build / Devices**; if
   you don't see the nodes list, click the status indicator for
   **Assigned System IDs**.
#. Click a node name to see device details.
#. Click the **Edit** button for **Hostname**, change the name, and click the
   **Save** button to stage the change.
#. :doc:`Commit <uncommitted>` the changes.

.. _staging_external_routers:

Staging External Routers
========================

.. image:: static/images/blueprints/staged/physical/external_router_stage_330.png

#. Make sure that the appropriate external routers have been
   :ref:`imported <import_external_router>` into the blueprint catalog from the
   global catalog.
#. From the :ref:`blueprint <access_blueprint>`, navigate to
   **Staged / Physical / Build / External Routers**.
#. Click the red status indicator for **External Router Links**. If the external
   router has not been imported yet, you can click the **Import External Router**
   button, select the external router from the drop-down list, and click
   **Import External Router**.
#. Click the **Edit Links** button, then select the external router from the
   drop-down list.
#. Select **Connectivity Type**.

   * L3 (default) - for BGP loopback (eBGP multi-hop) peering
   * L2 - for BGP interface on L2 SVI peering. Overlay control protocol must be
     MP-EBGP EVPN and external router connections must be leaf. Spine external
     router connections are not supported.
#. Select external router links. By default, all available external router
   interface links are selected. If more than one external router is imported,
   you can add and remove links as needed for each external router. All defined
   external router interface links must be configured before the blueprint can be
   deployed.
#. Click **Update** to stage the external router and return to the list view.
#. L3 connectivity requires additional IP resources for
   **Link IPs - To External Router** for the default security zone.
   See **Staging Physical Resources** above for steps.

As of AOS version 2.3.2, If external routers are used in an EVPN blueprint,
you must add external connectivity points (ECPs) in the security zone.
See :ref:`security_zones:Security Zone External Connectivity Points` for more
information.

.. _staging_configlets:

Staging Configlets
==================
Configlets are vendor-specific. AOS automatically ensures that configlets of a
specific vendor are not assigned to devices from a different vendor.

#. Make sure that the appropriate configlets have been
   :ref:`imported <import_configlet>` into the blueprint catalog from the
   global catalog.
#. From the :ref:`blueprint <access_blueprint>`, navigate to
   **Staged / Physical / Build / Configlets**.
#. If the configlet has not been imported yet, you can click **Manage Configlets**
   to import it. See :ref:`Importing Configlet <import_configlet>` for details.
#. Click the status indicator for the configlet. If the configlet uses a property
   set, click the **Import Property Set** button, select the property set from
   the drop-down list, then click **Import Property Set**.
