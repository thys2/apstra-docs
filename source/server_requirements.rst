==================================
AOS Server Requirements/References
==================================

Hypervisors
===========
AOS Server can be deployed on the following hypervisors.

   VMware ESXi
      Supported versions - 6.7, 6.5, 6.0, 5.5

   QEMU / KVM for Ubuntu
      Supported versions - 18.04 LTS

   Microsoft Hyper-V
      Supported version - Windows Server 2016 Datacenter Edition

   Oracle VirtualBox / VMware Workstation
      For lab / evaluation purposes only

AOS Server VM Resources
=======================
The required VM resources for an AOS server may be greater than the recommendations
below. Requirements are based on the size of the network (blueprint), the scaling
of off-box agents and the use of Intent Based Analytics (IBA). If one VM is
insufficient for your needs, you can increase resources by clustering several
VMs (Platform / AOS Cluster).

+----------+----------------------------------------------------+
| Resource | Recommendation                                     |
+==========+====================================================+
| Memory   | 64 GB RAM + 300 MB per installed off-box agent*    |
+----------+----------------------------------------------------+
| CPU      | 8 vCPU                                             |
+----------+----------------------------------------------------+
| Disk     | 80 GB                                              |
+----------+----------------------------------------------------+
| Network  | 1 network adapter, initially configured with DHCP  |
+----------+----------------------------------------------------+

.. note::

    \* AOS off-box agent memory usage is dependent on the number of IBA collectors
    enabled. Apstra recommends using the AOS web interface
    :ref:`aos_cluster:AOS Cluster` feature to monitor off-box container memory
    usage (e.g. aos-offbox-172_20_88_11-f). Additional AOS cluster
    worker nodes can be added to scale off-box agent capacity.

    .. image:: static/images/server_requirements/cluster_330.png

.. important::

   Although, an AOS server VM might run with fewer resources than specified above,
   the CPU and RAM allocations may be insufficient, depending on the size of the
   AOS network. In this case, the system encounters errors or a critical
   "segmentation fault" (core dump). If this happens, delete the VM and redeploy
   it with additional resources.

.. _network-security-requirements:

Network Security Protocols
==========================
Open ports and services that run on the AOS server are listed in the table below.
A running iptables instance ensures that network traffic to and from the AOS server
is restricted to the services listed.

.. include:: includes/network_requirements.rst

Other Network Protocols
=======================
The network protocols in the table below are not required for AOS Server
functionality, but they may be required for network device configuration
and discovery, and for direct access to Devices.

.. list-table:: Other Network Protocols
   :header-rows: 1
   :class: nowrap

   * - Source
     - Destination
     - Protocol
     - Description
   * - Administrator
     - Network Device
     - tcp/22 (ssh)
     - Device management from Administrator
   * - Network Device
     - DNS Server
     - udp/53 (dns)
     - DNS Discovery for AOS Server IP (if applicable)
   * - Network Device
     - DHCP Server
     - udp/67-68 (dhcp)
     - DHCP for automatic management IP (if applicable)
   * -
     -
     - (icmp type 0, type 8 for echo and response)
     - As necessary for network troubleshooting. Not required for AOS Server.

Network Client Services
=======================
AOS server use and configuration determine the number of network client services
that must be enabled.

.. list-table:: AOS Server Network Client Services
   :header-rows: 1
   :class: nowrap

   * - Source
     - Destination
     - Protocol
     - Description
   * - AOS Server
     - DNS Server
     - udp/53 (dns)
     - Server DNS Client
   * - AOS Server
     - LDAP Server
     - tcp/389 (ldap)
       tcp/636 (ldaps)
     - AOS Server LDAP Client (if configured)
   * - AOS Server
     - TACACS+ Server
     - tcp/udp/49 (tacacs)
     - AOS Server TACACS+ Client (if configured)
   * - AOS Server
     - RADIUS Server
     - tcp/udp/1812 (radius)
     - AOS Server RADIUS Client (if configured)
   * - AOS Server
     - Syslog Server
     - udp/514 (syslog)
     - AOS Server Syslog Client (if configured)
